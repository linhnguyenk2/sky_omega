<?php
require_once 'application\libraries\Doctrine_console.php';
return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);
