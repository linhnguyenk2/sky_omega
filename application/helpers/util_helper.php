<?php
    $CI =& get_instance();    
    $CI->load->helper('date');
    $CI->load->library('email');
    
    /**
     * formatMoney
     * use to format money
     * @param string $number
     * @return string
     */
    function formatMoney($number) {        
        while (true) { 
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1.$2', $number); 
            if ($replaced != $number) $number = $replaced; 
            else break; 
        } 
        return $number; 
    }
    /**
     * dateDiff
     * count day between two date
     * @param string $start fromdate yyyy-mm-dd
     * @param string $end todate yyyy-mm-dd
     * @return int
     */
    function dateDiff($start, $end) {
        date_default_timezone_set('Asia/Bangkok');
        $start_ts = strtotime($start);
        $end_ts = strtotime($end);
        $diff = $end_ts - $start_ts;//abs
        return round($diff / 86400);
    }
    /**
     * makeThumbnails    
     * @param string $img file path
     * @param int $ratio
     * @return new file path
     */
    function makeThumbnails($img, $ratio=1) {
        try {                
            $thumbnail_width = ($ratio ? 336 * $ratio : 336);
            $thumbnail_height = ($ratio ? 189 * $ratio : 189);
            $thumb_beforeword = ($ratio == 1 ? "thumb_" : ($ratio == 2 ? "show_" : "full_"));
            $updir = "/home/betaremar/domains/beta.remarket.vn/private_html" . substr($img, 0, strrpos($img, '/') + 1);
            $fileName = substr($img, strrpos($img, '/') + 1);

            $arr_image_details = getimagesize($updir . $fileName);
            $original_width = $arr_image_details[0];
            $original_height = $arr_image_details[1];
            if ($original_width > $original_height) {
                $new_width = $thumbnail_width;
                $new_height = intval($original_height * $new_width / $original_width);
            } else {
                $new_height = $thumbnail_height;
                $new_width = intval($original_width * $new_height / $original_height);
            }
            $dest_x = intval(($thumbnail_width - $new_width) / 2);
            $dest_y = intval(($thumbnail_height - $new_height) / 2);
            if ($arr_image_details[2] == 1) {
                $imgt = "ImageGIF";
                $imgcreatefrom = "ImageCreateFromGIF";
            }
            if ($arr_image_details[2] == 2) {
                $imgt = "ImageJPEG";
                $imgcreatefrom = "ImageCreateFromJPEG";
            }
            if ($arr_image_details[2] == 3) {
                $imgt = "ImagePNG";
                $imgcreatefrom = "ImageCreateFromPNG";
            }
            if ($imgt) {
                $old_image = $imgcreatefrom($updir . $fileName);
                $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
                imagecopyresampled($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, 
                                   $new_height, $original_width, $original_height);
                $imgt($new_image, $updir . $thumb_beforeword . $fileName, 100);
            }
            return substr($img, 0, strrpos($img, '/') + 1) . $thumb_beforeword . $fileName;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    /**
     * archiveAndStamp
     * zip and stamp watermark to img
     * @param string $filename img name
     * @param integer $filesize img size in byte
     * @return boolean
     */    
    function archiveAndStamp($filename, $filesize) {        
        // Load the stamp and the photo to apply the watermark to
        $stamp = imagecreatefrompng('/home/konned/domains/konned.com/public_html/assets/images/logo_color-02.png');
        $type = strtolower(substr(strrchr($filename, '.'), 1));
        switch ($type) {
            case 'jpg':
            case 'jpeg':
                $im = imagecreatefromjpeg($filename);
                break;
            case 'gif':
                $im = imagecreatefromgif($filename);
                break;
            case 'png':
                $im = imagecreatefrompng($filename);
                break;
            default:
                return false;
        }

        $compress = 100;

//        if ($filesize < 1024 * 400)
//            $compress = 100;

        $marge_right = 10;
        //$marge_bottom = 10;
        $marge_top = 10;
        $sx = imagesx($stamp);
        $sy = imagesy($stamp);

        //imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, 
        //          imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
        imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, 
                  $marge_top, 0, 0, imagesx($stamp), imagesy($stamp));
                
        //imagejpeg($im, $filename, $compress);
        
        // resize
        $orig_width = imagesx($im);
        $orig_height = imagesy($im);

        // Calc the new width
        $width = ceil(($orig_width * 468) / $orig_height);

        $new_image = imagecreatetruecolor($width, 468);
        imagecopyresized($new_image, $im,
                0, 0, 0, 0,
                $width, 468,
                $orig_width, $orig_height);

        imagejpeg($new_image, $filename, $compress);
        
        imagedestroy($im);
        imagedestroy($new_image);
    }
    /**
     * encrypt string
     * @param str $text text to encrypt
     * @param str $salt private key
     * @return str
     */
    function encrypt($text, $salt) {
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));            
    }
    /**
     * decrypt string
     * @param str $text text to decrypt
     * @param str $salt private key
     * @return str
     */
    function decrypt($text, $salt) {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));        
    }
    /**
     * log errors
     * @param type $seriousErr ['SERIOUS', 'NORMAL']
     * @param str $logType ['CARD', 'MASTER', 'DAILY', 'SQL']     
     * @param type $errType ['ERROR', 'WARNING', 'INFO', 'DANGER']
     * @param type $errContent
     */
    function logRM($seriousErr, $logType, $errType, $errContent) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $date = mdate("%Y-%m-%d %h:%i:%s", time());
        $mm = substr($date, 5, 2);        
        $dd = ($logType == "DAILY" ? substr($date, 8, 2) . "/" : "");
        $folder = "/home/konned/domains/konned.com/public_html/application/logs/{$seriousErr}/{$logType}/{$mm}/{$dd}";
        $fName = "Monthly.log";
        
        switch ($logType) {
                case 'CARD'  :                    
                    $fName = "CardFunding.log"; 
                    break;
                case 'MASTER':                     
                    $fName = "MasterFunding.log"; 
                    break;
                case 'SQL':                     
                    $fName = "SQL.log"; 
                    break;
                case 'DAILY' :                     
                    $fName = "Daily.log"; 
                    break;
                case 'CLIENTS' :                     
                    $fName = "Clients.log"; 
                    break;
                case 'BPN' :                     
                    $fName = "BPN.log"; 
                    break;
                default      :                     
                    $fName = "Default.log"; 
                    break;
            }
        
        try {
            if (!file_exists($folder))
                mkdir($folder, 0755, true);
            $file = $folder . $fName;
            $fh = fopen($file, 'a');

            fwrite($fh, "[" . $errType . "] " . $date . ": " . $errContent . "\r\n");        
            fclose($fh);
        } catch(Exception $ex) {}
    }    
        
    function timeSince ($time) {
        $time = time() - $time;
        $time = ($time<1)? 1 : $time;
        $tokens = array (
            31536000 => 'năm',
            2592000 => 'tháng',
            604800 => 'tuần',
            86400 => 'ngày',
            3600 => 'giờ',
            60 => 'phút',
            1 => 'giây'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits . ' ' . $text . (($numberOfUnits>1)? ' trước' : '');
        }
    }
    //danh sách nhân sự --> danh sach nhan su
    //Error Number: 1271
    function utf8convert($str) {
        //return $str;
        if(!$str) return false;
        $utf8 = array(
            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'd'=>'đ|Đ',
            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',
            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        );
        foreach($utf8 as $ascii=>$uni) $str = preg_replace("/($uni)/i",$ascii,$str);
        $str = strtolower( trim($str, '-') );
        $str = preg_replace(array('`[^a-z0-9]/\s+/`i','`[-]+`'), '_', $str);
        return $str;
    }
    //https://stackoverflow.com/a/8102246/9811260
    function help_seach_array_value($products, $field, $value){
        //var_dump($products, $field, $value);die;
        foreach($products as $key => $product)
        {
            if ( $product[$field] === $value )
                return $key;
        }
        return false;
    }
    /**
     * get description of a function
     */
    function help_get_info_table($name_table=null){
        if(!isset($name_table)) return null;

        $CI =& get_instance(); //extend all infomation of ci
        $query = $CI->db->query("DESCRIBE ".$name_table); //query database
        $result=$query->result_array();
        return $result;

    }
    function var_dumpci($value){
        echo '<pre>';
        var_dump($value);
        echo '</pre>';
    }
?>
