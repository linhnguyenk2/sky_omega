<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']   = 'Index/index';
$route['forgotpassword']       = 'app/Signin/forgotpassword';
$route['404_override']         = 'app/Signin/pageNotFound';
$route['translate_uri_dashes'] = false;

//sign-in 
$route['signin']    = 'app/Signin/index';
$route['dashboard'] = 'app/Dashboard/index';
//manage list

$route['list_nations'] = 'app/list/List_nations/list_nations';
$route['list_provinces'] = 'app/list/List_provinces/list_provinces';
$route['list_districts'] = 'app/list/List_districts/list_districts';
//$route['list_towns'] = 'app/list/List_towns/list_towns';
$route['list_wards']       = 'app/list/List_wards/list_wards';
$route['list_titles']      = 'app/list/List_titles/list_titles';
$route['list_departments'] = 'app/list/List_departments/list_departments';

$route['list_reason_discharge'] = 'app/list/List_reason_discharge/list_reason_discharge';
$route['list_reason_transfer']  = 'app/list/List_reason_transfer/list_reason_transfer';
$route['list_materials']        = 'app/Si_List/list_materials';
$route['list_midwives']         = 'app/Si_List/list_midwives';
$route['list_group_therapys']   = 'app/Si_List/list_group_therapys';
$route['list_vendors']          = 'app/Si_List/list_vendors';
$route['list_currencies']       = 'app/list/List_currencies/list_currencies';

$route['list_folks']            = 'app/list/List_folks/list_folks';
$route['list_clinic']           = 'app/Si_List/list_clinic';

$route['list_service'] = 'app/Si_List/list_service';
$route['list_service_group'] = 'app/list/List_service_group/list_service_group';
$route['list_service_type']  = 'app/list/List_service_type/list_service_type';

$route['list_package_healthcare_service_type'] = 'app/Si_List/list_package_healthcare_service_type';
$route['list_package_healthcare_service'] = 'app/list/List_package_healthcare_service/list_package_healthcare_service';

$route['list_contraindication']       = 'app/list/List_contraindication/list_contraindication';
$route['list_insurrance_company']     = 'app/list/List_insurrance_company/list_insurrance_company';
$route['list_health_insurrance_card'] = 'app/list/List_health_insurrance_card/list_health_insurrance_card';
$route['list_supllier']               = 'app/list/List_supllier/list_supllier';

$route['list_medical_record']               = 'app/list/List_medical_record/list_medical_record';
$route['list_relationship_between_patient'] = 'app/list/List_relationship_between_patient/list_relationship_between_patient';

$route['list_place_transfer']     = 'app/Si_List_Test/list_place_transfer';
$route['list_place_introduction'] = 'app/Si_List_Test/list_place_introduction';
$route['list_method_birth']       = 'app/Si_List_Test/list_method_birth';
$route['list_foreigner']          = 'app/list/List_foreigner/list_foreigner';

//$route['patient_information'] = 'app/Si_Patient/patient_information/';
//$route['patient_information/(:num)'] = 'app/Si_Patient/patient_information/$1';
//$route['patient_information'] = 'app/patient/Patient_information/patient_information';
$route['patient_information/(:num)'] = 'app/patient/Patient_information/patient_information/$1';
$route['list_register_services'] = 'app/patient/List_register_services/list_register_services';
$route['add_services'] = 'app/patient/Add_services/add_services';
$route['edit_services/(:num)'] = 'app/patient/Edit_services/edit_services/$1';
$route['maternity_hospital']         = 'app/Si_Patient/maternity_hospital';

$route['list_examination'] = 'app/Si_Enclitic/list_examination';
$route['list_set_time']    = 'app/Si_Enclitic/list_set_time';

$route['list_chapter_icd10'] = 'app/list/List_chapter_ICD10/list_chapter_icd10';
$route['list_disease_icd10'] = 'app/list/List_disease_ICD10/list_disease_icd10';
$route['list_group_icd10']   = 'app/list/List_group_ICD10/list_group_icd10';

$route['list_transport_vehicle'] = 'app/list/List_transport_vehicle/list_transport_vehicle';
$route['list_medical_supplies']  = 'app/list/List_medical_supplies/list_medical_supplies';
$route['list_functional_foods']  = 'app/list/List_functional_foods/list_functional_foods';

$route['list_patient_room_type'] = 'app/list/List_patient_room_type/list_patient_room_type';
$route['list_patient_room']      = 'app/list/List_patient_room/list_patient_room';
$route['list_patient_bed']       = 'app/list/List_patient_bed/list_patient_bed';
//$route['list_patient'] = 'app/Si_List_Test/list_patient';
$route['list_patient']     = 'app/receive/List_patient/list_patient';
$route['list_patient_add'] = 'app/receive/List_patient_add/list_patient_add';

$route['outpatient_book_list'] = 'app/book/Outpatient_book_list/outpatient_book_list'; //Danh Sách Sổ Khám Phụ Khoa
// $route['outpatient_book_detail'] = 'app/book/Outpatient_book_detail/outpatient_book_detail';
$route['outpatient_book_detail'] = 'app/book/Outpatient_book_detail/outpatient_book_detail';
// $route['gynecological_examination'] = 'app/book/Gynecological_examination/gynecological_examination';
$route['gynecological_examination/(:num)'] = 'app/book/Gynecological_examination/gynecological_examination/$1';

$route['pregnancy_checklist_list'] = 'app/book/Pregnancy_checklist_list/pregnancy_checklist_list'; //Danh Sách Sổ Khám Thai
// $route['pregnancy_checklist'] = 'app/book/Pregnancy_checklist/pregnancy_checklist';
$route['pregnancy_checklist']   = 'app/book/Pregnancy_checklist/pregnancy_checklist';
$route['pregnancy_examination/(:num)'] = 'app/book/Pregnancy_examination/pregnancy_examination/$1';

$route['book_of_examinations_list']   = 'app/book/Book_of_examinations_list/book_of_examinations_list'; //Danh Sách Sổ Khám Nhủ
$route['book_of_examinations'] = 'app/book/Book_of_examinations/book_of_examinations';
$route['visiting_breast/(:num)']      = 'app/book/Visiting_breast/visiting_breast/$1';

$route['test_list_service_designation'] = 'app/test_subclinical/List_service_designation/list_service_designation';
$route['test_list_form_check']          = 'app/test_subclinical/List_form_check/list_form_check';
$route['test_list_subclinical']         = 'app/test_subclinical/List_subclinical/list_subclinical';

$route['the_manchuria_book_list']    = 'app/book/The_manchuria_book_list/the_manchuria_book_list'; //Danh Sách Sổ Mãn Kinh
$route['the_manchuria_book']  = 'app/book/The_manchuria_book/the_manchuria_book';
$route['self_tracking_sheet/(:num)'] = 'app/book/Self_tracking_sheet/self_tracking_sheet/$1';


$route['family_planing_book']    = 'app/book/Family_planing_book/family_planing_book';
$route['family_planing_book_list']    = 'app/book/Family_planing_book_list/family_planing_book_list';
$route['family_plan_birth_control_form/(:num)']    = 'app/book/Family_plan_birth_control_form/family_plan_birth_control_form/$1';
$route['artificial_examination_form/(:num)']    = 'app/book/Artificial_examination_form/artificial_examination_form/$1';

/* Warehouse */
// warehouse - Kho
$route['warehouse_list']       = 'app/warehouse/Warehouse_list/warehouse_list';

// inventories - Lô hàng theo ngày hết hạn  - sản phẩm trong kho
$route['warehouse_product_in_package_list'] = 'app/warehouse/Warehouse_product_in_package_list/warehouse_product_in_package_list';

// Danh sách phiếu yêu cầu xuất
$route['warehouse_export_form_list']   = 'app/warehouse/Warehouse_export_form_list/warehouse_export_form_list';
$route['warehouse_export_form/(:any)'] = 'app/warehouse/Warehouse_export_form/warehouse_export_form/$1';

// Danh sách phiếu xuất bán
$route['warehouse_sale_form_list']   = 'app/warehouse/Warehouse_sale_form_list/warehouse_sale_form_list';
$route['warehouse_sale_form/(:any)'] = 'app/warehouse/Warehouse_sale_form/warehouse_sale_form/$1';

// Danh sách phiếu yêu cầu xuất
$route['warehouse_request_form_list']   = 'app/warehouse/Warehouse_request_form_list/warehouse_request_form_list';
$route['warehouse_request_form/(:any)'] = 'app/warehouse/Warehouse_request_form/warehouse_request_form/$1';

// Danh sách phiếu kiểm kho
$route['warehouse_check_form_list']   = 'app/warehouse/Warehouse_check_form_list/warehouse_check_form_list';
$route['warehouse_check_form/(:any)'] = 'app/warehouse/Warehouse_check_form/warehouse_check_form/$1';

$route['warehouse_import_form_list']   = 'app/warehouse/Warehouse_import_form_list/warehouse_import_form_list';
$route['warehouse_import_form/(:any)'] = 'app/warehouse/Warehouse_import_form/warehouse_import_form/$1';

$route['warehouse_use_form_list']   = 'app/warehouse/Warehouse_use_form_list/warehouse_use_form_list';
$route['warehouse_use_form/(:any)'] = 'app/warehouse/Warehouse_use_form/warehouse_use_form/$1';

$route['warehouse_product_in_import_form']     = 'app/warehouse/Warehouse_product_in_import_form/warehouse_product_in_import_form';
$route['warehouse_product_in_sale_form']       = 'app/warehouse/Warehouse_product_in_sale_form/warehouse_product_in_sale_form';
$route['warehouse_product_in_request_form']    = 'app/warehouse/Warehouse_product_in_request_form/warehouse_product_in_request_form';
$route['product_in_package_in_check_form']     = 'app/warehouse/Product_in_package_in_check_form/product_in_package_in_check_form';
$route['product_in_package_in_use_form']       = 'app/warehouse/Product_in_package_in_use_form/product_in_package_in_use_form';
$route['warehouse_package']                    = 'app/warehouse/Warehouse_package/warehouse_package';
$route['warehouse_product_in_package']         = 'app/warehouse/Wrehouse_product_in_package/warehouse_product_in_package';
$route['warehouse_product_in_package_by_date'] = 'app/warehouse/Warehouse_product_in_package_by_date/warehouse_product_in_package_by_date';

$route['cosmetic_list']                    = 'app/warehouse/Cosmetic_list/cosmetic_list';
$route['medicine_product']               = 'app/warehouse/Medicine_product/medicine_product';
$route['medical_supply']               = 'app/warehouse/Medical_supply/medical_supply';

/*medicine_product
$route['list_api'] = 'Si_List_Api/index';
$route['list_api/add'] = 'Si_List_Api/add';
$route['list_api/edit'] = 'Si_List_Api/edit';
$route['list_api/delete'] = 'Si_List_Api/delete';
$route['list_api/push'] = 'Si_List_Api/push';
$route['list_api/unpush'] = 'Si_List_Api/unpush';
*/



$route['list_towns/get']    = 'Si_List_Action/get'; //get
$route['list_towns/add']    = 'Si_List_Action/add';
$route['list_towns/edit']   = 'Si_List_Action/edit';
$route['list_towns/delete'] = 'Si_List_Action/delete';
$route['list_towns/push']   = 'Si_List_Action/push';
$route['list_towns/unpush'] = 'Si_List_Action/unpush';


$route['list_materials/get']    = 'Si_List_Action/get'; //get
$route['list_materials/add']    = 'Si_List_Action/add';
$route['list_materials/edit']   = 'Si_List_Action/edit';
$route['list_materials/delete'] = 'Si_List_Action/delete';
$route['list_materials/push']   = 'Si_List_Action/push';
$route['list_materials/unpush'] = 'Si_List_Action/unpush';


$route['list_midwives/get']    = 'Si_List_Action/get'; //get
$route['list_midwives/add']    = 'Si_List_Action/add';
$route['list_midwives/edit']   = 'Si_List_Action/edit';
$route['list_midwives/delete'] = 'Si_List_Action/delete';
$route['list_midwives/push']   = 'Si_List_Action/push';
$route['list_midwives/unpush'] = 'Si_List_Action/unpush';


$route['list_group_therapys/get']    = 'Si_List_Action/get'; //get
$route['list_group_therapys/add']    = 'Si_List_Action/add';
$route['list_group_therapys/edit']   = 'Si_List_Action/edit';
$route['list_group_therapys/delete'] = 'Si_List_Action/delete';
$route['list_group_therapys/push']   = 'Si_List_Action/push';
$route['list_group_therapys/unpush'] = 'Si_List_Action/unpush';

$route['list_vendors/get']    = 'Si_List_Action/get'; //get
$route['list_vendors/add']    = 'Si_List_Action/add';
$route['list_vendors/edit']   = 'Si_List_Action/edit';
$route['list_vendors/delete'] = 'Si_List_Action/delete';
$route['list_vendors/push']   = 'Si_List_Action/push';
$route['list_vendors/unpush'] = 'Si_List_Action/unpush';



$route['list_clinic/get']          = 'Si_List_Action/get'; //get
$route['list_clinic/add']          = 'Si_List_Action/add';
$route['list_clinic/edit']['get']  = 'Si_List_Action/edit_get'; //$_GET
$route['list_clinic/edit']['post'] = 'Si_List_Action/edit'; //$_POST
$route['list_clinic/delete']       = 'Si_List_Action/delete';
$route['list_clinic/push']         = 'Si_List_Action/push';
$route['list_clinic/unpush']       = 'Si_List_Action/unpush';



$route['list_service/get']          = 'Si_List_Action/get'; //get
$route['list_service/add']          = 'Si_List_Action/add';
$route['list_service/edit']['get']  = 'Si_List_Action/edit_get'; //$_GET
$route['list_service/edit']['post'] = 'Si_List_Action/edit'; //$_POST
$route['list_service/delete']       = 'Si_List_Action/delete';
$route['list_service/push']         = 'Si_List_Action/push';
$route['list_service/unpush']       = 'Si_List_Action/unpush';

$route['list_service_type/get']          = 'Si_List_Action/get'; //get
$route['list_service_type/add']          = 'Si_List_Action/add';
$route['list_service_type/edit']['get']  = 'Si_List_Action/edit_get'; //$_GET
$route['list_service_type/edit']['post'] = 'Si_List_Action/edit'; //$_POST
$route['list_service_type/delete']       = 'Si_List_Action/delete';
$route['list_service_type/push']         = 'Si_List_Action/push';
$route['list_service_type/unpush']       = 'Si_List_Action/unpush';

$route['list_package_healthcare_service/get']            = 'Si_List_Action/get'; //get
$route['list_package_healthcare_service/add']['post']    = 'Si_List_Action/add';
$route['list_package_healthcare_service/add']['get']     = 'Si_List_Action/add_get';
$route['list_package_healthcare_service/edit']['get']    = 'Si_List_Action/edit_get'; //$_GET
$route['list_package_healthcare_service/edit']['post']   = 'Si_List_Action/edit'; //$_POST
$route['list_package_healthcare_service/edit']['delete'] = 'Si_List_Action/edit_delete'; //$_DELETE
$route['list_package_healthcare_service/delete']         = 'Si_List_Action/delete';
$route['list_package_healthcare_service/push']           = 'Si_List_Action/push';
$route['list_package_healthcare_service/unpush']         = 'Si_List_Action/unpush';

$route['list_package_healthcare_service_type/get']          = 'Si_List_Action/get'; //get
$route['list_package_healthcare_service_type/add']          = 'Si_List_Action/add';
$route['list_package_healthcare_service_type/edit']['get']  = 'Si_List_Action/edit_get'; //$_GET
$route['list_package_healthcare_service_type/edit']['post'] = 'Si_List_Action/edit'; //$_POST
$route['list_package_healthcare_service_type/delete']       = 'Si_List_Action/delete';
$route['list_package_healthcare_service_type/push']         = 'Si_List_Action/push';
$route['list_package_healthcare_service_type/unpush']       = 'Si_List_Action/unpush';


//manage use

$route['manage_user']['get']  = 'app/Account/basic'; //get
$route['manage_user']['post'] = 'api/v1/Account/basic'; //post
$route['manage_user/get']     = 'api/v1/Account/get'; //get api data
$route['manage_user/add']     = 'api/v1/Account/add';
$route['manage_user/edit']    = 'api/v1/Account/edit';
$route['manage_user/delete']  = 'api/v1/Account/delete';
$route['manage_user/push']    = 'api/v1/Account/push';
$route['manage_user/unpush']  = 'api/v1/Account/unpush';

$route['profile'] = 'app/Account/profile';


// manage role and permission

$route['permission_role']        = 'app/Si_Permission/permission_role'; //jsut view
$route['permission_screen']      = 'app/Si_Permission/permission_screen';
$route['permission_role_screen'] = 'app/Si_Permission/permission_role_screen';

$route['permission_role/get']    = 'api/v1/Si_Permission_Action/get';
$route['permission_role/add']    = 'api/v1/Si_Permission_Action/add';
$route['permission_role/edit']   = 'api/v1/Si_Permission_Action/edit';
$route['permission_role/delete'] = 'api/v1/Si_Permission_Action/delete';
$route['permission_role/push']   = 'api/v1/Si_Permission_Action/push';
$route['permission_role/unpush'] = 'api/v1/Si_Permission_Action/unpush';

$route['permission_screen/get']    = 'api/v1/Si_Permission_Action/get';
$route['permission_screen/add']    = 'api/v1/Si_Permission_Action/add';
$route['permission_screen/edit']   = 'api/v1/Si_Permission_Action/edit';
$route['permission_screen/delete'] = 'api/v1/Si_Permission_Action/delete';
$route['permission_screen/push']   = 'api/v1/Si_Permission_Action/push';
$route['permission_screen/unpush'] = 'api/v1/Si_Permission_Action/unpush';

$route['permission_role_screen/get']  = 'api/v1/Si_Permission_Action/role_screen_get';
$route['permission_role_screen/edit'] = 'api/v1/Si_Permission_Action/role_screen_update';
// $route['permission_role_screen/user'] = 'api/v1/Si_Permission_Action/role_screen_update'; //set update permission for 1 user

$route['form']   = 'app/Si_Form/index_form';
$route['form_1'] = 'app/Si_Form/form_1';
$route['form_2'] = 'app/Si_Form/form_2';
$route['form_3'] = 'app/Si_Form/form_3';
$route['form_4'] = 'app/Si_Form/form_4';
$route['form_5'] = 'app/Si_Form/form_5';
$route['form_6'] = 'app/Si_Form/form_6';
$route['form_7'] = 'app/Si_Form/form_7';
// $route['form_8'] = 'app/Si_Form/form_2';
$route['form_9']  = 'app/Si_Form/form_9';
$route['form_10'] = 'app/Si_Form/form_10';
$route['form_11'] = 'app/Si_Form/form_11';
$route['form_12'] = 'app/Si_Form/form_12';
$route['form_13'] = 'app/Si_Form/form_13';
$route['form_14'] = 'app/Si_Form/form_14';
$route['form_15'] = 'app/Si_Form/form_15';
$route['form_16'] = 'app/Si_Form/form_16';
$route['form_17'] = 'app/Si_Form/form_17';
$route['form_18'] = 'app/Si_Form/form_18';
$route['form_19'] = 'app/Si_Form/form_19';
$route['form_20'] = 'app/Si_Form/form_20';
$route['form_21'] = 'app/Si_Form/form_21';
$route['form_22'] = 'app/Si_Form/form_22';
$route['form_23'] = 'app/Si_Form/form_23';
$route['form_24'] = 'app/Si_Form/form_24';
$route['form_25'] = 'app/Si_Form/form_25';
$route['form_26'] = 'app/Si_Form/form_26';
$route['form_27'] = 'app/Si_Form/form_27';
$route['form_28'] = 'app/Si_Form/form_28';
$route['form_29'] = 'app/Si_Form/form_29';
$route['form_30'] = 'app/Si_Form/form_30';
$route['form_31'] = 'app/Si_Form/form_31';
$route['form_32'] = 'app/Si_Form/form_32';
$route['form_33'] = 'app/Si_Form/form_33';
$route['form_34'] = 'app/Si_Form/form_34';
$route['form_35'] = 'app/Si_Form/form_35';
$route['form_36'] = 'app/Si_Form/form_36';


//$route['category_api'] = 'CategoryApi/index';
//$route['category_api/add'] = 'CategoryApi/add';
//$route['category_api/edit'] = 'CategoryApi/edit';
//$route['category_api/delete'] = 'CategoryApi/delete';
//$route['category_api/push'] = 'CategoryApi/push';
//$route['category_api/unpush'] = 'CategoryApi/unpush';


# Back end

//User
$route['user/signIn']     = 'api/v1/user/signIn';
$route['user/signOut']    = 'api/v1/user/signOut';
$route['user/changePass'] = 'api/v1/user/changePass';
$route['user/genPass']    = 'api/v1/user/genPass';
$route['modal/logout']    = 'api/v1/user/logout';
$route['delete_user'] = 'app/permission/Delete_user/delete_user';
include_once 'routes_api.php';