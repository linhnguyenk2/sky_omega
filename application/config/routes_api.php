<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$route['api/v1/list/insurance_company/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_22/putData/$1';
$route['api/v1/list/insurance_company/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_22/deleteData/$1';
$route['api/v1/list/insurance_company']['get']                     = 'api/v1/list/API_L_22/getList';
$route['api/v1/list/insurance_company']['post']                    = 'api/v1/list/API_L_22/postData';
$route['api/v1/list/insurance_company/(:num)']['get']              = 'api/v1/list/API_L_22/get/$1';

$route['api/v1/list/health_insurance_card/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_23/putData/$1';
$route['api/v1/list/health_insurance_card/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_23/deleteData/$1';
$route['api/v1/list/health_insurance_card']['get']                     = 'api/v1/list/API_L_23/getList';
$route['api/v1/list/health_insurance_card']['post']                    = 'api/v1/list/API_L_23/postData';
$route['api/v1/list/health_insurance_card/(:num)']['get']              = 'api/v1/list/API_L_23/get/$1';

$route['api/v1/list/supplier/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_24/putData/$1';
$route['api/v1/list/supplier/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_24/deleteData/$1';
$route['api/v1/list/supplier']['get']                     = 'api/v1/list/API_L_24/getList';
$route['api/v1/list/supplier']['post']                    = 'api/v1/list/API_L_24/postData';
$route['api/v1/list/supplier/(:num)']['get']              = 'api/v1/list/API_L_24/get/$1';

$route['api/v1/list/district/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_25/putData/$1';
$route['api/v1/list/district/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_25/deleteData/$1';
$route['api/v1/list/district']['get']                     = 'api/v1/list/API_L_25/getList';
$route['api/v1/list/district']['post']                    = 'api/v1/list/API_L_25/postData';
$route['api/v1/list/district/(:num)']['get']              = 'api/v1/list/API_L_25/get/$1';

$route['api/v1/list/ward/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_26/putData/$1';
$route['api/v1/list/ward/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_26/deleteData/$1';
$route['api/v1/list/ward']['get']                     = 'api/v1/list/API_L_26/getList';
$route['api/v1/list/ward']['post']                    = 'api/v1/list/API_L_26/postData';
$route['api/v1/list/ward/(:num)']['get']              = 'api/v1/list/API_L_26/get/$1';

$route['api/v1/list/type_medical_record/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_27/putData/$1';
$route['api/v1/list/type_medical_record/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_27/deleteData/$1';
$route['api/v1/list/type_medical_record']['get']                     = 'api/v1/list/API_L_27/getList';
$route['api/v1/list/type_medical_record']['post']                    = 'api/v1/list/API_L_27/postData';
$route['api/v1/list/type_medical_record/(:num)']['get']              = 'api/v1/list/API_L_27/get/$1';

$route['api/v1/list/chapter_icd10/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_28/putData/$1';
$route['api/v1/list/chapter_icd10/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_28/deleteData/$1';
$route['api/v1/list/chapter_icd10']['get']                     = 'api/v1/list/API_L_28/getList';
$route['api/v1/list/chapter_icd10']['post']                    = 'api/v1/list/API_L_28/postData';
$route['api/v1/list/chapter_icd10/(:num)']['get']              = 'api/v1/list/API_L_28/get/$1';

$route['api/v1/list/group_icd10/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_29/putData/$1';
$route['api/v1/list/group_icd10/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_29/deleteData/$1';
$route['api/v1/list/group_icd10']['get']                     = 'api/v1/list/API_L_29/getList';
$route['api/v1/list/group_icd10']['post']                    = 'api/v1/list/API_L_29/postData';
$route['api/v1/list/group_icd10/(:num)']['get']              = 'api/v1/list/API_L_29/get/$1';

$route['api/v1/list/disease_icd10/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_30/putData/$1';
$route['api/v1/list/disease_icd10/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_30/deleteData/$1';
$route['api/v1/list/disease_icd10']['get']                     = 'api/v1/list/API_L_30/getList';
$route['api/v1/list/disease_icd10']['post']                    = 'api/v1/list/API_L_30/postData';
$route['api/v1/list/disease_icd10/(:num)']['get']              = 'api/v1/list/API_L_30/get/$1';

$route['api/v1/list/transport_vehicle/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_31/putData/$1';
$route['api/v1/list/transport_vehicle/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_31/deleteData/$1';
$route['api/v1/list/transport_vehicle']['get']                     = 'api/v1/list/API_L_31/getList';
$route['api/v1/list/transport_vehicle']['post']                    = 'api/v1/list/API_L_31/postData';
$route['api/v1/list/transport_vehicle/(:num)']['get']              = 'api/v1/list/API_L_31/get/$1';

$route['api/v1/list/contraindication/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_33/putData/$1';
$route['api/v1/list/contraindication/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_33/deleteData/$1';
$route['api/v1/list/contraindication']['get']                     = 'api/v1/list/API_L_33/getList';
$route['api/v1/list/contraindication']['post']                    = 'api/v1/list/API_L_33/postData';
$route['api/v1/list/contraindication/(:num)']['get']              = 'api/v1/list/API_L_33/get/$1';

$route['api/v1/list/patient_room_type/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_37/putData/$1';
$route['api/v1/list/patient_room_type/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_37/deleteData/$1';
$route['api/v1/list/patient_room_type']['get']                     = 'api/v1/list/API_L_37/getList';
$route['api/v1/list/patient_room_type']['post']                    = 'api/v1/list/API_L_37/postData';
$route['api/v1/list/patient_room_type/(:num)']['get']              = 'api/v1/list/API_L_37/get/$1';

$route['api/v1/list/patient_room/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_38/putData/$1';
$route['api/v1/list/patient_room/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_38/deleteData/$1';
$route['api/v1/list/patient_room']['get']                     = 'api/v1/list/API_L_38/getList';
$route['api/v1/list/patient_room']['post']                    = 'api/v1/list/API_L_38/postData';
$route['api/v1/list/patient_room/(:num)']['get']              = 'api/v1/list/API_L_38/get/$1';

$route['api/v1/list/patient_bed/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_39/putData/$1';
$route['api/v1/list/patient_bed/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_39/deleteData/$1';
$route['api/v1/list/patient_bed']['get']                     = 'api/v1/list/API_L_39/getList';
$route['api/v1/list/patient_bed']['post']                    = 'api/v1/list/API_L_39/postData';
$route['api/v1/list/patient_bed/(:num)']['get']              = 'api/v1/list/API_L_39/get/$1';

$route['api/v1/list/nations_foreigners/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_40/putData/$1';
$route['api/v1/list/nations_foreigners/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_40/deleteData/$1';
$route['api/v1/list/nations_foreigners']['get']                     = 'api/v1/list/API_L_40/getList';
$route['api/v1/list/nations_foreigners']['post']                    = 'api/v1/list/API_L_40/postData';
$route['api/v1/list/nations_foreigners/(:num)']['get']              = 'api/v1/list/API_L_40/get/$1';

$route['api/v1/list/appointed_in_medicine/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_42/putData/$1';
$route['api/v1/list/appointed_in_medicine/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_42/deleteData/$1';
$route['api/v1/list/appointed_in_medicine']['get']                     = 'api/v1/list/API_L_42/getList';
$route['api/v1/list/appointed_in_medicine']['post']                    = 'api/v1/list/API_L_42/postData';
$route['api/v1/list/appointed_in_medicine/(:num)']['get']              = 'api/v1/list/API_L_42/get/$1';

$route['api/v1/list/not_combine_medicine/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_43/putData/$1';
$route['api/v1/list/not_combine_medicine/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_43/deleteData/$1';
$route['api/v1/list/not_combine_medicine']['get']                     = 'api/v1/list/API_L_43/getList';
$route['api/v1/list/not_combine_medicine']['post']                    = 'api/v1/list/API_L_43/postData';
$route['api/v1/list/not_combine_medicine/(:num)']['get']              = 'api/v1/list/API_L_43/get/$1';

$route['api/v1/list/contraindication_in_medicine/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_44/putData/$1';
$route['api/v1/list/contraindication_in_medicine/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_44/deleteData/$1';
$route['api/v1/list/contraindication_in_medicine']['get']                     = 'api/v1/list/API_L_44/getList';
$route['api/v1/list/contraindication_in_medicine']['post']                    = 'api/v1/list/API_L_44/postData';
$route['api/v1/list/contraindication_in_medicine/(:num)']['get']              = 'api/v1/list/API_L_44/get/$1';

$route['api/v1/list/package_healthcare_service/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_46/putData/$1';
$route['api/v1/list/package_healthcare_service/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_46/deleteData/$1';
$route['api/v1/list/package_healthcare_service']['get']                     = 'api/v1/list/API_L_46/getList';
$route['api/v1/list/package_healthcare_service']['post']                    = 'api/v1/list/API_L_46/postData';
$route['api/v1/list/package_healthcare_service/(:num)']['get']              = 'api/v1/list/API_L_46/get/$1';


$route['api/v1/list/units/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_52/putData/$1';
$route['api/v1/list/units/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_52/deleteData/$1';
$route['api/v1/list/units']['get']                     = 'api/v1/list/API_L_52/getList';
$route['api/v1/list/units']['post']                    = 'api/v1/list/API_L_52/postData';
$route['api/v1/list/units/(:num)']['get']              = 'api/v1/list/API_L_52/get/$1';

$route['api/v1/list/nations/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_53/putData/$1';
$route['api/v1/list/nations/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_53/deleteData/$1';
$route['api/v1/list/nations']['get']                     = 'api/v1/list/API_L_53/getList';
$route['api/v1/list/nations']['post']                    = 'api/v1/list/API_L_53/postData';
$route['api/v1/list/nations/(:num)']['get']              = 'api/v1/list/API_L_53/get/$1';

$route['api/v1/list/provinces/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_54/putData/$1';
$route['api/v1/list/provinces/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_54/deleteData/$1';
$route['api/v1/list/provinces']['get']                     = 'api/v1/list/API_L_54/getList';
$route['api/v1/list/provinces']['post']                    = 'api/v1/list/API_L_54/postData';
$route['api/v1/list/provinces/(:num)']['get']              = 'api/v1/list/API_L_54/get/$1';

$route['api/v1/list/titles/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_55/putData/$1';
$route['api/v1/list/titles/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_55/deleteData/$1';
$route['api/v1/list/titles']['get']                     = 'api/v1/list/API_L_55/getList';
$route['api/v1/list/titles']['post']                    = 'api/v1/list/API_L_55/postData';
$route['api/v1/list/titles/(:num)']['get']              = 'api/v1/list/API_L_55/get/$1';

$route['api/v1/list/departments/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_56/putData/$1';
$route['api/v1/list/departments/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_56/deleteData/$1';
$route['api/v1/list/departments']['get']                     = 'api/v1/list/API_L_56/getList';
$route['api/v1/list/departments']['post']                    = 'api/v1/list/API_L_56/postData';
$route['api/v1/list/departments/(:num)']['get']              = 'api/v1/list/API_L_56/get/$1';

$route['api/v1/list/reason_discharge/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_57/putData/$1';
$route['api/v1/list/reason_discharge/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_57/deleteData/$1';
$route['api/v1/list/reason_discharge']['get']                     = 'api/v1/list/API_L_57/getList';
$route['api/v1/list/reason_discharge']['post']                    = 'api/v1/list/API_L_57/postData';
$route['api/v1/list/reason_discharge/(:num)']['get']              = 'api/v1/list/API_L_57/get/$1';

$route['api/v1/list/reason_transfer/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_58/putData/$1';
$route['api/v1/list/reason_transfer/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_58/deleteData/$1';
$route['api/v1/list/reason_transfer']['get']                     = 'api/v1/list/API_L_58/getList';
$route['api/v1/list/reason_transfer']['post']                    = 'api/v1/list/API_L_58/postData';
$route['api/v1/list/reason_transfer/(:num)']['get']              = 'api/v1/list/API_L_58/get/$1';

$route['api/v1/list/currencies/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_70/putData/$1';
$route['api/v1/list/currencies/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_70/deleteData/$1';
$route['api/v1/list/currencies']['get']                     = 'api/v1/list/API_L_70/getList';
$route['api/v1/list/currencies']['post']                    = 'api/v1/list/API_L_70/postData';
$route['api/v1/list/currencies/(:num)']['get']              = 'api/v1/list/API_L_70/get/$1';

$route['api/v1/list/folks/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_71/putData/$1';
$route['api/v1/list/folks/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_71/deleteData/$1';
$route['api/v1/list/folks']['get']                     = 'api/v1/list/API_L_71/getList';
$route['api/v1/list/folks']['post']                    = 'api/v1/list/API_L_71/postData';
$route['api/v1/list/folks/(:num)']['get']              = 'api/v1/list/API_L_71/get/$1';

$route['api/v1/list/package_healthcare_service_type/([0-9_-]{1,200})']['put']    = 'api/v1/list/API_L_73/putData/$1';
$route['api/v1/list/package_healthcare_service_type/([0-9_-]{1,200})']['delete'] = 'api/v1/list/API_L_73/deleteData/$1';
$route['api/v1/list/package_healthcare_service_type']['get']                     = 'api/v1/list/API_L_73/getList';
$route['api/v1/list/package_healthcare_service_type']['post']                    = 'api/v1/list/API_L_73/postData';
$route['api/v1/list/package_healthcare_service_type/(:num)']['get']              = 'api/v1/list/API_L_73/get/$1';

$route['api/v1/patient/patients/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_1/putData/$1';
$route['api/v1/patient/patients/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_1/deleteData/$1';
$route['api/v1/patient/patients']['get']                     = 'api/v1/patient/API_P_1/getList';
$route['api/v1/patient/patients']['post']                    = 'api/v1/patient/API_P_1/postData';
$route['api/v1/patient/patients/(:num)']['get']              = 'api/v1/patient/API_P_1/get/$1';

$route['api/v1/patient/patient_emergency_contact/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_3/putData/$1';
$route['api/v1/patient/patient_emergency_contact/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_3/deleteData/$1';
$route['api/v1/patient/patient_emergency_contact']['get']                     = 'api/v1/patient/API_P_3/getList';
$route['api/v1/patient/patient_emergency_contact']['post']                    = 'api/v1/patient/API_P_3/postData';
$route['api/v1/patient/patient_emergency_contact/(:num)']['get']              = 'api/v1/patient/API_P_3/get/$1';

$route['api/v1/patient/patient_company/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_4/putData/$1';
$route['api/v1/patient/patient_company/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_4/deleteData/$1';
$route['api/v1/patient/patient_company']['get']                     = 'api/v1/patient/API_P_4/getList';
$route['api/v1/patient/patient_company']['post']                    = 'api/v1/patient/API_P_4/postData';
$route['api/v1/patient/patient_company/(:num)']['get']              = 'api/v1/patient/API_P_4/get/$1';

$route['api/v1/patient/gynecolog_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_5/putData/$1';
$route['api/v1/patient/gynecolog_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_5/deleteData/$1';
$route['api/v1/patient/gynecolog_book']['get']                     = 'api/v1/patient/API_P_5/getList';
$route['api/v1/patient/gynecolog_book']['post']                    = 'api/v1/patient/API_P_5/postData';
$route['api/v1/patient/gynecolog_book/(:num)']['get']              = 'api/v1/patient/API_P_5/get/$1';

$route['api/v1/patient/gynecolog_form/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_6/putData/$1';
$route['api/v1/patient/gynecolog_form/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_6/deleteData/$1';
$route['api/v1/patient/gynecolog_form']['get']                     = 'api/v1/patient/API_P_6/getList';
$route['api/v1/patient/gynecolog_form']['post']                    = 'api/v1/patient/API_P_6/postData';
$route['api/v1/patient/gynecolog_form/(:num)']['get']              = 'api/v1/patient/API_P_6/get/$1';

$route['api/v1/patient/user/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_7/putData/$1';
$route['api/v1/patient/user/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_7/deleteData/$1';
$route['api/v1/patient/user']['get']                     = 'api/v1/patient/API_P_7/getList';
$route['api/v1/patient/user']['post']                    = 'api/v1/patient/API_P_7/postData';
$route['api/v1/patient/user/(:num)']['get']              = 'api/v1/patient/API_P_7/get/$1';

$route['api/v1/patient/staff/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_8/putData/$1';
$route['api/v1/patient/staff/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_8/deleteData/$1';
$route['api/v1/patient/staff']['get']                     = 'api/v1/patient/API_P_8/getList';
$route['api/v1/patient/staff']['post']                    = 'api/v1/patient/API_P_8/postData';
$route['api/v1/patient/staff/(:num)']['get']              = 'api/v1/patient/API_P_8/get/$1';

$route['api/v1/patient/medical_record/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_9/putData/$1';
$route['api/v1/patient/medical_record/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_9/deleteData/$1';
$route['api/v1/patient/medical_record']['get']                     = 'api/v1/patient/API_P_9/getList';
$route['api/v1/patient/medical_record']['post']                    = 'api/v1/patient/API_P_9/postData';
$route['api/v1/patient/medical_record/(:num)']['get']              = 'api/v1/patient/API_P_9/get/$1';

$route['api/v1/patient/analysis_blood_paper/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_10/putData/$1';
$route['api/v1/patient/analysis_blood_paper/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_10/deleteData/$1';
$route['api/v1/patient/analysis_blood_paper']['get']                     = 'api/v1/patient/API_P_10/getList';
$route['api/v1/patient/analysis_blood_paper']['post']                    = 'api/v1/patient/API_P_10/postData';
$route['api/v1/patient/analysis_blood_paper/(:num)']['get']              = 'api/v1/patient/API_P_10/get/$1';

$route['api/v1/patient/analysis_urine_paper/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_11/putData/$1';
$route['api/v1/patient/analysis_urine_paper/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_11/deleteData/$1';
$route['api/v1/patient/analysis_urine_paper']['get']                     = 'api/v1/patient/API_P_11/getList';
$route['api/v1/patient/analysis_urine_paper']['post']                    = 'api/v1/patient/API_P_11/postData';
$route['api/v1/patient/analysis_urine_paper/(:num)']['get']              = 'api/v1/patient/API_P_11/get/$1';

$route['api/v1/patient/analysis_paper/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_12/putData/$1';
$route['api/v1/patient/analysis_paper/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_12/deleteData/$1';
$route['api/v1/patient/analysis_paper']['get']                     = 'api/v1/patient/API_P_12/getList';
$route['api/v1/patient/analysis_paper']['post']                    = 'api/v1/patient/API_P_12/postData';
$route['api/v1/patient/analysis_paper/(:num)']['get']              = 'api/v1/patient/API_P_12/get/$1';

$route['api/v1/patient/medical_examination_form/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_13/putData/$1';
$route['api/v1/patient/medical_examination_form/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_13/deleteData/$1';
$route['api/v1/patient/medical_examination_form']['get']                     = 'api/v1/patient/API_P_13/getList';
$route['api/v1/patient/medical_examination_form']['post']                    = 'api/v1/patient/API_P_13/postData';
$route['api/v1/patient/medical_examination_form/(:num)']['get']              = 'api/v1/patient/API_P_13/get/$1';

$route['api/v1/patient/breast_examination_form/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_16/putData/$1';
$route['api/v1/patient/breast_examination_form/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_16/deleteData/$1';
$route['api/v1/patient/breast_examination_form']['get']                     = 'api/v1/patient/API_P_16/getList';
$route['api/v1/patient/breast_examination_form']['post']                    = 'api/v1/patient/API_P_16/postData';
$route['api/v1/patient/breast_examination_form/(:num)']['get']              = 'api/v1/patient/API_P_16/get/$1';

$route['api/v1/patient/prescription_paper/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_17/putData/$1';
$route['api/v1/patient/prescription_paper/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_17/deleteData/$1';
$route['api/v1/patient/prescription_paper']['get']                     = 'api/v1/patient/API_P_17/getList';
$route['api/v1/patient/prescription_paper']['post']                    = 'api/v1/patient/API_P_17/postData';
$route['api/v1/patient/prescription_paper/(:num)']['get']              = 'api/v1/patient/API_P_17/get/$1';

$route['api/v1/patient/tpcn_mp_vtyt_paper/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_19/putData/$1';
$route['api/v1/patient/tpcn_mp_vtyt_paper/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_19/deleteData/$1';
$route['api/v1/patient/tpcn_mp_vtyt_paper']['get']                     = 'api/v1/patient/API_P_19/getList';
$route['api/v1/patient/tpcn_mp_vtyt_paper']['post']                    = 'api/v1/patient/API_P_19/postData';
$route['api/v1/patient/tpcn_mp_vtyt_paper/(:num)']['get']              = 'api/v1/patient/API_P_19/get/$1';

$route['api/v1/patient/prenatal_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_21/putData/$1';
$route['api/v1/patient/prenatal_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_21/deleteData/$1';
$route['api/v1/patient/prenatal_book']['get']                     = 'api/v1/patient/API_P_21/getList';
$route['api/v1/patient/prenatal_book']['post']                    = 'api/v1/patient/API_P_21/postData';
$route['api/v1/patient/prenatal_book/(:num)']['get']              = 'api/v1/patient/API_P_21/get/$1';

$route['api/v1/patient/prenatal_form/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_22/putData/$1';
$route['api/v1/patient/prenatal_form/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_22/deleteData/$1';
$route['api/v1/patient/prenatal_form']['get']                     = 'api/v1/patient/API_P_22/getList';
$route['api/v1/patient/prenatal_form']['post']                    = 'api/v1/patient/API_P_22/postData';
$route['api/v1/patient/prenatal_form/(:num)']['get']              = 'api/v1/patient/API_P_22/get/$1';

$route['api/v1/patient/breast_examination_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_23/putData/$1';
$route['api/v1/patient/breast_examination_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_23/deleteData/$1';
$route['api/v1/patient/breast_examination_book']['get']                     = 'api/v1/patient/API_P_23/getList';
$route['api/v1/patient/breast_examination_book']['post']                    = 'api/v1/patient/API_P_23/postData';
$route['api/v1/patient/breast_examination_book/(:num)']['get']              = 'api/v1/patient/API_P_23/get/$1';

$route['api/v1/patient/specify_service_paper/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_24/putData/$1';
$route['api/v1/patient/specify_service_paper/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_24/deleteData/$1';
$route['api/v1/patient/specify_service_paper']['get']                     = 'api/v1/patient/API_P_24/getList';
$route['api/v1/patient/specify_service_paper']['post']                    = 'api/v1/patient/API_P_24/postData';
$route['api/v1/patient/specify_service_paper/(:num)']['get']              = 'api/v1/patient/API_P_24/get/$1';

$route['api/v1/patient/menopause_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_27/putData/$1';
$route['api/v1/patient/menopause_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_27/deleteData/$1';
$route['api/v1/patient/menopause_book']['get']                     = 'api/v1/patient/API_P_27/getList';
$route['api/v1/patient/menopause_book']['post']                    = 'api/v1/patient/API_P_27/postData';
$route['api/v1/patient/menopause_book/(:num)']['get']              = 'api/v1/patient/API_P_27/get/$1';

$route['api/v1/patient/menopause_form/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_28/putData/$1';
$route['api/v1/patient/menopause_form/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_28/deleteData/$1';
$route['api/v1/patient/menopause_form']['get']                     = 'api/v1/patient/API_P_28/getList';
$route['api/v1/patient/menopause_form']['post']                    = 'api/v1/patient/API_P_28/postData';
$route['api/v1/patient/menopause_form/(:num)']['get']              = 'api/v1/patient/API_P_28/get/$1';

$route['api/v1/patient/survival_information/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_29/putData/$1';
$route['api/v1/patient/survival_information/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_29/deleteData/$1';
$route['api/v1/patient/survival_information']['get']                     = 'api/v1/patient/API_P_29/getList';
$route['api/v1/patient/survival_information']['post']                    = 'api/v1/patient/API_P_29/postData';
$route['api/v1/patient/survival_information/(:num)']['get']              = 'api/v1/patient/API_P_29/get/$1';

$route['api/v1/patient/appointment_form/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_30/putData/$1';
$route['api/v1/patient/appointment_form/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_30/deleteData/$1';
$route['api/v1/patient/appointment_form']['get']                     = 'api/v1/patient/API_P_30/getList';
$route['api/v1/patient/appointment_form']['post']                    = 'api/v1/patient/API_P_30/postData';
$route['api/v1/patient/appointment_form/(:num)']['get']              = 'api/v1/patient/API_P_30/get/$1';

$route['api/v1/patient/registration_form/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_31/putData/$1';
$route['api/v1/patient/registration_form/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_31/deleteData/$1';
$route['api/v1/patient/registration_form']['get']                     = 'api/v1/patient/API_P_31/getList';
$route['api/v1/patient/registration_form']['post']                    = 'api/v1/patient/API_P_31/postData';
$route['api/v1/patient/registration_form/(:num)']['get']              = 'api/v1/patient/API_P_31/get/$1';

$route['api/v1/patient/disease_icd10_in_gynecolog_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_32/putData/$1';
$route['api/v1/patient/disease_icd10_in_gynecolog_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_32/deleteData/$1';
$route['api/v1/patient/disease_icd10_in_gynecolog_book']['get']                     = 'api/v1/patient/API_P_32/getList';
$route['api/v1/patient/disease_icd10_in_gynecolog_book']['post']                    = 'api/v1/patient/API_P_32/postData';
$route['api/v1/patient/disease_icd10_in_gynecolog_book/(:num)']['get']              = 'api/v1/patient/API_P_32/get/$1';

$route['api/v1/patient/family_planing_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_33/putData/$1';
$route['api/v1/patient/family_planing_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_33/deleteData/$1';
$route['api/v1/patient/family_planing_book']['get']                     = 'api/v1/patient/API_P_33/getList';
$route['api/v1/patient/family_planing_book']['post']                    = 'api/v1/patient/API_P_33/postData';
$route['api/v1/patient/family_planing_book/(:num)']['get']              = 'api/v1/patient/API_P_33/get/$1';

$route['api/v1/patient/family_plan_birth_control_form/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_34/putData/$1';
$route['api/v1/patient/family_plan_birth_control_form/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_34/deleteData/$1';
$route['api/v1/patient/family_plan_birth_control_form']['get']                     = 'api/v1/patient/API_P_34/getList';
$route['api/v1/patient/family_plan_birth_control_form']['post']                    = 'api/v1/patient/API_P_34/postData';
$route['api/v1/patient/family_plan_birth_control_form/(:num)']['get']              = 'api/v1/patient/API_P_34/get/$1';

$route['api/v1/patient/artificial_examination_form/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_35/putData/$1';
$route['api/v1/patient/artificial_examination_form/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_35/deleteData/$1';
$route['api/v1/patient/artificial_examination_form']['get']                     = 'api/v1/patient/API_P_35/getList';
$route['api/v1/patient/artificial_examination_form']['post']                    = 'api/v1/patient/API_P_35/postData';
$route['api/v1/patient/artificial_examination_form/(:num)']['get']              = 'api/v1/patient/API_P_35/get/$1';

$route['api/v1/patient/health_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_36/putData/$1';
$route['api/v1/patient/health_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_36/deleteData/$1';
$route['api/v1/patient/health_book']['get']                     = 'api/v1/patient/API_P_36/getList';
$route['api/v1/patient/health_book']['post']                    = 'api/v1/patient/API_P_36/postData';
$route['api/v1/patient/health_book/(:num)']['get']              = 'api/v1/patient/API_P_36/get/$1';

$route['api/v1/patient/pregnancy_changes_in_health_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_37/putData/$1';
$route['api/v1/patient/pregnancy_changes_in_health_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_37/deleteData/$1';
$route['api/v1/patient/pregnancy_changes_in_health_book']['get']                     = 'api/v1/patient/API_P_37/getList';
$route['api/v1/patient/pregnancy_changes_in_health_book']['post']                    = 'api/v1/patient/API_P_37/postData';
$route['api/v1/patient/pregnancy_changes_in_health_book/(:num)']['get']              = 'api/v1/patient/API_P_37/get/$1';

$route['api/v1/patient/analysis_in_health_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_38/putData/$1';
$route['api/v1/patient/analysis_in_health_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_38/deleteData/$1';
$route['api/v1/patient/analysis_in_health_book']['get']                     = 'api/v1/patient/API_P_38/getList';
$route['api/v1/patient/analysis_in_health_book']['post']                    = 'api/v1/patient/API_P_38/postData';
$route['api/v1/patient/analysis_in_health_book/(:num)']['get']              = 'api/v1/patient/API_P_38/get/$1';

$route['api/v1/patient/vaccination_in_health_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_39/putData/$1';
$route['api/v1/patient/vaccination_in_health_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_39/deleteData/$1';
$route['api/v1/patient/vaccination_in_health_book']['get']                     = 'api/v1/patient/API_P_39/getList';
$route['api/v1/patient/vaccination_in_health_book']['post']                    = 'api/v1/patient/API_P_39/postData';
$route['api/v1/patient/vaccination_in_health_book/(:num)']['get']              = 'api/v1/patient/API_P_39/get/$1';

$route['api/v1/patient/medical_examination_form_in_health_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_40/putData/$1';
$route['api/v1/patient/medical_examination_form_in_health_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_40/deleteData/$1';
$route['api/v1/patient/medical_examination_form_in_health_book']['get']                     = 'api/v1/patient/API_P_40/getList';
$route['api/v1/patient/medical_examination_form_in_health_book']['post']                    = 'api/v1/patient/API_P_40/postData';
$route['api/v1/patient/medical_examination_form_in_health_book/(:num)']['get']              = 'api/v1/patient/API_P_40/get/$1';

$route['api/v1/patient/track_height_weight_in_health_book/([0-9_-]{1,200})']['put']    = 'api/v1/patient/API_P_41/putData/$1';
$route['api/v1/patient/track_height_weight_in_health_book/([0-9_-]{1,200})']['delete'] = 'api/v1/patient/API_P_41/deleteData/$1';
$route['api/v1/patient/track_height_weight_in_health_book']['get']                     = 'api/v1/patient/API_P_41/getList';
$route['api/v1/patient/track_height_weight_in_health_book']['post']                    = 'api/v1/patient/API_P_41/postData';
$route['api/v1/patient/track_height_weight_in_health_book/(:num)']['get']              = 'api/v1/patient/API_P_41/get/$1';

$route['api/v1/permission/menu_permission/([0-9_-]{1,200})']['put']    = 'api/v1/permission/API_PR_1/putData/$1';
$route['api/v1/permission/menu_permission/([0-9_-]{1,200})']['delete'] = 'api/v1/permission/API_PR_1/deleteData/$1';
$route['api/v1/permission/menu_permission']['get']                     = 'api/v1/permission/API_PR_1/getList';
$route['api/v1/permission/menu_permission']['post']                    = 'api/v1/permission/API_PR_1/postData';
$route['api/v1/permission/menu_permission/(:num)']['get']              = 'api/v1/permission/API_PR_1/get/$1';

$route['api/v1/permission/content_permission/([0-9_-]{1,200})']['put']    = 'api/v1/permission/API_PR_2/putData/$1';
$route['api/v1/permission/content_permission/([0-9_-]{1,200})']['delete'] = 'api/v1/permission/API_PR_2/deleteData/$1';
$route['api/v1/permission/content_permission']['get']                     = 'api/v1/permission/API_PR_2/getList';
$route['api/v1/permission/content_permission']['post']                    = 'api/v1/permission/API_PR_2/postData';
$route['api/v1/permission/content_permission/(:num)']['get']              = 'api/v1/permission/API_PR_2/get/$1';

$route['api/v1/permission/content_permission_addon/([0-9_-]{1,200})']['put']    = 'api/v1/permission/API_PR_3/putData/$1';
$route['api/v1/permission/content_permission_addon/([0-9_-]{1,200})']['delete'] = 'api/v1/permission/API_PR_3/deleteData/$1';
$route['api/v1/permission/content_permission_addon']['get']                     = 'api/v1/permission/API_PR_3/getList';
$route['api/v1/permission/content_permission_addon']['post']                    = 'api/v1/permission/API_PR_3/postData';
$route['api/v1/permission/content_permission_addon/(:num)']['get']              = 'api/v1/permission/API_PR_3/get/$1';

$route['api/v1/permission/api_permission/([0-9_-]{1,200})']['put']    = 'api/v1/permission/API_PR_4/putData/$1';
$route['api/v1/permission/api_permission/([0-9_-]{1,200})']['delete'] = 'api/v1/permission/API_PR_4/deleteData/$1';
$route['api/v1/permission/api_permission']['get']                     = 'api/v1/permission/API_PR_4/getList';
$route['api/v1/permission/api_permission']['post']                    = 'api/v1/permission/API_PR_4/postData';
$route['api/v1/permission/api_permission/(:num)']['get']              = 'api/v1/permission/API_PR_4/get/$1';

$route['api/v1/permission/menu/([0-9_-]{1,200})']['put']    = 'api/v1/permission/API_PR_5/putData/$1';
$route['api/v1/permission/menu/([0-9_-]{1,200})']['delete'] = 'api/v1/permission/API_PR_5/deleteData/$1';
$route['api/v1/permission/menu']['get']                     = 'api/v1/permission/API_PR_5/getList';
$route['api/v1/permission/menu']['post']                    = 'api/v1/permission/API_PR_5/postData';
$route['api/v1/permission/menu/(:num)']['get']              = 'api/v1/permission/API_PR_5/get/$1';

$route['api/v1/login/login']['post']         = 'api/v1/login/API_L_1/login';
$route['api/v1/login/isSessionValid']['get'] = 'api/v1/login/API_L_1/isSessionValid';

$route['api/v1/service/service_group/([0-9_-]{1,200})']['put']    = 'api/v1/service/API_S_1/putData/$1';
$route['api/v1/service/service_group/([0-9_-]{1,200})']['delete'] = 'api/v1/service/API_S_1/deleteData/$1';
$route['api/v1/service/service_group']['get']                     = 'api/v1/service/API_S_1/getList';
$route['api/v1/service/service_group']['post']                    = 'api/v1/service/API_S_1/postData';
$route['api/v1/service/service_group/(:num)']['get']              = 'api/v1/service/API_S_1/get/$1';

$route['api/v1/service/service_type/([0-9_-]{1,200})']['put']    = 'api/v1/service/API_S_2/putData/$1';
$route['api/v1/service/service_type/([0-9_-]{1,200})']['delete'] = 'api/v1/service/API_S_2/deleteData/$1';
$route['api/v1/service/service_type']['get']                     = 'api/v1/service/API_S_2/getList';
$route['api/v1/service/service_type']['post']                    = 'api/v1/service/API_S_2/postData';
$route['api/v1/service/service_type/(:num)']['get']              = 'api/v1/service/API_S_2/get/$1';

$route['api/v1/service/service_location/([0-9_-]{1,200})']['put']    = 'api/v1/service/API_S_3/putData/$1';
$route['api/v1/service/service_location/([0-9_-]{1,200})']['delete'] = 'api/v1/service/API_S_3/deleteData/$1';
$route['api/v1/service/service_location']['get']                     = 'api/v1/service/API_S_3/getList';
$route['api/v1/service/service_location']['post']                    = 'api/v1/service/API_S_3/postData';
$route['api/v1/service/service_location/(:num)']['get']              = 'api/v1/service/API_S_3/get/$1';

$route['api/v1/service/service/([0-9_-]{1,200})']['put']    = 'api/v1/service/API_S_4/putData/$1';
$route['api/v1/service/service/([0-9_-]{1,200})']['delete'] = 'api/v1/service/API_S_4/deleteData/$1';
$route['api/v1/service/service']['get']                     = 'api/v1/service/API_S_4/getList';
//  $route['api/v1/service/service']['post']                    = 'api/v1/service/API_S_4/postData';
//route['api/v1/service/service/(:num)']['get']              = 'api/v1/service/API_S_4/get/$1';

$route['api/v1/service/product_use_location_in_service/([0-9_-]{1,200})']['put']    = 'api/v1/service/API_S_5/putData/$1';
$route['api/v1/service/product_use_location_in_service/([0-9_-]{1,200})']['delete'] = 'api/v1/service/API_S_5/deleteData/$1';
$route['api/v1/service/product_use_location_in_service']['get']                     = 'api/v1/service/API_S_5/getList';
$route['api/v1/service/product_use_location_in_service']['post']                    = 'api/v1/service/API_S_5/postData';
$route['api/v1/service/product_use_location_in_service/(:num)']['get']              = 'api/v1/service/API_S_5/get/$1';

$route['api/v1/service/location_in_service/([0-9_-]{1,200})']['put']    = 'api/v1/service/API_S_6/putData/$1';
$route['api/v1/service/location_in_service/([0-9_-]{1,200})']['delete'] = 'api/v1/service/API_S_6/deleteData/$1';
$route['api/v1/service/location_in_service']['get']                     = 'api/v1/service/API_S_6/getList';
$route['api/v1/service/location_in_service']['post']                    = 'api/v1/service/API_S_6/postData';
$route['api/v1/service/location_in_service/(:num)']['get']              = 'api/v1/service/API_S_6/get/$1';

$route['api/v1/product/product/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_1/putData/$1';
$route['api/v1/product/product/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_1/deleteData/$1';
$route['api/v1/product/product']['get']                     = 'api/v1/product/API_PT_1/getList';
//$route['api/v1/product/product']['post']                    = 'api/v1/product/API_PT_1/postData';
$route['api/v1/product/product/(:num)']['get']              = 'api/v1/product/API_PT_1/get/$1';

$route['api/v1/product/product_group/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_2/putData/$1';
$route['api/v1/product/product_group/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_2/deleteData/$1';
$route['api/v1/product/product_group']['get']                     = 'api/v1/product/API_PT_2/getList';
$route['api/v1/product/product_group']['post']                    = 'api/v1/product/API_PT_2/postData';
$route['api/v1/product/product_group/(:num)']['get']              = 'api/v1/product/API_PT_2/get/$1';

$route['api/v1/product/unit/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_3/putData/$1';
$route['api/v1/product/unit/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_3/deleteData/$1';
$route['api/v1/product/unit']['get']                     = 'api/v1/product/API_PT_3/getList';
$route['api/v1/product/unit']['post']                    = 'api/v1/product/API_PT_3/postData';
$route['api/v1/product/unit/(:num)']['get']              = 'api/v1/product/API_PT_3/get/$1';

$route['api/v1/product/product_unit_exchange/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_4/putData/$1';
$route['api/v1/product/product_unit_exchange/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_4/deleteData/$1';
$route['api/v1/product/product_unit_exchange']['get']                     = 'api/v1/product/API_PT_4/getList';
$route['api/v1/product/product_unit_exchange']['post']                    = 'api/v1/product/API_PT_4/postData';
$route['api/v1/product/product_unit_exchange/(:num)']['get']              = 'api/v1/product/API_PT_4/get/$1';

$route['api/v1/product/raw_material_product/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_5/putData/$1';
$route['api/v1/product/raw_material_product/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_5/deleteData/$1';
$route['api/v1/product/raw_material_product']['get']                     = 'api/v1/product/API_PT_5/getList';
$route['api/v1/product/raw_material_product']['post']                    = 'api/v1/product/API_PT_5/postData';
$route['api/v1/product/raw_material_product/(:num)']['get']              = 'api/v1/product/API_PT_5/get/$1';

$route['api/v1/product/functional_food_product/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_6/putData/$1';
$route['api/v1/product/functional_food_product/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_6/deleteData/$1';
$route['api/v1/product/functional_food_product']['get']                     = 'api/v1/product/API_PT_6/getList';
$route['api/v1/product/functional_food_product']['post']                    = 'api/v1/product/API_PT_6/postData';
$route['api/v1/product/functional_food_product/(:num)']['get']              = 'api/v1/product/API_PT_6/get/$1';

$route['api/v1/product/cosmetic_product/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_7/putData/$1';
$route['api/v1/product/cosmetic_product/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_7/deleteData/$1';
$route['api/v1/product/cosmetic_product']['get']                     = 'api/v1/product/API_PT_7/getList';
$route['api/v1/product/cosmetic_product']['post']                    = 'api/v1/product/API_PT_7/postData';
$route['api/v1/product/cosmetic_product/(:num)']['get']              = 'api/v1/product/API_PT_7/get/$1';

$route['api/v1/product/chemical_product/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_8/putData/$1';
$route['api/v1/product/chemical_product/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_8/deleteData/$1';
$route['api/v1/product/chemical_product']['get']                     = 'api/v1/product/API_PT_8/getList';
$route['api/v1/product/chemical_product']['post']                    = 'api/v1/product/API_PT_8/postData';
$route['api/v1/product/chemical_product/(:num)']['get']              = 'api/v1/product/API_PT_8/get/$1';

$route['api/v1/product/food_product/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_9/putData/$1';
$route['api/v1/product/food_product/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_9/deleteData/$1';
$route['api/v1/product/food_product']['get']                     = 'api/v1/product/API_PT_9/getList';
$route['api/v1/product/food_product']['post']                    = 'api/v1/product/API_PT_9/postData';
$route['api/v1/product/food_product/(:num)']['get']              = 'api/v1/product/API_PT_9/get/$1';

$route['api/v1/product/medical_supply/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_10/putData/$1';
$route['api/v1/product/medical_supply/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_10/deleteData/$1';
$route['api/v1/product/medical_supply']['get']                     = 'api/v1/product/API_PT_10/getList';
$route['api/v1/product/medical_supply']['post']                    = 'api/v1/product/API_PT_10/postData';
$route['api/v1/product/medical_supply/(:num)']['get']              = 'api/v1/product/API_PT_10/get/$1';

$route['api/v1/product/medicine_product/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_11/putData/$1';
$route['api/v1/product/medicine_product/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_11/deleteData/$1';
$route['api/v1/product/medicine_product']['get']                     = 'api/v1/product/API_PT_11/getList';
$route['api/v1/product/medicine_product']['post']                    = 'api/v1/product/API_PT_11/postData';
$route['api/v1/product/medicine_product/(:num)']['get']              = 'api/v1/product/API_PT_11/get/$1';

$route['api/v1/product/product_in_tpcn_mp_vtyt_paper/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_12/putData/$1';
$route['api/v1/product/product_in_tpcn_mp_vtyt_paper/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_12/deleteData/$1';
$route['api/v1/product/product_in_tpcn_mp_vtyt_paper']['get']                     = 'api/v1/product/API_PT_12/getList';
$route['api/v1/product/product_in_tpcn_mp_vtyt_paper']['post']                    = 'api/v1/product/API_PT_12/postData';
$route['api/v1/product/product_in_tpcn_mp_vtyt_paper/(:num)']['get']              = 'api/v1/product/API_PT_12/get/$1';

$route['api/v1/product/medicine_in_prescription_paper/([0-9_-]{1,200})']['put']    = 'api/v1/product/API_PT_13/putData/$1';
$route['api/v1/product/medicine_in_prescription_paper/([0-9_-]{1,200})']['delete'] = 'api/v1/product/API_PT_13/deleteData/$1';
$route['api/v1/product/medicine_in_prescription_paper']['get']                     = 'api/v1/product/API_PT_13/getList';
$route['api/v1/product/medicine_in_prescription_paper']['post']                    = 'api/v1/product/API_PT_13/postData';
$route['api/v1/product/medicine_in_prescription_paper/(:num)']['get']              = 'api/v1/product/API_PT_13/get/$1';

$route['api/v1/warehouse/warehouse/([0-9_-]{1,200})']['put']    = 'api/v1/warehouse/API_W_1/putData/$1';
$route['api/v1/warehouse/warehouse/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_1/deleteData/$1';
$route['api/v1/warehouse/warehouse']['get']                     = 'api/v1/warehouse/API_W_1/getList';
$route['api/v1/warehouse/warehouse']['post']                    = 'api/v1/warehouse/API_W_1/postData';
$route['api/v1/warehouse/warehouse/(:num)']['get']              = 'api/v1/warehouse/API_W_1/get/$1';

$route['api/v1/warehouse/warehouse_import_form/([0-9_-]{1,200})']['put']    = 'api/v1/warehouse/API_W_2/putData/$1';
$route['api/v1/warehouse/warehouse_import_form/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_2/deleteData/$1';
$route['api/v1/warehouse/warehouse_import_form']['get']                     = 'api/v1/warehouse/API_W_2/getList';
$route['api/v1/warehouse/warehouse_import_form']['post']                    = 'api/v1/warehouse/API_W_2/postData';
$route['api/v1/warehouse/warehouse_import_form/(:num)']['get']              = 'api/v1/warehouse/API_W_2/get/$1';

$route['api/v1/warehouse/warehouse_request_form/([0-9_-]{1,200})']['put']    = 'api/v1/warehouse/API_W_3/putData/$1';
$route['api/v1/warehouse/warehouse_request_form/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_3/deleteData/$1';
$route['api/v1/warehouse/warehouse_request_form']['get']                     = 'api/v1/warehouse/API_W_3/getList';
$route['api/v1/warehouse/warehouse_request_form']['post']                    = 'api/v1/warehouse/API_W_3/postData';
$route['api/v1/warehouse/warehouse_request_form/(:num)']['get']              = 'api/v1/warehouse/API_W_3/get/$1';

$route['api/v1/warehouse/warehouse_export_form/([0-9_-]{1 200})']['put']    = 'api/v1/warehouse/API_W_4/putData/$1';
$route['api/v1/warehouse/warehouse_export_form/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_4/deleteData/$1';
$route['api/v1/warehouse/warehouse_export_form']['get']                     = 'api/v1/warehouse/API_W_4/getList';
$route['api/v1/warehouse/warehouse_export_form']['post']                    = 'api/v1/warehouse/API_W_4/postData';
$route['api/v1/warehouse/warehouse_export_form/(:num)']['get']              = 'api/v1/warehouse/API_W_4/get/$1';

$route['api/v1/warehouse/warehouse_sale_form/([0-9_-]{1,200})']['put']    = 'api/v1/warehouse/API_W_5/putData/$1';
$route['api/v1/warehouse/warehouse_sale_form/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_5/deleteData/$1';
$route['api/v1/warehouse/warehouse_sale_form']['get']                     = 'api/v1/warehouse/API_W_5/getList';
$route['api/v1/warehouse/warehouse_sale_form']['post']                    = 'api/v1/warehouse/API_W_5/postData';
$route['api/v1/warehouse/warehouse_sale_form/(:num)']['get']              = 'api/v1/warehouse/API_W_5/get/$5';

$route['api/v1/warehouse/warehouse_use_form/([0-9_-]{1,200})']['put']    = 'api/v1/warehouse/API_W_6/putData/$1';
$route['api/v1/warehouse/warehouse_use_form/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_6/deleteData/$1';
$route['api/v1/warehouse/warehouse_use_form']['get']                     = 'api/v1/warehouse/API_W_6/getList';
$route['api/v1/warehouse/warehouse_use_form']['post']                    = 'api/v1/warehouse/API_W_6/postData';
$route['api/v1/warehouse/warehouse_use_form/(:num)']['get']              = 'api/v1/warehouse/API_W_6/get/$1';

$route['api/v1/warehouse/warehouse_product_in_import_form/([0-9_-]{1,200})']['put']    = 'api/v1/warehouse/API_W_7/putData/$1';
$route['api/v1/warehouse/warehouse_product_in_import_form/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_7/deleteData/$1';
$route['api/v1/warehouse/warehouse_product_in_import_form']['get']                     = 'api/v1/warehouse/API_W_7/getList';
$route['api/v1/warehouse/warehouse_product_in_import_form']['post']                    = 'api/v1/warehouse/API_W_7/postData';
$route['api/v1/warehouse/warehouse_product_in_import_form/(:num)']['get']              = 'api/v1/warehouse/API_W_7/get/$1';

$route['api/v1/warehouse/warehouse_product_in_sale_form/([0-9_-]{1,200})']['put']    = 'api/v1/warehouse/API_W_8/putData/$1';
$route['api/v1/warehouse/warehouse_product_in_sale_form/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_8/deleteData/$1';
$route['api/v1/warehouse/warehouse_product_in_sale_form']['get']                     = 'api/v1/warehouse/API_W_8/getList';
$route['api/v1/warehouse/warehouse_product_in_sale_form']['post']                    = 'api/v1/warehouse/API_W_8/postData';
$route['api/v1/warehouse/warehouse_product_in_sale_form/(:num)']['get']              = 'api/v1/warehouse/API_W_8/get/$1';

$route['api/v1/warehouse/warehouse_product_in_request_form/([0-9_-]{1,200})']['put']    = 'api/v1/warehouse/API_W_9/putData/$1';
$route['api/v1/warehouse/warehouse_product_in_request_form/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_9/deleteData/$1';
$route['api/v1/warehouse/warehouse_product_in_request_form']['get']                     = 'api/v1/warehouse/API_W_9/getList';
$route['api/v1/warehouse/warehouse_product_in_request_form']['post']                    = 'api/v1/warehouse/API_W_9/postData';
$route['api/v1/warehouse/warehouse_product_in_request_form/(:num)']['get']              = 'api/v1/warehouse/API_W_9/get/$1';

$route['api/v1/warehouse/product_in_package_in_check_form/([0-9_-]{1,200})']['put']    = 'api/v1/warehouse/API_W_10/putData/$1';
$route['api/v1/warehouse/product_in_package_in_check_form/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_10/deleteData/$1';
$route['api/v1/warehouse/product_in_package_in_check_form']['get']                     = 'api/v1/warehouse/API_W_10/getList';
$route['api/v1/warehouse/product_in_package_in_check_form']['post']                    = 'api/v1/warehouse/API_W_10/postData';
$route['api/v1/warehouse/product_in_package_in_check_form/(:num)']['get']              = 'api/v1/warehouse/API_W_10/get/$1';

$route['api/v1/warehouse/product_in_package_in_use_form/([0-9_-]{1,200})']['put']    = 'api/v1/warehouse/API_W_11/putData/$1';
$route['api/v1/warehouse/product_in_package_in_use_form/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_11/deleteData/$1';
$route['api/v1/warehouse/product_in_package_in_use_form']['get']                     = 'api/v1/warehouse/API_W_11/getList';
$route['api/v1/warehouse/product_in_package_in_use_form']['post']                    = 'api/v1/warehouse/API_W_11/postData';
$route['api/v1/warehouse/product_in_package_in_use_form/(:num)']['get']              = 'api/v1/warehouse/API_W_11/get/$1';

$route['api/v1/warehouse/warehouse_package/([0-9_-]{1,200})']['put']    = 'api/v1/warehouse/API_W_12/putData/$1';
$route['api/v1/warehouse/warehouse_package/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_12/deleteData/$1';
$route['api/v1/warehouse/warehouse_package']['get']                     = 'api/v1/warehouse/API_W_12/getList';
$route['api/v1/warehouse/warehouse_package']['post']                    = 'api/v1/warehouse/API_W_12/postData';
$route['api/v1/warehouse/warehouse_package/(:num)']['get']              = 'api/v1/warehouse/API_W_12/get/$1';

$route['api/v1/warehouse/warehouse_product_in_package/([0-9_-]1,200})']['put']     = 'api/v1/warehouse/API_W_13/putData/$1';
$route['api/v1/warehouse/warehouse_product_in_package/([0-9_-]{1,200})']['delete'] = 'api/v1/warehouse/API_W_13/deleteData/$1';
$route['api/v1/warehouse/warehouse_product_in_package']['get']                     = 'api/v1/warehouse/API_W_13/getList';
$route['api/v1/warehouse/warehouse_product_in_package']['post']                    = 'api/v1/warehouse/API_W_13/postData';
$route['api/v1/warehouse/warehouse_product_in_package/(:num)']['get']              = 'api/v1/warehouse/API_W_13/get/$1';

$route['api/v1/warehouse/warehouse_product_in_package_by_date/([0-9_-]{1,200})']['put']      = 'api/v1/warehouse/API_W_14/putData/$1';
$route['api/v1/warehouse/warehouse_product_in_package_by_date/([0-9_-]{12,1200})']['delete'] = 'api/v1/warehouse/API_W_14/deleteData/$1';
$route['api/v1/warehouse/warehouse_product_in_package_by_date']['get']                       = 'api/v1/warehouse/API_W_14/getList';
$route['api/v1/warehouse/warehouse_product_in_package_by_date']['post']                      = 'api/v1/warehouse/API_W_14/postData';
$route['api/v1/warehouse/warehouse_product_in_package_by_date/(:num)']['get']                = 'api/v1/warehouse/API_W_14/get/$1';

$route['api/v1/warehouse/warehouse_product_in_package_history/([0-9_-]{1,200})']['put']      = 'api/v1/warehouse/API_W_15/putData/$1';
$route['api/v1/warehouse/warehouse_product_in_package_history/([0-9_-]{12,1200})']['delete'] = 'api/v1/warehouse/API_W_15/deleteData/$1';
$route['api/v1/warehouse/warehouse_product_in_package_history']['get']                       = 'api/v1/warehouse/API_W_15/getList';
$route['api/v1/warehouse/warehouse_product_in_package_history']['post']                      = 'api/v1/warehouse/API_W_15/postData';
$route['api/v1/warehouse/warehouse_product_in_package_history/(:num)']['get']                = 'api/v1/warehouse/API_W_15/get/$1';

$route['api/v1/subclinical/subclinical_paper/([0-9_-]{1,200})']['put']      = 'api/v1/subclinical/API_SCL_1/putData/$1';
$route['api/v1/subclinical/subclinical_paper/([0-9_-]{12,1200})']['delete'] = 'api/v1/subclinical/API_SCL_1/deleteData/$1';
$route['api/v1/subclinical/subclinical_paper']['get']                       = 'api/v1/subclinical/API_SCL_1/getList';
$route['api/v1/subclinical/subclinical_paper']['post']                      = 'api/v1/subclinical/API_SCL_1/postData';
$route['api/v1/subclinical/subclinical_paper/(:num)']['get']                = 'api/v1/subclinical/API_SCL_1/get/$1';

$route['api/v1/subclinical/subclinical_endoscopic_paper/([0-9_-]{1,200})']['put']      = 'api/v1/subclinical/API_SCL_2/putData/$1';
$route['api/v1/subclinical/subclinical_endoscopic_paper/([0-9_-]{12,1200})']['delete'] = 'api/v1/subclinical/API_SCL_2/deleteData/$1';
$route['api/v1/subclinical/subclinical_endoscopic_paper']['get']                       = 'api/v1/subclinical/API_SCL_2/getList';
$route['api/v1/subclinical/subclinical_endoscopic_paper']['post']                      = 'api/v1/subclinical/API_SCL_2/postData';
$route['api/v1/subclinical/subclinical_endoscopic_paper/(:num)']['get']                = 'api/v1/subclinical/API_SCL_2/get/$1';

$route['api/v1/subclinical/subclinical_xray_paper/([0-9_-]{1,200})']['put']      = 'api/v1/subclinical/API_SCL_3/putData/$1';
$route['api/v1/subclinical/subclinical_xray_paper/([0-9_-]{12,1200})']['delete'] = 'api/v1/subclinical/API_SCL_3/deleteData/$1';
$route['api/v1/subclinical/subclinical_xray_paper']['get']                       = 'api/v1/subclinical/API_SCL_3/getList';
$route['api/v1/subclinical/subclinical_xray_paper']['post']                      = 'api/v1/subclinical/API_SCL_3/postData';
$route['api/v1/subclinical/subclinical_xray_paper/(:num)']['get']                = 'api/v1/subclinical/API_SCL_3/get/$1';

$route['api/v1/subclinical/subclinical_xray_breast_paper/([0-9_-]{1,200})']['put']      = 'api/v1/subclinical/API_SCL_4/putData/$1';
$route['api/v1/subclinical/subclinical_xray_breast_paper/([0-9_-]{12,1200})']['delete'] = 'api/v1/subclinical/API_SCL_4/deleteData/$1';
$route['api/v1/subclinical/subclinical_xray_breast_paper']['get']                       = 'api/v1/subclinical/API_SCL_4/getList';
$route['api/v1/subclinical/subclinical_xray_breast_paper']['post']                      = 'api/v1/subclinical/API_SCL_4/postData';
$route['api/v1/subclinical/subclinical_xray_breast_paper/(:num)']['get']                = 'api/v1/subclinical/API_SCL_4/get/$1';

$route['api/v1/subclinical/subclinical_ultra_sound/([0-9_-]{1,200})']['put']      = 'api/v1/subclinical/API_SCL_5/putData/$1';
$route['api/v1/subclinical/subclinical_ultra_sound/([0-9_-]{12,1200})']['delete'] = 'api/v1/subclinical/API_SCL_5/deleteData/$1';
$route['api/v1/subclinical/subclinical_ultra_sound']['get']                       = 'api/v1/subclinical/API_SCL_5/getList';
$route['api/v1/subclinical/subclinical_ultra_sound']['post']                      = 'api/v1/subclinical/API_SCL_5/postData';
$route['api/v1/subclinical/subclinical_ultra_sound/(:num)']['get']                = 'api/v1/subclinical/API_SCL_5/get/$1';

$route['api/v1/subclinical/subclinical_ultra_sound_3d/([0-9_-]{1,200})']['put']      = 'api/v1/subclinical/API_SCL_6/putData/$1';
$route['api/v1/subclinical/subclinical_ultra_sound_3d/([0-9_-]{12,1200})']['delete'] = 'api/v1/subclinical/API_SCL_6/deleteData/$1';
$route['api/v1/subclinical/subclinical_ultra_sound_3d']['get']                       = 'api/v1/subclinical/API_SCL_6/getList';
$route['api/v1/subclinical/subclinical_ultra_sound_3d']['post']                      = 'api/v1/subclinical/API_SCL_6/postData';
$route['api/v1/subclinical/subclinical_ultra_sound_3d/(:num)']['get']                = 'api/v1/subclinical/API_SCL_6/get/$1';

$route['api/v1/subclinical/subclinical_template/([0-9_-]{1,200})']['put']      = 'api/v1/subclinical/API_SCL_7/putData/$1';
$route['api/v1/subclinical/subclinical_template/([0-9_-]{12,1200})']['delete'] = 'api/v1/subclinical/API_SCL_7/deleteData/$1';
$route['api/v1/subclinical/subclinical_template']['get']                       = 'api/v1/subclinical/API_SCL_7/getList';
$route['api/v1/subclinical/subclinical_template']['post']                      = 'api/v1/subclinical/API_SCL_7/postData';
$route['api/v1/subclinical/subclinical_template/(:num)']['get']                = 'api/v1/subclinical/API_SCL_7/get/$1';

$route['api/v1/subclinical/sih_subclinical_ctc/([0-9_-]{1,200})']['put']      = 'api/v1/subclinical/API_SCL_8/putData/$1';
$route['api/v1/subclinical/sih_subclinical_ctc/([0-9_-]{12,1200})']['delete'] = 'api/v1/subclinical/API_SCL_8/deleteData/$1';
$route['api/v1/subclinical/sih_subclinical_ctc']['get']                       = 'api/v1/subclinical/API_SCL_8/getList';
$route['api/v1/subclinical/sih_subclinical_ctc']['post']                      = 'api/v1/subclinical/API_SCL_8/postData';
$route['api/v1/subclinical/sih_subclinical_ctc/(:num)']['get']                = 'api/v1/subclinical/API_SCL_8/get/$1';

$route['api/v1/subclinical/sih_subclinical_endocrine_cells_through_vagina/([0-9_-]{1,200})']['put']      = 'api/v1/subclinical/API_SCL_9/putData/$1';
$route['api/v1/subclinical/sih_subclinical_endocrine_cells_through_vagina/([0-9_-]{12,1200})']['delete'] = 'api/v1/subclinical/API_SCL_9/deleteData/$1';
$route['api/v1/subclinical/sih_subclinical_endocrine_cells_through_vagina']['get']                       = 'api/v1/subclinical/API_SCL_9/getList';
$route['api/v1/subclinical/sih_subclinical_endocrine_cells_through_vagina']['post']                      = 'api/v1/subclinical/API_SCL_9/postData';
$route['api/v1/subclinical/sih_subclinical_endocrine_cells_through_vagina/(:num)']['get']                = 'api/v1/subclinical/API_SCL_9/get/$1';

$route['api/v1/subclinical/sih_subclinical_amniocentesis_cell_through_needle/([0-9_-]{1,200})']['put']      = 'api/v1/subclinical/API_SCL_10/putData/$1';
$route['api/v1/subclinical/sih_subclinical_amniocentesis_cell_through_needle/([0-9_-]{12,1200})']['delete'] = 'api/v1/subclinical/API_SCL_10/deleteData/$1';
$route['api/v1/subclinical/sih_subclinical_amniocentesis_cell_through_needle']['get']                       = 'api/v1/subclinical/API_SCL_10/getList';
$route['api/v1/subclinical/sih_subclinical_amniocentesis_cell_through_needle']['post']                      = 'api/v1/subclinical/API_SCL_10/postData';
$route['api/v1/subclinical/sih_subclinical_amniocentesis_cell_through_needle/(:num)']['get']                = 'api/v1/subclinical/API_SCL_10/get/$1';