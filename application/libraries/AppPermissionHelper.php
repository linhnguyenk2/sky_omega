<?php if ( ! defined('BASEPATH'))
{
	exit('No direct script access allowed');
}

class AppPermissionHelper {
	public function __construct()
	{
		$this->CI = get_instance();
		$this->CI->load->model('list/User_Model');
		$this->CI->load->model('list/Menu_Permission_Model');
		$this->CI->load->model('list/Content_Permission_Model');
	}

	public function getMenus()
	{
	}

	/**
	 * getMenuId
	 * Returns menu ID which like title
	 *
	 * @param    string $title
	 *
	 * @return    string menuID
	 */
	public function getMenuId($title)
	{
		$menuId = '';

		$this->CI->db->select('*');
		$this->CI->db->from('sih_menu');
		$this->CI->db->like('title', $title);

		$result = $this->CI->db->get()->row();

		if ( ! empty($result))
		{
			$menuId = $result->html_id;
		}

		return $menuId;
	}

	/**
	 * getMenuForUser
	 * Returns html menu of user, get all menu then remove
	 * html_id in menu_permission is NOT permission menu
	 *
	 * @param    int $userId
	 *
	 * @return    string
	 */
	public function getMenuForUser($userId, $data)
	{
		$htmlMenu = $this->CI->load->view($data['view']["menu"], $data, TRUE);

		if (empty($userId) == FALSE)
			$this->removeMenuForUser($userId, $htmlMenu);

		$htmlMenu = $this->setActiveMenu($htmlMenu, $data['activeMenuId']);

		return $htmlMenu;
	}

	private function removeMenuForUser($userId, $htmlMenu)
	{
		$userModel           = new User_Model;
		$menuPermissionModel = new Menu_Permission_Model;

		$user            = $userModel->get($userId);
		$menuPermissions = array();

		if ( ! empty($user) && isset($user->type) && $user->type)
		{
			$menuPermissions = $menuPermissionModel->getMenuPermissionByTypeUser($user->type);
		}

		foreach ($menuPermissions as $menuPerssion)
		{
			$htmlMenu = $this->deleteHtmlByID($htmlMenu, $menuPerssion->html_id);
		}

		return $htmlMenu;
	}

	/**
	 * getContentForUser
	 * Return content of user, get all content then remove
	 * html_id in content_permission is NOT permission menu
	 *
	 * @param    int $userId
	 *
	 * @return    string
	 */
	public function getContentForUser($userId, $data)
	{
		$htmlContent = $this->CI->load->view($data['view']["content"], $data, TRUE);

		if (empty($userId) == FALSE)
		{
			$htmlContent = $this->removeContentForUser($userId, $data, $htmlContent);
		}

		return $htmlContent;
	}

	private function removeContentForUser($userId, $data, $htmlContent)
	{
		$userModel              = new User_Model;
		$contentPermissionModel = new Content_Permission_Model;

		$user               = $userModel->get($userId);
		$contentPermissions = array();

		if ( ! empty($user) && isset($user->type) && $user->type)
		{
			$contentPermissions = $contentPermissionModel->getContentPermissionByTypeUser($user->type, $data['route_path']);
		}

		foreach ($contentPermissions as $contentPerssion)
		{
			$htmlContent = $this->deleteHtmlByID($htmlContent, $contentPerssion->html_id);
		}

		return $htmlContent;
	}

	/**
	 * getMenuForUser
	 * Returns html menu of user
	 *
	 * @param    int $userId
	 *
	 * @return    string
	 */
	public function getTemplate($userId, $data)
	{
		$html = '';

		if (isset($data['view']['header']) && ! empty($data['view']['header']))
		{
			$html .= $this->CI->load->view($data['view']['header'], $data, TRUE);
		}

		if (isset($data['view']['topbar']) && ! empty($data['view']['topbar']))
		{
			$html .= $this->CI->load->view($data['view']['topbar'], $data, TRUE);
		}

		$html .= "<section><section class=\"hbox stretch\">";

		$html .= $this->getMenuForUser($userId, $data);

		if (isset($data['view']['content']))
		{
			//$html .= $this->CI->load->view($data['view']['content'], $data, TRUE);
			$html .= $this->getContentForUser($userId, $data);
		}

		if (isset($data['view']['footer']))
		{
			$html .= $this->CI->load->view($data['view']['footer'], $data, TRUE);
		}

		$html .= "</section> </section>";

		if (isset($data['js']) && ! empty($data['js']))
		{
			foreach ($data['js'] as $srcJs)
			{
				$html .= '<script src="' . $srcJs . '" type="text/javascript"></script>';
			}
		}

		if (isset($data['css']) && ! empty($data['css']))
		{
			foreach ($data['css'] as $srcCss)
			{
				$html .= '<link rel="stylesheet" type="text/css" href="' . $srcCss . '">';
			}
		}

		return $html;
	}

	/**
	 * setActiveMenu
	 * Returns html menu after remove Menu ID
	 *
	 * @param    string $html
	 * @param    int    $menu_id
	 *
	 * @return    string
	 */
	public function setActiveMenu($html, $menuId)
	{
		libxml_use_internal_errors(TRUE);
		$dom                  = new DOMDocument;
		$dom->validateOnParse = TRUE;
		$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'),
			LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

		$parentMenus = explode("_",$menuId);
		$prefixParentMenu = $parentMenus[0];
		array_shift($parentMenus);
		array_pop($parentMenus);

		$xp = new DOMXPath($dom);
		// Set active class for parent menu
		if (!empty($parentMenus))
		{
			foreach ($parentMenus as $parentMenuId)
			{
				$parentMenuId = $prefixParentMenu . '_' .$parentMenuId;
				$col = $xp->query('//li[ @id="' . $parentMenuId . '" ]');

				foreach ($col as $node)
				{
					$class = $node->getAttribute('class');
					$node->setAttribute('class', $class . ' ' . 'active');
				}
			}
		}

		// Set active class for current menu
		$col = $xp->query('//li[ @id="' . $menuId . '" ]');

		if ( ! empty($col))
		{
			foreach ($col as $node)
			{
				$class = $node->getAttribute('class');
				$node->setAttribute('class', $class . ' ' . 'active');
			}
		}

		$html = $dom->saveHTML();
		$dom  = NULL;
		libxml_use_internal_errors(FALSE);

		return $html;
	}

	/**
	 * deleteHtmlByID
	 * Returns html menu after remove Menu ID
	 *
	 * @param    string $html
	 * @param    string $nameId
	 *
	 * @return    string
	 */
	public function deleteHtmlByID($html, $nameId)
	{
		libxml_use_internal_errors(TRUE);
		$dom                  = new DOMDocument;
		$dom->validateOnParse = TRUE;
		$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'),
			LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

		$xp = new DOMXPath($dom);

		$col = $xp->query('//*[ @id="' . $nameId . '" ]');

		if ( ! empty($col))
		{
			foreach ($col as $node)
			{
				$node->parentNode->removeChild($node);
			}
		}

		$html = $dom->saveHTML();
		$dom  = NULL;
		libxml_use_internal_errors(FALSE);

		return $html;
	}

	public function removeContentForCurrentUser()
	{

	}

	public function addContentForCurrentUser()
	{

	}
}