<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Doctrine 2.5.14 bootstrap
 *
 */
require_once "vendor/autoload.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class Doctrine {

    public $entityManager = null;

    public function __construct() {

        $this->ci = & get_instance();

        $db['default']['dbdriver'] = $this->ci->db->dbdriver;
        $db['default']['username'] = $this->ci->db->username;
        $db['default']['password'] = $this->ci->db->password;
        $db['default']['database'] = $this->ci->db->database;
        $db['default']['char_set'] = $this->ci->db->char_set;

        $daa = str_replace('\\', "/", __DIR__);
        $paths = array($daa . "/../models/Entities");
        $isDevMode = true;

        $dbParams = array(
            'driver' => $db['default']['dbdriver'],
            'user' => $db['default']['username'],
            'password' => $db['default']['password'],
            'dbname' => $db['default']['database'],
            'charset' => $db['default']['char_set'],
        );
        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
        $entityManager = EntityManager::create($dbParams, $config);
        $this->entityManager = $entityManager;
    }
    public function entityManager() {
        return $this->entityManager;
    }
}
