<?php if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_Library
{
    // UUID - universally unique identifier
    // 123e4567-e89b-12d3-a456-426655440000
    public function generateDeviceId()
    {
        // '%04x%04x-%04x-%04x-%04x-%04x%04x%04x'
        $format = '%04x%04x%04x%04x%04x%04x%04x%04x';

        return sprintf($format,
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Method to generate Salt = userId + current time, store this in sih_user
     *
     * @access public
     *
     * @param string $uniqueId uniqueId = userId
     *
     * @return string
     */
    public function generateSalt($uniqueId)
    {
        return md5($uniqueId + time());
    }
}