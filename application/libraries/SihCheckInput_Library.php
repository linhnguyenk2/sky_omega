<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * class retrun result check type input by string function
 */
class SihCheckInput_Library {

    /**
     * Convert and set from and to, max
     */
    public function convert_type_data_php($string){
        $php=[]; $return =[];
        $php['int']='it_isnumber';       
        $php['varchar']='it_isstring';
        $php['text']='it_isstring';
        $php['date']='is_isdate';
        $php['tinyint']='it_istinyint';

        $mystring = $string;
        $findme   = '(';
        $pos = strpos($mystring, $findme);
        if($pos){
            //example int(8), varchar(20)
            $_tem = explode("(",$mystring);
            $return['php_function_check']=$php[$_tem[0]];
            $trimdata = trim($_tem[1],')');
            $return['php_leng']=$trimdata;
        }else{
            //examle text,data
            $return['php_function_check'] = $php[$mystring];
            $return['php_leng'] = null;
        }
        return $return;
    }
    public function scil_check_input($str_function,$value,$default_null=null)
    {
        //var_dump($str_function);
        $namefunction=$str_function;
        return $this->$namefunction($value,$default_null);
    }
    private function it_isstring($value,$default_null=null){
        
        $result['check_status']=true;
        $result['check_msg']='';
        if(empty($value)){
            if($default_null=='NO'){
                $result['check_status']=false;
                $result['check_msg']='Not Null string';
            }
        }else{
            if(!is_string($value)){
                $result['check_status']=false;
                $result['check_msg']='Not String';
            }
        }
        return $result;
    }
    private function it_isnumber($value,$default_null=null){
      
        $result['check_status']=true;
        $result['check_msg']='';
        if(empty($value)){
            if($default_null=='NO'){
                $result['check_status']=false;
                $result['check_msg']='Not Null number';
            }
        }else{
            if(!is_numeric($value)){
                $result['check_status']=false;
                $result['check_msg']='Not Number';
            }
        }

        return $result;
    }
    //https://stackoverflow.com/a/19271434/9811260
    //why right https://stackoverflow.com/a/7099546/9811260
    private function is_isdate($value,$default_null=null){
        
        
        $result['check_status']=true;
        $result['check_msg']='';
        if(empty($value)){
            if($default_null=='NO'){
                $result['check_status']=false;
                $result['check_msg']='Not Null date';
            }
        }else{
            $format = 'Y-m-d';
            $d = DateTime::createFromFormat($format, $value);
            $check =  $d && $d->format($format) === $value;
            // var_dump($format, $value);
            //var_dump($check);die;
            if(!$check){
                $result['check_status']=false;
                $result['check_msg']='Not date right format Y-m-d';
            }
        }
        return $result;
    }
    //is true or false //http://php.net/manual/en/function.boolval.php
    private function it_istinyint($value,$default_null=null){
        
        $result['check_status']=true;
        $result['check_msg']='';
        if(empty($value)){
            if($default_null=='NO'){
                $result['check_status']=false;
                $result['check_msg']='Not Null T/F';
            }
        }else{
            if(!boolval($value)){
                $result['check_status']=false;
                $result['check_msg']='Not is true|fase';
            }
        }
        
        return $result;
    }
}


