<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Loaddefaultselect_library {

    var $CI;
    public function __construct($params = array())
    {
        $this->CI =& get_instance();
    }

    
    public function get_default($list, $variable, $at=null, $type=null){
        if(!$list){
            return null;
        }
        switch ($type) {
            case 'text':
                $value = $this->get_select_text_at($list,$at);
                break;
            case 'array':
                $value = $this->getselect_array($list);
                break;
            case 'option':
                $value = $this->getselect_option($list);
                break;
            default:
                $value = $this->getselect_array($list);
                break;
        }
        return $value;
    }

    public function getselect_array($list) {
        return $list;
    }

    public function getselect_option($list){
        $str='';
        foreach ($list as $key => $value) {
            //<option value="1">Hiện thị</option>
            $str .='<option value="'.$key.'">'.$value.'</option>';
        }
        return $str;
    }

    public function get_select_text_at($list,$at){
        //var_dump($list);die;
        if(isset($list[$at])){
            
            if(is_array($list[$at])){
                return '<span att-value="'.$at.'" att-forent="'.$list[$at][1].'">'.$list[$at][0].'</span>';
            }else{
                return '<span att-value="'.$at.'">'.$list[$at].'</span>';
            }
        }else{
            return '<span att-value="'.$at.'">'.$at.'</span>';
        }
    }
}