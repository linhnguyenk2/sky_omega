<?php

require_once "/../../vendor/autoload.php";

if (!defined('ENVIRONMENT')){
	define('ENVIRONMENT', 'development');
	define('BASEPATH', '');
	require_once "/../config/database.php"; //load infor database
}

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$daa = str_replace('\\', "/", __DIR__);

$paths = array($daa."/../models/Entities");
$isDevMode = true;

 $dbParams = array(
     'driver'   => $db['default']['dbdriver'],
     'user'     => $db['default']['username'],
     'password' => $db['default']['password'],
     'dbname'   => $db['default']['database'],
     'charset'  => $db['default']['char_set'],
 );

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config);