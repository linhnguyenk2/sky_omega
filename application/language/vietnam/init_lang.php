<?php
$lang['add']       = 'Thêm';
$lang['edit']      = 'Sửa';
$lang['delete']    = 'Xóa';
$lang['publish']   = 'Hiển thị';
$lang['unpublish'] = 'Ẩn';

$lang['add_new']     = 'Thêm mới';
$lang['edit_post']   = 'Chỉnh sửa bài';
$lang['save']        = 'Lưu';
$lang['save_change'] = 'Lưu thay đổi';
$lang['close']       = 'Đóng';

$lang['required'] = 'Yêu cầu';
$lang['search']   = 'Tìm kiếm';

$lang['creation_date']   = 'Ngày Tạo';
$lang['created_by_name'] = 'Người tạo';
$lang['status']          = 'Trạng thái';
$lang['ordering']        = 'Thứ tự hiện thị';
$lang['vat']             = 'VAT';

/* For publish,unpublish */
$lang['event_publish_one']         = 'Bạn có chắc chắn muốn đổi tình trạng của';
$lang['event_publish_one_change']  = 'thành hiển thị?';
$lang['event_publish_more']        = 'Bạn có chắc chắn muốn đổi tình trạng của các';
$lang['event_publish_more_change'] = 'đã được chọn thành hiển thị?';

$lang['event_unpublish_one']         = 'Bạn có chắc chắn muốn đổi tình trạng của';
$lang['event_unpublish_one_change']  = 'thành ẩn?';
$lang['event_unpublish_more']        = 'Bạn có chắc chắn muốn đổi tình trạng của các';
$lang['event_unpublish_more_change'] = 'đã được chọn thành ẩn?';

/* For delete */
$lang['event_delete_one']         = 'Bạn có chắc chắn muốn xóa ';
$lang['event_delete_one_change']  = 'đã chọn?';
$lang['event_delete_more']        = 'Bạn có chắc chắn muốn xóa các';
$lang['event_delete_more_change'] = 'đã được chọn?';