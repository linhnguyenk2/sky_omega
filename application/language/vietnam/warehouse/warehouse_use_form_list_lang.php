<?php
$lang['home']            = 'Home';
$lang['warehouse_title'] = 'Kho';
$lang['view_title']      = 'Danh sách dịch vụ tiêu hao';

$lang['form_code']              = 'Mã phiếu';
$lang['form_creation_date']     = 'Thời điểm thực hiện';
$lang['form_created_by_name']   = 'Nhân viên thực hiện';
$lang['form_used_service_name'] = 'Dịch vụ tiêu hao';
$lang['inventories']               = 'Danh sách tồn kho';
$lang['warehouse_export'] = 'Xuất kho';
$lang['warehouse_import'] = 'Nhập kho';
$lang['warehouse_sale']   = 'Xuất bán';
$lang['warehouse_use']    = 'Tiêu hao dịch vụ';
$lang['warehouse_check']  = 'Kiểm kho';