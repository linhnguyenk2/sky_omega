<?php
$lang['home']                 = 'Home';
$lang['warehouse_title']      = 'Kho';
$lang['view_title']           = 'Phiếu xuất bán';
$lang['warehouse_sale_staff'] = 'Nhân viên bán hàng: ';
$lang['warehouse_sale_date']  = 'Thời điểm xuất bán: ';

$lang['form_code']            = 'Mã phiếu';
$lang['form_creation_date']   = 'Ngày lập phiếu';
$lang['form_created_by_name'] = 'Người lập phiếu';
$lang['form_content']         = 'Nội dung lập phiếu';

$lang['inventories']      = 'Danh sách tồn kho';
$lang['warehouse_export'] = 'Xuất kho';
$lang['warehouse_import'] = 'Nhập kho';
$lang['warehouse_sale']   = 'Xuất bán';
$lang['warehouse_use']    = 'Tiêu hao dịch vụ';
$lang['warehouse_check']  = 'Kiểm kho';

$lang['sale_products']        = 'Danh sách sản phẩm bán';
$lang['product_code']         = 'Mã sản phẩm';
$lang['product_name']         = 'Tên sản phẩm';
$lang['product_type']         = 'Loại sản phẩm';
$lang['product_group']        = 'Nhóm sản phẩm';
$lang['product_number']       = 'Số lượng';
$lang['product_unit']         = 'Đơn vị';
$lang['product_expired_date'] = 'Ngày hết hạn';
$lang['product_package_code'] = 'Mã lô';

$lang['button_save']    = 'Lưu';
$lang['button_publish'] = 'Duyệt xuất';
$lang['button_exit']    = 'Thoát';