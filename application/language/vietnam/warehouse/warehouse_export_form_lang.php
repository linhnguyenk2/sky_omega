<?php
$lang['home']            = 'Home';
$lang['warehouse_title'] = 'Kho';
$lang['view_title']      = 'Phiếu xuất kho';

$lang['product_code']         = 'Mã sản phẩm';
$lang['product_name']         = 'Tên sản phẩm';
$lang['product_type']         = 'Loại sản phẩm';
$lang['product_group']        = 'Nhóm sản phẩm';
$lang['product_number']       = 'Số lượng';
$lang['product_package_code'] = 'Mã lô';

$lang['warehouse_request_form'] = 'Phiếu yêu cầu xuất';
$lang['storekeeper_staff']      = 'Thủ kho';
$lang['medicine_chief_staff']   = 'Trưởng khoa dược';
$lang['subclinic_chief_staff']  = 'Trưởng khoa lâm sàn';
$lang['reason_export']          = 'Lý do xuất';

$lang['button_save']    = 'Lưu';
$lang['button_publish'] = 'Duyệt xuất';




