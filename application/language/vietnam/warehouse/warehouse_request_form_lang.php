<?php
$lang['home']            = 'Home';
$lang['warehouse_title'] = 'Kho';
$lang['view_title']      = 'Phiếu yêu cầu xuất bán';

$lang['form_code']            = 'Mã phiếu';
$lang['form_creation_date']   = 'Ngày lập phiếu';
$lang['form_created_by_name'] = 'Người yêu cầu xuất';
$lang['form_content']         = 'Nội dung lập phiếu';


$lang['warehouse_export'] = 'Xuất kho';
$lang['warehouse_import'] = 'Nhập kho';
$lang['warehouse_sale']   = 'Xuất bán';
$lang['warehouse_use']    = 'Tiêu hao dịch vụ';
$lang['warehouse_check']  = 'Kiểm kho';
$lang['chief_staff']      = 'Trưởng khoa';

$lang['sale_products']        = 'Danh sách sản phẩm trong phiếu';
$lang['product_code']         = 'Mã sản phẩm';
$lang['product_name']         = 'Tên sản phẩm';
$lang['product_type']         = 'Loại sản phẩm';
$lang['product_group']        = 'Nhóm sản phẩm';
$lang['product_number']       = 'Số lượng';
$lang['product_unit']         = 'Đơn vị';
$lang['product_expired_date'] = 'Ngày hết hạn';
$lang['product_package_code'] = 'Mã lô';
$lang['reason']               = 'Lý do xuất ';
$lang['default_unit']         = 'Viên';

$lang['button_save_temporary']  = 'Lưu tạm';
$lang['product_request_number'] = 'Số lượng yêu cầu';
$lang['product_accept_number']  = 'Số lượng duyệt';
$lang['button_save']            = 'Lưu';
$lang['button_publish']         = 'Duyệt xuất';
$lang['button_exit']            = 'Thoát';