<?php
$lang['home']            = 'Home';
$lang['warehouse_title'] = 'Kho';
$lang['view_title']      = 'Phiếu nhập kho';

$lang['form_code']            = 'Mã phiếu';
$lang['form_creation_date']   = 'Ngày lập phiếu';
$lang['form_created_by_name'] = 'Người lập phiếu';
$lang['form_content']         = 'Nội dung lập phiếu';

$lang['inventories']       = 'Danh sách tồn kho';
$lang['warehouse_export']  = 'Xuất kho';
$lang['warehouse_import']  = 'Nhập kho';
$lang['warehouse_sale']    = 'Xuất bán';
$lang['warehouse_use']     = 'Tiêu hao dịch vụ';
$lang['warehouse_check']   = 'Kiểm kho';
$lang['default_unit']      = 'Viên';
$lang['storekeeper_staff'] = 'Thủ kho';

$lang['product_code']             = 'Mã sản phẩm';
$lang['product_name']             = 'Tên sản phẩm';
$lang['product_type']             = 'Loại sản phẩm';
$lang['product_group']            = 'Nhóm sản phẩm';
$lang['product_number']           = 'Số lượng';
$lang['product_warehouse_number'] = 'Số lượng kho';
$lang['product_current_number']   = 'Số lượng thực tế';
$lang['product_package_code']     = 'Mã lô';
$lang['reason']                   = 'Lý do';
$lang['product_sub_code']         = 'Mã phụ';
$lang['product_unit']             = 'Đơn vị';
$lang['product_expired_date']     = 'Ngày hết hạn';

$lang['button_export_warehouse'] = 'Xuất thông tin kho hiện tại';
$lang['button_import_warehouse'] = 'Nhập thông tin kiểm kho';
$lang['button_save_temporary']   = 'Lưu tạm';
$lang['button_publish']          = 'Duyệt';

$lang['search_products'] = 'Danh sách sản phẩm trong phiếu';

$lang['button_save']    = 'Lưu';
$lang['button_publish'] = 'Nhập kho';