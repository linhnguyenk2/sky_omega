<?php

$lang['home']                      = 'Home';
$lang['product_code']              = 'Mã sản phẩm';
$lang['product_name']              = 'Tên sản phẩm';
$lang['product_type']              = 'Loại sản phẩm';
$lang['number_of_outstanding']     = 'Loại sản phẩm';
$lang['number_of_outstanding']     = 'Số lượng tồn';
$lang['number_of_reservations']    = 'Số lượng đặt';
$lang['warehouse_title']           = 'Kho';
$lang['warehouse_code']            = 'Mã kho';
$lang['warehouse_name']            = 'Tên kho';
$lang['warehouse_description']     = 'Miêu tả kho';
$lang['warehouses']                = 'Danh sách kho';
$lang['inventories']               = 'Danh sách tồn kho';
$lang['warehouse_export_forms']    = 'Danh sách phiếu xuất kho';
$lang['warehousing_history']       = 'Danh sách lịch sử nhập kho';
$lang['history_check_warehouses']  = 'Danh sách lịch sử kiểm kho';
$lang['history_export_warehouses'] = 'Danh sách lịch sử xuất kho';
$lang['history_use_warehouses']    = 'Danh sách lịch sử tiêu hao';
$lang['warehouse_export_form']     = 'Chi tiết phiếu xuất kho';
$lang['warehouse_request_form']    = 'Chi tiết phiếu yêu cầu xuất';
$lang['warehouse_import_form']     = 'Chi tiết phiếu nhập kho';
$lang['warehouse_check_form']      = 'Chi tiết phiếu kiểm kho';
$lang['warehouse_sale_form']       = 'Chi tiết phiếu xuất bán';
$lang['warehouse_use_form']        = 'Chi tiết tiêu hao vật tư y tế';

$lang['warehouse_export'] = 'Xuất kho';
$lang['warehouse_import'] = 'Nhập kho';
$lang['warehouse_sale']   = 'Xuất bán';
$lang['warehouse_use']    = 'Tiêu hao dịch vụ';
$lang['warehouse_check']  = 'Kiểm kho';