<?php
$lang['home']       = 'Home';
$lang['warehouse_title'] = 'Kho';
$lang['view_title'] = 'Danh sách xuất bán';

$lang['form_code']            = 'Mã phiếu';
$lang['form_creation_date']   = 'Ngày lập phiếu';
$lang['form_created_by_name'] = 'Người lập phiếu';
$lang['form_content']         = 'Nội dung lập phiếu';

$lang['inventories']               = 'Danh sách tồn kho';
$lang['warehouse_export'] = 'Xuất kho';
$lang['warehouse_import'] = 'Nhập kho';
$lang['warehouse_sale']   = 'Xuất bán';
$lang['warehouse_use']    = 'Tiêu hao dịch vụ';
$lang['warehouse_check']  = 'Kiểm kho';