<?php
$lang['home']            = 'Home';
$lang['warehouse_title'] = 'Kho';
$lang['view_title']      = 'Phiếu tiêu hao vật tư';

$lang['form_code']              = 'Mã phiếu';
$lang['form_creation_date']     = 'Thời điểm thực hiện';
$lang['form_created_by_name']   = 'Nhân viên thực hiện';
$lang['form_used_service_name'] = 'Dịch vụ tiêu hao';
$lang['inventories']               = 'Danh sách tồn kho';
$lang['warehouse_export'] = 'Xuất kho';
$lang['warehouse_import'] = 'Nhập kho';
$lang['warehouse_sale']   = 'Xuất bán';
$lang['warehouse_use']    = 'Tiêu hao dịch vụ';
$lang['warehouse_check']  = 'Kiểm kho';

$lang['sale_products']        = 'Danh sách sản phẩm tiêu hao';
$lang['product_code']         = 'Mã sản phẩm';
$lang['product_name']         = 'Tên sản phẩm';
$lang['product_type']         = 'Loại sản phẩm';
$lang['product_group']        = 'Nhóm sản phẩm';
$lang['product_number']       = 'Số lượng';
$lang['product_unit']         = 'Đơn vị';
$lang['product_expired_date'] = 'Ngày hết hạn';
$lang['product_package_code'] = 'Mã lô';

$lang['button_save']    = 'Lưu';
$lang['button_publish'] = 'Duyệt xuất';
$lang['button_exit']    = 'Thoát';