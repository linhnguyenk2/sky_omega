<?php


$lang['add'] = 'Thêm';
$lang['edit'] = 'Sửa';
$lang['delete'] = 'Xóa';
$lang['publish'] = 'Hiển thị';
$lang['unpublish'] = 'Ẩn';

$lang['addnew'] = 'Thêm mới';
$lang['edit_post'] = 'Chỉnh sửa bài';
$lang['save'] = 'Lưu';
$lang['save_change'] = 'Lưu Thay Đổi';
$lang['close'] = 'Đóng';

$lang['required'] = 'Yêu cầu';
$lang['search'] = 'Tìm kiếm';

$lang['date_create'] = 'Ngày Tạo';
$lang['people_create'] = 'Người Tạo';
$lang['status'] = 'Trạng Thái';
$lang['order'] = 'Thứ tự hiện thị';

$lang['vat'] = 'VAT';


/* patient_information */
$lang['title_patient_information'] = 'Thông Tin Bệnh Nhân';
$lang['patient_information'] = 'Thông Tin Bệnh Nhân';

// maternity_hospital
$lang['title_maternity_hospital'] = 'Bệnh Án Sản Khoa';
$lang['maternity_hospital'] = 'Bệnh Án Sản Khoa';


/* for publish,unpublish */
$lang['event_publish_one']='Bạn có chắc chắn muốn đổi tình trạng của';
$lang['event_publish_one_change']='thành hiển thị?';
$lang['event_publish_more']='Bạn có chắc chắn muốn đổi tình trạng của các';
$lang['event_publish_more_change']='đã được chọn thành hiển thị?';

$lang['event_unpublish_one']='Bạn có chắc chắn muốn đổi tình trạng của';
$lang['event_unpublish_one_change']='thành ẩn?';
$lang['event_unpublish_more']='Bạn có chắc chắn muốn đổi tình trạng của các';
$lang['event_unpublish_more_change']='đã được chọn thành ẩn?';

/* for delete */
$lang['event_delete_one']='Bạn có chắc chắn muốn xóa ';
$lang['event_delete_one_change']='đã chọn?';
$lang['event_delete_more']='Bạn có chắc chắn muốn xóa các';
$lang['event_delete_more_change']='đã được chọn?';

/* list catgory for update,publish,unpublish */

$lang['for_patient_information'] = 'Thông Tin Bệnh Nhân';
$lang['for_maternity_hospital'] = 'Bệnh Án Sản Khoa';