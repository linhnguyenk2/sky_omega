<?php

$lang['add'] = 'Thêm';
$lang['edit'] = 'Sửa';
$lang['delete'] = 'Xóa';
$lang['publish'] = 'Publish';
$lang['unpublish'] = 'UnPublish';

$lang['addnew'] = 'Thêm mới';
$lang['edit_post'] = 'Chỉnh sửa bài';
$lang['save'] = 'Lưu';
$lang['save_change'] = 'Lưu Thay Đổi';
$lang['close'] = 'Đóng';

$lang['required'] = 'Yêu cầu';
$lang['search'] = 'Tìm kiếm';

/* manage user */
$lang['manage_user'] = 'Quản lý nhân sự';
$lang['list_user'] = 'Danh sách nhân sự';
$lang['role'] = 'Tên Quền';
$lang['title'] = 'Chức danh';
$lang['name'] = 'Tên tài khoản';
$lang['email'] = 'Email';
$lang['phone_number'] = 'Số điện thoại';
$lang['avatar'] = 'Avatar';
$lang['status'] = 'Trạng Thái';
$lang['change_pasword'] = 'Đổi mật khẩu';
$lang['pasword'] = 'Mật khẩu';