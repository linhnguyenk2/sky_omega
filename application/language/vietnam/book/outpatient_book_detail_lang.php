<?php


$lang['sokham_phukhoa'] = 'SỔ KHÁM PHỤ KHOA';
$lang['sokham_phukhoa1'] = 'Sổ khám phụ khoa';

$lang['skpk_thongtin_so'] = 'Thông tin sổ';
$lang['skpk_tt_benhnhan'] = 'Thông tin bệnh nhân';
$lang['skpk_tt_phukhoa'] = 'Thông tin phụ khoa';
$lang['skpk_gd'] = 'Gia đình';
$lang['skpk_benhnoikhoa'] = 'Bệnh nội khoa';
$lang['skpk_lichsu_thamkham'] = 'Lịch sử thăm khám';
