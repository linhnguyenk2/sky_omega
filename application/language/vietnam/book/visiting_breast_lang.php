<?php


$lang['sokham_thai'] = 'SỔ KHÁM THAI';

$lang['skt_thongtin_so'] = 'Thông tin sổ';
$lang['skt_tt_benhnhan'] = 'Thông tin bệnh nhân';
$lang['skt_tt_thaisan'] = 'Thông tin thai sản';
$lang['skt_tt_phukhoa'] = 'Thông tin phụ khoa';
$lang['skt_thongtin_khac'] = 'Thông tin khác';
$lang['skt_lichsu_thamkham'] = 'Lịch sử thăm khám';