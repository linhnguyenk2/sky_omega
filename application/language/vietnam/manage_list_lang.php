<?php

$lang['list'] = 'Danh Mục';

$lang['add'] = 'Thêm';
$lang['edit'] = 'Sửa';
$lang['delete'] = 'Xóa';
$lang['publish'] = 'Hiển thị';
$lang['unpublish'] = 'Ẩn';

$lang['addnew'] = 'Thêm mới';
$lang['edit_post'] = 'Chỉnh sửa bài';
$lang['save'] = 'Lưu';
$lang['save_change'] = 'Lưu Thay Đổi';
$lang['close'] = 'Đóng';

$lang['required'] = 'Yêu cầu';
$lang['search'] = 'Tìm kiếm';

$lang['date_create'] = 'Ngày Tạo';
$lang['people_create'] = 'Người Tạo';
$lang['status'] = 'Trạng Thái';
$lang['order'] = 'Thứ tự hiện thị';

$lang['vat'] = 'VAT';

/* list nation */
$lang['title_list_nations'] = 'Danh sách quốc gia';
$lang['name_nation'] = 'Tên Quốc Gia';
$lang['code_nation'] = 'Mã Quốc Gia';
$lang['list_nation'] = 'Danh Mục Quốc Gia';

/* list_foreigner */
$lang['title_list_foreigner'] = 'Danh Mục Ngoại Kiều';
$lang['name_foreigner'] = 'Tên Ngoại Kiều';
$lang['code_foreigner'] = 'Mã Ngoại Kiều';
$lang['list_foreigner'] = 'Danh Mục Ngoại Kiều';


/* list provinces */
$lang['title_list_provinces'] = 'Danh sách thành phố';
$lang['name_provinces'] = 'Tên Thành Phố';
$lang['list_provinces'] = 'Danh Mục Thành Phố';
$lang['code_provinces'] = 'Mã Thành Phố';
$lang['name_nation_provinces'] = 'Tên Quốc Gia';

/* list districts */
$lang['title_list_districts'] = 'Danh sách quận huyện';
$lang['code_districts'] = 'Code Quận/Huyện';
$lang['name_districts'] = 'Tên Quận/Huyện';
$lang['list_districts'] = 'Danh Mục Quận/Huyện';
/* list wards */
$lang['title_list_wards'] = 'Danh sách phường xã';
$lang['name_wards'] = 'Tên Phường Xã';
$lang['code_wards'] = 'Mã Phường Xã';
$lang['list_wards'] = 'Danh Mục Phường Xã';
/* list titles */
$lang['title_list_titles'] = 'Danh Mục Nghề Nghiệp';
$lang['name_titles'] = 'Tên Nghề Nghiệp';
$lang['code_titles'] = 'Mã Nghề Nghiệp';
$lang['note_titles'] = 'Ghi Chú';
$lang['list_titles'] = 'Danh Mục Nghề Nghiệp';

/* list departments */
$lang['title_list_departments'] = 'Danh Sách Khoa/Phòng';
$lang['list_departments'] = 'Danh Sách Khoa/Phòng';
$lang['name_departments']='Tên Khoa';
$lang['code_departments']='Mã Khoa';
$lang['note_departments']='Ghi Chú';

/* list_reason_discharge */
$lang['title_list_reason_discharge'] = 'Danh sách lý do xuất viện';
$lang['list_reason_discharge']='Lý Do Xuất Viện';
$lang['code_discharge']='Mã Lý Do';
$lang['name_discharge']='Tên Lý Do';
$lang['name']='Tên';
$lang['note']='Ghi Chú';
$lang['date']='Ngày';
$lang['code']='Mã';

/* list_reason_transfer */
$lang['title_list_reason_transfer'] = 'Danh sách lý do chuyển viện';
$lang['list_reason_transfer']='Lý Do Chuyển Viện';
$lang['code_transfer']='Mã Lý Do';
$lang['name_transfer']='Tên Lý Do';

/* list_materials */
$lang['title_list_materials'] = 'Danh sách vật dụng';
$lang['list_materials']='Danh Mục Vật Dụng';
$lang['code_materia']='Mã Vật Dụng';
$lang['name_materia']='Tên Vật Dụng';
$lang['dprice']='Đơn Giá';
$lang['dprice_eng']='Đơn Giá English';
$lang['muser']='Người Dùng';

/* list_midwives */
$lang['title_list_midwives'] = 'Danh sách nữ hộ sinh';
$lang['list_midwives']='Nữ Hộ Sinh';
$lang['code_midwive']='Mã Hộ Sinh';
$lang['full_name']='Họ và Tên';
$lang['at_department']='Phòng Ban';

/* list_group_therapys */
$lang['title_list_group_therapys'] = 'Danh sách khám chữa bệnh';
$lang['list_group_therapys']='Nhóm Khám Chữa Bệnh';
$lang['code_group_therapy']='Mã KCB';
$lang['name_group_therapy']='Tên Khám Chữa Bệnh';

/* list vendor */
$lang['title_list_vendor'] = 'Danh sách nhà cung cấp';
$lang['list_vendor']='Danh mục nhà cung cấp';
$lang['list_vd_vendor_code']='vn Mã Nhà Cung Cấp';
$lang['list_vd_second_code']='vn Mã Phụ';
$lang['list_vd_vendor_name']='vn Tên Nhà Cung Cấp';
$lang['list_vd_name_quick']='vn Tên Tìm Nhanh';
$lang['list_vd_address']='vn Địa Chỉ';
$lang['list_vd_city_province']='vn Tỉnh/Thành Phố';
$lang['list_vd_nation']='vn Quốc Gia';
$lang['list_vd_phone']='vn Điện Thoại';
$lang['list_vd_fax']='vn Số Fax';
$lang['list_vd_group_ncc']='vn Nhóm NCC';
$lang['list_vd_group_settlement']='vn Nhóm Định Khoản';
$lang['list_vd_group_tax']='vn Nhóm thuế';
$lang['list_vd_currency_code']='vn Mã tiền tệ';
$lang['list_vd_credit_limit']='vn Giới Hạn Tín Dụng';
$lang['list_vd_tax_code']='vn Mã Số Thuế';
$lang['list_vd_email']='vn E-mail';
$lang['list_vd_website']='vn WEBSITE';
$lang['list_vd_contact']='vn Người Liên Hệ';
$lang['list_vd_contact_phone']='vn Số ĐT Người Liên Hệ';
$lang['list_vd_date_created']='vn Ngày Tạo';
$lang['list_vd_user']='vn Người Dùng';
$lang['list_vd_bank_account']='vn Mã tài khoản ngân hàng';
$lang['list_vd_bank_name']='vn Tên ngân hàng';
$lang['list_vd_bank_address']='vn Địa chỉ ngân hàng';
$lang['list_vd_modules']='vn MODULES';

/* list currency */
$lang['list_currencies'] = 'Danh Mục Tỷ Giá';
$lang['title_list_currencies'] = 'Danh Mục Tỷ Giá';
$lang['list_curr_name']='Tỷ Giá';
$lang['list_curr_code']='Mã Tiền Tệ';
$lang['list_code_from']='Mã Tiền Tệ';
$lang['list_begin_date']='Ngày Bắt Đầu';
$lang['list_code_to']='Mã Tiền Tệ Quy Đổi';
$lang['list_rate']='Tỷ Giá Quy Đổi';

$lang['yes']='Yes';
$lang['no']='No';

/* list folks */
$lang['title_list_folks'] = 'Danh sách dân tộc';
$lang['name_folks'] = 'Tên Dân Tộc';
$lang['code_folks'] = 'Mã Dân Tộc';
$lang['list_folks'] = 'Danh Mục Dân Tộc';

/* list clinic */
$lang['title_list_clinic'] = 'Danh mục phòng khám';
$lang['name_clinic'] = 'Tên Phòng Khám';
$lang['code_clinic'] = 'Mã Phòng Khám';
// $lang['name_departments'] = 'Tên Khoa';
$lang['location_clinic'] = 'Vị trí';
$lang['phone_clinic'] = 'Điện thoại';
$lang['note_clinic'] = 'Ghi chú';
$lang['note'] = 'Ghi chú';
$lang['list_clinic'] = 'Danh Mục Phòng Khám';


/* list service */
$lang['title_list_service'] = 'Danh mục dịch vụ';
$lang['name_service'] = 'Tên Dịch Vụ';
$lang['code_service'] = 'Mã Dịch Vụ';
$lang['unit_service'] = 'ĐVT';
$lang['service_type_id_service'] = 'Loại Dịch Vụ';
$lang['price_in_hour_service'] = 'Giá Trong Giờ';
$lang['price_overtime_service'] = 'Giá Ngoài Giờ';
$lang['price_holiday_service'] = 'Giá Ngày Lễ';
$lang['price_foreigner_service'] = 'Giá Người Nước Ngoài';
$lang['list_service'] = 'Danh Mục Dịch Vụ';

/* list service type */
$lang['title_list_service_type'] = 'Danh mục loại dịch vụ';
$lang['name_service_type'] = 'Tên Loại Dịch Vụ';
$lang['code_service_type'] = 'Mã Loại Dịch Vụ';
$lang['list_service_type'] = 'Danh Mục Loại Dịch Vụ';

/* list service group */
$lang['title_list_service_group'] = 'Danh mục nhóm dịch vụ';
$lang['name_service_group'] = 'Tên Nhóm Dịch Vụ';
$lang['code_service_group'] = 'Mã Nhóm Dịch Vụ';
$lang['list_service_group'] = 'Danh Mục Nhóm Dịch Vụ';


/* list_package_healthcare_service_type */
$lang['title_list_package_healthcare_service_type'] = 'Danh Mục Loại Gói Khám Bệnh - Sanh';
$lang['name_package_healthcare_service_type'] = 'Tên Loại Gói';
$lang['code_package_healthcare_service_type'] = 'Mã Loại Gói';
$lang['list_package_healthcare_service_type'] = 'Danh Mục Loại Gói Khám Bệnh - Sanh';

/* list_package_healthcare_service */
$lang['title_list_package_healthcare_service'] = 'Danh Mục Gói Khám Bệnh - Sanh';
$lang['name_package_healthcare_service'] = 'Tên Gói Khám';
$lang['code_package_healthcare_service'] = 'Mã Gói Khám';
$lang['package_healthcare_service_type_id_package_healthcare_service'] = 'Loại Gói Khám';
$lang['price_package_healthcare_service'] = 'Giá Tiền';
$lang['price_foreigner_package_healthcare_service'] = 'Giá Ngoại Kiều';
$lang['list_package_healthcare_service'] = 'Danh Mục Gói Khám Bệnh - Sanh';

/* list medicine */
$lang['title_list_medicine'] = 'Danh Mục Thuốc';
$lang['name_medicine'] = 'Tên thuốc';
$lang['code_medicine'] = 'Mã thuốc';
//$lang['note_medicine'] = 'Mã thuốc';

$lang['compound_name_medicine']='Tên hợp chất';
$lang['way_in_medicine']='Đường vào';
$lang['specification_medicine']='Quy cách';
$lang['unit_medicine']='Đơn vị tính';
$lang['unit_price_medicine']='Đơn giá';
$lang['explain_medicine']='Diễn giải';
$lang['note_medicine']='Ghi chú';
$lang['point_medicine']='Chỉ định';
$lang['contraindications_medicine']='Chống chỉ định';
$lang['the_drug_is_not_combined_medicine']='Thuốc không kết hợp';
$lang['subordinate_units_medicine']='Đơn vị phụ';
$lang['list_medicine'] = 'Danh Mục Thuốc';

/* list_contraindication */
$lang['title_list_contraindication'] = 'Danh Mục Chống Chỉ Định';
$lang['name_list_contraindication'] = 'Tên chống chỉ định';
$lang['code_list_contraindication'] = 'Mã chống chỉ định';
$lang['list_contraindication'] = 'Danh Mục Chống Chỉ Định';

/* list_insurrance_company */
$lang['title_list_insurrance_company'] = 'Danh Mục Công Ty Bảo Hiểm';
$lang['name_list_insurrance_company'] = 'Tên Công Ty Bảo Hiểm';
$lang['code_list_insurrance_company'] = 'Mã Công Ty Bảo Hiểm';
$lang['address_list_insurrance_company'] = 'Địa Chỉ';
$lang['phone_list_insurrance_company'] = 'Điện Thoại';
$lang['contact_list_insurrance_company'] = 'Người Liên Hệ';
$lang['list_insurrance_company'] = 'Danh Mục Công Ty Bảo Hiểm';

/* list_insurrance_company */
$lang['title_list_health_insurrance_card'] = 'Danh Mục Loại Thẻ Bảo Hiểm';
$lang['name_list_health_insurrance_card'] = 'Tên Công Ty Bảo Hiểm';
$lang['code_list_health_insurrance_card'] = 'Mã Công Ty Bảo Hiểm';
$lang['insurrance_company_id_list_health_insurrance_card'] = 'Công Ty Bảo Hiểm';
$lang['llimit_list_health_insurrance_card'] = 'Hạn Mức';
$lang['list_health_insurrance_card'] = 'Danh Mục Loại Thẻ Bảo Hiểm';

/* list_supllier */
$lang['title_list_supllier'] = 'Danh Mục Nhà Cung Cấp';
$lang['name_list_supllier'] = 'Tên NCC';
$lang['code_list_supllier'] = 'Mã NCC';
$lang['address_list_supllier'] = 'Địa Chỉ';
$lang['phone_list_supllier'] = 'Điện Thoại';
$lang['mst_list_supllier'] = 'MST';
$lang['contact_list_supllier'] = 'Người Liên Hệ';
$lang['list_supllier'] = 'Danh Mục Nhà Cung Cấp';

/* list_patient */
$lang['title_list_patient'] = 'Danh Sách Bệnh Nhân';
$lang['name_list_patient'] = 'Tên Bệnh Nhân';
$lang['code_list_patient'] = 'Mã Bệnh Nhân';
$lang['cmnd_list_patient'] = 'CMND';
$lang['birthday_list_patient'] = 'Ngày Sinh';
$lang['gender_list_patient'] = 'Giới Tính';
$lang['address_list_patient'] = 'Địa Chỉ';
$lang['city_list_patient'] = 'Thành Phố';
$lang['district_list_patient'] = 'Quận';
$lang['ward_list_patient'] = 'Huyện';
$lang['list_patient'] = 'Danh Sách Bệnh Nhân';

/* list_medical_record */
$lang['title_list_medical_record'] = 'Danh Mục Loại Bệnh Án';
$lang['name_list_medical_record'] = 'Tên Loại Bệnh Án';
$lang['code_list_medical_record'] = 'Mã Loại Bệnh Án';
$lang['template_list_medical_record'] = 'Tên Loại Template';
$lang['list_medical_record'] = 'Danh Mục Loại Bệnh Án';

/* list_relationship_between_patient */ 
$lang['title_list_relationship_between_patient'] = 'Danh Mục Mối Quan Hệ Giữa Các Bệnh Nhân';
$lang['name_list_relationship_between_patient'] = 'Tên Mối Quan Hệ';
$lang['code_list_relationship_between_patient'] = 'Mã Mối Quan Hệ';
$lang['list_relationship_between_patient'] = 'Danh Mục Mối Quan Hệ Giữa Các Bệnh Nhân';

/* list_place_transfer */
$lang['title_list_place_transfer'] = 'Danh Mục Nơi Chuyển Viện';
$lang['name_list_place_transfer'] = 'Tên Nơi Chuyển Viện';
$lang['code_list_place_transfer'] = 'Mã Nơi Chuyển Viện';
$lang['list_place_transfer'] = 'Danh Mục Nơi Chuyển Viện';

/* list_place_introduction */
$lang['title_list_place_introduction'] = 'Danh Mục Nơi Giới Thiệu';
$lang['name_list_place_introduction'] = 'Tên Nơi Giới Thiệu';
$lang['code_list_place_introduction'] = 'Mã Nơi Giới Thiệu';
$lang['list_place_introduction'] = 'Danh Mục Nơi Giới Thiệu';

/* list_method_birth */
$lang['title_list_method_birth'] = 'Danh Mục Phương Pháp Sanh';
$lang['name_list_method_birth'] = 'Tên Phương Pháp Sanh';
$lang['code_list_method_birth'] = 'Mã Phương Pháp Sanh';
$lang['list_method_birth'] = 'Danh Mục Phương Pháp Sanh';

/* list_chapter_icd10 */
$lang['title_list_chapter_icd10'] = 'Danh Mục Hệ Bệnh';
$lang['name_list_chapter_icd10'] = 'Tên Hệ Bệnh';
$lang['code_list_chapter_icd10'] = 'Mã Hệ Bệnh';
$lang['list_chapter_icd10'] = 'Danh Mục Hệ Bệnh';
$lang['for_list_chapter_icd10'] = 'Danh Mục Hệ Bệnh';

/* list_disease_icd10 */
$lang['title_list_disease_icd10'] = 'Danh Mục Bệnh';
$lang['name_list_disease_icd10'] = 'Tên Bệnh';
$lang['code_list_disease_icd10'] = 'Mã Bệnh';
$lang['note_list_disease_icd10'] = 'Bao Gồm';
$lang['name_list_disease_group_icd10'] = 'Mã Nhóm Bệnh';
$lang['list_disease_icd10'] = 'Danh Mục Bệnh';
$lang['for_list_disease_icd10'] = 'Danh Mục Bệnh';

/* list_group_icd10 */
$lang['title_list_group_icd10'] = 'Danh Mục Nhóm Bệnh';
$lang['name_list_group_icd10'] = 'Tên Nhóm Bệnh';
$lang['code_list_group_icd10'] = 'Mã Nhóm Bệnh';
$lang['name_list_group_chapter_icd10'] = 'Mã Hệ Bệnh';
$lang['list_group_icd10'] = 'Danh Mục Nhóm Bệnh';
$lang['for_list_group_icd10'] = 'Danh Mục Nhóm Bệnh';

/* list_transport_vehicle */
$lang['title_list_transport_vehicle'] = 'Danh Mục Phương Tiện Chuyển Viện';
$lang['name_list_transport_vehicle'] = 'Tên PTCV';
$lang['code_list_transport_vehicle'] = 'Mã PTCV';
$lang['list_transport_vehicle'] = 'Danh Mục Phương Tiện Chuyển Viện';
$lang['for_list_transport_vehicle'] = 'Danh Mục Phương Tiện Chuyển Viện';

/* list_subclinical */
$lang['title_list_subclinical'] = 'Danh Mục Cận Lâm Sàn';
$lang['name_list_subclinical'] = 'Tên Cận Lâm Sàn';
$lang['code_list_subclinical'] = 'Mã Cận Lâm Sàn';
$lang['list_subclinical'] = 'Danh Mục Cận Lâm Sàn';
$lang['for_list_subclinical'] = 'Danh Mục Cận Lâm Sàn';

/* list_medical_supplies */
$lang['title_list_medical_supplies'] = 'Danh Mục Vật Tư Y Tế';
$lang['name_list_medical_supplies'] = 'Tên VTYT';
$lang['code_list_medical_supplies'] = 'Mã VTYT';
$lang['price_list_medical_supplies'] = 'Giá';
$lang['price_foreigner_list_medical_supplies'] = 'Giá Người Nước Ngoài';
$lang['vat_list_medical_supplies'] = 'VAT';
$lang['list_medical_supplies'] = 'Danh Mục Vật Tư Y Tế';
$lang['for_list_medical_supplies'] = 'Danh Mục Vật Tư Y Tế';

/* list_functional_foods */
$lang['title_list_functional_foods'] = 'Danh Mục Thực Phẩm Chức Năng';
$lang['name_list_functional_foods'] = 'Tên TPCN';
$lang['code_list_functional_foods'] = 'Mã TPCN';
$lang['list_functional_foods'] = 'Danh Mục Thực Phẩm Chức Năng';
$lang['for_list_functional_foods'] = 'Danh Mục Thực Phẩm Chức Năng';

/* list_patient_room_type */
$lang['title_list_patient_room_type'] = 'Danh Mục Loại Phòng';
$lang['name_list_patient_room_type'] = 'Tên Loại Phòng';
$lang['code_list_patient_room_type'] = 'Mã Loại Phòng';
$lang['list_patient_room_type'] = 'Danh Mục Loại Phòng';
$lang['for_list_patient_room_type'] = 'Danh Mục Loại Phòng';

/* list_patient_room */
$lang['title_list_patient_room'] = 'Danh Mục Phòng';
$lang['name_list_patient_room'] = 'Tên Phòng';
$lang['code_list_patient_room'] = 'Mã Phòng';
$lang['type_list_patient_room'] = 'Chọn Loại Phòng';
$lang['list_patient_room'] = 'Danh Mục Phòng';
$lang['for_list_patient_room'] = 'Danh Mục Phòng';

/* list_patient_bed */
$lang['title_list_patient_bed'] = 'Danh Mục Giường';
$lang['name_list_patient_bed'] = 'Tên Giường';
$lang['code_list_patient_bed'] = 'Mã Giường';
$lang['name_patient_room_list_patient_bed'] = 'Tên phòng';
$lang['list_patient_bed'] = 'Danh Mục Giường';
$lang['for_list_functional_foods'] = 'Danh Mục Giường';

/* for publish,unpublish */
$lang['event_publish_one']='Bạn có chắc chắn muốn đổi tình trạng của';
$lang['event_publish_one_change']='thành hiển thị?';
$lang['event_publish_more']='Bạn có chắc chắn muốn đổi tình trạng của các';
$lang['event_publish_more_change']='đã được chọn thành hiển thị?';

$lang['event_unpublish_one']='Bạn có chắc chắn muốn đổi tình trạng của';
$lang['event_unpublish_one_change']='thành ẩn?';
$lang['event_unpublish_more']='Bạn có chắc chắn muốn đổi tình trạng của các';
$lang['event_unpublish_more_change']='đã được chọn thành ẩn?';

/* for delete */
$lang['event_delete_one']='Bạn có chắc chắn muốn xóa ';
$lang['event_delete_one_change']='đã chọn?';
$lang['event_delete_more']='Bạn có chắc chắn muốn xóa các';
$lang['event_delete_more_change']='đã được chọn?';

/* list catgory for update,publish,unpublish */
$lang['for_list_nations']='Quốc Gia';
$lang['for_list_foreigner'] = 'Danh Mục Ngoại Kiều';
$lang['for_list_provinces']='Tỉnh Thành';
$lang['for_list_districts']='Quận Huyện';
$lang['for_list_wards']='Phường Xã';
$lang['for_list_titles']='Nghề Nghiệp';
$lang['for_list_departments']='Khoa/Phòng';
$lang['for_list_reason_discharge']='Lý Do Xuất Viện';
$lang['for_list_reason_transfer']='Lý Do Chuyển Viện';
$lang['for_list_materials']='Vật Tư';
$lang['for_list_materials']='Hộ Sinh';
$lang['for_list_midwives']='Khám Chữa Bệnh';
$lang['for_list_group_therapys']='Nhà Cung Cấp';
$lang['for_list_vendors']='Quốc Gia';
$lang['for_list_currencies']='Tiền Tệ';
$lang['for_list_clinic']='Phòng Khám';
$lang['for_list_service']='Dịch Vụ';
$lang['for_list_service_type']='Loại Dịch Vụ'; 
$lang['for_list_service_group']='Nhóm Dịch Vụ'; 
$lang['for_list_package_healthcare_service_type']='Loại Gói Khám Bệnh - Sanh';
$lang['for_list_package_healthcare_service']='Gói Khám Bệnh - Sanh';
$lang['for_list_healthcare_service']='Dịch Vụ Khám Bệnh';
$lang['for_list_medicine']='Danh Mục Thuốc'; 
$lang['for_list_contraindication']='Danh Mục Chống Chỉ Định';
$lang['for_list_insurrance_company'] = 'Danh Mục Công Ty Bảo Hiểm';
$lang['for_list_health_insurrance_card'] = 'Danh Mục Loại Thẻ Bảo Hiểm';
$lang['for_list_supllier'] = 'Danh Mục Nhà Cung Cấp';
