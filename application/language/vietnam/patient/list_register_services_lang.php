<?
$lang['home']            = 'Home';
$lang['breadcrumb_patient'] = 'Bệnh nhân';
$lang['list_register_services'] ='Danh sách đăng kí dịch vụ lễ tân';
$lang['add'] = 'Thêm';
$lang['edit'] = 'Sửa';
$lang['code_titles'] = 'Mã phiếu';
$lang['code_patient'] = 'Mã bệnh nhân';
$lang['name_patient'] = 'Tên bệnh nhân';
$lang['type_services'] = 'Loại dịch vụ';
$lang['reason'] = 'Lý do';
$lang['author'] = 'Người lập phiếu';
$lang['time'] = 'Thời gian thực hiện';
$lang['status'] = 'Trạng thái thực hiện';
$lang['search'] = 'Tìm kiếm';
$lang['datetimepicker'] = 'Ngày thực hiện';

?>

<?
$lang['home']            = 'Home';
$lang['breadcrumb_patient'] = 'Bệnh nhân';
$lang['list_register_services'] ='Đăng kí dịch vụ';
$lang['info_patient'] = 'Thông tin bệnh nhân';
$lang['info_register_services'] = 'Thông tin đăng kí dịch vụ';
$lang['add_patient']= 'Thêm bệnh nhân mới';
$lang['add']= 'Thêm';
$lang['id_patient']= 'Mã bệnh nhân';
$lang['name_patient']= 'Họ tên bệnh nhân';
$lang['birth_day']= 'Ngày sinh';
$lang['marital_status']= 'Tình trạng hôn nhân';
$lang['gender']= 'Giới tính';
$lang['nationality']= 'Quốc tịch';
$lang['identity_card']= 'CMND/Passport';
$lang['address_patient']= 'Địa chỉ';
$lang['email_patient']= 'Email';
$lang['phone_patient']= 'Số điện thoại';
$lang['cell_phone_patient']= 'Điện thoại di động';
$lang['job_patient']= 'Nghê nghiệp';
$lang['execution_time']= 'Thời gian thực hiện';
$lang['at_room']= 'Nơi thực hiện';
$lang['reason_registration']= 'Lý do đăng kí';
$lang['quantity']= 'Số lượng';
$lang['type_services']= 'Loại dịch vụ';
$lang['price_type']= 'Loại giá';
?>