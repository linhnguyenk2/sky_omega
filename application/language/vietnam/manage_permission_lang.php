<?php

$lang['permission'] = 'Phân Quyền';

$lang['add'] = 'Thêm';
$lang['edit'] = 'Sửa';
$lang['delete'] = 'Xóa';
$lang['publish'] = 'Publish';
$lang['unpublish'] = 'UnPublish';

$lang['addnew'] = 'Thêm mới';
$lang['edit_post'] = 'Chỉnh sửa bài';
$lang['save'] = 'Lưu';
$lang['save_change'] = 'Lưu Thay Đổi';
$lang['close'] = 'Đóng';

$lang['required'] = 'Yêu cầu';
$lang['search'] = 'Tìm kiếm';


/*page role*/
$lang['list_role'] = 'Quản Lý Quyền';
$lang['name'] = 'Tên';
$lang['date_create'] = 'Ngày Tạo';
$lang['people_create'] = 'Người Tạo';
$lang['status'] = 'Trạng Thái';
/*page screen*/
$lang['list_screen'] = 'Quản Lý Màn Hình';
$lang['name_router'] = 'Tên Màn Hình';
$lang['name_url'] = 'Mã url';
$lang['router_parent'] = 'Mã Router Cha';
$lang['icon'] = 'Icon';
//$lang['date_create'] = 'Date Create';
//$lang['people_create'] = 'People Create';
//$lang['status'] = 'Status';

/*page role screen*/
$lang['group_role'] = 'Danh Sách Nhóm Quyền';
//$lang['list_screen'] = 'List Screen';
$lang['name_router_main'] = 'Tên Màn Hình Chính';
$lang['view'] = 'Xem';