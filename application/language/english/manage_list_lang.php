<?php

$lang['list'] = 'List';

$lang['add'] = 'Add';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['publish'] = 'Publish';
$lang['unpublish'] = 'UnPublish';

$lang['addnew'] = 'Add New';
$lang['edit_post'] = 'Edit Post';
$lang['save'] = 'Save';
$lang['save_change'] = 'Save changes';
$lang['close'] = 'Close';

$lang['required'] = 'Required';
$lang['search'] = 'Search';

$lang['date_create'] = 'Date Create';
$lang['people_create'] = 'People Create';
$lang['status'] = 'Status';
$lang['order'] = 'Order';

/* list nation */
$lang['title_list_nations'] = 'List Nation';
$lang['name_nation'] = 'Name Nation';
$lang['code_nation'] = 'Code Nation';
$lang['list_nation'] = 'List Nation';

/* list provinces */
$lang['title_list_nations'] = 'List provinces';
$lang['name_provinces'] = 'Name Provinces';
$lang['list_provinces'] = 'List Provinces';
$lang['code_provinces'] = 'Mã Thành Phố';
$lang['name_nation_provinces'] = 'Name Nation';

/* list districts */
$lang['title_list_districts'] = 'List districts';
$lang['code_districts'] = 'Code Districts';
$lang['name_districts'] = 'Name Districts';
$lang['list_districts'] = 'List Districts';
/* list towns */
$lang['title_list_towns'] = 'List towns';
$lang['name_towns'] = 'Name Towns';
$lang['list_towns'] = 'List Towns';
/* list titles */
$lang['title_list_titles'] = 'List title';
$lang['name_titles'] = 'Name Titles';
$lang['list_titles'] = 'List Titles';

/* list departments */
$lang['title_list_departments'] = 'List departments';
$lang['list_departments'] = 'List Departments';
$lang['name_departments']='Name Department';
$lang['user_id_leader']='Code User Leader';

/* reason_for_discharge */
$lang['title_reason_for_discharge'] = 'List reason for discharge';
$lang['reason_for_discharge']='Reason for Discharge';
$lang['code_discharge']='Code Discharge';
$lang['name']='Name';
$lang['note']='Note';
$lang['date']='Date';
$lang['code']='Code';

/* reason_for_transfer */
$lang['title_reason_for_transfer'] = 'List reason for transfer';
$lang['reason_for_transfer']='Reason for transfer';
$lang['code_transfer']='Code Transfer';

/* list_materials */
$lang['title_list_materials'] = 'List materials';
$lang['list_materials']='List Of Materials';
$lang['code_materia']='Code Material';
$lang['name_materia']='Name Material';
$lang['dprice']='Price';
$lang['dprice_eng']='Price Eng';
$lang['muser']='User';

/* list_midwives */
$lang['title_list_midwives'] = 'List midwives';
$lang['list_midwives']='Midwives';
$lang['code_midwive']='Code Midwive';
$lang['full_name']='Full Name';
$lang['at_department']='Department';

/* list_group_therapys */
$lang['title_list_group_therapys'] = 'List therapys';
$lang['list_group_therapys']='List Group Therapys';
$lang['code_group_therapy']='Code KCB';
$lang['name_group_therapy']='Name KCB';

/* list vendor */
$lang['title_list_vendor'] = 'List vendor';
$lang['list_vendor']='List Vendor';
$lang['list_vd_vendor_code']='eng Mã Nhà Cung Cấp';
$lang['list_vd_second_code']='eng Mã Phụ';
$lang['list_vd_vendor_name']='eng Tên Nhà Cung Cấp';
$lang['list_vd_name_quick']='eng Tên Tìm Nhanh';
$lang['list_vd_address']='eng Địa Chỉ';
$lang['list_vd_city_province']='eng Tỉnh/Thành Phố';
$lang['list_vd_nation']='eng Quốc Gia';
$lang['list_vd_phone']='eng Điện Thoại';
$lang['list_vd_fax']='eng Số Fax';
$lang['list_vd_group_ncc']='eng Nhóm NCC';
$lang['list_vd_group_settlement']='eng Nhóm Định Khoản';
$lang['list_vd_group_tax']='eng Nhóm thuế';
$lang['list_vd_currency_code']='eng Mã tiền tệ';
$lang['list_vd_credit_limit']='eng Giới Hạn Tín Dụng';
$lang['list_vd_tax_code']='eng Mã Số Thuế';
$lang['list_vd_email']='eng E-mail';
$lang['list_vd_website']='eng WEBSITE';
$lang['list_vd_contact']='eng Người Liên Hệ';
$lang['list_vd_contact_phone']='eng Số ĐT Người Liên Hệ';
$lang['list_vd_date_created']='eng Ngày Tạo';
$lang['list_vd_user']='eng Người Dùng';
$lang['list_vd_bank_account']='eng Mã tài khoản ngân hàng';
$lang['list_vd_bank_name']='eng Tên ngân hàng';
$lang['list_vd_bank_address']='eng Địa chỉ ngân hàng';
$lang['list_vd_modules']='MODULES';

/* list currency */
$lang['title_list_currencies'] = 'List curency';
$lang['list_curr_name']='List Currency';
$lang['list_curr_code']='Code Currency';
$lang['list_code_from']='Code Currency';
$lang['list_begin_date']='Date Start';
$lang['list_code_to']='Code To Currency';
$lang['list_rate']='Rate';

$lang['yes']='Yes';
$lang['no']='No';

/* for publish,unpublish */
$lang['event_publish_one']='eng Bạn có chắc chắn muốn đổi tình trạng của';
$lang['event_publish_one_change']='eng thành hiển thị?';
$lang['event_publish_more']='eng Bạn có chắc chắn muốn đổi tình trạng của các';
$lang['event_publish_more_change']='eng đã được chọn thành hiển thị?';

$lang['event_unpublish_one']='eng Bạn có chắc chắn muốn đổi tình trạng của';
$lang['event_unpublish_one_change']='eng thành ẩn?';
$lang['event_unpublish_more']='eng Bạn có chắc chắn muốn đổi tình trạng của các';
$lang['event_unpublish_more_change']='eng đã được chọn thành ẩn?';

/* for delete */
$lang['event_delete_one']='eng Bạn có chắc chắn muốn xóa ';
$lang['event_delete_one_change']='eng đã chọn?';
$lang['event_delete_more']='eng Bạn có chắc chắn muốn xóa các';
$lang['event_delete_more_change']='eng đã được chọn?';

/* list catgory for update,publish,unpublish */
$lang['for_list_nations']='eng Quốc Gia';
$lang['for_list_provinces']='eng Tỉnh Thành';
$lang['for_list_districts']='eng Quận Huyện';
$lang['for_list_towns']='eng Phường Xã';
$lang['for_list_titles']='eng Chức Vụ/Nghề Nghiệp';
$lang['for_list_departments']='eng Phòng Ban';
$lang['for_reason_for_discharge']='eng Lý Do Xuất Viện';
$lang['for_reason_for_transfer']='eng Lý Do Chuyển Viện';
$lang['for_list_materials']='eng Vật Tư';
$lang['for_list_materials']='eng Hộ Sinh';
$lang['for_list_midwives']='eng Khám Chữa Bệnh';
$lang['for_list_group_therapys']='eng Nhà Cung Cấp';
$lang['for_list_vendors']='eng Quốc Gia';
$lang['for_list_currencies']='eng Tiền Tệ';