<?php

$lang['add'] = 'Add';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['publish'] = 'Publish';
$lang['unpublish'] = 'UnPublish';

$lang['addnew'] = 'Add New';
$lang['edit_post'] = 'Edit Post';
$lang['save'] = 'Save';
$lang['save_change'] = 'Save changes';
$lang['close'] = 'Close';

$lang['required'] = 'Required';
$lang['search'] = 'Search';

/* manage user */
$lang['manage_user'] = 'Manage User';
$lang['list_user'] = 'List User';
$lang['role'] = 'Role';
$lang['title'] = 'Title';
$lang['name'] = 'Name';
$lang['email'] = 'Email';
$lang['phone_number'] = 'Phone Number';
$lang['avatar'] = 'Avatar';
$lang['status'] = 'Status';
$lang['change_pasword'] = 'Change Password';
$lang['pasword'] = 'Password';