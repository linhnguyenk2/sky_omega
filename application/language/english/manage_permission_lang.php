<?php

$lang['permission'] = 'Permission';

$lang['add'] = 'Add';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['publish'] = 'Publish';
$lang['unpublish'] = 'UnPublish';

$lang['addnew'] = 'Add New';
$lang['edit_post'] = 'Edit Post';
$lang['save'] = 'Save';
$lang['save_change'] = 'Save changes';
$lang['close'] = 'Close';

$lang['required'] = 'Required';
$lang['search'] = 'Search';


/*page role*/
$lang['list_role'] = 'List Role';
$lang['name'] = 'Name';
$lang['date_create'] = 'Date Create';
$lang['people_create'] = 'People Create';
$lang['status'] = 'Status';
/*page screen*/
$lang['list_screen'] = 'List Screen';
$lang['name_router'] = 'Name Router';
$lang['name_url'] = 'Name Url';
$lang['router_parent'] = 'Router Parent';
$lang['icon'] = 'Icon';
//$lang['date_create'] = 'Date Create';
//$lang['people_create'] = 'People Create';
//$lang['status'] = 'Status';

/*page role screen*/
$lang['group_role'] = 'Group Role';
//$lang['list_screen'] = 'List Screen';
$lang['name_router_main'] = 'Name Screen Main';
$lang['view'] = 'View';