<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_37 pregnancy changes in health book Model
 *
 * @since v.1.0
 */
class API_P_37 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id',
            'health_book_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Pregnancy_Changes_In_Health_Book_Model");

        parent::__construct();
    }
}