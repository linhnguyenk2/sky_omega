<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_20 Tpcn In Tpcn Mp Vtyt Paper Model
 *
 * @since v.1.0
 */
class API_P_20 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'functional_food_id',
            'tpcn_mp_vtyt_paper_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));


        $this->setMainModelClassName("Product_In_Tpcn_Mp_Vtyt_Paper_Model");

        parent::__construct();
    }
}