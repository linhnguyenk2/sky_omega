<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_13 medical examination form
 *
 * @since v.1.0
 */
class API_P_13 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'medical_record_id',
            'patient_id',
            'staff_id',
            'gynecolog_form_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Medical_Examination_Form_Model");

        parent::__construct();
    }
}