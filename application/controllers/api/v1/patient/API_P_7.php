<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_7 User
 *
 * @since v.1.0
 */
class API_P_7 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array(
		    'username',
            'password',
            'fullname',
            'type'
        ));
		$this->setMainModelClassName("User_Model");

		parent::__construct();
	}
}