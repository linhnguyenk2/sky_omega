<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_41 track_height_weight in health book Model
 *
 * @since v.1.0
 */
class API_P_41 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id',
            'health_book_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Track_Height_Weight_In_Health_Book_Model");

        parent::__construct();
    }
}