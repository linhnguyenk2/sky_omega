<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_17 Prescription Paper
 *
 * @since v.1.0
 */
class API_P_17 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id',
            'staff_id',
            'medical_examination_form_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Prescription_Paper_Model");

        parent::__construct();
    }
}