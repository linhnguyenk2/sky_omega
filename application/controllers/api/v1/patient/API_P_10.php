<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_10 Analysis Blood Paper
 *
 * @since v.1r.0
 */
class API_P_10 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());

        $this->setListReferredColumn(array(
            'patient_id'
        ));

		$this->setMainModelClassName("Analysis_Blood_Paper_Model");

		parent::__construct();
	}
}