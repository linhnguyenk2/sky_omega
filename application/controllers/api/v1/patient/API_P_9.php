<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_9 Medical Record
 *
 * @since v.1.0
 */
class API_P_9 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Medical_Record_Model");

        parent::__construct();
    }
}