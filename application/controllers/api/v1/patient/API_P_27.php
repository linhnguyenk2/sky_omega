<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_27 Menopause Book Model
 *
 * @since v.1.0
 */
class API_P_27 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Menopause_Book_Model");

        parent::__construct();
    }
}