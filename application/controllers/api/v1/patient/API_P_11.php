<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_11 Analysis Urine Paper
 *
 * @since v.1.0
 */
class API_P_11 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());

        $this->setListReferredColumn(array(
            'patient_id'
        ));

		$this->setMainModelClassName("Analysis_Urine_Paper_Model");

		parent::__construct();
	}
}