<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_18 Medicine In Prescription Paper
 *
 * @since v.1.0
 */
class API_P_18 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'prescription_paper_id',
            'product_id',
        ));

        $this->setListReferredColumn(array(
            'patient_id',
            'prescription_paper_id',
            'origin_medicine_id',
            'parent_unit_medicine_id',
            'sub_unit_id',
            'medicine_id',
            'list_appointed_in_medicine',
            'list_not_combine_medicine',
            'list_unit_medicine',
            'list_contraindication_in_medicine'
        ));

        $this->setListSelfReferredColumn(array(
            'not_combine_medicine_id'
        ));

        $this->setMainModelClassName("Medicine_In_Prescription_Paper_Model");

        parent::__construct();
    }
}