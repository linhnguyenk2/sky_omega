<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_40 medical examination form in health book Model
 *
 * @since v.1.0
 */
class API_P_40 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id',
            'doctor_id',
            'health_book_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Medical_Examination_Form_In_Health_Book_Model");

        parent::__construct();
    }
}