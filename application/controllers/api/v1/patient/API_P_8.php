<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_8 Staff
 *
 * @since v.1.0
 */
class API_P_8 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array(
            'fullname',
            'type'
        ));
		$this->setMainModelClassName("Staff_Model");

		parent::__construct();
	}
}