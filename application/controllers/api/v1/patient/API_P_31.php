<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_31 registration Form Model
 *
 * @since v.1.0
 */
class API_P_31 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id',
            'patient_insurrance_id',
            'patient_company_id',
            'survival_information_id',
            'appointment_form_id'
        ));

        $this->setMainModelClassName("Registration_Form_Model");

        parent::__construct();
    }
}