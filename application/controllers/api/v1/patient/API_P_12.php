<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_12 Analysis Paper
 *
 * @since v.1.0
 */
class API_P_12 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id',
            'staff_id',
            'medical_record_id',
            'analysis_blood_paper_id',
            'analysis_urine_paper_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Analysis_Paper_Model");

        parent::__construct();
    }
}