<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PR_1 Patients
 *
 * @since v.1.0
 */
class API_P_1 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'fullname',
            'type'
        ));

        $this->setListReferredColumn(array(
            'list_patient_emergency_contact',
            'patient_id'

        ));
        $this->setMainModelClassName("Patients_Model");

        parent::__construct();
    }
}