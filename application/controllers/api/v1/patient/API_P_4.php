<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PR_4 Patient Company
 *
 * @since v.1.0
 */
class API_P_4 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Patient_Company_Model");

        parent::__construct();
    }
}