<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PR_3 Patient Emergency Contact
 *
 * @since v.1.0
 */
class API_P_3 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id',
            'patient_emergency_id',
            'list_patient_emergency_contact'
        ));

        $this->setMainModelClassName("Patient_Emergency_Contact_Model");

        parent::__construct();
    }
}