<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_24 Specify Service Form Model
 *
 * @since v.1.0
 */
class API_P_24 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id',
            // Able = null created by reception
            //'medical_examination_form_id',
            'service_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Specify_Service_Paper_Model");

        parent::__construct();
    }
}