<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_32 disease icd10 in gynecolog book
 *
 * @since v.1.0
 */
class API_P_32 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'disease_icd10_id',
            'gynecolog_book_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Disease_Icd10_In_Gynecolog_Book_Model");

        parent::__construct();
    }
}