<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_29 Survival Information  Model
 *
 * @since v.1.0
 */
class API_P_29 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array());

        $this->setMainModelClassName("Survival_Information_Model");

        parent::__construct();
    }
}