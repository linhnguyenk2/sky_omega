<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_35 artificial examination form Model
 *
 * @since v.1.0
 */
class API_P_35 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id',
            'medical_record_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Artificial_Examination_Form_Model");

        parent::__construct();
    }
}