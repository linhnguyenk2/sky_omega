<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_28 Menopause Form Model
 *
 * @since v.1.0
 */
class API_P_28 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id',
            'medical_record_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Menopause_Form_Model");

        parent::__construct();
    }
}