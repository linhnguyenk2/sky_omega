<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_P_19 Tpcn Mp Vtyt Paper Model
 *
 * @since v.1.0
 */
class API_P_19 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'patient_id',
            'staff_id',
            'medical_examination_form_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Tpcn_Mp_Vtyt_Paper_Model");

        parent::__construct();
    }
}