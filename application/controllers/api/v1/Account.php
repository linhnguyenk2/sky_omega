<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account extends ApiController
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('User_Action_model', 'model_user_api');
    }

    // home view
    public function basic()
    {
        $actiion = $this->input->post('action', TRUE);
        if ($actiion == 'get_role_department') {
            $data['role'] = $this->model_user_api->getRoles();
            $data['department'] = $this->model_user_api->getDepartments();
            echo json_encode($data);
        }
    }

    public function get()
    {
        error_reporting(E_ALL);
        $type_submit = $_REQUEST['type_submit'];
        
        switch ($type_submit) {
            case 'list_users':
                $columns = array(
                    array(
                        'db' => 'id',
                        'dt' => 0
                    ),
                    array(
                        'db' => 'id_role',
                        'dt' => 2
                    ),
                    // array('db' => 'ltId', 'dt' => 3),
                    array(
                        'db' => 'id_list_departments',
                        'dt' => 3
                    ),
                    array(
                        'db' => 'name',
                        'dt' => 4
                    ),
                    array(
                        'db' => 'email',
                        'dt' => 5
                    ),
                    array(
                        'db' => 'phone',
                        'dt' => 6
                    ),
                    // array('db' => 'uPassword', 'dt' => 7),
                    array(
                        'db' => 'avatar',
                        'dt' => 7
                    ),
                    array(
                        'db' => 'stat',
                        'dt' => 8
                    )
                );
                
                break;
            default:
                break;
        }
        
        $primaryKey = $columns[0]['db'];
        $table = 'sih_users';
        // SQL server connection information
        $sql_details = array(
            // 'user' => 'root',
            // 'pass' => '',
            // 'db' => 'sihospital',
            // 'host' => 'localhost'
        );
        
        // var_dump($columns);die;
        
        // /die;
        $data = $this->model_user_api->getDataTable($_POST, $sql_details, $table, $primaryKey, $columns);
        $data = $this->utf8ize($data);
        echo json_encode($data, true);
        die();
    }

    private function utf8ize($d)
    {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = $this->utf8ize($v);
            }
        } else if (is_string($d)) {
            return utf8_encode($d);
        }
        return $d;
    }

    public function add()
    {
        // var_dump($_POST);die;
        $type_submit = $_POST['type_submit'];
        $table_name = 'sih_users';
        
        switch ($type_submit) {
            case 'list_users':
                $list_colum = [
                    'id_role',
                    'id_list_departments',
                    'name',
                    'email',
                    'phone',
                    'password',
                    'avatar',
                    'stat'
                ];
                $list_data = $_POST['input'];
                $list_data[5] = md5($list_data[5]);
                break;
            
            default:
                break;
        }
        
        $data = $this->model_user_api->add_record($list_colum, $list_data, $table_name);
    }

    public function edit()
    {
        $type_submit = $_POST['type_submit'];
        $table_name = 'sih_users';
        $vl_id_column = $_POST['input']['0'];
        $vl_id = ''; // name of id every table
        unset($_POST['input']['0']);
        $_POST['input'] = array_values($_POST['input']);
        $list_data = $_POST['input'];
        $name_id = 'id';
        switch ($type_submit) {
            
            case 'list_users':
                
                // do have password empty push to save
                if (empty($list_data[5])) {
                    $list_colum = [
                        'id_role',
                        'id_list_departments',
                        'name',
                        'email',
                        'phone',
                        'avatar',
                        'stat'
                    ];
                    unset($list_data[5]);
                    $list_data = array_values($list_data);
                } else {
                    $list_colum = [
                        'id_role',
                        'id_list_departments',
                        'name',
                        'email',
                        'phone',
                        'password',
                        'avatar',
                        'stat'
                    ];
                    $list_data[5] = md5($list_data[5]);
                }
                
                break;
            default:
                break;
        }
        
        $data = $this->model_user_api->edit_record($name_id, $list_colum, $vl_id_column, $list_data, $table_name);
        // update name or link avata if change for show index if edit profile of user
        $user = $this->session->userdata('user');
        if ($vl_id_column == $user['uId']) {
            $user['uName'] = $_POST['input'][2];
            $user['uEmail'] = $_POST['input'][3];
            $user['uAvatar'] = $_POST['input'][6];
            $this->session->set_userdata('user', $user);
        }
    }

    public function delete()
    {
        
        // var_dump($_POST);die;
        // 1: need check crfs for submiit
        // 2: need chek login or check permission for action
        if (isset($_POST['type_submit']) && $_POST['input']) {
            $list_name = $_POST['type_submit'];
            $table_name = 'sih_users';
            
            $name_id = 'id';
            
            $value_input = $_POST['input'];
            $value_input = explode(',', $value_input);
            
            $data = $this->model_user_api->delete_record($name_id, $value_input, $table_name);
        } else {
            // echo 'Not correct input';
            $return['status'] = false;
            $return['message'] = 'Input not correct ID/some thing';
            echo json_encode($return);
        }
    }

    public function push()
    {
        
        // var_dump($_POST);die;
        // 1: need check crfs for submiit
        // 2: need chek login or check permission for action
        if (isset($_POST['type_submit']) && $_POST['input']) {
            $category_name = $_POST['type_submit'];
            $table_name = 'sih_users';
            $value_input = $_POST['input'];
            $value_input = explode(',', $value_input);
            $name_id = 'u';
            
            $data = $this->model_user_api->push_record($name_id, $value_input, $table_name);
        } else {
            echo 'Not correct input';
        }
    }

    public function unpush()
    {
        
        // var_dump($_POST);die;
        // 1: need check crfs for submiit
        // 2: need chek login or check permission for action
        if (isset($_POST['type_submit']) && $_POST['input']) {
            $category_name = $_POST['type_submit'];
            $table_name = 'sih_users';
            $value_input = $_POST['input'];
            $value_input = explode(',', $value_input);
            $name_id = 'u';
            $data = $this->model_user_api->unpush_record($name_id, $value_input, $table_name);
        } else {
            echo 'Not correct input';
        }
    }
}