<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends ApiController
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model', 'user');
        $this->load->model('Util_model', 'util');
    }

    public function signIn()
    {
        $info = $this->user->signIn($_POST['email'], $_POST['password']);
        $jsonRes=[];
        if (is_null($info) == FALSE) {
            $this->session->set_userdata('user', $info);
            $jsonRes = array(
                "info" => $info,
                "ok" => 1
            );
        } else {
            $jsonRes = array(
                "info" => $info,
                "ok" => 0
            );
        }
        echo json_encode($jsonRes);
    }

    public function signOut()
    {
        $this->session->sess_destroy();
        echo json_encode(array(
            "ok" => 1
        ));
    }

    public function changePass()
    {
        $this->user->changePass($_POST['password']);
    }

    public function genPass()
    {
        $res = $this->db->query("select * from sih_users
                              where (uEmail={$this->db->escape($_POST['email'])}
                                    or uPhone={$this->db->escape($_POST['email'])})
                                and uStat='O'
                              limit 1")->result();
        
        foreach ($res as $row) {
            $pass = substr(str_shuffle('0123456789'), 0, 4);
            $this->db->query("update sih_users
                                 set uPassChanged=0, uPasswordNew={$this->db->escape($pass)}
                               where (uEmail={$this->db->escape($_POST['email'])}
                                      or uPhone={$this->db->escape($_POST['email'])})
                                 and uStat='O'
                               limit 1");
            
            $this->sendEmail($_POST['email'], 4, $row->uName, $row->uEmail, $pass);
            echo json_encode(array(
                "ok" => 1
            ));
            return;
        }
        
        $jsonRes = array(
            "ok" => 0
        );
        echo json_encode($jsonRes);
    }

    public function get_user_all()
    {
        echo 'get user all';
        die();
    }

    public function get_user($id)
    {
        $list_user = $this->getUserData();
        $data_send = isset($list_user[$id]) ? $list_user[$id] : null;
        $this->printJson($data_send);
    }

    public function post_user()
    {
        echo 'post user';
        die();
    }

    public function put_user()
    {
        echo 'put user';
        die();
    }

    public function delete_user($id)
    {
        echo 'delete user';
        die();
    }

    private function getUserData()
    {
        $list_user = [];
        $list_user[1] = [
            'id' => 1,
            'name' => 'van a'
        ];
        $list_user[2] = [
            'id' => 2,
            'name' => 'van b'
        ];
        $list_user[3] = [
            'id' => 3,
            'name' => 'van c'
        ];
        return $list_user;
    }
}