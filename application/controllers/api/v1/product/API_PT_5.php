<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PT_5 Raw Material Product
 *
 * @since v.1.0
 */
class API_PT_5 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'product_group_id'
        ));

        $this->setListSelfReferredColumn(array(
            'product_id'
        ));

        $this->setMainModelClassName("Raw_Material_Product_Model");

        parent::__construct();
    }
}