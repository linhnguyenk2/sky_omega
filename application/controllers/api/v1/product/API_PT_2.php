<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PT_2 product group
 *
 * @since v.1.0
 */
class API_PT_2 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array('type'));

        $this->setMainModelClassName("Product_Group_Model");

        parent::__construct();
    }
}