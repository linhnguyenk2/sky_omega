<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PT_3 Unit
 *
 * @since v.1.0
 */
class API_PT_3 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array());

        $this->setMainModelClassName("Unit_Model");

        parent::__construct();
    }
}