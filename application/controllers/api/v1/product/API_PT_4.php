<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PT_4 product unit exchange
 *
 * @since v.1.0
 */
class API_PT_4 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'unit_exchange_id', 'product_id'
        ));

        $this->setListSelfReferredColumn(array(
            'product_id'
        ));

        $this->setMainModelClassName("Product_Unit_Exchange_Model");

        parent::__construct();
    }
}