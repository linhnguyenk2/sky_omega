<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PT_8 chemical product Product
 *
 * @since v.1.0
 */
class API_PT_8 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListReferredColumn(array(
            'product_group_id',
            'unit_id',
            'medicine_id',
            'function_food_id',
            'medical_supply_id',
            'chemical_id',
            'cosmetic_id',
            'raw_material_id',
            'food_id',
            'nation_id',
            'supplier_id'
        ));

        $this->setListSelfReferredColumn(array(
            'origin_medicine_id',
            'not_combine_medicine_id',
            'product_id'
        ));

        $this->setMainModelClassName("Chemical_Product_Model");

        parent::__construct();
    }
}