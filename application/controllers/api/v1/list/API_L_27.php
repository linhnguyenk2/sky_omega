<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_27 Type Medical Record  Model
 *
 * @since v.1.0
 */
class API_L_27 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Type_Medical_Record_Model");

		parent::__construct();
	}
}