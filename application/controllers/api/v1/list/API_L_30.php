<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_30 Disease ICD10
 *
 * @since v.1.0
 */
class API_L_30 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('group_icd10_id'));
		$this->setMainModelClassName("Disease_ICD10_Model");

		parent::__construct();
	}
}