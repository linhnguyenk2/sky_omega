<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_40 Nations Foreigners Model
 *
 * @since v.1.0
 */
class API_L_40 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Nations_Foreigners_Model");

		parent::__construct();
	}
}