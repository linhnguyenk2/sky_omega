<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_L_35 Functional_Food_Product_Model
 *
 * @since v.1.0
 */
class API_L_35 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Functional_Food_Product_Model");

		parent::__construct();
	}
}