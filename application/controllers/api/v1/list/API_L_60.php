<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_60 Currencies
 *
 * @since v.1.0
 */
class API_L_60 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Currencies_Model");

		parent::__construct();
	}
}