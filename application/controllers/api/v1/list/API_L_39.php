<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_39 Patient Bed Model
 *
 * @since v.1.0
 */
class API_L_39 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('patient_room_id'));
		$this->setMainModelClassName("Patient_Bed_Model");

		parent::__construct();
	}
}