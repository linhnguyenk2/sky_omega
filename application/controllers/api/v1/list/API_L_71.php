<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_71 folks
 *
 * @since v.1.0
 */
class API_L_71 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array());
        $this->setMainModelClassName("Folks_Model");

        parent::__construct();
    }
}