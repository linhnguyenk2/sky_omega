<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_46 Package Healthcare Service Model
 *
 * @since v.1.0
 */
class API_L_46 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('package_healthcare_service_type_id'));
		$this->setMainModelClassName("Package_Healthcare_Service_Model");

		parent::__construct();
	}
}