<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_55 Titles
 *
 * @since v.1.0
 */
class API_L_55 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Titles_Model");

		parent::__construct();
	}
}