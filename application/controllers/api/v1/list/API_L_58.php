<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_58 Reason Transfer
 *
 * @since v.1.0
 */
class API_L_58 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Reason_Transfer_Model");

		parent::__construct();
	}
}