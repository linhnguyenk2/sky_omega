<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_L_34 Medical Supplies
 *
 * @since v.1.0
 */
class API_L_34 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Medical_Supplies_Model");

		parent::__construct();
	}
}