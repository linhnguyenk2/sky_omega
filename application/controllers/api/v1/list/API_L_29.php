<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_29 Group ICD10
 *
 * @since v.1.0
 */
class API_L_29 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('chapter_icd10_id'));
		$this->setMainModelClassName("Group_ICD10_Model");

		parent::__construct();
	}
}