<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_L_22 Insurrance Company
 *
 * @since v.1.0
 */
class API_L_22 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('name'));
		$this->setMainModelClassName("Insurrance_Company_Model");

		parent::__construct();
	}
}