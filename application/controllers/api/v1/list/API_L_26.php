<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_26 Wards
 *
 * @since v.1.0
 */
class API_L_26 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('id_district'));
		$this->setMainModelClassName("Ward_Model");

		parent::__construct();
	}
}
