<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_57 Reason Discharge
 *
 * @since v.1.0
 */
class API_L_57 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Reason_Discharge_Model");

		parent::__construct();
	}
}