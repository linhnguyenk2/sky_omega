<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_54 Provinces
 *
 * @since v.1.0
 */
class API_L_54 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('id_nation'));
		$this->setMainModelClassName("Provinces_Model");

		parent::__construct();
	}
}