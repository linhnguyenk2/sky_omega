<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_38 Patient Room Model
 *
 * @since v.1.0
 */
class API_L_38 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('patient_room_type_id'));
		$this->setMainModelClassName("Patient_Room_Model");

		parent::__construct();
	}
}