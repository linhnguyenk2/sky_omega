<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_L_28 Chapter ICD10
 *
 * @since v.1.0
 */
class API_L_28 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Chapter_ICD10_Model");

		parent::__construct();
	}
}