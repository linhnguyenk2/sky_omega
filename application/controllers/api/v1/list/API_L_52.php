<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_52 Units
 *
 * @since v.1.0
 */
class API_L_52 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Unit_Model");

		parent::__construct();
	}
}