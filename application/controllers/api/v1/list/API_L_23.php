<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_23 Health Insurance Card
 *
 * @since v.1.0
 */
class API_L_23 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('insurance_company_id', 'name'));
		$this->setMainModelClassName("Health_Insurance_Card_Model");

		parent::__construct();
	}
}
