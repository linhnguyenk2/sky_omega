<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_66 Healthcare
 *
 * @since v.1.0
 */
class API_L_66 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('healthcare_type_id'));
		$this->setMainModelClassName("Healthcare_Model");

		parent::__construct();
	}
}