<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_63 Package Healthcare Service Type
 *
 * @since v.1.0
 */
class API_L_63 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Package_Healthcare_Service_Type_Model");

		parent::__construct();
	}
}