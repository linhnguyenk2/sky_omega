<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_64 Healthcare Group
 *
 * @since v.1.0
 */
class API_L_64 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Healthcare_Group_Model");

		parent::__construct();
	}
}