<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_53 Nations
 *
 * @since v.1.0
 */
class API_L_53 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Nations_Model");

		parent::__construct();
	}
}