<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_L_31 Transport Vehicle
 *
 * @since v.1.0
 */
class API_L_31 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Transport_Vehicle_Model");

		parent::__construct();
	}
}