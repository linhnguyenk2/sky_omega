<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_42 Appointed In Medicine Model
 *
 * @since v.1.0
 */
class API_L_42 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('medicine_id'));

        $this->setListReferredColumn(array(
            'origin_medicine_id',
            'parent_unit_medicine_id',
            'sub_unit_id',
            //'not_combine_medicine_id',
            'medicine_id',
            'list_appointed_in_medicine',
            'list_not_combine_medicine',
            'list_unit_medicine',
            'list_contraindication_in_medicine'
        ));

		$this->setMainModelClassName("Appointed_In_Medicine_Model");

		parent::__construct();
	}
}