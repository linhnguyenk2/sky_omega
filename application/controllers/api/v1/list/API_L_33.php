<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_L_33 contraindication
 *
 * @since v.1.0
 */
class API_L_33 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Contraindication_Model");

		parent::__construct();
	}
}