<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_25 Districts
 *
 * @since v.1.0
 */
class API_L_25 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('id_provinces'));
		$this->setMainModelClassName("District_Model");

		parent::__construct();
	}
}