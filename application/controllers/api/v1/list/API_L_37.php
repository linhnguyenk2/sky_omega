<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_37 Patient Room Type Model
 *
 * @since v.1.0
 */
class API_L_37 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Patient_Room_Type_Model");

		parent::__construct();
	}
}