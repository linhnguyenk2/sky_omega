<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_56 Departments
 *
 * @since v.1.0
 */
class API_L_56 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Departments_Model");

		parent::__construct();
	}
}