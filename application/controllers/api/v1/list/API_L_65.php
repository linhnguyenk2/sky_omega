<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_65 Healthcare Type
 *
 * @since v.1.0
 */
class API_L_65 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('healthcare_group_id'));
		$this->setMainModelClassName("Healthcare_Type_Model");

		parent::__construct();
	}
}