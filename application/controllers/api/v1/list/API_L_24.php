<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_L_24 Supplier
 *
 * @since v.1.0
 */
class API_L_24 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Supplier_Model");

		parent::__construct();
	}
}