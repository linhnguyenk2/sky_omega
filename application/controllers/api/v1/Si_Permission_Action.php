<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Si_Permission_Action extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Datatable_model_action', 'datatable_model');
        $this->load->model('Permission_model', 'permiossion_model');
    } 

    public function get() { 
        //error_reporting(E_ALL);
        //$name_cate = $_REQUEST['category_name'];
        $name_router = $this->getNameRoute();
        //var_dump($name_router);die;
        $table = '';

        switch ($name_router) {
            case 'permission_role':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'name', 'dt' => 2),
                    array('db' => 'created_at', 'dt' => 3),
                    array('db' => 'created_by', 'dt' => 4),
                    array('db' => 'stat', 'dt' => 5),
                );
                $table = 'sih_roles';
                break;
            case 'permission_screen':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'title', 'dt' => 2),
                    array('db' => 'route', 'dt' => 3),
                    array('db' => 'routed_id', 'dt' => 4),
                    array('db' => 'ico', 'dt' => 5),
                    array('db' => 'created_at', 'dt' => 6),
                    array('db' => 'created_by', 'dt' => 7),
                    array('db' => 'stat', 'dt' => 8),
                );
                $table = 'sih_screens';
                break;
            // case 'permission_role_screen':
            //     $columns = array(
            //         array('db' => 'rsId', 'dt' => 0),
            //         array('db' => 'rId', 'dt' => 2),
            //         array('db' => 'sId', 'dt' => 3),
            //         array('db' => 'rsCreatedAt', 'dt' => 4),
            //         array('db' => 'rsCreatedBy', 'dt' => 5),
            //         array('db' => 'rsStat', 'dt' => 6),
            //     );
            //     $table ='sih_role_screens';
            //     break;


            default:
                break;
        }


        $primaryKey = $columns[0]['db'];
        //$table = 'sih_category_n';
        // SQL server connection information
        $sql_details = array(
            // 'user' => 'root',
            // 'pass' => '',
            // 'db' => 'sihospital',
            // 'host' => 'localhost'
        );

        //var_dump($columns);die;
        ///die;
        $data = $this->datatable_model->getDataTable($_POST, $sql_details, $table, $primaryKey, $columns);
        //var_dump( $data);die;
        //$data = $this->utf8ize($data);

        echo json_encode($data, true);
        die;
    }

    public function role_screen_get() {
        $action  =$_POST['action'];
        switch ($action) {
            case 'get_role_group':
                if (isset($_POST['id_role'])) {
                    $role_id = $_POST['id_role'];
                    $return = $this->permiossion_model->db_role_get_list_screen($role_id);
                    echo json_encode($return);
                    die;
                }
                break;
            case 'get_list_main_screen': //for table
                //get list menu and title show
                //$return = $this->permiossion_model->db_get_screen_main();
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'title', 'dt' => 1),
                    array('db' => 'route', 'dt' => 2),
                    array('db' => 'routed_id', 'dt' => 3),
                    // array('db' => 'sCreatedAt', 'dt' => 5),
                    // array('db' => 'sCreatedBy', 'dt' => 6),
                    // array('db' => 'sStat', 'dt' => 7),
                );
                // sId,sTitle,sRoute,sRouteId

                //$return = $this->permiossion_model->db_get_screen_main();
                //var_dump($return);die;

                $table = 'sih_screens';
                $primaryKey = $columns[0]['db'];
                $sql_details=[];
                $where_in = " `route` IS NOT NULL AND `routed_id` !='0' AND `routed_id` NOT LIKE '%!_%' ESCAPE '!' AND `stat` = '1'";
                //var_dump( $sql_details, $table, $primaryKey, $columns,$where_in);die;
                $data = $this->datatable_model->getDataTable($_POST, $sql_details, $table, $primaryKey, $columns,$where_in);
                //var_dump($data);die;
                $data['submore'] = $this->permiossion_model->db_get_screen_sub_detail();
                echo json_encode($data, true);
                die;

                var_dump($return);
                # code...
                break;
            default:
                echo 'Not event';die;
                # code...
                break;
        }
    }

    private function getNameRoute() {
        return $this->uri->segment(1); // n=1 for controller, n=2 for method, etc
    }

    private function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = $this->utf8ize($v);
            }
        } else if (is_string($d)) {
            return utf8_encode($d);
        }
        return $d;
    }

    public function add() {
        $list_data = $this->input->post('input');
        if(!$list_data){
            die;
        }
        // if($this->input->method(FALSE) !='post'){
        //     die;
        // }
        //var_dump($_POST);die;
        //$category_name = $_POST['category_name'];
        //$table_name = 'sih_' . $category_name;
        $name_router = $this->getNameRoute();
        $table_name = '';

        switch ($name_router) {
            case 'permission_role':
                $list_colum = ['name', 'created_at', 'created_by', 'stat'];
                //$list_data = $_POST['input'];
                $table_name = 'sih_roles';
                break;
            case 'permission_screen':
                $list_colum = ['title', 'route', 'routed_id','ico', 'created_at', 'created_by', 'stat'];
                //$list_data = $_POST['input'];
                $table_name = 'sih_screens';
                break;
            default:
                break;
        }

        $data = $this->datatable_model->add_record($list_colum, $list_data, $table_name);
    }

    public function edit() {

//         case 'list_nations':
//                $columns = array(
//                    array('db' => 'lnId', 'dt' => 0),
//                    array('db' => 'lnName', 'dt' => 2),
//                    array('db' => 'lnCreatedAt', 'dt' => 3),
//                    array('db' => 'lnCreatedBy', 'dt' => 4),
//                    array('db' => 'lnStat', 'dt' => 5),
//                    array('db' => 'lnOrder', 'dt' => 6),

        $list_data = $this->input->post('input');
        if(!$list_data){
            die;
        }

        $name_router = $this->getNameRoute();
        $vl_id_column = $_POST['input']['0'];
        unset($_POST['input']['0']);
        $table_name = '';


        switch ($name_router) {
            case 'permission_role':
                $list_colum = ['name', 'created_at', 'created_by', 'stat'];
                $list_data = $_POST['input'];
                $table_name = 'sih_roles';
                $name_id = 'id';
                break;
            case 'permission_screen':
                $list_colum = ['title', 'route', 'routed_id','ico', 'created_at', 'created_by', 'stat'];
                $list_data = $_POST['input'];
                $table_name = 'sih_screens';
                $name_id = 'id';
                break;
            default:
                break;
        }

        /*
          //var_dump($_POST);die;
          $list_name = $_POST['category_name'];
          $table_name = 'sih_' . $list_name;
          $vl_id_column = $_POST['input']['0'];
          $vl_id ='';//name of id every table
          unset($_POST['input']['0']);
          $_POST['input'] = array_values($_POST['input']);
          $name_id =$this->sub_name_table($list_name);
          switch ($list_name) {
          case 'list_nations':

          $list_colum = ['lnName', 'lnCreatedAt', 'lnCreatedBy', 'lnStat', 'lnOrder'];
          $list_data = $_POST['input'];
          break;
          case 'list_provinces':
          //$list_colum = ['ten_dn', 'ten_tinhthanh', 'ma_qh', 'ten_qh', 'dien_giai', 'trang_thai', 'order'];
          $list_colum = ['lpName', 'lpCreatedAt', 'lpCreatedBy', 'lpStat', 'lpOrder'];
          $list_data = $_POST['input'];
          break;

          default:
          break;
          }
         */
        //var_dump($name_id,$list_colum,$vl_id_column, $list_data, $table_name);die;

        $data = $this->datatable_model->edit_record($name_id, $list_colum, $vl_id_column, $list_data, $table_name);
    }

    public function delete() {

        //var_dump($_POST);die;
        //1: need check crfs for submiit
        //2: need chek login or check permission for action

        $list_data = $this->input->post('input');
        if(!$list_data){
            die;
        }
        //if (isset($_POST['input'])) {

            $name_router = $this->getNameRoute();
            switch ($name_router) {
                case 'permission_role':
                    $table_name = 'sih_roles';
                    $name_id = 'rId';
                    break;
                case 'permission_screen':
                    $table_name = 'sih_screens';
                    $name_id = 'sId';
                    break;
                default:
                    break;
            }


            // $name_id =$this->sub_name_table($list_name);

            //$value_input = $_POST['input'];
            $list_data = explode(',', $list_data);
//            if (!is_array($list_id_input)) {
//                $list_id_input[] = $value_input;
//            }else{
//                $list_id_input = $value_input;
//            }

            $data = $this->datatable_model->delete_record($name_id, $list_data, $table_name);
        //} else {
         //   echo 'Not correct input';
        //}
    }

    public function push() {

        //var_dump($_POST);die;
        //1: need check crfs for submiit
        //2: need chek login or check permission for action
        $list_data = $this->input->post('input');
        if(!$list_data){
            die;
        }

        //if (isset($_POST['input'])) {
            $name_router = $this->getNameRoute();
            
            switch ($name_router) {
                case 'permission_role':
                    $table_name = 'sih_roles';
                    $name_id = 'id';
                    break;
                case 'permission_screen':
                    $table_name = 'sih_screens';
                    $name_id = 'id';
                    break;
                default:
                    break;
            }
            
            //$table_name = 'sih_' . $name_router;
            //$value_input = $_POST['input'];
            $list_data = explode(',', $list_data);
            //$name_id = $this->sub_name_table($name_router);
//            if (!is_array($list_id_input)) {
//                $list_id_input[] = $value_input;
//            }else{
//                $list_id_input = $value_input;
//            }

            $data = $this->datatable_model->push_record($name_id, $list_data, $table_name);
        //} else {
         //   echo 'Not correct input';
        //}
    }

    public function unpush() {

        //var_dump($_POST);die;
        //1: need check crfs for submiit
        //2: need chek login or check permission for action

        if (isset($_POST['input'])) {
            $name_router = $this->getNameRoute();
            switch ($name_router) {
                case 'permission_role':
                    $table_name = 'sih_roles';
                    $name_id = 'r';
                    break;
                case 'permission_screen':
                    $table_name = 'sih_screens';
                    $name_id = 's';
                    break;
                default:
                    break;
            }
//            $table_name = 'sih_' . $name_router;
            //$value_input = $_POST['input'];
        $list_data = $this->input->post('input');
        if(!$list_data){
            die;
        }
        $list_data = explode(',', $list_data);
//            $name_id = $this->sub_name_table($name_router);
//            if (!is_array($list_id_input)) {
//                $list_id_input[] = $value_input;
//            }else{
//                $list_id_input = $value_input;
//            }

            $data = $this->datatable_model->unpush_record($name_id, $list_data, $table_name);
        } else {
            echo 'Not correct input';
        }
    }

    /*
     * function from list_districts -> ld
     */

    private function sub_name_table($name_table = '') {
        $list = explode('_', $name_table);
        //var_dump($list);
        $name_string = '';
        foreach ($list as $key => $value) {
            $name_string .=substr($value, 0, 1);
        }
        return $name_string;
    }



    public function role_screen_update() {
        if (isset($_POST['id_role']) && isset($_POST['id_screen']) && isset($_POST['checked'])) {
            $id_role = $_POST['id_role'];
            $id_screen = $_POST['id_screen'];
            $id_screen_main = $_POST['id_screen_main']; //defaul if everthing is checked -> set checked for screen main
            $checked = $_POST['checked'];
            $type_view = $_POST['type_view'];
            //if type="view" then update status for main show menu.
            if($type_view == 'view'){
                echo $id_role.'role update main '.$id_screen_main.' status '.$checked.'<br>';
                $return = $this->permiossion_model->db_role_screen_update($id_role, $id_screen_main, $checked);
                //add for main show 
                //var_dump($return);die;
				
                $return = $this->permiossion_model->db_get_screens_where('id',$id_screen_main);
                //var_dump($return);die;
				if(sizeof($return)>0){
					
					// $id_screen_at= $return[0]['routed_id'];
					$id_screen_at= $return[0]['id'];
					//check have exit same menu tre 
					//$return = $this->permiossion_model->db_get_screens_where('sRouteId',$id_screen_at);
					/*
					select sih_role_screens.sId
					from sih_role_screens 
					INNER JOIN sih_screens on sih_screens.sRouteId=sih_role_screens.sId
					where sih_screens.sRouteId='60' and sih_role_screens.rId=2
					*/
					//if(<0){ then update status current of }
					
					$return = $this->permiossion_model->db_get_list_same_main_menu($id_screen_at);
					//if not same level then update for main first
					if(sizeof($return)<0){
						$return = $this->permiossion_model->db_role_screen_update($id_role, $id_screen_at, $checked);
					}
					
				}
                
            }
            $return = $this->permiossion_model->db_role_screen_update($id_role, $id_screen, $checked);

            //var_dump($return);die;
            echo json_encode($return);
            die;
            //$return =implode(',',$return);
            //var_dump($return);die;
        } else {
            echo 'Error need something';
        }
    }

}
