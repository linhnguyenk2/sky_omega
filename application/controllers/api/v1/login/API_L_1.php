<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_L_1 User Action
 *
 * @since v.1.0
 */
class API_L_1 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'username',
            'password',
            'device_id'
        ));

        $this->setMainModelClassName("User_Action_Model");

        parent::__construct();
    }

    /**
     * Method to login
     *
     * @access public
     *
     *
     * @return object An object of data items on success, false on failure.
     */
    public function login()
    {
        $listParamInvalid = $this->getListParamInvalid($_POST);

        // Check required fields
        if ( ! empty($listParamInvalid)) {
            $status = $this->getStatusObject($this::API_STATUS_MISSING_PARAM_LIST, json_encode($listParamInvalid));
            $this->printJson(null, $status);
        } else {
            // need clear $_POST first.
            //$clean = $this->security->xss_clean($post);

            $userInfo = $this->model->checkLogin($_POST);

            // Authenticate false
            if (empty($userInfo)) {
                $status = $this->getStatusObject($this::API_STATUS_AUTH_FAIL);
                $this->printJson(null, $status);
            } else {
                // Set user session for browser
                // api need add device_id and session_id to HEADER to check permission
                $this->session->set_userdata('user', $userInfo);

                // Set cookie
                // 86400 = 1 day
              /*  unset($_COOKIE['device_id']);
                unset($_COOKIE['session_id']);
                unset($_COOKIE['expiry']);*/

               /* setcookie("device_id", $userInfo->device_id, time() + (86400 * 5), "/");
                setcookie("session_id", $userInfo->session_id, time() + (86400 * 5), "/");
                setcookie("expiry", $userInfo->expiry, time() + (86400 * 5), "/");*/

                $this->printJson($userInfo);
            }
        }
    }

    /**
     * Method isSessionValid
     *
     * @access public
     *
     *
     * @return object An object of data items on success, false on failure.
     */
    public function isSessionValid()
    {
        $isValidate         = 0;
        $listParamNeedValid = array('session_id', 'device_id');
        $listParamInvalid   = [];

        foreach ($listParamNeedValid as $paramNeedValid) {
            if (empty($_GET[$paramNeedValid])) {
                $listParamInvalid[] = $paramNeedValid;
            }
        }

        if ( ! empty($listParamInvalid)) {
            $status = $this->getStatusObject($this::API_STATUS_MISSING_PARAM_LIST, json_encode($listParamInvalid));
            $this->printJson(null, $status);
        } else {
            $isSessionValid = $this->model->isSessionValid($_GET['session_id'], $_GET['device_id']);

            if ($isSessionValid) {
                $status = $this->getStatusObject($this::API_STATUS_OK);
            } else {
                $status = $this->getStatusObject($this::API_STATUS_AUTH_FAIL, 'Token is invalid or expired');
            }

            $this->printJson(null, $status);
        }
    }
}