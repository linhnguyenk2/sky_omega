<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_W_4 Warehouse Export Form
 *
 * @since v.1.0
 */
class API_W_4 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'staff_id',
            'storekeeper_staff_id',
            'warehouse_id',
            "medicine_chief_staff_id",
            "subclinic_chief_staff_id",
            "request_form_id"
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

        $this->setMainModelClassName("Warehouse_Export_Form_Model");

        parent::__construct();
    }
}