<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_W_7 warehouse product in import form model
 *
 * @since v.1.0
 */
class API_W_7 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array(
            'product_id',
            'warehouse_import_form_id'
        ));

        $this->setListSelfReferredColumn(array(
            'product_id'
        ));
        
		$this->setMainModelClassName("Warehouse_Product_In_Import_Form_Model");

		parent::__construct();
	}
}