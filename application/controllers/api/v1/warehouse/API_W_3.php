<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_W_3 Warehouse request Form Model
 *
 * @since v.1.0
 */
class API_W_3 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array(
            "staff_id",
            "request_staff_id"
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

		$this->setMainModelClassName("Warehouse_Request_Form_Model");

		parent::__construct();
	}
}