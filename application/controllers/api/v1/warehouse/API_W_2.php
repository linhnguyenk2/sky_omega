<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_W_2 Warehouse Import Form
 *
 * @since v.1.0
 */
class API_W_2 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array(
		    'staff_id',
            'storekeeper_staff_id',
            'warehouse_id'
        ));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

		$this->setMainModelClassName("Warehouse_Import_Form_Model");

		parent::__construct();
	}
}