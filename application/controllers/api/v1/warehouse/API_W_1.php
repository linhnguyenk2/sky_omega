<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_W_1 Warehouse
 *
 * @since v.1.0
 */
class API_W_1 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array());
		$this->setMainModelClassName("Warehouse_Model");

		parent::__construct();
	}
}