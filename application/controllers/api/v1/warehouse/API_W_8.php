<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_W_8 warehouse product in sale form model
 *
 * @since v.1.0
 */
class API_W_8 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array(
            'warehouse_product_in_package_id',
            'warehouse_sale_form_id'
        ));

        $this->setListSelfReferredColumn(array(
            'product_id'
        ));
        
		$this->setMainModelClassName("Warehouse_Product_In_Sale_Form_Model");

		parent::__construct();
	}
}