<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_SCL_4 Subclinical xray  breast paper
 *
 * @since v.1.0
 */
class API_SCL_4 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array(
		    'type',
            'medical_examination_form_id',
            'staff_id',
            'specify_service_paper_id'));

		$this->setMainModelClassName("Subclinical_Xray_Breast_Paper_Model");

		parent::__construct();
	}
}