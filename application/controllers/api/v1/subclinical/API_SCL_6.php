<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_SCL_6 Subclinical ultra sound 3d
 *
 * @since v.1.0
 */
class API_SCL_6 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array(
		    'type',
            'medical_examination_form_id',
            'staff_id',
            'specify_service_paper_id'));

		$this->setMainModelClassName("Subclinical_Ultra_Sound_3d_Model");

		parent::__construct();
	}
}