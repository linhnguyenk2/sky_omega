<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_SCL_7 Subclinical template
 *
 * @since v.1.0
 */
class API_SCL_7 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array(
		    'type'));

		$this->setMainModelClassName("Subclinical_Template_Model");

		parent::__construct();
	}
}