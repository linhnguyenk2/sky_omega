<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_SCL_5 Subclinical ultra sound
 *
 * @since v.1.0
 */
class API_SCL_5 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array(
		    'type',
            'medical_examination_form_id',
            'staff_id',
            'specify_service_paper_id'));

		$this->setMainModelClassName("Subclinical_Ultra_Sound_Model");

		parent::__construct();
	}
}