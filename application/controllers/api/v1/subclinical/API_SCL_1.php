<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_SCL_1 Subclinical Paper
 *
 * @since v.1.0
 */
class API_SCL_1 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array(
		    'type',
            'medical_examination_form_id',
            'staff_id',
            'specify_service_paper_id'));

        $this->setListReferredColumn(array(
            'patient_id'
        ));

		$this->setMainModelClassName("Subclinical_Paper_Model");

		parent::__construct();
	}
}