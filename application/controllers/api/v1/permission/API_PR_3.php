<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PR_3 Content Permission Addon
 *
 * @since v.1.0
 */
class API_PR_3 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'type_user',
            'html_inner_content',
            'html_id'
        ));

        $this->setMainModelClassName("Content_Permission_Addon_Model");

        parent::__construct();
    }
}