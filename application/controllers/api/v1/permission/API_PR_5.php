<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PR_5 Menu
 *
 * @since v.1.0
 */
class API_PR_5 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'title',
            'html_id'
        ));

        $this->setMainModelClassName("Menu_Model");

        parent::__construct();
    }
}