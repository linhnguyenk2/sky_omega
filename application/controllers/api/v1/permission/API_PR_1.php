<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PR_1 Menu Permission
 *
 * @since v.1.0
 */
class API_PR_1 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'type_user',
            'html_id'
        ));

        $this->setMainModelClassName("Menu_Permission_Model");

        parent::__construct();
    }
}