<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PR_4 api permission
 *
 * @since v.1.0
 */
class API_PR_4 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'type_user',
            'route_path',
            'method'
        ));

        $this->setMainModelClassName("Api_Permission_Model");

        parent::__construct();
    }
}