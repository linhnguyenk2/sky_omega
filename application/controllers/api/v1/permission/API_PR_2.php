<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_PR_2 Content Permission
 *
 * @since v.1.0
 */
class API_PR_2 extends ApiController
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        $this->setListParamNeedValid(array(
            'type_user',
            'html_id'
        ));

        $this->setMainModelClassName("Content_Permission_Model");

        parent::__construct();
    }
}