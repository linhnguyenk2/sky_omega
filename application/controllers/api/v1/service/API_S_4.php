<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_S_4 Service
 *
 * @since v.1.0
 */
class API_S_4 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('service_type_id'));
		$this->setMainModelClassName("Service_Model");

		parent::__construct();
	}
}