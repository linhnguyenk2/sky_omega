<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_S_5 Product Use Location In Service
 *
 * @since v.1.0
 */
class API_S_5 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('location_in_service_id', 'product_id'));

        $this->setListSelfReferredColumn(array(
            'product_id',
            'medicine_id'
        ));

        $this->setMainModelClassName("Product_Use_Location_In_Service_Model");

		parent::__construct();
	}
}