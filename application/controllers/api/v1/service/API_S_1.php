<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_S_1 ServiceGroup
 *
 * @since v.1.0
 */
class API_S_1 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('name'));
		$this->setMainModelClassName("Service_Group_Model");

		parent::__construct();
	}
}