<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API_S_2 Service Type
 *
 * @since v.1.0
 */
class API_S_2 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('name', 'service_group_id'));
		$this->setMainModelClassName("Service_Type_Model");

		parent::__construct();
	}
}