<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * API_S_1 location in service model
 *
 * @since v.1.0
 */
class API_S_6 extends ApiController
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		$this->setListParamNeedValid(array('service_id', 'location_id', 'warehouse_id'));
		$this->setMainModelClassName("Location_In_Service_Model");

		parent::__construct();
	}
}