<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Si_List_Action extends CI_Controller {

    //private $data;
    function __construct() {
        parent::__construct();
        $this->load->model('List_Action_model', 'model_list_api');
    }


    private $glb_list_tables=[
        'sih_list_package_healthcare_service',
        'sih_list_service_in_package_healthcare',
    ];

    private function check_login() {
        $user = $this->session->userdata('user');
        if (empty($user)) {
            $this->load->view('content/signIn');
            //return;
            $method = $this->input->method(FALSE);
            if($method=='get'){
                redirect('/', 'refresh');
            }else{
                echo 'Not login for use';die;
                //redirect('/', 'refresh');
            }
        }
    }

    private function getNameRoute(){
        return $this->uri->segment(1); // n=1 for controller, n=2 for method, etc
    }

    public function get() {
        $name_router =$this->getNameRoute();
        $table ='';

        switch ($name_router) {
            case 'list_nations':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'code', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    // array('db' => 'lnCreatedAt', 'dt' => 3),
                    // array('db' => 'lnCreatedBy', 'dt' => 4),
                    array('db' => 'stat', 'dt' => 4, 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 5),
                );
                
                break;
            case 'list_provinces':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'code', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'id_nation', 'dt' => 4, 'select' =>'select-nations'),
                    array('db' => 'stat', 'dt' => 5, 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 6),
                );
                break;
            case 'list_districts':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'code', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'id_provinces', 'dt' => 4, 'select' =>'select-provinces'),
                    array('db' => 'created_at', 'dt' => 5, 'select-default' =>'selectprovincesnations'),
                    // array('db' => 'created_by', 'dt' => 6),
                    array('db' => 'stat', 'dt' => 6, 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 7),
                );
                break;
            case 'list_towns':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'id_districts', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'created_at', 'dt' => 4),
                    array('db' => 'created_by', 'dt' => 5),
                    array('db' => 'stat', 'dt' => 6, 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 7),
                );
                break;
            case 'list_titles':
                ///Idlt, ltNam, ltCreatedAt, ltCreatedBy, ltStat
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'code', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'note', 'dt' => 4),
                    array('db' => 'stat', 'dt' => 5, 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 6),
                    // array('db' => 'ltCreatedBy', 'dt' => 5),
                    // array('db' => 'ltOrder', 'dt' => 7),
                );
                //case 'list_towns':
                //$list_colum = ['ltId', 'ldId', 'ltName', 'ltCreatedAt', 'ltCreatedBy', 'ltStat', 'ltOrder'];
                break;
            case 'list_departments':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'code', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'stat', 'dt' => 4, 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 5),
                    array('db' => 'note', 'dt' => 6),
                    // array('db' => 'ltCreatedBy', 'dt' => 5),
                    // array('db' => 'ltOrder', 'dt' => 7),
                );
                break;
            case 'list_reason_discharge':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'code', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'note', 'dt' => 4),
                    array('db' => 'stat', 'dt' => 5 , 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 6),
                );
                break;
            case 'list_reason_transfer':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'code', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'note', 'dt' => 4),
                    array('db' => 'stat', 'dt' => 5, 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 6),
                );
                break;
            case 'list_materials':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'code', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'price', 'dt' => 4),
                    array('db' => 'vat', 'dt' => 5),
                    array('db' => 'stat', 'dt' => 6, 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 7),
                );
                break;
            case 'list_midwives':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'ma_ho_sinh', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'phong_ban', 'dt' => 4),
                    array('db' => 'created_at', 'dt' => 5),
                    array('db' => 'stat', 'dt' => 6)
                );
                break;
            case 'list_group_therapys':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'ma_kham', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'date', 'dt' => 4),
                    array('db' => 'stat', 'dt' => 5)
                );
                break;
            case 'list_vendors':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'vendor_code', 'dt' => 2),
                    array('db' => 'second_code', 'dt' => 3),
                    array('db' => 'vendor_name', 'dt' => 4),
                    array('db' => 'name_quick', 'dt' => 5),
                    array('db' => 'address', 'dt' => 6),
                    array('db' => 'city_province', 'dt' => 7),
                    array('db' => 'nation', 'dt' => 8),
                    array('db' => 'phone', 'dt' => 9),
                    array('db' => 'fax', 'dt' => 10),
                    array('db' => 'group_ncc', 'dt' => 11),
                    array('db' => 'group_settlement', 'dt' => 12),
                    array('db' => 'group_tax', 'dt' => 13),
                    array('db' => 'curency_code', 'dt' => 14),
                    array('db' => 'credit_limit', 'dt' => 15),
                    array('db' => 'tax_code', 'dt' => 16),
                    array('db' => 'email', 'dt' => 17),
                    array('db' => 'website', 'dt' => 18),
                    array('db' => 'contact', 'dt' => 19),
                    array('db' => 'contact_phone', 'dt' => 20),
                    array('db' => 'date_create', 'dt' => 21),
                    array('db' => 'user', 'dt' => 22),
                    array('db' => 'bank_account', 'dt' => 23),
                    array('db' => 'bank_name', 'dt' => 24),
                    array('db' => 'bank_address', 'dt' => 25),
                    array('db' => 'bank_modules', 'dt' => 26),
                );
                break;
            case 'list_currencies':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'code_from', 'dt' => 2),
                    array('db' => 'begin_date', 'dt' => 3),
                    array('db' => 'code_from', 'dt' => 4),
                    array('db' => 'rate', 'dt' => 5),
                    array('db' => 'stat', 'dt' => 6,'select'=>'select-status'),
                );
                break;
            case 'list_folks':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'code', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'stat', 'dt' => 4, 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 5),
                );
                
                break;
            case 'list_clinic':
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'code', 'dt' => 2),
                    array('db' => 'name', 'dt' => 3),
                    array('db' => 'department_id', 'dt' => 4, 'select'=>'select-departments'),
                    array('db' => 'location', 'dt' => 5),
                    array('db' => 'phone_number', 'dt' => 6),
                    array('db' => 'stat', 'dt' => 7, 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 8),
                    array('db' => 'note', 'dt' => 9),
                );
                
                break;
                
                case 'list_service':
                    $columns = array(
                        array('db' => 'id', 'dt' => 0),
                        array('db' => 'code', 'dt' => 2),
                        array('db' => 'name', 'dt' => 3),
                        array('db' => 'service_type_id', 'dt' => 4, 'select'=>'select-service-type'),
                        array('db' => 'unit', 'dt' => 5),
                        array('db' => 'price_in_hour', 'dt' => 6),
                        array('db' => 'price_overtime', 'dt' => 7),
                        array('db' => 'price_holiday', 'dt' => 8),
                        array('db' => 'price_foreigner', 'dt' => 9),
                        array('db' => 'vat', 'dt' => 10),
                        array('db' => 'note', 'dt' => 11),
                        array('db' => 'stat', 'dt' => 12, 'select'=>'select-status'),
                        array('db' => 'orders', 'dt' => 13),
                    );
                break;
                case 'list_service_type':
                    $columns = array(
                        array('db' => 'id', 'dt' => 0),
                        array('db' => 'code', 'dt' => 2),
                        array('db' => 'name', 'dt' => 3),
                        array('db' => 'stat', 'dt' => 4, 'select'=>'select-status'),
                        array('db' => 'orders', 'dt' => 5),
                    );
                break;
                case 'list_package_healthcare_service_type':
                    $columns = array(
                        array('db' => 'id', 'dt' => 0),
                        array('db' => 'code', 'dt' => 2),
                        array('db' => 'name', 'dt' => 3),
                        array('db' => 'stat', 'dt' => 4, 'select'=>'select-status'),
                        array('db' => 'orders', 'dt' => 5),
                    );
                break;
                case 'list_package_healthcare_service':
                    $columns = array(
                        array('db' => 'id', 'dt' => 0),
                        array('db' => 'code', 'dt' => 2),
                        array('db' => 'name', 'dt' => 3),
                        array('db' => 'package_healthcare_service_type_id', 'dt' => 4,'select'=>'select-package-healthcare-service-type'),
                        array('db' => 'price', 'dt' => 5),
                        array('db' => 'price_foreigner', 'dt' => 6),
                        array('db' => 'stat', 'dt' => 7, 'select'=>'select-status'),
                        array('db' => 'orders', 'dt' => 8),
                    );
                break;
            default:
                break;
        }
       
        $primaryKey = $columns[0]['db'];
        $table ='sih_'.$name_router;
        // SQL server connection information
        $sql_details = array(
            // 'user' => 'root',
            // 'pass' => '',
            // 'db' => 'sihospital',
            // 'host' => 'localhost'
        );
        
        $data = $this->model_list_api->getDataTable($_POST, $sql_details, $table, $primaryKey, $columns);
        
        //var_dump($data);die;
        //$data = $this->utf8ize($data);
        // var_dump($data);die;
        
        //$data['csrfName'] = $this->security->get_csrf_token_name();
        //$data['csrfHash'] = $this->security->get_csrf_hash();
        
        echo json_encode($data,true);
        die;
    }

    private function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = $this->utf8ize($v);
            }
        } else if (is_string($d)) {
            return utf8_encode($d);
        }
        return $d;
    }

    public function add() {
        if(isset($_POST['type'])&& $_POST['type']=='need_column'){  
            $this->add_need_column();
            return;
        }
        //for http://projectsky/list_package_healthcare_service
        if(isset($_POST['type'])&& $_POST['type']=='add_save_by_tb_col'){  
            $this->add_need_tb_col();
            return;
        }

        if(isset($_POST['type'])&& $_POST['type']=='add_save_by_tb_col_more'){
            $this->add_need_tb_col_more();
            return;
        }

        //var_dump($_POST);
        /*

        $name_router =$this->getNameRoute();
        $table_name ='sih_' . $name_router;
        $list_colum= array();
        $list_data= array();
       
        
        switch ($name_router) {
            case 'list_nations':
                $list_colum = [ 'code', 'name', 'stat', 'order'];
                $list_data = $_POST['input'];
                break;
            case 'list_provinces':
                $list_colum = ['name', 'created_at', 'created_by', 'stat', 'order'];
                $list_data = $_POST['input'];
                break;
            case 'list_districts':
                $list_colum = ['id_provinces', 'name', 'created_at', 'created_by', 'stat','order'];
                $list_data = $_POST['input'];
                break;
            case 'list_towns':
                $list_colum = ['id_districts', 'name', 'created_at', 'created_by', 'stat', 'order'];
                $list_data = $_POST['input'];
                break;

            case 'list_titles':
                $list_colum = ['name', 'created_at', 'created_by', 'stat'];
                $list_data = $_POST['input'];
                break;
            
            case 'list_departments':
                $list_colum = ['name', 'id_user' ,'created_at', 'created_by', 'stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_reason_discharge':
                $list_colum = ['ma', 'name' ,'note', 'date', 'stat'];
                $list_data = $_POST['input'];
                break;
            case 'reason_for_transfer':
                $list_colum = ['ma', 'name' ,'note', 'date', 'stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_materials':
                $list_colum = ['ma_qa', 'name' ,'price', 'price_eng', 'date','user','stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_midwives':
                $list_colum = ['ma_ho_sinh', 'name' ,'phong_ban', 'created_at','stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_group_therapys':
                $list_colum = ['ma_kham', 'name' ,'date', 'stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_vendors':
                //$list_colum = ['lvVendorCode','lvSecondCode','lvVendorName','lvNameQuick','lvAddress','lvCityProvince','lvNation','lvPhone','lvFax','lvGroupNcc','lvGroupSettlement','lvGroupTax','lvCurrencyCode','lvCreditLimit','lvTaxCode','lvEmail','lvWebsite','lvContact','lvContactPhone','lvDateCreate','lvUser','lvBankAccount','lvBankName','lvBankAddress','lvModules'];
                $list_colum = ['vendor_code','second_code','vendor_name','name_quick','address','city_province','nation','phone','fax','group_ncc','group_settlement','group_tax','curency_code','credit_limit','tax_code','email','website','contact','contact_phone','date_create','user','bank_account','bank_name','bank_address','bank_modules'];
                $list_data = $_POST['input'];
                break;
            case 'list_currencies':
                $list_colum = ['code','rate','created_at','stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_folks':
                $list_colum = [ 'code', 'name', 'stat', 'order'];
                $list_data = $_POST['input'];
                break;
            case 'list_clinic':
                $list_colum = [ 'code', 'name','id_departments','location','phone', 'stat', 'order','note'];
                $list_data = $_POST['input'];
                break;
            default:
                break;
        }

        $data = $this->model_list_api->add_record($list_colum, $list_data, $table_name);
        */
    }

    private function add_need_column(){
        $data_in = $this->input->post();
        if(isset($data_in['input'])){
            $name_router =$this->getNameRoute();
            $table_name ='sih_' . $name_router;
            $list_colum =[];
            $list_data =[];
            foreach ($data_in['input'] as $key => $value) {
                $list_colum[] = $key;
                $list_data[] = $value;
            }
            switch ($name_router) {
                case 'list_nations':
                    $list_colum[] = 'created_at';
                    $list_data[] = date('Y-m-d H:i:s');
                    break;
                case 'list_provinces':
                    break;
                case 'list_folks':
                    $list_colum[] = 'created_at';
                    $list_data[] = date('Y-m-d H:i:s');
                    break;
                case 'list_clinic':
                    $list_colum[] = 'created_at';
                    $list_data[] = date('Y-m-d H:i:s');
                    break;

                    
                case 'list_service':
                    $list_colum[] = 'created_at';
                    $list_data[] = date('Y-m-d H:i:s');
                    break;
                case 'list_service_type':
                    $list_colum[] = 'created_at';
                    $list_data[] = date('Y-m-d H:i:s');
                    break;
            }
            $data = $this->model_list_api->add_record($list_colum, $list_data, $table_name);
        }
        //var_dump($data_in);
    }
    private function add_need_tb_col(){
        $list_table =$this->glb_list_tables;
        
        $data_in = $this->input->post();
        if(isset($data_in['input'])){
            $list_tb_cl_ok =[];
            foreach ($data_in['input'] as $key => $value_cl) {
                $tb_name= 'sih_'.$key;
                if(in_array($tb_name,$list_table)){
                    // $list_tb_cl_ok[$tb_name]
                    //foreach ($value_cl as $key_cl => $value) {
                        # code...
                    //}
                    $list_tb_cl_ok[$tb_name] = $value_cl;
                }else{
                    echo 'Not ext tb '.$key;die;
                }
            }
            foreach ($list_tb_cl_ok as $key => $value) {
                //save tablbe and data
                $data_id[] = $this->model_list_api->add_record_tb_cl($key,$value);
            }
            $return['status']='ok';
            $return['msg']=$data_id;
            echo json_encode($return);
            exit();
        }
    }
    private function add_need_tb_col_more(){
        $list_table =$this->glb_list_tables;
        
        $data_in = $this->input->post();
        if(isset($data_in['input'])){
            $list_tb_cl_ok =[];
            /*
            foreach ($data_in['input'] as $key => $value_cl) {
                $tb_name= 'sih_'.$key;
                if(in_array($tb_name,$list_table)){
                    // $list_tb_cl_ok[$tb_name]
                    //foreach ($value_cl as $key_cl => $value) {
                        # code...
                    //}
                    $list_tb_cl_ok[$tb_name] = $value_cl;
                }else{
                    echo 'Not ext tb '.$key;die;
                }
            }
            */
            $list_tb_cl_ok_more =[];
            foreach ($data_in['input'] as $key => $value_cl) {
                foreach ($value_cl as $key_1 => $value_1) {
                    $tb_name= 'sih_'.$key_1;
                    if(in_array($tb_name,$list_table)){
                        $list_tb_cl_ok_more[][$tb_name] = $value_1;
                    }else{
                        echo 'Not ext tb '.$key;die;
                    }
                }
                
            }
            /*
            foreach ($list_tb_cl_ok as $key => $value) {
                //save tablbe and data
                $data_id[] = $this->model_list_api->add_record_tb_cl($key,$value);
            }
            */
            foreach ($list_tb_cl_ok_more as $key => $value) {
                //save tablbe and data
                foreach ($value as $key_sub => $value_sub) {
                    $data_id[] = $this->model_list_api->add_record_tb_cl($key_sub,$value_sub);
                }
                
            }
            $return['status']='ok';
            $return['msg']=$data_id;
            echo json_encode($return);
            exit();
        }
    }

    public function edit() {
        if(isset($_POST['type_submit']) && ($_POST['type_submit']=='edit_just_colum')){
            $this->f_edit_just_colum();
            return;
        }
        if(isset($_POST['type'])&& $_POST['type']=='edit_save_by_tb_col'){  
            $this->edit_need_tb_col();
            return;
        }
        if(isset($_POST['type'])&& $_POST['type']=='edit_save_by_tb_col_more'){  
            $this->edit_save_by_tb_col_more();
            return;
        }

        //off function
        /*
        
//         case 'list_nations':
//                $columns = array(
//                    array('db' => 'lnId', 'dt' => 0),
//                    array('db' => 'lnName', 'dt' => 2),
//                    array('db' => 'lnCreatedAt', 'dt' => 3),
//                    array('db' => 'lnCreatedBy', 'dt' => 4),
//                    array('db' => 'lnStat', 'dt' => 5),
//                    array('db' => 'lnOrder', 'dt' => 6),
        
        
        //var_dump($_POST);die;
        //$list_name = $_POST['category_name'];
       // $table_name = 'sih_' . $list_name;

        $name_router =$this->getNameRoute();
        $table_name ='sih_' . $name_router;


        $vl_id_column = $_POST['input']['0'];
        $vl_id ='';//name of id every table
        if(!isset($_POST['type_submit'])){
            unset($_POST['input']['0']);
            unset($_POST['input']['1']);
            // $_POST['input'] = arrayf
            $_POST['input'] = array_values($_POST['input']);
        }
        //var_dump($_POST['input']);die;
        
        //var_dump($_POST['input']);
        //$_POST['input'] = array_values($_POST['input']);
        //$name_id =$this->sub_name_table($name_router);
        $name_id ='id';
        switch ($name_router) {
            case 'list_nations':
                
                // $list_colum = ['lnName', 'lnCreatedAt', 'lnCreatedBy', 'lnStat', 'lnOrder'];
                $list_colum = [ 'code', 'name', 'stat', 'order'];
                $list_data = $_POST['input'];
                //var_dump($list_colum,$list_data);die;
                break;
            case 'list_provinces':
                //$list_colum = ['ten_dn', 'ten_tinhthanh', 'ma_qh', 'ten_qh', 'dien_giai', 'trang_thai', 'order'];
                $list_colum = ['name', 'created_at', 'created_by', 'stat', 'order'];
                $list_data = $_POST['input'];
                break;
            case 'list_districts':
                $list_colum = ['id_provinces', 'name', 'created_at', 'created_by', 'stat','order'];
                $list_data = $_POST['input'];
                break;
            case 'list_towns':
                $list_colum = ['id_districts', 'name', 'created_at', 'created_by', 'stat', 'order'];
                $list_data = $_POST['input'];
                break;
            case 'list_titles':
                $list_colum = ['name', 'created_at', 'created_by', 'stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_departments':
                $list_colum = ['name', 'id_user' ,'created_at', 'created_by', 'stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_reason_discharge':
                $list_colum = ['ma', 'name' ,'note', 'date', 'stat'];
                $list_data = $_POST['input'];
                break;
            case 'reason_for_transfer':
                $list_colum = ['ma', 'name' ,'note', 'date', 'stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_materials':
                $list_colum = ['ma_qa', 'name' ,'price', 'price_eng', 'date','user','stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_midwives':
                $list_colum = ['ma_ho_sinh', 'name' ,'phong_ban', 'created_at','stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_group_therapys':
                $list_colum = ['ma_kham', 'name' ,'date', 'stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_vendors':
                $list_colum = ['vendor_code','second_code','vendor_name','name_quick','address','city_province','nation','phone','fax','group_ncc','group_settlement','group_tax','curency_code','credit_limit','tax_code','email','website','contact','contact_phone','date_create','user','bank_account','bank_name','bank_address','bank_modules'];
                $list_data = $_POST['input'];
                break;
            case 'list_currencies':
                $list_colum = ['code','rate','created_at','stat'];
                $list_data = $_POST['input'];
                break;
            case 'list_folks':
                $list_colum = [ 'code', 'name', 'stat', 'order'];
                $list_data = $_POST['input'];
                break;  
                
            default:
                break;
        }

        //xu ly event save 1 edit cho table
        if(isset($_POST['type_submit']) && ($_POST['type_submit']=='edit_just_colum')){
            //var_dump($list_data,$list_colum);//die;
            $name_id ='id';
            $vl_id_column =$list_data[0];
            // var_dump($list_colum);
            // var_dump((int)sizeof($list_data));
            // var_dump((int)sizeof($list_data)-1);
            $list_colum_tem[]=$list_colum[(int)sizeof($list_data)-2];
            $list_data_tem[]=$list_data[(int)sizeof($list_data)-1];

            $list_colum = $list_colum_tem;
            $list_data = $list_data_tem;
        
        }
        
        $data = $this->model_list_api->edit_record($name_id,$list_colum,$vl_id_column, $list_data, $table_name);
        */
    }
    private function f_edit_just_colum(){
        $data_in = $this->input->post();
        //var_dump($data_in);die;
        if(isset($data_in['input'])){
            //var_dump($data_in['input']);die;
            //set not save when not right key text
            if(isset($data_in['input'][1]) || isset($data_in['input'][2])){
                echo 'Not save';die;
            }
            $name_router =$this->getNameRoute();
            $table_name ='sih_' . $name_router;
            $var_nameid='';
            $var_valid='';
            $var_namecl='';
            $var_valcl='';
            $tang=0;
            foreach ($data_in['input'] as $key => $value) {
                # code...
                if($tang==0){
                    $var_nameid =$key;
                    $var_valid =$value;
                    $tang++;
                }else{
                    $var_namecl =$key;
                    $var_valcl =$value;
                }
            }
            $data = $this->model_list_api->edit_record_just_colum($table_name,$var_nameid,$var_valid,$var_namecl,$var_valcl);
        }
    }

    private function edit_need_tb_col(){
        $list_table =$this->glb_list_tables;
        
        $data_in = $this->input->post();
        //var_dump($data_in);die;

        if(isset($data_in['input'])){
            $list_tb_cl_ok =[];
            foreach ($data_in['input'] as $key => $value_cl) {
                $tb_name= 'sih_'.$key;
                if(in_array($tb_name,$list_table)){
                    // $list_tb_cl_ok[$tb_name]
                    //foreach ($value_cl as $key_cl => $value) {
                        # code...
                    //}
                    $list_tb_cl_ok[$tb_name] = $value_cl;
                }else{
                    echo 'Not ext tb '.$key;die;
                }
            }
            foreach ($list_tb_cl_ok as $key => $value) {
                //save tablbe and data
                $data = $this->model_list_api->edit_record_tb_cl($key,$value);
            }
            exit();
        }
    }
    private function edit_save_by_tb_col_more(){
        $list_table =$this->glb_list_tables;
        
        $data_in = $this->input->post();
        //var_dump($data_in);die;

        if(isset($data_in['input'])){
            $list_tb_cl_ok_more =[];
            foreach ($data_in['input'] as $key => $value_cl) {
                foreach ($value_cl as $key_1 => $value_1) {
                    $tb_name= 'sih_'.$key_1;
                    if(in_array($tb_name,$list_table)){
                        $list_tb_cl_ok_more[][$tb_name] = $value_1;
                    }else{
                        echo 'Not ext tb '.$key;die;
                    }
                }
                
            }
            //var_dump($list_tb_cl_ok_more);die;
            foreach ($list_tb_cl_ok_more as $key => $value) {
                //save tablbe and data
                foreach ($value as $key_sub => $value_sub) {
                    $data = $this->model_list_api->edit_auto_record_tb_cl($key_sub,$value_sub);
                }
                
            }
            exit();
        }
    }
    
    public function add_get(){
        $data_in = $this->input->get();
        $name_router =$this->getNameRoute();
        $table_name ='sih_' . $name_router;
         

        //var_dump($name_router);die;   
        switch ($name_router){
            case 'list_package_healthcare_service':
                if($data_in['action']=='get_list_healthcare'){
                    $data = $this->model_list_api->get_list_healthcare();
                    echo json_encode($data);die;
                }
                break;
            default:
                break;
        }
        //var_dump($data_in);
        die;
    }
    
    public function edit_get(){
        $data_in = $this->input->get();

        if(isset($data_in['type'])&& $data_in['type']=='get_datatable'){
            $this->edit_get_datatable();
            return;
        }

        // $name_router =$this->getNameRoute();
        // $table_name ='sih_' . $name_router;
         

        // //var_dump($name_router);die;   
        // switch ($name_router) {
        //     case 'list_nations':
        //         break;
        //     default:
        //         break;
        // }
        // var_dump($data_in);
        // die;
    }

    private function event_edit_get_one(){
        
    }
    private function edit_get_datatable(){
        
        $data_in = $this->input->get();
        $table = $data_in['table'];
        $name_table = 'sih_';
        switch ($table) {
            case 'list_service_in_package_healthcare':
                $name_table = $name_table.$table;
                $columns = array(
                    array('db' => 'id', 'dt' => 0),
                    array('db' => 'package_healthcare_service_id', 'dt' => 1),
                    array('db' => 'healthcare_service_id', 'dt' => 2),
                    array('db' => 'number', 'dt' => 7),
                    array('db' => 'stat', 'dt' => 13 , 'select'=>'select-status'),
                    array('db' => 'orders', 'dt' => 14),
                );

                $_GET['columns']['1']['search']['value'] =$_GET['id_get'];
                $_GET['columns']['1']['data'] ='1';
                $_GET['length'] ='10';
                
                break;
            default:
                # code...
                break;
        }
        if($name_table != 'sih_'){
            $primaryKey = $columns[0]['db'];
            $data = $this->model_list_api->getDataTable($_GET, '', $name_table, $primaryKey, $columns);
            echo json_encode($data,true);
            die;
        }
        echo 'Not right input';die;
    }

    public function edit_delete(){
        $request_vars = array();
        $data1 = file_get_contents("php://input");
        parse_str($data1, $request_vars );
        if(isset($request_vars['type']) && $request_vars['type'] =='delete_in_tb'){
            $name_id = 'id';
            $table_name = 'sih_'.$request_vars['type_table'];
            $list = explode(",", $request_vars['list']);
            $this->delete_list_in_tb($name_id,$table_name,$list);
            
        }
        
        //var_dump($request_vars);die;
    }
    private function delete_list_in_tb($name_id,$table_name,$list){
        foreach ($list as $key => $value) {
            $data = $this->model_list_api->delete_record($name_id,$value, $table_name);
        }
        return null;
    }
    
    public function delete() {

        //var_dump($_POST);die;
        //1: need check crfs for submiit
        //2: need chek login or check permission for action
        //$list_name =$this->getNameRoute();
        //$table ='sih_' . $list_name;

        if (isset($_POST['input'])) {
            $name_router = $this->getNameRoute();
            $table_name = 'sih_' . $name_router;
            
            
            //$name_id =$this->sub_name_table($name_router);
            $name_id ='id';
            
            $value_input = $_POST['input'];
            $value_input=  explode(',', $value_input);
//            if (!is_array($list_id_input)) {
//                $list_id_input[] = $value_input;
//            }else{
//                $list_id_input = $value_input;
//            }
            
            $data = $this->model_list_api->delete_record($name_id,$value_input, $table_name);
        }else{
            echo 'Not correct input';
        }
    }
    public function push() {

        //var_dump($_POST);die;
        //1: need check crfs for submiit
        //2: need chek login or check permission for action

        if (isset($_POST['input'])) {
            $name_router = $this->getNameRoute();
            $table_name = 'sih_' . $name_router;
            $value_input = $_POST['input'];
            $value_input=  explode(',', $value_input);
            //$name_id =$this->sub_name_table($name_router);
            $name_id ='id';
//            if (!is_array($list_id_input)) {
//                $list_id_input[] = $value_input;
//            }else{
//                $list_id_input = $value_input;
//            }
            
            $data = $this->model_list_api->push_record($name_id,$value_input, $table_name);
        }else{
            echo 'Not correct input';
        }
    }
    public function unpush() {

        //var_dump($_POST);die;
        //1: need check crfs for submiit
        //2: need chek login or check permission for action

        if (isset($_POST['input'])) {
            $name_router = $this->getNameRoute();
            $table_name = 'sih_' . $name_router;
            $value_input = $_POST['input'];
            $value_input=  explode(',', $value_input);
            //$name_id =$this->sub_name_table($name_router);
            $name_id ='id';
//            if (!is_array($list_id_input)) {
//                $list_id_input[] = $value_input;
//            }else{
//                $list_id_input = $value_input;
//            }
            
            $data = $this->model_list_api->unpush_record($name_id,$value_input, $table_name);
        }else{
            echo 'Not correct input';
        }
    }
    /*
     * function from list_districts -> ld
     */
    private function sub_name_table($name_table =''){
        $list=  explode('_', $name_table);
        //var_dump($list);
        $name_string='';
        foreach ($list as $key => $value) {
            $name_string .=substr($value, 0,1);
        }
        return $name_string;
    }

}
