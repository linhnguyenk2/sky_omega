<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Si_List extends AppAuthControler {
    
    protected function get_css_js_basic() {
        $data= parent::get_css_js_basic();
        $data['css'][]= base_url('assets/css/list.css');
        $data['js']['basic']=base_url('assets/js/list/basic.js');
        return $data;
    }

    public function list_nations() {
        
        $data = $this->get_css_js_basic();
//        $data['title'] = 'Danh mục Quốc gia';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_nations';
        


        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_nations', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }

    public function list_provinces() {
        
        $data = $this->get_css_js_basic();
        //$data['title'] = 'Danh mục thành phố';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_provinces';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_provinces', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }
    public function list_districts() {
        
        $data = $this->get_css_js_basic();
        //$data['title'] = 'Danh mục quận huyện';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_districts';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

        $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_district', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }
    public function list_towns() {
        
        $data = $this->get_css_js_basic();
        //$data['title'] = 'Danh mục phường xã';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_towns';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
       
         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_town', $data);

        
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }

    public function list_titles() {
        
        $data = $this->get_css_js_basic();
        //$data['title'] = 'Danh mục chức vụ/nghề nghiệp';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_titles';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_titles', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }

    public function list_departments() {
        
        $data = $this->get_css_js_basic();
        //$data['title'] = 'Danh mục Phòng Ban';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_departments';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_departments', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }
    
	public function list_reason_discharge(){
        
        $data = $this->get_css_js_basic();
        //$data['title'] = 'list_reason_discharge';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_reason_discharge';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_reason_discharge', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

	}

	public function list_reason_transfer(){
        $data = $this->get_css_js_basic();
        //$data['title'] = 'list_reason_transfer';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_reason_transfer';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_reason_transfer', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

	}

	public function list_materials(){
		$data = $this->get_css_js_basic();
        //$data['title'] = 'Danh Mục Vật Dụng';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_materials';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_materials', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

	}

	public function list_midwives(){
		$data = $this->get_css_js_basic();
        //$data['title'] = 'list_midwives';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_midwives';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_midwives', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

	}

	public function list_group_therapys(){
		$data = $this->get_css_js_basic();
        //$data['title'] = 'list_group_therapys';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_group_therapys';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_group_therapys', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

	}

	public function list_vendors(){
        $data = $this->get_css_js_basic();
        
        //$data['title'] = 'list_vendors';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_vendors';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_vendors', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

	}

	public function list_currencies(){
		$data = $this->get_css_js_basic();
        //$data['title'] = 'Tỷ giá';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_currencies';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_currencies', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }
    
    public function list_folks() {
        
        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_folks';
        


        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_folks', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }

    public function list_clinic() {
        
        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_clinic';
        


        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_clinic', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }
   
    

    public function list_service_type() {
        
        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_service_type';
        


        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_service_type', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }
    public function list_service() {
        
        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_service';
        


        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_service', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }

    public function list_package_healthcare_service_type() {
        
        $data = $this->get_css_js_basic();
        
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_package_healthcare_service_type';
        


        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_package_healthcare_service_type', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }

    public function list_package_healthcare_service() {
        
        $data = $this->get_css_js_basic();
        unset($data['js']['basic']);
        $data['js']['list_package_healthcare_service'] =base_url('assets/js/list/list_package_healthcare_service.js');
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_package_healthcare_service';

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
         $data['list_lang'] = $this->lang;//list lang content for view
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_package_healthcare_service_1', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }

    //if return true thi load, false thi khong
    private function is_get_layout() {
        if ((isset($_GET['layout']) ? $_GET['layout'] : null) != 'none') {
            return true;
        }
        return false;
//        if ((isset($_GET['layout']) ? $_GET['layout'] : null) != 'none') {
//            $this->load->view('header', $data);
//            $this->load->view('nav/topbar', $data);
//            $this->load->view('nav/leftbar', $data);
//            $this->load->view('footer', $data);
//        }
    }

    protected function getLanguageFileName()
    {
        return 'manage_list_lang';
    }

}
