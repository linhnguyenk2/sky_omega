<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Family_planing_book_list extends AppAuthControler
{
    function __construct()
    {
        parent::__construct();
    }
    
    protected function getListJavascript()
    {
        $listJavasript = parent::getListJavascript();
        $listJavasript[] = base_url('assets/js/receive/list_function_use_patient.js');
        $listJavasript[] = base_url('assets/js/book/family_planing_book_list.js');
        return $listJavasript;
    }

    protected function getListCSS()
    {
        return parent::getListCSS();
    }

    public function family_planing_book_list()
    {
        $html = $this->getTemplate();
        echo $html;
    }

    protected function getContentViewName()
    {
        $name_title_by_method = $this->router->fetch_method();
        return 'book/' . $name_title_by_method;
    }

    protected function getActiveMenuId()
    {
        return "menu_10_5";
    }

    protected function getLanguageFileName()
    {
        return 'book/family_planing_book_list_lang';
    }
    
}