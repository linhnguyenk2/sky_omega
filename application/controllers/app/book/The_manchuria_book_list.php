<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class The_manchuria_book_list extends AppAuthControler {

    function __construct() {
        parent::__construct();
        //$this->lang->load('manage_list_lang', 'vietnam');
        $this->lang->load('book/the_manchuria_book_list_lang', 'vietnam');
    }
    
    protected function get_css_js_basic() {
        $data= parent::get_css_js_basic();
        $data['js'][]=base_url('assets/js/api/wraper.js');
        $data['js'][]=base_url('assets/js/api/status_code.js');
        $data['js'][]=base_url('assets/js/api/select_basic.js');
        $data['js'][]=base_url('assets/js/api/create_page.js');
        // $data['js'][]=base_url('assets/js/api/list_function_use.js');
        $data['js'][]=base_url('assets/js/receive/list_function_use_patient.js');
        $data['css'][]=base_url('assets/css/api_list.css');
        return $data;
    }
    public function the_manchuria_book_list() {
        $data = $this->get_css_js_basic();

        $data['js'][] = base_url('assets/js/book/the_manchuria_book_list.js');
        $name_title_by_method = $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_' . $name_title_by_method);
        $data['groupMenu'] = 'outpatient'; //need change
        $data['menu'] = $name_title_by_method;

        $data['element_event'] = null;

        $data['glb_info_route_view'] = null;
        $this->load->view('header', $data);
        $this->load->view('nav/topbar', $data);
        $this->load->view('nav/leftbar', $data);
        
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('book/template_delete_push_unpush', $data, true);
        $this->load->view('book/' . $name_title_by_method, $data);

        $this->load->view('footer', $data);
    }
}