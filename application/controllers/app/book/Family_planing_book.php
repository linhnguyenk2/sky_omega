<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Family_planing_book extends AppAuthControler {

    function __construct() {
        parent::__construct();
        //$this->lang->load('manage_list_lang', 'vietnam');
        $this->lang->load('book/family_planing_book_lang', 'vietnam');
    }
    
    protected function get_css_js_basic() {
        $data= parent::get_css_js_basic();
        
        $data['js'][]=base_url('assets/js/receive/list_function_use_patient.js');
        $data['js'][]=base_url('assets/js/book/for_book_history_examination.js');
        $data['js'][]=base_url('assets/js/book/for_id_myDeleteEverything.js');
        return $data;
    }
    public function family_planing_book() {
        $data = $this->get_css_js_basic();

        $data['js'][] = base_url('assets/js/book/family_planing_book.js');
        $name_title_by_method = $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_' . $name_title_by_method);
        $data['groupMenu'] = 'outpatient'; //need change
        $data['menu'] = $name_title_by_method;

        $data['element_event'] = null;
        
		$data['id_book']=isset($_GET['book_id'])?$_GET['book_id']:'';
        $data['id_parent']=isset($_GET['parent_id'])?$_GET['parent_id']:'';

        $data['glb_info_route_view'] = null;
        $this->load->view('header', $data);
        $this->load->view('nav/topbar', $data);
        $this->load->view('nav/leftbar', $data);
        
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('book/template_delete_push_unpush', $data, true);
        $this->load->view('book/' . $name_title_by_method, $data);

        $this->load->view('footer', $data);
    }
}