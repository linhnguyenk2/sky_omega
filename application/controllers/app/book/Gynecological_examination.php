<?php
/*
defined('BASEPATH') OR exit('No direct script access allowed');

class Gynecological_examination extends AppAuthControler {

    function __construct() {
        parent::__construct();
        $this->lang->load('book/gynecological_examination', 'vietnam');
    }
    
    protected function get_css_js_basic() {
        $data= parent::get_css_js_basic();
        $data['js'][]=base_url('assets/js/api/wraper.js');
        $data['js'][]=base_url('assets/js/api/status_code.js');
        $data['js'][]=base_url('assets/js/api/select_basic.js');
        $data['js'][]=base_url('assets/js/api/create_page.js');
        $data['js'][]=base_url('assets/js/receive/list_function_use_patient.js');
        $data['css'][]=base_url('assets/css/api_list.css');
        return $data;
    }
    public function gynecological_examination($id_at) {
        $data = $this->get_css_js_basic();

        $data['js'][] = base_url('assets/js/book/gynecological_examination.js');
        $data['js'][] = base_url('assets/js/book/gynecological_examination_function_table.js');
        $data['js'][] = base_url('assets/js/book/gynecological_examination_chidinhdichvu.js');
        $data['js'][] = base_url('assets/js/book/gynecological_examination_toathuoc.js');
        $data['js'][] = base_url('assets/js/book/gynecological_examination_tpcn.js');
        $name_title_by_method = $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_' . $name_title_by_method);
        $data['groupMenu'] = 'outpatient'; //need change
        $data['menu'] = $name_title_by_method;
        $data['id_at'] = $id_at;

        $data['element_event'] = null;

        $data['glb_info_route_view'] = null;
        $this->load->view('header', $data);
        $this->load->view('nav/topbar', $data);
        $this->load->view('nav/leftbar', $data);
        
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('book/template_delete_push_unpush', $data, true);
        $this->load->view('book/' . $name_title_by_method, $data);

        $this->load->view('footer', $data);
    }
}
*/
?>

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gynecological_examination extends AppAuthControler
{
    function __construct()
    {
        parent::__construct();
    }
    
    protected function getListJavascript()
    {
        $listJavasript = parent::getListJavascript();
        $listJavasript[] = base_url('assets/js/receive/list_function_use_patient.js');
        $listJavasript[] = base_url('assets/js/book/gynecological_examination.js');
        $listJavasript[] = base_url('assets/js/book/gynecological_examination_function_table.js');
        $listJavasript[] = base_url('assets/js/book/gynecological_examination_chidinhdichvu.js');
        $listJavasript[] = base_url('assets/js/book/gynecological_examination_toathuoc.js');
        $listJavasript[] = base_url('assets/js/book/gynecological_examination_tpcn.js');
        return $listJavasript;
    }

    protected function getListCSS()
    {
        return parent::getListCSS();
    }

    public function gynecological_examination($id_at)
    {
        $data['id_at'] = $id_at;

        $html = $this->getTemplate($data);
        echo $html;
    }

    protected function getContentViewName()
    {
        $name_title_by_method = $this->router->fetch_method();
        return 'book/' . $name_title_by_method;
    }

    protected function getActiveMenuId()
    {
        return "menu_10_1";
    }

    protected function getLanguageFileName()
    {
        return 'book/gynecological_examination_lang';
    }
    
}