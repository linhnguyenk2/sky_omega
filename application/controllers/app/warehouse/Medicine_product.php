<?php
/*
defined('BASEPATH') OR exit('No direct script access allowed');

class medicine_product extends AppAuthControler {

    function __construct() {
        parent::__construct();
    }
    
    protected function get_css_js_basic() {
        $data= parent::get_css_js_basic();
        $data['js'][]=base_url('assets/js/api/wraper.js');
        $data['js'][]=base_url('assets/js/api/status_code.js');
        $data['js'][]=base_url('assets/js/api/select_basic.js');
        $data['js'][]=base_url('assets/js/api/create_page.js');
//        $data['js'][]=base_url('assets/js/api/list_function_use.js');
        $data['css'][]=base_url('assets/css/api_list.css');
        return $data;
    }
    

    public function medicine_product() {
        $data = $this->get_css_js_basic();

        // $data['js'][] = base_url('assets/js/api/medicine_product_require_function.js');
        $data['js'][] = base_url('assets/js/receive/list_function_use_patient.js');
        $data['js'][] = base_url('assets/js/api/medicine_product_event.js');
        $data['js'][] = base_url('assets/js/api/medicine_product_sub_pupop.js');
        $name_title_by_method = $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_' . $name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = $name_title_by_method;

        $data['element_event'] = null;

        $data['glb_info_route_view'] = null;
        $this->load->view('header', $data);
        $this->load->view('nav/topbar', $data);
        $this->load->view('nav/leftbar', $data);
        
        $this->lang->load('manage_list_lang', 'vietnam');
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/' . $name_title_by_method, $data);

        $this->load->view('footer', $data);
    }


}
*/
?>


<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Medicine_product extends AppAuthControler
{
    function __construct()
    {
        parent::__construct();
    }
    
    protected function getListJavascript()
    {
        $listJavasript = parent::getListJavascript();
        // $listJavasript[] = base_url('assets/js/receive/list_function_use_patient.js');
        // $listJavasript[] = base_url('assets/js/warehouse/cosmetic_list.js');
        //$listJavasript[] = base_url('assets/js/api/cosmetic_list.js');
        $listJavasript[] = base_url('assets/js/receive/list_function_use_patient.js');
        $listJavasript[] = base_url('assets/js/api/medicine_product_event.js');
        $listJavasript[] = base_url('assets/js/api/medicine_product_sub_pupop.js');

        return $listJavasript;
    }

    protected function getListCSS()
    {
        return parent::getListCSS();
    }

    public function medicine_product()
    {
        $html = $this->getTemplate();
        echo $html;
    }

    protected function getContentViewName()
    {
        $name_title_by_method = $this->router->fetch_method();
        return 'warehouse/' . $name_title_by_method;
    }

    protected function getActiveMenuId()
    {
        return "menu_8_5";
    }

    protected function getLanguageFileName()
    {
        //return 'warehouse/medicine_product_lang';
        return 'manage_list_lang';
    }
    
}