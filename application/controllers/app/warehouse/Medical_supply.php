<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Medical_supply extends AppAuthControler
{
    function __construct()
    {
        parent::__construct();
    }
    
    protected function getListJavascript()
    {
        $listJavasript = parent::getListJavascript();
        $listJavasript[] = base_url('assets/js/receive/list_function_use_patient.js');
        $listJavasript[] = base_url('assets/js/warehouse/medical_supply.js');
        //$listJavasript[] = base_url('assets/js/api/medical_supply.js');
        return $listJavasript;
    }

    protected function getListCSS()
    {
        return parent::getListCSS();
    }

    public function medical_supply()
    {
        $html = $this->getTemplate();
        echo $html;
    }

    protected function getContentViewName()
    {
        $name_title_by_method = $this->router->fetch_method();
        return 'warehouse/' . $name_title_by_method;
    }

    protected function getActiveMenuId()
    {
        return "menu_8_6";
    }

    protected function getLanguageFileName()
    {
        return 'warehouse/medical_supply_lang';
    }
    
}