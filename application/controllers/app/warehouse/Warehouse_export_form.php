<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Warehouse_export_form extends AppAuthControler
{
    function __construct()
    {
        parent::__construct();
        //$this->lang->load('manage_list_lang', 'vietnam');
        $this->lang->load('warehouse/warehouse_export_form_lang', 'vietnam');
    }

    protected function get_css_js_basic()
    {
        $data          = parent::get_css_js_basic();
        $data['js'][]  = base_url('assets/js/api/wraper.js');
        $data['js'][]  = base_url('assets/js/api/status_code.js');
        $data['js'][]  = base_url('assets/js/api/select_basic.js');
        $data['js'][]  = base_url('assets/js/api/create_page.js');
        $data['js'][]  = base_url('assets/js/api/list_function_use.js');
        $data['css'][] = base_url('assets/css/api_list.css');

        return $data;
    }

    public function warehouse_export_form($str)
    {
        $data = $this->get_css_js_basic();

        //$data['js'][]         = base_url('assets/js/book/outpatient_book_detail.js');
        $name_title_by_method = $this->router->fetch_method();
        $data['title']        = $this->lang->line('title_' . $name_title_by_method);

        // Need change
        $data['groupMenu'] = 'warehouse';
        $data['menu']      = $name_title_by_method;

        $listid = explode("_", $str);
        /*$data['id_book']       = $listid[0];
        $data['id_parent']     = $listid[1];*/
        $data['element_event'] = null;

        $data['glb_info_route_view'] = null;
        $this->load->view('header', $data);
        $this->load->view('nav/topbar', $data);
        $this->load->view('nav/leftbar', $data);

        $data['list_lang']                   = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('warehouse/template_delete_push_unpush', $data, true);
        $this->load->view('warehouse/' . $name_title_by_method, $data);

        $this->load->view('footer', $data);
    }
}