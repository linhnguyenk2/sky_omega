<?php
defined('BASEPATH') or exit('No direct script access allowed');

class List_functional_foods extends AppAuthControler
{
    function __construct()
    {
        parent::__construct();
    }
    
    protected function getListJavascript()
    {
        $listJavasript = parent::getListJavascript();
        $listJavasript[] = base_url('assets/js/receive/list_function_use_patient.js');
        $listJavasript[] = base_url('assets/js/warehouse/list_functional_foods.js');
        //$listJavasript[] = base_url('assets/js/api/list_functional_foods.js');
        return $listJavasript;
    }

    protected function getListCSS()
    {
        return parent::getListCSS();
    }

    public function list_functional_foods()
    {
        $html = $this->getTemplate();
        echo $html;
    }

    protected function getContentViewName()
    {
        $name_title_by_method = $this->router->fetch_method();
        return 'warehouse/' . $name_title_by_method;
    }

    protected function getActiveMenuId()
    {
        return "menu_8_3";
    }

    protected function getLanguageFileName()
    {
        return 'warehouse/list_functional_foods_lang';
    }
    
}