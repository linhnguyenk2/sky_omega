<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Signin extends CI_Controller
{

    public function index()
    {
        $user = $this->session->userdata('user');
        if (empty($user)) {
            // Set device ID
            $this->load->library('User_Library');
            $deviceId = $this->user_library->generateDeviceId();
            $data['deviceId'] = $deviceId;

            // Load View
            $this->load->view('content/signIn', $data);

        } else {
            redirect('/dashboard');
        }
    }

    public function forgotpassword()
    {
        $user = $this->session->userdata('user');
        if (empty($user)) {
            $this->load->view('content/forgotpassword');
            return;
        }
        redirect('/');
    }

    public function pageNotFound()
    {
        $user = $this->session->userdata('user');
        
        $data = array(
            'title' => 'Không tìm thấy trang',
            'menu' => '404',
            'user' => $user
        );
        
        $this->load->view('header', $data);
        $this->load->view('nav/topbar', $data);
        $this->load->view('nav/leftbar', $data);
        $this->load->view('content/pagenotfound', $data);
        $this->load->view('footer', $data);
    }
}