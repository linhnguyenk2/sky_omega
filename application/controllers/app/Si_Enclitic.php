<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Si_Enclitic extends CI_Controller {
	public function list_set_time(){

        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'enclitic';
        $data['menu'] = $name_title_by_method;
       
        $data['element_event'] = null;

        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->glb_info_route;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $this->lang->load('enclitic_lang', 'vietnam');
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('enclitic/template_delete_push_unpush', $data, true);
        $this->load->view('enclitic/'.$name_title_by_method, $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
    public function list_examination(){

        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'enclitic';
        $data['menu'] = $name_title_by_method;
       
        $data['element_event'] = null;

        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->glb_info_route;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $this->lang->load('enclitic_lang', 'vietnam');
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('enclitic/template_delete_push_unpush', $data, true);
        $this->load->view('enclitic/'.$name_title_by_method, $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
    
    
    private function get_css_js_basic() {
        $user = $this->session->userdata('user');
        $data = array('title' => 'Patient', 'menu' => 'category_n', 'groupMenu' => 'category',
            'css' => array(
                base_url('assets/css/font.css'),
                base_url('assets/css/app.v1.css'),
                base_url('assets/css/style.css'), //customer css style
                base_url('assets/js/datatables/datatables.css'), //dataTable
                base_url('assets/js/datepicker/datepicker.css'), //date_input
                base_url('assets/css/header.css')),
            'js' => array(
                //'./assets/js/app.v1.js',
                base_url('assets/js/datatables/jquery.dataTables.min.js'), //dataTable
                base_url('assets/js/datepicker/bootstrap-datepicker.js'), //date_input
                // './assets/js/datatables/demo.js', //dataTable
                base_url('assets/js/app.plugin.js'),
                'basic_just_demo'=>base_url('assets/js/list/basic_just_demo.js')),
            'user' => $user
        );
        return $data;
    }
	private function is_get_layout(){
		return true;
	}
}