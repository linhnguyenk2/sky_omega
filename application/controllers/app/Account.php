<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account extends AppAuthControler
{

    public function basic()
    {
        
        $data = $this->get_css_js_basic();
        $data['title'] = 'Quản lý tài khoản';
        $data['groupMenu'] = 'manage_user';
        $data['menu'] = 'list_users';
        
        
        $this->load->view('header', $data);
        $this->load->view('nav/topbar', $data);
        $this->load->view('nav/leftbar', $data);
        
        
        $data['template_delete_push_unpush'] = $this->load->view('user/template_delete_push_unpush', '', true);
        $this->load->view('user/list_user', $data);
        
        $this->load->view('footer', $data);
    }

    protected function get_css_js_basic()
    {
        $data = parent::get_css_js_basic();
        $data['js'][] = base_url('assets/js/user/basic_user.js');
        return $data;
    }

    public function profile()
    {
        // var_dump($_REQUEST);
        $this->load->model('User_model', 'user_model');
        $user = $this->session->userdata('user');
        
        if (isset($_FILES['file'])) {
            $file_name = $_FILES['file']['name'];
            $error_is = $_FILES['file']['error'];
            $file_type = $_FILES['file']['type']; // returns the mimetype
            $file_size = $_FILES['file']['size']; // returns the mimetype
            $file_type = strtolower($file_type);
            $file_extention = str_replace("image/", "", $file_type);
            
            $size_limit = 1024 * 1024 * 5; // size limit 5MB
            
            $target_path = BASEPATH . '../uploads/avatas';
            $target_url = 'uploads/avatas';
            $name_file = $user['uId'] . '_' . time() . rand(1, 10) . "." . $file_extention; // userId + random file
            $target_url_public = $target_url . '/' . $name_file;
            $direction_path = $target_path . '/' . $name_file;
            
            $return['status'] = false;
            $return['link'] = '';
            $return['msg'] = '';
            
            if ($error_is != 0) {
                // list message error when upload
                $phpFileUploadErrors = array(
                    0 => 'There is no error, the file uploaded with success',
                    1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
                    2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
                    3 => 'The uploaded file was only partially uploaded',
                    4 => 'No file was uploaded',
                    6 => 'Missing a temporary folder',
                    7 => 'Failed to write file to disk.',
                    8 => 'A PHP extension stopped the file upload.'
                );
                $return['msg'] = $phpFileUploadErrors[$error_is];
            } else {
                
                if ($size_limit < $file_size) {
                    echo 'Limit size max size = ' . ($size_limit / 1024 / 1024) . "MB";
                    die();
                }
                $allowed = array(
                    "image/jpeg",
                    "image/png"
                );
                
                if (! in_array($file_type, $allowed)) {
                    $error_message = 'Only jpg and png files are allowed.';
                    // echo $error_message;
                    $return['msg'] = $error_message;
                } else {
                    if (move_uploaded_file($_FILES["file"]["tmp_name"], $direction_path)) {
                        // upload success
                        $error_message = "Your profile picture has successfully been updated";
                        $return['link'] = $target_url_public;
                        $return['status'] = true;
                        $return['msg'] = $error_message;
                        
                        // update model
                        $this->user_model->updateAvata($user['uId'], $target_url_public);
                        // update session avatar
                        $avata_old = $user['uAvatar']; // uploads/avatas/1.jpg
                                                       // remove uplaods/avatas/
                        $name_file_old = str_replace($target_url, '', $avata_old);
                        unlink($target_path . $name_file_old); // remove file old
                        
                        $user['uAvatar'] = $target_url_public;
                        $this->session->set_userdata('user', $user);
                    } else {
                        $return['msg'] = "oh noes.. there was an error :( Please do try again!";
                    }
                }
            }
            echo json_encode($return);
            // end upload file for picture profile
            die();
        }
        
        $data = $this->get_css_js_basic();
        
        $data['js'][] = base_url('assets/js/user/script_profile.js');
        $data['css'][] = base_url('assets/css/si_input_form.css');
        
        $data['title'] = 'Profile';
        $data['groupMenu'] = 'system';
        $data['menu'] = 'profile';
        
        // $user = $this->session->userdata('user');
        // var_dump($user);
        $data['user'] = $user;
        
        $info = $this->user_model->getProfile($user['uId']);
        $data['profile'] = [];
        if (sizeof($info) > 0) {
            $data['profile'] = $info[0];
        }
        
        // var_dump($info);
        
        
        $this->load->view('header', $data);
        $this->load->view('nav/topbar', $data);
        $this->load->view('nav/leftbar', $data);
        
        // $this->load->view('user/profile', $data);
        $this->load->view('template_si/36_profile', $data);
        
        $this->load->view('footer', $data);
    }
    
    protected function getLanguageFileName()
    {
        return 'manage_user_lang';
    }
}
