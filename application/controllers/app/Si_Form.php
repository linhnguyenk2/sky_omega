<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Si_Form extends AppAuthControler {

    protected function get_css_js_basic() {
        $data= parent::get_css_js_basic();
        $data['js'][]=base_url('assets/js/form/script_form.js');
        $data['js'][]=base_url('assets/js/template_si/form_submit.js');
        $data['js'][]=base_url('assets/js/form/script_form.js');
        return $data;
    }
    private function is_get_layout() {
        if ((isset($_GET['layout']) ? $_GET['layout'] : null) != 'none') {
            return true;
        }
        return false;
    }

    public function index_form(){
        $data = $this->get_css_js_basic();
        $data['title'] = 'List Template';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'index_form';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

        
        //$data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', '', true);
        $this->load->view('form/index', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_1(){
        
//        echo CI_VERSION;die;
        //die;
        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu đăng ký khám bệnh';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_1';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

        
        $data['content_of_form']= $this->load->view('template_si/1','',true);
        $this->load->view('form/show_1', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_2(){
        
        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu khám nhũ';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_2';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

        
        $data['content_of_form']= $this->load->view('template_si/2','',true);
        $this->load->view('form/show_2', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_3(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu khám hiếm muộn';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_3';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

        
        $data['content_of_form']= $this->load->view('template_si/3','',true);
        $this->load->view('form/show_3', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_4(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu khám bệnh mãn kinh';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_4';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

        
        $data['content_of_form']= $this->load->view('template_si/4','',true);
        $this->load->view('form/show_4', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_5(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu khám khh gia đình';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_5';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

        
        $data['content_of_form']= $this->load->view('template_si/5','',true);
        $this->load->view('form/show_5', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_6(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu khám phụ khoa';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_6';
        

        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }

        
        $data['content_of_form']= $this->load->view('template_si/6','',true);
        $this->load->view('form/show_6', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_7(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu khám thai';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_7';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/7','',true);
        $this->load->view('form/show_7', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_8(){
        echo 'Empty';die;
        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu khám khh gia đình';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_8';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/8','',true);
        $this->load->view('form/show_8', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_9(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu chỉ định dịch vụ';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_9';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/9','',true);
        $this->load->view('form/show_9', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_10(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Đơn thuốc';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_10';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/10','',true);
        $this->load->view('form/show_10', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_11(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'TPCN - MP -VTYT';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_11';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/11','',true);
        $this->load->view('form/show_11', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_12(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu thu tiền tạm ứng điều trị nội trú';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_12';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/12','',true);
        $this->load->view('form/show_12', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_13(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu thanh toán ra viện';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_13';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/13','',true);
        $this->load->view('form/show_13', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_14(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Hóa đơn giá trị gia tăng';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_14';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/14','',true);
        $this->load->view('form/show_14', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_15(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Xv giá phòng và giường nội trú điều trị bệnh nhân';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_15';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/15','',true);
        $this->load->view('form/show_15', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_16(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu khám bệnh examination form';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_16';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/16','',true);
        $this->load->view('form/show_16', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_17(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Đơn đề nghị thực hiện kỹ thuật hỗ trợ sinh sản với phương pháp xin trứng';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_17';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/17','',true);
        $this->load->view('form/show_17', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_18(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Bản cam kết';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_18';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/18','',true);
        $this->load->view('form/show_18', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_19(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Đơn tự nguyện cho trứng';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_19';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/19','',true);
        $this->load->view('form/show_19', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_20(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Đơn xin tinh trùng (vô danh)';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_20';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/20','',true);
        $this->load->view('form/show_20', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_21(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Đơn đề nghị thực hiện kỹ thuật hỗ trợ sinh sản';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_21';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/21','',true);
        $this->load->view('form/show_21', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_22(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Đơn tự nguyện cho tinh trùng';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_22';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/22','',true);
        $this->load->view('form/show_22', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_23(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Tờ cam kết thực hiện thụ tinh trong ống nghiệm (ivf và icsi)';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_23';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/23','',true);
        $this->load->view('form/show_23', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_24(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Hồ sơ theo dõi hỗ trợ sinh sản';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_24';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/24','',true);
        $this->load->view('form/show_24', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_25(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phác đồ điều trị';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_25';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/25','',true);
        $this->load->view('form/show_25', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_26(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Bệnh án phụ khoa';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_26';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/26','',true);
        $this->load->view('form/show_26', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_27(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu đánh giá tình trạng dinh dưỡng';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_27';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/27','',true);
        $this->load->view('form/show_27', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_28(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu chăm sóc';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_28';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/28','',true);
        $this->load->view('form/show_28', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_29(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Tờ điều trị';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_29';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/29','',true);
        $this->load->view('form/show_29', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_30(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Bệnh án sơ sinh';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_30';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/30','',true);
        $this->load->view('form/show_30', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_31(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu thực hiện và công khai thuốc, dịch truyền';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_31';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/31','',true);
        $this->load->view('form/show_31', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_32(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu chăm sóc - theo dõi';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_32';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/32','',true);
        $this->load->view('form/show_32', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_33(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Bảng đánh giá tuổi thai';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_33';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/33','',true);
        $this->load->view('form/show_33', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_34(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Bệnh án sản khoa';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_34';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/34','',true);
        $this->load->view('form/show_34', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_35(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Phiếu đánh giá tình trạng dinh dưỡng';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_35';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/35','',true);
        $this->load->view('form/show_35', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }
    public function form_36(){

        $data = $this->get_css_js_basic();
        $data['title'] = 'Hồ sơ nhân viên';
        $data['groupMenu'] = 'form';
        $data['menu'] = 'form_36';
        
    
        if ($this->is_get_layout()) {
            
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
    
        
        $data['content_of_form']= $this->load->view('template_si/36','',true);
        $data['content_of_form_edit']= $this->load->view('template_si/36_edit','',true);
        $this->load->view('form/show_36', $data);
    
        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
       
    }

}