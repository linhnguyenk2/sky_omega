<?php
defined('BASEPATH') or exit('No direct script access allowed');

class List_service_group extends AppAuthControler
{
    function __construct()
    {
        parent::__construct();
    }
    
    protected function getListJavascript()
    {
        $listJavasript = parent::getListJavascript();
        $listJavasript[] = base_url('assets/js/api/list_function_use.js');
        $listJavasript[] = base_url('assets/js/api/list_service_group.js');
        return $listJavasript;
    }

    protected function getListCSS()
    {
        return parent::getListCSS();
    }

    public function list_service_group()
    {
        $html = $this->getTemplate();
        echo $html;
    }

    protected function getContentViewName()
    {
        $name_title_by_method = $this->router->fetch_method();
        return 'list/' . $name_title_by_method;
    }

    protected function getActiveMenuId()
    {
        return "menu_10";
    }

    protected function getLanguageFileName()
    {
        return 'manage_list_lang';
    }
    
}