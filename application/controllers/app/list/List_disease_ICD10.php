<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class List_disease_ICD10 extends AppAuthControler {

    function __construct() {
        parent::__construct();
    }
    
    protected function get_css_js_basic() {
        $data= parent::get_css_js_basic();
        $data['js'][]=base_url('assets/js/api/wraper.js');
        $data['js'][]=base_url('assets/js/api/status_code.js');
        $data['js'][]=base_url('assets/js/api/select_basic.js');
        $data['js'][]=base_url('assets/js/api/create_page.js');
        $data['js'][]=base_url('assets/js/api/list_function_use.js');
        $data['css'][]=base_url('assets/css/api_list.css');
        return $data;
    }
    

    public function list_disease_icd10() {
        $data = $this->get_css_js_basic();

        $data['js'][] = base_url('assets/js/api/list_disease_ICD10.js');
        $name_title_by_method = $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_' . $name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = $name_title_by_method;

        $data['element_event'] = null;

        $data['glb_info_route_view'] = null;
        $this->load->view('header', $data);
        $this->load->view('nav/topbar', $data);
        $this->load->view('nav/leftbar', $data);
        
        $this->lang->load('manage_list_lang', 'vietnam');
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/' . $name_title_by_method, $data);

        $this->load->view('footer', $data);
    }


}
