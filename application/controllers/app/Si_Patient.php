<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Si_Patient extends CI_Controller {
    
    public function patient_information($user_id_get=null){
        $data = $this->get_css_js_basic();
        unset($data['js']['basic_just_demo']);
        $data['js']['patient_infomation']=base_url('assets/js/list/patient_infomation.js');
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'patient';
        $data['menu'] = $name_title_by_method;
        $data['user_id_get'] = $user_id_get;
        // $data['element_event'] = $this->list_event;
        // $listrole['list_supllier']=[
        //     'get'=>'ok',
        //     'push'=>'ok',
        //     'unpush'=>'ok',
        //     'edit'=>'ok',
        //     'delete'=>'ok',
        //     'add'=>'ok'
        // ];
        // $data['element_event'] = $listrole;
        $data['element_event'] = null;


        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->glb_info_route;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $this->lang->load('patient_lang', 'vietnam');
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('patient/template_delete_push_unpush', $data, true);
        $this->load->view('patient/'.$name_title_by_method, $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
	
	public function maternity_hospital(){

        $data = $this->get_css_js_basic();
        unset($data['js']['basic_just_demo']);
        $data['js']['maternity_hospital']=base_url('assets/js/list/maternity_hospital.js');
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'patient';
        $data['menu'] = $name_title_by_method;
        // $data['element_event'] = $this->list_event;
        // $listrole['list_supllier']=[
        //     'get'=>'ok',
        //     'push'=>'ok',
        //     'unpush'=>'ok',
        //     'edit'=>'ok',
        //     'delete'=>'ok',
        //     'add'=>'ok'
        // ];
        // $data['element_event'] = $listrole;
        $data['element_event'] = null;

        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->glb_info_route;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $this->lang->load('patient_lang', 'vietnam');
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('patient/template_delete_push_unpush', $data, true);
        $this->load->view('patient/'.$name_title_by_method, $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
    
    
    private function get_css_js_basic() {
        $user = $this->session->userdata('user');
        $data = array('title' => 'Patient', 'menu' => 'category_n', 'groupMenu' => 'category',
            'css' => array(
                base_url('assets/css/font.css'),
                base_url('assets/css/app.v1.css'),
                base_url('assets/css/style.css'), //customer css style
                base_url('assets/js/datatables/datatables.css'), //dataTable
                base_url('assets/js/datepicker/datepicker.css'), //date_input
                base_url('assets/css/header.css')),
            'js' => array(
                //'./assets/js/app.v1.js',
                base_url('assets/js/datatables/jquery.dataTables.min.js'), //dataTable
                base_url('assets/js/datepicker/bootstrap-datepicker.js'), //date_input
                // './assets/js/datatables/demo.js', //dataTable
                base_url('assets/js/app.plugin.js'),
                'basic_just_demo'=>base_url('assets/js/list/basic_just_demo.js')),
            'user' => $user
        );
        return $data;
    }
	private function is_get_layout(){
		return true;
	}
}