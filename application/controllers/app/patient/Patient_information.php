<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_information extends AppAuthControler {

    function __construct() {
        parent::__construct();
    }
    
    protected function get_css_js_basic() {
        $data= parent::get_css_js_basic();
        // $data['js'][]=base_url('assets/js/api/wraper.js');
        // $data['js'][]=base_url('assets/js/api/status_code.js');
        // $data['js'][]=base_url('assets/js/api/select_basic.js');
        // $data['js'][]=base_url('assets/js/api/create_page.js');
        // $data['js'][]=base_url('assets/js/api/list_function_use.js');
        $data['js'][]=base_url('assets/js/receive/list_function_use_patient.js');
        $data['css'][]=base_url('assets/css/api_list.css');
        return $data;
    }
    
    public function patient_information($patient_id_get=null) {
        $data = $this->get_css_js_basic();
        unset($data['js']['basic_just_demo']);
        $data['js']['patient_information']=base_url('assets/js/patient/patient_information.js');
        $data['js']['patient_information_popup']=base_url('assets/js/patient/patient_information_popup.js');
        $data['js']['patient_information_tab_menu1']=base_url('assets/js/patient/patient_information_tab_menu.js');
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'patient';
        $data['menu'] = $name_title_by_method;
        $data['patient_id_get'] = $patient_id_get;

        $data['element_event'] = null;

        // $data['glb_info_route_view']= $this->glb_info_route;
        $data['glb_info_route_view']= null;
        $this->load->view('header', $data);
        $this->load->view('nav/topbar', $data);
        $this->load->view('nav/leftbar', $data);
        
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $this->lang->load('patient_lang', 'vietnam');
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('patient/template_delete_push_unpush', $data, true);
        $this->load->view('patient/'.$name_title_by_method, $data);
        $this->load->view('footer', $data);
    }


}
