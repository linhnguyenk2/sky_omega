<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Add_services extends AppAuthControler {

    function __construct() {
        parent::__construct();
    }
    
    protected function get_css_js_basic() {
        $data= parent::get_css_js_basic();
        // $data['js'][]=base_url('assets/js/api/wraper.js');
        // $data['js'][]=base_url('assets/js/api/status_code.js');
        // $data['js'][]=base_url('assets/js/api/select_basic.js');
        // $data['js'][]=base_url('assets/js/api/create_page.js');
        // $data['js'][]=base_url('assets/js/api/list_function_use.js');
        $data['js'][]=base_url('assets/js/receive/list_function_use_patient.js');
        $data['js'][]=base_url('assets/js/api/add_services.js');
        $data['css'][]=base_url('assets/css/api_list.css');
        return $data;
    }
    
    public function add_services()
    {
        $html = $this->getTemplate();
        echo $html;
    }
    protected function getContentViewName()
    {
        $name_title_by_method = $this->router->fetch_method();
        return 'patient/' . $name_title_by_method;
    }
    protected function getLanguageFileName()
    {
        return 'patient/add_services';
    }
    protected function getActiveMenuId()
    {
        return "menu_8_1";
    }
    protected function getListJavascript()
    {
        $listJavasript = parent::getListJavascript();
        /*$listJavasript[] = base_url('assets/js/receive/list_function_use_patient.js');
        $listJavasript[] = base_url('assets/js/warehouse/warehouse_list.js');*/
        return $listJavasript;
    }

    protected function getListCSS()
    {
        return parent::getListCSS();
    }

}
