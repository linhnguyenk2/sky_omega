<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Si_List_Test extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->lang->load('manage_list_lang', 'vietnam');
    }
    public function list_medicine() {
        
        $data = $this->get_css_js_basic();
//        $data['title'] = 'Danh mục Quốc gia';
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_medicine';
        // 
        $listrole['list_medicine']=[
            'get'=>'ok',
            'push'=>'ok',
            'unpush'=>'ok',
            'edit'=>'ok',
            'delete'=>'ok',
            'add'=>'ok'
        ];
        $data['element_event'] = $listrole;


        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->listMenuGrantAccess;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_medicine_1', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }
    public function list_contraindication() {
        
        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_contraindication';
        // 
        $listrole['list_contraindication']=[
            'get'=>'ok',
            'push'=>'ok',
            'unpush'=>'ok',
            'edit'=>'ok',
            'delete'=>'ok',
            'add'=>'ok'
        ];
        $data['element_event'] = $listrole;


        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->listMenuGrantAccess;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_contraindication', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }

    }
    public function list_insurrance_company(){
        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_insurrance_company';
        // 
        // $listrole['list_insurrance_company']=[
        //     'get'=>'ok',
        //     'push'=>'ok',
        //     'unpush'=>'ok',
        //     'edit'=>'ok',
        //     'delete'=>'ok',
        //     'add'=>'ok'
        // ];
        // $data['element_event'] = $listrole;
        $data['element_event'] = null;


        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->listMenuGrantAccess;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_insurrance_company', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
    public function list_health_insurrance_card(){
        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_health_insurrance_card';
        // 
        // $listrole['list_health_insurrance_card']=[
        //     'get'=>'ok',
        //     'push'=>'ok',
        //     'unpush'=>'ok',
        //     'edit'=>'ok',
        //     'delete'=>'ok',
        //     'add'=>'ok'
        // ];
        // $data['element_event'] = $listrole;
        $data['element_event'] = null;


        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->listMenuGrantAccess;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_health_insurrance_card', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }

    public function list_supllier(){
        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = 'list_supllier';
        // 
        // $listrole['list_supllier']=[
        //     'get'=>'ok',
        //     'push'=>'ok',
        //     'unpush'=>'ok',
        //     'edit'=>'ok',
        //     'delete'=>'ok',
        //     'add'=>'ok'
        // ];
        // $data['element_event'] = $listrole;
        $data['element_event'] = null;


        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->listMenuGrantAccess;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/list_supllier', $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
    public function list_patient(){
        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'enclitic';
        $data['menu'] = $name_title_by_method;
        // 
        // $listrole['list_supllier']=[
        //     'get'=>'ok',
        //     'push'=>'ok',
        //     'unpush'=>'ok',
        //     'edit'=>'ok',
        //     'delete'=>'ok',
        //     'add'=>'ok'
        // ];
        // $data['element_event'] = $listrole;
        $data['element_event'] = null;


        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->listMenuGrantAccess;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        //$this->load->view('list/'.$name_title_by_method, $data);
        $this->load->view('enclitic/'.$name_title_by_method, $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
    public function list_medical_record(){
        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = $name_title_by_method;
        // 
        // $listrole['list_supllier']=[
        //     'get'=>'ok',
        //     'push'=>'ok',
        //     'unpush'=>'ok',
        //     'edit'=>'ok',
        //     'delete'=>'ok',
        //     'add'=>'ok'
        // ];
        // $data['element_event'] = $listrole;
        $data['element_event'] = null;


        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->listMenuGrantAccess;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/'.$name_title_by_method, $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
    
    
    
    public function list_place_transfer(){

        $data = $this->get_css_js_basic();
        //unset($data['js']['basic_just_demo']);
        //$data['js']['maternity_hospital']=base_url('assets/js/list/maternity_hospital.js');
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = $name_title_by_method;
        // $data['element_event'] = $this->list_event;
        // $listrole['list_supllier']=[
        //     'get'=>'ok',
        //     'push'=>'ok',
        //     'unpush'=>'ok',
        //     'edit'=>'ok',
        //     'delete'=>'ok',
        //     'add'=>'ok'
        // ];
        // $data['element_event'] = $listrole;
        $data['element_event'] = null;

        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->glb_info_route;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/'.$name_title_by_method, $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
    public function list_place_introduction(){

        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = $name_title_by_method;
        $data['element_event'] = null;

        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->glb_info_route;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/'.$name_title_by_method, $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
    public function list_method_birth(){

        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = $name_title_by_method;
       
        $data['element_event'] = null;

        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->glb_info_route;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/'.$name_title_by_method, $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
	public function lis_foreigner(){

        $data = $this->get_css_js_basic();
        $name_title_by_method= $this->router->fetch_method();
        $data['title'] = $this->lang->line('title_'.$name_title_by_method);
        $data['groupMenu'] = 'category';
        $data['menu'] = $name_title_by_method;
       
        $data['element_event'] = null;

        if ($this->is_get_layout()) {
            // $data['glb_info_route_view']= $this->glb_info_route;
            $data['glb_info_route_view']= null;
            $this->load->view('header', $data);
            $this->load->view('nav/topbar', $data);
            $this->load->view('nav/leftbar', $data);
        }
        // $data['list_lang'] = $this->list_lang;//list lang content for view
        $data['list_lang'] = $this->lang;
        $data['template_delete_push_unpush'] = $this->load->view('list/template_delete_push_unpush', $data, true);
        $this->load->view('list/'.$name_title_by_method, $data);

        if ($this->is_get_layout()) {
            $this->load->view('footer', $data);
        }
    }
    
    
    private function get_css_js_basic() {
        $user = $this->session->userdata('user');
        $data = array('title' => 'Patient', 'menu' => 'category_n', 'groupMenu' => 'category',
            'css' => array(
                base_url('assets/css/font.css'),
                base_url('assets/css/app.v1.css'),
                base_url('assets/css/style.css'), //customer css style
                base_url('assets/js/datatables/datatables.css'), //dataTable
                base_url('assets/js/datepicker/datepicker.css'), //date_input
                base_url('assets/css/header.css')),
            'js' => array(
                //'./assets/js/app.v1.js',
                base_url('assets/js/datatables/jquery.dataTables.min.js'), //dataTable
                base_url('assets/js/datepicker/bootstrap-datepicker.js'), //date_input
                // './assets/js/datatables/demo.js', //dataTable
                base_url('assets/js/app.plugin.js'),
                'basic_just_demo'=>base_url('assets/js/list/basic_just_demo.js')),
            'user' => $user
        );
        return $data;
    }
	private function is_get_layout(){
		return true;
	}

}
