<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends AppAuthControler
{
    
    protected function get_css_js_basic()
    {
        $data = parent::get_css_js_basic();
        $data['js'][] = base_url('assets/js/charts/easypiechart/jquery.easy-pie-chart.js');
        $data['js'][] = base_url('assets/js/charts/sparkline/jquery.sparkline.min.js');
        $data['js'][] = base_url('assets/js/charts/flot/jquery.flot.min.js');
        $data['js'][] = base_url('assets/js/charts/flot/jquery.flot.tooltip.min.js');
        $data['js'][] = base_url('assets/js/charts/flot/jquery.flot.resize.js');
        $data['js'][] = base_url('assets/js/charts/flot/jquery.flot.grow.js');
        $data['js'][] = base_url('assets/js/charts/flot/jquery.flot.orderBars.js');
        $data['js'][] = base_url('assets/js/charts/flot/jquery.flot.pie.min.js');
        $data['js'][] = base_url('assets/js/datatables/jquery.dataTables.min.js');
        $data['js'][] = base_url('assets/js/charts/flot/demo.js');
        $data['js'][] = base_url('assets/js/calendar/demo.js');
        $data['js'][] = base_url('assets/js/calendar/bootstrap_calendar.js');
        $data['js'][] = base_url('assets/js/sortable/jquery.sortable.js');
        $data['js'][] = base_url('assets/js/handle.js');
        $data['css'][] = base_url('assets/js/calendar/bootstrap_calendar.css');
        return $data;
    }
    
    public function index()
    {
        $html = $this->getTemplate();
        echo $html;
    }
    
    protected function getActiveMenuId()
    {
        return  "menu_1";
    }
    
    protected function getTitle()
    {
        return  'Trang chủ';
    }
}