<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Index extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $user = $this->session->userdata('user');
        if (empty($user)) {
            redirect('/signin');
        } else {
            redirect('/dashboard');
        }
    }
}