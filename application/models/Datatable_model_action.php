<?php

class Datatable_model_action extends CI_Model {

    public function __construct() {
        
        parent::__construct();
        $this->load->helper(array('text', 'url', 'date','util'));
        $this->load->library('session');
       // $this->load->library('email');
    }

    public function getDataTable($request, $conn, $table, $primaryKey, $columns,$where_in='') {
        
        $bindings = array();
        
        $limit = $this->limit($request, $columns);
        $order = $this->order($request, $columns);
        $where = $this->filter($request, $columns, $bindings);
        if(empty($where)){
            if(!empty($where_in)){
                $where .= ' WHERE'.$where_in;
            }
        }else{
            if(!empty($where_in)){
                $where .= ' AND' .$where_in;
            }
        }

        $bind =[];
        foreach($bindings as $vl){
            $bind[]=$vl['val'];
        }

        $sql ="SELECT `" . implode("`, `", $this->pluck($columns, 'db')) . "`
        FROM `$table`
        $where
        $order
        $limit";


        $data = $this->db->query($sql, ($bind))->result_array();

       
        $sql = "SELECT COUNT(`{$primaryKey}`)
        FROM   `$table`
        $where";

        $resFilterLength = $this->db->query($sql, ($bind))->result_array();
        
        $recordsFiltered = $resFilterLength[0]["COUNT(`{$primaryKey}`)"];
        
        $sql ="SELECT COUNT(`{$primaryKey}`)
        FROM   `$table`";
        $resTotalLength = $this->db->query($sql, ($bind))->result_array();

        $recordsTotal = $resTotalLength[0]["COUNT(`{$primaryKey}`)"];
        
        /*
         * Output
         */
        return array(
            "draw" => isset($request['draw']) ?
                    intval($request['draw']) :
                    0,
            "recordsTotal" => intval($recordsTotal),
            "recordsFiltered" => intval($recordsFiltered),
            "data" => $this->data_output($columns, $data)
        );
    }

    public function add_record($list_colum, $list_data, $table_name) {

        foreach ($list_colum as $key => $value) {
            $this->$value = $list_data[$key];
        }

        $this->db->insert($table_name, $this);
    }

    public function edit_record($name_id, $list_colum, $vl_id_column, $list_data, $table_name) {
       
        $list_data = array_values($list_data);
        foreach ($list_colum as $key => $value) {
            $this->$value = $list_data[$key];
        }
       
        $this->db->update($table_name, $this, array($name_id => $vl_id_column));
    }

    public function delete_record($name_id, $list_id_input, $table_name) {

        $names = $list_id_input;
        $this->db->where_in($name_id , $names);
        $this->db->delete($table_name);

    }

    public function push_record($name_id, $list_id_input, $table_name) {

        foreach ($list_id_input as $key => $value) {
            $this->db->set('stat', "1");
            $this->db->where($name_id, (int) $value);
            $this->db->update($table_name); // gives UPDATE mytable SET field = field+1 WHERE id = 2
        }
		
    }

    public function unpush_record($name_id, $list_id_input, $table_name) {
        foreach ($list_id_input as $key => $value) {
            $this->db->set($name_id . 'Stat', "C");
            $this->db->where($name_id . 'Id', (int) $value);
            $this->db->update($table_name); // gives UPDATE mytable SET field = field+1 WHERE id = 2
        }
    }

    private function limit($request, $columns) {
        $limit = '';
        if (isset($request['start']) && $request['length'] != -1) {
            $limit = "LIMIT " . intval($request['start']) . ", " . intval($request['length']);
        }
        return $limit;
    }

    private function order($request, $columns) {
        $order = '';
        if (isset($request['order']) && count($request['order'])) {
            $orderBy = array();
            $dtColumns = $this->pluck($columns, 'dt');
            for ($i = 0, $ien = count($request['order']); $i < $ien; $i++) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if ($requestColumn['orderable'] == 'true') {
                    $dir = $request['order'][$i]['dir'] === 'asc' ?
                            'ASC' :
                            'DESC';
                    $orderBy[] = '`' . $column['db'] . '` ' . $dir;
                }
            }
            if (count($orderBy)) {
                $order = 'ORDER BY ' . implode(', ', $orderBy);
            }
        }
        return $order;
    }

    private function filter($request, $columns, &$bindings) {
        // var_dump($request, $columns, $bindings);die;
        $globalSearch = array();
        $columnSearch = array();
        $dtColumns = $this->pluck($columns, 'dt');
        if (isset($request['search']) && $request['search']['value'] != '') {
            $str = $request['search']['value'];
            $str =utf8convert($str); //fix Error Number: 1271 Illegal mix of collations for operation 'like'
            for ($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if ($requestColumn['searchable'] == 'true') {
                    $binding = $this->bind($bindings, '%' . $str . '%', PDO::PARAM_STR);
                    $globalSearch[] = "`" . $column['db'] . "` LIKE " . $binding;
                }
            }
        }
        // Individual column filtering
        if (isset($request['columns'])) {
            for ($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                $str = $requestColumn['search']['value'];
                if ($requestColumn['searchable'] == 'true' &&
                        $str != '') {
                    $binding = $this->bind($bindings, '%' . $str . '%', PDO::PARAM_STR);
                    $columnSearch[] = "`" . $column['db'] . "` LIKE " . $binding;
                }
            }
        }
        // Combine the filters into a single string
        $where = '';
        if (count($globalSearch)) {
            $where = '(' . implode(' OR ', $globalSearch) . ')';
        }
        if (count($columnSearch)) {
            $where = $where === '' ?
                    implode(' AND ', $columnSearch) :
                    $where . ' AND ' . implode(' AND ', $columnSearch);
        }
        if ($where !== '') {
            $where = 'WHERE ' . $where;
        }
        //var_dump($where);die;
        return $where;
        
    }

    private function pluck($a, $prop) {
        $out = array();
        for ($i = 0, $len = count($a); $i < $len; $i++) {
            $out[] = $a[$i][$prop];
        }
        return $out;
    }

    private function bind(&$a, $val, $type) {
        //$key = ':binding_' . count($a);
        $key = '?';
        $a[] = array(
            'key' => $key,
            'val' => $val,
            'type' => $type
        );
        return $key;
    }

    private function sql_exec($db, $bindings, $sql = null) {
        // Argument shifting
        if ($sql === null) {
            $sql = $bindings;
        }
        $stmt = $db->prepare($sql);
        //echo $sql;
        // Bind parameters
        if (is_array($bindings)) {
            for ($i = 0, $ien = count($bindings); $i < $ien; $i++) {
                $binding = $bindings[$i];
                $stmt->bindValue($binding['key'], $binding['val'], $binding['type']);
            }
        }
        // Execute
        try {
            $stmt->execute();
        } catch (PDOException $e) {
            $this->fatal("An SQL error occurred: " . $e->getMessage());
        }
        // Return all
        return $stmt->fetchAll(PDO::FETCH_BOTH);
    }

    private function fatal($msg) {
        echo json_encode(array(
            "error" => $msg
        ));
        exit(0);
    }

    private function data_output($columns, $data) {

        $out = array();
        for ($i = 0, $ien = count($data); $i < $ien; $i++) {
            $row = array();
            for ($j = 0, $jen = count($columns); $j < $jen; $j++) {
                $column = $columns[$j];
                // Is there a formatter?
                if (isset($column['formatter'])) {
                    $row[$column['dt']] = $column['formatter']($data[$i][$column['db']], $data[$i]);
                } else {
                    $row[$column['dt']] = $data[$i][$columns[$j]['db']];
                }
            }
            $out[] = $row;
        }
        return $out;
    }

    private function db($conn) {
        if (is_array($conn)) {
            return $this->sql_connect($conn);
        }
        return $conn;
    }

    private function sql_connect($sql_details) {
        try {
            $db = @new PDO(
                    "mysql:host={$sql_details['host']};dbname={$sql_details['db']}", $sql_details['user'], $sql_details['pass'], array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
            );
        } catch (PDOException $e) {
            $this->fatal(
                    "An error occurred while connecting to the database. " .
                    "The error reported by the server was: " . $e->getMessage()
            );
        }
        return $db;
    }
}

?>