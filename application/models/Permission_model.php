<?php
class Permission_model extends CI_Model {
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();        
        $this->load->helper(array('text', 'url', 'date'));
        $this->load->library('session');
    }
    public function getListRole(){
        $this->db->where('stat', '1');
        // $this->db->or_where('rStat', 'O');
        $query = $this->db->get('sih_roles');
        // $dt = $query->result_array();
        // var_dump($dt);die;
        return $query->result_array();
    }
    public function db_role_get_list_screen($role_id){
        // $query = $this->db->get('sih_roles');
        $query = $this->db->select('id_screens as id_screens')->get_where('sih_role_screens', array('id_role' => $role_id,'stat'=>'1'));
        $lists= $query->result_array();
        //var_dump($lists);die;
        return $lists;
    }
    public function db_role_screen_update($id_role,$id_screen,$checked){
        
		$user = $this->session->userdata('user');
        
		if($checked=='true'){
            $checked='1';
        }else{
            $checked ='0';
        }
        $query = $this->db->select('id')->get_where('sih_role_screens', array('id_role' => $id_role,'id_screens'=>$id_screen),1);
        $lists= $query->result_array();
        //var_dump($lists,$checked,$id);die;
        if(sizeof($lists) >0){
            $id=$lists[0]['id'];
            //var_dump($id,$lists,$checked);die;
            //update
            $this->db->set('stat', $checked);
            $this->db->where('id', $id);
            $this->db->update('sih_role_screens'); 
        }else{
            //add
            $array = array(
                'id_role' => $id_role,
                'id_screens' => $id_screen,
                'stat' => $checked,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $user['uId'],
            );
            
            $this->db->set($array);
            $this->db->insert('sih_role_screens');
        }
        
    }
    public function db_get_screen_main(){


        $this->db->select('sId,sTitle,sRoute');
        
		$this->db->where('sRoute !=',null);
        $this->db->where('sRouteId',0);
        $this->db->where('sStat','O');
        $query = $this->db->get('sih_screens');
        $lists['main']= $query->result_array();


        $this->db->select('sId,sTitle,sRoute,sRouteId');
        
		$this->db->where('sRoute !=',null);
        $this->db->not_like('sRouteId','_');
        $this->db->where('sStat','O');
        $query = $this->db->get('sih_screens');
        $lists['sub']= $query->result_array();

		return $lists;
    }
    public function db_get_screen_sub_detail(){
        $this->db->select('id,title,route,routed_id');
        
		$this->db->where('route !=',null);
        $this->db->like('routed_id','_');
        $this->db->where('stat','1');
        $query = $this->db->get('sih_screens');
        $lists= $query->result_array();
        $list_reuslt=[];
        foreach ($lists as $key => $value) {
            $main_sub=explode('_',$value['routed_id']);
            $event=explode('/',$value['route']);

            $list_reuslt[$main_sub[1]][$event[sizeof($event)-1]] =$value['id'];
        }
        
		return $list_reuslt;
    }
	public function db_get_screens_where($colum,$content){
		//sih_screens
		$this->db->where($colum,$content);
		$this->db->where('stat','1');
        $query = $this->db->get('sih_screens');
		$lists= $query->result_array();
		return $lists;
	}
	
	/*
	select sih_role_screens.sId
	from sih_role_screens 
	INNER JOIN sih_screens on sih_screens.sRouteId=sih_role_screens.sId
	where sih_screens.sRouteId='60' and sih_role_screens.rId=2
	*/
	public function db_get_list_same_main_menu($sRouteId){
		
		$this->db->select('sih_role_screens.id_screens');
        $this->db->from('sih_role_screens');
        
        $this->db->join('sih_screens', 'sih_screens.routed_id = sih_role_screens.id_screens');
        $this->db->where('sih_screens.routed_id=', $sRouteId);
        $this->db->where('sih_screens.stat=', '1');
        $this->db->where('sih_role_screens.stat=', '1');
        $query = $this->db->get();
        $data = $query->result_array();
		
	}
}
?>
