<?php

class List_Action_model extends CI_Model {

    public function __construct() {
        // Call the CI_model constructor
        parent::__construct();
        $this->load->helper(array('text', 'url', 'date','util'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('loaddefaultselect_library');
        $this->load->model('Loaddefaultselect_library_model','cl_select_model');
    }

    public function getList() {
        
    }

    public function getDataTable($request, $conn, $table, $primaryKey, $columns) {
//        if($table=='sih_list_districts'){
//            return $this->getDataTable_district($request, $conn, $table, $primaryKey, $columns);
//        }
		$bindings = array();
        
		$limit = $this->limit($request, $columns);
        $order = $this->order($request, $columns);
        $where = $this->filter($request, $columns, $bindings);
       
	   $bind =[];
        foreach($bindings as $vl){
            $bind[]=$vl['val'];
        }
        
        
        $sql ="SELECT `" . implode("`, `", $this->pluck($columns, 'db')) . "`
        FROM `$table`
        $where
        $order
        $limit";

        $data = $this->db->query($sql, ($bind))->result_array();

        $sql = "SELECT COUNT(`{$primaryKey}`)
        FROM   `$table`
        $where";

        $resFilterLength = $this->db->query($sql, ($bind))->result_array();
        
		$recordsFiltered = $resFilterLength[0]["COUNT(`{$primaryKey}`)"];
        
		$sql ="SELECT COUNT(`{$primaryKey}`)
        FROM   `$table`";
        $resTotalLength = $this->db->query($sql, ($bind))->result_array();

        $recordsTotal = $resTotalLength[0]["COUNT(`{$primaryKey}`)"];
        //Output

        $data  =$this->data_output($columns, $data);

        $repo =$this->fileter_date_by_select_update($columns,$data);
        $data =$repo[0];
        $select_temp =$repo[1];
        
		
        return array(
            "draw" => isset($request['draw']) ?
                    intval($request['draw']) :
                    0,
            "recordsTotal" => intval($recordsTotal),
            "recordsFiltered" => intval($recordsFiltered),
            //"data" => $this->data_output($columns, $data),
            "data" =>  $data,
            "selectTem"=>$select_temp
        );
    }
    
    public function getDataTable_district($request, $conn, $table, $primaryKey, $columns) {

        $bindings = array();
        
        $limit = $this->limit($request, $columns);
        $order = $this->order($request, $columns);
        $where = $this->filter($request, $columns, $bindings);
       
	   $bind =[];
        foreach($bindings as $vl){
            $bind[]=$vl['val'];
        }
        $join='INNER JOIN sih_list_provinces ON sih_list_provinces.id=sih_list_districts.id_provinces';
        $join_where=' and sih_list_provinces';
        
        
        $sql ="SELECT `" . implode("`, `", $this->pluck($columns, 'db')) . "`
        FROM `$table`
        $where
            $join
        $order
        $limit";

        $data = $this->db->query($sql, ($bind))->result_array();

        $repo =$this->fileter_date_by_select($columns,$data);
        $data =$repo[0];
        $select_temp =$repo[1];

        $sql = "SELECT COUNT(`{$primaryKey}`)
        FROM   `$table`
        $where";

        $resFilterLength = $this->db->query($sql, ($bind))->result_array();
        
		$recordsFiltered = $resFilterLength[0]["COUNT(`{$primaryKey}`)"];
        
		$sql ="SELECT COUNT(`{$primaryKey}`)
        FROM   `$table`";
        $resTotalLength = $this->db->query($sql, ($bind))->result_array();

        $recordsTotal = $resTotalLength[0]["COUNT(`{$primaryKey}`)"];
        //Output
		
        return array(
            "draw" => isset($request['draw']) ?
                    intval($request['draw']) :
                    0,
            "recordsTotal" => intval($recordsTotal),
            "recordsFiltered" => intval($recordsFiltered),
            "data" => $this->data_output($columns, $data),
            "selectTem"=>$select_temp
        );
    }

    private function fileter_date_by_select($column,$data){
        //before return data need filter select option
        //var_dump($column,$data);
        $select_default=[];
        foreach ($column as $key => $value) {
            if(isset($value['select'])){
                $list_default =$this->cl_select_model->get_select_option($value['select']);
                $select_default[$value['select']]=$list_default;
                foreach ($data as $key_data => $value_data) {
                    $at_col=$value['db'];
                    $value_at = $value_data[$at_col];
                    $currentfilter = $this->loaddefaultselect_library->get_default($list_default, $value['select'],$value_at,'text');
                    $data[$key_data][$at_col] = $currentfilter;
                }
            }elseif(isset($value['select-default'])){
                $list_default =$this->cl_select_model->get_select_option($value['select-default']);
                $select_default[$value['select-default']]=$list_default;
                foreach ($data as $key_data => $value_data) {
                    $at_col=$value['db'];
                    //$value_at = $value_data[$at_col];
                    $value_at = $value_data[$at_col];
                    $value_at =0;
                    $currentfilter = $this->loaddefaultselect_library->get_default($list_default, $value['select-default'],$value_at,'text');
                    $data[$key_data][$at_col] = $currentfilter;
                }
            }
            # code...
        }
        return [$data,$select_default];
    }

    //select-none: not filter just get model for 3 table join
    //select-default: just return every is value for district 
    private function fileter_date_by_select_update($column,$data){
        //before return data need filter select option
        //var_dump($column,$data);die;
        $select_default=[];
        foreach ($column as $key => $value) {
            if(isset($value['select'])){
                // var_dump($value['select']);
                $list_default =$this->cl_select_model->get_select_option($value['select']);
                //var_dump($list_default);die;
                $select_default[$value['select']]=$list_default;
                foreach ($data as $key_data => $value_data) {
                    $at_col=$value['dt'];
                    $value_at = $value_data[$at_col];
                    $currentfilter = $this->loaddefaultselect_library->get_default($list_default, $value['select'],$value_at,'text');
                    $data[$key_data][$at_col] = $currentfilter;
                }
            }elseif(isset($value['select-none'])){
                //just get data
                $list_default_first =$this->cl_select_model->get_select_option($value['select-none']);
                $list_default = $list_default_first['compare'];
                unset($list_default_first['compare']);
                $select_default[$value['select-none']]=$list_default_first;
                foreach ($data as $key_data => $value_data) {
                    $at_col=$value['dt'];
                    $value_at = $value_data[$at_col];
                    $currentfilter = $this->loaddefaultselect_library->get_default($list_default, $value['select-none'],$value_at,'text');
                    $data[$key_data][$at_col] = $currentfilter;
                }

            }elseif(isset($value['select-default'])){
                $list_default =$this->cl_select_model->get_select_option($value['select-default']);
                $select_default[$value['select-default']]=$list_default;
                foreach ($data as $key_data => $value_data) {
                    $at_col=$value['dt'];
                    //$value_at = $value_data[$at_col];
                    $value_at = $value_data[$at_col];
                    $value_at =0;
                    $currentfilter = $this->loaddefaultselect_library->get_default($list_default, $value['select-default'],$value_at,'text');
                    $data[$key_data][$at_col] = $currentfilter;
                }
            }
            # code...
        }
        return [$data,$select_default];
    }

    public function add_record($list_colum, $list_data, $table_name) {
        
        foreach ($list_colum as $key => $value) {
            $this->$value = $list_data[$key];
        }

        $this->db->insert($table_name, $this);
        return $this->db->insert_id();
    }
    public function add_record_tb_cl($table_name,$list_data) {
        
        foreach ($list_data as $key => $value) {
            $this->$key = $value;
        }

        $this->db->insert($table_name, $this);
        return $this->db->insert_id();
    }

    public function edit_record($name_id, $list_colum, $vl_id_column, $list_data, $table_name) {
        // var_dump($name_id, $list_colum, $vl_id_column, $list_data, $table_name);die;
        $list_data = array_values($list_data);
        foreach ($list_colum as $key => $value) {
            $this->$value = $list_data[$key];
        }
        
        $this->db->update($table_name, $this, array($name_id => $vl_id_column));
    }
    public function edit_record_tb_cl($table_name,$list_data) {
        $name_id='id';
        $vl_name_id=$list_data['id'];
        unset($list_data['id']);
        //$list_data = array_values($list_data);
        foreach ($list_data as $key => $value) {
            $this->$key = $value;
        }
        $this->db->update($table_name, $this, array($name_id => $vl_name_id));
    }

    /**
     * edit table with id or auto add new value for table
     */
    public function edit_auto_record_tb_cl($table_name,$list_data) {
        $name_id='id';
        $vl_name_id=$list_data['id'];
        if($vl_name_id !='' && $vl_name_id!=null){
            unset($list_data['id']);
            foreach ($list_data as $key => $value) {
                $this->$key = $value;
            }
            $this->db->update($table_name, $this, array($name_id => $vl_name_id));
        }else{
            foreach ($list_data as $key => $value) {
                $this->$key = $value;
            }
            $this->db->insert($table_name, $this);
        }
    }

    public function edit_record_just_colum($table_name,$var_nameid,$var_valid,$var_namecl,$var_valcl){
        $this->db->set($var_namecl, $var_valcl);
        $this->db->where($var_nameid, $var_valid);
        $this->db->update($table_name);
        //$this->db->update($table_name, $this, array($name_id => $vl_id_column));
    }

    public function delete_record($name_id, $list_id_input, $table_name) {
        
		$names = $list_id_input;
        $this->db->where_in($name_id , $names);
        $this->db->delete($table_name);

    }

    public function push_record($name_id, $list_id_input, $table_name) {
    
		foreach ($list_id_input as $key => $value) {
            $this->db->set( 'stat', "1");
            $this->db->where($name_id , (int) $value);
            $this->db->update($table_name); // gives UPDATE mytable SET field = field+1 WHERE id = 2
        }
	}

    public function unpush_record($name_id, $list_id_input, $table_name) {
        foreach ($list_id_input as $key => $value) {
            $this->db->set('stat', "0");
            $this->db->where($name_id , (int) $value);
            $this->db->update($table_name); // gives UPDATE mytable SET field = field+1 WHERE id = 2
        }
    }
    
    private function limit($request, $columns) {
        $limit = '';
        if (isset($request['start']) && $request['length'] != -1) {
            $limit = "LIMIT " . intval($request['start']) . ", " . intval($request['length']);
        }
        return $limit;
    }

    private function order($request, $columns) {
        $order = '';
        if (isset($request['order']) && count($request['order'])) {
            $orderBy = array();
            $dtColumns = $this->pluck($columns, 'dt');
            for ($i = 0, $ien = count($request['order']); $i < $ien; $i++) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if ($requestColumn['orderable'] == 'true') {
                    $dir = $request['order'][$i]['dir'] === 'asc' ?
                            'ASC' :
                            'DESC';
                    $orderBy[] = '`' . $column['db'] . '` ' . $dir;
                }
            }
            if (count($orderBy)) {
                $order = 'ORDER BY ' . implode(', ', $orderBy);
            }
        }
        return $order;
    }

    private function filter($request, $columns, &$bindings) {
        // var_dump($request, $columns, $bindings);die;
        $globalSearch = array();
        $columnSearch = array();
        $dtColumns = $this->pluck($columns, 'dt');
        if (isset($request['search']) && $request['search']['value'] != '') {
            $str = $request['search']['value'];
            $str =utf8convert($str); //fix Error Number: 1271 Illegal mix of collations for operation 'like'
            for ($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if ($requestColumn['searchable'] == 'true') {
                    $binding = $this->bind($bindings, '%' . $str . '%', PDO::PARAM_STR);
                    $globalSearch[] = "`" . $column['db'] . "` LIKE " . $binding;
                }
            }
        }
        // Individual column filtering
        if (isset($request['columns'])) {
            for ($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                $str = $requestColumn['search']['value'];
                if ($requestColumn['searchable'] == 'true' &&
                        $str != '') {
                    $binding = $this->bind($bindings, '%' . $str . '%', PDO::PARAM_STR);
                    $columnSearch[] = "`" . $column['db'] . "` LIKE " . $binding;
                }
            }
        }
        // Combine the filters into a single string
        $where = '';
        if (count($globalSearch)) {
            $where = '(' . implode(' OR ', $globalSearch) . ')';
        }
        if (count($columnSearch)) {
            $where = $where === '' ?
                    implode(' AND ', $columnSearch) :
                    $where . ' AND ' . implode(' AND ', $columnSearch);
        }
        if ($where !== '') {
            $where = 'WHERE ' . $where;
        }
        //var_dump($where);die;
        return $where;
        
    }

    private function pluck($a, $prop) {
        $out = array();
        for ($i = 0, $len = count($a); $i < $len; $i++) {
            $out[] = $a[$i][$prop];
        }
        return $out;
    }

    private function bind(&$a, $val, $type) {
        //$key = ':binding_' . count($a);
        $key = '?';
        $a[] = array(
            'key' => $key,
            'val' => $val,
            'type' => $type
        );
        return $key;
    }

    private function sql_exec($db, $bindings, $sql = null) {
        // Argument shifting
        if ($sql === null) {
            $sql = $bindings;
        }
        $stmt = $db->prepare($sql);
        //echo $sql;
        // Bind parameters
        if (is_array($bindings)) {
            for ($i = 0, $ien = count($bindings); $i < $ien; $i++) {
                $binding = $bindings[$i];
                $stmt->bindValue($binding['key'], $binding['val'], $binding['type']);
            }
        }
        // Execute
        try {
            $stmt->execute();
        } catch (PDOException $e) {
            $this->fatal("An SQL error occurred: " . $e->getMessage());
        }
        // Return all
        return $stmt->fetchAll(PDO::FETCH_BOTH);
    }

    private function fatal($msg) {
        echo json_encode(array(
            "error" => $msg
        ));
        exit(0);
    }

    private function data_output($columns, $data) {

        $out = array();
        for ($i = 0, $ien = count($data); $i < $ien; $i++) {
            $row = array();
            for ($j = 0, $jen = count($columns); $j < $jen; $j++) {
                $column = $columns[$j];
                // Is there a formatter?
                if (isset($column['formatter'])) {
                    $row[$column['dt']] = $column['formatter']($data[$i][$column['db']], $data[$i]);
                } else {
                    $row[$column['dt']] = $data[$i][$columns[$j]['db']];
                }
            }
            $out[] = $row;
        }
        return $out;
    }

    private function db($conn) {
        var_dump($conn);die;
        if (is_array($conn)) {
            return $this->sql_connect($conn);
        }
        return $conn;
    }

    private function sql_connect($sql_details) {
        try {
            $db = @new PDO(
                    "mysql:host={$sql_details['host']};dbname={$sql_details['db']}", $sql_details['user'], $sql_details['pass'], array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
            );
        } catch (PDOException $e) {
            $this->fatal(
                    "An error occurred while connecting to the database. " .
                    "The error reported by the server was: " . $e->getMessage()
            );
        }
        return $db;
    }

    /*

      Old

     */

    /*
    public function signIn($email, $password) {
        $res = $this->db->query("select * from sih_users 
                                  where (uEmail={$this->db->escape($email)} 
                                        or uPhone={$this->db->escape($email)} )
                                    and (uPassword={$this->db->escape($password)} 
                                        or uPasswordNew={$this->db->escape($password)}) 
                                    and uStat='O' 
                                  limit 1")->result();

        foreach ($res as $row) {
            $info = array(
                'uId' => $row->uId,
                'uName' => $row->uName,
                'uPassChanged' => $row->uPassChanged,
                'uPhone' => $row->uPhone,
                'uRole' => $row->uRole,
                'uEmail' => $row->uEmail,
                'uCreatedAt' => $row->uCreatedAt
            );

            $this->session->set_userdata('user', $info);

            $jsonRes = array("info" => $info,
                "role" => $row->uRole,
                "ok" => 1);
            echo json_encode($jsonRes);
            return;
        }

        $jsonRes = array("info" => null,
            "ok" => 0);
        echo json_encode($jsonRes);
    }

    public function changePass($password) {
        $user = $this->session->userdata('user');
        if (empty($user)) {
            echo json_encode(array("ok" => '0'));
            return;
        }

        $this->db->query("update sih_users 
                             set uPassword={$this->db->escape($password)}, 
                                 uPasswordNew=NULL,
                                 uPassChanged=1 
                           where uId={$user['uId']} 
                           limit 1");
        if ($this->db->affected_rows()) {
            $info = array(
                'uId' => $user['uId'],
                'uName' => $user['uName'],
                'uPassChanged' => 1,
                'uPhone' => $user['uPhone'],
                'uRole' => $user['uRole'],
                'uEmail' => $user['uEmail'],
                'uCreatedAt' => $user['uCreatedAt']
            );

            $this->session->set_userdata('user', $info);
            echo json_encode(array("ok" => 1));
            return;
        }

        echo json_encode(array("ok" => 0));
    }

    public function getUsers($page) {
        $offset = ($page > 1 ? (intval($page) - 1 ) * 30 : 0);
        $query = $this->db->query("select u.uId, u.uName, u.uEmail, u.uPhone, 
                                          DATE_FORMAT(u.uCreatedAt,'%d/%m/%Y %h:%i %p') uCreatedAt, u.uStat 
                                     from sih_users u                                     
                                 order by u.uCreatedAt desc 
                                    limit 30 offset {$offset}");
        return $query->result();
    }
    */

}

?>