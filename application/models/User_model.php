<?php
class User_model extends CI_Model {
    public function __construct() {
        // Call the CI_model constructor
        parent::__construct();        
        $this->load->helper(array('text', 'url', 'date'));
        $this->load->library('session');
        $this->load->library('email');
    }    
        
    public function signIn($email, $password) {       
        $info=null;
        $res = $this->db->query("select * from sih_users 
                                  where (email={$this->db->escape($email)} 
                                        or phone={$this->db->escape($email)} )
                                    and (password={$this->db->escape(md5($password))} 
                                        
                                            ) 
                                    and stat='1' 
                                  limit 1")->result();
         
        foreach ($res as $row) {
            //set avata default if null
            if(empty($row->uAvatar)){
                $row->uAvatar = base_url('assets/images/avatar.jpg');
            }
            
            $info = array(
                    'uId'          => $row->id,                    
                    'rId'          => $row->id_role,                    
                    'ltId'          => $row->id_list_departments,                    
                    'uName'        => $row->name,        
                    //'uPassChanged' => $row->uPassChanged,    
                    'uPhone'       => $row->phone,                    
                    //'uRole'        => $row->uRole,
                    'uEmail'       => $row->email,                    
                    'uCreatedAt'   => $row->created_at,
                    'uAvatar'   => $row->avatar
               );
        }
        return $info;
    }   
    
    public function changePass($password) {              
        $user = $this->session->userdata('user');
        if (empty($user)) {            
            echo json_encode(array("ok" => '0'));
            return;
        }
                        
        $this->db->query("update sih_users 
                             set password={$this->db->escape($password)}, 
                           where id={$user['uId']} 
                           limit 1");
        if ($this->db->affected_rows()) {                   
            $info = array(
                    'uId'          => $user['id'], 
                    'uName'        => $user['name'],        
                    'uPassChanged' => 1,    
                    'uPhone'       => $user['phone'],  
                    'uRole'        => $user['id_role'],
                    'uEmail'       => $user['email'],
                    'uCreatedAt'   => $user['created_at']
               );
            
            $this->session->set_userdata('user', $info);                   
            echo json_encode(array("ok" => 1));
            return;
        }
        
        echo json_encode(array("ok" => 0));
    }
    
    public function getUsers($page) {        
        $offset = ($page > 1 ? (intval($page) - 1 )* 30 : 0);
        $query = $this->db->query("select u.id, u.name, u.email, u.phone, 
                                          DATE_FORMAT(u.uCreatedAt,'%d/%m/%Y %h:%i %p') uCreatedAt, u.uStat 
                                     from sih_users u                                     
                                 order by u.created_at desc 
                                    limit 30 offset {$offset}");
        // $query = $this->db->query("select u.uId, u.uName, u.uEmail, u.uPhone, 
        //                                   DATE_FORMAT(u.uCreatedAt,'%d/%m/%Y %h:%i %p') uCreatedAt, u.uStat 
        //                              from sih_users u                                     
        //                          order by u.uCreatedAt desc 
        //                             limit 30 offset {$offset}");
        return $query->result();
    }

    public function getProfile($id){
        $query = $this->db->get_where('sih_user_profiles', array('id' => $id));
        return $query->result_array();
    }
    public function updateAvata($userId,$avata){

        $avata = ($avata);
        $userid = ($userId);
       
        $this->db->set('avata', $avata);
        $this->db->where('id', $userid);
        $result = $this->db->update('sih_users');
        
    }
    
    public function getRole($uId)
    {
        $query = $this->db->get_where('sih_users', array('id' => $uId));
        $data = $query->result_array();
        $role="";
        if(isset($data[0]))
            $role = $data[0]['id_role'];
        return $role;
    }
    
    public function getListRoute($role_id)
    {
        $this->db->select('route');
        $this->db->from('sih_screens');
        
        $this->db->join('sih_role_screens', 'sih_screens.id = sih_role_screens.id_screens');
        $this->db->where('sih_role_screens.id_role=', $role_id);
        $this->db->where('sih_screens.stat=', '1');
        $this->db->where('sih_role_screens.stat=', '1');
        $query = $this->db->get();
        $data = $query->result_array();
        $list_role=[];
        foreach($data as $value){
            $list_role[]=$value['route'];
        }
        return $list_role;
    }
}
?>