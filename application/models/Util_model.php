<?php
class Util_model extends CI_Model {
    public function __construct() {
        // Call the CI_model constructor
        parent::__construct();        
        $this->load->helper(array('text', 'url', 'date'));
        $this->load->library('session');
        $this->load->library('email');
    }
    
    function sendEmail($to, $cId, $placeHolder1='', $placeHolder2='', $placeHolder3='', $placeHolder4=''){
        $subj = $msg = "";
        $query = $this->db->query("select cTitle, cDesc 
                                     from sih_contents 
                                    where cId={$cId} 
                                      and cGroup='EMAIL' 
                                    limit 1");
        foreach ($query->result() as $row) {       
            $subj = $row->cTitle;
            $msg = str_replace('{$placeHolder1}', $placeHolder1, 
                        str_replace('{$placeHolder2}', $placeHolder2, 
                                str_replace('{$placeHolder3}', $placeHolder3, 
                                        str_replace('{$placeHolder4}', $placeHolder4, $row->cDesc))));             
        } 
        
        try {
            if ($subj) {
                $this->load->library('My_PHPMailer');
                $mail = new PHPMailer();
                $mail->IsSMTP(); // we are going to use SMTP
                $mail->SMTPAuth   = true; // enabled SMTP authentication
                $mail->SMTPSecure = "tls";  // prefix for secure protocol to connect to the server
                $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
                $mail->Port       = 587;                   // SMTP port to connect to GMail
                $mail->CharSet    = 'utf-8';                
                $mail->Username   = "";  // user email address
                $mail->Password   = "";            // password in GMail
                $mail->SetFrom('contact@', '');  //Who is sending the email
                $mail->AddAddress($to);
                $mail->AddReplyTo('', '');  //email address that receives the response
                $mail->Subject    = $subj;
                $mail->msgHTML($msg);

                return $mail->Send();
            }
        }  catch (Exception $ex) {
            var_dump($ex);
        }
        
        return false;
    }
}
?>