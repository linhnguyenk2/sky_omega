<?php
require_once APPPATH . 'models/Entities/sih_product.php';

require_once APPPATH . 'models/Entities/sih_unit.php';
require_once APPPATH . 'models/Entities/sih_medicine_product.php';
require_once APPPATH . 'models/Entities/sih_functional_food_product.php';
require_once APPPATH . 'models/Entities/sih_medical_supply.php';
require_once APPPATH . 'models/Entities/sih_chemical_product.php';
require_once APPPATH . 'models/Entities/sih_cosmetic_product.php';
require_once APPPATH . 'models/Entities/sih_raw_material_product.php';
require_once APPPATH . 'models/Entities/sih_food_product.php';
require_once APPPATH . 'models/Entities/sih_product_group.php';
require_once APPPATH . 'models/Entities/sih_product_unit_exchange.php';
require_once APPPATH . 'models/Entities/sih_list_contraindication_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_not_combine_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_appointed_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_contraindication.php';
require_once APPPATH . 'models/Entities/sih_list_supplier.php';

/**
 *  Product Model
 *
 * @since v.1.0
 */
class Product_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [
            "unit_id"           => "sih_unit",
            "food_id"           => "sih_food_product",
            "raw_material_id"   => "sih_raw_material_product",
            "medicine_id"       => "sih_medicine_product",
            "function_food_id"  => "sih_functional_food_product",
            "medical_supply_id" => "sih_medical_supply",
            "cosmetic_id"       => "sih_cosmetic_product",
            "chemical_id"       => "sih_chemical_product",
            "nation_id"         => "sih_list_nations",
            "supplier_id"       => "sih_list_supplier",
            "product_group_id"  => 'sih_product_group'
        ];

        $this->mainTableName       = "sih_product";
        $this->mainEntityClassName = "Sih_product";
    }
}