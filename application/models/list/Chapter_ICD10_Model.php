<?php
require_once APPPATH . 'models/Entities/sih_list_chapter_icd10.php';
/**
 * Chapter ICD10 Model
 *
 * @since v.1.0
 */
class Chapter_ICD10_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_chapter_icd10";
        $this->mainEntityClassName = "Sih_list_chapter_icd10";
    }
}