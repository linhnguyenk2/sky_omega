<?php
require_once APPPATH . 'models/Entities/sih_user_key.php';
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

/**
 * Analysis_Blood Model
 *
 * @since v.1.0
 */
class User_Action_Model extends MY_Model
{
    protected $userModelName = 'User_Model';
    protected $userTableName = 'sih_user';

    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            "user_id" => "sih_user"
        ];

        $this->mainTableName       = "sih_user_key";
        $this->mainEntityClassName = "sih_user_key";
    }

    /**
     * Method to check login: increase expiry time if checkogin success
     *
     * @access public
     *
     * @param array $listParam
     *
     * @return object User info
     */
    public function checkLogin($listParam)
    {
        $entityManager = $this->entityManager;
        //$this->load->library('password');

        // deviceID = 32a1e5085aee42408166618b5e81785a
        $tmpFindArray = array(
            'username' => $listParam['username']
        );

        $this->load->model('list/' . $this->userModelName);
        $userModel = new $this->userModelName;

        // A single user by its username
        $userEntity = $entityManager->getRepository($userModel->getMainTableName())->findOneBy($tmpFindArray);

        if ( ! empty($userEntity)) {
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            // will rewrite: Decrypt password or validate_password($password, $correct_hash)

            // Wrong password
            //if ($userEntity->getPassword() != $listParam['password']) {
            if (password_verify($listParam['password'], $userEntity->getPassword())) {
                return false;
            }

            // Check is user login before
            $userId            = $userEntity->getId();
            $userKeyInfoEntity = $this->entityManager->getRepository($this->getMainTableName())->findOneBy(array('user_id' => $userId));

            if ( ! empty($userKeyInfoEntity)) {
                // Delete element
                try {
                    $entity = $entityManager->find($this->getMainTableName(), $userKeyInfoEntity->getId());

                    // Return 11000 not found this item
                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                    }

                    $entityManager->remove($entity);
                    $entityManager->flush();

                } catch (Exception $e) {
                    $entityManager->getConnection()->rollBack();
                }
            }

            // Inset Into sih_user_key table
            $sessionId = base64_encode($userId . '_' . time());

            // Sess_expiration = 7200 = 4 hours in config
            $expiryDate = time() + $this->config->config['sess_expiration'];

            $userKeyParams = array(
                'user_id'    => $userEntity->getId(),
                'session_id' => $sessionId,
                'expiry'     => $expiryDate,
                'device_id'  => $listParam['device_id']
            );

            $data = $this->postEvent($userKeyParams);
            // Remove password and device_id
            unset($data->user_id->password);
            unset($data->user_id->device_id);
            unset($data->user_id->salt);

            $entityManager->getConnection()->commit();
            $entityManager->close();

            return $data;
        }

        return false;
    }

    /**
     * Method to delete item.
     *
     * @access public
     *
     * @param array $id 1
     *
     * @return boolean
     */
    public function deleteEvent($id)
    {
        $status = true;

        $entityManager = $this->entityManager;
        $entityManager->getConnection()->beginTransaction();
        $entityManager->getConnection()->setAutoCommit(false);

        try {
            $entity = $entityManager->find($this->getMainTableName(), $id);

            // Return 11000 not found this item
            if (empty($entity)) {
                $entityManager->getConnection()->rollBack();
                $status = false;
                break;
            }

            $entityManager->remove($entity);
            $entityManager->flush();


            $entityManager->getConnection()->commit();
            $entityManager->clear();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollBack();
            $status = false;
        }

        return $status;
    }

    /**
     * Method to get userKey Entity from sessionId and deviceId
     *
     * @access public
     *
     * @param string $sessionId
     * @param string $deviceId
     *
     * @return object
     */
    public function getUserKey($sessionId, $deviceId)
    {
        $userKeyInfoEntity = $this->entityManager->getRepository($this->getMainTableName())->findOneBy(array(
            'session_id' => $sessionId,
            'device_id'  => $deviceId
        ));

        $data = new stdClass();

        if ( ! empty($userKeyInfoEntity)) {
            $data = $userKeyInfoEntity->buildObject();
        }

        return $data;
    }

    /**
     * Method to update multi item.
     *
     * @access public
     *
     * @param string $ids
     *            list of id 1_2_12
     * @param array  $listParam
     *            item
     *
     * @return array
     */
    public function putMultiEvent($ids, $listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $data = array();

        if ( ! empty($ids) && ! empty($listParam)) {
            unset($listParam['id']);

            $entityManager = $this->entityManager;

            foreach ($ids as $id) {
                $entity = $entityManager->find($this->getMainTableName(), $id);

                if (empty($entity)) {
                    $data = array();

                    return $data;
                } else {
                    foreach ($listParam as $key => $value) {
                        if ($this->isItemIsForeignKey($key)) {
                            $listForeignKey = $this->getListForeignKey();
                            $tableName      = $listForeignKey[$key];
                            $value          = $entityManager->find($tableName, $value);

                            if (empty($value)) {
                                $data = array();

                                return $data;
                            }
                        }

                        $methodSet = $this->getMethodNameInEntity($key);
                        $entity->$methodSet($value);
                    }

                    $entityManager->flush();

                    $data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
                }
            }

        } else {
            $data = array();
        }

        return $data;
    }

    /**
     * Update login time
     *
     * @access public
     *
     * @param int $id primary key of sih_user_key
     *
     * @return string
     */
    public function updateLoginTime($id)
    {
        $listUpdateParams['expiry'] = time() + $this->config->config['sess_expiration'];
        $arrUsers                   = array($id);

        $data = $this->putMultiEvent($arrUsers, $listUpdateParams);

        if ( ! empty($data)) {
            // Set expiry
            // setcookie("expiry", $listUpdateParams['expiry'], time() + (86400 * 5), "/");

            return true;
        }

        return false;
    }

    /**
     * Method isSessionValid: if isSessionValid
     *
     *
     * @access public
     *
     * @param string $sessionId      session
     * @param string $deviceId       deviceId: UUID of device - save in cookie\
     * @param int    $isUpdateExpiry update expiry time: 0 not update, 1 update
     *
     * @return boolean
     */
    public function isSessionValid($sessionId, $deviceId, $isUpdateExpiry = 1)
    {
        $tmpFindArray      = array('session_id' => $sessionId, 'device_id' => $deviceId);
        $userKeyInfoEntity = $this->entityManager->getRepository($this->getMainTableName())->findOneBy($tmpFindArray);

        if (empty($userKeyInfoEntity)) {
            return false;
        }

        //if ($userKeyInfo->user_id !=  $this->session->userdata('id')) return 1;

        $sessionId = base64_decode($sessionId);

        $userId = $userKeyInfoEntity->getUser_id()->getId();
        $expiry = $userKeyInfoEntity->getExpiry();

        $sessionArr = explode("_", $sessionId);
        $sessionTime = $sessionArr[1];

        if ($expiry >= $sessionTime) {
            if ($isUpdateExpiry) {
                $this->updateLoginTime($userKeyInfoEntity->getId());
            }

            return true;
        }

        return false;
    }
}