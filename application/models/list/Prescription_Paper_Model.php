<?php
require_once APPPATH . 'models/Entities/sih_prescription_paper.php';
require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_staff.php';
require_once APPPATH . 'models/Entities/sih_specify_service_paper.php';
require_once APPPATH . 'models/Entities/sih_service_location.php';
require_once APPPATH . 'models/Entities/sih_service.php';
require_once APPPATH . 'models/Entities/sih_service_type.php';
require_once APPPATH . 'models/Entities/sih_service_group.php';
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';

require_once APPPATH . 'models/Entities/sih_list_departments.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

require_once APPPATH . 'models/Entities/sih_family_planing_book.php';
require_once APPPATH . 'models/Entities/sih_family_plan_birth_control_form.php';
require_once APPPATH . 'models/Entities/sih_artificial_examination_form.php';
require_once APPPATH . 'models/Entities/sih_medical_record.php';
require_once APPPATH . 'models/Entities/sih_prenatal_book.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_book.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form.php';
require_once APPPATH . 'models/Entities/sih_prenatal_form.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_form.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_form.php';
require_once APPPATH . 'models/Entities/sih_menopause_book.php';
require_once APPPATH . 'models/Entities/sih_menopause_form.php';
require_once APPPATH . 'models/Entities/sih_analysis_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_health_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_pregnancy_changes_in_health_book.php';
/**
 * Prescription Paper
 *
 * @since v.1.0
 */
class Prescription_Paper_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->listForeignKey = [
			"patient_id" => "sih_patients",
            "staff_id" => "sih_staff",
            "medical_examination_form_id" => "sih_medical_examination_form"
		];

		$this->mainTableName = "sih_prescription_paper";
		$this->mainEntityClassName = "Sih_prescription_paper";
	}
}