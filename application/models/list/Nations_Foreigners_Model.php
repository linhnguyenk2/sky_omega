<?php
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';

/**
 * Nations Foreigners Model
 *
 * @since v.1.0
 */
class Nations_Foreigners_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_nations_foreigners";
        $this->mainEntityClassName = "Sih_list_nations_foreigners";
    }

}
