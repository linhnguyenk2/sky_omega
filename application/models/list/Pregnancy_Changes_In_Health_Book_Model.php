<?php
require_once APPPATH . 'models/Entities/sih_pregnancy_changes_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_health_book.php';
require_once APPPATH . 'models/Entities/sih_user.php';

require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_staff.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';

require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

/**
 * Pregnancy Changes In Health Book Model
 *
 * @since v.1.0
 */
class Pregnancy_Changes_In_Health_Book_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            'patient_id' => 'sih_patients',
            'health_book_id' => 'sih_health_book',
        ];

        $this->mainTableName       = "sih_pregnancy_changes_in_health_book";
        $this->mainEntityClassName = "Sih_pregnancy_changes_in_health_book";
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id     = new CollumJoin("k", "patient_id");
        $column_join_health_book_id = new CollumJoin("khb", "health_book_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addCollumJoin($column_join_health_book_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);


        $joinTableQueryObjectTable           = "sih_health_book";
        $joinTableQueryObjectEntityClassName = "sih_health_book";
        $joinTableQueryObjectTableAlias      = "khb";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}