<?php
require_once APPPATH . 'models/Entities/sih_specify_service_paper.php';
require_once APPPATH . 'models/Entities/sih_service_location.php';
require_once APPPATH . 'models/Entities/sih_service.php';
require_once APPPATH . 'models/Entities/sih_service_type.php';
require_once APPPATH . 'models/Entities/sih_service_group.php';

require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_staff.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';

require_once APPPATH . 'models/Entities/sih_list_departments.php';
require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';
require_once APPPATH . 'models/Entities/sih_analysis_paper.php';

require_once APPPATH . 'models/Entities/sih_family_planing_book.php';
require_once APPPATH . 'models/Entities/sih_family_plan_birth_control_form.php';
require_once APPPATH . 'models/Entities/sih_artificial_examination_form.php';
require_once APPPATH . 'models/Entities/sih_medical_record.php';
require_once APPPATH . 'models/Entities/sih_prenatal_book.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_book.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form.php';
require_once APPPATH . 'models/Entities/sih_prenatal_form.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_form.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_form.php';
require_once APPPATH . 'models/Entities/sih_menopause_book.php';
require_once APPPATH . 'models/Entities/sih_menopause_form.php';
require_once APPPATH . 'models/Entities/sih_analysis_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_health_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_pregnancy_changes_in_health_book.php';
/**
 * Specify Service Form Model
 *
 * @since v.1.0
 */
class Specify_Service_Paper_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            "patient_id"                  => "sih_patients",
            "service_location_id"         => "sih_service_location",
            "staff_id"                    => "sih_staff",
            "service_id"                    => "sih_service",
            "medical_examination_form_id" => "sih_medical_examination_form"
        ];

        $this->mainTableName       = "sih_specify_service_paper";
        $this->mainEntityClassName = "sih_specify_service_paper";
    }

    /**
     * Method to insert item.
     *
     * @access public
     *
     * @param array $listParam
     *            item
     *
     * @return object An object of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;

        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $listParam['created_at'] = new DateTime("now");

        if (isset($listParam['begin_date'])) {
            $listParam['begin_date'] = new DateTime($listParam['begin_date']);
        }

        if (isset($listParam['expiration_date'])) {
            $listParam['expiration_date'] = new DateTime($listParam['expiration_date']);
        }

        if (isset($listParam['appointment_time'])) {
            $listParam['appointment_time'] = new DateTime($listParam['appointment_time']);
        }

        // Add temp - to get code - stt = 0
        if (isset($listParam['stat']) && $listParam['stat'] == 0) {
            if ( ! empty($listParam)) {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if (!$this->isItemIsForeignKey($key)) {
                        $methodSet = $this->getMethodNameInEntity($key);
                        // Check method
                        if (method_exists($entityObject, $methodSet)) {
                            $entityObject->$methodSet($value);
                        }
                    }
                }
            }
        }
        elseif ( ! empty($listParam)) {
            foreach ($listParam as $key => $value) {
                // Is this Id exist in foreign table
                if ($this->isItemIsForeignKey($key)) {
                    $listForeignKey = $this->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $this->getMethodNameInEntity($key);
                // Check method
                if (method_exists($entityObject, $methodSet)) {
                    $entityObject->$methodSet($value);
                }
            }
        }

        $entityManager->persist($entityObject);
        $entityManager->flush();

        $lastInsertId = $entityObject->getId();
        $entity       = $entityManager->find($this->getMainTableName(), $lastInsertId);

        if ($entityObject->isHaveCodeField()) {
            $newCode = $this->getCodeWithID($lastInsertId);
            $entity->setCode($newCode);
            $entityManager->flush();
        }

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }

    /**
     * Method to update multi item.
     *
     * @access public
     *
     * @param string $ids
     *            list of id 1_2_12
     * @param array  $listParam
     *            item
     *
     * @return array
     */
    public function putMultiEvent($ids, $listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $data = array();

        if ( ! empty($ids) && ! empty($listParam)) {
            unset($listParam['id']);

            if (isset($listParam['last_modified'])) {
                $listParam['last_modified'] = new DateTime( $listParam['last_modified']);
            }

            if (isset($listParam['begin_date'])) {
                $listParam['begin_date'] = new DateTime($listParam['begin_date']);
            }

            if (isset($listParam['expiration_date'])) {
                $listParam['expiration_date'] = new DateTime($listParam['expiration_date']);
            }

            if (isset($listParam['appointment_time'])) {
                $listParam['appointment_time'] = new DateTime($listParam['appointment_time']);
            }

            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $data = array();

                        return $data;
                    } else {
                        foreach ($listParam as $key => $value) {
                            if ($this->isItemIsForeignKey($key)) {
                                $listForeignKey = $this->getListForeignKey();
                                $tableName      = $listForeignKey[$key];
                                $value          = $entityManager->find($tableName, $value);

                                if (empty($value)) {
                                    $entityManager->getConnection()->rollBack();
                                    $data = array();

                                    return $data;
                                }
                            }

                            $methodSet = $this->getMethodNameInEntity($key);
                            $entity->$methodSet($value);
                        }

                        $entityManager->flush();

                        $data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
                    }
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $data = array();
            }
        } else {
            $data = array();
        }

        return $data;
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id                  = new CollumJoin("k", "patient_id");
        $column_join_staff_id_1                  = new CollumJoin("ks1", "staff_id");
        $column_join_medical_examination_form_id = new CollumJoin("km", "medical_examination_form_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addCollumJoin($column_join_staff_id_1);
        $mainTableQueryObject->addCollumJoin($column_join_medical_examination_form_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_staff";
        $joinTableQueryObjectEntityClassName = "sih_staff";
        $joinTableQueryObjectTableAlias      = "ks1";
        $joinTableQueryObject3               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject3->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject3->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_medical_examination_form";
        $joinTableQueryObjectEntityClassName = "sih_medical_examination_form";
        $joinTableQueryObjectTableAlias      = "km";
        $joinTableQueryObject4               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject4->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject4->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = array();
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject3;
        $listJoinTableQueryObject[] = $joinTableQueryObject4;

        $additionalInjectionObject = new AdditionalInjection();

        // Type of staff = lễ tân = 400.
        if (isset($_GET['staff_id_type']) && $_GET['staff_id_type'])
        {
            $additionalInjectionObject->injectJoinClause="\n LEFT JOIN ks1.user_id mu \n";
            $additionalInjectionObject->injectWhereClause="\n mu.type= " . $_GET['staff_id_type'];
        }

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword, $additionalInjectionObject);

        //var_dump($DQL);
        return $DQL;
    }
}