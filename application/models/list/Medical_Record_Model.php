<?php
require_once APPPATH . 'models/Entities/sih_medical_record.php';

require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';


require_once APPPATH . 'models/Entities/sih_family_planing_book.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_book.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_form.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_book.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_form.php';
require_once APPPATH . 'models/Entities/sih_prenatal_book.php';
require_once APPPATH . 'models/Entities/sih_prenatal_form.php';
require_once APPPATH . 'models/Entities/sih_menopause_book.php';
require_once APPPATH . 'models/Entities/sih_menopause_form.php';
require_once APPPATH . 'models/Entities/sih_health_book.php';
require_once APPPATH . 'models/Entities/sih_analysis_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_health_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_pregnancy_changes_in_health_book.php';

require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

/**
 * Medical Record Model
 *
 * @since v.1.0
 */
class Medical_Record_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    publicg
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            'patient_id'                 => 'sih_patients',
            'gynecolog_book_id'          => 'sih_gynecolog_book',
            'breast_examination_book_id' => 'sih_breast_examination_book',
            'prenatal_book_id'           => 'sih_prenatal_book',
            'menopause_book_id'          => 'sih_menopause_book',
            'family_planing_book_id'     => 'sih_family_planing_book',
            'health_book_id'             => 'sih_health_book',
        ];

        $this->mainTableName       = "sih_medical_record";
        $this->mainEntityClassName = "Sih_medical_record";
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id                 = new CollumJoin("k", "patient_id");
        $column_join_gynecolog_book_id          = new CollumJoin("kh", "gynecolog_book_id");
        $column_join_breast_examination_book_id = new CollumJoin("kb", "breast_examination_book_id");
        $column_join_prenatal_book_id           = new CollumJoin("kp", "prenatal_book_id");
        $column_join_family_planing_book_id     = new CollumJoin("kf", "family_planing_book_id");
        $column_join_health_book_id     = new CollumJoin("khb", "health_book_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addCollumJoin($column_join_gynecolog_book_id);
        $mainTableQueryObject->addCollumJoin($column_join_breast_examination_book_id);
        $mainTableQueryObject->addCollumJoin($column_join_prenatal_book_id);
        $mainTableQueryObject->addCollumJoin($column_join_family_planing_book_id);
        $mainTableQueryObject->addCollumJoin($column_join_health_book_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_gynecolog_book";
        $joinTableQueryObjectEntityClassName = "sih_gynecolog_book";
        $joinTableQueryObjectTableAlias      = "kh";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_breast_examination_book";
        $joinTableQueryObjectEntityClassName = "sih_breast_examination_book";
        $joinTableQueryObjectTableAlias      = "kb";
        $joinTableQueryObject3               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject3->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject3->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_prenatal_book";
        $joinTableQueryObjectEntityClassName = "sih_prenatal_book";
        $joinTableQueryObjectTableAlias      = "kp";
        $joinTableQueryObject4               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject4->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject4->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_family_planing_book";
        $joinTableQueryObjectEntityClassName = "sih_family_planing_book";
        $joinTableQueryObjectTableAlias      = "kf";
        $joinTableQueryObject5               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject5->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject5->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_health_book";
        $joinTableQueryObjectEntityClassName = "sih_health_book";
        $joinTableQueryObjectTableAlias      = "khb";
        $joinTableQueryObject6               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject6->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject6->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;
        $listJoinTableQueryObject[] = $joinTableQueryObject3;
        $listJoinTableQueryObject[] = $joinTableQueryObject4;
        $listJoinTableQueryObject[] = $joinTableQueryObject5;
        $listJoinTableQueryObject[] = $joinTableQueryObject6;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}