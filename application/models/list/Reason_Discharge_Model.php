<?php
require_once APPPATH . 'models/Entities/sih_list_reason_discharge.php';

/**
 * Reason Discharge Model
 *
 * @since v.1.0
 */
class Reason_Discharge_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_reason_discharge";
        $this->mainEntityClassName = "Sih_list_reason_discharge";
    }

}
