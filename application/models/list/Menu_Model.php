<?php
require_once APPPATH . 'models/Entities/sih_menu.php';

/**
 * Menu Model
 *
 * @since v.1.0
 */
class Menu_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey      = [];
        $this->mainTableName       = "sih_menu";
        $this->mainEntityClassName = "Sih_menu";
    }
}