<?php
require_once APPPATH . 'models/Entities/sih_list_departments.php';

/**
 * Departments Model
 *
 * @since v.1.0
 */
class Departments_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_departments";
        $this->mainEntityClassName = "Sih_list_departments";
    }

}
