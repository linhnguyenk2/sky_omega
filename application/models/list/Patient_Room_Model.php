<?php
require_once APPPATH . 'models/Entities/sih_list_patient_room_type.php';
require_once APPPATH . 'models/Entities/sih_list_patient_room.php';

/**
 * Patient Room Model
 *
 * @since v.1.0
 */
class Patient_Room_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->listForeignKey = [
			"patient_room_type_id" => "sih_list_patient_room_type"
		];
		$this->mainTableName = "sih_list_patient_room";
		$this->mainEntityClassName = "Sih_list_patient_room";
	}

    /**
     * get Query for Get List
     *
     * @access    public
     *
     * @param   string $table         table name
     * @param   string $orderBy       The ordering code.
     * @param   string $sortDirection sort direction
     *
     * @return  string  The SQL field(s)
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $columJoin_patient_room_type_id = new CollumJoin("k", "patient_room_type_id");
        $OrderClause                                  = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($columJoin_patient_room_type_id);
        $mainTableQueryObject->addOrderByCondition($OrderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_list_patient_room_type";
        $joinTableQueryObjectEntityClassName = "sih_list_patient_room_type";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}