<?php
require_once APPPATH . 'models/Entities/sih_subclinical_paper.php';
require_once APPPATH . 'models/Entities/sih_subclinical_xray_paper.php';
require_once APPPATH . 'models/Entities/sih_subclinical_endoscopic_paper.php';
require_once APPPATH . 'models/Entities/sih_subclinical_xray_breast_paper.php';
require_once APPPATH . 'models/Entities/sih_subclinical_ultra_sound.php';
require_once APPPATH . 'models/Entities/sih_subclinical_ultra_sound_3d.php';
require_once APPPATH . 'models/Entities/sih_specify_service_paper.php';
require_once APPPATH . 'models/Entities/sih_service_location.php';
require_once APPPATH . 'models/Entities/sih_service.php';
require_once APPPATH . 'models/Entities/sih_service_type.php';
require_once APPPATH . 'models/Entities/sih_service_group.php';

require_once APPPATH . 'models/Entities/sih_list_departments.php';
require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_staff.php';
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';


require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

require_once APPPATH . 'models/Entities/sih_family_planing_book.php';
require_once APPPATH . 'models/Entities/sih_family_plan_birth_control_form.php';
require_once APPPATH . 'models/Entities/sih_artificial_examination_form.php';
require_once APPPATH . 'models/Entities/sih_medical_record.php';
require_once APPPATH . 'models/Entities/sih_prenatal_book.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_book.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form.php';
require_once APPPATH . 'models/Entities/sih_prenatal_form.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_form.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_form.php';
require_once APPPATH . 'models/Entities/sih_menopause_book.php';
require_once APPPATH . 'models/Entities/sih_menopause_form.php';
require_once APPPATH . 'models/Entities/sih_analysis_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_health_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_pregnancy_changes_in_health_book.php';
/**
 * Subclinical_Paper Model
 *
 * @since v.1.0
 */
class Subclinical_Paper_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            "patient_id"                       => "sih_patients",
            "staff_id"                         => "sih_staff",
            "approval_staff_id"                => "sih_staff",
            "medical_examination_form_id"      => "sih_medical_examination_form",
            "specify_service_paper_id"         => "sih_specify_service_paper",
            "subclinical_endoscopic_paper_id"  => "sih_subclinical_endoscopic_paper",
            "subclinical_xray_paper_id"        => "sih_subclinical_xray_paper",
            "subclinical_xray_breast_paper_id" => "sih_subclinical_xray_breast_paper",
            "subclinical_ultra_sound_id"       => "sih_subclinical_ultra_sound",
            "subclinical_ultra_sound_3d_id"    => "sih_subclinical_ultra_sound_3d"
        ];

        $this->mainTableName       = "sih_subclinical_paper";
        $this->mainEntityClassName = "Sih_subclinical_paper";
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id                       = new CollumJoin("k", "patient_id");
        $column_join_medical_examination_form_id      = new CollumJoin("km", "medical_examination_form_id");
        $column_join_staff_id                         = new CollumJoin("ks", "staff_id");
        $column_join_approval_staff_id                = new CollumJoin("kas", "approval_staff_id");
        $column_join_subclinical_endoscopic_paper_id  = new CollumJoin("ksep", "subclinical_endoscopic_paper_id");
        $column_join_subclinical_ultra_sound_id       = new CollumJoin("ksus", "subclinical_ultra_sound_id");
        $column_join_subclinical_ultra_sound_3d_id    = new CollumJoin("ksus3", "subclinical_ultra_sound_3d_id");
        $column_join_subclinical_xray_breast_paper_id = new CollumJoin("ksxbp", "subclinical_xray_breast_paper_id");
        $column_join_subclinical_xray_paper_id        = new CollumJoin("ksxp", "subclinical_xray_paper_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);

        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addCollumJoin($column_join_medical_examination_form_id);
        $mainTableQueryObject->addCollumJoin($column_join_staff_id);
        $mainTableQueryObject->addCollumJoin($column_join_approval_staff_id);
        $mainTableQueryObject->addCollumJoin($column_join_subclinical_endoscopic_paper_id);
        $mainTableQueryObject->addCollumJoin($column_join_subclinical_ultra_sound_id);
        $mainTableQueryObject->addCollumJoin($column_join_subclinical_ultra_sound_3d_id);
        $mainTableQueryObject->addCollumJoin($column_join_subclinical_xray_breast_paper_id);
        $mainTableQueryObject->addCollumJoin($column_join_subclinical_xray_paper_id);

        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_medical_examination_form";
        $joinTableQueryObjectEntityClassName = "sih_medical_examination_form";
        $joinTableQueryObjectTableAlias      = "km";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_subclinical_xray_paper";
        $joinTableQueryObjectEntityClassName = "sih_subclinical_xray_paper";
        $joinTableQueryObjectTableAlias      = "ksxp";
        $joinTableQueryObject3               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject3->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject3->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_staff";
        $joinTableQueryObjectEntityClassName = "sih_staff";
        $joinTableQueryObjectTableAlias      = "ks";
        $joinTableQueryObject4               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject4->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject4->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_staff";
        $joinTableQueryObjectEntityClassName = "sih_staff";
        $joinTableQueryObjectTableAlias      = "kas";
        $joinTableQueryObject5               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject5->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject5->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_subclinical_endoscopic_paper";
        $joinTableQueryObjectEntityClassName = "sih_subclinical_endoscopic_paper";
        $joinTableQueryObjectTableAlias      = "ksep";
        $joinTableQueryObject6               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject6->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject6->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_subclinical_ultra_sound";
        $joinTableQueryObjectEntityClassName = "sih_subclinical_ultra_sound";
        $joinTableQueryObjectTableAlias      = "ksus";
        $joinTableQueryObject7               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject7->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject7->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_subclinical_ultra_sound_3d";
        $joinTableQueryObjectEntityClassName = "sih_subclinical_ultra_sound_3d";
        $joinTableQueryObjectTableAlias      = "ksus3";
        $joinTableQueryObject8               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject8->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject8->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_subclinical_xray_breast_paper";
        $joinTableQueryObjectEntityClassName = "sih_subclinical_xray_breast_paper";
        $joinTableQueryObjectTableAlias      = "ksxbp";
        $joinTableQueryObject9               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject9->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject9->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);


        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;
        $listJoinTableQueryObject[] = $joinTableQueryObject3;
        $listJoinTableQueryObject[] = $joinTableQueryObject4;
        $listJoinTableQueryObject[] = $joinTableQueryObject5;
        $listJoinTableQueryObject[] = $joinTableQueryObject6;
        $listJoinTableQueryObject[] = $joinTableQueryObject7;
        $listJoinTableQueryObject[] = $joinTableQueryObject8;
        $listJoinTableQueryObject[] = $joinTableQueryObject9;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}