<?php
require_once APPPATH . 'models/Entities/sih_content_permission.php';

/**
 * Chapter ICD10 Model
 *
 * @since v.1.0
 */
class Content_Permission_Model extends MY_Model {
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->listForeignKey      = [];
		$this->mainTableName       = "sih_content_permission";
		$this->mainEntityClassName = "Sih_content_permission";
	}

	/**
	 * Method to get a list of items by type user
	 *
	 * @access public
	 *
	 * @return object An list of object of data items on success, null on failure.
	 */
	public function getContentPermissionByTypeUser($typeUser, $routePath)
	{
		$entityManager = $this->entityManager;

		$tmpFindArray = array(
			'type_user'  => $typeUser,
			'route_path' => $routePath,
		);

		$listEntity = $entityManager->getRepository($this->getMainTableName())->findBy($tmpFindArray);

		$data = array();

		foreach ($listEntity as $entity)
		{
			$data[] = $entity->buildObject();
		}

		return $data;
	}

	/**
	 * Method to insert item.
	 *
	 * @access public
	 *
	 * @param array $listParam
	 *            item
	 *
	 * @return object An object of data items on success, false on failure.
	 */
	public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
	{
		$entityManager = $this->entityManager;

		$mainEntityClassName = $this->getMainEntityClassName();
		$entityObject        = new $mainEntityClassName();

		// Check duplicate
		$tmpFindArray = array(
			'type_user' => $listParam['type_user'],
			'html_id'   => $listParam['html_id']
		);

		$listEntity = $entityManager->getRepository($this->getMainTableName())->findBy($tmpFindArray);

		if ( ! empty($listEntity))
		{
			return NULL;
		}

		$listParam['created_at'] = new DateTime("now");

		// Add temp - to get code - stt = 0
		if (isset($listParam['stat']) && $listParam['stat'] == 0)
		{
			if ( ! empty($listParam))
			{
				foreach ($listParam as $key => $value)
				{
					// Is this Id exist in foreign table
					if ( ! $this->isItemIsForeignKey($key))
					{
						$methodSet = $this->getMethodNameInEntity($key);
						// Check method
						if (method_exists($entityObject, $methodSet))
						{
							$entityObject->$methodSet($value);
						}
					}
				}
			}
		}
		elseif ( ! empty($listParam))
		{
			foreach ($listParam as $key => $value)
			{
				// Is this Id exist in foreign table
				if ($this->isItemIsForeignKey($key))
				{
					$listForeignKey = $this->getListForeignKey();
					$tableName      = $listForeignKey[$key];
					$value          = $entityManager->find($tableName, $value);

					if (empty($value))
					{
						return NULL;
					}
				}

				$methodSet = $this->getMethodNameInEntity($key);
				// Check method
				if (method_exists($entityObject, $methodSet))
				{
					$entityObject->$methodSet($value);
				}
			}
		}

		$entityManager->persist($entityObject);
		$entityManager->flush();

		$lastInsertId = $entityObject->getId();
		$entity       = $entityManager->find($this->getMainTableName(), $lastInsertId);

		if ($entityObject->isHaveCodeField())
		{
			$newCode = $this->getCodeWithID($lastInsertId);
			$entity->setCode($newCode);
			$entityManager->flush();
		}

		$data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

		return $data;
	}

	/**
	 * Method to update multi item.
	 *
	 * @access public
	 *
	 * @param string $ids
	 *            list of id 1_2_12
	 * @param array  $listParam
	 *            item
	 *
	 * @return array
	 */
	public function putMultiEvent($ids, $listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
	{
		$data = array();

		if ( ! empty($ids) && ! empty($listParam))
		{
			// Check duplicate
			$tmpFindArray = array(
				'type_user' => $listParam['type_user'],
				'html_id'   => $listParam['html_id']
			);

			$listEntity = $entityManager->getRepository($this->getMainTableName())->findBy($tmpFindArray);

			if ( ! empty($listEntity))
			{
				return NULL;
			}

			unset($listParam['id']);

			if (isset($listParam['last_modified']))
			{
				$listParam['last_modified'] = new DateTime("now");
			}

			$entityManager = $this->entityManager;
			$entityManager->getConnection()->beginTransaction();
			$entityManager->getConnection()->setAutoCommit(FALSE);

			try
			{
				foreach ($ids as $id)
				{
					$entity = $entityManager->find($this->getMainTableName(), $id);

					if (empty($entity))
					{
						$entityManager->getConnection()->rollBack();
						$data = array();

						return $data;
					}
					else
					{
						foreach ($listParam as $key => $value)
						{
							if ($this->isItemIsForeignKey($key))
							{
								$listForeignKey = $this->getListForeignKey();
								$tableName      = $listForeignKey[$key];
								$value          = $entityManager->find($tableName, $value);

								if (empty($value))
								{
									$entityManager->getConnection()->rollBack();
									$data = array();

									return $data;
								}
							}

							$methodSet = $this->getMethodNameInEntity($key);
							$entity->$methodSet($value);
						}

						$entityManager->flush();

						$data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
					}
				}

				$entityManager->getConnection()->commit();
				$entityManager->close();
			}
			catch (Exception $e)
			{
				$entityManager->getConnection()->rollBack();
				$data = array();
			}
		}
		else
		{
			$data = array();
		}

		return $data;
	}
}