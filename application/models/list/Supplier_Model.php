<?php
require_once APPPATH . 'models/Entities/sih_list_supplier.php';

/**
 * Supplier Model
 *
 * @since v.1.0
 */
class Supplier_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->listForeignKey = [];
		$this->mainTableName = "sih_list_supplier";
		$this->mainEntityClassName = "Sih_list_supplier";
	}

	/**
	 * get Code With ID
	 *
	 * @access    public
	 *
	 * @param   int  $id   The unique identifier for the object
	 *
	 * @return  string  code
	 */
	public function getCodeWithID($id)
	{
		$createdAt    = date("dmy");
		$newCode      = $createdAt . sprintf("%06d", $id);

		return $newCode;
	}
}
