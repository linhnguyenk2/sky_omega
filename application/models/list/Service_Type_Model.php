<?php
require_once APPPATH . 'models/Entities/sih_service_type.php';
require_once APPPATH . 'models/Entities/sih_service_group.php';

/**
 * Service Type Model
 *
 * @since v.1.0
 */
class Service_Type_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = array(
            'service_group_id' => 'sih_service_group'
        );

        $this->mainTableName       = "sih_service_type";
        $this->mainEntityClassName = "Sih_service_type";
    }
}