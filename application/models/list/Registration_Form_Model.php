<?php
require_once APPPATH . 'models/Entities/sih_registration_form.php';
require_once APPPATH . 'models/Entities/sih_list_health_insurance_card.php';
require_once APPPATH . 'models/Entities/sih_list_insurance_company.php';
require_once APPPATH . 'models/Entities/sih_survival_information.php';
require_once APPPATH . 'models/Entities/sih_appointment_form.php';

require_once APPPATH . 'models/Entities/sih_appointment_form.php';
require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';


require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

/**
 * Staff Model
 *
 * @since v.1.0
 */
class Registration_Form_Model extends MY_Model
{
    /**
     * entityManager
     *
     * @var entityManager
     */
    protected $listSubForeignKey = array(
        'sih_survival_information' => 'Survival_Information_Model',
        'sih_appointment_form'     => 'Appointment_Form_Model',
    );

    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            'patient_id'              => 'sih_patients',
            'patient_insurrance_id'   => 'sih_list_health_insurance_card',
            'patient_company_id'      => 'sih_list_insurance_company',
            'survival_information_id' => 'sih_survival_information',
            'appointment_form_id'     => 'sih_appointment_form'
        ];

        $this->mainTableName       = "sih_registration_form";
        $this->mainEntityClassName = "Sih_registration_form";
    }

    /**
     * Method to delete multi item.
     *
     * @access public
     *
     * @param array $ids
     *            list of id [1,2,3]
     *
     * @return boolean
     */
    public function deleteMultiEvent($ids)
    {
        $status = true;

        if ( ! empty($ids)) {
            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    // Delete Registration_Form_Model
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    // Return 11000 not found this item
                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $status = false;
                        break;
                    }

                    $survival_information_id = $entity->getSurvival_information_id()->getId();
                    $appointment_form_id = $entity->getAppointment_form_id()->getId();

                    $entityManager->remove($entity);
                    $entityManager->flush();

                    // Delete Survival_Infomation_Model and Appointment_Form_Model
                    foreach ($this->listSubForeignKey as $tableName => $modelName) {
                        $this->load->model('list/' . $modelName);
                        $newModel     = new $modelName;
                        $entityObject = new stdClass();

                        switch ($tableName) {
                            case 'sih_survival_information':
                                $entityObject = $entityManager->find($tableName, $survival_information_id);
                                break;
                            case 'sih_appointment_form':
                                $entityObject = $entityManager->find($tableName, $appointment_form_id);
                                break;
                        }

                        // Return 11000 not found this item
                        if (empty($entityObject)) {
                            $entityManager->getConnection()->rollBack();
                            $status = false;
                            break;
                        }

                        $entityManager->remove($entityObject);
                        $entityManager->flush();
                    }
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $status = false;
            }
        } else {
            $status = false;
        }

        return $status;
    }

    /**
     * Method to update multi item.
     *
     * @access public
     *
     * @param string $ids
     *            list of id 1_2_12
     * @param array  $listParam
     *            item
     *
     * @return array
     */
    public function putMultiEvent($ids, $listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $data = array();

        /*if (isset($listParam['date_appointment']) && ! empty($listParam['date_appointment'])) {
            $listParam['date_appointment'] = new DateTime($listParam['date_appointment']);
        } else {
            $listParam['date_appointment'] = new DateTime("now");
        }*/

        if ( ! empty($ids) && ! empty($listParam)) {
            unset($listParam['id']);
            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $data = array();

                        return $data;
                    } else {
                        // Update Survival_Infomation_Model and Appointment_Form_Model
                        foreach ($this->listSubForeignKey as $tableName => $modelName) {
                            $this->load->model('list/' . $modelName);
                            $newModel     = new $modelName;
                            $entityObject = new stdClass();

                            switch ($tableName) {
                                case 'sih_survival_information':
                                    $entityObject = $entityManager->find($tableName,
                                        $entity->getSurvival_information_id()->getId());
                                    break;
                                case 'sih_appointment_form':
                                    $entityObject = $entityManager->find($tableName,
                                        $entity->getAppointment_form_id()->getId());
                                    break;
                            }

                            if ( ! empty($entityObject)) {
                                foreach ($listParam as $key => $value) {
                                    // Is this Id exist in foreign table
                                    if ($newModel->isItemIsForeignKey($key)) {
                                        $listForeignKey = $newModel->getListForeignKey();
                                        $tableName      = $listForeignKey[$key];
                                        $value          = $entityManager->find($tableName, $value);

                                        if (empty($value)) {
                                            return null;
                                        }
                                    }

                                    $methodSet = $newModel->getMethodNameInEntity($key);

                                    if (method_exists($entityObject, $methodSet) && ! empty($value)) {
                                        $entityObject->$methodSet($value);

                                    }
                                }

                                $entityManager->flush();
                            }
                        }

                        // Update Registration_Form_Model
                        foreach ($listParam as $key => $value) {
                            if ($this->isItemIsForeignKey($key)) {
                                $listForeignKey = $this->getListForeignKey();
                                $tableName      = $listForeignKey[$key];
                                $value          = $entityManager->find($tableName, $value);

                                if (empty($value)) {
                                    $entityManager->getConnection()->rollBack();
                                    $data = array();

                                    return $data;
                                }
                            }

                            $methodSet = $this->getMethodNameInEntity($key);

                            if (method_exists($entity, $methodSet)) {
                                $entity->$methodSet($value);
                            }
                        }

                        $entityManager->flush();

                        $data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
                    }
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $data = array();
            }
        } else {
            $data = array();
        }

        return $data;
    }

    /**
     * Method to insert item.
     *
     * @access public
     *
     * @param   array $item item
     *
     * @return object An object of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;
        $entityManager->getConnection()->beginTransaction();
        $entityManager->getConnection()->setAutoCommit(false);

        $listParam['created_at'] = new DateTime("now");

        if (isset($listParam['date_appointment']) && ! empty($listParam['date_appointment'])) {
            $listParam['date_appointment'] = new DateTime($listParam['date_appointment']);
        } else {
            $listParam['date_appointment'] = new DateTime("now");
        }

        // Insert Survival_Infomation_Model and Appointment_Form_Model
        foreach ($this->listSubForeignKey as $tableName => $modelName) {
            $this->load->model('list/' . $modelName);
            $entityObject = new $tableName();
            $newModel     = new $modelName;

            foreach ($listParam as $key => $value) {
                // Is this Id exist in foreign table
                if ($newModel->isItemIsForeignKey($key)) {
                    $listForeignKey = $newModel->getListForeignKey();
                    $tableName2     = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName2, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $newModel->getMethodNameInEntity($key);

                if (method_exists($entityObject, $methodSet)) {
                    $entityObject->$methodSet($value);
                }
            }

            $entityManager->persist($entityObject);
            $entityManager->flush();

            $lastInsertId = $entityObject->getId();

            switch ($tableName) {
                case 'sih_survival_information':
                    $listParam['survival_information_id'] = $lastInsertId;
                    break;
                case 'sih_appointment_form':

                    $listParam['appointment_form_id'] = $lastInsertId;
                    break;
            }
        }

        // Insert registration_form
        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        foreach ($listParam as $key => $value) {
            // Is this Id exist in foreign table
            if ($this->isItemIsForeignKey($key)) {
                $listForeignKey = $this->getListForeignKey();
                $tableName      = $listForeignKey[$key];
                $value          = $entityManager->find($tableName, $value);

                if (empty($value)) {
                    return null;
                }
            }

            $methodSet = $this->getMethodNameInEntity($key);

            if (method_exists($entityObject, $methodSet)) {
                $entityObject->$methodSet($value);
            }
        }

        // Rollback
        try {
            $entityManager->persist($entityObject);
            $entityManager->flush();

            // Get last insert ID
            $lastInsertId = $entityObject->getId();

            $entity = $entityManager->find($this->getMainTableName(), $lastInsertId);

            if ($entityObject->isHaveCodeField()) {
                $newCode = $this->getCodeWithID($lastInsertId);
                $entity->setCode($newCode);
                $entityManager->flush();

                $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
            }

            $entityManager->getConnection()->commit();
            $entityManager->close();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollBack();
            $data = array();
        }

        return $data;
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id              = new CollumJoin("k", "patient_id");
        $column_join_patient_insurrance_id   = new CollumJoin("kpi", "patient_insurrance_id");
        $column_join_patient_company_id      = new CollumJoin("kpc", "patient_company_id");
        $column_join_survival_information_id = new CollumJoin("ks", "survival_information_id");
        $column_join_appointment_form_id     = new CollumJoin("ka", "appointment_form_id");


        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addCollumJoin($column_join_patient_insurrance_id);
        $mainTableQueryObject->addCollumJoin($column_join_patient_company_id);
        $mainTableQueryObject->addCollumJoin($column_join_survival_information_id);
        $mainTableQueryObject->addCollumJoin($column_join_appointment_form_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_list_health_insurance_card";
        $joinTableQueryObjectEntityClassName = "sih_list_health_insurance_card";
        $joinTableQueryObjectTableAlias      = "kpi";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_list_insurance_company";
        $joinTableQueryObjectEntityClassName = "sih_list_insurance_company";
        $joinTableQueryObjectTableAlias      = "kpc";
        $joinTableQueryObject3               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject3->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject3->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_survival_information";
        $joinTableQueryObjectEntityClassName = "sih_survival_information";
        $joinTableQueryObjectTableAlias      = "ks";
        $joinTableQueryObject4               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject4->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject4->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_appointment_form";
        $joinTableQueryObjectEntityClassName = "sih_appointment_form";
        $joinTableQueryObjectTableAlias      = "ka";
        $joinTableQueryObject5               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject5->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject5->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);


        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;
        $listJoinTableQueryObject[] = $joinTableQueryObject3;
        $listJoinTableQueryObject[] = $joinTableQueryObject4;
        $listJoinTableQueryObject[] = $joinTableQueryObject5;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}