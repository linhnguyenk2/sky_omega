<?php
require_once APPPATH . 'models/Entities/sih_tpcn_mp_vtyt_paper.php';

require_once APPPATH . 'models/Entities/sih_staff.php';
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';
require_once APPPATH . 'models/Entities/sih_specify_service_paper.php';
require_once APPPATH . 'models/Entities/sih_service_location.php';
require_once APPPATH . 'models/Entities/sih_service.php';
require_once APPPATH . 'models/Entities/sih_service_type.php';
require_once APPPATH . 'models/Entities/sih_service_group.php';

require_once APPPATH . 'models/Entities/sih_list_departments.php';
require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';
require_once APPPATH . 'models/Entities/sih_analysis_paper.php';

require_once APPPATH . 'models/Entities/sih_family_planing_book.php';
require_once APPPATH . 'models/Entities/sih_family_plan_birth_control_form.php';
require_once APPPATH . 'models/Entities/sih_artificial_examination_form.php';
require_once APPPATH . 'models/Entities/sih_medical_record.php';
require_once APPPATH . 'models/Entities/sih_prenatal_book.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_book.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form.php';
require_once APPPATH . 'models/Entities/sih_prenatal_form.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_form.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_form.php';
require_once APPPATH . 'models/Entities/sih_menopause_book.php';
require_once APPPATH . 'models/Entities/sih_menopause_form.php';
require_once APPPATH . 'models/Entities/sih_analysis_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_health_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_pregnancy_changes_in_health_book.php';
/**
 * Tpcn Mp Vtyt Paper Model
 *
 * @since v.1.0
 */
class Tpcn_Mp_Vtyt_Paper_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey      = [
            "patient_id"                  => "sih_patients",
            "staff_id"                    => "sih_staff",
            "medical_examination_form_id" => "sih_medical_examination_form"
        ];
        $this->mainTableName       = "sih_tpcn_mp_vtyt_paper";
        $this->mainEntityClassName = "Sih_tpcn_mp_vtyt_paper";
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id                  = new CollumJoin("k", "patient_id");
        $column_join_staff_id                    = new CollumJoin("ks", "staff_id");
        $column_join_medical_examination_form_id = new CollumJoin("km", "medical_examination_form_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addCollumJoin($column_join_staff_id);
        $mainTableQueryObject->addCollumJoin($column_join_medical_examination_form_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_staff";
        $joinTableQueryObjectEntityClassName = "sih_staff";
        $joinTableQueryObjectTableAlias      = "ks";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_medical_examination_form";
        $joinTableQueryObjectEntityClassName = "sih_medical_examination_form";
        $joinTableQueryObjectTableAlias      = "km";
        $joinTableQueryObject3               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject3->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject3->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;
        $listJoinTableQueryObject[] = $joinTableQueryObject3;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}