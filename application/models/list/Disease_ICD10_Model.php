<?php
require_once APPPATH . 'models/Entities/sih_list_disease_icd10.php';
require_once APPPATH . 'models/Entities/sih_list_group_icd10.php';
require_once APPPATH . 'models/Entities/sih_list_chapter_icd10.php';

/**
 * Disease ICD10 Model
 *
 * @since v.1.0
 */
class Disease_ICD10_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->listForeignKey = [
			"group_icd10_id" => "sih_list_group_icd10"
		];
		$this->mainTableName = "sih_list_disease_icd10";
		$this->mainEntityClassName = "Sih_list_disease_icd10";
	}

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object $apiGetMethodParamObject   api GetMethodParamObject
     * @param boolean $countMode countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_group_icd10_id           = new CollumJoin("k", "group_icd10_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_group_icd10_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_list_group_icd10";
        $joinTableQueryObjectEntityClassName = "sih_list_group_icd10";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
    
	/**
	 * get Code With ID
	 *
	 * @access    public
	 *
	 * @param   int  $id   The unique identifier for the object
	 *
	 * @return  string  code
	 */
	public function getCodeWithID($id)
	{
		return $id;
	}
}