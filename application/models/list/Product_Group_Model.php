<?php
require_once APPPATH . 'models/Entities/sih_product_group.php';

/**
 *  Product Group Model
 *
 * @since v.1.0
 */
class Product_Group_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->listForeignKey      = [];
		$this->mainTableName       = "sih_product_group";
		$this->mainEntityClassName = "Sih_product_group";
	}
}