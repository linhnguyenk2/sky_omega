<?php
require_once APPPATH . 'models/Entities/sih_subclinical_template.php';
/**
 * Subclinical Template Model
 *
 * @since v.1.0
 */
class Subclinical_Template_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->listForeignKey = [
		];

		$this->mainTableName = "sih_subclinical_template";
		$this->mainEntityClassName = "Sih_subclinical_template";
	}
}