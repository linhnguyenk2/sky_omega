<?php
require_once APPPATH . 'models/Entities/sih_list_titles.php';

/**
 * Titles Model
 *
 * @since v.1.0
 */
class Titles_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_titles";
        $this->mainEntityClassName = "Sih_list_titles";
    }

}
