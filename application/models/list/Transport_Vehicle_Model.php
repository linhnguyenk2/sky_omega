<?php
require_once APPPATH . 'models/Entities/sih_list_transport_vehicle.php';

/**
 * Transport Vehicle Model
 *
 * @since v.1.0
 */
class Transport_Vehicle_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_transport_vehicle";
        $this->mainEntityClassName = "Sih_list_transport_vehicle";
    }

}