<?php
require_once APPPATH . 'models/Entities/sih_product.php';

require_once APPPATH . 'models/Entities/sih_unit.php';
require_once APPPATH . 'models/Entities/sih_medicine_product.php';
require_once APPPATH . 'models/Entities/sih_functional_food_product.php';
require_once APPPATH . 'models/Entities/sih_medical_supply.php';
require_once APPPATH . 'models/Entities/sih_chemical_product.php';
require_once APPPATH . 'models/Entities/sih_cosmetic_product.php';
require_once APPPATH . 'models/Entities/sih_raw_material_product.php';
require_once APPPATH . 'models/Entities/sih_food_product.php';
require_once APPPATH . 'models/Entities/sih_product_group.php';

require_once APPPATH . 'models/Entities/sih_list_appointed_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_not_combine_medicine.php';

require_once APPPATH . 'models/Entities/sih_list_contraindication_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_contraindication.php';

/**
 * Food Product Model
 *
 * @since v.1.0
 */
class Food_Product_Model extends MY_Model
{
    /**
     * entityManager
     *
     * @var entityManager
     */
    protected $listSubForeignKey = array(
        'sih_product' => 'Product_Model'
    );

    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey      = [];
        $this->mainTableName       = "sih_food_product";
        $this->mainEntityClassName = "Sih_food_product";
    }

    /**
     * Method to update multi item.
     * 1/sih_product and
     * 2/sih_function_food
     * @access public
     *
     * @param string $ids
     *            list of id 1_2_12
     * @param array  $listParam
     *            item
     *
     * @return array
     */
    public function putMultiEvent($ids, $listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $data = array();
        // Update in sih_product
        $this->load->model('list/Product_Model');
        $newModel = new Product_Model;

        if ( ! empty($ids) && ! empty($listParam)) {
            unset($listParam['id']);

            $entityManager = $newModel->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    // find in sih_functional_food_product
                    $entity  = $entityManager->getRepository($newModel->getMainTableName())->findOneBy(array('food_id' => $id));
                    $entity2 = $entityManager->find($this->getMainTableName(), $id);

                    if (empty($entity) || empty($entity2)) {
                        $entityManager->getConnection()->rollBack();
                        $data = array();

                        return $data;
                    } else {
                        foreach ($listParam as $key => $value) {
                            if ($newModel->isItemIsForeignKey($key)) {
                                $listForeignKey = $newModel->getListForeignKey();
                                $tableName      = $listForeignKey[$key];
                                $value          = $entityManager->find($tableName, $value);

                                if (empty($value)) {
                                    $entityManager->getConnection()->rollBack();
                                    $data = array();

                                    return $data;
                                }
                            }

                            // Update sih_medical_examination_form
                            $methodSet = $newModel->getMethodNameInEntity($key);

                            if (method_exists($entity, $methodSet)) {
                                $entity->$methodSet($value);
                            }

                            // Update sih_breast_examination_form
                            $methodSet2 = $this->getMethodNameInEntity($key);

                            if (method_exists($entity2, $methodSet2)) {
                                $entity2->$methodSet2($value);
                            }
                        }

                        $entityManager->flush();

                        $data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
                    }
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $data = array();
            }
        } else {
            $data = array();
        }

        return $data;
    }


    /**
     * Method to get an item
     *
     * @access public
     *
     * @param string $id
     *            primary key
     *
     * @return object An object of data items on success, false on failure.
     */
    public function get($id, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $this->load->model('list/Product_Model');
        $newModel = new Product_Model;

        $entityManager = $newModel->entityManager;
        $entity        = $entityManager->getRepository($newModel->getMainTableName())->findOneBy(array('food_id' => $id));

        $data = new stdClass();

        if ( ! empty($entity)) {
            $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
        }

        return $data;
    }

    /**
     * get Query for Get List call from sih_product
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_unit_id          = new CollumJoin("ku", "unit_id");
        $column_join_food_id          = new CollumJoin("kr", "food_id");
        $column_join_product_group_id = new CollumJoin("kp", "product_group_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);


        $this->load->model('list/Product_Model');
        $newModel                            = new Product_Model;
        $mainTableQueryObjectTableName       = $newModel->mainTableName;
        $mainTableQueryObjectEntityClassName = $newModel->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_unit_id);
        $mainTableQueryObject->addCollumJoin($column_join_food_id);
        $mainTableQueryObject->addCollumJoin($column_join_product_group_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_unit";
        $joinTableQueryObjectEntityClassName = "sih_unit";
        $joinTableQueryObjectTableAlias      = "ku";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_food_product";
        $joinTableQueryObjectEntityClassName = "sih_food_product";
        $joinTableQueryObjectTableAlias      = "kr";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_product_group";
        $joinTableQueryObjectEntityClassName = "sih_product_group";
        $joinTableQueryObjectTableAlias      = "kp";
        $joinTableQueryObject3               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject3->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject3->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);


        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;
        $listJoinTableQueryObject[] = $joinTableQueryObject3;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }

    /**
     * Method to delete multi item. and five table
     * 1/sih_food and
     * 2/sih_product
     *
     * @access public
     *
     * @param array $ids
     *            list of id [1,2,3]
     *
     * @return boolean
     */
    public function deleteMultiEvent($ids)
    {
        $status = true;

        if ( ! empty($ids)) {
            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    // Return 11000 not found this item
                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $status = false;
                        break;
                    }

                    // Find another foreign table and delete - sih_medical_examination_form
                    $listSubForeignKey = $this->listSubForeignKey;

                    foreach ($listSubForeignKey as $entityName => $subModelClassName) {
                        if ($entityName == 'sih_product') {
                            // Find not combine medicine first
                            $this->load->model('list/' . $subModelClassName);
                            $listEntity2 = $entityManager->getRepository($entityName)->findBy(array('food_id' => $id));

                            if ( ! empty($listEntity2)) {
                                foreach ($listEntity2 as $entity2) {
                                    $entityManager->remove($entity2);
                                    $entityManager->flush();
                                }
                            }
                        }
                    }

                    $entityManager->remove($entity);
                    $entityManager->flush();
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $status = false;
            }
        } else {
            $status = false;
        }

        return $status;
    }

    /**
     * Method to insert item for sub.
     *
     * @access public
     *
     * @param   array $item item
     *
     * @return object An object of data items on success, false on failure.
     */
    protected function postSubEvent($newModel, $item, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $newModel->entityManager;

        $mainEntityClassName = $newModel->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $item['created_at'] = new DateTime("now");

        if ( ! empty($item)) {
            // Check duplicate
            $tmpFindArray            = array();
            $tmpFindArray['food_id'] = $item['food_id'];

            // food_id
            $listEntity2 = $entityManager->getRepository($newModel->getMainTableName())->findBy($tmpFindArray);

            // Check if have duplicate, rollback
            if ( ! empty($listEntity2)) {
                $data = array();

                return $data;
            }

            foreach ($item as $key => $value) {
                if ($newModel->isItemIsForeignKey($key)) {
                    $listForeignKey = $newModel->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $newModel->getMethodNameInEntity($key);

                // Check method
                if (method_exists($entityObject, $methodSet)) {
                    $entityObject->$methodSet($value);
                }
            }
        }

        $entityManager->persist($entityObject);
        $entityManager->flush();

        $lastInsertId = $entityObject->getId();
        $entity       = $entityManager->find($newModel->getMainTableName(), $lastInsertId);

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }

    /**
     * Method to insert item. - insert in two table sih_product and sih_food_product
     *
     * @access public
     *
     * @param array $listParam
     *            item
     *
     * @return object An object of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;
        $entityManager->getConnection()->beginTransaction();
        $entityManager->getConnection()->setAutoCommit(false);

        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $listParam['created_at'] = new DateTime("now");

        // Add temp - to get code - stt = 0
        if (isset($listParam['stat']) && $listParam['stat'] == 0) {

            if ( ! empty($listParam)) {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if ( ! $this->isItemIsForeignKey($key)) {
                        $methodSet = $this->getMethodNameInEntity($key);
                        // Check method
                        if (method_exists($entityObject, $methodSet)) {
                            $entityObject->$methodSet($value);
                        }
                    }
                }
            }
        } elseif ( ! empty($listParam)) {

            foreach ($listParam as $key => $value) {
                // Is this Id exist in foreign table
                if ($this->isItemIsForeignKey($key)) {
                    $listForeignKey = $this->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $this->getMethodNameInEntity($key);

                if (method_exists($entityObject, $methodSet)) {
                    $entityObject->$methodSet($value);
                }
            }
        }

        // Rollback
        try {
            $entityManager->persist($entityObject);
            $entityManager->flush();

            $lastInsertId = $entityObject->getId();

            // Get last insert ID
            $entity = $entityManager->find($this->getMainTableName(), $lastInsertId);

            if ($entityObject->isHaveCodeField()) {
                $newCode = $this->getCodeWithID($lastInsertId);
                $entity->setCode($newCode);
                $entityManager->flush();

                $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
            }

            // Insert in  sih_medical_record
            $this->load->model('list/Product_Model');

            $subItem            = $listParam;
            $subItem['food_id'] = $lastInsertId;

            if (isset($newCode)) {
                $subItem['code'] = $newCode;
            }

            $newModel = new Product_Model;

            $result2 = $this->postSubEvent($newModel, $subItem, $listReferredColumn, $listSelfReferredColumn);

            if (empty($result2)) {
                $entityManager->getConnection()->rollBack();
                $data = array();

                return $data;
            }

            $entityManager->getConnection()->commit();
            $entityManager->close();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollBack();
            $data = array();
        }

        return $result2;
    }
}