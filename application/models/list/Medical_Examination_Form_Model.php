<?php
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_staff.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';


require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';
require_once APPPATH . 'models/Entities/sih_analysis_paper.php';
require_once APPPATH . 'models/Entities/sih_specify_service_paper.php';
require_once APPPATH . 'models/Entities/sih_service.php';
require_once APPPATH . 'models/Entities/sih_service_location.php';
require_once APPPATH . 'models/Entities/sih_service_type.php';
require_once APPPATH . 'models/Entities/sih_list_departments.php';
require_once APPPATH . 'models/Entities/sih_service_group.php';

require_once APPPATH . 'models/Entities/sih_family_planing_book.php';
require_once APPPATH . 'models/Entities/sih_family_plan_birth_control_form.php';
require_once APPPATH . 'models/Entities/sih_artificial_examination_form.php';
require_once APPPATH . 'models/Entities/sih_medical_record.php';
require_once APPPATH . 'models/Entities/sih_prenatal_book.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_book.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form.php';
require_once APPPATH . 'models/Entities/sih_prenatal_form.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_form.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_form.php';
require_once APPPATH . 'models/Entities/sih_menopause_book.php';
require_once APPPATH . 'models/Entities/sih_menopause_form.php';
require_once APPPATH . 'models/Entities/sih_analysis_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_health_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_pregnancy_changes_in_health_book.php';

/**
 * Medical Examination Form Model
 *
 * @since v.1.0
 */
class Medical_Examination_Form_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            "patient_id"                                 => "sih_patients",
            "staff_id"                                   => "sih_staff",
            "medical_record_id"                          => "sih_medical_record",
            "breast_examination_form_id"                 => "sih_breast_examination_form",
            "gynecolog_form_id"                          => "sih_gynecolog_form",
            "prenatal_form_id"                           => "sih_prenatal_form",
            "menopause_form_id"                          => "sih_menopause_form",
            "artificial_examination_form_id"             => "sih_artificial_examination_form",
            "family_plan_birth_planing_form_id"          => "sih_family_plan_birth_control_form",
            "specify_service_paper_id"                   => "sih_specify_service_paper",
            "medical_examination_form_in_health_book_id" => "sih_medical_examination_form_in_health_book",
            "staff_result_id"                            => "sih_staff"
        ];

        $this->mainTableName       = "sih_medical_examination_form";
        $this->mainEntityClassName = "Sih_medical_examination_form";
    }

    /**
     * Method to insert item.
     *
     * @access public
     *
     * @param array $listParam
     *            item
     *
     * @return object An object of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;

        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $listParam['created_at'] = new DateTime("now");

        if (isset($listParam['check_in_time'])) {
            $listParam['check_in_time'] = new DateTime($listParam['check_in_time']);
        } else {
            $listParam['check_in_time'] = new DateTime("now");
        }

        if (isset($listParam['re_examination_date'])) {
            $listParam['re_examination_date'] = new DateTime($listParam['re_examination_date']);
        } else {
            $listParam['re_examination_date'] = new DateTime("now");
        }

        // Add temp - to get code - stt = 0
        if (isset($listParam['stat']) && $listParam['stat'] == 0) {
            if ( ! empty($listParam)) {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if ( ! $this->isItemIsForeignKey($key)) {
                        $methodSet = $this->getMethodNameInEntity($key);
                        // Check method
                        if (method_exists($entityObject, $methodSet)) {
                            $entityObject->$methodSet($value);
                        }
                    }
                }
            }
        } elseif ( ! empty($listParam)) {
            foreach ($listParam as $key => $value) {
                // Is this Id exist in foreign table
                if ($this->isItemIsForeignKey($key)) {
                    $listForeignKey = $this->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $this->getMethodNameInEntity($key);
                // Check method
                if (method_exists($entityObject, $methodSet)) {
                    $entityObject->$methodSet($value);
                }
            }
        }

        $entityManager->persist($entityObject);
        $entityManager->flush();

        $lastInsertId = $entityObject->getId();
        $entity       = $entityManager->find($this->getMainTableName(), $lastInsertId);

        if ($entityObject->isHaveCodeField()) {
            $newCode = $this->getCodeWithID($lastInsertId);
            $entity->setCode($newCode);
            $entityManager->flush();
        }

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }

    /**
     * Method to update multi item.
     *
     * @access public
     *
     * @param string $ids
     *            list of id 1_2_12
     * @param array  $listParam
     *            item
     *
     * @return array
     */
    public function putMultiEvent($ids, $listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $data = array();

        if ( ! empty($ids) && ! empty($listParam)) {
            unset($listParam['id']);

            if (isset($listParam['last_modified'])) {
                $listParam['last_modified'] = new DateTime("now");
            }

            if (isset($listParam['check_in_time'])) {
                $listParam['check_in_time'] = new DateTime($listParam['check_in_time']);
            } else {
                $listParam['check_in_time'] = new DateTime("now");
            }

            if (isset($listParam['re_examination_date'])) {
                $listParam['re_examination_date'] = new DateTime($listParam['re_examination_date']);
            } else {
                $listParam['re_examination_date'] = new DateTime("now");
            }

            if (isset($listParam['date_perform'])) {
                $listParam['date_perform'] = new DateTime($listParam['date_perform']);
            } else {
                $listParam['date_perform'] = new DateTime("now");
            }

            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $data = array();

                        return $data;
                    } else {
                        foreach ($listParam as $key => $value) {
                            if ($this->isItemIsForeignKey($key)) {
                                $listForeignKey = $this->getListForeignKey();
                                $tableName      = $listForeignKey[$key];
                                $value          = $entityManager->find($tableName, $value);

                                if (empty($value)) {
                                    $entityManager->getConnection()->rollBack();
                                    $data = array();

                                    return $data;
                                }
                            }

                            $methodSet = $this->getMethodNameInEntity($key);
                            $entity->$methodSet($value);
                        }

                        $entityManager->flush();

                        $data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
                    }
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $data = array();
            }
        } else {
            $data = array();
        }

        return $data;
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id                         = new CollumJoin("k", "patient_id");
        $column_join_medical_record_id                  = new CollumJoin("km", "medical_record_id");
        $column_join_gynecolog_form_id                  = new CollumJoin("kg", "gynecolog_form_id");
        $column_join_breast_examination_form_id         = new CollumJoin("kb", "breast_examination_form_id");
        $column_join_menopause_form_id                  = new CollumJoin("kmenopause", "menopause_form_id");
        $column_join_prenatal_form_id                   = new CollumJoin("kp", "prenatal_form_id");
        $column_join_artificial_examination_form_id     = new CollumJoin("ka", "artificial_examination_form_id");
        $column_join_family_plan_birth_planing_form_id  = new CollumJoin("kf", "family_plan_birth_planing_form_id");
        $column_medical_examination_form_in_health_book_id = new CollumJoin("kmef",
            "medical_examination_form_in_health_book_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addCollumJoin($column_join_medical_record_id);
        $mainTableQueryObject->addCollumJoin($column_join_gynecolog_form_id);
        $mainTableQueryObject->addCollumJoin($column_join_breast_examination_form_id);
        $mainTableQueryObject->addCollumJoin($column_join_menopause_form_id);
        $mainTableQueryObject->addCollumJoin($column_join_prenatal_form_id);
        $mainTableQueryObject->addCollumJoin($column_join_artificial_examination_form_id);
        $mainTableQueryObject->addCollumJoin($column_join_family_plan_birth_planing_form_id);
        $mainTableQueryObject->addCollumJoin($column_medical_examination_form_in_health_book_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_medical_record";
        $joinTableQueryObjectEntityClassName = "sih_medical_record";
        $joinTableQueryObjectTableAlias      = "km";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_gynecolog_form";
        $joinTableQueryObjectEntityClassName = "sih_gynecolog_form";
        $joinTableQueryObjectTableAlias      = "kg";
        $joinTableQueryObject3               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject3->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject3->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_breast_examination_form";
        $joinTableQueryObjectEntityClassName = "sih_breast_examination_form";
        $joinTableQueryObjectTableAlias      = "kb";
        $joinTableQueryObject4               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject4->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject4->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_menopause_form";
        $joinTableQueryObjectEntityClassName = "sih_menopause_form";
        $joinTableQueryObjectTableAlias      = "kmenopause";
        $joinTableQueryObject5               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject5->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject5->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_prenatal_form";
        $joinTableQueryObjectEntityClassName = "sih_prenatal_form";
        $joinTableQueryObjectTableAlias      = "kp";
        $joinTableQueryObject6               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject6->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject6->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_artificial_examination_form";
        $joinTableQueryObjectEntityClassName = "sih_artificial_examination_form";
        $joinTableQueryObjectTableAlias      = "ka";
        $joinTableQueryObject7               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject7->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject7->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_family_plan_birth_control_form";
        $joinTableQueryObjectEntityClassName = "sih_family_plan_birth_control_form";
        $joinTableQueryObjectTableAlias      = "kf";
        $joinTableQueryObject8               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject8->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject8->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_medical_examination_form_in_health_book";
        $joinTableQueryObjectEntityClassName = "sih_medical_examination_form_in_health_book";
        $joinTableQueryObjectTableAlias      = "kmef";
        $joinTableQueryObject9               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject9->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject9->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;
        $listJoinTableQueryObject[] = $joinTableQueryObject3;
        $listJoinTableQueryObject[] = $joinTableQueryObject4;
        $listJoinTableQueryObject[] = $joinTableQueryObject5;
        $listJoinTableQueryObject[] = $joinTableQueryObject6;
        $listJoinTableQueryObject[] = $joinTableQueryObject7;
        $listJoinTableQueryObject[] = $joinTableQueryObject8;
        $listJoinTableQueryObject[] = $joinTableQueryObject9;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}