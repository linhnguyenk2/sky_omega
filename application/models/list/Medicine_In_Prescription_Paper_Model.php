<?php
require_once APPPATH . 'models/Entities/sih_medicine_in_prescription_paper.php';
require_once APPPATH . 'models/Entities/sih_prescription_paper.php';

require_once APPPATH . 'models/Entities/sih_specify_service_paper.php';
require_once APPPATH . 'models/Entities/sih_service_location.php';
require_once APPPATH . 'models/Entities/sih_service.php';
require_once APPPATH . 'models/Entities/sih_service_type.php';
require_once APPPATH . 'models/Entities/sih_service_group.php';

require_once APPPATH . 'models/Entities/sih_medicine_product.php';

require_once APPPATH . 'models/Entities/sih_list_appointed_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_not_combine_medicine.php';

require_once APPPATH . 'models/Entities/sih_list_contraindication_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_contraindication.php';
require_once APPPATH . 'models/Entities/sih_list_departments.php';

require_once APPPATH . 'models/Entities/sih_product_unit_exchange.php';
require_once APPPATH . 'models/Entities/sih_product.php';
require_once APPPATH . 'models/Entities/sih_unit.php';
require_once APPPATH . 'models/Entities/sih_medicine_product.php';
require_once APPPATH . 'models/Entities/sih_functional_food_product.php';
require_once APPPATH . 'models/Entities/sih_medical_supply.php';
require_once APPPATH . 'models/Entities/sih_chemical_product.php';
require_once APPPATH . 'models/Entities/sih_cosmetic_product.php';
require_once APPPATH . 'models/Entities/sih_raw_material_product.php';
require_once APPPATH . 'models/Entities/sih_food_product.php';
require_once APPPATH . 'models/Entities/sih_product_group.php';
require_once APPPATH . 'models/Entities/sih_list_supplier.php';

require_once APPPATH . 'models/Entities/sih_family_planing_book.php';
require_once APPPATH . 'models/Entities/sih_family_plan_birth_control_form.php';
require_once APPPATH . 'models/Entities/sih_artificial_examination_form.php';
require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_staff.php';
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';

require_once APPPATH . 'models/Entities/sih_medical_record.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_book.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_form.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_form.php';

require_once APPPATH . 'models/Entities/sih_medical_examination_form.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';
require_once APPPATH . 'models/Entities/sih_prenatal_form.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_book.php';
require_once APPPATH . 'models/Entities/sih_prenatal_book.php';
require_once APPPATH . 'models/Entities/sih_menopause_book.php';
require_once APPPATH . 'models/Entities/sih_menopause_form.php';
require_once APPPATH . 'models/Entities/sih_analysis_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_health_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_pregnancy_changes_in_health_book.php';
/**
 * Medicine In Prescription Paper Model
 *
 * @since v.1.0
 */
class Medicine_In_Prescription_Paper_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            "prescription_paper_id" => "sih_prescription_paper",
            "product_id"            => "sih_product"
        ];

        $this->mainTableName       = "sih_medicine_in_prescription_paper";
        $this->mainEntityClassName = "Sih_medicine_in_prescription_paper";
    }
}