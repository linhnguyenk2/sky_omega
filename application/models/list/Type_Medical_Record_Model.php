<?php
require_once APPPATH . 'models/Entities/sih_list_type_medical_record.php';

/**
 * Type Medical Record Model
 *
 * @since v.1.0
 */
class Type_Medical_Record_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->listForeignKey = [];
		$this->mainTableName = "sih_list_type_medical_record";
		$this->mainEntityClassName = "Sih_list_type_medical_record";
	}
}