<?php
require_once APPPATH . 'models/Entities/sih_warehouse.php';

/**
 * Service Location Model
 *
 * @since v.1.0
 */
class Warehouse_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [];

        $this->mainTableName       = "sih_warehouse";
        $this->mainEntityClassName = "Sih_warehouse";
    }

}
