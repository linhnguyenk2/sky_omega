<?php

/**
 * Insurrance Company Relationship Model
 *
 * @since v.1.0
 */
require_once APPPATH . 'models/Entities/sih_list_insurance_company.php';

class Insurrance_Company_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_insurance_company";
        $this->mainEntityClassName = "Sih_list_insurance_company";
    }

}