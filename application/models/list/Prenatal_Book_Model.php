<?php
require_once APPPATH . 'models/Entities/sih_prenatal_book.php';
require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';


require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

require_once APPPATH . 'models/Entities/sih_medical_record.php';

/**
 * Prenatal Book Model
 *
 * @since v.1.0
 */
class Prenatal_Book_Model extends MY_Model
{
    /**
     * entityManager
     *
     * @var entityManager
     */
    protected $listSubForeignKey = array(
        'sih_medical_record' => 'Medical_Record_Model'
    );

    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey      = [
            "patient_id" => "sih_patients"
        ];
        $this->mainTableName       = "sih_prenatal_book";
        $this->mainEntityClassName = "Sih_prenatal_book";
    }

    /**
     * Method to update multi item.
     *
     * @access public
     *
     * @param string $ids
     *            list of id 1_2_12
     * @param array  $listParam
     *            item
     *
     * @return array
     */
    public function putMultiEvent($ids, $listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $data = array();

        if ( ! empty($ids) && ! empty($listParam)) {
            unset($listParam['id']);

            if (isset($listParam['last_modified'])) {
                $listParam['last_modified'] = new DateTime("now");
            }

            if (isset($listParam['last_period'])) {
                $listParam['last_period'] = new DateTime($listParam['last_period']);
            } else {
                $listParam['last_period'] = new DateTime("now");
            }

            if (isset($listParam['expected_date_confinement'])) {
                $listParam['expected_date_confinement'] = new DateTime($listParam['expected_date_confinement']);
            } else {
                $listParam['expected_date_confinement'] = new DateTime("now");
            }

            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $data = array();

                        return $data;
                    } else {
                        foreach ($listParam as $key => $value) {
                            if ($this->isItemIsForeignKey($key)) {
                                $listForeignKey = $this->getListForeignKey();
                                $tableName      = $listForeignKey[$key];
                                $value          = $entityManager->find($tableName, $value);

                                if (empty($value)) {
                                    $entityManager->getConnection()->rollBack();
                                    $data = array();

                                    return $data;
                                }
                            }

                            $methodSet = $this->getMethodNameInEntity($key);
                            $entity->$methodSet($value);
                        }

                        $entityManager->flush();

                        $data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
                    }
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $data = array();
            }
        } else {
            $data = array();
        }

        return $data;
    }

    /**
     * Method to delete multi item. and five table
     * 1/sih_gynecolog_form and
     * 2/sih_medical_examination_form
     *
     * @access public
     *
     * @param array $ids
     *            list of id [1,2,3]
     *
     * @return boolean
     */
    public function deleteMultiEvent($ids)
    {
        $status = true;

        if ( ! empty($ids)) {
            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    // Return 11000 not found this item
                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $status = false;
                        break;
                    }

                    // Find another foreign table and delete - sih_medical_examination_form
                    $listSubForeignKey = $this->listSubForeignKey;

                    foreach ($listSubForeignKey as $entityName => $subModelClassName) {
                        if ($entityName == 'sih_medical_record') {
                            // Find not combine medicine first
                            $this->load->model('list/' . $subModelClassName);
                            $listEntity2 = $entityManager->getRepository($entityName)->findBy(array('prenatal_book_id' => $id));

                            if ( ! empty($listEntity2)) {
                                foreach ($listEntity2 as $entity2) {
                                    $entityManager->remove($entity2);
                                    $entityManager->flush();
                                }
                            }
                        }
                    }

                    $entityManager->remove($entity);
                    $entityManager->flush();
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $status = false;
            }
        } else {
            $status = false;
        }

        return $status;
    }

    /**
     * Method to insert item for sub.
     *
     * @access public
     *
     * @param   array $item item
     *
     * @return object An object of data items on success, false on failure.
     */
    protected function postSubEvent($newModel, $item, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $newModel->entityManager;

        $mainEntityClassName = $newModel->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $item['created_at'] = new DateTime("now");

        if (isset($item['last_period'])) {
            $item['last_period'] = new DateTime($item['last_period']);
        } else {
            $item['last_period'] = new DateTime("now");
        }

        if (isset($item['expected_date_confinement'])) {
            $item['expected_date_confinement'] = new DateTime($item['expected_date_confinement']);
        } else {
            $item['expected_date_confinement'] = new DateTime("now");
        }

        if ( ! empty($item)) {
            // Check duplicate
            $tmpFindArray                     = array();
            $tmpFindArray['prenatal_book_id'] = $item['prenatal_book_id'];
            $tmpFindArray['type']             = $item['type'];

            // type and prenatal_book_id
            $listEntity2 = $entityManager->getRepository($newModel->getMainTableName())->findBy($tmpFindArray);

            // Check if have duplicate, rollback
            if ( ! empty($listEntity2)) {
                $data = array();

                return $data;
            }

            foreach ($item as $key => $value) {
                if ($newModel->isItemIsForeignKey($key)) {
                    $listForeignKey = $newModel->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $newModel->getMethodNameInEntity($key);

                // Check method
                if (method_exists($entityObject, $methodSet)) {
                    $entityObject->$methodSet($value);
                }
            }
        }

        $entityManager->persist($entityObject);
        $entityManager->flush();

        $lastInsertId = $entityObject->getId();
        $entity       = $entityManager->find($newModel->getMainTableName(), $lastInsertId);

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }

    /**
     * Method to insert item. - insert in two table sih_prenatal_book and sih_medical_record type = 2 (so kham thai)
     *
     * @access public
     *
     * @param array $listParam
     *            item
     *
     * @return object An object of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;
        $entityManager->getConnection()->beginTransaction();
        $entityManager->getConnection()->setAutoCommit(false);

        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $listParam['created_at']    = new DateTime("now");
        $listParam['last_modified'] = new DateTime("now");


        // Add temp - to get code - stt = 0
        if (isset($listParam['stat']) && $listParam['stat'] == 0) {

            if ( ! empty($listParam)) {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if ( ! $this->isItemIsForeignKey($key)) {
                        $methodSet = $this->getMethodNameInEntity($key);
                        // Check method
                        if (method_exists($entityObject, $methodSet)) {
                            $entityObject->$methodSet($value);
                        }
                    }
                }
            }
        } elseif ( ! empty($listParam)) {

            foreach ($listParam as $key => $value) {
                // Is this Id exist in foreign table
                if ($this->isItemIsForeignKey($key)) {
                    $listForeignKey = $this->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $this->getMethodNameInEntity($key);
                // Check method
                if (method_exists($entityObject, $methodSet)) {
                    $entityObject->$methodSet($value);
                }
            }
        }

        // Rollback
        try {
            $entityManager->persist($entityObject);
            $entityManager->flush();

            $lastInsertId = $entityObject->getId();

            // Get last insert ID
            $entity = $entityManager->find($this->getMainTableName(), $lastInsertId);

            if ($entityObject->isHaveCodeField()) {
                $newCode = $this->getCodeWithID($lastInsertId);
                $entity->setCode($newCode);
                $entityManager->flush();

                $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
            }

            // Insert in  sih_medical_record
            $this->load->model('list/Medical_Record_Model');

            $subItem                     = $listParam;
            $subItem['prenatal_book_id'] = $lastInsertId;
            // type = 2 = prenatal_book
            $subItem['type'] = 2;

            if (isset($newCode)) {
                $subItem['code'] = $newCode;
            }

            $newModel = new Medical_Record_Model;

            $result2 = $this->postSubEvent($newModel, $subItem, $listReferredColumn, $listSelfReferredColumn);

            if (empty($result2)) {
                $entityManager->getConnection()->rollBack();
                $data = array();

                return $data;
            }

            $entityManager->getConnection()->commit();
            $entityManager->close();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollBack();
            $data = array();
        }

        return $data;
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id = new CollumJoin("k", "patient_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = array();
        $listJoinTableQueryObject[] = $joinTableQueryObject;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}