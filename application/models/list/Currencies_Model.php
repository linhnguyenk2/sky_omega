<?php
require_once APPPATH . 'models/Entities/sih_list_currencies.php';

/**
 * Currencies Model
 *
 * @since v.1.0
 */
class Currencies_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_currencies";
        $this->mainEntityClassName = "Sih_list_currencies";
    }

}
