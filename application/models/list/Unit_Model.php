<?php
require_once APPPATH . 'models/Entities/sih_unit.php';

/**
 * Unit Model
 *
 * @since v.1.0
 */
class Unit_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->listForeignKey = [];
		$this->mainTableName = "sih_unit";
		$this->mainEntityClassName = "Sih_unit";
	}
}