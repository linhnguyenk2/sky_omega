<?php
require_once APPPATH . 'models/Entities/sih_list_group_icd10.php';
require_once APPPATH . 'models/Entities/sih_list_chapter_icd10.php';

/**
 * Group ICD10 Model
 *
 * @since v.1.0
 */
class Group_ICD10_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->listForeignKey = [
			"chapter_icd10_id" => "sih_list_chapter_icd10"
		];
		$this->mainTableName = "sih_list_group_icd10";
		$this->mainEntityClassName = "Sih_list_group_icd10";
	}

	/**
	 * get Code With ID
	 *
	 * @access    public
	 *
	 * @param   int  $id   The unique identifier for the object
	 *
	 * @return  string  code
	 */
	public function getCodeWithID($id)
	{
		return $id;
	}
}