<?php
require_once APPPATH . 'models/Entities/sih_service_location.php';
require_once APPPATH . 'models/Entities/sih_list_departments.php';
require_once APPPATH . 'models/Entities/sih_service.php';
require_once APPPATH . 'models/Entities/sih_service_type.php';
require_once APPPATH . 'models/Entities/sih_service_group.php';
/**
 * Service Location Model
 *
 * @since v.1.0
 */
class Service_Location_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            "department_id" => "sih_list_departments"
        ];

        $this->mainTableName       = "sih_service_location";
        $this->mainEntityClassName = "Sih_service_location";
    }

}
