<?php
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';

require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

/**
 * Patient Emergency Contact Model
 *
 * @since v.1.0
 */
class Patient_Emergency_Contact_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            'patient_id'                        => 'sih_patients',
            'province_id'                       => 'sih_list_provinces',
            'district_id'                       => 'sih_list_districts',
            'ward_id'                           => 'sih_list_wards',
            'patient_emergency_id'              => 'sih_patients'
        ];

        $this->mainTableName       = "sih_patient_emergency_contact";
        $this->mainEntityClassName = "Sih_patient_emergency_contact";
    }

    /**
     * Method to insert item.
     *
     * @access public
     *
     * @param array $listParam
     *            item
     *
     * @return object An object of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;

        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $listParam['created_at'] = new DateTime("now");


        // Add temp - to get code - stt = 0
        if (isset($listParam['stat']) && $listParam['stat'] == 0) {

            if ( ! empty($listParam)) {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if ( ! $this->isItemIsForeignKey($key)) {
                        $methodSet = $this->getMethodNameInEntity($key);
                        // Check method
                        if (method_exists($entityObject, $methodSet)) {
                            $entityObject->$methodSet($value);
                        }
                    }
                }
            }
        } elseif ( ! empty($listParam)) {
            // Check Duplicate - Is these foreign key exist in this table
            if (isset($listParam['patient_emergency_id']) && isset($listParam['patient_id'])) {
                $listEntity = $entityManager->getRepository($mainEntityClassName)->findBy(
                    array(
                        'patient_emergency_id' => $listParam['patient_emergency_id'],
                        'patient_id'           => $listParam['patient_id']
                    )
                );

                if ( ! empty($listEntity) || $listParam['patient_emergency_id'] == $listParam['patient_id']) {
                    return null;
                }
            }

            foreach ($listParam as $key => $value) {
                // Is this Id exist in foreign table
                if ($this->isItemIsForeignKey($key)) {
                    $listForeignKey = $this->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $this->getMethodNameInEntity($key);

                $entityObject->$methodSet($value);
            }
        }

        $entityManager->persist($entityObject);
        $entityManager->flush();

        $lastInsertId = $entityObject->getId();

        $entity = $entityManager->find($this->getMainTableName(), $lastInsertId);

        if ($entityObject->isHaveCodeField()) {
            $newCode = $this->getCodeWithID($lastInsertId);
            $entity->setCode($newCode);
            $entityManager->flush();
        }

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id                        = new CollumJoin("k", "patient_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);


        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}