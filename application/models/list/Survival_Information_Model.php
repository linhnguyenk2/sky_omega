<?php
require_once APPPATH . 'models/Entities/sih_survival_information.php';

/**
 *  Survival Information Model
 *
 * @since v.1.0
 */

class Survival_Information_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_survival_information";
        $this->mainEntityClassName = "Sih_survival_information";
    }

}
