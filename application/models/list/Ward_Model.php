<?php
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';

/**
 * Ward Model
 *
 * @since v.1.0
 */
class Ward_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->listForeignKey = [
			"id_district" => "sih_list_districts"
		];
		$this->mainTableName = "sih_list_wards";
		$this->mainEntityClassName = "Sih_list_wards";
	}
	

	/**
	 * get Code With ID
	 *
	 * @access    public
	 *
	 * @param   int  $id   The unique identifier for the object
	 *
	 * @return  string  code
	 */
	public function getCodeWithID($id)
	{
		return $id;
	}
}
