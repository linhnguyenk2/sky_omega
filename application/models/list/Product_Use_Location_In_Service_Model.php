<?php
require_once APPPATH . 'models/Entities/sih_location_in_service.php';
require_once APPPATH . 'models/Entities/sih_product_use_location_in_service.php';

require_once APPPATH . 'models/Entities/sih_warehouse.php';
require_once APPPATH . 'models/Entities/sih_service_location.php';
require_once APPPATH . 'models/Entities/sih_service.php';
require_once APPPATH . 'models/Entities/sih_service_type.php';
require_once APPPATH . 'models/Entities/sih_service_group.php';
require_once APPPATH . 'models/Entities/sih_list_departments.php';

require_once APPPATH . 'models/Entities/sih_product.php';
require_once APPPATH . 'models/Entities/sih_unit.php';
require_once APPPATH . 'models/Entities/sih_medicine_product.php';
require_once APPPATH . 'models/Entities/sih_functional_food_product.php';
require_once APPPATH . 'models/Entities/sih_medical_supply.php';
require_once APPPATH . 'models/Entities/sih_chemical_product.php';
require_once APPPATH . 'models/Entities/sih_cosmetic_product.php';
require_once APPPATH . 'models/Entities/sih_raw_material_product.php';
require_once APPPATH . 'models/Entities/sih_food_product.php';
require_once APPPATH . 'models/Entities/sih_product_group.php';
require_once APPPATH . 'models/Entities/sih_product_unit_exchange.php';
require_once APPPATH . 'models/Entities/sih_list_contraindication_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_not_combine_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_appointed_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_contraindication.php';
require_once APPPATH . 'models/Entities/sih_list_supplier.php';/**
 * Product Use In Service Model
 *
 * @since v.1.0
 */
class Product_Use_Location_In_Service_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey      = [
            "product_id"             => "sih_product",
            "location_in_service_id" => "sih_location_in_service"
        ];
        $this->mainTableName       = "sih_product_use_location_in_service";
        $this->mainEntityClassName = "Sih_product_use_location_in_service";
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_product_id             = new CollumJoin("k", "product_id");
        $column_join_location_in_service_id = new CollumJoin("kl", "location_in_service_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_product_id);
        $mainTableQueryObject->addCollumJoin($column_join_location_in_service_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_product";
        $joinTableQueryObjectEntityClassName = "sih_product";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_location_in_service";
        $joinTableQueryObjectEntityClassName = "sih_location_in_service";
        $joinTableQueryObjectTableAlias      = "kl";
        $joinTableQueryObject2                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}