<?php
require_once APPPATH . 'models/Entities/sih_medicine_product.php';
require_once APPPATH . 'models/Entities/sih_list_appointed_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_contraindication_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_not_combine_medicine.php';

require_once APPPATH . 'models/Entities/sih_list_contraindication.php';

/**
 * Contraindication In Medicine Model
 *
 * @since v.1.0
 */
class Contraindication_In_Medicine_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->listForeignKey = [
			"medicine_id" => "sih_medicine_product",
			"contraindication_id" => "sih_list_contraindication"
		];

		$this->mainTableName = "sih_list_contraindication_in_medicine";
		$this->mainEntityClassName = "Sih_list_contraindication_in_medicine";
	}

    /**
     * Method to insert item.
     *
     * @access public
     *
     * @param array $listParam
     *            item
     *
     * @return object An object of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;

        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $listParam['created_at'] = new DateTime("now");



        // Add temp - to get code - stt = 0
        if (isset($listParam['stat']) && $listParam['stat'] == 0) {

            if ( ! empty($listParam)) {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if (!$this->isItemIsForeignKey($key)) {
                        $methodSet = $this->getMethodNameInEntity($key);
                        // Check method
                        if (method_exists($entityObject, $methodSet)) {
                            $entityObject->$methodSet($value);
                        }
                    }
                }
            }
        }
        elseif ( ! empty($listParam)) {
            // Check Duplicate - Is these foreign key exist in this table
            if (isset($listParam['medicine_id']) && isset($listParam['contraindication_id'])) {
                $listEntity = $entityManager->getRepository($mainEntityClassName)->findBy(
                    array(
                        'medicine_id'      => $listParam['medicine_id'],
                        'contraindication_id' => $listParam['contraindication_id']
                    )
                );

                if ( ! empty($listEntity)) {
                    return null;
                }
            }

            foreach ($listParam as $key => $value) {
                // Is this Id exist in foreign table
                if ($this->isItemIsForeignKey($key)) {
                    $listForeignKey = $this->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $this->getMethodNameInEntity($key);
                $entityObject->$methodSet($value);
            }
        }

        $entityManager->persist($entityObject);
        $entityManager->flush();

        $lastInsertId = $entityObject->getId();
        $entity       = $entityManager->find($this->getMainTableName(), $lastInsertId);

        if ($entityObject->isHaveCodeField()) {
            $newCode = $this->getCodeWithID($lastInsertId);
            $entity->setCode($newCode);
            $entityManager->flush();
        }

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }
}