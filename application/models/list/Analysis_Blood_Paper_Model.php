<?php
require_once APPPATH . 'models/Entities/sih_analysis_blood_paper.php';

require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_staff.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';


require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';
require_once APPPATH . 'models/Entities/sih_analysis_paper.php';


require_once APPPATH . 'models/Entities/sih_medical_record.php';
require_once APPPATH . 'models/Entities/sih_prenatal_book.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_book.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form.php';
require_once APPPATH . 'models/Entities/sih_prenatal_form.php';
require_once APPPATH . 'models/Entities/sih_breast_examination_form.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_form.php';
require_once APPPATH . 'models/Entities/sih_menopause_book.php';
require_once APPPATH . 'models/Entities/sih_menopause_form.php';
require_once APPPATH . 'models/Entities/sih_analysis_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_health_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_pregnancy_changes_in_health_book.php';
/**
 * Analysis_Blood Model
 *
 * @since v.1.0
 */
class Analysis_Blood_Paper_Model extends MY_Model
{
    /**
     * entityManager
     *
     * @var entityManager
     */
    protected $listSubForeignKey = array(
        'sih_analysis_paper' => 'Analysis_Paper_Model'
    );

	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->listForeignKey = [
            "patient_id"                  => "sih_patients",
            "staff_id"                    => "sih_staff",
            "approve_staff_id"            => "sih_staff",
            "medical_examination_form_id" => "sih_medical_examination_form",
            "analysis_blood_paper_id"     => "sih_analysis_blood_paper"
		];

		$this->mainTableName = "sih_analysis_blood_paper";
		$this->mainEntityClassName = "Sih_analysis_blood_paper";
	}

    /**
     * Method to delete multi item. and five table
     * 1/sih_gynecolog_form and
     * 2/sih_medical_examination_form
     *
     * @access public
     *
     * @param array $ids
     *            list of id [1,2,3]
     *
     * @return boolean
     */
    public function deleteMultiEvent($ids)
    {
        $status = true;

        if ( ! empty($ids)) {
            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    // Return 11000 not found this item
                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $status = false;
                        break;
                    }

                    // Find another foreign table and delete - sih_analysis_paper
                    $listSubForeignKey = $this->listSubForeignKey;

                    foreach ($listSubForeignKey as $entityName => $subModelClassName) {
                        if ($entityName == 'sih_analysis_paper') {
                            // Find not combine medicine first
                            $this->load->model('list/' . $subModelClassName);
                            $listEntity2 = $entityManager->getRepository($entityName)->findBy(array('analysis_blood_paper_id' => $id));

                            if ( ! empty($listEntity2)) {
                                foreach ($listEntity2 as $entity2) {
                                    $entityManager->remove($entity2);
                                    $entityManager->flush();
                                }
                            }
                        }
                    }

                    $entityManager->remove($entity);
                    $entityManager->flush();
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $status = false;
            }
        } else {
            $status = false;
        }

        return $status;
    }

    /**
     * Method to insert item for sub.
     *
     * @access public
     *
     * @param   array $item item
     *
     * @return object An object of data items on success, false on failure.
     */
    protected function postSubEvent($newModel, $item, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $newModel->entityManager;

        $mainEntityClassName = $newModel->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $item['created_at'] = new DateTime("now");

        if ( ! empty($item)) {
            // Check duplicate
            $tmpFindArray                               = array();
            $tmpFindArray['analysis_blood_paper_id'] = $item['analysis_blood_paper_id'];

            // type and breast_examination_book_id
            $listEntity2 = $entityManager->getRepository($newModel->getMainTableName())->findBy($tmpFindArray);

            // Check if have duplicate, rollback
            if ( ! empty($listEntity2)) {
                $data = array();

                return $data;
            }

            foreach ($item as $key => $value) {
                if ($newModel->isItemIsForeignKey($key)) {
                    $listForeignKey = $newModel->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $newModel->getMethodNameInEntity($key);

                // Check method
                if (method_exists($entityObject, $methodSet)) {
                    $entityObject->$methodSet($value);
                }
            }
        }

        $entityManager->persist($entityObject);
        $entityManager->flush();

        $lastInsertId = $entityObject->getId();
        $entity       = $entityManager->find($newModel->getMainTableName(), $lastInsertId);

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }

    /**
     * Method to insert item. - insert in two table sih_breast_examination_book and sih_medical_record type = 3 (so kham nhũ)
     *
     * @access public
     *
     * @param array $listParam
     *            item
     *
     * @return object An object of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;
        $entityManager->getConnection()->beginTransaction();
        $entityManager->getConnection()->setAutoCommit(false);

        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $listParam['created_at'] = new DateTime("now");

        // Máu - Analysis_Blood_Paper, type = 1
        $listParam['type'] = 1;
        // Add temp - to get code - stt = 0
        if (isset($listParam['stat']) && $listParam['stat'] == 0) {

            if ( ! empty($listParam)) {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if (!$this->isItemIsForeignKey($key)) {
                        $methodSet = $this->getMethodNameInEntity($key);
                        // Check method
                        if (method_exists($entityObject, $methodSet)) {
                            $entityObject->$methodSet($value);
                        }
                    }
                }
            }
        }
        elseif (isset($listParam['stat']) && $listParam['stat'] == 0) {

            if ( ! empty($listParam)) {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if (!$this->isItemIsForeignKey($key)) {
                        $methodSet = $this->getMethodNameInEntity($key);
                        // Check method
                        if (method_exists($entityObject, $methodSet)) {
                            $entityObject->$methodSet($value);
                        }
                    }
                }
            }
        }
        elseif ( ! empty($listParam)) {

            foreach ($listParam as $key => $value) {
                // Is this Id exist in foreign table
                if ($this->isItemIsForeignKey($key)) {
                    $listForeignKey = $this->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $this->getMethodNameInEntity($key);

                if (method_exists($entityObject, $methodSet))
                {
                    $entityObject->$methodSet($value);
                }
            }
        }

        // Rollback
        try {
            $entityManager->persist($entityObject);
            $entityManager->flush();

            $lastInsertId = $entityObject->getId();

            // Get last insert ID
            $entity = $entityManager->find($this->getMainTableName(), $lastInsertId);

            if ($entityObject->isHaveCodeField()) {
                $newCode = $this->getCodeWithID($lastInsertId);
                $entity->setCode($newCode);
                $entityManager->flush();

                $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
            }

            // Insert in  sih_medical_record
            $this->load->model('list/Analysis_Paper_Model');

            $subItem                               = $listParam;
            $subItem['analysis_blood_paper_id'] = $lastInsertId;

            if (isset($newCode)) {
                $subItem['code'] = $newCode;
            }

            $newModel = new Analysis_Paper_Model;

            $result2 = $this->postSubEvent($newModel, $subItem, $listReferredColumn, $listSelfReferredColumn);

            if (empty($result2)) {
                $entityManager->getConnection()->rollBack();
                $data = array();

                return $data;
            }

            $entityManager->getConnection()->commit();
            $entityManager->close();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollBack();
            $data = array();
        }

        return $result2;
    }

    /**
     * Method to update multi item.
     * 1/sih_breast_examination_form and
     * 2/sih_medical_examination_form
     * @access public
     *
     * @param string $ids
     *            list of id 1_2_12
     * @param array  $listParam
     *            item
     *
     * @return array
     */
    public function putMultiEvent($ids, $listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $data = array();
        // Update in sih_medical_examination_form
        $this->load->model('list/Analysis_Paper_Model');
        $newModel = new Analysis_Paper_Model;

        if ( ! empty($ids) && ! empty($listParam)) {
            unset($listParam['id']);

            // Máu - Analysis_Blood_Paper, type = 1
            $listParam['type'] = 1;
            $entityManager = $newModel->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    // find in sih_medical_examination_form
                    $entity = $entityManager->getRepository($newModel->getMainTableName())->findOneBy(array('analysis_blood_paper_id' => $id));
                    $entity2 = $entityManager->find($this->getMainTableName(), $id);

                    if (empty($entity) || empty($entity2)) {
                        $entityManager->getConnection()->rollBack();
                        $data = array();

                        return $data;
                    } else {
                        foreach ($listParam as $key => $value) {
                            if ($newModel->isItemIsForeignKey($key)) {
                                $listForeignKey = $newModel->getListForeignKey();
                                $tableName      = $listForeignKey[$key];
                                $value          = $entityManager->find($tableName, $value);

                                if (empty($value)) {
                                    $entityManager->getConnection()->rollBack();
                                    $data = array();

                                    return $data;
                                }
                            }

                            // Update sih_medical_examination_form
                            $methodSet = $newModel->getMethodNameInEntity($key);

                            if (method_exists($entity, $methodSet)) {
                                $entity->$methodSet($value);
                            }

                            // Update sih_breast_examination_form
                            $methodSet2 = $this->getMethodNameInEntity($key);

                            if (method_exists($entity2, $methodSet2)) {
                                $entity2->$methodSet2($value);
                            }
                        }

                        $entityManager->flush();

                        $data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
                    }
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $data = array();
            }
        } else {
            $data = array();
        }

        return $data;
    }

    /**
     * Method to get an item
     *
     * @access public
     *
     * @param string $id
     *            primary key
     *
     * @return object An object of data items on success, false on failure.
     */
    public function get($id, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $this->load->model('list/Analysis_Paper_Model');
        $newModel = new Analysis_Paper_Model;

        $entityManager = $newModel->entityManager;
        $entity        = $entityManager->getRepository($newModel->getMainTableName())->findOneBy(array('analysis_blood_paper_id' => $id));

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }

    /**
     * get Query for Get List call from sih_medical_examination_form
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id        = new CollumJoin("k", "patient_id");
        $column_join_medical_examination_form_id = new CollumJoin("km", "medical_examination_form_id");
        $column_join_analysis_blood_paper_id = new CollumJoin("kg", "analysis_blood_paper_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);


        $this->load->model('list/Analysis_Paper_Model');
        $newModel = new Analysis_Paper_Model;
        $mainTableQueryObjectTableName       = $newModel->mainTableName;
        $mainTableQueryObjectEntityClassName = $newModel->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addCollumJoin($column_join_medical_examination_form_id);
        $mainTableQueryObject->addCollumJoin($column_join_analysis_blood_paper_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_medical_examination_form";
        $joinTableQueryObjectEntityClassName = "sih_medical_examination_form";
        $joinTableQueryObjectTableAlias      = "km";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_analysis_blood_paper";
        $joinTableQueryObjectEntityClassName = "sih_analysis_blood_paper";
        $joinTableQueryObjectTableAlias      = "kg";
        $joinTableQueryObject3               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject3->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject3->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);


        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;
        $listJoinTableQueryObject[] = $joinTableQueryObject3;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}