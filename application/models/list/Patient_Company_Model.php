<?php
require_once APPPATH . 'models/Entities/sih_patient_company.php';
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';

require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

/**
 * Patient Company Model
 *
 * @since v.1.0
 */
class Patient_Company_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey      = [
            "patient_id"  => "sih_patients",
            "province_id" => "sih_list_provinces",
            "district_id" => "sih_list_districts",
            "ward_id"     => "sih_list_wards"
        ];
        $this->mainTableName       = "sih_patient_company";
        $this->mainEntityClassName = "Sih_patient_company";
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object $apiGetMethodParamObject   api GetMethodParamObject
     * @param boolean $countMode countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id  = new CollumJoin("k", "patient_id");
        $column_join_province_id = new CollumJoin("kp", "province_id");
        $column_join_district_id = new CollumJoin("kd", "district_id");
        $column_join_ward_id     = new CollumJoin("kw", "ward_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addCollumJoin($column_join_province_id);
        $mainTableQueryObject->addCollumJoin($column_join_district_id);
        $mainTableQueryObject->addCollumJoin($column_join_ward_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_list_provinces";
        $joinTableQueryObjectEntityClassName = "sih_list_provinces";
        $joinTableQueryObjectTableAlias      = "kp";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_list_districts";
        $joinTableQueryObjectEntityClassName = "sih_list_districts";
        $joinTableQueryObjectTableAlias      = "kd";
        $joinTableQueryObject3               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject3->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject3->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_list_wards";
        $joinTableQueryObjectEntityClassName = "sih_list_wards";
        $joinTableQueryObjectTableAlias      = "kw";
        $joinTableQueryObject4               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject4->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject4->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;
        $listJoinTableQueryObject[] = $joinTableQueryObject3;
        $listJoinTableQueryObject[] = $joinTableQueryObject4;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}
