<?php
require_once APPPATH . 'models/Entities/sih_service_group.php';

/**
 * Service Group Model
 *
 * @since v.1.0
 */
class Service_Group_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = array();

        $this->mainTableName = "sih_service_group";
        $this->mainEntityClassName = "Sih_service_group";
    }

}
