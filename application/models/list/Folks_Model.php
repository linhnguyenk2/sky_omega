<?php
require_once APPPATH . 'models/Entities/sih_list_folks.php';

/**
 * Folks Model
 *
 * @since v.1.0
 */
class Folks_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_folks";
        $this->mainEntityClassName = "Sih_list_folks";
    }

}
