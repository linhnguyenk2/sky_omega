<?php
require_once APPPATH . 'models/Entities/sih_warehouse.php';
require_once APPPATH . 'models/Entities/sih_warehouse_export_form.php';
require_once APPPATH . 'models/Entities/sih_warehouse_request_form.php';

require_once APPPATH . 'models/Entities/sih_staff.php';
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';

require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

/**
 * /**
 * Warehouse Export Form Model
 *
 * @since v.1.0
 */
class Warehouse_Export_Form_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            "staff_id"                 => "sih_staff",
            "storekeeper_staff_id"     => "sih_staff",
            "medicine_chief_staff_id"  => "sih_staff",
            "subclinic_chief_staff_id" => "sih_staff",
            "request_form_id"          => "sih_warehouse_request_form",
            "warehouse_id"             => "sih_warehouse"
        ];

        $this->mainTableName       = "sih_warehouse_export_form";
        $this->mainEntityClassName = "Sih_warehouse_export_form";
    }

    /**
     * get Query for Get List call from sih_medical_examination_form
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_staff_id                 = new CollumJoin("k", "staff_id");
        $column_join_storekeeper_staff_id     = new CollumJoin("km", "storekeeper_staff_id");
        $column_join_warehouse_id             = new CollumJoin("kg", "warehouse_id");
        $column_join_medicine_chief_staff_id  = new CollumJoin("kmcf", "medicine_chief_staff_id");
        $column_join_subclinic_chief_staff_id = new CollumJoin("kscf", "subclinic_chief_staff_id");
        $column_join_request_form_id          = new CollumJoin("kr", "request_form_id");


        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_staff_id);
        $mainTableQueryObject->addCollumJoin($column_join_storekeeper_staff_id);
        $mainTableQueryObject->addCollumJoin($column_join_warehouse_id);
        $mainTableQueryObject->addCollumJoin($column_join_medicine_chief_staff_id);
        $mainTableQueryObject->addCollumJoin($column_join_subclinic_chief_staff_id);
        $mainTableQueryObject->addCollumJoin($column_join_request_form_id);

        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_staff";
        $joinTableQueryObjectEntityClassName = "sih_staff";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_staff";
        $joinTableQueryObjectEntityClassName = "sih_staff";
        $joinTableQueryObjectTableAlias      = "km";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_warehouse";
        $joinTableQueryObjectEntityClassName = "sih_warehouse";
        $joinTableQueryObjectTableAlias      = "kg";
        $joinTableQueryObject3               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject3->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject3->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_staff";
        $joinTableQueryObjectEntityClassName = "sih_staff";
        $joinTableQueryObjectTableAlias      = "kmcf";
        $joinTableQueryObject4               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject4->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject4->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_staff";
        $joinTableQueryObjectEntityClassName = "sih_staff";
        $joinTableQueryObjectTableAlias      = "kscf";
        $joinTableQueryObject5               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject5->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject5->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_warehouse_request_form";
        $joinTableQueryObjectEntityClassName = "sih_warehouse_request_form";
        $joinTableQueryObjectTableAlias      = "kr";
        $joinTableQueryObject6               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject6->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject6->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;
        $listJoinTableQueryObject[] = $joinTableQueryObject3;
        $listJoinTableQueryObject[] = $joinTableQueryObject4;
        $listJoinTableQueryObject[] = $joinTableQueryObject5;
        $listJoinTableQueryObject[] = $joinTableQueryObject6;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}
