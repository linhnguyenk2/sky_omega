<?php
require_once APPPATH . 'models/Entities/sih_disease_icd10_in_gynecolog_book.php';
require_once APPPATH . 'models/Entities/sih_list_disease_icd10.php';
require_once APPPATH . 'models/Entities/sih_list_group_icd10.php';
require_once APPPATH . 'models/Entities/sih_list_chapter_icd10.php';


require_once APPPATH . 'models/Entities/sih_gynecolog_book.php';
require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';


require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

require_once APPPATH . 'models/Entities/sih_medical_record.php';

/**
 * Gynecolog Book Model
 *
 * @since v.1.0
 */
class Disease_Icd10_In_Gynecolog_Book_Model extends MY_Model
{
    /**
     * entityManager
     *
     * @var entityManager
     */
    protected $listSubForeignKey = array(
        'sih_medical_record' => 'Medical_Record_Model'
    );

    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            "disease_icd10_id"  => "sih_list_disease_icd10",
            "gynecolog_book_id" => "sih_gynecolog_book"
        ];

        $this->mainTableName       = "sih_disease_icd10_in_gynecolog_book";
        $this->mainEntityClassName = "Sih_disease_icd10_in_gynecolog_book";
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_disease_icd10_id  = new CollumJoin("k", "disease_icd10_id");
        $column_join_gynecolog_book_id = new CollumJoin("kg", "gynecolog_book_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_disease_icd10_id);
        $mainTableQueryObject->addCollumJoin($column_join_gynecolog_book_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_disease_icd10";
        $joinTableQueryObjectEntityClassName = "sih_disease_icd10";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_gynecolog_book";
        $joinTableQueryObjectEntityClassName = "sih_gynecolog_book";
        $joinTableQueryObjectTableAlias      = "kg";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = array();
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}