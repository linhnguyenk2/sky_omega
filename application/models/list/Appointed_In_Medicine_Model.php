<?php
require_once APPPATH . 'models/Entities/sih_medicine_product.php';
require_once APPPATH . 'models/Entities/sih_list_appointed_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_contraindication_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_not_combine_medicine.php';

require_once APPPATH . 'models/Entities/sih_list_contraindication.php';

/**
 * Appointed In Medicine Model
 *
 * @since v.1.0
 */
class Appointed_In_Medicine_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->listForeignKey = [
			"medicine_id" => "sih_medicine_product"
		];
		$this->mainTableName = "sih_list_appointed_in_medicine";
		$this->mainEntityClassName = "Sih_list_appointed_in_medicine";
	}	
}