<?php
require_once APPPATH . 'models/Entities/sih_list_contraindication.php';

/**
 * Contraindication Model
 *
 * @since v.1.0
 */
class Contraindication_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_contraindication";
        $this->mainEntityClassName = "Sih_list_contraindication";
    }
}
