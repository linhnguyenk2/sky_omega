<?php

require_once APPPATH . 'models/Entities/sih_list_room_type_in_package_healthcare_service.php';
require_once APPPATH . 'models/Entities/sih_list_package_healthcare_service_type.php';

require_once APPPATH . 'models/Entities/sih_list_package_healthcare_service.php';
require_once APPPATH . 'models/Entities/sih_list_package_healthcare_service_type.php';

require_once APPPATH . 'models/Entities/sih_list_patient_room_type.php';
require_once APPPATH . 'models/Entities/sih_medicine_product.php';



require_once APPPATH . 'models/Entities/sih_service.php';
require_once APPPATH . 'models/Entities/sih_service_type.php';

require_once APPPATH . 'models/Entities/sih_list_appointed_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_contraindication_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_not_combine_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_contraindication.php';


/**
 * Package Healthcare Service Model
 *
 * @since v.1.0
 */
class Package_Healthcare_Service_Model extends MY_Model
{
    /**
     * entityManager
     *
     * @var entityManager
     */
    protected $listSubForeignKey = array(

    );

    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey      = [
        ];
        $this->mainTableName       = "sih_list_package_healthcare_service";
        $this->mainEntityClassName = "Sih_list_package_healthcare_service";
    }

    /**
     * Method to insert item.
     *
     * @access public
     *
     * @param   array $item item
     *
     * @return object An object of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;
        $entityManager->getConnection()->beginTransaction();
        $entityManager->getConnection()->setAutoCommit(false);

        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $item['created_at'] = new DateTime("now");

        if ( ! empty($item)) {

            foreach ($item as $key => $value) {
                if ($this->isItemIsForeignKey($key)) {
                    $listForeignKey = $this->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

            }
        }
        try {
            $entityManager->persist($entityObject);
            $entityManager->flush();

            $lastInsertId = $entityObject->getId();

            $listSubForeignKey = $this->listSubForeignKey;

            foreach ($listSubForeignKey as $key => $subModelClassName) {
                if (isset($item[$key])) {
                    $list = json_decode($item[$key], true);
                    $this->load->model('list/' . $subModelClassName);
                    $newModel = new $subModelClassName;


                    foreach ($list as $value2) {
                        $subItem                                  = $value2;
                        $subItem['package_healthcare_service_id'] = $lastInsertId;
                        $subItem['created_at']                    = $item['created_at'];

                        $result2 = $this->postSubEvent($newModel, $subItem, $listReferredColumn,
                            $listSelfReferredColumn);

                        if (empty($result2)) {
                            $entityManager->getConnection()->rollBack();
                            $data = array();

                            return $data;
                        }
                    }
                }
            }
            // Get last insert ID
            $entity = $entityManager->find($this->getMainTableName(), $lastInsertId);

            if ($entityObject->isHaveCodeField()) {
                $newCode = $this->getCodeWithID($lastInsertId);
                $entity->setCode($newCode);
                $entityManager->flush();

                $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
            }

            $entityManager->getConnection()->commit();
            $entityManager->close();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollBack();
            $data = array();
        }

        return $data;
    }

    /**
     * Method to delete multi item. and five table
     * Subclinical_In_Package_Healthcare_Service_Model
     * Service_In_Package_Healthcare_Service_Model
     * Medical_Supplies_In_Package_Healthcare_Service_Model
     * Room_Type_In_Package_Healthcare_Service_Model
     * Medicine_In_Package_Healthcare_Service_Model
     *
     * @access public
     *
     * @param array $ids
     *            list of id [1,2,3]
     *
     * @return boolean
     */
    public function deleteMultiEvent($ids)
    {
        $status = true;

        if ( ! empty($ids)) {
            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    // Return 11000 not found this item
                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $status = false;
                        break;
                    }

                    //find anotther table

                    $listSubForeignKey = $this->listSubForeignKey;

                    foreach ($listSubForeignKey as $key => $subModelClassName) {
                        $entityName = 'sih_' . $key;

                        $listEntity2 = $entityManager->getRepository($entityName)->findBy(array('package_healthcare_service_id' => $id));

                        if ( ! empty($listEntity2)) {
                            foreach ($listEntity2 as $entity2) {
                                $entityManager->remove($entity2);
                                $entityManager->flush();
                            }
                        }
                    }

                    $entityManager->remove($entity);
                    $entityManager->flush();
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $status = false;
            }
        } else {
            $status = false;
        }

        return $status;
    }

    /**
     * Method to sub item
     *
     * @access public
     *
     * @param mixed $entityManager entityManager
     * @param mixed $newModel
     * @param array $item
     *
     * @return boolean
     */
    public function deleteSubEvent($entityManager, $newModel, $item)
    {
        $tmpFindArray = array();
        foreach ($item as $key => $value) {
            if ($newModel->isItemIsForeignKey($key)) {
                $tmpFindArray[$key] = $value;
            }
        }

        if (isset($item['id']) && $item['id']) {
            $entity = $entityManager->getRepository($newModel->getMainTableName())->findOneBy(array('id' => $item['id']));
        } else {
            $entity = $entityManager->getRepository($newModel->getMainTableName())->findOneBy($tmpFindArray);
        }

        if (empty($entity)) {
            return false;
        }

        $entityManager->remove($entity);
        $entityManager->flush();

        return true;
    }

    /**
     * Method to update multi item.
     *
     * @access public
     *
     * @param string $ids
     *            list of id 1_2_12
     * @param array  $listParam
     *            item
     *
     * @return array
     */
    public function putMultiEvent($ids, $listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $data = array();

        if ( ! empty($ids) && ! empty($listParam)) {
            unset($listParam['id']);
            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $data = array();

                        return $data;
                    } else {
                        $listSubForeignKey = $this->listSubForeignKey;

                        foreach ($listParam as $key => $value) {
                            if (array_key_exists($key, $listSubForeignKey)) {
                                $listTemp = json_decode($value, true);
                                $tableSub = $listSubForeignKey[$key];

                                $this->load->model('list/' . $tableSub);
                                $newModel = new $tableSub;

                                foreach ($listTemp as $lt) {
                                    $subItem = $lt;

                                    if ( ! isset($lt['action']) || empty($lt['action'])) {
                                        // Not have action = insert new record
                                        unset($subItem['id']);
                                        unset($subItem['action']);
                                        $subItem['package_healthcare_service_id'] = $id;

                                        $result2 = $this->postSubEvent($newModel, $subItem, $listReferredColumn,
                                            $listSelfReferredColumn);

                                        if (empty($result2)) {
                                            $entityManager->getConnection()->rollBack();
                                            $data = array();

                                            return $data;
                                        }
                                    } elseif ($lt['action'] == 'update') {

                                        unset($subItem['action']);
                                        $subItem['package_healthcare_service_id'] = $id;

                                        $result2 = $this->putSubEvent($entityManager, $newModel, $subItem,
                                            $listReferredColumn, $listSelfReferredColumn);

                                        // roolBack
                                        if (empty($result2)) {
                                            $entityManager->getConnection()->rollBack();
                                            $data = array();

                                            return $data;
                                        }

                                    } elseif ($lt['action'] == 'delete') {
                                        unset($subItem['action']);
                                        $subItem['package_healthcare_service_id'] = $id;

                                        $result2 = $this->deleteSubEvent($entityManager, $newModel, $subItem);

                                        // roolBack
                                        if ( ! $result2) {
                                            $entityManager->getConnection()->rollBack();
                                            $data = array();

                                            return $data;
                                        }
                                    }
                                }
                            } else {
                                if ($this->isItemIsForeignKey($key)) {
                                    $listForeignKey = $this->getListForeignKey();
                                    $tableName      = $listForeignKey[$key];
                                    $value          = $entityManager->find($tableName, $value);

                                    if (empty($value)) {
                                        $entityManager->getConnection()->rollBack();
                                        $data = array();

                                        return $data;
                                    }
                                }

                                $methodSet = $this->getMethodNameInEntity($key);
                                $entity->$methodSet($value);
                            }
                        }

                        $entityManager->flush();

                        $data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
                    }
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $data = array();
            }
        } else {
            $data = array();
        }

        return $data;
    }

    /**
     * Method to insert item for sub.
     *
     * @access public
     *
     * @param   array $item item
     *
     * @return object An object of data items on success, false on failure.
     */
    protected function postSubEvent($newModel, $item, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $newModel->entityManager;

        $mainEntityClassName = $newModel->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $item['created_at'] = new DateTime("now");

        if ( ! empty($item)) {
            // Check duplicate
            $tmpFindArray = array();
            foreach ($item as $key => $value) {
                if ($newModel->isItemIsForeignKey($key)) {
                    $tmpFindArray[$key] = $value;
                }
            }

            $listEntity2 = $entityManager->getRepository($newModel->getMainTableName())->findBy($tmpFindArray);

            // Check if have duplicate, rollback
            if ( ! empty($listEntity2)) {
                $data = array();

                return $data;
            }

            foreach ($item as $key => $value) {
                if ($newModel->isItemIsForeignKey($key)) {
                    $listForeignKey = $newModel->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $newModel->getMethodNameInEntity($key);
                $entityObject->$methodSet($value);
            }
        }

        $entityManager->persist($entityObject);
        $entityManager->flush();

        $lastInsertId = $entityObject->getId();
        $entity       = $entityManager->find($newModel->getMainTableName(), $lastInsertId);

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }


    /**
     * Method to update multi item.
     *
     * @access public
     *
     * @param string $ids
     *            list of id 1_2_12
     * @param array  $listParam
     *            item
     *
     * @return array
     */
    public function putSubEvent(
        $entityManager,
        $newModel,
        $item,
        $listReferredColumn = array(),
        $listSelfReferredColumn = array()
    ) {
        $data = array();

        // Check duplicate
        $tmpFindArray = array();
        foreach ($item as $key => $value) {
            if ($newModel->isItemIsForeignKey($key)) {
                $tmpFindArray[$key] = $value;
            }
        }

        if (isset($item['id']) && $item['id']) {
            $entity = $entityManager->getRepository($newModel->getMainTableName())->findOneBy(array('id' => $item['id']));
        } else {
            $entity = $entityManager->getRepository($newModel->getMainTableName())->findOneBy($tmpFindArray);
        }

        if (empty($entity)) {
            return $data;
        }

        //var_dump($entity);

        foreach ($item as $key => $value) {
            if ($newModel->isItemIsForeignKey($key)) {
                $listForeignKey = $newModel->getListForeignKey();
                $tableName      = $listForeignKey[$key];
                $value          = $entityManager->find($tableName, $value);

                if (empty($value)) {
                    $entityManager->getConnection()->rollBack();
                    $data = array();

                    return $data;
                }
            }

            $methodSet = $newModel->getMethodNameInEntity($key);
            $entity->$methodSet($value);
        }

        $entityManager->flush();
        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }

    /**
     * get Query for Get List
     *
     * @access    public
     *
     * @param   string $table         table name
     * @param   string $orderBy       The ordering code.
     * @param   string $sortDirection sort direction
     *
     * @return  string  The SQL field(s)
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $columJoin_package_healthcare_service_type_id = new CollumJoin("k", "package_healthcare_service_type_id");
        $OrderClause                                  = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($columJoin_package_healthcare_service_type_id);
        $mainTableQueryObject->addOrderByCondition($OrderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_list_package_healthcare_service_type";
        $joinTableQueryObjectEntityClassName = "sih_list_package_healthcare_service_type";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }

    /**
     * get Code With ID
     *
     * @access    public
     *
     * @param   int $id The unique identifier for the object
     *
     * @return  string  code
     */
    public function getCodeWithID($id)
    {
        $createdAt = date("dmy");
        $newCode   = $createdAt . sprintf("%06d", $id);

        return $newCode;
    }
}
