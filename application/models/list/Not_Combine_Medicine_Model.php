<?php
require_once APPPATH . 'models/Entities/sih_medicine_product.php';
require_once APPPATH . 'models/Entities/sih_list_appointed_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_contraindication_in_medicine.php';
require_once APPPATH . 'models/Entities/sih_list_not_combine_medicine.php';

require_once APPPATH . 'models/Entities/sih_list_contraindication.php';


/**
 * Not Combine Medicine Model
 *
 * @since v.1.0
 */
class Not_Combine_Medicine_Model extends MY_Model
{
    /**
     * Constructor
     *
     * @access    public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->listForeignKey = [
            "origin_medicine_id"      => "sih_medicine_product",
            "not_combine_medicine_id" => "sih_medicine_product"
        ];

        $this->mainTableName       = "sih_list_not_combine_medicine";
        $this->mainEntityClassName = "Sih_list_not_combine_medicine";
    }

    /**
     * Method to insert item.
     *
     * @access public
     *
     * @param array $listParam
     *            item
     *
     * @return object An object of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;

        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $listParam['created_at'] = new DateTime("now");

        // Add temp - to get code - stt = 0
        if (isset($listParam['stat']) && $listParam['stat'] == 0) {

            if ( ! empty($listParam)) {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if ( ! $this->isItemIsForeignKey($key)) {
                        $methodSet = $this->getMethodNameInEntity($key);
                        // Check method
                        if (method_exists($entityObject, $methodSet)) {
                            $entityObject->$methodSet($value);
                        }
                    }
                }
            }
        } elseif ( ! empty($listParam)) {

            // Check Duplicate - Is these foreign key exist in this table
            if (isset($listParam['origin_medicine_id']) && isset($listParam['not_combine_medicine_id'])) {
                $listEntity = $entityManager->getRepository($mainEntityClassName)->findBy(
                    array(
                        'origin_medicine_id'      => $listParam['origin_medicine_id'],
                        'not_combine_medicine_id' => $listParam['not_combine_medicine_id']
                    )
                );

                // The opposite side
                $listEntity2 = $entityManager->getRepository($mainEntityClassName)->findBy(
                    array(
                        'origin_medicine_id'      => $listParam['not_combine_medicine_id'],
                        'not_combine_medicine_id' => $listParam['origin_medicine_id']
                    )
                );

                if ( ! empty($listEntity) || ! empty($listEntity2) || $listParam['origin_medicine_id'] == $listParam['not_combine_medicine_id']) {
                    return null;
                }
            }

            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if ($this->isItemIsForeignKey($key)) {
                        $listForeignKey = $this->getListForeignKey();
                        $tableName      = $listForeignKey[$key];
                        $value          = $entityManager->find($tableName, $value);

                        if (empty($value)) {
                            return null;
                        }
                    }

                    $methodSet = $this->getMethodNameInEntity($key);
                    $entityObject->$methodSet($value);
                }

                $entityManager->persist($entityObject);
                $entityManager->flush();
                $entityManager->clear();

                $lastInsertId = $entityObject->getId();
                $entity       = $entityManager->find($this->getMainTableName(), $lastInsertId);

                $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

                // Opposite
                if ( ! empty($data)) {
                    $tmp = $listParam['origin_medicine_id'];

                    $listParam['origin_medicine_id']      = $listParam['not_combine_medicine_id'];
                    $listParam['not_combine_medicine_id'] = $tmp;

                    foreach ($listParam as $key => $value) {
                        // Is this Id exist in foreign table
                        if ($this->isItemIsForeignKey($key)) {
                            $listForeignKey = $this->getListForeignKey();
                            $tableName      = $listForeignKey[$key];
                            $value          = $entityManager->find($tableName, $value);

                            if (empty($value)) {
                                return null;
                            }
                        }

                        $methodSet = $this->getMethodNameInEntity($key);
                        $entityObject->$methodSet($value);
                    }

                    $entityManager->persist($entityObject);
                    $entityManager->flush();
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $status = false;
            }
        }

        return $data;
    }
}