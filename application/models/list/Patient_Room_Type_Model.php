<?php
require_once APPPATH . 'models/Entities/sih_list_patient_room_type.php';
/**
 * Patient Room Type Model
 *
 * @since v.1.0
 */
class  Patient_Room_Type_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_patient_room_type";
        $this->mainEntityClassName = "Sih_list_patient_room_type";
    }
}