<?php
require_once APPPATH . 'models/Entities/sih_list_package_healthcare_service_type.php';

/**
 * Package Healthcare Service Type Model
 *
 * @since v.1.0
 */
class Package_Healthcare_Service_Type_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [];
        $this->mainTableName = "sih_list_package_healthcare_service_type";
        $this->mainEntityClassName = "Sih_list_package_healthcare_service_type";
    }

}
