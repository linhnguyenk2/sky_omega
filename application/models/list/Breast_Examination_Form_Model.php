<?php
require_once APPPATH . 'models/Entities/sih_breast_examination_form.php';
require_once APPPATH . 'models/Entities/sih_patients.php';
require_once APPPATH . 'models/Entities/sih_user.php';
require_once APPPATH . 'models/Entities/sih_patient_emergency_contact.php';


require_once APPPATH . 'models/Entities/sih_list_nations.php';
require_once APPPATH . 'models/Entities/sih_list_nations_foreigners.php';
require_once APPPATH . 'models/Entities/sih_list_provinces.php';
require_once APPPATH . 'models/Entities/sih_list_districts.php';
require_once APPPATH . 'models/Entities/sih_list_wards.php';
require_once APPPATH . 'models/Entities/sih_list_titles.php';
require_once APPPATH . 'models/Entities/sih_list_folks.php';

require_once APPPATH . 'models/Entities/sih_family_planing_book.php';
require_once APPPATH . 'models/Entities/sih_family_plan_birth_control_form.php';
require_once APPPATH . 'models/Entities/sih_artificial_examination_form.php';
require_once APPPATH . 'models/Entities/sih_medical_record.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_form.php';
require_once APPPATH . 'models/Entities/sih_gynecolog_book.php';
require_once APPPATH . 'models/Entities/sih_prenatal_form.php';
require_once APPPATH . 'models/Entities/sih_prenatal_book.php';
require_once APPPATH . 'models/Entities/sih_menopause_form.php';
require_once APPPATH . 'models/Entities/sih_menopause_book.php';
require_once APPPATH . 'models/Entities/sih_analysis_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_health_book.php';
require_once APPPATH . 'models/Entities/sih_medical_examination_form_in_health_book.php';
require_once APPPATH . 'models/Entities/sih_pregnancy_changes_in_health_book.php';
/**
 *  Breast Examination Form Model
 *
 * @since v.1.0
 */
class Breast_Examination_Form_Model extends MY_Model
{
	/**
	 * Constructor
	 *
	 * @access    public
	 *
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->listForeignKey = [];
		$this->mainTableName = "sih_breast_examination_form";
		$this->mainEntityClassName = "Sih_breast_examination_form";
	}


    /**
     * Method to insert item for sub.
     *
     * @access public
     *
     * @param   array $item item
     *
     * @return object An object of data items on success, false on failure.
     */
    protected function postSubEvent($newModel, $item, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $newModel->entityManager;

        $mainEntityClassName = $newModel->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $item['created_at'] = new DateTime("now");

        if ( ! empty($item)) {
            // Check duplicate
            $tmpFindArray                      = array();
            $tmpFindArray['breast_examination_form_id'] = $item['breast_examination_form_id'];

            // type and breast_examination_book_id
            $listEntity2 = $entityManager->getRepository($newModel->getMainTableName())->findBy($tmpFindArray);

            // Check if have duplicate, rollback
            if ( ! empty($listEntity2)) {
                $data = array();

                return $data;
            }

            foreach ($item as $key => $value) {
                if ($newModel->isItemIsForeignKey($key)) {
                    $listForeignKey = $newModel->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $newModel->getMethodNameInEntity($key);

                // Check method
                if (method_exists($entityObject, $methodSet)) {
                    $entityObject->$methodSet($value);
                }
            }
        }

        $entityManager->persist($entityObject);
        $entityManager->flush();

        $lastInsertId = $entityObject->getId();
        $entity       = $entityManager->find($newModel->getMainTableName(), $lastInsertId);

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }

    /**
     * Method to insert item. - insert in two table :
     * 1/sih_breast_examination_form and
     * 2/sih_medical_examination_form
     * breast_examination_form_id get this value after insert in sih_breast_examination_form
     * medical_record_id
     * staff_id
     * patient_id
     * add more basic information
     *
     *
     * @access public
     *
     * @param array $listParam
     *            item
     *
     * @return object An objectMedical_examination_form of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;
        $entityManager->getConnection()->beginTransaction();
        $entityManager->getConnection()->setAutoCommit(false);

        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $listParam['created_at'] = new DateTime("now");

        if (isset($listParam['check_in_time'])) {
            $listParam['check_in_time'] = new DateTime($listParam['check_in_time']);
        } else {
            $listParam['check_in_time'] = new DateTime("now");
        }

        if (isset($listParam['re_examination_date'])) {
            $listParam['re_examination_date'] = new DateTime($listParam['re_examination_date']);
        } else {
            $listParam['re_examination_date'] = new DateTime("now");
        }
        // Khám nhủ - breast_examination_form, type = 3
        $listParam['type'] = 3;

        // Add temp - to get code - stt = 0
        if (isset($listParam['stat']) && $listParam['stat'] == 0) {

            if ( ! empty($listParam)) {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if (!$this->isItemIsForeignKey($key)) {
                        $methodSet = $this->getMethodNameInEntity($key);
                        // Check method
                        if (method_exists($entityObject, $methodSet)) {
                            $entityObject->$methodSet($value);
                        }
                    }
                }
            }
        }
        elseif ( ! empty($listParam)) {

            foreach ($listParam as $key => $value) {
                // Is this Id exist in foreign table
                if ($this->isItemIsForeignKey($key)) {
                    $listForeignKey = $this->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $this->getMethodNameInEntity($key);
                // Check method
                if (method_exists($entityObject, $methodSet)) {
                    $entityObject->$methodSet($value);
                }
            }
        }

        // Rollback
        try {
            $entityManager->persist($entityObject);
            $entityManager->flush();

            $lastInsertId = $entityObject->getId();

            // Get last insert ID
            $entity = $entityManager->find($this->getMainTableName(), $lastInsertId);

            if ($entityObject->isHaveCodeField()) {
                $newCode = $this->getCodeWithID($lastInsertId);
                $entity->setCode($newCode);
                $entityManager->flush();

                $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
            }

            // Insert in sih_medical_examination_form
            $this->load->model('list/Medical_Examination_Form_Model');

            $subItem                      = $listParam;
            $subItem['breast_examination_form_id'] = $lastInsertId;

            if (isset($newCode)) {
                $subItem['code'] = $newCode;
            }

            $newModel = new Medical_Examination_Form_Model;

            $result2 = $this->postSubEvent($newModel, $subItem, $listReferredColumn, $listSelfReferredColumn);

            if (empty($result2)) {
                $entityManager->getConnection()->rollBack();
                $data = array();

                return $data;
            }

            $entityManager->getConnection()->commit();
            $entityManager->close();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollBack();
            $data = array();
        }

        // return objectMedicalExaminationForm
        return $result2;
    }


    /**
     * Method to update multi item.
     * 1/sih_breast_examination_form and
     * 2/sih_medical_examination_form
     * @access public
     *
     * @param string $ids
     *            list of id 1_2_12
     * @param array  $listParam
     *            item
     *
     * @return array
     */
    public function putMultiEvent($ids, $listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $data = array();
        // Update in sih_medical_examination_form
        $this->load->model('list/Medical_Examination_Form_Model');
        $newModel = new Medical_Examination_Form_Model;

        if (isset($listParam['re_examination_date'])) {
            $listParam['re_examination_date'] = new DateTime($listParam['re_examination_date']);
        } else {
            $listParam['re_examination_date'] = new DateTime("now");
        }

        if ( ! empty($ids) && ! empty($listParam)) {
            unset($listParam['id']);

            $entityManager = $newModel->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    // find in sih_medical_examination_form
                    $entity = $entityManager->getRepository($newModel->getMainTableName())->findOneBy(array('breast_examination_form_id' => $id));
                    $entity2 = $entityManager->find($this->getMainTableName(), $id);

                    if (empty($entity) || empty($entity2)) {
                        $entityManager->getConnection()->rollBack();
                        $data = array();

                        return $data;
                    } else {
                        foreach ($listParam as $key => $value) {
                            if ($newModel->isItemIsForeignKey($key)) {
                                $listForeignKey = $newModel->getListForeignKey();
                                $tableName      = $listForeignKey[$key];
                                $value          = $entityManager->find($tableName, $value);

                                if (empty($value)) {
                                    $entityManager->getConnection()->rollBack();
                                    $data = array();

                                    return $data;
                                }
                            }

                            // Update sih_medical_examination_form
                            $methodSet = $newModel->getMethodNameInEntity($key);

                            if (method_exists($entity, $methodSet)) {
                                $entity->$methodSet($value);
                            }

                            // Update sih_breast_examination_form
                            $methodSet2 = $this->getMethodNameInEntity($key);

                            if (method_exists($entity2, $methodSet2)) {
                                $entity2->$methodSet2($value);
                            }
                        }

                        $entityManager->flush();

                        $data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
                    }
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $data = array();
            }
        } else {
            $data = array();
        }

        return $data;
    }

    /**
     * Method to delete multi item. and five table
     * 1/sih_breast_examination_form and
     * 2/sih_medical_examination_form
     *
     * @access public
     *
     * @param array $ids
     *            list of id [1,2,3]
     *
     * @return boolean
     */
    public function deleteMultiEvent($ids)
    {
        $status = true;

        if ( ! empty($ids)) {
            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    // Return 11000 not found this item
                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $status = false;
                        break;
                    }

                    // Find another foreign table and delete - sih_medical_examination_form
                    $listSubForeignKey = $this->listSubForeignKey;

                    foreach ($listSubForeignKey as $entityName => $subModelClassName) {
                        if ($entityName == 'sih_medical_examination_form') {
                            // Find not combine medicine first
                            $this->load->model('list/' . $subModelClassName);
                            $listEntity2 = $entityManager->getRepository($entityName)->findBy(array('breast_examination_form_id' => $id));

                            if ( ! empty($listEntity2)) {
                                foreach ($listEntity2 as $entity2) {
                                    $entityManager->remove($entity2);
                                    $entityManager->flush();
                                }
                            }
                        }
                    }

                    $entityManager->remove($entity);
                    $entityManager->flush();
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $status = false;
            }
        } else {
            $status = false;
        }

        return $status;
    }

    /**
     * Method to get an item
     *
     * @access public
     *
     * @param string $id
     *            primary key
     *
     * @return object An object of data items on success, false on failure.
     */
    public function get($id, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $this->load->model('list/Medical_Examination_Form_Model');
        $newModel = new Medical_Examination_Form_Model;

        $entityManager = $newModel->entityManager;
        $entity        = $entityManager->getRepository($newModel->getMainTableName())->findOneBy(array('breast_examination_form_id' => $id));

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }

    /**
     * get Query for Get List call from sih_medical_examination_form
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject api GetMethodParamObject
     * @param boolean $countMode               countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);

        $column_join_patient_id        = new CollumJoin("k", "patient_id");
        $column_join_medical_record_id = new CollumJoin("km", "medical_record_id");
        $column_join_breast_examination_form_id = new CollumJoin("kg", "breast_examination_form_id");

        $orderClause = new Order($apiGetMethodParamObject->sortBy,
            $apiGetMethodParamObject->sortDirection);


        $this->load->model('list/Medical_Examination_Form_Model');
        $newModel = new Medical_Examination_Form_Model;
        $mainTableQueryObjectTableName       = $newModel->mainTableName;
        $mainTableQueryObjectEntityClassName = $newModel->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = "h";
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($column_join_patient_id);
        $mainTableQueryObject->addCollumJoin($column_join_medical_record_id);
        $mainTableQueryObject->addCollumJoin($column_join_breast_examination_form_id);
        $mainTableQueryObject->addOrderByCondition($orderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_patients";
        $joinTableQueryObjectEntityClassName = "sih_patients";
        $joinTableQueryObjectTableAlias      = "k";
        $joinTableQueryObject                = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_medical_record";
        $joinTableQueryObjectEntityClassName = "sih_medical_record";
        $joinTableQueryObjectTableAlias      = "km";
        $joinTableQueryObject2               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject2->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject2->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

        $joinTableQueryObjectTable           = "sih_breast_examination_form";
        $joinTableQueryObjectEntityClassName = "sih_breast_examination_form";
        $joinTableQueryObjectTableAlias      = "kg";
        $joinTableQueryObject3               = new TableQueryObject($joinTableQueryObjectTable,
            $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject3->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject3->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);


        $listJoinTableQueryObject   = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        $listJoinTableQueryObject[] = $joinTableQueryObject2;
        $listJoinTableQueryObject[] = $joinTableQueryObject3;

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }
}