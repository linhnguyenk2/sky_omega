<?php
require_once APPPATH . 'models/Entities/sih_list_health_insurance_card.php';
require_once APPPATH . 'models/Entities/sih_list_insurance_company.php';

/**
 * Health Insurance Card Relationship Model
 *
 * @since v.1.0
 */
class Health_Insurance_Card_Model extends MY_Model
{

    /**
     * Constructor
     *
     * @access public
     *        
     *        
     */
    public function __construct()
    {
        parent::__construct();
        $this->listForeignKey = [
            "insurance_company_id" => "sih_list_insurance_company"
        ];
        $this->mainTableName = "sih_list_health_insurance_card";
        $this->mainEntityClassName = "Sih_list_health_insurance_card";
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object $apiGetMethodParamObject   api GetMethodParamObject
     * @param boolean $countMode countMode
     *
     * @return string DQL
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = FALSE)
    {
        $queryBuilder = $this->getQueryBuilder($countMode);
        
        $collumJoin_insurance_company_id = new CollumJoin("k", "insurance_company_id");
        $OrderClause = new Order($apiGetMethodParamObject->sortBy, $apiGetMethodParamObject->sortDirection);
        
        $mainTableQueryObjectTableName = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias = "h";
        $mainTableQueryObject = new TableQueryObject($mainTableQueryObjectTableName, $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addCollumJoin($collumJoin_insurance_company_id);
        $mainTableQueryObject->addOrderByCondition($OrderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        
        $joinTableQueryObjectTable = "sih_list_insurance_company";
        $joinTableQueryObjectEntityClassName = "sih_list_insurance_company";
        $joinTableQueryObjectTableAlias = "k";
        $joinTableQueryObject = new TableQueryObject($joinTableQueryObjectTable, $joinTableQueryObjectTableAlias, $joinTableQueryObjectEntityClassName, false);
        $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
        $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);
        
        $listJoinTableQueryObject = [];
        $listJoinTableQueryObject[] = $joinTableQueryObject;
        
        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject, $apiGetMethodParamObject->keyword);
        return $DQL;
    }
}
