<?php
class Report_model extends CI_Model {
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();        
        $this->load->helper(array('text', 'url', 'date'));
        $this->load->library('session');
    }
}
?>
