<?php
/**
 * create by @Chau
 * global model 
 * access: view, controller
 * goal: everyone can get data need
 * vision: jsut function is public not show data
 */

class Global_model extends CI_Model {

    public function __construct() {
        // Call the CI_model constructor
        parent::__construct();
        $this->load->helper(array('text', 'url', 'date'));
        $this->load->library('session');
        $this->load->library('email');

        //check login
    }

    public function getListScreenWithGroupRoleId($roleId) {
        
        $this->db->select(
            'sih_screens.id,
            sih_screens.route,
            sih_screens.routed_id,
            sih_screens.title,
            sih_screens.ico'
        );
        $this->db->from('sih_screens');
        $this->db->join('sih_role_screens', 'sih_screens.id = sih_role_screens.id_screens');
        $this->db->where('sih_role_screens.id_role=', $roleId);
        $this->db->where('sih_screens.stat=', '1');
        $this->db->where('sih_role_screens.stat=', '1');
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }
    
    
    public function getListScreenWithGroupRoleIdAndRouteName($routeName,$roleId) {
        $this->db->from('sih_screens');
        $this->db->join('sih_role_screens', 'sih_screens.routed_id = sih_role_screens.id_screens');
        $this->db->like('sih_screens.routed_id','_');
        $this->db->like('sih_screens.route',$routeName."/");
        $this->db->where('sih_role_screens.id_role',$roleId);
        $this->db->where('sih_screens.stat=', '1');
        $this->db->where('sih_role_screens.stat=', '1');
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }
}