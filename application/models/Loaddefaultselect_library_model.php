<?php

class Loaddefaultselect_library_model extends CI_Model {

    protected $glblist=[];
    public function __construct() {
        // Call the CI_model constructor
        parent::__construct();
        $this->load->helper(array('text', 'url', 'date','util'));
        $this->load->library('session');
        // $this->load->library('email');

    }
    public function get_select_option($name_class){
        //$func = url_title($name_class, '_',TRUE);
        $func = str_replace('-','_',$name_class);
        
        return $this->$func();

        if (function_exists($this->func)) {
            return $this->$func();
        }else{
            //echo 'Loaddefaultselect_library_model have function not correct ';die;
            return null;
        }
    }

    private function select_status(){
        //lang default =vn
        //return'<option value="1">Hiện thị</option><option value="0">Ẩn</option>';
        $list=[];
        $list['1']='Hiển thị';
        $list['0']='Ẩn';
        return $list;
    }
    private function select_nations(){
        $this->db->select('id,name');
        $this->db->where('stat', '1');
        $query = $this->db->get('sih_list_nations');
        $data =  $query->result_array();
        $list=[];
        foreach ($data as $key => $value) {
            $list[$value['id']]=$value['name'];
        }
        return $list;
    }
    private function select_departments(){
        $this->db->select('id,name');
        $this->db->where('stat', '1');
        $query = $this->db->get('sih_list_departments');
        $data =  $query->result_array();
        $list=[];
        foreach ($data as $key => $value) {
            $list[$value['id']]=$value['name'];
        }
        return $list;
    }
    private function select_provinces(){
        $this->db->select('sih_list_provinces.id,sih_list_provinces.name');
        $this->db->from('sih_list_provinces');
        $this->db->join('sih_list_nations', 'sih_list_provinces.id_nation = sih_list_nations.id');
        $this->db->like('sih_list_nations.name', 'Viet nam','both'); //default get country vietnam for show
        $this->db->where('sih_list_provinces.stat', 1);
        $query = $this->db->get();
        $data =  $query->result_array();
        $list=[];
        foreach ($data as $key => $value) {
            $list[$value['id']]=$value['name'];
        }
        return $list;
    }
    private function selectprovincesnations(){
        $list=[];
        $list['0']='Viet Nam';
        return $list;
        //
        
        $this->db->select('sih_list_provinces.id,sih_list_nations.name');
        $this->db->from('sih_list_provinces');
        $this->db->join('sih_list_nations', 'sih_list_provinces.id_nation = sih_list_nations.id');
        $this->db->where('sih_list_provinces.stat', '1');
        $query = $this->db->get();
        $data =  $query->result_array();
        $list=[];
        foreach ($data as $key => $value) {
            $list[$value['id']]=$value['name'];
        }
        return $list;
    }
    
    private function select_healthcare_group(){
        $this->db->select('id,name');
        $this->db->where('stat', '1');
        $query = $this->db->get('sih_list_healthcare_group');
        $data =  $query->result_array();
        $list=[];
        foreach ($data as $key => $value) {
            $list[$value['id']]=$value['name'];
        }
        return $list;
    }
    private function select_package_healthcare_service_type(){
        $this->db->select('id,name');
        $this->db->where('stat', '1');
        $query = $this->db->get('sih_list_package_healthcare_service_type');
        $data =  $query->result_array();
        $list=[];
        foreach ($data as $key => $value) {
            $list[$value['id']]=$value['name'];
        }
        return $list;
    }
    
    //function filter by type and reruurn default
    private function select_none_healthcare_group(){
        $this->db->select('id,name');
        $this->db->where('stat', '1');
        $query = $this->db->get('sih_list_healthcare_group');
        $data =  $query->result_array();
        $list=[];
        foreach ($data as $key => $value) {
            $list[$value['id']]=$value['name'];
        }
        $list['compare']=$this->select_healthcare_type_group();
        return $list;
    }

    private function select_healthcare_type_group(){
        $this->db->select('sih_list_healthcare_type.id,sih_list_healthcare_group.name');
        $this->db->from('sih_list_healthcare_group');
        $this->db->join('sih_list_healthcare_type', 'sih_list_healthcare_type.healthcare_group_id = sih_list_healthcare_group.id');
        $this->db->where('sih_list_healthcare_group.stat', '1');
        $this->db->where('sih_list_healthcare_type.stat', '1');

        // $this->db->select('id,name');
        // $this->db->where('stat', '1');
        // $query = $this->db->get('sih_list_healthcare_group');
        $query = $this->db->get();
        $data =  $query->result_array();
        $list=[];
        foreach ($data as $key => $value) {
            $list[$value['id']]=$value['name'];
        }
        return $list;
    }
    
    private function select_healthcare_type(){
        // $this->db->select('id,name');
        // $this->db->where('stat', '1');
        // $query = $this->db->get('sih_list_healthcare_type');
        // $data =  $query->result_array();
        // $list=[];
        $this->db->select('sih_list_healthcare_type.id as id,sih_list_healthcare_type.name, sih_list_healthcare_group.id as id1');
        $this->db->from('sih_list_healthcare_type');
        $this->db->join('sih_list_healthcare_group', 'sih_list_healthcare_type.healthcare_group_id = sih_list_healthcare_group.id');
        $this->db->where('sih_list_healthcare_group.stat', '1');
        $this->db->where('sih_list_healthcare_type.stat', '1');
        $query = $this->db->get();
        $data =  $query->result_array();
        $list=[];
        //var_dump($data);die;
        foreach ($data as $key => $value) {
            $list[$value['id']][]=$value['name'];
            $list[$value['id']][]=$value['id1'];
        }
        //var_dump($list);die;
        return $list;
    }
    
    private function select_service_type(){
        $this->db->select('id,name');
        $this->db->where('stat', '1');
        $query = $this->db->get('sih_service_type');
        $data =  $query->result_array();
        $list=[];
        foreach ($data as $key => $value) {
            $list[$value['id']]=$value['name'];
        }
        return $list;
    }
}