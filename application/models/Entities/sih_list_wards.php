<?php
include_once 'BaseEntity.php';
// Entities/sih_list_wards.php

/**
 * @Entity @Table(name="sih_list_wards")
 **/
class Sih_list_wards extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $name;

	/** @Column(type="string", nullable=true) * */
	protected $code;

	/**
	 * @ManyToOne(targetEntity="Sih_list_districts")
	 * @JoinColumn(name="id_district", referencedColumnName="id", onDelete="NO ACTION")
	 */
	
	protected $id_district;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"comment":"filter when show","default":0}) * */
	protected $orders = 0;

	function getId()
	{
		return $this->id;
	}

	function getName()
	{
		return $this->name;
	}

	function getCode()
	{
		return $this->code;
	}

	function getId_district()
	{
		return $this->id_district;
	}


	function getCreated_at()
	{
		return $this->created_at;
	}

	function getStat()
	{
		return $this->stat;
	}

	function getOrders()
	{
		return $this->orders;
	}

	function setName($name)
	{
		$this->name = $name;
	}

	function setCode($code)
	{
		$this->code = $code;
	}

	function setId_district($id_district)
	{
		$this->id_district = $id_district;
	}

	function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	function setStat($stat)
	{
		$this->stat = $stat;
	}

	function setOrders($orders)
	{
		$this->orders = $orders;
	}


}