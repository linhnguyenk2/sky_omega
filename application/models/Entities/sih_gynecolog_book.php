<?php
include_once 'BaseEntity.php';
// Entities/sih_gynecolog_book.php

/**
 * @Entity @Table(name="sih_gynecolog_book")
 **/
class Sih_gynecolog_book extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $code;

    /** @Column(type="string", nullable=true) * */
    protected $age_have_menstrual;

    /** @Column(type="string", nullable=true) * */
    protected $period;

    /** @Column(type="string", nullable=true) * */
    protected $menstrual_status;

    /** @Column(type="string", nullable=true) * */
    protected $day_of_menstrual;

    /** @Column(type="string", nullable=true) * */
    protected $menstrual_amount;

    /** @Column(type="string", nullable=true) * */
    protected $married_age;

    /** @Column(type="string", nullable=true) * */
    protected $number_of_pregnancies;

    /** @Column(type="string", nullable=true) * */
    protected $parity;

    /** @Column(type="string", nullable=true) * */
    protected $family_history;

    /**
	 * @ManyToOne(targetEntity="sih_patients")
	 * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $patient_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

	public function getCode()
	{
		return $this->code;
	}

    public function getAge_have_menstrual()
    {
        return $this->age_have_menstrual;
    }

    public function getPeriod()
    {
        return $this->period;
    }

    public function getMenstrual_status()
    {
        return $this->menstrual_status;
    }

    public function getDay_of_menstrual()
    {
        return $this->day_of_menstrual;
    }

    public function getMenstrual_amount()
    {
        return $this->menstrual_amount;
    }

    public function getMarried_age()
    {
        return $this->married_age;
    }

    public function getNumber_of_pregnancies()
    {
        return $this->number_of_pregnancies;
    }

    public function getParity()
    {
        return $this->parity;
    }

    public function getFamily_history()
    {
        return $this->family_history;
    }

    public function getPatient_id()
    {
        return $this->patient_id;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

	public function setCode($code)
	{
		$this->code = $code;
	}

    public function setAge_have_menstrual($age_have_menstrual)
    {
        $this->age_have_menstrual = $age_have_menstrual;
    }

    public function setPeriod($period)
    {
        $this->period = $period;
    }

    public function setMenstrual_status($menstrual_status)
    {
        $this->menstrual_status = $menstrual_status;
    }

    public function setDay_of_menstrual($day_of_menstrual)
    {
        $this->day_of_menstrual = $day_of_menstrual;
    }

    public function setMenstrual_amount($menstrual_amount)
    {
        $this->menstrual_amount = $menstrual_amount;
    }

    public function setMarried_age($married_age)
    {
        $this->married_age = $married_age;
    }

    public function setNumber_of_pregnancies($number_of_pregnancies)
    {
        $this->number_of_pregnancies = $number_of_pregnancies;
    }

    public function setParity($parity)
    {
        $this->parity = $parity;
    }

    public function setFamily_history($family_history)
    {
        $this->family_history = $family_history;
    }

    public function setPatient_id($patient_id)
    {
        $this->patient_id = $patient_id;
    }

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}
}

