<?php
include_once 'BaseEntity.php';
// Entities/sih_service_location.php
/**
 * @Entity @Table(name="sih_service_location")
 **/
class Sih_service_location extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

	/** @Column(type="string", nullable=true) **/
    protected $code;

	/** @Column(type="string", nullable=false) **/
    protected $name;

    /**
     * @ManyToOne(targetEntity="sih_list_departments")
     * @JoinColumn(name="department_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $department_id;

	/** @Column(type="string", nullable=true) **/
    protected $location;

	/** @Column(type="string", nullable=true) **/
    protected $phone_number;

	/** @Column(type="string", nullable=true) **/
    protected $note;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":0}, nullable=true) * */
    protected $orders = 0;

    /** @Column(type="integer", options={"default":1}) **/
    protected $stat = 1;

    public function getId() {
        return $this->id;
    }

    public function getCode() {
        return $this->code;
    }

    public function getName() {
        return $this->name;
    }

    public function getLocation() {
        return $this->location;
    }

    public function getPhone_number() {
        return $this->phone_number;
    }

    public function getNote() {
        return $this->note;
    }

    public function getDepartment_id() {
        return $this->department_id;
    }

    public function getCreated_at() {
        return $this->created_at;
    }

    public function getStat() {
        return $this->stat;
    }

    public function getOrders() {
        return $this->orders;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setLocation($location) {
        $this->location = $location;
    }

    public function setPhone_number($phone_number) {
        $this->phone_number = $phone_number;
    }

    public function setNote($note) {
        $this->note = $note;
    }

    public function setDepartment_id($department_id) {
        $this->department_id = $department_id;
    }

    public function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    public function setStat($stat) {
        $this->stat = $stat;
    }

    public function setOrders($orders) {
        $this->orders = $orders;
    }
}
