<?php
include_once 'BaseEntity.php';
// Entities/sih_list_currencies.php

/**
 * @Entity @Table(name="sih_list_currencies")
 **/
class Sih_list_currencies extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code_from;

    /** @Column(type="string", nullable=true) * */
    protected $code_to;

    /** @Column(type="string", nullable=true) * */
    protected $rate;

    /** @Column(type="datetime") * */
    protected $created_at;

    /** @Column(type="datetime" , nullable=true) * */
    protected $begin_date;

    /** @Column(type="integer", options={"default":0}, nullable=true) * */
    protected $orders = 0;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function getCode_from()
    {
        return $this->code_from;
    }

    public function getCode_to()
    {
        return $this->code_to;
    }

    public function getRate()
    {
        return $this->rate;
    }

    public function getBegin_date()
    {
        return $this->begin_date;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function getOrders()
    {
        return $this->orders;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCode_from($code_from)
    {
        $this->code_from = $code_from;
    }

    public function setCode_to($code_to)
    {
        $this->code_to = $code_to;
    }

    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    public function setBegin_date($begin_date)
    {
        $this->begin_date = $begin_date;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }

    public function setOrders($orders)
    {
        $this->orders = $orders;
    }
}
