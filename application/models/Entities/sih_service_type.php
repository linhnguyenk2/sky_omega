<?php
include_once 'BaseEntity.php';
// Entities/sih_service_type.php

/**
 * @Entity @Table(name="sih_service_type")
 **/
class Sih_service_type extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $code;

	/** @Column(type="string", nullable=false) * */
	protected $name;

	/**
	 * @ManyToOne(targetEntity="sih_service_group")
	 * @JoinColumn(name="service_group_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $service_group_id;

	//** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"default":0}, nullable=true) * */
	protected $orders = 0;

	public function getId()
	{
		return $this->id;
	}

	public function getCode()
	{
		return $this->code;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getService_group_id()
	{
		return $this->service_group_id;
	}

	public function getStat()
	{
		return $this->stat;
	}

	public function getOrders()
	{
		return $this->orders;
	}

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setCode($code)
	{
		$this->code = $code;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setService_group_id($service_group_id)
	{
		$this->service_group_id = $service_group_id;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}

	public function setOrders($orders)
	{
		$this->orders = $orders;
	}

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}
}
