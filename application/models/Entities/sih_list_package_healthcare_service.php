<?php
include_once 'BaseEntity.php';
// Entities/sih_list_package_healthcare_service.php
/**
 * @Entity @Table(name="sih_list_package_healthcare_service")
 **/
class Sih_list_package_healthcare_service extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    
	/** @Column(type="string", nullable=true) **/
    protected $code;
	
	/** @Column(type="string", nullable=true) **/
    protected $name;

	/**
	 * @ManyToOne(targetEntity="sih_list_package_healthcare_service_type")
	 * @JoinColumn(name="package_healthcare_service_type_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $package_healthcare_service_type_id;
	
	/** @Column(type="string", nullable=true) **/
    protected $price;
	
	/** @Column(type="string", nullable=true) **/
    protected $price_foreigner;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"default":0}, nullable=true) * */
	protected $orders = 0;

	public function getId() {
		return $this->id;
	}

	public function getCode() {
		return $this->code;
	}

	public function getName() {
		return $this->name;
	}

	public function getPackage_healthcare_service_type_id() {
		return $this->package_healthcare_service_type_id;
	}

	public function getPrice() {
		return $this->price;
	}

	public function getPrice_foreigner() {
		return $this->price_foreigner;
	}

	public function getCreated_at() {
		return $this->created_at;
	}

	public function getStat() {
		return $this->stat;
	}

	public function getOrders() {
		return $this->orders;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setCode($code) {
		$this->code = $code;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setPrice_foreigner($price_foreigner) {
		$this->price_foreigner = $price_foreigner;
	}

	public function setPrice($price) {
		$this->price = $price;
	}

	public function setPackage_healthcare_service_type_id($package_healthcare_service_type_id) {
		$this->package_healthcare_service_type_id = $package_healthcare_service_type_id;
	}

	public function setCreated_at($created_at) {
		$this->created_at = $created_at;
	}

	public function setStat($stat) {
		$this->stat = $stat;
	}

	public function setOrders($orders) {
		$this->orders = $orders;
	}
}