<?php
include_once 'BaseEntity.php';
// Entities/sih_location_in_service.php
/**
 * @Entity @Table(name="sih_location_in_service")
 **/
class Sih_location_in_service extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /**
     * @ManyToOne(targetEntity="sih_service")
     * @JoinColumn(name="service_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $service_id;

    /**
     * @ManyToOne(targetEntity="sih_service_location")
     * @JoinColumn(name="location_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $location_id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse")
     * @JoinColumn(name="warehouse_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_id;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":0}, nullable=true) * */
    protected $orders = 0;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function getService_id()
    {
        return $this->service_id;
    }

    public function getLocation_id()
    {
        return $this->location_id;
    }

    public function getWarehouse_id()
    {
        return $this->warehouse_id;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function getOrders()
    {
        return $this->orders;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setService_id($service_id)
    {
        $this->service_id = $service_id;
    }

    public function setLocation_id($location_id)
    {
        $this->location_id = $location_id;
    }

    public function setWarehouse_id($warehouse_id)
    {
        $this->warehouse_id = $warehouse_id;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }

    public function setOrders($orders)
    {
        $this->orders = $orders;
    }
}