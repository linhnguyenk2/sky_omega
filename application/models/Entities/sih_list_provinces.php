<?php
include_once 'BaseEntity.php';
// Entities/sih_list_provinces.php
/**
 * @Entity @Table(name="sih_list_provinces")
 **/
class Sih_list_provinces extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
	
	/** @Column(type="string", nullable=true) **/
    protected $code;
	
	/** @Column(type="string", nullable=true) **/
    protected $name;
    
    /**
     * @ManyToOne(targetEntity="sih_list_nations")
     * @JoinColumn(name="id_nation", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $id_nation;
	
	/** @Column(type="datetime") **/
    protected $created_at;

    /** @Column(type="integer", options={"default":1}, nullable=true) * */
    protected $stat = 1;

    /** @Column(type="integer", options={"default":0}, nullable=true) * */
    protected $orders = 0;

    function getId() {
        return $this->id;
    }

    function getCode() {
        return $this->code;
    }

    function getName() {
        return $this->name;
    }

    function getId_nation() {
        return $this->id_nation;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function getStat() {
        return $this->stat;
    }

    function getOrders() {
        return $this->orders;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCode($code) {
        $this->code = $code;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setId_nation($id_nation) {
        $this->id_nation = $id_nation;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function setStat($stat) {
        $this->stat = $stat;
    }

    function setOrders($orders) {
        $this->orders = $orders;
    }


}