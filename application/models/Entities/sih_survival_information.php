<?php
include_once 'BaseEntity.php';
// Entities/sih_survival_information.php
/**
 * @Entity @Table(name="sih_survival_information")
 * */
class Sih_survival_information  extends BaseEntity {

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $pulse;

    /** @Column(type="string", nullable=true) * */
    protected $body_temperature;

    /** @Column(type="string", nullable=true) * */
    protected $blood_pressure;

    /** @Column(type="string", nullable=true) * */
    protected $height;

    /** @Column(type="string", nullable=true) * */
    protected $weight;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function getPulse()
    {
        return $this->pulse;
    }

    public function getBody_temperature()
    {
        return $this->body_temperature;
    }

    public function getBlood_pressure()
    {
        return $this->blood_pressure;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setPulse($pulse)
    {
        $this->pulse = $pulse;
    }

    public function setBody_temperature($body_temperature)
    {
        $this->body_temperature = $body_temperature;
    }

    public function setBlood_pressure($blood_pressure)
    {
        $this->blood_pressure = $blood_pressure;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }
}

