<?php
include_once 'BaseEntity.php';
// Entities/sih_medicine_in_prescription_paper.php

/**
 * @Entity @Table(name="sih_medicine_in_prescription_paper")
 **/
class Sih_medicine_in_prescription_paper extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $quantity;

    /** @Column(type="string", nullable=true) * */
    protected $usages;

    /**
     * @ManyToOne(targetEntity="sih_prescription_paper")
     * @JoinColumn(name="prescription_paper_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $prescription_paper_id;

    /**
     * @ManyToOne(targetEntity="sih_product")
     * @JoinColumn(name="product_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $product_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getUsages()
    {
        return $this->usages;
    }

	public function getPrescription_paper_id()
	{
		return $this->prescription_paper_id;
	}

    public function getProduct_id()
    {
        return $this->product_id;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setUsages($usages)
    {
        $this->usages = $usages;
    }

    public function setPrescription_paper_id($prescription_paper_id)
    {
        $this->prescription_paper_id = $prescription_paper_id;
    }

    public function setProduct_id($product_id)
    {
        $this->product_id = $product_id;
    }

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
