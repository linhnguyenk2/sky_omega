<?php
include_once 'BaseEntity.php';
// Entities/sih_warehouse_product_in_package.php

/**
 * @Entity @Table(name="sih_warehouse_product_in_package")
 **/
class Sih_warehouse_product_in_package extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_product")
     * @JoinColumn(name="product_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $product_id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse_import_form")
     * @JoinColumn(name="warehouse_package_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_package_id;

    /** @Column(type="string", nullable=true) * */
    protected $quantity;

    /** @Column(type="string", nullable=true) * */
    protected $used_quantity;

    /** @Column(type="datetime", nullable=true) * */
    protected $expiration_date;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getCode()
    {
        return $this->code;
    }

    public function getProduct_id()
    {
        return $this->product_id;
    }

    public function getWarehouse_package_id()
    {
        return $this->warehouse_package_id;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getUsed_quantity()
    {
        return $this->used_quantity;
    }

    public function getExpiration_date()
    {
        return $this->expiration_date;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setProduct_id($product_id)
    {
        $this->product_id = $product_id;
    }

    public function setWarehouse_package_id($warehouse_package_id)
    {
        $this->warehouse_package_id = $warehouse_package_id;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setUsed_quantity($used_quantity)
    {
        $this->used_quantity = $used_quantity;
    }

    public function setExpiration_date($expiration_date)
    {
        $this->expiration_date = $expiration_date;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
