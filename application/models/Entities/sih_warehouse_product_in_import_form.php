<?php
include_once 'BaseEntity.php';
// Entities/sih_warehouse_product_in_import_form.php

/**
 * @Entity @Table(name="sih_warehouse_product_in_import_form")
 **/
class Sih_warehouse_product_in_import_form extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /**
     * @ManyToOne(targetEntity="sih_product")
     * @JoinColumn(name="product_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $product_id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse_import_form")
     * @JoinColumn(name="warehouse_import_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_import_form_id;

    /** @Column(type="string", nullable=true) * */
    protected $quantity;

    /** @Column(type="string", nullable=true) * */
    protected $price;

    /** @Column(type="datetime", nullable=true) * */
    protected $expired_date;

    /**
     * @ManyToOne(targetEntity="sih_unit")
     * @JoinColumn(name="unit_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $unit_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getProduct_id()
    {
        return $this->product_id;
    }

    public function getWarehouse_import_form_id()
    {
        return $this->warehouse_import_form_id;
    }

    public function getUnit_id()
    {
        return $this->unit_id;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getExpired_date()
    {
        return $this->expired_date;
    }

    public function getPrice()
    {
        return $this->price;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setWarehouse_import_form_id($warehouse_import_form_id)
    {
        $this->warehouse_import_form_id = $warehouse_import_form_id;
    }

    public function setProduct_id($product_id)
    {
        $this->product_id = $product_id;
    }

    public function setUnit_id($unit_id)
    {
        $this->unit_id = $unit_id;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setExpired_date($expired_date)
    {
        $this->expired_date = $expired_date;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
