<?php
include_once 'BaseEntity.php';
// Entities/sih_content_permission.php

/**
 * @Entity @Table(name="sih_content_permission")
 **/
class Sih_content_permission extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="integer", nullable=false) * */
	protected $type_user;

    /** @Column(type="string", nullable=false) * */
    protected $route_path;

    /** @Column(type="string", nullable=false) * */
	protected $html_id;

	public function getId()
	{
		return $this->id;
	}

	public function getType_user()
	{
		return $this->type_user;
	}

    public function getRoute_path()
    {
        return $this->route_path;
    }

	public function getHtml_id()
	{
		return $this->html_id;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

	public function setType_user($type_user)
	{
		$this->type_user = $type_user;
	}

    public function setRoute_path($route_path)
    {
        $this->route_path = $route_path;
    }

	public function setHtml_id($html_id)
	{
		$this->html_id = $html_id;
	}
}