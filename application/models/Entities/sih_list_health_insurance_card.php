<?php
// Entities/sih_list_health_insurance_card.php
include_once 'BaseEntity.php';

/**
 * @Entity @Table(name="sih_list_health_insurance_card")
 * @since v.1.0
 **/
class Sih_list_health_insurance_card extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=false) * */
	protected $code;

	/** @Column(type="string", nullable=false) * */
	protected $name;

	/**
	 * @ManyToOne(targetEntity="sih_list_insurance_company")
	 * @JoinColumn(name="insurance_company_id", referencedColumnName="id" , onDelete="NO ACTION")
	 */
	protected $insurance_company_id;

	/** @Column(type="datetime") * */
	protected $created_at;

	/** @Column(type="integer", options={"default":0}, nullable=true) * */
	protected $orders = 0;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

	public function getCode()
	{
		return $this->code;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getInsurance_company_id()
	{
		return $this->insurance_company_id;
	}

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

	public function getOrders()
	{
		return $this->orders;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setCode($code)
	{
		$this->code = $code;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setInsurance_company_id($insurance_company_id)
	{
		$this->insurance_company_id = $insurance_company_id;
	}

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}

	public function setOrders($orders)
	{
		$this->orders = $orders;
	}
}
