<?php
include_once 'BaseEntity.php';
// Entities/sih_list_appointed_in_medicine.php
/**
 * @Entity @Table(name="sih_list_appointed_in_medicine")
 * */

class Sih_list_appointed_in_medicine extends BaseEntity {

	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $from_age;

	/** @Column(type="string", nullable=true) * */
	protected $to_age;

	/** @Column(type="string", nullable=true) * */
	protected $dose;

    /**
     * @ManyToOne(targetEntity="sih_medicine_product", inversedBy="list_appointed_in_medicine")
     * @JoinColumn(name="medicine_id", referencedColumnName="id", onDelete="NO ACTION", nullable=true)
     */
    protected $medicine_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"default":0}, nullable=true) * */
	protected $orders = 0;

	public function getId() {
		return $this->id;
	}

	public function getFrom_age() {
		return $this->from_age;
	}

	public function getTo_age() {
		return $this->to_age;
	}

	public function getDose() {
		return $this->dose;
	}

	public function getMedicine_id() {
		return $this->medicine_id;
	}

	public function getCreated_at() {
		return $this->created_at;
	}

	public function getStat() {
		return $this->stat;
	}

	public function getOrders() {
		return $this->orders;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setFrom_age($from_age) {
		$this->from_age = $from_age;
	}

	public function setTo_age($to_age) {
		$this->to_age = $to_age;
	}

	public function setDose($dose) {
		$this->dose = $dose;
	}

	public function setMedicine_id($medicine_id) {
		$this->medicine_id = $medicine_id;
	}

	public function setCreated_at($created_at) {
		$this->created_at = $created_at;
	}

	public function setStat($stat) {
		$this->stat = $stat;
	}

	public function setOrders($orders) {
		$this->orders = $orders;
	}
}
