<?php
include_once 'BaseEntity.php';
// Entities/sih_subclinical_ctc.php

/**
 * @Entity @Table(name="sih_subclinical_ctc")
 **/
class Sih_subclinical_ctc extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_pathologist_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_pathologist_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_doctor_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_doctor_idxx;

    /** @Column(type="datetime", nullable=true) * */
    protected $surgery_date;

    /** @Column(type="datetime", nullable=true) * */
    protected $surgery_biomass_date;

    /** @Column(type="string", nullable=true) * */
    protected $lam_pap;

    /** @Column(type="string", nullable=true) * */
    protected $para;

    /** @Column(type="string", nullable=true) * */
    protected $kkc;

    /** @Column(type="string", nullable=true) * */
    protected $evaluate_pap_smear_tinprep;

    /** @Column(type="integer", nullable=true) * */
    protected $evaluate_pap_smear_casualty;
    /** @Column(type="integer", nullable=true) * */
    protected $evaluate_pap_smear_liqui_prep;
    /** @Column(type="integer", nullable=true) * */
    protected $evaluate_pap_smear_qualified;
    /** @Column(type="integer", nullable=true) * */
    protected $evaluate_pap_smear_failure;
    /** @Column(type="integer", nullable=true) * */
    protected $evaluate_pap_smear_reason_failure;
    /** @Column(type="integer", nullable=true) * */
    protected $result_no_lesions_or_cancer;
    /** @Column(type="integer", nullable=true) * */
    protected $result_abnormal_epithelial_cells;
    /** @Column(type="integer", nullable=true) * */
    protected $result_other_abnormalities;
    /** @Column(type="integer", nullable=true) * */
    protected $result_endometrium;
    /** @Column(type="integer", nullable=true) * */
    protected $bacterial_transformation_trichomonas_vainailis;
    /** @Column(type="integer", nullable=true) * */
    protected $bacterial_transformation_candida_sp;
    /** @Column(type="integer", nullable=true) * */
    protected $bacterial_transformation_nematode;
    /** @Column(type="integer", nullable=true) * */
    protected $bacterial_transformation_actinomyces_sp;
    /** @Column(type="integer", nullable=true) * */
    protected $bacterial_transformation_herpes_simplex_virus;
    /** @Column(type="integer", nullable=true) * */
    protected $gauze_cells_asc_us;
    /** @Column(type="integer", nullable=true) * */
    protected $gauze_cells_asc_h;
    /** @Column(type="integer", nullable=true) * */
    protected $gauze_cells_lsil;
    /** @Column(type="integer", nullable=true) * */
    protected $gauze_cells_lsil_hpv;
    /** @Column(type="integer", nullable=true) * */
    protected $gauze_cells_hsil;
    /** @Column(type="integer", nullable=true) * */
    protected $gauze_cells_scc;
    /** @Column(type="integer", nullable=true) * */
    protected $transform_by_other_cell_inflammation;
    /** @Column(type="integer", nullable=true) * */
    protected $transform_by_other_cell_radiotherapy;
    /** @Column(type="integer", nullable=true) * */
    protected $transform_by_other_cell_iud;
    /** @Column(type="integer", nullable=true) * */
    protected $transform_by_other_cell_atrophy;
    /** @Column(type="integer", nullable=true) * */
    protected $gland_cellagc_ctc;
    /** @Column(type="integer", nullable=true) * */
    protected $gland_cellagc_endothelium_tc;
    /** @Column(type="integer", nullable=true) * */
    protected $gland_cellagc_unknown;
    /** @Column(type="integer", nullable=true) * */
    protected $gland_cellu_ctc;
    /** @Column(type="integer", nullable=true) * */
    protected $gland_cellu_noi_mac_tc;
    /** @Column(type="integer", nullable=true) * */
    protected $gland_cellu_unknown;
    /** @Column(type="integer", nullable=true) * */
    protected $gland_cellcarcinom_ais;
    /** @Column(type="integer", nullable=true) * */
    protected $gland_cellcarcinom_invasive;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getStaff_pathologist_id()
    {
        return $this->staff_pathologist_id;
    }

    public function setStaff_pathologist_id($staff_pathologist_id)
    {
        $this->staff_pathologist_id = $staff_pathologist_id;
    }

    public function getStaff_doctor_idxx()
    {
        return $this->staff_doctor_idxx;
    }

    public function setStaff_doctor_idxx($staff_doctor_idxx)
    {
        $this->staff_doctor_idxx = $staff_doctor_idxx;
    }

    public function getSurgery_date()
    {
        return $this->surgery_date;
    }

    public function setSurgery_date($surgery_date)
    {
        $this->surgery_date = $surgery_date;
    }

    public function getSurgery_biomass_date()
    {
        return $this->surgery_biomass_date;
    }

    public function setSurgery_biomass_date($surgery_biomass_date)
    {
        $this->surgery_biomass_date = $surgery_biomass_date;
    }

    public function getLam_pap()
    {
        return $this->lam_pap;
    }

    public function setLam_pap($lam_pap)
    {
        $this->lam_pap = $lam_pap;
    }

    public function getPara()
    {
        return $this->para;
    }

    public function setPara($para)
    {
        $this->para = $para;
    }

    public function getKkc()
    {
        return $this->kkc;
    }

    public function setKkc($kkc)
    {
        $this->kkc = $kkc;
    }

    public function getEvaluate_pap_smear_tinprep()
    {
        return $this->evaluate_pap_smear_tinprep;
    }

    public function setEvaluate_pap_smear_tinprep($evaluate_pap_smear_tinprep)
    {
        $this->evaluate_pap_smear_tinprep = $evaluate_pap_smear_tinprep;
    }

    public function getEvaluate_pap_smear_casualty()
    {
        return $this->evaluate_pap_smear_casualty;
    }

    public function setEvaluate_pap_smear_casualty($evaluate_pap_smear_casualty)
    {
        $this->evaluate_pap_smear_casualty = $evaluate_pap_smear_casualty;
    }

    public function getEvaluate_pap_smear_liqui_prep()
    {
        return $this->evaluate_pap_smear_liqui_prep;
    }

    public function setEvaluate_pap_smear_liqui_prep($evaluate_pap_smear_liqui_prep)
    {
        $this->evaluate_pap_smear_liqui_prep = $evaluate_pap_smear_liqui_prep;
    }

    public function getEvaluate_pap_smear_qualified()
    {
        return $this->evaluate_pap_smear_qualified;
    }

    public function setEvaluate_pap_smear_qualified($evaluate_pap_smear_qualified)
    {
        $this->evaluate_pap_smear_qualified = $evaluate_pap_smear_qualified;
    }

    public function getEvaluate_pap_smear_failure()
    {
        return $this->evaluate_pap_smear_failure;
    }

    public function setEvaluate_pap_smear_failure($evaluate_pap_smear_failure)
    {
        $this->evaluate_pap_smear_failure = $evaluate_pap_smear_failure;
    }

    public function getEvaluate_pap_smear_reason_failure()
    {
        return $this->evaluate_pap_smear_reason_failure;
    }

    public function setEvaluate_pap_smear_reason_failure($evaluate_pap_smear_reason_failure)
    {
        $this->evaluate_pap_smear_reason_failure = $evaluate_pap_smear_reason_failure;
    }

    public function getResult_no_lesions_or_cancer()
    {
        return $this->result_no_lesions_or_cancer;
    }

    public function setResult_no_lesions_or_cancer($result_no_lesions_or_cancer)
    {
        $this->result_no_lesions_or_cancer = $result_no_lesions_or_cancer;
    }

    public function getResult_abnormal_epithelial_cells()
    {
        return $this->result_abnormal_epithelial_cells;
    }

    public function setResult_abnormal_epithelial_cells($result_abnormal_epithelial_cells)
    {
        $this->result_abnormal_epithelial_cells = $result_abnormal_epithelial_cells;
    }

    public function getResult_other_abnormalities()
    {
        return $this->result_other_abnormalities;
    }

    public function setResult_other_abnormalities($result_other_abnormalities)
    {
        $this->result_other_abnormalities = $result_other_abnormalities;
    }

    public function getResult_endometrium()
    {
        return $this->result_endometrium;
    }

    public function setResult_endometrium($result_endometrium)
    {
        $this->result_endometrium = $result_endometrium;
    }

    public function getBacterial_transformation_trichomonas_vainailis()
    {
        return $this->bacterial_transformation_trichomonas_vainailis;
    }

    public function setBacterial_transformation_trichomonas_vainailis($bacterial_transformation_trichomonas_vainailis)
    {
        $this->bacterial_transformation_trichomonas_vainailis = $bacterial_transformation_trichomonas_vainailis;
    }

    public function getBacterial_transformation_candida_sp()
    {
        return $this->bacterial_transformation_candida_sp;
    }

    public function setBacterial_transformation_candida_sp($bacterial_transformation_candida_sp)
    {
        $this->bacterial_transformation_candida_sp = $bacterial_transformation_candida_sp;
    }

    public function getBacterial_transformation_nematode()
    {
        return $this->bacterial_transformation_nematode;
    }

    public function setBacterial_transformation_nematode($bacterial_transformation_nematode)
    {
        $this->bacterial_transformation_nematode = $bacterial_transformation_nematode;
    }

    public function getBacterial_transformation_actinomyces_sp()
    {
        return $this->bacterial_transformation_actinomyces_sp;
    }

    public function setBacterial_transformation_actinomyces_sp($bacterial_transformation_actinomyces_sp)
    {
        $this->bacterial_transformation_actinomyces_sp = $bacterial_transformation_actinomyces_sp;
    }

    public function getBacterial_transformation_herpes_simplex_virus()
    {
        return $this->bacterial_transformation_herpes_simplex_virus;
    }

    public function setBacterial_transformation_herpes_simplex_virus($bacterial_transformation_herpes_simplex_virus)
    {
        $this->bacterial_transformation_herpes_simplex_virus = $bacterial_transformation_herpes_simplex_virus;
    }

    public function getGauze_cells_asc_us()
    {
        return $this->gauze_cells_asc_us;
    }

    public function setGauze_cells_asc_us($gauze_cells_asc_us)
    {
        $this->gauze_cells_asc_us = $gauze_cells_asc_us;
    }

    public function getGauze_cells_asc_h()
    {
        return $this->gauze_cells_asc_h;
    }

    public function setGauze_cells_asc_h($gauze_cells_asc_h)
    {
        $this->gauze_cells_asc_h = $gauze_cells_asc_h;
    }

    public function getGauze_cells_lsil()
    {
        return $this->gauze_cells_lsil;
    }

    public function setGauze_cells_lsil($gauze_cells_lsil)
    {
        $this->gauze_cells_lsil = $gauze_cells_lsil;
    }

    public function getGauze_cells_lsil_hpv()
    {
        return $this->gauze_cells_lsil_hpv;
    }

    public function setGauze_cells_lsil_hpv($gauze_cells_lsil_hpv)
    {
        $this->gauze_cells_lsil_hpv = $gauze_cells_lsil_hpv;
    }

    public function getGauze_cells_hsil()
    {
        return $this->gauze_cells_hsil;
    }

    public function setGauze_cells_hsil($gauze_cells_hsil)
    {
        $this->gauze_cells_hsil = $gauze_cells_hsil;
    }

    public function getGauze_cells_scc()
    {
        return $this->gauze_cells_scc;
    }

    public function setGauze_cells_scc($gauze_cells_scc)
    {
        $this->gauze_cells_scc = $gauze_cells_scc;
    }

    public function getTransform_by_other_cell_inflammation()
    {
        return $this->transform_by_other_cell_inflammation;
    }

    public function setTransform_by_other_cell_inflammation($transform_by_other_cell_inflammation)
    {
        $this->transform_by_other_cell_inflammation = $transform_by_other_cell_inflammation;
    }

    public function getTransform_by_other_cell_radiotherapy()
    {
        return $this->transform_by_other_cell_radiotherapy;
    }

    public function setTransform_by_other_cell_radiotherapy($transform_by_other_cell_radiotherapy)
    {
        $this->transform_by_other_cell_radiotherapy = $transform_by_other_cell_radiotherapy;
    }

    public function getTransform_by_other_cell_iud()
    {
        return $this->transform_by_other_cell_iud;
    }

    public function setTransform_by_other_cell_iud($transform_by_other_cell_iud)
    {
        $this->transform_by_other_cell_iud = $transform_by_other_cell_iud;
    }

    public function getTransform_by_other_cell_atrophy()
    {
        return $this->transform_by_other_cell_atrophy;
    }

    public function setTransform_by_other_cell_atrophy($transform_by_other_cell_atrophy)
    {
        $this->transform_by_other_cell_atrophy = $transform_by_other_cell_atrophy;
    }

    public function getGland_cellagc_ctc()
    {
        return $this->gland_cellagc_ctc;
    }

    public function setGland_cellagc_ctc($gland_cellagc_ctc)
    {
        $this->gland_cellagc_ctc = $gland_cellagc_ctc;
    }

    public function getGland_cellagc_endothelium_tc()
    {
        return $this->gland_cellagc_endothelium_tc;
    }

    public function setGland_cellagc_endothelium_tc($gland_cellagc_endothelium_tc)
    {
        $this->gland_cellagc_endothelium_tc = $gland_cellagc_endothelium_tc;
    }

    public function getGland_cellagc_unknown()
    {
        return $this->gland_cellagc_unknown;
    }

    public function setGland_cellagc_unknown($gland_cellagc_unknown)
    {
        $this->gland_cellagc_unknown = $gland_cellagc_unknown;
    }

    public function getGland_cellu_ctc()
    {
        return $this->gland_cellu_ctc;
    }

    public function setGland_cellu_ctc($gland_cellu_ctc)
    {
        $this->gland_cellu_ctc = $gland_cellu_ctc;
    }

    public function getGland_cellu_noi_mac_tc()
    {
        return $this->gland_cellu_noi_mac_tc;
    }

    public function setGland_cellu_noi_mac_tc($gland_cellu_noi_mac_tc)
    {
        $this->gland_cellu_noi_mac_tc = $gland_cellu_noi_mac_tc;
    }

    public function getGland_cellu_unknown()
    {
        return $this->gland_cellu_unknown;
    }

    public function setGland_cellu_unknown($gland_cellu_unknown)
    {
        $this->gland_cellu_unknown = $gland_cellu_unknown;
    }

    public function getGland_cellcarcinom_ais()
    {
        return $this->gland_cellcarcinom_ais;
    }

    public function setGland_cellcarcinom_ais($gland_cellcarcinom_ais)
    {
        $this->gland_cellcarcinom_ais = $gland_cellcarcinom_ais;
    }

    public function getGland_cellcarcinom_invasive()
    {
        return $this->gland_cellcarcinom_invasive;
    }

    public function setGland_cellcarcinom_invasive($gland_cellcarcinom_invasive)
    {
        $this->gland_cellcarcinom_invasive = $gland_cellcarcinom_invasive;
    }
}