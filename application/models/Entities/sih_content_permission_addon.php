<?php
include_once 'BaseEntity.php';
// Entities/sih_content_permission_addon.php

/**
 * @Entity @Table(name="sih_content_permission_addon")
 **/
class Sih_content_permission_addon extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="integer", nullable=false) * */
	protected $type_user;

    /** @Column(type="string", nullable=false) * */
	protected $html_id;

    /** @Column(type="string", nullable=false) * */
    protected $html_inner_content;

	public function getId()
	{
		return $this->id;
	}

	public function getType_user()
	{
		return $this->type_user;
	}

	public function getHtml_id()
	{
		return $this->html_id;
	}

    public function getHtml_inner_content()
    {
        return $this->html_inner_content;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

	public function setType_user($type_user)
	{
		$this->type_user = $type_user;
	}

	public function setHtml_id($html_id)
	{
		$this->html_id = $html_id;
	}

    public function setHtml_inner_content($html_inner_content)
    {
        $this->html_inner_content = $html_inner_content;
    }
}
