<?php
include_once 'BaseEntity.php';
// Entities/sih_warehouse_product_in_package_by_date.php

/**
 * @Entity @Table(name="sih_warehouse_product_in_package_by_date")
 **/
class Sih_warehouse_product_in_package_by_date extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse_product_in_package")
     * @JoinColumn(name="warehouse_product_in_package_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_product_in_package_id;

    /** @Column(type="string", nullable=true) * */
    protected $quantity;

    /** @Column(type="string", nullable=true) * */
    protected $used_quantity;

    /** @Column(type="datetime", nullable=true) * */
    protected $history_date;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getWarehouse_product_in_package_id()
    {
        return $this->warehouse_product_in_package_id;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getUsed_quantity()
    {
        return $this->used_quantity;
    }

    public function getHistory_date()
    {
        return $this->history_date;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setWarehouse_product_in_package_id($warehouse_product_in_package_id)
    {
        $this->warehouse_product_in_package_id = $warehouse_product_in_package_id;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setUsed_quantity($used_quantity)
    {
        $this->used_quantity = $used_quantity;
    }

    public function setHistory_date($history_date)
    {
        $this->history_date = $history_date;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
