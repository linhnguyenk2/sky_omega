<?php
include_once 'BaseEntity.php';
// Entities/sih_medical_examination_form_in_health_book.php

/**
 * @Entity @Table(name="sih_medical_examination_form_in_health_book")
 * */
class Sih_medical_examination_form_in_health_book extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_health_book")
     * @JoinColumn(name="health_book_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $health_book_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="doctor_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $doctor_id;

    /** @Column(type="datetime", nullable=true) * */
    protected $examination_date;

    /** @Column(type="string", nullable=true) * */
    protected $day_of_age;

    /** @Column(type="string", nullable=true) * */
    protected $weight;

    /** @Column(type="string", nullable=true) * */
    protected $height;

    /** @Column(type="string", nullable=true) * */
    protected $mucodermal;

    /** @Column(type="string", nullable=true) * */
    protected $mucodermal_description;

    /** @Column(type="string", nullable=true) * */
    protected $eye;

    /** @Column(type="string", nullable=true) * */
    protected $eye_description;

    /** @Column(type="string", nullable=true) * */
    protected $glosbe;

    /** @Column(type="string", nullable=true) * */
    protected $glosbe_description;

    /** @Column(type="string", nullable=true) * */
    protected $pholcodine;

    /** @Column(type="string", nullable=true) * */
    protected $pholcodine_description;

    /** @Column(type="string", nullable=true) * */
    protected $heart;

    /** @Column(type="string", nullable=true) * */
    protected $heart_description;

    /** @Column(type="string", nullable=true) * */
    protected $alimentary_abdomen;

    /** @Column(type="string", nullable=true) * */
    protected $alimentary_abdomen_description;

    /** @Column(type="string", nullable=true) * */
    protected $alimentary_anal;

    /** @Column(type="string", nullable=true) * */
    protected $alimentary_anal_description;

    /** @Column(type="string", nullable=true) * */
    protected $nervous;

    /** @Column(type="string", nullable=true) * */
    protected $nervous_description;

    /** @Column(type="string", nullable=true) * */
    protected $muscle;

    /** @Column(type="string", nullable=true) * */
    protected $muscle_description;

    /** @Column(type="string", nullable=true) * */
    protected $antidiuretics;

    /** @Column(type="string", nullable=true) * */
    protected $antidiuretics_description;

    /** @Column(type="string", nullable=true) * */
    protected $abdominal;

    /** @Column(type="string", nullable=true) * */
    protected $abdominal_description;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getPatient_id()
    {
        return $this->patient_id;
    }

    public function setPatient_id($patient_id)
    {
        $this->patient_id = $patient_id;
    }

    public function getHealth_book_id()
    {
        return $this->health_book_id;
    }

    public function setHealth_book_id($health_book_id)
    {
        $this->health_book_id = $health_book_id;
    }

    public function getDoctor_id()
    {
        return $this->doctor_id;
    }

    public function setDoctor_id($doctor_id)
    {
        $this->doctor_id = $doctor_id;
    }

    public function getExamination_date()
    {
        return $this->examination_date;
    }

    public function setExamination_date($examination_date)
    {
        $this->examination_date = $examination_date;
    }

    public function getDay_of_age()
    {
        return $this->day_of_age;
    }

    public function setDay_of_age($day_of_age)
    {
        $this->day_of_age = $day_of_age;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getMucodermal()
    {
        return $this->mucodermal;
    }

    public function setMucodermal($mucodermal)
    {
        $this->mucodermal = $mucodermal;
    }

    public function getMucodermal_description()
    {
        return $this->mucodermal_description;
    }

    public function setMucodermal_description($mucodermal_description)
    {
        $this->mucodermal_description = $mucodermal_description;
    }

    public function getEye()
    {
        return $this->eye;
    }

    public function setEye($eye)
    {
        $this->eye = $eye;
    }

    public function getEye_description()
    {
        return $this->eye_description;
    }

    public function setEye_description($eye_description)
    {
        $this->eye_description = $eye_description;
    }

    public function getGlosbe()
    {
        return $this->glosbe;
    }

    public function setGlosbe($glosbe)
    {
        $this->glosbe = $glosbe;
    }

    public function getGlosbe_description()
    {
        return $this->glosbe_description;
    }

    public function setGlosbe_description($glosbe_description)
    {
        $this->glosbe_description = $glosbe_description;
    }

    public function getPholcodine()
    {
        return $this->pholcodine;
    }

    public function setPholcodine($pholcodine)
    {
        $this->pholcodine = $pholcodine;
    }

    public function getPholcodine_description()
    {
        return $this->pholcodine_description;
    }

    public function setPholcodine_description($pholcodine_description)
    {
        $this->pholcodine_description = $pholcodine_description;
    }

    public function getHeart()
    {
        return $this->heart;
    }

    public function setHeart($heart)
    {
        $this->heart = $heart;
    }

    public function getHeart_description()
    {
        return $this->heart_description;
    }

    public function setHeart_description($heart_description)
    {
        $this->heart_description = $heart_description;
    }

    public function getAlimentary_abdomen()
    {
        return $this->alimentary_abdomen;
    }

    public function setAlimentary_abdomen($alimentary_abdomen)
    {
        $this->alimentary_abdomen = $alimentary_abdomen;
    }

    public function getAlimentary_abdomen_description()
    {
        return $this->alimentary_abdomen_description;
    }

    public function setAlimentary_abdomen_description($alimentary_abdomen_description)
    {
        $this->alimentary_abdomen_description = $alimentary_abdomen_description;
    }

    public function getAlimentary_anal()
    {
        return $this->alimentary_anal;
    }

    public function setAlimentary_anal($alimentary_anal)
    {
        $this->alimentary_anal = $alimentary_anal;
    }

    public function getAlimentary_anal_description()
    {
        return $this->alimentary_anal_description;
    }

    public function setAlimentary_anal_description($alimentary_anal_description)
    {
        $this->alimentary_anal_description = $alimentary_anal_description;
    }

    public function getNervous()
    {
        return $this->nervous;
    }

    public function setNervous($nervous)
    {
        $this->nervous = $nervous;
    }

    public function getNervous_description()
    {
        return $this->nervous_description;
    }

    public function setNervous_description($nervous_description)
    {
        $this->nervous_description = $nervous_description;
    }

    public function getMuscle()
    {
        return $this->muscle;
    }

    public function setMuscle($muscle)
    {
        $this->muscle = $muscle;
    }

    public function getMuscle_description()
    {
        return $this->muscle_description;
    }

    public function setMuscle_description($muscle_description)
    {
        $this->muscle_description = $muscle_description;
    }

    public function getAntidiuretics()
    {
        return $this->antidiuretics;
    }

    public function setAntidiuretics($antidiuretics)
    {
        $this->antidiuretics = $antidiuretics;
    }

    public function getAntidiuretics_description()
    {
        return $this->antidiuretics_description;
    }

    public function setAntidiuretics_description($antidiuretics_description)
    {
        $this->antidiuretics_description = $antidiuretics_description;
    }

    public function getAbdominal()
    {
        return $this->abdominal;
    }

    public function setAbdominal($abdominal)
    {
        $this->abdominal = $abdominal;
    }

    public function getAbdominal_description()
    {
        return $this->abdominal_description;
    }

    public function setAbdominal_description($abdominal_description)
    {
        $this->abdominal_description = $abdominal_description;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }


}

