<?php
include_once 'BaseEntity.php';
// Entities/sih_medical_examination_form.php

/**
 * @Entity @Table(name="sih_medical_examination_form")
 **/
class Sih_medical_examination_form extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /** @Column(type="datetime", nullable=true) * */
    protected $check_in_time;

    /** @Column(type="string", nullable=true) * */
    protected $exam;

    /** @Column(type="string", nullable=true) * */
    protected $diagnose;

    /** @Column(type="string", nullable=true) * */
    protected $tracking_treatment;

    /** @Column(type="datetime", nullable=true) * */
    protected $re_examination_date;

    /** @Column(type="string", nullable=true) * */
    protected $type;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
	protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_id;

    /**
     * @ManyToOne(targetEntity="sih_medical_record")
     * @JoinColumn(name="medical_record_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $medical_record_id;

    /**
     * @ManyToOne(targetEntity="sih_gynecolog_form")
     * @JoinColumn(name="gynecolog_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $gynecolog_form_id;

    /**
         * @ManyToOne(targetEntity="sih_breast_examination_form")
     * @JoinColumn(name="breast_examination_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $breast_examination_form_id;

    /**
     * @ManyToOne(targetEntity="sih_prenatal_form")
     * @JoinColumn(name="prenatal_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $prenatal_form_id;

    /**
     * @ManyToOne(targetEntity="sih_menopause_form")
     * @JoinColumn(name="menopause_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $menopause_form_id;

    /**
     * @ManyToOne(targetEntity="sih_family_plan_birth_control_form")
     * @JoinColumn(name="family_plan_birth_planing_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $family_plan_birth_planing_form_id;

    /**
     * @ManyToOne(targetEntity="sih_artificial_examination_form")
     * @JoinColumn(name="artificial_examination_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $artificial_examination_form_id;

    /**
     * @ManyToOne(targetEntity="sih_medical_examination_form_in_health_book")
     * @JoinColumn(name="medical_examination_form_in_health_book_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $medical_examination_form_in_health_book_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_result_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_result_id;

    /**
     * @ManyToOne(targetEntity="sih_specify_service_paper")
     * @JoinColumn(name="specify_service_paper_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $specify_service_paper_id;


	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getCode()
    {
        return $this->code;
    }

    public function getCheck_in_time()
    {
        return $this->check_in_time;
    }

    public function getExam()
    {
        return $this->exam;
    }

    public function getDiagnose()
    {
        return $this->diagnose;
    }

    public function getTracking_treatment()
    {
        return $this->tracking_treatment;
    }

    public function getRe_examination_date()
    {
        return $this->re_examination_date;
    }

    public function getBreast_examination_form_id()
    {
        return $this->breast_examination_form_id;
    }

    public function getPrenatal_form_id()
    {
        return $this->prenatal_form_id;
    }

    public function getMenopause_form_id()
    {
        return $this->menopause_form_id;
    }

    public function getFamily_plan_birth_planing_form_id()
    {
        return $this->family_plan_birth_planing_form_id;
    }

    public function getArtificial_examination_form_id()
    {
        return $this->artificial_examination_form_id;
    }

    public function getStaff_result_id()
    {
        return $this->staff_result_id;
    }

    public function getSpecify_service_paper_id()
    {
        return $this->specify_service_paper_id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getPatient_id()
	{
		return $this->patient_id;
	}

    public function getStaff_id()
    {
        return $this->staff_id;
    }

    public function getMedical_record_id()
    {
        return $this->medical_record_id;
    }

    public function getGynecolog_form_id()
    {
        return $this->gynecolog_form_id;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setBreast_examination_form_id($breast_examination_form_id)
    {
        $this->breast_examination_form_id = $breast_examination_form_id;
    }

    public function setPrenatal_form_id($prenatal_form_id)
    {
        $this->prenatal_form_id = $prenatal_form_id;
    }

    public function setCheck_in_time($check_in_time)
    {
        $this->check_in_time = $check_in_time;
    }

    public function setExam($exam)
    {
        $this->exam = $exam;
    }

    public function setDiagnose($diagnose)
    {
        $this->diagnose = $diagnose;
    }

    public function setTracking_treatment($tracking_treatment)
    {
        $this->tracking_treatment = $tracking_treatment;
    }

    public function setRe_examination_date($re_examination_date)
    {
        $this->re_examination_date = $re_examination_date;
    }

	public function setPatient_id($patient_id)
	{
		$this->patient_id = $patient_id;
	}

    public function setStaff_id($staff_id)
    {
        $this->staff_id = $staff_id;
    }

    public function setFamily_plan_birth_planing_form_id($family_plan_birth_planing_form_id)
    {
        $this->family_plan_birth_planing_form_id = $family_plan_birth_planing_form_id;
    }

    public function setArtificial_examination_form_id($artificial_examination_form_id)
    {
        $this->artificial_examination_form_id = $artificial_examination_form_id;
    }

    public function setSpecify_service_paper_id($specify_service_paper_id)
    {
        $this->specify_service_paper_id = $specify_service_paper_id;
    }

    public function setStaff_result_id($staff_result_id)
    {
        $this->staff_result_id = $staff_result_id;
    }

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

    public function setMedical_record_id($medical_record_id)
    {
        $this->medical_record_id = $medical_record_id;
    }

    public function setGynecolog_form_id($gynecolog_form_id)
    {
        $this->gynecolog_form_id = $gynecolog_form_id;
    }

    public function setMenopause_form_id($menopause_form_id)
    {
        $this->menopause_form_id = $menopause_form_id;
    }

	public function setStat($stat)
	{
		$this->stat = $stat;
	}

    public function getMedical_examination_form_in_health_book_id()
    {
        return $this->medical_examination_form_in_health_book_id;
    }

    public function setMedical_examination_form_in_health_book_id($medical_examination_form_in_health_book_id)
    {
        $this->medical_examination_form_in_health_book_id = $medical_examination_form_in_health_book_id;
    }
}
