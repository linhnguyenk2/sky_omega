<?php
include_once 'BaseEntity.php';
// Entities/sih_product_use_location_in_service.php
/**
 * @Entity @Table(name="sih_product_use_location_in_service")
 **/
class Sih_product_use_location_in_service extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string", nullable=true) **/
    protected $quatity;

    /**
     * @ManyToOne(targetEntity="sih_product")
     * @JoinColumn(name="product_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $product_id;

    /**
     * @ManyToOne(targetEntity="sih_location_in_service")
     * @JoinColumn(name="location_in_service_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $location_in_service_id;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}, nullable=true) * */
    protected $stat = 1;

    /** @Column(type="integer", options={"default":0}, nullable=true) * */
    protected $orders = 0;

    public function getId()
    {
        return $this->id;
    }

    public function getQuatity()
    {
        return $this->quatity;
    }

    public function getLocation_in_service_id()
    {
        return $this->location_in_service_id;
    }

    public function getProduct_id()
    {
        return $this->product_id;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function getOrders()
    {
        return $this->orders;
    }

    public function setLocation_in_service_id($location_in_service_id)
    {
        $this->location_in_service_id = $location_in_service_id;
    }

    public function setQuatity($quatity)
    {
        $this->quatity = $quatity;
    }

    public function setProduct_id($product_id)
    {
        $this->product_id = $product_id;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }

    public function setOrders($orders)
    {
        $this->orders = $orders;
    }
}
