<?php
include_once 'BaseEntity.php';
// Entities/sih_warehouse_check_form.php

/**
 * @Entity @Table(name="sih_warehouse_check_form")
 **/
class Sih_warehouse_check_form extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="check_staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $check_staff_id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse")
     * @JoinColumn(name="warehouse_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getCode()
    {
        return $this->code;
    }

    public function getStaff_id()
    {
        return $this->staff_id;
    }

    public function getWarehouse_id()
    {
        return $this->warehouse_id;
    }

    public function getCheck_staff_id()
    {
        return $this->check_staff_id;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setStaff_id($staff_id)
    {
        $this->staff_id = $staff_id;
    }

    public function setWarehouse_id($warehouse_id)
    {
        $this->warehouse_id = $warehouse_id;
    }

    public function setCheck_staff_id($check_staff_id)
    {
        $this->check_staff_id = $check_staff_id;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
