<?php
include_once 'BaseEntity.php';
// Entities/sih_list_reason_transfer.php

/**
 * @Entity @Table(name="sih_list_reason_transfer")
 **/
class Sih_list_reason_transfer extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $code;

	/** @Column(type="string", nullable=true) * */
	protected $name;

	/** @Column(type="string", nullable=true) * */
	protected $note;

	/** @Column(type="datetime") * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"default":0}, nullable=true) * */
	protected $orders = 0;

	public function getId()
	{
		return $this->id;
	}

	public function getCode()
	{
		return $this->code;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getNote()
	{
		return $this->note;
	}

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

	public function getOrders()
	{
		return $this->orders;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setCode($code)
	{
		$this->code = $code;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setNote($note)
	{
		$this->note = $note;
	}

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}

	public function setOrders($orders)
	{
		$this->orders = $orders;
	}
}
