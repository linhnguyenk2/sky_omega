<?php
include_once 'BaseEntity.php';
// Entities/sih_menu_permission.php

/**
 * @Entity @Table(name="sih_menu_permission")
 **/
class Sih_menu_permission extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=false) * */
	protected $type_user;

    /** @Column(type="string", nullable=false) * */
	protected $html_id;

	public function getId()
	{
		return $this->id;
	}

	public function getType_user()
	{
		return $this->type_user;
	}

	public function getHtml_id()
	{
		return $this->html_id;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

	public function setType_user($type_user)
	{
		$this->type_user = $type_user;
	}

	public function setHtml_id($html_id)
	{
		$this->html_id = $html_id;
	}
}