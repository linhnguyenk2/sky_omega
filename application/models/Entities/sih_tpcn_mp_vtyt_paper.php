<?php
include_once 'BaseEntity.php';
// Entities/sih_tpcn_mp_vtyt_paper.php

/**
 * @Entity @Table(name="sih_tpcn_mp_vtyt_paper")
 **/
class Sih_tpcn_mp_vtyt_paper extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $code;

    /** @Column(type="string", nullable=true) * */
    protected $quantity;

    /** @Column(type="string", nullable=true) * */
    protected $usages;

	/**
	 * @ManyToOne(targetEntity="sih_patients")
	 * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_id;

    /**
     * @ManyToOne(targetEntity="sih_medical_examination_form")
     * @JoinColumn(name="medical_examination_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $medical_examination_form_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

	public function getCode()
	{
		return $this->code;
	}

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getUsages()
    {
        return $this->usages;
    }

	public function getPatient_id()
	{
		return $this->patient_id;
	}

    public function getStaff_id()
    {
        return $this->staff_id;
    }

    public function getMedical_examination_form_id()
    {
        return $this->medical_examination_form_id;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

	public function setCode($code)
	{
		$this->code = $code;
	}

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setUsages($usages)
    {
        $this->usages = $usages;
    }

	public function setPatient_id($patient_id)
	{
		$this->patient_id = $patient_id;
	}

    public function setStaff_id($staff_id)
    {
        $this->staff_id = $staff_id;
    }

    public function setMedical_examination_form_id($medical_examination_form_id)
    {
        $this->medical_examination_form_id = $medical_examination_form_id;
    }

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}
}