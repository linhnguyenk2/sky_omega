<?php
include_once 'BaseEntity.php';
// Entities/sih_user_key.php

/**
 * @Entity @Table(name="sih_user_key")
 **/
class Sih_user_key extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /**
     * @ManyToOne(targetEntity="sih_user")
     * @JoinColumn(name="user_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $user_id;

    /** @Column(type="string", nullable=false) * */
    protected $session_id;

    /** @Column(type="string", nullable=false) * */
    protected $device_id;

    /** @Column(type="string", nullable=true) * */
    protected $expiry;

    /** @Column(type="integer", options={"default":1}, nullable=true) * */
    protected $stat = 1;


    public function getId() {
        return $this->id;
    }

    public function getSession_id() {
        return $this->session_id;
    }

    public function getUser_id() {
        return $this->user_id;
    }

    public function getDevice_id() {
        return $this->device_id;
    }

    public function getExpiry() {
        return $this->expiry;
    }

    public function getStat() {
        return $this->stat;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUser_id($user_id) {
        $this->user_id = $user_id;
    }

    public function setSession_id($session_id) {
        $this->session_id = $session_id;
    }

    public function setDevice_id($device_id) {
        $this->device_id = $device_id;
    }

    public function setExpiry($expiry) {
        $this->expiry = $expiry;
    }

    public function setStat($stat) {
        $this->stat = $stat;
    }
}

