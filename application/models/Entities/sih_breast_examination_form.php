<?php
include_once 'BaseEntity.php';
// Entities/sih_breast_examination_form.php
/**
 * @Entity @Table(name="sih_breast_examination_form")
 * */
class Sih_breast_examination_form  extends BaseEntity {

	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $code;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	public function getId() {
		return $this->id;
	}

	public function getCode() {
		return $this->code;
	}

	public function getCreated_at() {
		return $this->created_at;
	}

	public function getStat() {
		return $this->stat;
	}

	public function setId($id) {
		$this->id = $id;
	}

    public function setCode($code) {
        $this->code = $code;
    }

	public function setCreated_at($created_at) {
		$this->created_at = $created_at;
	}

	public function setStat($stat) {
		$this->stat = $stat;
	}
}

