<?php
include_once 'BaseEntity.php';
// Entities/sih_product_in_tpcn_mp_vtyt_paper.php

/**
 * @Entity @Table(name="sih_product_in_tpcn_mp_vtyt_paper")
 **/
class Sih_product_in_tpcn_mp_vtyt_paper extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $quantity;

    /** @Column(type="string", nullable=true) * */
    protected $usages;

	/**
	 * @ManyToOne(targetEntity="sih_product")
	 * @JoinColumn(name="product_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $product_id;

    /**
     * @ManyToOne(targetEntity="sih_tpcn_mp_vtyt_paper")
     * @JoinColumn(name="tpcn_mp_vtyt_paper_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $tpcn_mp_vtyt_paper_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getUsages()
    {
        return $this->usages;
    }

	public function getProduct_id()
	{
		return $this->product_id;
	}

    public function getTpcn_mp_vtyt_paper_id()
    {
        return $this->tpcn_mp_vtyt_paper_id;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setUsages($usages)
    {
        $this->usages = $usages;
    }

	public function setTpcn_mp_vtyt_paper_id($tpcn_mp_vtyt_paper_id)
	{
		$this->tpcn_mp_vtyt_paper_id = $tpcn_mp_vtyt_paper_id;
	}

    public function setProduct_id($product_id)
    {
        $this->product_id = $product_id;
    }

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}
}