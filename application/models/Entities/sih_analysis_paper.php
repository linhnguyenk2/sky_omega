<?php
include_once 'BaseEntity.php';
// Entities/sih_analysis_paper.php

/**
 * @Entity @Table(name="sih_analysis_paper")
 **/
class Sih_analysis_paper extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
	protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="approve_staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $approve_staff_id;


    /**
     * @ManyToOne(targetEntity="sih_medical_examination_form")
     * @JoinColumn(name="medical_examination_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $medical_examination_form_id;

    /**
     * @ManyToOne(targetEntity="sih_analysis_blood_paper")
     * @JoinColumn(name="analysis_blood_paper_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $analysis_blood_paper_id;

    /**
     * @ManyToOne(targetEntity="sih_analysis_urine_paper")
     * @JoinColumn(name="analysis_urine_paper_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $analysis_urine_paper_id;

    /** @Column(type="string", nullable=true) * */
    protected $type;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getCode()
    {
        return $this->code;
    }

	public function getPatient_id()
	{
		return $this->patient_id;
	}

    public function getApprove_staff_id()
    {
        return $this->approve_staff_id;
    }

    public function getStaff_id()
    {
        return $this->staff_id;
    }

    public function getMedical_examination_form_id()
    {
        return $this->medical_examination_form_id;
    }

    public function getAnalysis_blood_paper_id()
    {
        return $this->analysis_blood_paper_id;
    }

    public function getAnalysis_urine_paper_id()
    {
        return $this->analysis_urine_paper_id;
    }

    public function getType()
    {
        return $this->type;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

	public function setPatient_id($patient_id)
	{
		$this->patient_id = $patient_id;
	}

    public function setStaff_id($staff_id)
    {
        $this->staff_id = $staff_id;
    }

    public function setApprove_staff_id($approve_staff_id)
    {
        $this->approve_staff_id = $approve_staff_id;
    }

    public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

    public function setMedical_examination_form_id($medical_examination_form_id)
    {
        $this->medical_examination_form_id = $medical_examination_form_id;
    }

    public function setAnalysis_blood_paper_id($analysis_blood_paper_id)
    {
        $this->analysis_blood_paper_id = $analysis_blood_paper_id;
    }

    public function setAnalysis_urine_paper_id($analysis_urine_paper_id)
    {
        $this->analysis_urine_paper_id = $analysis_urine_paper_id;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
