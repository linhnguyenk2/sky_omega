<?php
include_once 'BaseEntity.php';
// Entities/sih_subclinical_endoscopic_paper.php

/**
 * @Entity @Table(name="sih_subclinical_endoscopic_paper")
 **/
class Sih_subclinical_endoscopic_paper extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /** @Column(type="string", nullable=true) * */
    protected $para;

    /** @Column(type="string", nullable=true) * */
    protected $paps_mear;

    /** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getPara()
    {
        return $this->para;
    }

    /**
     * @param mixed $para
     */
    public function setPara($para)
    {
        $this->para = $para;
    }

    /**
     * @return mixed
     */
    public function getPaps_mear()
    {
        return $this->paps_mear;
    }

    /**
     * @param mixed $paps_mear
     */
    public function setPaps_mear($paps_mear)
    {
        $this->paps_mear = $paps_mear;
    }
}