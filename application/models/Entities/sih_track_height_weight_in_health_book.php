<?php
include_once 'BaseEntity.php';
// Entities/sih_track_height_weight_in_health_book.php

/**
 * @Entity @Table(name="sih_track_height_weight_in_health_book")
 **/
class Sih_track_height_weight_in_health_book extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_health_book")
     * @JoinColumn(name="health_book_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $health_book_id;

    /** @Column(type="string", nullable=true) * */
    protected $age;

    /** @Column(type="string", nullable=true) * */
    protected $height;

    /** @Column(type="string", nullable=true) * */
    protected $weight;

    /** @Column(type="string", nullable=true) * */
    protected $result;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getPatient_id()
    {
        return $this->patient_id;
    }

    public function setPatient_id($patient_id)
    {
        $this->patient_id = $patient_id;
    }

    public function getHealth_book_id()
    {
        return $this->health_book_id;
    }

    public function setHealth_book_id($health_book_id)
    {
        $this->health_book_id = $health_book_id;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function setResult($result)
    {
        $this->result = $result;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }
}

