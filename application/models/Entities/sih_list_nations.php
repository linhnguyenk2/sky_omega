<?php
include_once 'BaseEntity.php';
// Entities/sih_list_nations.php
/**
 * @Entity @Table(name="sih_list_nations")
 **/
class Sih_list_nations extends  BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    
	/** @Column(type="string", nullable=true) **/
    protected $code;
	
	/** @Column(type="string", nullable=true) **/
    protected $name;
	
	/** @Column(type="datetime") **/
    protected $created_at;

    /** @Column(type="integer", options={"default":1}, nullable=true) * */
    protected $stat = 1;

    /** @Column(type="integer", options={"default":0}, nullable=true) * */
    protected $orders = 0;

    function getId() {
        return $this->id;
    }

    function getCode() {
        return $this->code;
    }

    function getName() {
        return $this->name;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function getStat() {
        return $this->stat;
    }

    function getOrders() {
        return $this->orders;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCode($code) {
        $this->code = $code;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function setStat($stat) {
        $this->stat = $stat;
    }

    function setOrders($orders) {
        $this->orders = $orders;
    }


}