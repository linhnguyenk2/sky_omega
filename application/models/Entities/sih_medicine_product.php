<?php
include_once 'BaseEntity.php';

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

// Entities/sih_medicine_product`.php

/**
 * @Entity @Table(name="sih_medicine_product")
 * */
class Sih_medicine_product extends BaseEntity
{
    public function __construct()
    {
        $this->list_appointed_in_medicine        = new ArrayCollection();
        $this->list_not_combine_medicine         = new ArrayCollection();
        $this->list_unit_medicine                = new ArrayCollection();
        $this->list_contraindication_in_medicine = new ArrayCollection();
    }

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /**
     * One Sih_medicine_product has Many Sih_list_contraindication_in_medicine.
     * @OneToMany(targetEntity="sih_list_contraindication_in_medicine", mappedBy="medicine_id")
     */
    protected $list_contraindication_in_medicine;

    /**
     * One Sih_medicine_product has Many Sih_list_not_combine_medicine.
     * @OneToMany(targetEntity="sih_list_not_combine_medicine", mappedBy="origin_medicine_id")
     */
    protected $list_not_combine_medicine;

    /**
     * One Sih_medicine_product has Many Sih_list_appointed_in_medicine.
     * @OneToMany(targetEntity="sih_list_appointed_in_medicine", mappedBy="medicine_id")
     */
    protected $list_appointed_in_medicine;

    /** @Column(type="string", nullable=true) * */
    protected $atc;

    /** @Column(type="string", nullable=true) * */
    protected $chemical_compound;

    /** @Column(type="string", nullable=true) * */
    protected $way_in;

    /** @Column(type="string", nullable=true) * */
    protected $amount;

    /** @Column(type="string", nullable=true) * */
    protected $note;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}, nullable=true) * */
    protected $stat = 1;

    /** @Column(type="integer", options={"default":0}, nullable=true) * */
    protected $orders = 0;

    public function getId()
    {
        return $this->id;
    }

    public function getList_appointed_in_medicine()
    {
        return $this->list_appointed_in_medicine;
    }

    public function getList_not_combine_medicine()
    {
        return $this->list_not_combine_medicine;
    }


    public function getList_contraindication_in_medicine()
    {
        return $this->list_contraindication_in_medicine;
    }

    public function getChemical_compound()
    {
        return $this->chemical_compound;
    }

    public function getWay_in()
    {
        return $this->way_in;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function getOrders()
    {
        return $this->orders;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setList_appointed_in_medicine(ArrayCollection $list_appointed_in_medicine)
    {
        $this->list_appointed_in_medicine = $list_appointed_in_medicine;
    }

    public function setList_not_combine_medicine(ArrayCollection $list_not_combine_medicine)
    {
        $this->list_not_combine_medicine = $list_not_combine_medicine;
    }

    public function setList_contraindication_in_medicine(ArrayCollection $list_contraindication_in_medicine)
    {
        $this->list_contraindication_in_medicine = $list_contraindication_in_medicine;
    }

    public function setChemical_compound($chemical_compound)
    {
        $this->chemical_compound = $chemical_compound;
    }

    public function setWay_in($way_in)
    {
        $this->way_in = $way_in;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function setNote($note)
    {
        $this->note = $note;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }

    public function setOrders($orders)
    {
        $this->orders = $orders;
    }
}
