<?php
include_once 'BaseEntity.php';
// Entities/sih_list_supplier.php

/**
 * @Entity @Table(name="sih_list_supplier")
 **/
class Sih_list_supplier extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $code;

	/** @Column(type="string", nullable=true) * */
	protected $name;

	/** @Column(type="string", nullable=true) * */
	protected $address;

	/** @Column(type="string", nullable=true) * */
	protected $phone;

	/** @Column(type="string", nullable=true) * */
	protected $contact;

	/** @Column(type="string", nullable=true) * */
	protected $bank_name;

	/** @Column(type="string", nullable=true) * */
	protected $bank_branch_name;

	/** @Column(type="string", nullable=true) * */
	protected $bank_account_number;

	/** @Column(type="datetime") * */
	protected $created_at;

	/** @Column(type="integer", options={"default":0}, nullable=true) * */
	protected $orders = 0;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

	public function getCode()
	{
		return $this->code;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getAddress()
	{
		return $this->address;
	}

	public function getPhone()
	{
		return $this->phone;
	}

	public function getContact()
	{
		return $this->contact;
	}

	public function getBank_name()
	{
		return $this->bank_name;
	}

	public function getBank_branch_name()
	{
		return $this->bank_branch_name;
	}

	public function getBank_account_number()
	{
		return $this->bank_account_number;
	}

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

	public function getOrders()
	{
		return $this->orders;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setCode($code)
	{
		$this->code = $code;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setAddress($address)
	{
		$this->address = $address;
	}

	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	public function setContact($contact)
	{
		$this->contact = $contact;
	}


	public function setBank_name($bank_name)
	{
		$this->bank_name = $bank_name;
	}

	public function setBank_branch_name($bank_branch_name)
	{
		$this->bank_branch_name = $bank_branch_name;
	}

	public function setBank_account_number($bank_account_number)
	{
		$this->bank_account_number = $bank_account_number;
	}

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}

	public function setOrders($orders)
	{
		$this->orders = $orders;
	}
}