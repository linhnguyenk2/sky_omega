<?php
include_once 'BaseEntity.php';
// Entities/sih_product_in_package_in_check_form.php

/**
 * @Entity @Table(name="sih_product_in_package_in_check_form")
 **/
class Sih_product_in_package_in_check_form extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse_check_form")
     * @JoinColumn(name="warehouse_check_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_check_form_id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse_product_in_package")
     * @JoinColumn(name="warehouse_product_in_package_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_product_in_package_id;


    /** @Column(type="string", nullable=true) * */
    protected $current_quantity;

    /** @Column(type="string", nullable=true) * */
    protected $check_quantity;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getWarehouse_check_form_id()
    {
        return $this->warehouse_check_form_id;
    }

    public function getWarehouse_product_in_package_id()
    {
        return $this->warehouse_product_in_package_id;
    }

    public function getCurrent_quantity()
    {
        return $this->current_quantity;
    }

    public function getCheck_quantity()
    {
        return $this->check_quantity;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setWarehouse_check_form_id($warehouse_check_form_id)
    {
        $this->warehouse_check_form_id = $warehouse_check_form_id;
    }

    public function setWarehouse_product_in_package_id($warehouse_product_in_package_id)
    {
        $this->warehouse_product_in_package_id = $warehouse_product_in_package_id;
    }

    public function setCurrent_quantity($current_quantity)
    {
        $this->current_quantity = $current_quantity;
    }

    public function setCheck_quantity($check_quantity)
    {
        $this->check_quantity = $check_quantity;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
