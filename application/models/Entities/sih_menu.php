<?php
include_once 'BaseEntity.php';
// Entities/sih_menu.php

/**
 * @Entity @Table(name="sih_menu")
 **/
class Sih_menu extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=false) * */
	protected $title;

    /** @Column(type="string", nullable=false) * */
	protected $html_id;

	public function getId()
	{
		return $this->id;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function getHtml_id()
	{
		return $this->html_id;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function setHtml_id($html_id)
	{
		$this->html_id = $html_id;
	}
}