<?php
include_once 'BaseEntity.php';
// Entities/sih_product_unit_exchange.php
/**
 * @Entity @Table(name="sih_product_unit_exchange")
 * */

class Sih_product_unit_exchange extends BaseEntity {

	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) **/
	protected $exchange_quality;

    /**
     * @ManyToOne(targetEntity="sih_unit")
     * @JoinColumn(name="unit_exchange_id ", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $unit_exchange_id;

    /**
     * @ManyToOne(targetEntity="sih_product", inversedBy="list_unit_medicine")
     * @JoinColumn(name="product_id", referencedColumnName="id", onDelete="NO ACTION", nullable=true)
     */
	protected $product_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"default":0}, nullable=true) * */
	protected $orders = 0;

	public function getId() {
		return $this->id;
	}

	public function getUnit_exchange_id() {
		return $this->unit_exchange_id;
	}

	public function getExchange_quality() {
		return $this->exchange_quality;
	}

	public function getProduct_id() {
		return $this->product_id;
	}

	public function getCreated_at() {
		return $this->created_at;
	}

	public function getStat() {
		return $this->stat;
	}

	public function getOrders() {
		return $this->orders;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setExchange_quality($exchange_quality) {
		$this->exchange_quality = $exchange_quality;
	}

	public function setUnit_exchange_id($unit_exchange_id) {
		$this->unit_exchange_id = $unit_exchange_id;
	}

	public function setProduct_id($product_id) {
		$this->product_id = $product_id;
	}

	public function setCreated_at($created_at) {
		$this->created_at = $created_at;
	}

	public function setStat($stat) {
		$this->stat = $stat;
	}

	public function setOrders($orders) {
		$this->orders = $orders;
	}
}
