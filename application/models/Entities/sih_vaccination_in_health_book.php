<?php
include_once 'BaseEntity.php';
// Entities/sih_vaccination_in_health_book.php

/**
 * @Entity @Table(name="sih_vaccination_in_health_book")
 **/
class Sih_vaccination_in_health_book extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_health_book")
     * @JoinColumn(name="health_book_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $health_book_id;

    /** @Column(type="string", nullable=true) * */
    protected $injection_name;

    /** @Column(type="string", nullable=true) * */
    protected $injection_times;

    /** @Column(type="datetime", nullable=true) * */
    protected $injection_date;

    /** @Column(type="string", nullable=true) * */
    protected $injection_location;

    /** @Column(type="string", nullable=true) * */
    protected $note;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getPatient_id()
    {
        return $this->patient_id;
    }

    public function setPatient_id($patient_id)
    {
        $this->patient_id = $patient_id;
    }

    public function getHealth_book_id()
    {
        return $this->health_book_id;
    }

    public function setHealth_book_id($health_book_id)
    {
        $this->health_book_id = $health_book_id;
    }

    public function getInjection_name()
    {
        return $this->injection_name;
    }

    public function setInjection_name($injection_name)
    {
        $this->injection_name = $injection_name;
    }

    public function getInjection_times()
    {
        return $this->injection_times;
    }

    public function setInjection_times($injection_times)
    {
        $this->injection_times = $injection_times;
    }

    public function getInjection_date()
    {
        return $this->injection_date;
    }

    public function setInjection_date($injection_date)
    {
        $this->injection_date = $injection_date;
    }

    public function getInjection_location()
    {
        return $this->injection_location;
    }

    public function setInjection_location($injection_location)
    {
        $this->injection_location = $injection_location;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setNote($note)
    {
        $this->note = $note;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }
}

