<?php
include_once 'BaseEntity.php';
// Entities/sih_list_not_combine_medicine.php
/**
 * @Entity @Table(name="sih_list_not_combine_medicine")
 * */

class Sih_list_not_combine_medicine extends  BaseEntity {

	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/**
	 * @ManyToOne(targetEntity="sih_medicine_product", inversedBy="list_not_combine_medicine")
	 * @JoinColumn(name="origin_medicine_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $origin_medicine_id;

	/**
	 * @ManyToOne(targetEntity="sih_medicine_product")
	 * @JoinColumn(name="not_combine_medicine_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $not_combine_medicine_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"default":0}, nullable=true) * */
	protected $orders = 0;

	public function getId() {
		return $this->id;
	}

	public function getOrigin_medicine_id() {
		return $this->origin_medicine_id;
	}

	public function getNot_combine_medicine_id() {
		return $this->not_combine_medicine_id;
	}

	public function getCreated_at() {
		return $this->created_at;
	}

	public function getStat() {
		return $this->stat;
	}

	public function getOrders() {
		return $this->orders;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setOrigin_medicine_id($origin_medicine_id) {
		$this->origin_medicine_id = $origin_medicine_id;
	}

	public function setNot_combine_medicine_id($not_combine_medicine_id) {
		$this->not_combine_medicine_id = $not_combine_medicine_id;
	}

	public function setCreated_at($created_at) {
		$this->created_at = $created_at;
	}

	public function setStat($stat) {
		$this->stat = $stat;
	}

	public function setOrders($orders) {
		$this->orders = $orders;
	}
}
