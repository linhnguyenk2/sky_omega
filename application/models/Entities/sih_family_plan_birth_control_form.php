<?php
include_once 'BaseEntity.php';
// Entities/sih_family_plan_birth_control_form.php
/**
 * @Entity @Table(name="sih_family_plan_birth_control_form")
 * */
class Sih_family_plan_birth_control_form  extends BaseEntity {

	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $surgery;

    /** @Column(type="string", nullable=true) * */
    protected $anesthesia;

    /** @Column(type="string", nullable=true) * */
    protected $reexamination_reason;

    /** @Column(type="datetime", nullable=true) * */
    protected $date_perform;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	public function getId() {
		return $this->id;
	}

	public function getSurgery() {
		return $this->surgery;
	}

    public function getAnesthesia() {
        return $this->anesthesia;
    }

    public function getDate_perform() {
        return $this->date_perform;
    }

    public function getReexamination_reason() {
        return $this->reexamination_reason;
    }

	public function getCreated_at() {
		return $this->created_at;
	}

	public function getStat() {
		return $this->stat;
	}

	public function setId($id) {
		$this->id = $id;
	}

    public function setSurgery($surgery) {
        $this->surgery = $surgery;
    }

    public function setReexamination_reason($reexamination_reason) {
        $this->reexamination_reason = $reexamination_reason;
    }

    public function setDate_perform($date_perform) {
        $this->date_perform = $date_perform;
    }

    public function setAnesthesia($anesthesia) {
        $this->anesthesia = $anesthesia;
    }

    public function setCreated_at($created_at) {
		$this->created_at = $created_at;
	}

	public function setStat($stat) {
		$this->stat = $stat;
	}
}

