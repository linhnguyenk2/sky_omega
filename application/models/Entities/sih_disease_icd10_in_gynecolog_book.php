<?php
include_once 'BaseEntity.php';
// Entities/sih_disease_icd10_in_gynecolog_book.php

/**
 * @Entity @Table(name="sih_disease_icd10_in_gynecolog_book")
 **/
class Sih_disease_icd10_in_gynecolog_book extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/**
	 * @ManyToOne(targetEntity="sih_list_disease_icd10")
	 * @JoinColumn(name="disease_icd10_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $disease_icd10_id;

    /**
     * @ManyToOne(targetEntity="sih_gynecolog_book")
     * @JoinColumn(name="gynecolog_book_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $gynecolog_book_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"comment":"filter when show","default":0}) * */
	protected $orders = 0;

	function getId()
	{
		return $this->id;
	}

	function getDisease_icd10_id()
	{
		return $this->disease_icd10_id;
	}

	function getGynecolog_book_id()
	{
		return $this->gynecolog_book_id;
	}

	function getCreated_at()
	{
		return $this->created_at;
	}

	function getStat()
	{
		return $this->stat;
	}

	function getOrders()
	{
		return $this->orders;
	}

	function setDisease_icd10_id($disease_icd10_id)
	{
		$this->disease_icd10_id = $disease_icd10_id;
	}

	function setGynecolog_book_id($gynecolog_book_id)
	{
		$this->gynecolog_book_id = $gynecolog_book_id;
	}

	function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	function setStat($stat)
	{
		$this->stat = $stat;
	}

	function setOrders($orders)
	{
		$this->orders = $orders;
	}
}
