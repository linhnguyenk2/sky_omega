<?php
include_once 'BaseEntity.php';
// Entities/sih_list_group_icd10.php

/**
 * @Entity @Table(name="sih_list_group_icd10")
 **/
class Sih_list_group_icd10 extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $name;

	/** @Column(type="string", nullable=true) * */
	protected $code;

	/**
	 * @ManyToOne(targetEntity="sih_list_chapter_icd10")
	 * @JoinColumn(name="chapter_icd10_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $chapter_icd10_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"comment":"filter when show","default":0}) * */
	protected $orders = 0;

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getCode()
	{
		return $this->code;
	}

	public function getChapter_icd10_id()
	{
		return $this->chapter_icd10_id;
	}


	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

	public function getOrders()
	{
		return $this->orders;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setCode($code)
	{
		$this->code = $code;
	}

	public function setChapter_icd10_id($chapter_icd10_id)
	{
		$this->chapter_icd10_id = $chapter_icd10_id;
	}

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}

	public function setOrders($orders)
	{
		$this->orders = $orders;
	}


}