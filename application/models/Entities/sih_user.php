<?php
include_once 'BaseEntity.php';
// Entities/sih_user.php

/**
 * @Entity @Table(name="sih_user")
 **/
class Sih_user extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $code;

    /** @Column(type="string", nullable=true) * */
    protected $username;

    /** @Column(type="string", nullable=true) * */
    protected $password;

    /** @Column(type="string", nullable=true) * */
    protected $fullname;

    /** @Column(type="string", nullable=true) * */
    protected $gender;

    /** @Column(type="string", nullable=true) * */
    protected $is_married;

    /** @Column(type="string", nullable=true) * */
    protected $passport;

    /** @Column(type="string", nullable=true) * */
    protected $address;

    /** @Column(type="string", nullable=true) * */
    protected $email;

    /** @Column(type="string", nullable=true) * */
    protected $home_phone;

    /** @Column(type="string", nullable=true) * */
    protected $mobile;

    /** @Column(type="string", nullable=true) * */
    protected $birthday;

    /** @Column(type="string", nullable=true) * */
    protected $type;

    /** @Column(type="string", nullable=true) * */
    protected $blood_type;

    /** @Column(type="string", nullable=true) * */
    protected $sibling_order;

    /** @Column(type="string", nullable=true) * */
    protected $medical_history;

    /** @Column(type="string", nullable=true) * */
    protected $salt;

    /** @Column(type="string", nullable=true) * */
    protected $avatar;

    /** @Column(type="string", nullable=true) * */
    protected $temporary_address;

    /**
     * @ManyToOne(targetEntity="sih_list_provinces")
     * @JoinColumn(name="temporary_address_province_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $temporary_address_province_id;


    /**
     * @ManyToOne(targetEntity="sih_list_districts")
     * @JoinColumn(name="temporary_address_district_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $temporary_address_district_id;

    /**
     * @ManyToOne(targetEntity="sih_list_wards")
     * @JoinColumn(name="temporary_address_ward_id ", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $temporary_address_ward_id ;

    /**
     * @ManyToOne(targetEntity="sih_list_nations")
     * @JoinColumn(name="nationality_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $nationality_id;

    /**
     * @ManyToOne(targetEntity="sih_list_provinces")
     * @JoinColumn(name="province_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $province_id;

    /**
     * @ManyToOne(targetEntity="sih_list_districts")
     * @JoinColumn(name="district_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $district_id;

    /**
     * @ManyToOne(targetEntity="sih_list_wards")
     * @JoinColumn(name="ward_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $ward_id;

    /**
     * @ManyToOne(targetEntity="sih_list_titles")
     * @JoinColumn(name="title_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $title_id;

    /**
     * @ManyToOne(targetEntity="sih_list_folks")
     * @JoinColumn(name="folk_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $folk_id;

    /** @Column(type="datetime") **/
    protected $created_at;

    /** @Column(type="integer", options={"default":1}, nullable=true) * */
    protected $stat = 1;


    public function getId() {
        return $this->id;
    }

    public function getCode() {
        return $this->code;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getFullname() {
        return $this->fullname;
    }

    public function getGender() {
        return $this->gender;
    }

    public function getIs_married() {
        return $this->is_married;
    }

    public function getPassport() {
        return $this->passport;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getHome_phone() {
        return $this->home_phone;
    }

    public function getMobile() {
        return $this->mobile;
    }

    public function getBirthday() {
        return $this->birthday;
    }

    public function getType() {
        return $this->type;
    }

    public function getAvatar() {
        return $this->avatar;
    }

    public function getSalt() {
        return $this->salt;
    }

    public function getNationality_id() {
        return $this->nationality_id;
    }

    public function getProvince_id() {
        return $this->province_id;
    }

    public function getDistrict_id() {
        return $this->district_id;
    }

    public function getWard_id() {
        return $this->ward_id;
    }

    public function getTitle_id() {
        return $this->title_id;
    }

    public function getFolk_id() {
        return $this->folk_id;
    }

    public function getCreated_at() {
        return $this->created_at;
    }

    public function getStat() {
        return $this->stat;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setFullname($fullname) {
        $this->fullname = $fullname;
    }

    public function setGender($gender) {
        $this->gender = $gender;
    }

    public function setIs_married($is_married) {
        $this->is_married = $is_married;
    }

    public function setPassport($passport) {
        $this->passport = $passport;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setHome_phone($home_phone) {
        $this->home_phone = $home_phone;
    }

    public function setMobile($mobile) {
        $this->mobile = $mobile;
    }

    public function setBirthday($birthday) {
        $this->birthday = $birthday;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function setSalt($salt) {
        $this->salt = $salt;
    }

    public function setAvatar($avatar) {
        $this->avatar = $avatar;
    }

    public function setNationality_id($nationality_id) {
        $this->nationality_id = $nationality_id;
    }

    public function setProvince_id($province_id) {
        $this->province_id = $province_id;
    }

    public function setDistrict_id($district_id) {
        $this->district_id = $district_id;
    }
    public function setWard_id($ward_id) {
        $this->ward_id = $ward_id;
    }

    public function setTitle_id($title_id) {
        $this->title_id = $title_id;
    }

    public function setFolk_id($folk_id) {
        $this->folk_id = $folk_id;
    }

    public function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    public function setStat($stat) {
        $this->stat = $stat;
    }

    /**
     * @return mixed
     */
    public function getBlood_type()
    {
        return $this->blood_type;
    }

    /**
     * @param mixed $blood_type
     */
    public function setBlood_type($blood_type)
    {
        $this->blood_type = $blood_type;
    }

    /**
     * @return mixed
     */
    public function getTemporary_address()
    {
        return $this->temporary_address;
    }

    /**
     * @param mixed $temporary_address
     */
    public function setTemporary_address($temporary_address)
    {
        $this->temporary_address = $temporary_address;
    }

    /**
     * @return mixed
     */
    public function getTemporary_address_province_id()
    {
        return $this->temporary_address_province_id;
    }

    /**
     * @param mixed $temporary_address_province_id
     */
    public function setTemporary_address_province_id($temporary_address_province_id)
    {
        $this->temporary_address_province_id = $temporary_address_province_id;
    }

    /**
     * @return mixed
     */
    public function getTemporary_address_district_id()
    {
        return $this->temporary_address_district_id;
    }

    /**
     * @param mixed $temporary_address_district_id
     */
    public function setTemporary_address_district_id($temporary_address_district_id)
    {
        $this->temporary_address_district_id = $temporary_address_district_id;
    }

    /**
     * @return mixed
     */
    public function getTemporary_address_ward_id()
    {
        return $this->temporary_address_ward_id;
    }

    /**
     * @param mixed $temporary_address_ward_id
     */
    public function setTemporary_address_ward_id($temporary_address_ward_id)
    {
        $this->temporary_address_ward_id = $temporary_address_ward_id;
    }

    /**
     * @return mixed
     */
    public function getSibling_order()
    {
        return $this->sibling_order;
    }

    /**
     * @param mixed $sibling_order
     */
    public function setSibling_order($sibling_order)
    {
        $this->sibling_order = $sibling_order;
    }

    /**
     * @return mixed
     */
    public function getMedical_history()
    {
        return $this->medical_history;
    }

    /**
     * @param mixed $medical_history
     */
    public function setMedical_history($medical_history)
    {
        $this->medical_history = $medical_history;
    }
    
    
}

