<?php
include_once 'BaseEntity.php';
// Entities/sih_service.php
/**
 * @Entity @Table(name="sih_service")
 **/
class Sih_service extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    
	/** @Column(type="string", nullable=true) **/
    protected $code;

	/** @Column(type="string", nullable=true) **/
    protected $name;

    /**
     * @ManyToOne(targetEntity="sih_service_type")
     * @JoinColumn(name="service_type_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $service_type_id;

	/** @Column(type="string", nullable=true) **/
    protected $price_overtime;

	/** @Column(type="string", nullable=true) **/
    protected $price_in_hour;

	/** @Column(type="string", nullable=true) **/
    protected $price_foreigner;

	/** @Column(type="string", nullable=true) **/
    protected $price_holiday;
	
	/** @Column(type="string", nullable=true) **/
    protected $vat;

	/** @Column(type="string", nullable=true) **/
    protected $note;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}, nullable=true) * */
    protected $stat = 1;

    /** @Column(type="integer", options={"default":0}, nullable=true) * */
    protected $orders = 0;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getService_type_id()
    {
        return $this->service_type_id;
    }

    public function getPrice_overtime()
    {
        return $this->price_overtime;
    }

    public function getPrice_in_hour()
    {
        return $this->price_in_hour;
    }

    public function getPrice_holiday()
    {
        return $this->price_holiday;
    }

    public function getPrice_foreigner()
    {
        return $this->price_foreigner;
    }

    public function getVat()
    {
        return $this->vat;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function getOrders()
    {
        return $this->orders;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setService_type_id($service_type_id)
    {
        $this->service_type_id = $service_type_id;
    }

    public function setPrice_overtime($price_overtime)
    {
        $this->price_overtime = $price_overtime;
    }

    public function setPrice_in_hour($price_in_hour)
    {
        $this->price_in_hour = $price_in_hour;
    }

    public function setPrice_holiday($price_holiday)
    {
        $this->price_holiday = $price_holiday;
    }

    public function setPrice_foreigner($price_foreigner)
    {
        $this->price_foreigner = $price_foreigner;
    }

    public function setVat($vat)
    {
        $this->vat = $vat;
    }

    public function setNote($note)
    {
        $this->note = $note;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }

    public function setOrders($orders)
    {
        $this->orders = $orders;
    }
}