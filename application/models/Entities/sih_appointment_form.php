<?php
include_once 'BaseEntity.php';
// Entities/sih_appointment_form.php

/**
 * @Entity @Table(name="sih_appointment_form")
 * */
class Sih_appointment_form extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /** @Column(type="string", nullable=true) * */
    protected $phone;

    /** @Column(type="datetime", nullable=true) * */
    protected $date_appointment;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_id;

    /** @Column(type="string", nullable=true) * */
    protected $reason;


    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getPatient_id()
    {
        return $this->patient_id;
    }

    public function getDate_appointment()
    {
        return $this->date_appointment;
    }

    public function getReason()
    {
        return $this->reason;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function setPatient_id($patient_id)
    {
        $this->patient_id = $patient_id;
    }

    public function setDate_appointment($date_appointment)
    {
        $this->date_appointment = $date_appointment;
    }

    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }
}