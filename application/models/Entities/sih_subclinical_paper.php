<?php
include_once 'BaseEntity.php';
// Entities/sih_subclinical_paper.php

/**
 * @Entity @Table(name="sih_subclinical_paper")
 **/
class Sih_subclinical_paper extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /** @Column(type="string", nullable=true) * */
    protected $type;

    /** @Column(type="string", nullable=true) * */
    protected $image_path;

    /** @Column(type="string", nullable=true) * */
    protected $diagnose;

    /** @Column(type="string", nullable=true) * */
    protected $description;

    /** @Column(type="string", nullable=true) * */
    protected $conclusion;

    /** @Column(type="string", nullable=true) * */
    protected $management;

    /** @Column(type="string", nullable=true) * */
    protected $suggestion;

    /**
     * @ManyToOne(targetEntity="sih_specify_service_paper")
     * @JoinColumn(name="specify_service_paper_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $specify_service_paper_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="approval_staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $approval_staff_id;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_id;

    /**
     * @ManyToOne(targetEntity="sih_medical_examination_form")
     * @JoinColumn(name="medical_examination_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $medical_examination_form_id;

    /**
     * @ManyToOne(targetEntity="sih_subclinical_endoscopic_paper")
     * @JoinColumn(name="subclinical_endoscopic_paper_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $subclinical_endoscopic_paper_id;

    /**
     * @ManyToOne(targetEntity="sih_subclinical_xray_paper")
     * @JoinColumn(name="subclinical_xray_paper_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $subclinical_xray_paper_id;

    /**
     * @ManyToOne(targetEntity="sih_subclinical_xray_breast_paper")
     * @JoinColumn(name="subclinical_xray_breast_paper_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $subclinical_xray_breast_paper_id;

    /**
     * @ManyToOne(targetEntity="sih_subclinical_ultra_sound_3d")
     * @JoinColumn(name="subclinical_ultra_sound_3d_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $subclinical_ultra_sound_3d_id;

    /**
     * @ManyToOne(targetEntity="sih_subclinical_ultra_sound")
     * @JoinColumn(name="subclinical_ultra_sound_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $subclinical_ultra_sound_id;

    /** @Column(type="datetime", nullable=true) * */
    protected $date_action;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getImage_path()
    {
        return $this->image_path;
    }

    /**
     * @param mixed $image_path
     */
    public function setImage_path($image_path)
    {
        $this->image_path = $image_path;
    }

    /**
     * @return mixed
     */
    public function getDiagnose()
    {
        return $this->diagnose;
    }

    /**
     * @param mixed $diagnose
     */
    public function setDiagnose($diagnose)
    {
        $this->diagnose = $diagnose;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getConclusion()
    {
        return $this->conclusion;
    }

    /**
     * @param mixed $conclusion
     */
    public function setConclusion($conclusion)
    {
        $this->conclusion = $conclusion;
    }

    /**
     * @return mixed
     */
    public function getManagement()
    {
        return $this->management;
    }

    /**
     * @param mixed $management
     */
    public function setManagement($management)
    {
        $this->management = $management;
    }

    /**
     * @return mixed
     */
    public function getSuggestion()
    {
        return $this->suggestion;
    }

    /**
     * @param mixed $suggestion
     */
    public function setSuggestion($suggestion)
    {
        $this->suggestion = $suggestion;
    }

    /**
     * @return mixed
     */
    public function getSpecify_service_paper_id()
    {
        return $this->specify_service_paper_id;
    }

    /**
     * @param mixed $specify_service_paper_id
     */
    public function setSpecify_service_paper_id($specify_service_paper_id)
    {
        $this->specify_service_paper_id = $specify_service_paper_id;
    }

    /**
     * @return mixed
     */
    public function getApproval_staff_id()
    {
        return $this->approval_staff_id;
    }

    /**
     * @param mixed $approval_staff_id
     */
    public function setApproval_staff_id($approval_staff_id)
    {
        $this->approval_staff_id = $approval_staff_id;
    }

    /**
     * @return mixed
     */
    public function getPatient_id()
    {
        return $this->patient_id;
    }

    /**
     * @param mixed $patient_id
     */
    public function setPatient_id($patient_id)
    {
        $this->patient_id = $patient_id;
    }

    /**
     * @return mixed
     */
    public function getStaff_id()
    {
        return $this->staff_id;
    }

    /**
     * @param mixed $staff_id
     */
    public function setStaff_id($staff_id)
    {
        $this->staff_id = $staff_id;
    }

    /**
     * @return mixed
     */
    public function getMedical_examination_form_id()
    {
        return $this->medical_examination_form_id;
    }

    /**
     * @param mixed $medical_examination_form_id
     */
    public function setMedical_examination_form_id($medical_examination_form_id)
    {
        $this->medical_examination_form_id = $medical_examination_form_id;
    }

    /**
     * @return mixed
     */
    public function getSubclinical_xray_paper_id()
    {
        return $this->subclinical_xray_paper_id;
    }

    /**
     * @param mixed $subclinical_xray_paper_id
     */
    public function setSubclinical_xray_paper_id($subclinical_xray_paper_id)
    {
        $this->subclinical_xray_paper_id = $subclinical_xray_paper_id;
    }

    /**
     * @return mixed
     */
    public function getSubclinical_xray_breast_paper_id()
    {
        return $this->subclinical_xray_breast_paper_id;
    }

    /**
     * @param mixed $subclinical_xray_breast_paper_id
     */
    public function setSubclinical_xray_breast_paper_id($subclinical_xray_breast_paper_id)
    {
        $this->subclinical_xray_breast_paper_id = $subclinical_xray_breast_paper_id;
    }

    /**
     * @return mixed
     */
    public function getSubclinical_ultra_sound_3d_id()
    {
        return $this->subclinical_ultra_sound_3d_id;
    }

    /**
     * @param mixed $subclinical_ultra_sound_3d_id
     */
    public function setSubclinical_ultra_sound_3d_id($subclinical_ultra_sound_3d_id)
    {
        $this->subclinical_ultra_sound_3d_id = $subclinical_ultra_sound_3d_id;
    }

    /**
     * @return mixed
     */
    public function getSubclinical_ultra_sound_id()
    {
        return $this->subclinical_ultra_sound_id;
    }

    /**
     * @param mixed $subclinical_ultra_sound_id
     */
    public function setSubclinical_ultra_sound_id($subclinical_ultra_sound_id)
    {
        $this->subclinical_ultra_sound_id = $subclinical_ultra_sound_id;
    }

    /**
     * @return mixed
     */
    public function getDate_action()
    {
        return $this->date_action;
    }

    /**
     * @param mixed $date_action
     */
    public function setDate_action($date_action)
    {
        $this->date_action = $date_action;
    }

    /**
     * @return mixed
     */
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getStat()
    {
        return $this->stat;
    }

    /**
     * @param mixed $stat
     */
    public function setStat($stat)
    {
        $this->stat = $stat;
    }

    /**
     * @return mixed
     */
    public function getSubclinical_endoscopic_paper_id()
    {
        return $this->subclinical_endoscopic_paper_id;
    }

    /**
     * @param mixed $subclinical_endoscopic_paper_id
     */
    public function setSubclinical_endoscopic_paper_id($subclinical_endoscopic_paper_id)
    {
        $this->subclinical_endoscopic_paper_id = $subclinical_endoscopic_paper_id;
    }
}
