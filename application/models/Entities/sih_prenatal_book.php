<?php
include_once 'BaseEntity.php';
// Entities/sih_prenatal_book.php

/**
 * @Entity @Table(name="sih_prenatal_book")
 **/
class Sih_prenatal_book extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $code;

    /** @Column(type="string", nullable=true) * */
    protected $blood_group;

    /** @Column(type="string", nullable=true) * */
    protected $age_marriage;

    /** @Column(type="string", nullable=true) * */
    protected $para;

    /** @Column(type="datetime", nullable=true) * */
    protected $last_period;

    /** @Column(type="datetime", nullable=true) * */
    protected $expected_date_confinement;

    /** @Column(type="string", nullable=true) * */
    protected $age_first_menes;

    /** @Column(type="string", nullable=true) * */
    protected $menstrual_cycle;

    /** @Column(type="string", nullable=true) * */
    protected $menstrual_status;

    /** @Column(type="string", nullable=true) * */
    protected $duration_menses;

    /** @Column(type="string", nullable=true) * */
    protected $menstrual_flow;

    /** @Column(type="string", nullable=true) * */
    protected $bw;

    /** @Column(type="string", nullable=true) * */
    protected $hiv;

    /** @Column(type="string", nullable=true) * */
    protected $hbsag;

    /** @Column(type="string", nullable=true) * */
    protected $hct;

    /** @Column(type="string", nullable=true) * */
    protected $hemoglobin;

    /**
	 * @ManyToOne(targetEntity="sih_patients")
	 * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $patient_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

	public function getCode()
	{
		return $this->code;
	}

    public function getPatient_id()
    {
        return $this->patient_id;
    }

    public function getBlood_group()
    {
        return $this->blood_group;
    }

    public function getAge_marriage()
    {
        return $this->age_marriage;
    }

    public function getPara()
    {
        return $this->para;
    }

    public function getLast_period()
    {
        return $this->last_period;
    }

    public function getExpected_date_confinement()
    {
        return $this->expected_date_confinement;
    }

    public function getAge_first_menes()
    {
        return $this->age_first_menes;
    }

    public function getMenstrual_cycle()
    {
        return $this->menstrual_cycle;
    }

    public function getMenstrual_status()
    {
        return $this->menstrual_status;
    }

    public function getDuration_menses()
    {
        return $this->duration_menses;
    }

    public function getMenstrual_flow()
    {
        return $this->menstrual_flow;
    }

    public function getBw()
    {
        return $this->bw;
    }

    public function getHiv()
    {
        return $this->hiv;
    }

    public function getHbsag()
    {
        return $this->hbsag;
    }

    public function getHemoglobin()
    {
        return $this->hemoglobin;
    }

    public function getHct()
    {
        return $this->hct;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

	public function setCode($code)
	{
		$this->code = $code;
	}

    public function setPatient_id($patient_id)
    {
        $this->patient_id = $patient_id;
    }

    public function setBlood_group($blood_group)
    {
        $this->blood_group = $blood_group;
    }

    public function setAge_marriage($age_marriage)
    {
        $this->age_marriage = $age_marriage;
    }

    public function setPara($para)
    {
        $this->para = $para;
    }

    public function setLast_period($last_period)
    {
        $this->last_period = $last_period;
    }

    public function setExpected_date_confinement($expected_date_confinement)
    {
        $this->expected_date_confinement = $expected_date_confinement;
    }

    public function setAge_first_menes($age_first_menes)
    {
        $this->age_first_menes = $age_first_menes;
    }

    public function setMenstrual_cycle($menstrual_cycle)
    {
        $this->menstrual_cycle = $menstrual_cycle;
    }

    public function setMenstrual_status($menstrual_status)
    {
        $this->menstrual_status = $menstrual_status;
    }

    public function setDuration_menses($duration_menses)
    {
        $this->duration_menses = $duration_menses;
    }

    public function setMenstrual_flow($menstrual_flow)
    {
        $this->menstrual_flow = $menstrual_flow;
    }

    public function setBw($bw)
    {
        $this->bw = $bw;
    }

    public function setHiv($hiv)
    {
        $this->hiv = $hiv;
    }

    public function setHbsag($hbsag)
    {
        $this->hbsag = $hbsag;
    }

    public function setHemoglobin($hemoglobin)
    {
        $this->hemoglobin = $hemoglobin;
    }

    public function setHct($hct)
    {
        $this->hct = $hct;
    }

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}
}

