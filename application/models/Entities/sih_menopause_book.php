<?php
include_once 'BaseEntity.php';
// Entities/sih_menopause_book.php

/**
 * @Entity @Table(name="sih_menopause_book")
 **/
class Sih_menopause_book extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $code;

    /** @Column(type="string", nullable=true) * */
    protected $medical_history;

    /** @Column(type="string", nullable=true) * */
    protected $surgical_history;

    /** @Column(type="string", nullable=true) * */
    protected $obstetrical_history;

    /** @Column(type="string", nullable=true) * */
    protected $family_history;


    /**
	 * @ManyToOne(targetEntity="sih_patients")
	 * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $patient_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

	public function getCode()
	{
		return $this->code;
	}

    public function getMedical_history()
    {
        return $this->medical_history;
    }

    public function getSurgical_history()
    {
        return $this->surgical_history;
    }

    public function getObstetrical_history()
    {
        return $this->obstetrical_history;
    }

    public function getFamily_history()
    {
        return $this->family_history;
    }

    public function getPatient_id()
    {
        return $this->patient_id;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

	public function setCode($code)
	{
		$this->code = $code;
	}

    public function setMedical_history($medical_history)
    {
        $this->medical_history = $medical_history;
    }

    public function setSurgical_history($surgical_history)
    {
        $this->surgical_history = $surgical_history;
    }

    public function setObstetrical_history($obstetrical_history)
    {
        $this->obstetrical_history = $obstetrical_history;
    }

    public function setFamily_history($family_history)
    {
        $this->family_history = $family_history;
    }

    public function setPatient_id($family_history)
    {
        $this->patient_id = $family_history;
    }

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}
}

