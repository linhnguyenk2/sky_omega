<?php
include_once 'BaseEntity.php';
// Entities/sih_product.php

/**
 * @Entity @Table(name="sih_product")
 **/
class Sih_product extends  BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string", nullable=true) **/
    protected $code;

    /** @Column(type="string", nullable=true) **/
    protected $name;

    /** @Column(type="string", nullable=true) **/
    protected $price;

    /** @Column(type="string", nullable=true) **/
    protected $price_foreigner;

    /** @Column(type="string", nullable=true) **/
    protected $description;

    /** @Column(type="string", nullable=true) **/
    protected $note;

    /** @Column(type="string", nullable=true) **/
    protected $vat;

    /**
     * @ManyToOne(targetEntity="sih_product_group")
     * @JoinColumn(name="product_group_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $product_group_id;

    /**
     * @ManyToOne(targetEntity="sih_unit")
     * @JoinColumn(name="unit_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $unit_id;

    /**
     * @ManyToOne(targetEntity="sih_list_nations")
     * @JoinColumn(name="nation_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $nation_id;

    /**
     * @ManyToOne(targetEntity="sih_list_supplier")
     * @JoinColumn(name="supplier_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $supplier_id;

    /**
     * @ManyToOne(targetEntity="sih_medicine_product")
     * @JoinColumn(name="medicine_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $medicine_id;

    /**
     * @ManyToOne(targetEntity="sih_functional_food_product")
     * @JoinColumn(name="function_food_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $function_food_id;

    /**
     * @ManyToOne(targetEntity="sih_medical_supply")
     * @JoinColumn(name="medical_supply_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $medical_supply_id;

    /**
     * @ManyToOne(targetEntity="sih_chemical_product")
     * @JoinColumn(name="chemical_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $chemical_id;

    /**
     * @ManyToOne(targetEntity="sih_cosmetic_product")
     * @JoinColumn(name="cosmetic_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $cosmetic_id;

    /**
     * @ManyToOne(targetEntity="sih_raw_material_product")
     * @JoinColumn(name="raw_material_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $raw_material_id;

    /**
     * @ManyToOne(targetEntity="sih_food_product")
     * @JoinColumn(name="food_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $food_id;

    /**
     * One Sih_product has Many sih_product_unit_exchange
     * @OneToMany(targetEntity="sih_product_unit_exchange", mappedBy="product_id")
     */
    protected $list_product_unit_exchange;

    /** @Column(type="datetime") **/
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) **/
    protected $stat = 1;

    /** @Column(type="integer", options={"default":0}) **/
    protected $orders = 0;


    public function getId() {
        return $this->id;
    }

    public function getCode() {
        return $this->code;
    }

    public function getName() {
        return $this->name;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getPrice_foreigner() {
        return $this->price_foreigner;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getNote() {
        return $this->note;
    }

    public function getVat() {
        return $this->vat;
    }

    public function getNation_id() {
        return $this->nation_id;
    }

    public function getSupplier_id() {
        return $this->supplier_id;
    }

    public function getUnit_id() {
        return $this->unit_id;
    }

    public function getProduct_group_id() {
        return $this->product_group_id;
    }

    public function getMedicine_id() {
        return $this->medicine_id;
    }

    public function getFunction_food_id() {
        return $this->function_food_id;
    }

    public function getMedical_supply_id() {
        return $this->medical_supply_id;
    }

    public function getChemical_id() {
        return $this->chemical_id;
    }

    public function getCosmetic_id() {
        return $this->cosmetic_id;
    }

    public function getRaw_material_id() {
        return $this->raw_material_id;
    }

    public function getFood_id() {
        return $this->food_id;
    }

    public function getList_product_unit_exchange() {
        return $this->list_product_unit_exchange;
    }

    public function getCreated_at() {
        return $this->created_at;
    }

    public function getStat() {
        return $this->stat;
    }

    public function getOrders() {
        return $this->orders;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function setPrice_foreigner($price_foreigner) {
        $this->price_foreigner = $price_foreigner;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setNote($note) {
        $this->note = $note;
    }

    public function setVat($vat) {
        $this->vat = $vat;
    }

    public function setNation_id($nation_id) {
        $this->nation_id = $nation_id;
    }

    public function setSupplier_id($supplier_id) {
        $this->supplier_id = $supplier_id;
    }

    public function setUnit_id($unit_id) {
        $this->unit_id = $unit_id;
    }

    public function setProduct_group_id($product_group_id) {
        $this->product_group_id = $product_group_id;
    }

    public function setMedicine_id($medicine_id) {
        $this->medicine_id = $medicine_id;
    }

    public function setFunction_food_id($function_food_id) {
        $this->function_food_id = $function_food_id;
    }

    public function setChemical_id($chemical_id) {
        $this->chemical_id = $chemical_id;
    }

    public function setMedical_supply_id($medical_supply_id) {
        $this->medical_supply_id = $medical_supply_id;
    }

    public function setCosmetic_id($cosmetic_id) {
        $this->cosmetic_id = $cosmetic_id;
    }

    public function setRaw_material_id($raw_material_id) {
        $this->raw_material_id = $raw_material_id;
    }

    public function setFood_id($food_id) {
        $this->food_id = $food_id;
    }

    public function setList_product_unit_exchange($list_product_unit_exchange) {
        $this->list_product_unit_exchange = $list_product_unit_exchange;
    }

    public function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    public function setStat($stat) {
        $this->stat = $stat;
    }

    public function setOrders($orders) {
        $this->orders = $orders;
    }
}
