<?php
include_once 'BaseEntity.php';
// Entities/sih_prenatal_form.php
/**
 * @Entity @Table(name="sih_prenatal_form")
 * */
class Sih_prenatal_form  extends BaseEntity {

	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $code;

	/** @Column(type="string", nullable=true) * */
	protected $weight;

    /** @Column(type="string", nullable=true) * */
    protected $blood_pressure;

    /** @Column(type="string", nullable=true) * */
    protected $urine_albumin;

    /** @Column(type="string", nullable=true) * */
    protected $urine_glucose;

    /** @Column(type="string", nullable=true) * */
    protected $edema;

    /** @Column(type="string", nullable=true) * */
    protected $fh;

    /** @Column(type="string", nullable=true) * */
    protected $fhr;

    /** @Column(type="string", nullable=true) * */
    protected $presentation;

    /** @Column(type="string", nullable=true) * */
    protected $remarks;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	public function getId() {
		return $this->id;
	}

	public function getCode() {
		return $this->code;
	}

	public function getWeight() {
		return $this->weight;
	}

    public function getBlood_pressure() {
        return $this->blood_pressure;
    }

    public function getUrine_albumin() {
        return $this->urine_albumin;
    }
    public function getUrine_glucose() {
        return $this->urine_glucose;
    }
    public function getEdema() {
        return $this->edema;
    }
    public function getFh() {
        return $this->fh;
    }

    public function getFhr() {
        return $this->fhr;
    }

    public function getPresentation() {
        return $this->presentation;
    }

    public function getRemarks() {
        return $this->remarks;
    }

	public function getCreated_at() {
		return $this->created_at;
	}

	public function getStat() {
		return $this->stat;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setCode($code) {
		$this->code = $code;
	}

    public function setWeight($weight) {
        $this->weight = $weight;
    }

    public function setBlood_pressure($blood_pressure) {
        $this->blood_pressure = $blood_pressure;
    }

    public function setUrine_albumin($urine_albumin) {
        $this->urine_albumin = $urine_albumin;
    }
    
    public function setUrine_glucose($urine_glucose) {
        $this->urine_glucose = $urine_glucose;
    }
    public function setEdema($edema) {
        $this->edema = $edema;
    }
    public function setFh($fh) {
        $this->fh = $fh;
    }

    public function setFhr($fhr) {
        $this->fhr = $fhr;
    }

    public function setPresentation($presentation) {
        $this->presentation = $presentation;
    }

    public function setRemarks($remarks) {
        $this->remarks = $remarks;
    }

	public function setCreated_at($created_at) {
		$this->created_at = $created_at;
	}

	public function setStat($stat) {
		$this->stat = $stat;
	}
}

