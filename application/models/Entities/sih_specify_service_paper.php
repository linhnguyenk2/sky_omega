<?php
include_once 'BaseEntity.php';
// Entities/sih_specify_service_paper.php

/**
 * @Entity @Table(name="sih_specify_service_paper")
 * */
class Sih_specify_service_paper extends BaseEntity
{

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /** @Column(type="string", nullable=true) * */
    protected $reason;

    /** @Column(type="string", nullable=true) * */
    protected $quantity;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_service")
     * @JoinColumn(name="service_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $service_id;

    /**
     * @ManyToOne(targetEntity="sih_service_location")
     * @JoinColumn(name="service_location_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $service_location_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_id;

    /**
     * @ManyToOne(targetEntity="sih_medical_examination_form")
     * @JoinColumn(name="medical_examination_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $medical_examination_form_id;

    /** @Column(type="string", nullable=true) * */
    protected $price_type;

    /** @Column(type="datetime", nullable=true) * */
    protected $appointment_time;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getReason()
    {
        return $this->reason;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getAppointment_time()
    {
        return $this->appointment_time;
    }

    public function getService_id()
    {
        return $this->service_id;
    }

    public function getService_location_id()
    {
        return $this->service_location_id;
    }

    public function getPatient_id()
    {
        return $this->patient_id;
    }

    public function getStaff_id()
    {
        return $this->staff_id;
    }

    public function getMedical_examination_form_id()
    {
        return $this->medical_examination_form_id;
    }

    public function getPrice_type()
    {
        return $this->price_type;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setAppointment_time($appointment_time)
    {
        $this->appointment_time = $appointment_time;
    }

    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setService_id($service_id)
    {
        $this->service_id = $service_id;
    }

    public function setService_location_id($service_location_id)
    {
        $this->service_location_id = $service_location_id;
    }

    public function setPatient_id($patient_id)
    {
        $this->patient_id = $patient_id;
    }

    public function setStaff_id($staff_id)
    {
        $this->staff_id = $staff_id;
    }

    public function setMedical_examination_form_id($medical_examination_form_id)
    {
        $this->medical_examination_form_id = $medical_examination_form_id;
    }

    public function setPrice_type($price_type)
    {
        $this->price_type = $price_type;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }
}

