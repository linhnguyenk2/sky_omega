<?php
include_once 'BaseEntity.php';
// Entities/sih_patient_company.php
/**
 * @Entity @Table(name="sih_patient_company")
 **/
class Sih_patient_company extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string", nullable=true) **/
    protected $tax_code;

    /** @Column(type="string", nullable=true) **/
    protected $name;

    /** @Column(type="string", nullable=true) **/
    protected $address;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_list_provinces")
     * @JoinColumn(name="province_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $province_id;

    /**
     * @ManyToOne(targetEntity="sih_list_districts")
     * @JoinColumn(name="district_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $district_id;

    /**
     * @ManyToOne(targetEntity="sih_list_wards")
     * @JoinColumn(name="ward_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $ward_id;

    /** @Column(type="string", nullable=true) **/
    protected $email;

    /** @Column(type="string", nullable=true) **/
    protected $phone_1;

    /** @Column(type="string", nullable=true) **/
    protected $phone_2;

    /** @Column(type="datetime") **/
    protected $created_at;

    /** @Column(type="integer", options={"default":1}, nullable=true) * */
    protected $stat = 1;

    /** @Column(type="integer", options={"default":0}, nullable=true) * */
    protected $orders = 0;


    public function getId() {
        return $this->id;
    }

    public function getTax_code() {
        return $this->tax_code;
    }

    public function getName() {
        return $this->name;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getPatient_id() {
        return $this->patient_id;
    }

    public function getDistrict_id() {
        return $this->district_id;
    }

    public function getWard_id() {
        return $this->ward_id;
    }

    public function getProvince_id() {
        return $this->province_id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPhone_1() {
        return $this->phone_1;
    }

    public function getPhone_2() {
        return $this->phone_2;
    }

    public function getCreated_at() {
        return $this->created_at;
    }

    public function getStat() {
        return $this->stat;
    }

    public function getOrders() {
        return $this->orders;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setTax_code($tax_code) {
        $this->tax_code = $tax_code;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function setPatient_id($patient_id) {
        $this->patient_id = $patient_id;
    }

    public function setDistrict_id($district_id) {
        $this->district_id = $district_id;
    }

    public function setWard_id($ward_id) {
        $this->ward_id = $ward_id;
    }

    public function setProvince_id($province_id) {
        $this->province_id = $province_id;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
    public function setPhone_1($phone_1) {
        $this->phone_1 = $phone_1;
    }

    public function setPhone_2($phone_2) {
        $this->phone_2 = $phone_2;
    }

    public function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    public function setStat($stat) {
        $this->stat = $stat;
    }

    public function setOrders($orders) {
        $this->orders = $orders;
    }
}