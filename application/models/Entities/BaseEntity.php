<?php

class BaseEntity
{
    public function buildListProperties()
    {
        return get_object_vars($this);
    }
    
    public function isHaveCodeField()
    {
        $listField = get_object_vars($this);
        return array_key_exists("code", $listField);
    }
    
    public function isValidProperty($propertyName)
    {
        $listField = get_object_vars($this);
        return array_key_exists($propertyName, $listField);
    }
    
    private function buildListGetMethod()
    {
        $listmethodget = get_class_methods($this);
        $listmehod = array();

        if ($listmethodget)
        {
            foreach ($listmethodget as $key => $value) {
                if (substr($value, 0, 3) == 'get') {
                    $listmehod[] = $value;
                }
            }
        }

        return $listmehod;
    }
    
    public function buildObject($listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $listGetMethod = $this->buildListGetMethod();
        
        $result = new \stdClass();
        
        foreach ($listGetMethod as $getMethod) {
            $valueOfGetMethod = $this->$getMethod();
            $nameOfAtribute = strtolower($this->_getAtributeNameFromGetMethod($getMethod));
            
            if (is_object($valueOfGetMethod)) {
                if (get_class($valueOfGetMethod) == 'DateTime') {
                    $data = $valueOfGetMethod->format('Y-m-d H:i:s');
                    $result->$nameOfAtribute = (string) $data;
                }
                else if (get_class($valueOfGetMethod) == 'Doctrine\ORM\PersistentCollection') {
                    $data = $valueOfGetMethod->toArray();
                    $arr = array();
                    
                    // get Sub list - view API_L_41
                    foreach ($data as $obj)
                    {
                        $result2 = new \stdClass();
                        $listGetMethod2 = $obj->buildListGetMethod();
                        
                        foreach ($listGetMethod2 as $getMethod2) {
                            $valueOfGetMethod2 = $obj->$getMethod2();
                            $nameOfAtribute2 = strtolower($obj->_getAtributeNameFromGetMethod($getMethod2));
                            if (is_object($valueOfGetMethod2)) {
                                if (get_class($valueOfGetMethod2) == 'DateTime') {
                                    $data2 = $valueOfGetMethod2->format('Y-m-d H:i:s');
                                    $result2->$nameOfAtribute2 = (string) $data2;
                                }
                                else {
                                    // array('list_patient_emergency_contact', 'patient_id')
                                    if (!in_array($nameOfAtribute2, $listReferredColumn))
                                    {
                                        if (in_array($nameOfAtribute2, $listSelfReferredColumn))
                                        {
                                            $listReferredColumn = array_merge($listReferredColumn, $listSelfReferredColumn);
                                            $result2->$nameOfAtribute2 = $valueOfGetMethod2->buildObject($listReferredColumn, $listSelfReferredColumn);
                                        }
                                        else
                                        {
                                          $result2->$nameOfAtribute2 = $valueOfGetMethod2->buildObject($listReferredColumn, $listSelfReferredColumn);
                                        }
                                    }
                                    else {
                                        $result2->$nameOfAtribute2 = $valueOfGetMethod2->getID();
                                    }
                                }
                            }
                            else {
                                $result2->$nameOfAtribute2 = (string) $valueOfGetMethod2;
                            }
                        }
                        
                        $arr[] = $result2;
                    }
                    
                    $result->$nameOfAtribute = $arr;
                } else {
                    $objectOfGetMethod = $valueOfGetMethod->buildObject($listReferredColumn, $listSelfReferredColumn);
                    $result->$nameOfAtribute = $objectOfGetMethod;
                }
            } else {
                $result->$nameOfAtribute = (string) $valueOfGetMethod;
            }
        }
        
        return $result;
    }
    
    private function _getAtributeNameFromGetMethod($getMethodName)
    {
        return substr($getMethodName, 3, strlen($getMethodName));
    }
}