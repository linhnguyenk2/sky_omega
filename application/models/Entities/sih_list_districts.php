<?php
include_once 'BaseEntity.php';
// Entities/sih_list_districts.php

/**
 * @Entity @Table(name="sih_list_districts")
 **/
class Sih_list_districts extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $name;

	/** @Column(type="string", nullable=true) * */
	protected $code;


	/**
	 * @ManyToOne(targetEntity="sih_list_provinces")
	 * @JoinColumn(name="id_provinces", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $id_provinces;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"comment":"filter when show","default":0}) * */
	protected $orders = 0;

	function getId()
	{
		return $this->id;
	}

	function getName()
	{
		return $this->name;
	}

	function getCode()
	{
		return $this->code;
	}

	function getId_provinces()
	{
		return $this->id_provinces;
	}


	function getCreated_at()
	{
		return $this->created_at;
	}

	function getStat()
	{
		return $this->stat;
	}

	function getOrders()
	{
		return $this->orders;
	}

	function setName($name)
	{
		$this->name = $name;
	}

	function setCode($code)
	{
		$this->code = $code;
	}

	function setId_provinces($id_provinces)
	{
		$this->id_provinces = $id_provinces;
	}

	function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	function setStat($stat)
	{
		$this->stat = $stat;
	}

	function setOrders($orders)
	{
		$this->orders = $orders;
	}


}