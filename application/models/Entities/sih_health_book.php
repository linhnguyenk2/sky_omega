<?php
include_once 'BaseEntity.php';
// Entities/sih_health_book.php

/**
 * @Entity @Table(name="sih_health_book")
 **/
class Sih_health_book extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_id;

    /** @Column(type="string", nullable=true) * */
    protected $borning_health_facility;

    /** @Column(type="string", nullable=true) * */
    protected $weight;

    /** @Column(type="string", nullable=true) * */
    protected $height;

    /** @Column(type="string", nullable=true) * */
    protected $head_circumference;

    /** @Column(type="string", nullable=true) * */
    protected $borning_week_number;

    /** @Column(type="integer", nullable=true) * */
    protected $full_term;

    /** @Column(type="string", nullable=true) * */
    protected $one_min_apgar;

    /** @Column(type="string", nullable=true) * */
    protected $five_min_apgar;

    /** @Column(type="integer", nullable=true) * */
    protected $innate_malformation;

    /** @Column(type="string", nullable=true) * */
    protected $natural_childbirth;

    /** @Column(type="string", nullable=true) * */
    protected $reason_meddle_childbirth;

    /** @Column(type="string", nullable=true) * */
    protected $breastfeeding_winthin_one_hour;

    /** @Column(type="string", nullable=true) * */
    protected $diagnostic;

    /** @Column(type="string", nullable=true) * */
    protected $solution;

    /** @Column(type="string", nullable=true) * */
    protected $note;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getPatient_id()
    {
        return $this->patient_id;
    }

    public function setPatient_id($patient_id)
    {
        $this->patient_id = $patient_id;
    }

    public function getBorning_health_facility()
    {
        return $this->borning_health_facility;
    }

    public function setBorning_health_facility($borning_health_facility)
    {
        $this->borning_health_facility = $borning_health_facility;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getHead_circumference()
    {
        return $this->head_circumference;
    }

    public function setHead_circumference($head_circumference)
    {
        $this->head_circumference = $head_circumference;
    }

    public function getBorning_week_number()
    {
        return $this->borning_week_number;
    }

    public function setBorning_week_number($borning_week_number)
    {
        $this->borning_week_number = $borning_week_number;
    }

    public function getFull_term()
    {
        return $this->full_term;
    }

    public function setFull_term($full_term)
    {
        $this->full_term = $full_term;
    }

    public function getOne_min_apgar()
    {
        return $this->one_min_apgar;
    }

    public function setOne_min_apgar($one_min_apgar)
    {
        $this->one_min_apgar = $one_min_apgar;
    }

    public function getFive_min_apgar()
    {
        return $this->five_min_apgar;
    }

    public function setFive_min_apgar($five_min_apgar)
    {
        $this->five_min_apgar = $five_min_apgar;
    }

    public function getInnate_malformation()
    {
        return $this->innate_malformation;
    }

    public function setInnate_malformation($innate_malformation)
    {
        $this->innate_malformation = $innate_malformation;
    }

    public function getNatural_childbirth()
    {
        return $this->natural_childbirth;
    }

    public function setNatural_childbirth($natural_childbirth)
    {
        $this->natural_childbirth = $natural_childbirth;
    }

    public function getReason_meddle_childbirth()
    {
        return $this->reason_meddle_childbirth;
    }

    public function setReason_meddle_childbirth($reason_meddle_childbirth)
    {
        $this->reason_meddle_childbirth = $reason_meddle_childbirth;
    }

    public function getBreastfeeding_winthin_one_hour()
    {
        return $this->breastfeeding_winthin_one_hour;
    }

    public function setBreastfeeding_winthin_one_hour($breastfeeding_winthin_one_hour)
    {
        $this->breastfeeding_winthin_one_hour = $breastfeeding_winthin_one_hour;
    }

    public function getDiagnostic()
    {
        return $this->diagnostic;
    }

    public function setDiagnostic($diagnostic)
    {
        $this->diagnostic = $diagnostic;
    }

    public function getSolution()
    {
        return $this->solution;
    }

    public function setSolution($solution)
    {
        $this->solution = $solution;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setNote($note)
    {
        $this->note = $note;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }
}

