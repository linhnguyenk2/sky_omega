<?php
include_once 'BaseEntity.php';
// Entities/sih_list_patient_room.php

/**
 * @Entity @Table(name="sih_list_patient_room")
 **/
class Sih_list_patient_room extends  BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $name;

	/** @Column(type="string", nullable=true) * */
	protected $code;

	/**
	 * @ManyToOne(targetEntity="sih_list_patient_room_type")
	 * @JoinColumn(name="patient_room_type_id", referencedColumnName="id", onDelete="NO ACTION")
	 */
	protected $patient_room_type_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"comment":"filter when show","default":0}) * */
	protected $orders = 0;

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getCode()
	{
		return $this->code;
	}

	public function getPatient_room_type_id()
	{
		return $this->patient_room_type_id;
	}

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

	public function getOrders()
	{
		return $this->orders;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setCode($code)
	{
		$this->code = $code;
	}

	public function setPatient_room_type_id($patient_room_type_id)
	{
		$this->patient_room_type_id = $patient_room_type_id;
	}

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}

	public function setOrders($orders)
	{
		$this->orders = $orders;
	}


}