<?php
include_once 'BaseEntity.php';
// Entities/sih_warehouse_request_form.php

/**
 * @Entity @Table(name="sih_warehouse_request_form")
 **/
class Sih_warehouse_request_form extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="request_staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $request_staff_id;

    /** @Column(type="string", nullable=true) * */
    protected $reason;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getCode()
    {
        return $this->code;
    }

    public function getStaff_id()
    {
        return $this->staff_id;
    }

    public function getRequest_staff_id()
    {
        return $this->request_staff_id;
    }

    public function getReason()
    {
        return $this->reason;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setStaff_id($staff_id)
    {
        $this->staff_id = $staff_id;
    }

    public function setRequest_staff_id($request_staff_id)
    {
        $this->request_staff_id = $request_staff_id;
    }

    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
