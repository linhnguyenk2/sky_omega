<?php
include_once 'BaseEntity.php';
// Entities/sih_menopause_form.php
/**
 * @Entity @Table(name="sih_menopause_form")
 * */
class Sih_menopause_form  extends BaseEntity {

	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $code;

    /** @Column(type="datetime", nullable=true) * */
    protected $firstday_of_menses;

    /** @Column(type="datetime", nullable=true) * */
    protected $lastday_of_menses;

    /** @Column(type="datetime", nullable=true) * */
    protected $month_watch;

    /** @Column(type="string", nullable=true) * */
    protected $irritability;

    /** @Column(type="string", nullable=true) * */
    protected $palpitation;

    /** @Column(type="string", nullable=true) * */
    protected $poor_concerntration;

    /** @Column(type="string", nullable=true) * */
    protected $hot_flushes;

    /** @Column(type="string", nullable=true) * */
    protected $headache;

    /** @Column(type="string", nullable=true) * */
    protected $night_sweat;

    /** @Column(type="string", nullable=true) * */
    protected $insomnia;

    /** @Column(type="string", nullable=true) * */
    protected $dry_skin;

    /** @Column(type="string", nullable=true) * */
    protected $backache;

    /** @Column(type="string", nullable=true) * */
    protected $joint_pains;

    /** @Column(type="string", nullable=true) * */
    protected $vaginal_dryness;

    /** @Column(type="string", nullable=true) * */
    protected $smoking;

    /** @Column(type="string", nullable=true) * */
    protected $drinking;

    /** @Column(type="string", nullable=true) * */
    protected $gymmastics;

    /** @Column(type="string", nullable=true) * */
    protected $milk_drinking;

    /** @Column(type="string", nullable=true) * */
    protected $seafood_eating;

    /** @Column(type="string", nullable=true) * */
    protected $urinations_day;

    /** @Column(type="string", nullable=true) * */
    protected $sex_month;

    /** @Column(type="string", nullable=true) * */
    protected $depression;

    /** @Column(type="datetime", nullable=true) * */
    protected $last_menstruation;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	public function getId() {
		return $this->id;
	}

	public function getCode() {
		return $this->code;
	}

    public function getFirstday_of_menses() {
        return $this->firstday_of_menses;
    }

    public function getLastday_of_menses() {
        return $this->lastday_of_menses;
    }

    public function getDepression() {
        return $this->depression;
    }

    public function getUrinations_day() {
        return $this->urinations_day;
    }

    public function getSex_month() {
        return $this->sex_month;
    }

    public function getMonth_watch() {
        return $this->month_watch;
    }

    public function getIrritability() {
        return $this->irritability;
    }

    public function getPalpitation() {
        return $this->palpitation;
    }

    public function getPoor_concerntration() {
        return $this->poor_concerntration;
    }

    public function getHot_flushes() {
        return $this->hot_flushes;
    }

    public function getHeadache() {
        return $this->headache;
    }

    public function getNight_sweat() {
        return $this->night_sweat;
    }

    public function getInsomnia() {
        return $this->insomnia;
    }

    public function getDry_skin() {
        return $this->dry_skin;
    }

    public function getBackache() {
        return $this->backache;
    }

    public function getJoint_pains() {
        return $this->joint_pains;
    }

    public function getVaginal_dryness() {
        return $this->vaginal_dryness;
    }

    public function getSmoking() {
        return $this->smoking;
    }

    public function getDrinking() {
        return $this->drinking;
    }

    public function getGymmastics() {
        return $this->gymmastics;
    }

    public function getMilk_drinking() {
        return $this->milk_drinking;
    }

    public function getSeafood_eating() {
        return $this->seafood_eating;
    }

    public function getLast_menstruation() {
        return $this->last_menstruation;
    }

	public function getCreated_at() {
		return $this->created_at;
	}

	public function getStat() {
		return $this->stat;
	}

	public function setId($id) {
		$this->id = $id;
	}

    public function setCode($code) {
        $this->code = $code;
    }

    public function setFirstday_of_menses($firstday_of_menses) {
        $this->firstday_of_menses = $firstday_of_menses;
    }

    public function setLastday_of_menses($lastday_of_menses) {
        $this->lastday_of_menses = $lastday_of_menses;
    }

    public function setDepression($depression) {
        $this->depression = $depression;
    }

    public function setMonth_watch($month_watch) {
        $this->month_watch = $month_watch;
    }

    public function setIrritability($irritability) {
        $this->irritability = $irritability;
    }

    public function setPalpitation($palpitation) {
        $this->palpitation = $palpitation;
    }

    public function setPoor_concerntration($poor_concerntration) {
        $this->poor_concerntration = $poor_concerntration;
    }

    public function setHot_flushes($hot_flushes) {
        $this->hot_flushes = $hot_flushes;
    }

    public function setHeadache($headache) {
        $this->headache = $headache;
    }

    public function setNight_sweat($night_sweat) {
        $this->night_sweat = $night_sweat;
    }

    public function setInsomnia($insomnia) {
        $this->insomnia = $insomnia;
    }

    public function setDry_skin($dry_skin) {
        $this->dry_skin = $dry_skin;
    }

    public function setBackache($backache) {
        $this->backache = $backache;
    }

    public function setJoint_pains($joint_pains) {
        $this->joint_pains = $joint_pains;
    }

    public function setVaginal_dryness($vaginal_dryness) {
        $this->vaginal_dryness = $vaginal_dryness;
    }

    public function setSmoking($smoking) {
        $this->smoking = $smoking;
    }
    
    public function setDrinking($drinking) {
        $this->drinking = $drinking;
    }

    public function setGymmastics($gymmastics) {
        $this->gymmastics = $gymmastics;
    }

    public function setMilk_drinking($milk_drinking) {
        $this->milk_drinking = $milk_drinking;
    }

    public function setSeafood_eating($seafood_eating) {
        $this->seafood_eating = $seafood_eating;
    }

    public function setUrinations_day($urinations_day) {
        $this->urinations_day = $urinations_day;
    }

    public function setSex_month($sex_month) {
        $this->sex_month = $sex_month;
    }

    public function setLast_menstruation($last_menstruation) {
        $this->last_menstruation = $last_menstruation;
    }

	public function setCreated_at($created_at) {
		$this->created_at = $created_at;
	}

	public function setStat($stat) {
		$this->stat = $stat;
	}
}

