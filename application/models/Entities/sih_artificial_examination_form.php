<?php
include_once 'BaseEntity.php';
// Entities/sih_artificial_examination_form.php
/**
 * @Entity @Table(name="sih_artificial_examination_form")
 * */
class Sih_artificial_examination_form  extends BaseEntity {

	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="string", nullable=true) * */
	protected $physical_exam;

    /** @Column(type="string", nullable=true) * */
    protected $laboratory_test;

    /** @Column(type="string", nullable=true) * */
    protected $instructions;

    /** @Column(type="string", nullable=true) * */
    protected $specified;

    /** @Column(type="string", nullable=true) * */
    protected $reexamination_reason;

    /** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	public function getId() {
		return $this->id;
	}

	public function getPhysical_exam() {
		return $this->physical_exam;
	}

    public function getLaboratory_test() {
        return $this->laboratory_test;
    }

    public function getInstructions() {
        return $this->instructions;
    }

    public function getSpecified() {
        return $this->specified;
    }

    public function getReexamination_reason() {
        return $this->reexamination_reason;
    }

	public function getCreated_at() {
		return $this->created_at;
	}

	public function getStat() {
		return $this->stat;
	}

	public function setId($id) {
		$this->id = $id;
	}

    public function setPhysical_exam($physical_exam) {
        $this->physical_exam = $physical_exam;
    }

    public function setLaboratory_test($laboratory_test) {
        $this->laboratory_test = $laboratory_test;
    }

    public function setInstructions($instructions) {
        $this->instructions = $instructions;
    }

    public function setSpecified($specified) {
        $this->specified = $specified;
    }

    public function setReexamination_reason($reexamination_reason) {
        $this->reexamination_reason = $reexamination_reason;
    }

    public function setCreated_at($created_at) {
		$this->created_at = $created_at;
	}

	public function setStat($stat) {
		$this->stat = $stat;
	}
}

