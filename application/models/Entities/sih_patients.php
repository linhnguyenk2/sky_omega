<?php
include_once 'BaseEntity.php';
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
// Entities/sih_patients.php
/**
 * @Entity @Table(name="sih_patients")
 **/
class Sih_patients extends BaseEntity
{
    public function __construct() {
        $this->list_patient_emergency_contact = new ArrayCollection();
    }

    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /**
     * @ManyToOne(targetEntity="sih_user")
     * @JoinColumn(name="user_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $user_id;

    /**
     * One Sih_patients has Many Sih_patient_emergency_contact.
     * @OneToMany(targetEntity="sih_patient_emergency_contact", mappedBy="patient_id")
     */
    protected $list_patient_emergency_contact;

    /** @Column(type="datetime") **/
    protected $created_at;

    /** @Column(type="integer", options={"default":1}, nullable=true) * */
    protected $stat = 1;


    public function getId() {
        return $this->id;
    }

    public function getUser_id() {
        return $this->user_id;
    }

    public function getList_patient_emergency_contact() {
        return $this->list_patient_emergency_contact;
    }

    public function getCreated_at() {
        return $this->created_at;
    }

    public function getStat() {
        return $this->stat;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUser_id($user_id) {
        $this->user_id = $user_id;
    }

    public function setList_patient_emergency_contact(ArrayCollection $list_patient_emergency_contact) {
        $this->list_patient_emergency_contact = $list_patient_emergency_contact;
    }

    public function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    public function setStat($stat) {
        $this->stat = $stat;
    }

    public function setOrders($orders) {
        $this->orders = $orders;
    }
}