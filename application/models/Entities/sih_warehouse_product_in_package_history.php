<?php
include_once 'BaseEntity.php';
// Entities/sih_warehouse_product_in_package_history.php

/**
 * @Entity @Table(name="sih_warehouse_product_in_package_history")
 **/
class Sih_warehouse_product_in_package_history extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse_product_in_package")
     * @JoinColumn(name="warehouse_product_in_package_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_product_in_package_id;

    /** @Column(type="string", nullable=true) * */
    protected $quantity_before;

    /** @Column(type="string", nullable=true) * */
    protected $quantity_after;

    /** @Column(type="string", nullable=true) * */
    protected $reason_type;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getWarehouse_product_in_package_id()
    {
        return $this->warehouse_product_in_package_id;
    }

    public function getQuantity_before()
    {
        return $this->quantity_before;
    }

    public function getQuantity_after()
    {
        return $this->quantity_after;
    }

    public function getReason_type()
    {
        return $this->reason_type;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setWarehouse_product_in_package_id($warehouse_product_in_package_id)
    {
        $this->warehouse_product_in_package_id = $warehouse_product_in_package_id;
    }

    public function setQuantity_before($quantity_before)
    {
        $this->quantity_before = $quantity_before;
    }

    public function setQuantity_after($quantity_after)
    {
        $this->quantity_after = $quantity_after;
    }

    public function setReason_type($reason_type)
    {
        $this->reason_type = $reason_type;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }
}
