<?php
include_once 'BaseEntity.php';
// Entities/sih_subclinical_amniocentesis_cell_through_needle.php

/**
 * @Entity @Table(name="sih_subclinical_amniocentesis_cell_through_needle")
 **/
class Sih_subclinical_amniocentesis_cell_through_needle extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_pathologist_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_pathologist_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_doctor_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_doctor_id;

    /** @Column(type="datetime", nullable=true) * */
    protected $surgery_date;

    /** @Column(type="string", nullable=true) * */
    protected $history;

    /** @Column(type="string", nullable=true) * */
    protected $fna;

    /** @Column(type="string", nullable=true) * */
    protected $kkc;

    /** @Column(type="string", nullable=true) * */
    protected $subclinical;

    /** @Column(type="string", nullable=true) * */
    protected $diagnostic_subclinical;

    /** @Column(type="string", nullable=true) * */
    protected $template_reason_path;


    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getStaff_pathologist_id()
    {
        return $this->staff_pathologist_id;
    }

    public function setStaff_pathologist_id($staff_pathologist_id)
    {
        $this->staff_pathologist_id = $staff_pathologist_id;
    }

    public function getStaff_doctor_id()
    {
        return $this->staff_doctor_id;
    }

    public function setStaff_doctor_id($staff_doctor_id)
    {
        $this->staff_doctor_id = $staff_doctor_id;
    }

    public function getSurgery_date()
    {
        return $this->surgery_date;
    }

    public function setSurgery_date($surgery_date)
    {
        $this->surgery_date = $surgery_date;
    }

    public function getHistory()
    {
        return $this->history;
    }

    public function setHistory($history)
    {
        $this->history = $history;
    }

    public function getFna()
    {
        return $this->fna;
    }

    public function setFna($fna)
    {
        $this->fna = $fna;
    }

    public function getKkc()
    {
        return $this->kkc;
    }

    public function setKkc($kkc)
    {
        $this->kkc = $kkc;
    }

    public function getSubclinical()
    {
        return $this->subclinical;
    }

    public function setSubclinical($subclinical)
    {
        $this->subclinical = $subclinical;
    }

    public function getDiagnostic_subclinical()
    {
        return $this->diagnostic_subclinical;
    }

    public function setDiagnostic_subclinical($diagnostic_subclinical)
    {
        $this->diagnostic_subclinical = $diagnostic_subclinical;
    }

    public function getTemplate_reason_path()
    {
        return $this->template_reason_path;
    }

    public function setTemplate_reason_path($template_reason_path)
    {
        $this->template_reason_path = $template_reason_path;
    }
}