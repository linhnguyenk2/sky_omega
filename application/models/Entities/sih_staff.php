<?php
include_once 'BaseEntity.php';
// Entities/sih_staff.php

/**
 * @Entity @Table(name="sih_staff")
 **/
class Sih_staff extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /**
     * @ManyToOne(targetEntity="sih_user")
     * @JoinColumn(name="user_id", referencedColumnName="id", onDelete="NO ACTION")
     */
	protected $user_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"comment":"filter when show","default":0}) * */
	protected $orders = 0;

	public function getId()
	{
		return $this->id;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}


	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

	public function getOrders()
	{
		return $this->orders;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}

	public function setOrders($orders)
	{
		$this->orders = $orders;
	}
}