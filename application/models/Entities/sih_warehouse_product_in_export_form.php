<?php
include_once 'BaseEntity.php';
// Entities/sih_warehouse_product_in_export_form.php

/**
 * @Entity @Table(name="sih_warehouse_product_in_export_form")
 **/
class Sih_warehouse_product_in_export_form extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse_export_form")
     * @JoinColumn(name="warehouse_export_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_export_form_id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse_import_form")
     * @JoinColumn(name="warehouse_product_in_package_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_product_in_package_id;

    /** @Column(type="string", nullable=true) * */
    protected $approve_quantity;

    /** @Column(type="string", nullable=true) * */
    protected $request_quantity;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getWarehouse_export_form_id()
    {
        return $this->warehouse_export_form_id;
    }

    public function getWarehouse_product_in_package_id()
    {
        return $this->warehouse_product_in_package_id;
    }

    public function getApprove_quantity()
    {
        return $this->approve_quantity;
    }

    public function getRequest_quantity()
    {
        return $this->request_quantity;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setWarehouse_product_in_package_id($warehouse_product_in_package_id)
    {
        $this->warehouse_product_in_package_id = $warehouse_product_in_package_id;
    }

    public function setWarehouse_export_form_id($warehouse_export_form_id)
    {
        $this->warehouse_export_form_id = $warehouse_export_form_id;
    }

    public function setApprove_quantity($approve_quantity)
    {
        $this->approve_quantity = $approve_quantity;
    }

    public function setRequest_quantity($request_quantity)
    {
        $this->request_quantity = $request_quantity;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
