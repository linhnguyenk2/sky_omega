<?php
include_once 'BaseEntity.php';
// Entities/sih_medical_record.php

/**
 * @Entity @Table(name="sih_medical_record")
 **/
class Sih_medical_record extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
	protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_gynecolog_book")
     * @JoinColumn(name="gynecolog_book_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $gynecolog_book_id;

    /**
     * @ManyToOne(targetEntity="sih_breast_examination_book")
     * @JoinColumn(name="breast_examination_book_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $breast_examination_book_id;

    /**
     * @ManyToOne(targetEntity="sih_prenatal_book")
     * @JoinColumn(name="prenatal_book_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $prenatal_book_id;

    /**
     * @ManyToOne(targetEntity="sih_menopause_book")
     * @JoinColumn(name="menopause_book_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $menopause_book_id;

    /**
     * @ManyToOne(targetEntity="sih_family_planing_book")
     * @JoinColumn(name="family_planing_book_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $family_planing_book_id;

    /**
     * @ManyToOne(targetEntity="sih_health_book")
     * @JoinColumn(name="health_book_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $health_book_id;

    /** @Column(type="string", nullable=true) * */
    protected $obstetric_record_id;

    /** @Column(type="string", nullable=true) * */
    protected $gynecolog_record_id;

    /** @Column(type="string", nullable=true) * */
    protected $pediatrics_record_id;

    /** @Column(type="string", nullable=true) * */
    protected $type;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

    /** @Column(type="datetime", nullable=true) * */
    protected $last_modified;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getCode()
    {
        return $this->code;
    }

	public function getPatient_id()
	{
		return $this->patient_id;
	}

    public function getGynecolog_book_id()
    {
        return $this->gynecolog_book_id;
    }

    public function getBreast_examination_book_id()
    {
        return $this->breast_examination_book_id;
    }

    public function getPrenatal_book_id()
    {
        return $this->prenatal_book_id;
    }

    public function getMenopause_book_id()
    {
        return $this->menopause_book_id;
    }

    public function getObstetric_record_id()
    {
        return $this->obstetric_record_id;
    }

    public function getGynecolog_record_id()
    {
        return $this->gynecolog_record_id;
    }

    public function getPediatrics_record_id()
    {
        return $this->pediatrics_record_id;
    }

    public function getFamily_planing_book_id()
    {
        return $this->family_planing_book_id;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

    public function getLast_modified()
    {
        return $this->last_modified;
    }

    public function getType()
    {
        return $this->type;
    }

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id) {
        $this->id = $id;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

	public function setPatient_id($patient_id)
	{
		$this->patient_id = $patient_id;
	}

    public function setGynecolog_book_id($gynecolog_book_id)
    {
        $this->gynecolog_book_id = $gynecolog_book_id;
    }

    public function setBreast_examination_book_id($breast_examination_book_id)
    {
        $this->breast_examination_book_id = $breast_examination_book_id;
    }

    public function setPrenatal_book_id($prenatal_book_id)
    {
        $this->prenatal_book_id = $prenatal_book_id;
    }

    public function setMenopause_book_id($menopause_book_id)
    {
        $this->menopause_book_id = $menopause_book_id;
    }

    public function setObstetric_record_id($obstetric_record_id)
    {
        $this->obstetric_record_id = $obstetric_record_id;
    }

    public function setGynecolog_record_id($gynecolog_record_id)
    {
        $this->gynecolog_record_id = $gynecolog_record_id;
    }

    public function setPediatrics_record_id($pediatrics_record_id)
    {
        $this->pediatrics_record_id = $pediatrics_record_id;
    }

    public function setFamily_planing_book_id($family_planing_book_id)
    {
        $this->family_planing_book_id = $family_planing_book_id;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getHealth_book_id()
    {
        return $this->health_book_id;
    }

    public function setHealth_book_id($health_book_id)
    {
        $this->health_book_id = $health_book_id;
    }

    public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

    public function setLast_modified($last_modified)
    {
        $this->last_modified = $last_modified;
    }

	public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
