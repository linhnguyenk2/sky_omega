<?php
include_once 'BaseEntity.php';
// Entities/sih_patient_emergency_contact.php

/**
 * @Entity @Table(name="sih_patient_emergency_contact")
 **/
class Sih_patient_emergency_contact extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $name;

    /** @Column(type="string", nullable=true) * */
    protected $tel;

    /** @Column(type="string", nullable=true) * */
    protected $address;

    /**
     * Many Sih_patient_emergency_contact have One Sih_patients.
     * @ManyToOne(targetEntity="Sih_patients", inversedBy="list_patient_emergency_contact")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_emergency_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_emergency_id;

    /** @Column(type="string", nullable=true) * */
    protected $emergency_contact_relationship_type;

    /**
     * @ManyToOne(targetEntity="sih_list_provinces")
     * @JoinColumn(name="province_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $province_id;

    /**
     * @ManyToOne(targetEntity="sih_list_districts")
     * @JoinColumn(name="district_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $district_id;

    /**
     * @ManyToOne(targetEntity="sih_list_wards")
     * @JoinColumn(name="ward_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $ward_id;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    /** @Column(type="integer", options={"comment":"filter when show","default":0}) * */
    protected $orders = 0;

    public function getId()
    {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getTel() {
        return $this->tel;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getPatient_id()
    {
        return $this->patient_id;
    }

    public function getEmergency_contact_relationship_type()
    {
        return $this->emergency_contact_relationship_type;
    }

    public function getPatient_emergency_id()
    {
        return $this->patient_emergency_id;
    }

    public function getProvince_id() {
        return $this->province_id;
    }

    public function getDistrict_id() {
        return $this->district_id;
    }

    public function getWard_id() {
        return $this->ward_id;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function getOrders()
    {
        return $this->orders;
    }

    public function setPatient_id($patient_id)
    {
        $this->patient_id = $patient_id;
    }

    public function setEmergency_contact_relationship_type($emergency_contact_relationship_type)
    {
        $this->emergency_contact_relationship_type = $emergency_contact_relationship_type;
    }

    public function setPatient_emergency_id($patient_emergency_id)
    {
        $this->patient_emergency_id = $patient_emergency_id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setTel($tel) {
        $this->tel = $tel;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }

    public function setOrders($orders)
    {
        $this->orders = $orders;
    }

    public function setProvince_id($province_id) {
        $this->province_id = $province_id;
    }

    public function setDistrict_id($district_id) {
        $this->district_id = $district_id;
    }
    public function setWard_id($ward_id) {
        $this->ward_id = $ward_id;
    }
}