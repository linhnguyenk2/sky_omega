<?php
include_once 'BaseEntity.php';
// Entities/sih_warehouse_product_in_request_form.php

/**
 * @Entity @Table(name="sih_warehouse_product_in_request_form")
 **/
class Sih_warehouse_product_in_request_form extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse_export_form")
     * @JoinColumn(name="warehouse_request_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_request_form_id;

    /**
     * @ManyToOne(targetEntity="sih_product")
     * @JoinColumn(name="product_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $product_id;

    /** @Column(type="string", nullable=true) * */
    protected $approve_quantity;

    /** @Column(type="string", nullable=true) * */
    protected $request_quantity;

    /**
     * @ManyToOne(targetEntity="sih_unit")
     * @JoinColumn(name="unit_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $unit_id;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getWarehouse_request_form_id()
    {
        return $this->warehouse_request_form_id;
    }

    public function getProduct_id()
    {
        return $this->product_id;
    }

    public function getUnit_id()
    {
        return $this->unit_id;
    }

    public function getApprove_quantity()
    {
        return $this->approve_quantity;
    }

    public function getRequest_quantity()
    {
        return $this->request_quantity;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setProduct_id($product_id)
    {
        $this->product_id = $product_id;
    }

    public function setWarehouse_request_form_id($warehouse_request_form_id)
    {
        $this->warehouse_request_form_id = $warehouse_request_form_id;
    }

    public function setUnit_id($unit_id)
    {
        $this->unit_id = $unit_id;
    }

    public function setApprove_quantity($approve_quantity)
    {
        $this->approve_quantity = $approve_quantity;
    }

    public function setRequest_quantity($request_quantity)
    {
        $this->request_quantity = $request_quantity;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
