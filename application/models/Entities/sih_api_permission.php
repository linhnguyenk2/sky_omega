<?php
include_once 'BaseEntity.php';
// Entities/sih_api_permission.php

/**
 * @Entity @Table(name="sih_api_permission")
 **/
class Sih_api_permission extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="integer", nullable=false) * */
	protected $type_user;

    /** @Column(type="string", nullable=false) * */
    protected $route_path;

    /** @Column(type="string", nullable=false) * */
	protected $method;

	public function getId()
	{
		return $this->id;
	}

	public function getType_user()
	{
		return $this->type_user;
	}

    public function getRoute_path()
    {
        return $this->route_path;
    }

	public function getMethod()
	{
		return $this->method;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

	public function setType_user($type_user)
	{
		$this->type_user = $type_user;
	}

    public function setRoute_path($route_path)
    {
        $this->route_path = $route_path;
    }

	public function setMethod($method)
	{
		$this->method = $method;
	}
}