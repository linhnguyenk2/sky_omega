<?php
include_once 'BaseEntity.php';
// Entities/sih_registration_form.php

/**
 * @Entity @Table(name="sih_registration_form")
 * */
class Sih_registration_form extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_patients")
     * @JoinColumn(name="patient_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_id;

    /**
     * @ManyToOne(targetEntity="sih_list_health_insurance_card")
     * @JoinColumn(name="patient_insurrance_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_insurrance_id;

    /**
     * @ManyToOne(targetEntity="sih_list_insurance_company")
     * @JoinColumn(name="patient_company_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $patient_company_id;

    /**
     * @ManyToOne(targetEntity="sih_survival_information")
     * @JoinColumn(name="survival_information_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $survival_information_id;

    /**
     * @ManyToOne(targetEntity="sih_appointment_form")
     * @JoinColumn(name="appointment_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $appointment_form_id;

    /** @Column(type="string", nullable=true) * */
    protected $reason;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getPatient_id()
    {
        return $this->patient_id;
    }

    public function getPatient_insurrance_id()
    {
        return $this->patient_insurrance_id;
    }

    public function getPatient_company_id()
    {
        return $this->patient_company_id;
    }

    public function getSurvival_information_id()
    {
        return $this->survival_information_id;
    }

    public function getAppointment_form_id()
    {
        return $this->appointment_form_id;
    }

    public function getReason()
    {
        return $this->reason;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setPatient_insurrance_id($patient_insurrance_id)
    {
        $this->patient_insurrance_id = $patient_insurrance_id;
    }

    public function setPatient_id($patient_id)
    {
        $this->patient_id = $patient_id;
    }

    public function setPatient_company_id($patient_company_id)
    {
        $this->patient_company_id = $patient_company_id;
    }

    public function setSurvival_information_id($survival_information_id)
    {
        $this->survival_information_id = $survival_information_id;
    }

    public function setAppointment_form_id($appointment_form_id)
    {
        $this->appointment_form_id = $appointment_form_id;
    }

    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }
}