<?php
include_once 'BaseEntity.php';
// Entities/sih_warehouse_export_form.php

/**
 * @Entity @Table(name="sih_warehouse_export_form")
 **/
class Sih_warehouse_export_form extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $staff_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="storekeeper_staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $storekeeper_staff_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="medicine_chief_staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $medicine_chief_staff_id;

    /**
     * @ManyToOne(targetEntity="sih_staff")
     * @JoinColumn(name="subclinic_chief_staff_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $subclinic_chief_staff_id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse")
     * @JoinColumn(name="warehouse_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $warehouse_id;

    /**
     * @ManyToOne(targetEntity="sih_warehouse_request_form")
     * @JoinColumn(name="request_form_id", referencedColumnName="id", onDelete="NO ACTION")
     */
    protected $request_form_id;

    /** @Column(type="string", nullable=true) * */
    protected $reason;

	/** @Column(type="datetime", nullable=true) * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}) * */
	protected $stat = 1;

	public function getId()
	{
		return $this->id;
	}

    public function getCode()
    {
        return $this->code;
    }

    public function getStaff_id()
    {
        return $this->staff_id;
    }

    public function getStorekeeper_staff_id()
    {
        return $this->storekeeper_staff_id;
    }

    public function getMedicine_chief_staff_id()
    {
        return $this->medicine_chief_staff_id;
    }

    public function getSubclinic_chief_staff_id()
    {
        return $this->subclinic_chief_staff_id;
    }

    public function getWarehouse_id()
    {
        return $this->warehouse_id;
    }

    public function getRequest_form_id()
    {
        return $this->request_form_id;
    }

    public function getReason()
    {
        return $this->reason;
    }

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setStaff_id($staff_id)
    {
        $this->staff_id = $staff_id;
    }

    public function setStorekeeper_staff_id($storekeeper_staff_id)
    {
        $this->storekeeper_staff_id = $storekeeper_staff_id;
    }

    public function setMedicine_chief_staff_id($medicine_chief_staff_id)
    {
        $this->medicine_chief_staff_id = $medicine_chief_staff_id;
    }

    public function setSubclinic_chief_staff_id($subclinic_chief_staff_id)
    {
        $this->subclinic_chief_staff_id = $subclinic_chief_staff_id;
    }

    public function setWarehouse_id($warehouse_id)
    {
        $this->warehouse_id = $warehouse_id;
    }

    public function setRequest_form_id($request_form_id)
    {
        $this->request_form_id = $request_form_id;
    }

    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
	{
		$this->stat = $stat;
	}
}
