<?php
include_once 'BaseEntity.php';
// Entities/sih_raw_material_product.php

/**
 * @Entity @Table(name="sih_raw_material_product")
 **/
class Sih_raw_material_product extends BaseEntity
{
	/** @Id @Column(type="integer") @GeneratedValue * */
	protected $id;

	/** @Column(type="datetime") * */
	protected $created_at;

	/** @Column(type="integer", options={"default":1}, nullable=true) * */
	protected $stat = 1;

	/** @Column(type="integer", options={"default":0}, nullable=true) * */
	protected $orders = 0;

	public function getId()
	{
		return $this->id;
	}

	public function getCreated_at()
	{
		return $this->created_at;
	}

	public function getStat()
	{
		return $this->stat;
	}

	public function getOrders()
	{
		return $this->orders;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function setCreated_at($created_at)
	{
		$this->created_at = $created_at;
	}

	public function setStat($stat)
	{
		$this->stat = $stat;
	}

	public function setOrders($orders)
	{
		$this->orders = $orders;
	}
}
