<?php
include_once 'BaseEntity.php';
// Entities/sih_subclinical_endocrine_cells_through_vagina.php

/**
 * @Entity @Table(name="sih_subclinical_endocrine_cells_through_vagina")
 **/
class Sih_subclinical_endocrine_cells_through_vagina extends BaseEntity
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", nullable=true) * */
    protected $code;

    /** @Column(type="string", nullable=true) * */
    protected $staff_pathologist_id;

    /** @Column(type="string", nullable=true) * */
    protected $staff_doctor_id;

    /** @Column(type="string", nullable=true) * */
    protected $surgery_date;

    /** @Column(type="string", nullable=true) * */
    protected $pap_date;

    /** @Column(type="string", nullable=true) * */
    protected $lam_pap;

    /** @Column(type="string", nullable=true) * */
    protected $para;

    /** @Column(type="string", nullable=true) * */
    protected $kkc;

    /** @Column(type="string", nullable=true) * */
    protected $evaluate_pap_smear;

    /** @Column(type="string", nullable=true) * */
    protected $cell;

    /** @Column(type="string", nullable=true) * */
    protected $bottom_cell;

    /** @Column(type="string", nullable=true) * */
    protected $intermediate_cell;

    /** @Column(type="string", nullable=true) * */
    protected $surface_cell;

    /** @Column(type="datetime", nullable=true) * */
    protected $created_at;

    /** @Column(type="integer", options={"default":1}) * */
    protected $stat = 1;

    public function getId()
    {
        return $this->id;
    }

    public function getCreated_at()
    {
        return $this->created_at;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }

    public function setStat($stat)
    {
        $this->stat = $stat;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getStaff_pathologist_id()
    {
        return $this->staff_pathologist_id;
    }

    public function setStaff_pathologist_id($staff_pathologist_id)
    {
        $this->staff_pathologist_id = $staff_pathologist_id;
    }

    public function getStaff_doctor_id()
    {
        return $this->staff_doctor_id;
    }

    public function setStaff_doctor_id($staff_doctor_id)
    {
        $this->staff_doctor_id = $staff_doctor_id;
    }

    public function getSurgery_date()
    {
        return $this->surgery_date;
    }

    public function setSurgery_date($surgery_date)
    {
        $this->surgery_date = $surgery_date;
    }

    public function getPap_date()
    {
        return $this->pap_date;
    }

    public function setPap_date($pap_date)
    {
        $this->pap_date = $pap_date;
    }

    public function getLam_pap()
    {
        return $this->lam_pap;
    }

    public function setLam_pap($lam_pap)
    {
        $this->lam_pap = $lam_pap;
    }

    public function getPara()
    {
        return $this->para;
    }

    public function setPara($para)
    {
        $this->para = $para;
    }

    public function getKkc()
    {
        return $this->kkc;
    }

    public function setKkc($kkc)
    {
        $this->kkc = $kkc;
    }

    public function getEvaluate_pap_smear()
    {
        return $this->evaluate_pap_smear;
    }

    public function setEvaluate_pap_smear($evaluate_pap_smear)
    {
        $this->evaluate_pap_smear = $evaluate_pap_smear;
    }

    public function getCell()
    {
        return $this->cell;
    }

    public function setCell($cell)
    {
        $this->cell = $cell;
    }

    public function getBottom_cell()
    {
        return $this->bottom_cell;
    }

    public function setBottom_cell($bottom_cell)
    {
        $this->bottom_cell = $bottom_cell;
    }

    public function getIntermediate_cell()
    {
        return $this->intermediate_cell;
    }

    public function setIntermediate_cell($intermediate_cell)
    {
        $this->intermediate_cell = $intermediate_cell;
    }

    public function getSurface_cell()
    {
        return $this->surface_cell;
    }

    public function setSurface_cell($surface_cell)
    {
        $this->surface_cell = $surface_cell;
    }
}