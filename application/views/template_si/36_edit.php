<div class="container" style=" overflow: auto; height: 100%; ">


    <div class="row">

    </div>

    <div class="row">
        <div class="si-header col-md-12 center">
            <h3>HỒ SƠ NHÂN VIÊN</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <label class="left">Mã Nhân Viên</label>
                    <span class="left2">
                        <input type="text">
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="left">Mã Phụ</label>
                    <span class="left2">
                        <input type="text">
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="left">Họ:</label>
                    <span class="left2">
                        <input type="text">
                    </span>
                </div>
                <div class="col-md-6">
                    <label class="left">Tên:</label>
                    <span class="left2">
                        <input type="text">
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="left">Họ và Tên:</label>
                    <span class="left2">
                        <input type="text">
                    </span>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="left">Ngày Sinh:</label>
                    <span class="left2">
                        <input type="text">
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="left">Email:</label>
                    <span class="left2">
                        <input type="text">
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="left">Tên Đăng Nhập:</label>
                    <span class="left2">
                        <input type="text" disabled>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="left">Password:</label>
                    <span class="left2">
                        <input type="password">
                    </span>
                </div>
            </div>





        </div>
        <div class="col-md-5">
            <div class="row">
                <!-- <i class="fa fa-user-md fa-stack-1x text-white"></i> -->
                <img src="<?= site_url();?>/assets/images/avatar_default.jpg">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#home_edit">Cơ Bản</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#menu1_edit">Trình Độ</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#menu2_edit">Công Việc</a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="home_edit" class="tab-pane fade in active">
                    <div class="col-md-12">
                        <!--<h3>Cơ bản</h3>-->
                        <br>
                        <div class="row">
                            <!-- start -->
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Nơi Sinh:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Nguyên Quán:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Địa Chỉ Thường Trú:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Địa Chỉ Tạm Trú:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">ĐTDD:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Điện Thoại:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="left">CMND:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                                <div class="col-md-6">
                                    <label class="left">Ngày Cấp:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Nơi Cấp:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Giới Tính:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="left">Dân Tộc:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                                <div class="col-md-6">
                                    <label class="left">Tôn Giáo:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Tình trạng hôn nhân:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Quận Huyện:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Tỉnh/Thành Phố:</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Có thẻ BHXH</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Ngày báo tăng BHXH</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Ngày báo giảm BHXH</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Có thẻ BHTN</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Ngày báo tăng BHXH</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Ngày báo giảm BHXH</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Số BHYT</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Số BHXH</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Mã Quốc Gia</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Mã Bưu Điện</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Mã số thuế cá nhân</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <!-- end -->
                        </div>
                    </div>
                </div>
                <div id="menu1_edit" class="tab-pane fade">
                    <div class="col-md-12">
                        <!--<h3>Trình Độ</h3>-->
                        <br>
                        <div class="row">
                            <div class="table-responsive">          
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th></th>
                                            <th>Trình Độ</th>
                                            <th>Bằng Cấp/Chứng Chỉ</th>
                                            <th>Năm Tốt Nghiệp</th>
                                            <th>Có tính ABC</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><input type="checkbox"></td>
                                            <td>Đại Học</td>
                                            <td>File_1.docx</td>
                                            <td>2016</td>
                                            <td>Có</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td><input type="checkbox"></td>
                                            <td>Chứng chỉ 1</td>
                                            <td>File_2.docx</td>
                                            <td>2017</td>
                                            <td>Có</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td><input type="checkbox"></td>
                                            <td>Chứng chỉ 2</td>
                                            <td>File_3.docx</td>
                                            <td>2017</td>
                                            <td>Có</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-6">
                            <!-- start -->
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary">New</button>
                                    <button class="btn btn-primary">Delete</button>
                                </div>
                            </div>
                            <!-- end -->
                        </div>
                        </div>
                    </div>
                </div>
                <div id="menu2_edit" class="tab-pane fade">
                    <div class="col-md-12">
                        <!--<h3>Công việc</h3>-->
                        <br>
                        <div class="row">
                            <!-- start -->
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Phòng ban</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Nhóm lương</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Chức vụ</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Chức danh</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Ca làm việc</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Ngày vào làm</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Ngày nghỉ việc</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Nguyên nhân thôi việc</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Có tính năng suất</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Ngày tính năng suất</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Thứ tự in lương</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Nhân viên có tính hệ số tăng ca</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="left">Số phép năm</label>
                                    <span class="left2">
                                        <input type="text">
                                    </span>
                                </div>
                            </div>
                            <!-- end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
