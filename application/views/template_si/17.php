
    <div class="container" style=" overflow: auto; height: 100%; ">

	 <div class="row">
		<div class="si-header col-md-12">
			<h4>Cộng Hòa Xã Hội Chủ Nghĩa Việt Nam</h4>
			<h4>Độc Lập - Tự Do - Hạnh Phúc</h4>
			<p>-----o0o-----<p>
		  </div>
		  
		  <div class="si-header col-md-12">
			<h3>ĐƠN ĐỀ NGHỊ THỰC HIỆN KỸ THUẬT HỖ TRỢ SINH SẢN</h3>
			<h3>VỚI PHƯƠNG PHÁP XIN TRỨNG</h3>
			<br/>
		  </div>
		  
		  <div class="center">
		  <h4>Kính gởi: Hội Đồng Chuyên Môn Kỹ Thuật Hỗ Trợ Sinh Sản </h4>
		  <h4>Bệnh Viện Phụ Sản Quốc Tế Sài Gòn.</h4>
		  </div>
		  
		  
	  </div>
	  
		<div class="row">
			<div class="col-md-6">
				<label class="left">Mã đơn:</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-6">
				<label class="left">Mã bệnh nhân:</label>
				<span class="left2"><input type="text"/></span>
			</div>
	  </div>

	  <div class="row">
		<div class="col-md-8">
			<label class="left">Họ tên vợ:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Tuổi:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-4">
			<label class="left">Số CMND/Hộ chiếu:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Ngày cấp:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Nơi cấp:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left">Lập gia đình lần: [ ]</label>
			<span class="left2"><input type="text"/></span>
			
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left">Địa chỉ thường trú:</label>
			<span class="left2"><input type="text"/></span>
			<br/>
		</div>
		
	  </div>
	  
	  <div class="row">
		<div class="col-md-8">
			<label class="left">Họ tên chồng:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Tuổi:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-4">
			<label class="left">Số CMND/Hộ chiếu:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Ngày cấp:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Nơi cấp:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left">Lập gia đình lần: [ ]</label>
			<span class="left2"><input type="text"/></span>
			
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left">Địa chỉ thường trú:</label>
			<span class="left2"><input type="text"/></span>
			
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left">Tình trạng hôn nhân và gia đình:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  
	  
	  <div class="row">
		  <div class="col-md-12 si-table-basic">
			<p>
			Vợ chồng chúng tôi làm đơn này đề nghị được thực hiện Kỹ thuật Hỗ Trợ Sinh Sản với phương pháp xin trứng tại Bệnh viện Phụ Sản Quốc Tế Sài Gòn. Trong quá trình thực hiện Kỹ Thuật Hỗ Trợ Sinh Sản, vợ chồng chúng tôi xin thực hiện đúng theo quy định của Bệnh viện và Bộ Y Tế.
Chúng tôi xin cam đoan sẽ chấp nhận và hoàn toàn chịu trách nhiệm mọi tai biến rủi ro nếu có xảy ra, không khiếu kiện bất cứ điều gì đối với Bệnh viện Phụ Sản Quốc Tế Sài Gòn dù kết quả của việc thực hiện Kỹ Thuật Hộ Trợ Sinh Sản thành công hay thất bại trong lúc thực hiện hoặc trong tương lai về sau.
			</p>
		  </div>
	  </div>
	  
	 <div class="row">
		<div class="col-md-offset-7 col-md-5">
			<label><span>Thành phố Hồ Chí Minh, Ngày <input type="text" class="in-small"><span>&emsp;<span>tháng <input type="text" class="in-small"><span>&emsp;<span>năm <input type="text"  class="in-small"><span></label>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<label class="left"><b>Chồng:</b><br>(Ký tên ghi rõ họ tên)</label>
			
		</div>
		<div class="col-md-6">
			<label class="left"><b>Vợ</b><br>(Ký tên ghi rõ họ tên)</label>
			
		</div>
		<br>
		<br>
		<br>
		<div class="col-md-6">
			<label class="left">Họ tên</label>
			<span class="left2"><input type="text"/></span>
			
		</div>
		<div class="col-md-6">
			<label class="left">Họ tên</label>
			<span class="left2"><input type="text"/></span>
			
		</div>
		<br>
		<br>
		<br>
	 </div>
	  
	

    </div><!-- /.container -->
	