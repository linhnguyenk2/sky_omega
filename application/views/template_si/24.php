
    <div class="container" style=" overflow: auto; height: 100%; ">

	 <div class="row">
		  <div class="si-header col-md-12">
			<h3>HỒ SƠ THEO DÕI HỖ TRỢ SINH SẢN</h3>
		  </div>
	  </div>
	  
	  <hr>
	  <div class="row">
		<div class="col-md-6">
			<h4>PHẦN HÀNH CHÍNH</h4>
		</div>
	  </div>
	  
		<div class="row">
			<div class="col-md-6">
				<label class="left">Mã hồ sơ:</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-6">
				<label class="left">Mã bệnh nhân:</label>
				<span class="left2"><input type="text"/></span>
			</div>
	  </div>

	  <div class="row">
		<div class="col-md-12">Vợ</div>
		<div class="col-md-6">
			<label class="left">Họ và tên:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<label class="left">Ngày tháng năm sinh:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<label class="left">PARA:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<label class="left">Nghề nghiệp:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<label class="left">Điện thoại liên lạc:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<label class="left">Địa chỉ thường trú:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">Chồng</div>
		<div class="col-md-6">
			<label class="left">Họ và tên:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<label class="left">Ngày tháng năm sinh:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<label class="left">PARA:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<label class="left">Nghề nghiệp:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<label class="left">Điện thoại liên lạc:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<label class="left">Địa chỉ thường trú:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
	  </div>
	  
	  <hr>
	  <div class="row">
		<div class="col-md-6">
			<h4>BỆNH SỬ VỀ HIẾM MUỘN</h4>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-8">
					<label class="left">Thời gian hiếm muốn:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-4">
					<label class="left">năm:</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<label class="left">Hiếm muộn</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-4">
					<label class="left">của vợ</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<label class="left">Hiếm muộn:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-4">
					<label class="left">của chồng</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<label class="left">Hiếm muộn</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-4">
					<label class="left">của cặp vợ chồng</label>
				</div>
			</div>
			
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-9">
					<label class="left">Thời gian sống chung</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">tháng.</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-9">
					<label class="left">Mật độ giao hợp</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">lần/tuần lễ.</div>
			</div>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-3">
			<label class="left">Ngừa thai trước đây</label>
		</div>
		<div class="col-md-3">
			<label class="left"> Có <input type="checkbox"></label>
			<label class="left"> Không <input type="checkbox"></label>
		</div>
		<div class="col-md-6">
			<label class="left">Phương pháp, thời gian</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-3">
			<label class="left">Hút thai trước đây:</label>
		</div>
		<div class="col-md-3">
			<label class="left"> Nội khoan <input type="checkbox"></label>
			<label class="left"> Ngoại khoa <input type="checkbox"> .</label>
		</div>
		<div class="col-md-6">
			<label class="left">Nơi thực hiện</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-7">
			<label class="left">Thời gian kể từ lần có thai cuối cùng</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-5">
			<label class="left">tháng</label>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left">Đã được thăm khám và điều trị vô sinh trước đây:</label>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	 
	 <hr>
	  <div class="row">
		<div class="col-md-6">
			<h4>TIỀN SỬ NỘI NGOẠI KHOA</h4>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-1">
			<label class="left">Vợ:</label>
		</div>
		<div class="col-md-11">
			<label class="left">Lao, Tiểu đường, Thuốc hướng tâm thần, thốc tránh thai, Phẫu thuật vùng chậu, Phẫu thuật ruột thừa, Bệnh lây qua đường tình dục.</label>
		</div>
		<div class="col-md-11">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-11">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-11">
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-1">
			<label class="left">Chồng:</label>
		</div>
		<div class="col-md-11">
			<label class="left">Lao, Tiểu đường, Thuống hướng tâm thần, Chấn thương chỉnh hình, Bệnh lây qua đường tình dục, Quai bị, Đặc điểm nghề nghiệp nếu có (nhiệt độ cao, hóa chất bay hơi...), Thói quen (rượu, thuốc lá)...</label>
		</div>
		<div class="col-md-11">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-11">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-11">
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  
	  
	  <hr>
	  <div class="row">
		<div class="col-md-6">
			<h4>TÍNH CHẤT KINH NGUYỆT</h4>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-9">
					<label class="left">Chu kỳ sinh</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">ngày.</div>
			</div>
		</div>
		<div class="col-md-6">
			<label class="left"> Đều <input type="checkbox"></label>
			<label class="left"> Không đều. <input type="checkbox"></label>
			<label class="left"> Vô kinh.<input type="checkbox"></label>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-9">
					<label class="left">Số ngày có kinh:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">ngày.</div>
			</div>
		</div>
		<div class="col-md-8">
			<label class="left">Số ngày có kinh: </label>
			<label class="left"> Ít <input type="checkbox"></label>
			<label class="left"> Vừa <input type="checkbox"></label>
			<label class="left"> Nhiều.<input type="checkbox"></label>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-6">
					<label class="left">Đau bụng kinh:</label>
				</div>
				<div class="col-md-6">
					<label class="left"> Có. <input type="checkbox"></label>
					<label class="left"> Không.<input type="checkbox"></label>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-6">
					<label class="left">Hội chứng tiền kinh:</label>
				</div>
				<div class="col-md-6">
					<label class="left"> Không. <input type="checkbox"></label>
					<label class="left"> Có.<input type="checkbox"></label>
				</div>
			</div>
		</div>
	  </div>
	  
	  <hr>
	  <div class="row">
		<div class="col-md-6">
			<h4>BỆNH SỬ VỀ HIẾM MUỘN</h4>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
					<label class="left">AMH:</label>
				</div>
				<div class="col-md-5">
					<label class="left"> <input type="checkbox"></label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<label class="left"> IU/L</label>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
					<label class="left">LH:</label>
				</div>
				<div class="col-md-5">
					<label class="left"> <input type="checkbox"></label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<label class="left"> IU/L</label>
				</div>
			</div>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
					<label class="left">Prolactine:</label>
				</div>
				<div class="col-md-5">
					<label class="left"> <input type="checkbox"></label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<label class="left"> ng/mL</label>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
					<label class="left">Estradiol:</label>
				</div>
				<div class="col-md-5">
					<label class="left"> <input type="checkbox"></label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<label class="left"> pg/mL</label>
				</div>
			</div>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
					<label class="left">Testoterone:</label>
				</div>
				<div class="col-md-5">
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<label class="left"> ng/mL</label>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-5">
					<label class="left">:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				
			</div>
		</div>
	  </div>
	  
	  <hr>
	  
	  
	  <div class="row">
		<div class="col-md-12">
			<label class="left">HSG (ngày ..... tháng .... năm .....)</label>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-3">
			<label class="left">- Buồng từ cung</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  
		<div class="col-md-3">
			<label class="left">- Ống dẫn trứng (P)</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Ống dẫn trứng (T)</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Nghiệm pháp Cotte</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Ghi nhận khác</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
	  </div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left">Nội soi buồng từ cung ngày ..... tháng .... năm ....</label>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-3">
			<label class="left">- Kênh cổ tử cung</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Mặt trước, sau</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Cạnh (P), (T)</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Lỗ ống dẫn trứng (P)</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Lỗ ống dẫn trứng (T)</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  
	  
		
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">
			<label class="left">Nội soi ổ bụng ngày ..... tháng .... năm ....</label>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-3">
			<label class="left">- Tử cung</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Ống dẫn trứng (P)</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Ống dẫn trứng (T)</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Buồng trứng (P)</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Buồng trứng (T)</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Douglas</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Lạc nội mạc tử cung</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-3">
			<label class="left">- Vùng chậu</label>
		</div>
		<div class="col-md-9">
			<label class="left">:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  
	  
		
	  </div>
	  
	  <hr>
	  <div class="row">
		<div class="col-md-6">
			<h4>BILAN</h4>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-6">
			<p>Bilan Chồng</p>
			<p><label class=""> - 3 miễn dịch (MD) <input type="checkbox"></label></p>
			<p><label class=""> - Koryotype <input type="checkbox"></label></p>
			<p><label class=""> - Nội tiết <input type="checkbox"></label></p>
			<p><label class=""> - Siêu âm <input type="checkbox"></label></p>
			<p><label class=""> - Tinh dịch đồ <input type="checkbox"></label></p>
		</div>
		<div class="col-md-6">
			<p>Bilan Vợ</p>
			<div class="row">
				<p>
					<label class=""> - 3 miễn dịch (MD) <input type="checkbox"></label>
					<label class=""> TQRR<input type="checkbox"></label>
					<label class=""> Karyotype <input type="checkbox"></label>
				</p>
				<p>
					<label class=""> - Pap's<input type="checkbox"></label>
					<label class=""> Soi CTC<input type="checkbox"></label>
					<label class=""> Siêu âm <input type="checkbox"></label>
				</p>
				<p>
					<label class="">- XN CB<input type="checkbox"></label>
					<label class=""> XN TP<input type="checkbox"></label>
				</p>
			</div>
			<div class="row">
			<div class="col-sm-2">
				<p>- Nội tiết</p>
			</div>
			<div class="col-sm-4">
				<ul>
					<li>AMH</li>
					<li>FSH</li>
					<li>LH</li>
					<li>E2</li>
					<li>Prolatu</li>
				</ul>
			</div>
			</div>
			
			<div class="row">
				<p>
					HSG: (/ / /)
				</p>
				<p>- Nội soi</p>
				<ul>
					<li>Buồng TC</li>
					<li>Ổ tử cung</li>
				</ul>
			</div>
		</div>
	  </div>
	  
	  
	   <hr>
	  <div class="row">
		<div class="col-md-6">
			<h4>CHUẨN ĐOÁN HIẾM MUỘN</h4>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-sm-6">- Rối loạn phóng noãn</div>
				<div class="col-sm-3">(RLPN)</div>
				<div class="col-sm-3"><label class="left"> <input type="checkbox"></label></div>
			</div>
			<div class="row">
				<div class="col-sm-6">- Ống dẫn trứng</div>
				<div class="col-sm-3">(ODT)</div>
				<div class="col-sm-3"><label class="left"> <input type="checkbox"></label></div>
			</div>
			<div class="row">
				<div class="col-sm-6">- Lạc nội mạc tử cung</div>
				<div class="col-sm-3">(LNM)</div>
				<div class="col-sm-3"><label class="left"> <input type="checkbox"></label></div>
			</div>
			<div class="row">
				<div class="col-sm-6">- Tinh trùng</div>
				<div class="col-sm-3">(TT)</div>
				<div class="col-sm-3"><label class="left"> <input type="checkbox"></label></div>
			</div>
			<div class="row">
				<div class="col-sm-6">- Không rõ nguyên nhân</div>
				<div class="col-sm-3">(KRNN)</div>
				<div class="col-sm-3"><label class="left"> <input type="checkbox"></label></div>
			</div>
			<div class="row">
				<div class="col-sm-9">- Khác (ghi rõ)</div>
				<div class="col-sm-3"><label class="left"> <input type="checkbox"></label></div>
				
			</div>
		</div>
		<div class="col-md-6">
			<label class="left"> Ghi chú</label>
			<textarea></textarea>
		</div>
	  </div>
	  
	  
	  <hr>
	  <div class="row">
		<div class="col-md-6">
			<h4>HƯỚNG HỖ TRỢ SINH SẢN  - LẦN <input type="text" class="text-frame-one"></h4>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">
			<label>Ngày..... tháng..... năm 20....
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-3">
			<label class="left">1) Theo dõi noãn</label>
			<span class="left2"><input type="checkbox"/></span>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-sm-6"><label class="">- giao hợp tự nhiên</label></div>
				<div class="col-sm-6"><span class=""><input type="checkbox"/></span></div>
			</div>
			<div class="row">
				<div class="col-sm-6"><label class="">- IUI</label></div>
				<div class="col-sm-6"><span class=""><input type="checkbox"/></span></div>
			</div>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-3">
			<div class="row">
				<div class="col-sm-9"><label class="">2) Kích thích buồn trứng (u)</label></div>
				<div class="col-sm-3"><span class=""><input type="checkbox"/></span></div>
			</div>
			<div class="row">
				<div class="col-sm-9 right"><label class="">(t)</label></div>
				<div class="col-sm-3"><span class=""><input type="checkbox"/></span></div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-sm-6"><label class="">- giao hợp tự nhiên</label></div>
				<div class="col-sm-6"><span class=""><input type="checkbox"/></span></div>
			</div>
			<div class="row">
				<div class="col-sm-6"><label class="">- IUI</label></div>
				<div class="col-sm-6"><span class=""><input type="checkbox"/></span></div>
			</div>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">
			<label class="">3) Kích thích buồng trứng ICSI</label>
			<span class=""><input type="checkbox"/></span>
		</div>
		
	  </div>
	  
	  <div class="row">
		<div class="col-md-2">
			<div class="">
				<label class="">Phác đồ</label>
			</div>
			<div class="">
				<label class="">- Autogouist</label>
				<span class=""><input type="checkbox"/></span>
			</div>
			<div class="">
				<label class="">- Khác</label>
				<span class=""><input type="checkbox"/></span>
			</div>
		</div>
		<div class="col-md-4">
			<div class="">
				<label class="">Cập nhật phần Lab</label>
			</div>
			<div class="">
				<label class="">(liên kết được hồ sơ của phòng Lab)</label>
			</div>
		</div>
	  </div>
	  
	  
	  <hr>
	  <div class="row">
		<div class="col-md-6">
			<h4>HƯỚNG HỖ TRỢ SINH SẢN  - LẦN <input type="text" class="text-frame-one"></h4>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">
			<label>Ngày..... tháng..... năm 20....
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-3">
			<label class="left">1) Theo dõi noãn</label>
			<span class="left2"><input type="checkbox"/></span>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-sm-6"><label class="">- giao hợp tự nhiên</label></div>
				<div class="col-sm-6"><span class=""><input type="checkbox"/></span></div>
			</div>
			<div class="row">
				<div class="col-sm-6"><label class="">- IUI</label></div>
				<div class="col-sm-6"><span class=""><input type="checkbox"/></span></div>
			</div>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-3">
			<div class="row">
				<div class="col-sm-9"><label class="">2) Kích thích buồn trứng (u)</label></div>
				<div class="col-sm-3"><span class=""><input type="checkbox"/></span></div>
			</div>
			<div class="row">
				<div class="col-sm-9 right"><label class="">(t)</label></div>
				<div class="col-sm-3"><span class=""><input type="checkbox"/></span></div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-sm-6"><label class="">- giao hợp tự nhiên</label></div>
				<div class="col-sm-6"><span class=""><input type="checkbox"/></span></div>
			</div>
			<div class="row">
				<div class="col-sm-6"><label class="">- IUI</label></div>
				<div class="col-sm-6"><span class=""><input type="checkbox"/></span></div>
			</div>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">
			<label class="">3) Kích thích buồng trứng ICSI</label>
			<span class=""><input type="checkbox"/></span>
		</div>
		
	  </div>
	  
	  <div class="row">
		<div class="col-md-2">
			<div class="">
				<label class="">Phác đồ</label>
			</div>
			<div class="">
				<label class="">- Autogouist</label>
				<span class=""><input type="checkbox"/></span>
			</div>
			<div class="">
				<label class="">- Khác</label>
				<span class=""><input type="checkbox"/></span>
			</div>
		</div>
		<div class="col-md-4">
			<div class="">
				<label class="">Cập nhật phần Lab</label>
			</div>
			<div class="">
				<label class="">(liên kết được hồ sơ của phòng Lab)</label>
			</div>
		</div>
	  </div>
	  
	  
	  
	  
	  
	  

    </div><!-- /.container -->
