<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Các biểu mẫu</a></li>
                <li class="active"><?= $title;?></li>
                
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <h3 class="m-b-none"><?= $title;?></h3>
            </div>
			
			<div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <div id="DataTables_Table_0" class="dataTables_wrapper no-footer">                                        
								<table data-example="example_more" class="table table-striped m-b-none" style="width:100%"> <thead> <tr> <th style="">ID</th> <th></th> <th>Tên </th> <th>Ngày</th></tr> </thead> 
								<tbody>
									<tr role="row" class="odd">
										<td>1</td><td><input type="checkbox"></td>
										<td>Phiếu đăng ký khám bệnh</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>2</td><td><input type="checkbox"></td>
										<td>Phiếu khám nhũ</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>3</td><td><input type="checkbox"></td>
										<td>Phiếu khám hiến muộn</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>4</td><td><input type="checkbox"></td>
										<td>Phiếu khám bệnh mãn kinh</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>5</td><td><input type="checkbox"></td>
										<td>Phiếu khám khh gia đình</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>6</td><td><input type="checkbox"></td>
										<td>Phiếu khám phụ khoa</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>7</td><td><input type="checkbox"></td>
										<td>Phiếu khám thai</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>8</td><td><input type="checkbox"></td>
										<td></td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>9</td><td><input type="checkbox"></td>
										<td>Phiếu chỉ định dịch vụ</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>10</td><td><input type="checkbox"></td>
										<td>Đơn thuốc</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>11</td><td><input type="checkbox"></td>
										<td>TPCN - MP -VTYT</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>12</td><td><input type="checkbox"></td>
										<td>PHIẾU THU TIỀN TẠM ỨNG ĐIỀU TRỊ NỘI TRÚ</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>13</td><td><input type="checkbox"></td>
										<td>PHIẾU THANH TOÁN RA VIỆN</td>
										<td></td>
									</tr>
									
									<tr role="row" class="odd">
										<td>14</td><td><input type="checkbox"></td>
										<td>HÓA ĐƠN GIÁ TRỊ GIA TĂNG</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>15</td><td><input type="checkbox"></td>
										<td>XV GIÁ PHÒNG VÀ GIƯỜNG NỘI TRÚ ĐIỀU TRỊ BỆNH NHÂN</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>16</td><td><input type="checkbox"></td>
										<td>PHIẾU KHÁM BỆNH EXAMINATION FORM</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>17</td><td><input type="checkbox"></td>
										<td>ĐƠN ĐỀ NGHỊ THỰC HIỆN KỸ THUẬT HỖ TRỢ SINH SẢN VỚI PHƯƠNG PHÁP XIN TRỨNG</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>18</td><td><input type="checkbox"></td>
										<td>BẢN CAM KẾT</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>19</td><td><input type="checkbox"></td>
										<td>ĐƠN TỰ NGUYỆN CHO TRỨNG</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>20</td><td><input type="checkbox"></td>
										<td>ĐƠN XIN TINH TRÙNG (Vô Danh)</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>21</td><td><input type="checkbox"></td>
										<td>ĐƠN ĐỀ NGHỊ THỰC HIỆN KỸ THUẬT HỖ TRỢ SINH SẢN</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>22</td><td><input type="checkbox"></td>
										<td>ĐƠN TỰ NGUYỆN CHO TINH TRÙNG</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>23</td><td><input type="checkbox"></td>
										<td>TỜ CAM KẾT THỰC HIỆN THỤ TINH TRONG ỐNG NGHIỆM (IVF VÀ ICSI)</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>24</td><td><input type="checkbox"></td>
										<td>HỒ SƠ THEO DÕI HỖ TRỢ SINH SẢN</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>25</td><td><input type="checkbox"></td>
										<td>PHÁC ĐỒ ĐIỀU TRỊ</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>26</td><td><input type="checkbox"></td>
										<td>BỆNH ÁN PHỤ KHOA</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>27</td><td><input type="checkbox"></td>
										<td>PHIẾU ĐÁNH GIÁ TÌNH TRẠNG DINH DƯỠNG</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>28</td><td><input type="checkbox"></td>
										<td>PHIẾU CHĂM SÓC</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>29</td><td><input type="checkbox"></td>
										<td>TỜ ĐIỀU TRỊ</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>30</td><td><input type="checkbox"></td>
										<td>BỆNH ÁN SƠ SINH</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>31</td><td><input type="checkbox"></td>
										<td>PHIẾU THỰC HIỆN VÀ CÔNG KHÁI THUỐC, DỊCH TRUYỀN</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>32</td><td><input type="checkbox"></td>
										<td>PHIẾU CHĂM SÓC - THEO DÕI</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>33</td><td><input type="checkbox"></td>
										<td>BẢNG ĐÁNH GIÁ TUỔI THAI</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>34</td><td><input type="checkbox"></td>
										<td>BỆNH ÁN SẢN KHOA</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>35</td><td><input type="checkbox"></td>
										<td>PHIẾU ĐÁNH GIÁ TÌNH TRẠNG DINH DƯỠNG</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>36</td><td><input type="checkbox"></td>
										<td>HỒ SƠ NHÂN VIÊN</td>
										<td></td>
									</tr>
									
								</tbody>
								</table>


                            </div>
                        </div>

                    </section>
					<!-- <a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myAdd" id="btn_add"><i class="fa fa-plus"></i> Thêm</a> -->
					
					<div class="" id="add" style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title" id="myModalLabel">View</h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <div id="content_ajax"></div>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
									<button type="button" class="btn btn-warning">Lưu & In</button>
									<button type="button" class="btn btn-primary">Lưu</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
					
                </div>
			</div>
				
		</section>
	</section>
<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>

<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
</section>
</section>
</section>


<style>
.dataTables_length{
    display: inline-block;
}
table.dataTable tbody tr td:first-child:hover{
	background-color: #adbece!important;
}
</style>



