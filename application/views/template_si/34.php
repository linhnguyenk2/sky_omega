
    <div class="container" style=" overflow: auto; height: 100%; ">

	 <div class="row">
		<div class="col-md-offset-9 col-md-3">
		
			<p class=""><label class="left">Số lưu trữ:</label>
			<span class="left2"><input type="text"/></span></p>
			<p class=""><label class="left">Mã YT:</label>
			<span class="left2"><input type="text"/></span></p>
			
		</div>
	  <div class="si-header col-md-12">
		<div class="row">
			
			<div class="col-md-12">
				<h3>BỆNH ÁN SẢN KHOA</h3>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-offset-3 col-md-3"><label class="left">KHOA:</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-md-3"><label class="left">GIƯỜNG:</label>
						<span class="left2"><input type="text"/></span>
					</div>
				</div>
			</div>
			
	  </div>
	  </div>
		  
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">
			<h4>I. HÀNH CHÍNH:</h4>
		</div>
		
	  </div>

	<div class="row">
		<div class="col-md-6">
			<label class="left">Mã bệnh án:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<label class="left">Mã bệnh nhân:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	</div>

	  <div class="row">
		<div class="col-md-6">
			<label class="left">1. Họ và tên (In hoa):</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3">
					<label class="left">2. Ngày sinh:</label>
				</div>
				<div class="col-md-6">
					<input type="text" class="text-frame"/>
					<input type="text" class="text-frame"/>
					&emsp;
					<input type="text" class="text-frame"/>
					<input type="text" class="text-frame"/>
					&emsp;
					<input type="text" class="text-frame"/>
					<input type="text" class="text-frame"/>
					<input type="text" class="text-frame"/>
				</div>
				<div class="col-md-3">
					<label>Tuổi</label>
					&emsp;
					<input type="text" class="text-frame"/>
					<input type="text" class="text-frame"/>
				</div>
			</div>
		</div>
	  </div>
	  
	  <div class="row">
		
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-9">
					<label class="left">3. Nghề nghiệp:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<input type="text" class="text-frame"/>
					<input type="text" class="text-frame"/>
				</div>
				
			</div>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-9">
					<label class="left">4. Dân tộc:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<input type="text" class="text-frame"/>
					<input type="text" class="text-frame"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<label class="left">6. Địa chỉ: Số nhà</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-6">
					<label class="left">Thôn, Phố</label>
					<span class="left2"><input type="text"/></span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9">
					<label class="left">Huyện (Q,TX)</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<input type="text" class="text-frame"/>
					<input type="text" class="text-frame"/>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-9">
					<label class="left">5. Ngoại kiều:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<input type="text" class="text-frame"/>
					<input type="text" class="text-frame"/>
					
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-12">
					<label class="left">Xã, phường:</label>
					<span class="left2"><input type="text"/></span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9">
					<label class="left">Tỉnh, thành phố</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<input type="text" class="text-frame"/>
					<input type="text" class="text-frame"/>
					
				</div>
				
			</div>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-6">
			<label class="left">7. Nơi làm việc:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3">
					<label class="left">8. Đối tượng.</label>
				</div>
				<div class="col-md-2">
					<label class="left">1 BHYT<input type="checkbox"></label>
				</div>
				<div class="col-md-3">
					<label class="left">2. Thu phí<input type="checkbox"></label>
				</div>
				<div class="col-md-2">
					<label class="left">3. Miễn<input type="checkbox"></label>
				</div>
				<div class="col-md-2">
					<label class="left">4.Khác<input type="checkbox"></label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label class="left">Số thẻ BHYT</label>
					&emsp;&emsp;
					<input type="text" class="text-frame"/>
					&emsp;
					<input type="text" class="text-frame"/>
					&emsp;
					<input type="text" class="text-frame"/>
					&emsp;
					<input type="text" class="text-frame"/>
					&emsp;
					<input type="text" class="text-frame-big"/>
				</div>
			</div>
		</div>
	  </div>
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6">
						<label class="left">9. BHYT giá trị đến ngày:</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-md-3">
						<label class="left">tháng</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-md-3">
						<label class="left">năm 20</label>
						<span class="left2"><input type="text"/></span>
					</div>
					
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="left">10. Họ tên, địa chỉ người nhà khi cần báo tin.</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-md-6">
						<label class="left">Điện thoại số:</label>
						<span class="left2"><input type="text"/></span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
			<h4>II. QUẢN LÝ NGƯỜI BỆNH</h4>
			</div>
		</div>
		
		<div class="row bd-top pd-none">
			<div class="col-md-6 pd-px">
				<div class="row">
					<div class="col-md-6">11. Vào viện:   giờ   phút</div>
					<div class="col-md-6">ngày / / </div>
				</div>
				<div class="row">
					<div class="col-md-3">12. Trực tiếp vào:</div>
					<div class="col-md-3">1 Cấp cứu</div>
					<div class="col-md-3">2 KKB</div>
					<div class="col-md-3">3 Khoa điều trị</div>
				</div>
			</div>
			<div class="col-md-6 pd-px bd-left">
				<div class="row">
					<div class="col-md-3">13. Nơi giới thiệu:</div>
					<div class="col-md-3">1 Cơ quan y tế</div>
					<div class="col-md-3">2. Tự đến</div>
					<div class="col-md-3">3. Khác</div>
				</div>
				<div class="row">
					<div class="col-md-12">- Vào viện do bệnh này lần thứ </div>
					
				</div>
			</div>
		</div>
		
		<div class="row bd-top bd-bottom pd-none">
			<div class="col-md-6 pd-px">
				<div class="row">
					<div class="col-md-offset-6 col-md-6">ngày/tháng/năm. Số ngày Đ Tr</div>
				</div>
				<div class="row">
					<div class="col-md-6">14. Vào khoa: 
						&emsp;
						<input type="text" class="text-frame-big"/>
						&emsp;
					</div>
					<div class="col-md-6">giờ   phút / / 
					&emsp;&emsp;<input type="text" class="text-frame"/>
					<input type="text" class="text-frame"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">15. Chuyển 
						&emsp;&emsp;
						<input type="text" class="text-frame-big"/>
						&emsp;
					</div>
					<div class="col-md-6">giờ   phút / / 
						&emsp;&emsp;<input type="text" class="text-frame"/>
						<input type="text" class="text-frame"/>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-6">Khoa
						&emsp;&emsp;&emsp;&emsp;&emsp;
						<input type="text" class="text-frame-big"/>
						&emsp;
					</div>
					<div class="col-md-6">giờ   phút / / 
						&emsp;&emsp;<input type="text" class="text-frame"/>
						<input type="text" class="text-frame"/>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-6">
						&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
						<input type="text" class="text-frame-big"/>
						&emsp;
					</div>
					<div class="col-md-6">giờ   phút / / 
						&emsp;&emsp;<input type="text" class="text-frame"/>
						<input type="text" class="text-frame"/>
					</div>
					
				</div>
			</div>
			<div class="col-md-6 pd-px bd-left">
				<div class="row">
					<div class="col-md-3">16. Chuyển viện:</div>
					<div class="col-md-3">
						<label class="left">1.Tuyến trên<input type="checkbox"></label>
					</div>
					<div class="col-md-3">
						<label class="left">2.Tuyến dưới<input type="checkbox"></label>
					</div>
					<div class="col-md-3">
						<label class="left">3.CK<input type="checkbox"></label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="left">- Chuyển đến</label>
						<span class="left2"><input type="text"/></span>
					</div>
				</div>
				<div class="row">
					
					<div class="col-md-12">
						<span class="left2"><input type="text"/></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="left">17. Ra viện:</label>
						<span class="left2"><input type="text"/></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<label class="left">1. Ra viện<input type="checkbox"></label>
					</div>
					<div class="col-md-3">
						<label class="left">2. Xin về<input type="checkbox"></label>
					</div>
					<div class="col-md-3">
						<label class="left">3. Bỏ về<input type="checkbox"></label>
					</div>
					<div class="col-md-3">
						<label class="left">4. Đưa về<input type="checkbox"></label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="left">18. Tổng số ngày điều trị</label>
						<span class="left2"><input type="text"/></span>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
			<h4>III. CHUẨN ĐOÁN</h4>
			</div>
		</div>
		
		<div class="row bd-top bd-bottom pd-none">
			<div class="col-md-7 pd-px bd-right">
				<div class="row">
					<div class="col-md-9">
						<label class="left">19. Nơi chuyển đến.</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-md-3">
						<input type="text" class="text-frame"/>
						<input type="text" class="text-frame"/>
						<input type="text" class="text-frame"/>
						<input type="text" class="text-frame"/>
					</div>
					<div class="col-md-12">
						<span class="left2"><input type="text"/></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-9">
						<label class="left">20. KKB, Cấp cứu.</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-md-3">
						<input type="text" class="text-frame"/>
						<input type="text" class="text-frame"/>
						<input type="text" class="text-frame"/>
						<input type="text" class="text-frame"/>
					</div>
					<div class="col-md-12">
						<span class="left2"><input type="text"/></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="left">21. Lúc vào đẻ:</label>
						<span class="left2"><input type="text"/></span>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<label class="left">22. Ngày đẻ (mổ đẻ):</label>
						<span class="left2"><input type="text"/></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="left">23. Ngôi thai:</label>
						<span class="left2"><input type="text"/></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="left">24. Cách thức đẻ:</label>
						<span class="left2"><input type="text"/></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-11">
						<label class="left">- Kiểm soát tử cung:</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-md-1">
						<input type="text" class="text-frame"/>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<label class="left">- Tai biến <input type="checkbox"></label>
					</div>
					<div class="col-md-6">
						<label class="left">- Biến chứng <input type="checkbox"></label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<label class="left">1. Do phẫu thuật<input type="checkbox"></label>
					</div>
					<div class="col-md-6">
						<label class="left">2. Do gây mê<input type="checkbox"></label>
					</div>
					<div class="col-md-6">
						<label class="left">3. Do nhiễm khuẩn<input type="checkbox"></label>
					</div>
					<div class="col-md-6">
						<label class="left">4. Khác<input type="checkbox"></label>
					</div>
				</div>
				
			</div>
			<div class="col-md-5 pd-px">
				<div class="row">
					<div class="col-md-6">
					25. Tình hình phẫu thuật:
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<label class="left">1. Cấp cứu<input type="checkbox"></label>
							</div>
							<div class="col-md-6">
								<label class="left">2. Chủ động<input type="checkbox"></label>
							</div>
						</div>
					</div>
					
					
					
					<div class="col-md-12">
						<label class="left">+ Chuẩn đoán trước phẫu thuật:</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-8">
								<span class="left2"><input type="text"/></span>
							</div>
							<div class="col-md-4">
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<label class="left">+ Chuẩn đoán sau phẫu thuật:</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-8">
								<span class="left2"><input type="text"/></span>
							</div>
							<div class="col-md-4">
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
							</div>
						</div>
					</div>

					<div class="col-md-6">
					26. Trẻ sơ sinh:
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<label class="left">1. Đơn thai<input type="checkbox"></label>
							</div>
							<div class="col-md-6">
								<label class="left">2. Đa thai<input type="checkbox"></label>
							</div>
						</div>
					</div>

					<div class="col-md-2">
					3. Trai
					</div>
					<div class="col-md-2">
					4. Gái
					</div>
					<div class="col-md-2">
					a-Sống
					</div>
					<div class="col-md-6">
						b-Chết &emsp;
						<input type="text" class="text-frame"/>
						<input type="text" class="text-frame"/>
					</div>
					<div class="col-md-2">
					5. Di tật
					</div>
					<div class="col-md-12">
						<div class="row">
						<span class="left2"><input type="text"/></span>
						</div>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-8">
								<label class="left">6. Cân nặng:</label>
								<span class="left2"><input type="text"/></span>
							</div>
							<div class="col-md-4">
								<label>gram</label>
							</div>
						</div>
					</div>
					
				</div>
			</div>

			
			<div class="col-md-7 bd-top pd-px bd-right">
				
				<div class="row">
					<div class="col-md-10">
						<label class="left">27. Tổng số ngày điều trị sau phẫu thuật</label>
						
					</div>
					<div class="col-md-2">
						<input type="text" class="text-frame">
						<input type="text" class="text-frame">
					</div>
				</div>
			</div>
			<div class="col-md-5 bd-top pd-px">
				<div class="row">
					<div class="col-md-9">
						<label class="left">28. Tổng số lần phẫu thuật</label>
						
					</div>
					<div class="col-md-3">
						<input type="text" class="text-frame">
						<input type="text" class="text-frame">
					</div>
				</div>
			</div>
		
		</div>
		
		<div class="row">
			<div class="col-md-12">
			<h4>IV. TÌNH TRẠNG RA VIỆN</h4>
			</div>
		</div>
		
		<div class="row bd-top bd-bottom pd-none">
			<div class="col-md-5 pd-px">
				<div class="row">
					<div class="col-md-12">
						29. Kết quả điều trị
					</div>
					<div class="col-md-6">
						<label class="left">1. Khỏi<input type="checkbox"></label>
					</div>
					<div class="col-md-6">
						<label class="left">4. Nặng hơn<input type="checkbox"></label>
					</div>
					<div class="col-md-6">
						<label class="left">2. Đỡ, giảm<input type="checkbox"></label>
					</div>
					<div class="col-md-6">
						<label class="left">5. Tử vong<input type="checkbox"></label>
					</div>
					<div class="col-md-6">
						<label class="left">3. Không thay đổi<input type="checkbox"></label>
					</div>
					<div class="col-md-12">
						30. Giải phẫu bệnh (khi có sinh thiết):
					</div>
					<div class="col-md-4">
						<label class="left">1. Lành tính<input type="checkbox"></label>
					</div>
					<div class="col-md-4">
						<label class="left">2. Nghi ngờ<input type="checkbox"></label>
					</div>
					<div class="col-md-4">
						<label class="left">3. Ác tính<input type="checkbox"></label>
					</div>
				</div>
			</div>
			<div class="col-md-7 pd-px bd-left">
				<div class="row">
					<div class="col-md-6">
					31. Tình hình tử vong giờ  ph
					</div>
					<div class="col-md-6">
					ngày tháng năm
					</div>
					<div class="col-md-4">
						<label class="left">1. Do bệnh<input type="checkbox"></label>
					</div>
					<div class="col-md-4">
						<label class="left">2. Do tai biến điều trị<input type="checkbox"></label>
					</div>
					<div class="col-md-4">
						<label class="left">3. Khác<input type="checkbox"></label>
					</div>
					<div class="col-md-4">
						<label class="left">1 Trong 24 giờ vào viện <input type="checkbox"></label>
					</div>
					<div class="col-md-4">
						<label class="left">2 Trong 48 giờ vào viện<input type="checkbox"></label>
					</div>
					<div class="col-md-4">
						<label class="left">3 Trong 72 giờ vào viện<input type="checkbox"></label>
					</div>
					
					<div class="col-md-12">
						<label class="left">32. Nguyên nhân chính tử vong</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-md-12">
						<div class="col-md-8">
							<span class="left2"><input type="text"/></span>
						</div>
						<div class="col-md-4">
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
							</div>
					</div>
					<div class="col-md-5">
						<label class="left">33. Khám nghiệm tử thi:<input type="checkbox"></label>
					</div>
					<div class="col-md-7">
						<label class="left">34. Chẩn đoán giải phẩu tử thi</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-md-12">
						<div class="col-md-8">
							<span class="left2"><input type="text"/></span>
						</div>
						<div class="col-md-4">
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
								<input type="text" class="text-frame"/>
							</div>
					</div>
				</div>
			</div>
		</div>
	  
	  
	  <div class="row">
		<div class="col-md-offset-7 col-md-5">
			<label><span>Ngày<span> <span>tháng<span> <span>năm<span></span></span></span></span></span></span></label>
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-6">
		Giám đốc bệnh viện<br>
		(Ký tên, ghi rõ họ tên)
		<br>
		<br>
		<br>
		</div>
		<div class="col-md-6">
		Trưởng khoa<br>
		(Ký tên, ghi rõ họ tên)
		</div>
		
		
	</div>
	
	
	
	<hr>
	<br/>
	<br/>

	<div class="row">
		<div class="col-md-12">
		<h4>A. BỆNH ÁN</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-7">
			<label class="left">I. Lý do vào viện:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Vào ngày thứ.</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-1">
			<label class="left">của bệnh</label>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<label class="left">II. Hỏi bệnh:</label>
		</div>
	</div>
	<div class="row">
		<div class="col-md-7">
			<label class="left">1. Quá trình thai kỳ: kinh cuối cùng từ ngày</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-5">
			<label class="left">đến ngày </label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3">
					<label class="left">- Tuổi thai: </label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<label class="left">tuần</label>
					<span class="left2"><input type="text"/></span>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<label class="left">- Khám thai tại: </label>
			<span class="left2"><input type="text"/></span>
		</div>

		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3">
					<label>- Tiêm phòng uốn ván<input type="checkbox"/></label>
					
				</div>
				<div class="col-md-3">
					<label class="left">Được tiêm </label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-3">
					<label class="left">lần </label>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row">
				
				<div class="col-md-3">
					<label class="left">- Bắt đầu chuyển dạ từ </label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-2">
					<label class="left">giờ </label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-2">
					<label class="left">phút ngày </label>
					<span class="left2"><input type="text"/></span>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<label class="left">- Dấu hiệu lúc đầu:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<label class="left">- Biến chuyển: </label>
			<span class="left2"><input type="text"/></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<label class="left">2. Tiền sử bệnh:</label>
			<span class="left2"><input type="text"/></span>
			
		</div>
		<div class="col-md-12">
			<label class="left">+ Bản thân: (những bệnh đã mắc, dị ứng, thói quan ăn uống, sinh hoạt, thuốc lá, rượu bia, ma túy, khác...)</label>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<label class="left">+ Gia đình:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<label class="left">3. Tiền sử phụ khoa:</label>			
		</div>
		<div class="row container">
			<div class="col-md-4">
				<label class="left">- Bắt đầu thấy kinh năm: </label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-3">
				<label class="left">Tuổi</label>	
				<span class="left2"><input type="text"/></span>				
			</div>
		</div>
		<div class="col-md-7">
			<div class="row">
				<div class="col-md-6">
					<label class="left">- Tính chất kinh nguyệt:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-8">
							<label class="left">Chu kỳ</label>	
							<span class="left2"><input type="text"/></span>				
						</div>
						<div class="col-md-4">
							<label class="left">Ngày.</label>	
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="row">
				
				<div class="col-md-12">
					<label class="left">lượng kinh</label>	
					<span class="left2"><input type="text"/></span>				
				</div>
			</div>
		</div>
		
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-6">
					<label class="left">- Lấy chồng năm:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-6">
					<label class="left">tuổi</label>
					<span class="left2"><input type="text"/></span>
				</div>
				
			</div>
		</div>
		
		<div class="col-md-12">
			<label class="left">- Những bệnh phụ khoa đã điều trị:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<label class="left">4. Tiền sử sản khoa:</label>			
		</div>
	</div>


	
	<!-- <div class="row">
		  <div class="col-md-12 si-table-basic">
			<table style="width:100%">
			  <tbody><tr>
				<th>Số lần có thai</th>
				<th>Năm</th>
				<th>Đẻ đủ tháng</th>
				<th>Đẻ thiếu tháng</th>
				<th>Sẩy</th>
				<th>Hút</th>
				<th>Nạo</th>
				<th>Co-vac</th>
				<th>Chửa ngoài TC</th>
				<th>Chửa trứng</th>
				<th>Thai chết lưu</th>
				<th>Con hiện sống</th>
				<th>Cân nặng</th>
				<th>Phương pháp đẻ</th>
				<th>Tai biến</th>
				
				
			  </tr>
			  <tr>
				<td>1</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>2</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  
			  
			</tbody></table>
		  </div>
	  </div> -->

	  <div class="col-md-12">
			
			<br>
			<div class="row">
					<div class="table-responsive">          
							<table class="table">
									<thead>
											<tr>
													<th></th>
													<th>Số lần có thai</th>
													<th>Năm</th>
													<th>Đẻ đủ tháng</th>
													<th>Đẻ thiếu tháng</th>
													<th>Sẩy</th>
													<th>Hút</th>
													<th>Nạo</th>
													<th>Co-vac</th>
													<th>Chửa ngoài TC</th>
													<th>Chửa trứng</th>
													<th>Thai chết lưu</th>
													<th>Con hiện sống</th>
													<th>Cân nặng</th>
													<th>Phương pháp đẻ</th>
													<th>Tai biến</th>
											</tr>
									</thead>
									<tbody>
											<tr>
													<td><input type="checkbox"></td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													
											</tr>
											<tr>
													<td><input type="checkbox"></td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
													<td>text</td>
											</tr>
											
									</tbody>
							</table>
					</div>
			</div>
			<div class="row">
			<div class="col-md-6">
					<!-- start -->
					
					<div class="row">
							<div class="col-md-12">
									<button class="btn btn-primary">Thêm mới</button>
									<button class="btn btn-primary">Xóa</button>
							</div>
					</div>
					<!-- end -->
			</div>
			</div>
			<br>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<h4>III. Khám bệnh</h4>			
		</div>
	</div>
	
	
	
	<div class="row">
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-12">
					<label class="left">1. Toàn thân:</label>
					
				</div>
				<div class="col-md-12">
					<label class="left">- Toàn trạng</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-9">
							<span class="left2"><input type="text"/></span>
						</div>
						<div class="col-md-3">
						<label class="left">Phù &emsp;<input type="checkbox"></label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<label class="left">- Tuần hoàn</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-12">
					<span class="left2"><input type="text"/></span>
				</div>
				
			</div>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-8">
							<label class="left">Mạch:</label>	
							<span class="left2"><input type="text"></span>				
						</div>
						<div class="col-md-4">
							<label class="left">lần/ph</label>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<label class="left">Nhiệt độ:</label>	
							<span class="left2"><input type="text"></span>				
						</div>
						<div class="col-md-4">
							<label class="left">C</label>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<label class="left">Huyết áp:</label>	
							<span class="left2"><input type="text"></span>				
						</div>
						<div class="col-md-4">
							<label class="left">mmHg</label>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<label class="left">Nhịp thở:</label>	
							<span class="left2"><input type="text"></span>				
						</div>
						<div class="col-md-4">
							<label class="left">lần/ph</label>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<label class="left">Chiều cao:</label>	
							<span class="left2"><input type="text"></span>				
						</div>
						<div class="col-md-4">
							<label class="left">cm</label>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<label class="left">Cân nặng:</label>	
							<span class="left2"><input type="text"></span>				
						</div>
						<div class="col-md-4">
							<label class="left">kg</label>	
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<label class="left">- Hô hấp</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<label class="left">- Tiêu hóa</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<label class="left">- Tiết niệu</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<label class="left">- Các bộ phận khác</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<label class="left">2. Khám ngoài:</label>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
					<label>- Bụng có sẹo phẫu thuật cũ <input type="checkbox"></label>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-5">
					<label class="left">- Chiều cao TC:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-5">
					<label class="left">cm, vòng bụng:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-2">
					<label class="left">cm</label>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-7">
					<label class="left">- Tim thai: </label>
					<span class="left2"><input type="text"/></span>
				</div>
				
				<div class="col-md-2">
					<label class="left">lần/phút</label>
				</div>
			</div>	
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-7">
					<label class="left">- Hình dạng TC:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-5">
					<label class="left">Tư thế</label>
					<span class="left2"><input type="text"/></span>
				</div>
				
			</div>	
			<div class="row">
				<div class="col-md-12">
					<label class="left">- Cơn co TC:</label>
					<span class="left2"><input type="text"/></span>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-12">
					<label class="left">- Vú:</label>
					<span class="left2"><input type="text"/></span>
				</div>
			</div>	
		</div>
		
	</div>
	
	
	<div class="row">
		<div class="col-md-12">
			<label class="left">3. Khám trong:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	</div>
	<div class="row">
        <div class="col-md-2">
            <label class="left">+ Chỉ số Bishop</label>					
		</div>
		<div class="col-md-3 bd-all">
			<div class="row">
				<div class="col-md-10">
					<span class="left2"><input type="text"></span>				
				</div>	
				<div class="col-md-2">
					<label class="left">điểm</label>	
				</div>	
			</div>	
        </div>
        
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-6">
					<label class="left">- Âm hộ</label>
					<span class="left2"><input type="text"/></span>
				</div>	
				<div class="col-md-6">
					<label class="left">- Âm đạo:</label>
					<span class="left2"><input type="text"/></span>
				</div>	
			</div>
			
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
					<label class="left">- Tầng sinh môn:</label>
					<span class="left2"><input type="text"/></span>
				</div>	
				
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-6">
					<label class="left">- Cổ tử cung:</label>
					<span class="left2"><input type="text"/></span>
				</div>	
				<div class="col-md-6">
					<label class="left">- Phần phụ:</label>
					<span class="left2"><input type="text"/></span>
				</div>	
			</div>
			
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3">
					<label class="left">Tình trạng ối:</label>
					
				</div>	
				<div class="col-md-3">
					<label class="left">1. Ối hồng <input type="checkbox"/></label>
				</div>
				<div class="col-md-3">
					<label class="left">2. Ối dẹt <input type="checkbox"/></label>
				</div>
				<div class="col-md-3">
					<label class="left">3. Ối quả lê <input type="checkbox"/></label>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<label class="left">- Màu sắc nước ối:</label>
			<span class="left2"><input type="text"/></span>
			
		</div>
		<div class="col-md-6">
			<label class="left">- Nước ối nhiều hay ít:</label>
			<span class="left2"><input type="text"/></span>
			
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-6">
					<label class="left">- Ngôi:</label>
					<span class="left2"><input type="text"/></span>
				</div>	
				<div class="col-md-6">
					<label class="left">- Thế:</label>
					<span class="left2"><input type="text"/></span>
				</div>	
			</div>
		</div>
		<div class="col-md-6">
			<label class="left">- Kiểu thế:</label>
			<span class="left2"><input type="text"/></span>
			
		</div>
		
	</div>
	
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-2">
					<label class="left">- Độ lọt:</label>
				</div>	
				<div class="col-md-2">
					<label class="left">1. Cao<input type="checkbox"></label>
				</div>	
				<div class="col-md-2">
					<label class="left">2. Chúc<input type="checkbox"></label>
				</div>	
				<div class="col-md-2">
					<label class="left">3. Chặt<input type="checkbox"></label>
				</div>	
				<div class="col-md-2">
					<label class="left">4. Lọt<input type="checkbox"></label>
				</div>	
			</div>
		</div>
		<div class="col-md-6">
			<label class="left">- Đường kính nhô hạ vệ:</label>
			<span class="left2"><input type="text"/></span>
			
		</div>
		
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<label class="left">4. Các xét nghiệm cận lâm sàn:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<label class="left">5. Chuẩn đoán:</label>
			
		</div>
		<div class="col-md-12">
			<label class="left">- Khi vào khoa:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<label class="left">- Phân biệt:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-12">
			<label class="left">6. Tiền lượng:</label>
			
		</div>
		
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-12">
			<label class="left">7. Hướng điều trị:</label>
			
		</div>
		<div class="col-md-12">
			<label class="left">- Phương pháp chính:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
		
	</div>
	
	
	  
	<div class="row">
		<div class="col-md-offset-7 col-md-5">
			<label><span>Lúc ... giờ ....,Ngày<span> <span>tháng<span> <span>năm<span></span></span></span></span></span></span></label>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-offset-7 col-md-5">
			<label>Bác sỹ làm bệnh án</label>
		</div>
		<br/>
		<br/>
		<br/>
		<div class="col-md-offset-7 col-md-5">
			<label class="left">Họ và tên</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
	</div>


	<div class="row">
		<div class="col-md-12">
			<h4>IV. Theo dõi tại buồng đẻ:</h4>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-3">
			<label class="left">- Vào buồng đẻ lúc:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-2">
			<label class="left">giờ</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-3">
			<label class="left">phút ngày:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  
		<div class="col-md-5">
			<label class="left">- Tên người theo dõi:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-7">
			<label class="left">Chức danh:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left"><b>1. Đặc điểm trẻ sơ sinh:</b></label>
		</div>
	</div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left"><b>2. Đặc điểm sổ rau:</b></label>
		</div>
	</div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left"><b>3. Tình trạng sản phụ khi đẻ:</b></label>
		</div>
	</div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left"><b>4. Tình hình phẫu thuật (nếu có):</b></label>
		</div>
	</div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left"><b>5. Hướng điều trị và các chế độ tiếp theo:</b></label>
		</div>
	</div>

	  <div class="row">
		<div class="col-md-12">
			<label class="left">V. Tiền lương:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">
			<label class="left">VI. Hướng điều trị:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-12">
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	
	<div class="row">
		<div class="col-md-12">
			<h4>B. TỔNG KẾT BỆNH ÁN</h4>
		</div>
	</div>
	<div class="container bd-all">
		<div class="row">
			<div class="col-md-12">
				<label class="left">1. Quá trình bệnh lý và diễn biến lâm sàn:</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-12">
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-12">
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-12">
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-12">
				<span class="left2"><input type="text"/></span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="left">2. Tóm tắt kết quả xét nghiệm cần lâm sàng có giá trị chẩn đoán:</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-12">
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-12">
				<span class="left2"><input type="text"/></span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="left">3. Phương pháp điều trị:</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-12">
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-12">
				<span class="left2"><input type="text"/></span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<label>- Phẫu thuật<input type="checkbox"/></label>
			</div>
			<div class="col-md-3">
				<label>- Thủ thuật<input type="checkbox"/></label>
			</div>
		</div>
		
		<!-- <div class="row">
			  <div class="col-md-12 si-table-basic">
				<table style="width:100%">
				  <tbody><tr>
					<th>Giờ, ngày</th>
					<th>Phương pháp phẫu thuật vô cảm</th> 
					<th>Bác sỹ phẫu thuật</th>
					<th>Bác kỹ gây mê</th>
				  </tr>
				  <tr>
					<td>1</td>
					<td></td>
					<td></td>
					<td></td>
				  </tr>
				  
				</tbody></table>
			  </div>
		</div> -->

	<div class="col-md-12">
			<!--<h3>Trình Độ</h3>-->
			<br>
			<div class="row">
					<div class="table-responsive">          
							<table class="table">
									<thead>
											<tr>
													<th></th>
													<th>Giờ, Ngày</th>
													<th>Phương pháp phẫu thuật vô cảm</th>
													
													<th>Bác sỹ phẫu thuật</th>
													<th>Bác sỹ gây mê</th>
											</tr>
									</thead>
									<tbody>
											<tr>
													<td><input type="checkbox"></td>
													<td>4/4/2018</td>
													<td>Phương pháp 1</td>
													
													<td>BS 1</td>
													<td>BS 1</td>
											</tr>
											<tr>
													<td><input type="checkbox"></td>
													<td>4/4/2018</td>
													<td>Phương pháp 2</td>
													
													<td>BS 2</td>
													<td>BS 2</td>
											</tr>
											
									</tbody>
							</table>
					</div>
			</div>
			<div class="row">
			<div class="col-md-6">
					<!-- start -->
					
					<div class="row">
							<div class="col-md-12">
									<button class="btn btn-primary">Thêm mới</button>
									<button class="btn btn-primary">Xóa</button>
							</div>
					</div>
					<!-- end -->
			</div>
			</div>
			<br>
	</div>

		<div class="row">
			<div class="col-md-12">
				<label class="left">4. Tình trạng người bệnh ra viện:</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-12">
				<span class="left2"><input type="text"/></span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="left">5. Hướng điều trị và các chế độ tiếp theo:</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-12">
				<span class="left2"><input type="text"/></span>
			</div>
		</div>
		<!-- <div class="row">
            <div class="col-md-12 si-table-basic">
                <table style="width:100%">
                    <tbody><tr>
                            <th colspan="2">Hồ sơ, phim, ảnh</th>

                            <th rowspan="4">
								<p>Người giao hồ sơ</p>
								<br/>
								<br/>
								<br/>
								<div>
									<label class="left">Họ tên</label>
									<span class="left2"><input type="text"/></span>
								</div>
							</th>
                            <th rowspan="8">
								<p>Ngày tháng năm 20..</p>
								<p>Bác sỹ điều trị</p>
								<br/>
								<br/>
								<br/>
								<div>
									<label class="left">Họ tên</label>
									<span class="left2"><input type="text"/></span>
								</div>

							</th>
                        </tr>
                        <tr>
                            <td>Loại</td>
                            <td>Số tờ</td>
                        </tr>
                        <tr>
                            <td>- X - quang</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>- CT Scanner</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>- Siêu âm</td>
                            <td></td>
                            <td rowspan="4">
								<p>Người nhận hồ sơ</p>
								<br/>
								<br/>
								<br/>
								<div>
									<label class="left">Họ tên</label>
									<span class="left2"><input type="text"/></span>
								</div>
							</td>
                        </tr>
                        <tr>
                            <td>- Xét nghiệm</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>- Khác</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>- Toàn bộ hồ sơ</td>
                            <td></td>
                        </tr>

                    </tbody></table>
            </div> -->
			<div class="col-md-12">
			<!--<h3>Trình Độ</h3>-->
			<br>
			<div class="row">
					<div class="table-responsive">          
							<table class="table">
									<thead>
											<tr>
													<th></th>
													<th>Loại HS, phim,ảnh</th>
													<th>File</th>
													
													<th>Ghi chú</th>
													<th>Ngày</th>
											</tr>
									</thead>
									<tbody>
											<tr>
													<td><input type="checkbox"></td>
													
													<td>X - quang</td>
													
													<td>fil1.png</td>
													<td>Note</td>
													<td>4/4/2018</td>
											</tr>
											<tr>
													<td><input type="checkbox"></td>
													<td>CT Scanner</td>
													
													<td>File2.pdf</td>
													<td>Note</td>
													<td>4/4/2018</td>
											</tr>
											<tr>
													<td><input type="checkbox"></td>
													<td>Siêu âm</td>
													
													<td>File2.pdf</td>
													<td>Note</td>
													<td>4/4/2018</td>
											</tr>
											<tr>
													<td><input type="checkbox"></td>
													<td>Xét nghiệm</td>
													
													<td>File2.pdf</td>
													<td>Note</td>
													<td>4/4/2018</td>
											</tr>
											<tr>
													<td><input type="checkbox"></td>
													<td>Khác</td>
													
													<td>File2.pdf</td>
													<td>Note</td>
													<td>4/4/2018</td>
											</tr>
											<tr>
													<td><input type="checkbox"></td>
													<td>Toàn bộ hồ sơ</td>
													
													<td>File2.pdf</td>
													<td>Note</td>
													<td>4/4/2018</td>
											</tr>
											
									</tbody>
							</table>
					</div>
			</div>
			<div class="row">
			<div class="col-md-6">
					<!-- start -->
					
					<div class="row">
							<div class="col-md-12">
									<button class="btn btn-primary">Thêm mới</button>
									<button class="btn btn-primary">Xóa</button>
							</div>
					</div>
					<!-- end -->
			</div>
			</div>
			<br>
	</div>
	<div class="row">
            <div class="col-md-12 si-table-basic">
                <table style="width:100%">
                    <tbody>
					<tr>
                            

                            <th rowspan="">
								<p>Ngày tháng năm 20..</p>
								<p>Người giao hồ sơ</p>
								<br>
								<br>
								<br>
								<div>
									<label class="left">Họ tên</label>
									<span class="left2"><input type="text"></span>
								</div>

							</th>
                            <th rowspan="">
								<p>Ngày tháng năm 20..</p>
								<p>Người nhận hồ sơ</p>
								<br>
								<br>
								<br>
								<div>
									<label class="left">Họ tên</label>
									<span class="left2"><input type="text"></span>
								</div>

							</th>
                            <th rowspan="">
								<p>Ngày tháng năm 20..</p>
								<p>Bác sỹ điều trị</p>
								<br>
								<br>
								<br>
								<div>
									<label class="left">Họ tên</label>
									<span class="left2"><input type="text"></span>
								</div>

							</th>
                        </tr>

                    </tbody>
				</table>
            </div>
        </div>


        </div>
	</div>
	
	
	
	
	
	<br>
	<br>
	<br>
	
	

    </div><!-- /.container -->
	