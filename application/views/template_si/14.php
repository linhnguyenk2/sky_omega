
    <div class="container" style=" overflow: auto; height: 100%; ">

	 <div class="row">
		  <div class="si-header col-md-8">
			<h3>HÓA ĐƠN GIÁ TRỊ GIA TĂNG</h3>
			<h4>VAT INVOICE</h4>
			<p>Liên 1:</p>
			<p>Ngày/tháng/năm (Date):</p>
		  </div>
		  <div class="col-md-4">
			Mẫu số: <input type="text"><br/>
			Ký hiệu: <input type="text"><br/>
			Số HĐ: <input type="text"><br/>
			Số CT: <input type="text">
		  </div>
		  
	  </div>
	  
		<div class="row">
		<div class="col-md-6">
			<label class="left">Mã hóa đơn:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<label class="left">Mã bệnh nhân:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	
		</div>
	  
	  <div class="row">
		<div class="col-md-12">
			<label class="left">Tên khách hàng (Customer's name):</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">
			<label class="left">Tên đơn vị (Company's name):</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-7">
			<label class="left">Địa chỉ (Address):</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-5">
			<label class="left">Mã số thuế (Tax ID):</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
	  </div>
	  
	  
	  <div class="row">
		  <div class="col-md-12 si-table-basic">
			<table style="width:100%">
			  <tr>
				<th rowspan="2">Diễn giải<br/><i class="font-thin">Description</i></th>
				<th rowspan="2">Đơn vị tính<br/><i class="font-thin">Unit</i></th> 
				<th rowspan="2">SL<br/><i class="font-thin">Qty</i></th>
				<th rowspan="2">Đơn giá<br/><i class="font-thin">Unit Price</i></th>
				<th rowspan="2">Thành tiền<br/><i class="font-thin">Amount</i></th>
				<th colspan="2">Thuế GTGT (VAT)</th>
				<th rowspan="2">Cộng<br/><i class="font-thin">Total</i></th>
			  </tr>
			  <tr>
				
				<th>%</th>
				<th>Tiền thuế<br/><i class="font-thin">VAT amout</i></th>
				
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  
			  <tr>
				<th colspan="8" class="center tb-1-row">
					<div class="row">
						<div class="col-md-7">
							<div class="row">
								<label class="left">- Cộng tiền không chịu thuết GTGT:</label>
								<span class="left2"><input type="text"/></span>
							</div>
						</div>
						<div class="col-md-5">
							<div class="row">
								<label class="left">- Tổng cộng tiền chưa thuế GTGT:</label>
								<span class="left2"><input type="text"/></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="row">
								<label class="left">- Cộng tiền chưa thuế GTGT 5%:</label>
								<span class="left2"><input type="text"/></span>
							</div>
							<div class="row">
								<label class="left">- Cộng thêm chưa thuế GTGT 10%:</label>
								<span class="left2"><input type="text"/></span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="row">
								<label class="left">- Cộng tiền thuế 5%:</label>
								<span class="left2"><input type="text"/></span>
							</div>
							<div class="row">
								<label class="left">- Cộng tiền thuế 10%:</label>
								<span class="left2"><input type="text"/></span>
							</div>
						</div>
						<div class="col-md-5">
							<div class="row">
								<label class="left">- Tổng cộng tiền thuế GTGT:</label>
								<span class="left2"><input type="text"/></span>
							</div>
							<div class="row">
								<label class="left">- Tổng cộng tiền phải thanh toán:</label>
								<span class="left2"><input type="text"/></span>
							</div>
						</div>
					</div>
				</th>
			  </tr>
			  
			  
			</table>
			<div class="leo-cot">In từ phần mềm của Cty CP Bệnh Viện Phụ Sản Quốc Tế Sài Gòn MST: 0301482886</div>
		  </div>
	  </div>
	  
	
	
	<div class="row">
		<div class="col-md-12">
			<label class="left">Số tiền viết bằng chữ (In words):</label>
			<span class="left2"><input type="text"/></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<label class="center"><b>Khách hàng (Customer)</b><br>Ký, ghi rõ họ tên<br/>Sign & full name</label>
			
		</div>
		<div class="col-md-4">
			<label class="center"><b>Thu ngân(Cashier)</b><br>Ký, ghi rõ họ tên<br/>Sign & full name</label>
			
		</div>
		<div class="col-md-4">
			<label class="center"><b>Thủ trường đơn vị (Director)</b><br>Ký, đóng dấu, ghi rõ họ tên<br/>Sign, stamp & full name</label>
		</div>
	 </div>
	  
	 <br/>
	 <br/>
	  

    </div><!-- /.container -->
	
