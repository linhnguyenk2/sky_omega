
    <div class="container" style=" overflow: auto; height: 100%; ">
		<div class="rơw left">
		<h4>KHOA HIẾM MUỘN</h4>
		</div>
      <div class="si-header col-xs-12">
        <h2>TỜ CAM KẾT</h2>
		<h2>THỰC HIỆN THỤ TINH TRONG ỐNG NGHIỆM</h2>
		<h2>(IVF VÀ ICSI)</h2>
      </div>
	 
	 
	  <div class="row">
			<div class="col-md-6">
				<label class="left">Mã cam kết:</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-6">
				<label class="left">Mã bệnh nhân:</label>
				<span class="left2"><input type="text"/></span>
			</div>
	  </div>

	  <div class="row">
		<div class="col-md-1">
			<label class="left">Điều 1:</label>
		</div>
		<div class="col-md-11">
<pre>
Thụ tinh trong ống nghiệm (TTTON) là phương pháp hỗ trợ sinh sản điều trị Hiếm Muộn cho các trường hợp:
- Tắc vòi trứng.
- Lạc nội mạc tử cung.
- Tình trùng yếu, ít, dị dạng.
- Hiếm muộn không rõ nguyên nhân, bơm tinh trùng nhiều lần thất bại.
- Xin trứng, xin tinh trùng... (Nếu có và phải theo đúng nghị định thông tư của Bộ Y Tế)
</pre>
		</div>
		
	  </div>
	  
	  <div class="row">
		<div class="col-md-1">
			<label class="left">Điều 2:</label>
		</div>
		<div class="col-md-11">
<pre>
Quy trình TTTON được tiền hành như sau:
- Khám, làm hồ sơ và các xét nghiệm cần thiết tại Phòng khám Hiếm Muộn BV
- Hồ sơ TTTON được Khoa Hiếm muộn Hội chẩn với Ban Giám Đốc.
- Sau khi hội cẩn, người bệnh sẽ được ghi tên vào sổ để xếp lịch hẹn là TTTON.
- Khi đến ngày đầu chu kỳ kinh đã hẹn người bệnh đến Bệnh viện để biết thêm chi tiết về chu ký điều trị.
- Khám tiền mê để xác nhận khả năng có thể trải qua các thủ thuật và khả năng có thể mang thai.
- Tiêm Thuốc kích thích buồng trứng từ ngày đầu hoặc ngày 21 của chu kỳ theo phác đồ điều trị của khoa, khoảng 2 tuần đến 6 tuần. Cần tiêm thuốc đúng giờ theo y lệnh.
- Siêu âm, thử máu để theo dõi đáp ứng và điều chỉnh liều thuốc.
Tiêm thuốc để chọc hút trứng (hCG) khi nang noãn trưởng thành.
- Chọc hút trứng 36 giờ sau khi tiêm hCG.
Lấy tinh trùng chồng để chuẩn bị TTTON song song ngày chọc hút trứng.
- Cấy trứng với tinh trùng trong phòng nuôi cấy.
- Theo dõi sự phát triển của phôi trong vòng 2-3 ngày.
- Chọn phôi và chuyển phôi vào tử cung 2=3 ngày sau khi cấy.
- Dùng thuốc hỗ trợ sau khi chuyển phôi.
- Thử thai 14 ngày sau khi chuyển phôi.
- Siêu âm 2 tuần sau nếu thử thai dương tính
Hiện nay tỉ lệ thành công của TTTON-ICSI tại Bệnh Viện Phụ Sản Quốc Tế Sài Gòn vào khoảng 37% đến 40%. Tỷ lệ này ngày càng giảm nếu người vợ lớn tuổi, nếu người vợ trên 35 tuổi tỷ lệ thành công giảm nhiều, tỷ lệ thành công càng thấp nếu người vợ trên 40 tuổi.

Người bệnh đóng viện phí cho Bệnh viện trước ngày chọc hút trứng cụ thể như sau:
IVF: 15.000.000 đ (Mười lăm triệu đồng)
ICSI: 18.000.000 đ (Mười tám triệu đồng)
Không kể các chi phí khác và tiền thuốc.
</pre>
		</div>
		
	  </div>
	  
	  <div class="row">
		<div class="col-md-1">
			<label class="left">Điều 3:</label>
		</div>
		<div class="col-md-11">
<pre>
Trong quá trình điều trị có thể xảy ra các vấn đề bất thường với tỷ lệ xuất hiện như sau:
- Buồng trứng đáp ứng kém, ngưng điều trị. 1--15%
- Quá kích buồng trứng khi kích thích. 5-10%
- Quá kích buồng trứng rất nặng, có thể dẫn đến tử vong. 0.1%
- Rụng trứng sớm trước khi chọc hút trứng 1%
- Chọc hút không có trứng. 1%
- Xuất huyết nội sau chọc hút trứng, phải mổ cầm máu. 0.2%
- Không thụ tinh, không có phôi. 1-5%
- Quá kích khi có thai. 5-10%
- Có nhiều hơn 1 thai (2,3,4 thai). 15-20%

Đây là các vấn đề khách quan thường xảy ra ở các trung tâm TTTON trên thế giới. Bệnh viện sẽ giải thích, hướng dẫn và điều trị cho người bệnh khi có các vấn đề trên xảy ra. Người bệnh sẽ tự chi trả các chi phí điều trị phát sinh khi các vấn đề trên xảy ra. Khi có thai người bênh sẽ tự chi trả chi phí trong quá trình khám, theo dõi thai, dưỡng thai và chi phí chăm sóc trẻ sau khi sanh tại bệnh viện trong trường sanh non tháng.
</pre>
		</div>
		
	  </div>
	  <div class="row">
		<div class="col-md-1">
			<label class="left">Điều 4:</label>
		</div>
		<div class="col-md-11">
<pre>
Chúng tôi đã đọc kỹ và được BS tư vấn, giải thích kỹ các vấn đề liên quan đến việc thực hiện TTTON (IVF và ICSI).
</pre>
		</div>
		
	  </div>
	  <div class="row">
		<div class="col-md-1">
			<label class="left">Điều 5:</label>
		</div>
		<div class="col-md-11">
<pre>
Chúng tôi xin tự nguyện hợp thực hiện kỹ thuật TTTON (IVF và ICSI) tại Bệnh Viện Phụ Sản Quốc Tế Sài Gòn và xin tự nguyện cam kết với những điều sau đây:
- Cam kết chúng tôi là vợ chồng hợp pháp
- Cam kết mẫu tinh trùng và trứng được lấy ra để thực hiện TTTON (IVF và ICSI) là của vợ chồng chúng tôi.
- Cam kết chi trả toàn bộ chi phí trong quá trình thực hiện TTTON (IVF và ICSI) theo đúng qui định của Bệnh viện.
- Cam kết sẽ chấp nhận mỏi rủi ro tai biến nếu xảy ra và không khiếu kiện trong các trường hợp nếu có xảy ra rủi ro trong quá trình thực hiện kỹ thuật Hỗ trợ sinh sản (TTTON).
</pre>
		</div>
		
	  </div>
	  
	<div class="row">
		<div class="col-md-offset-7 col-md-5">
		<label><span>Thành phố Hồ Chí Minh, Ngày <input type="text"  class="in-small"><span>&emsp;<span>tháng <input type="text"  class="in-small"><span>&emsp;<span>năm <input type="text"  class="in-small"><span></label>
			<br>
			Khoa Hiếm Muộn - TTTON
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-6">
		NHS hướng dẫn<br>
		(Ký tên, ghi rõ họ tên)
		<br/>
		<br/>
		<br/>
		</div>
		<div class="col-md-6">
		Bác sỹ điều trị<br>
		(Ký tên, ghi rõ họ tên)
		</div>
		
		
	</div>
	<div class="row">
		<div class="col-md-6">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<span class="left2"><input type="text"/></span>
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-6">
		<br/>
		Chồng<br>
		(Ký tên, ghi rõ họ tên)
		<br/>
		<br/>
		<br/>
		</div>
		<div class="col-md-6">
		<br/>
		Vợ<br>
		(Ký tên, ghi rõ họ tên)
		</div>
		
		
	</div>
	<div class="row">
		<div class="col-md-6">
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<span class="left2"><input type="text"/></span>
		</div>
		
	</div>
	
	  
	  

    </div><!-- /.container -->
