
    <div class="container" style=" overflow: auto; height: 100%; ">

	 <div class="row">
		
		  <div class="si-header col-md-12">
			<h3>PHÁC ĐỒ ĐIỀU TRỊ</h3>
			
		  </div>
		  
		  
	  </div>
	  <hr>
	  <div class="row">
		<div class="col-md-12">
			<h4>QUY TRÌNH CHUẨN BỊ TINH TRÙNG THU NHẬN BẰNG PHẪU THUẬT</h4>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">
		<h4>A. Nguyên tắc</h4>
			<p>Thu được tối đa lượng tinh trùng lấy bằng phẫu thuật, đảm bảo số lượng, chất lượng tinh trùng cho ICSI.</p>
		<h4>B.Dụng cụ và môi trường</h4>

<p>- Tủ thao tác vô trùng (laminar flow)</p>
<p>- Máy ly tâm (trục quay ngang)</p>
<p>- Tủ cấy CO2</p>
<p>- Kính hiển vi soi nổi có bệ ấm</p>
<p>- Kính hiển vi đảo ngược có bệ ấm</p>
<p>- Pasteur pipette tiệt trùng loại dài 150mm (Volac)</p>
<p>- Đĩa petri 35mm</p>
<p>- Lame</p>
<p>- Bộ nhíp xé mô chuyên dụng</p>
<p>- G-Mops Plus (Vitrolife)</p>
<p>- G-IVF Plus (Vitrolife)</p>
		
		<h4>C. Thực hiện</h4>
		<h4>* PESA/MESA:</h4>
		<p>
		- Tìm tinh trùng trên đĩa petri bằng kiến hiển vi đảo ngược (có bệ ấm) ở độ phóng đại 200 lần.
Nếu có sự hiện diện của tih trùng di động, sử dụng pipette Pasteur hút giọt mào tinh trên đĩa cho vào ống nghiệm 5ml, tráng lại đĩa bằng G-Mops Plus. Ly tâm dịch mào tinh thu được ở 1500 vòng trong 5 phút. Loại bỏ dịch nổi, chừa lại 0,5 ml, trộn đều phần dịch còn lại.
Đánh giá tinh trùng bằng cách ước lượng: mật độ, tỷ lệ di động.

		</p>
		</div>
	  </div>
	  
	  
	
	
	

    </div><!-- /.container -->
	