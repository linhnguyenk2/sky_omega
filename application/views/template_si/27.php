
    <div class="container" style=" overflow: auto; height: 100%; ">

	 <div class="row">
		  <div class="si-header col-md-2">
			<p>
				<label class="left">Sở Y tế:</label>
				<span class="left2"><input type="text"/></span>
			</p>
			<p>
				<label class="left">BV:</label>
				<span class="left2"><input type="text"/></span>
			</p>
			<p>
				<label class="left">Khoa:</label>
				<span class="left2"><input type="text"/></span>
			</p>
			
		  </div>
		  <div class="si-header col-md-10">
			<h3>PHIẾU ĐÁNH GIÁ TÌNH TRẠNG DINH DƯỠNG</h3>
			<p>(Dùng cho người bệnh >= 18 tuổi, không mang thai)</p>
		  </div>
		 
		  
		  
	  </div>
	  
	  <div class="container">
		  
		<div class="row">
		<div class="col-md-4">
			<label class="left">Mã phiếu:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Mã BN:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		</div>

			<div class="row">
			<div class="col-md-8">
				<label class="left">- Họ tên người bệnh:</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-3">
				<label class="left">Tuổi:</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-1">
				<label class="left">Nam/Nữ:</label>
			</div>
		  </div>
	  
		   <div class="row">
				<div class="col-md-12">
					<label class="left">- Chuẩn đoán:</label>
					<span class="left2"><input type="text"/></span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="row">
					<div class="col-sm-9">
						<label class="left">- Cân nặng vào viện:</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-sm-3">kg</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="row">
					<div class="col-sm-9">
						<label class="left">Chiều cao:</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-sm-3">cm</div>
					</div>
				</div>
				<div class="col-md-4">
					<label class="left">Chỉ số khối cơ thể (BMI):</label>
					<span class="left2"><input type="text"/></span>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<h4>1. Sàng lọc nguy cơ dinh dưỡng:</h4>
				</div>
				
			</div>
			<div class="row">
				  <div class="col-md-12 si-table-basic">
					<table style="width:100%">
					  <tr>
						<th>Yếu tố nguy cơ</th>
						<th class="center">Không</th> 
						<th class="center">Có</th>
					  </tr>
					  <tr>
						<td>BMI < 20.5 kh/m2</td>
						<td class="center"><input type="checkbox"></td>
						<td class="center"><input type="checkbox"></td>
					  </tr>
					  <tr>
						<td>Sụt cân trong 1 tháng qua</td>
						<td class="center"><input type="checkbox"></td>
						<td class="center"><input type="checkbox"></td>
					  </tr>
					  <tr>
						<td>Lượng ăn sụt giảm trong tuần qua</td>
						<td class="center"><input type="checkbox"></td>
						<td class="center"><input type="checkbox"></td>
					  </tr>
					  <tr>
						<td>Bệnh nặng hạn chế đi lại</td>
						<td class="center"><input type="checkbox"></td>
						<td class="center"><input type="checkbox"></td>
					  </tr>
					  <tr>
						<td>Kết luận: nguy cơ suy dinh dưỡng (Khi có ít nhất 1 yếu tố nguy cơ)</td>
						<td class="center"><input type="checkbox"></td>
						<td class="center"><input type="checkbox"></td>
					  </tr>
					  <tr>
						<td>Chỉ định</td>
						<td class="center"><input type="checkbox"><br/>Tái sàng lọc sau 1 tuần</td>
						<td class="center"><input type="checkbox"><br/>Đánh giá tình trạng dinh dưỡng</td>
					  </tr>
					  
					</table>
				  </div>
			  </div>
			  
			  <div class="row">
				<div class="col-md-12">
					<h4>2. Đánh giá tình trạng dinh dưỡng:</h4>
				</div>
				
			</div>
			<div class="row">
				  <div class="col-md-12 si-table-basic">
					<table style="width:100%">
					  <tr>
						<td>Chỉ số khối cơ thể (BMI)</td>
						<td class="right">
						<p>>=20.5kg/m2</p>
						<p>18.5-20.4kg/m2</p>
						<p><18.5 kg/m2</p>
						</td> 
						<td>
							<p><input type="checkbox"> 0 điểm</p>
							<p><input type="checkbox"> 1 điểm</p>
							<p><input type="checkbox"> 2 điểm</p>
						</td>
					  </tr>
					  
					  <tr>
						<td>Sụt cân</td>
						<td class="right">
						<p>Không sụt cân</p>
						<p>5% - 9% trong 1 tháng qua</p>
						<p>>=10 % trong 1 tháng qua</p>
						</td> 
						<td>
							<p><input type="checkbox"> 0 điểm</p>
							<p><input type="checkbox"> 1 điểm</p>
							<p><input type="checkbox"> 2 điểm</p>
						</td>
					  </tr>
					  <tr>
						<td>Lượng ăn</td>
						<td class="right">
						<p>Không giảm hoặc giảm nhẹ</p>
						<p>Giảm >=5% trong tuần qua</p>
						<p>Giảm >=75% trong tuần qua</p>
						</td> 
						<td>
							<p><input type="checkbox"> 0 điểm</p>
							<p><input type="checkbox"> 1 điểm</p>
							<p><input type="checkbox"> 2 điểm</p>
						</td>
					  </tr>
					  <tr>
						<td>Bệnh lý</td>
						<td class="right">
						<p>Bệnh nhẹ - trung bình</p>
						<p>Bệnh nặng (đại phẫu, TBMMN, nhiễm trùng nặng, ung thư...)</p>
						<p>Bệnh rất nặng (chấn thương nặng, chăm sóc tích cực ...)</p>
						</td> 
						<td>
							<p><input type="checkbox"> 0 điểm</p>
							<p><input type="checkbox"> 1 điểm</p>
							<p><input type="checkbox"> 2 điểm</p>
						</td>
					  </tr>
					  <tr>
						<td>Kết luận</td>
						<td class="right">
						<p>< 2 điểm</p>
						<p>>= 2 điểm</p>
						</td> 
						<td>
							<p><input type="checkbox"> Không SDD</p>
							<p><input type="checkbox"> Suy dinh dưỡng</p>
						</td>
					  </tr>
					  
					  
					</table>
				  </div>
			  </div>
			  
			  <div class="row">
				<div class="col-md-12">
					<h4>3. Kế hoạch can thiệp:</h4>
				</div>
				
			</div>
			<div class="row">
				  <div class="col-md-12 si-table-basic">
					<table style="width:100%">
					  <tr>
						<td>Chỉ định chế độ ăn</td>
						<td class="right">
						Mã số:
						</td> 
						<td>
							
						</td>
					  </tr>
					  
					  <tr>
						<td>Đường nuôi ăn</td>
						<td class="right">
						<p>Đường miệng</p>
						<p>Ống thông</p>
						<p>Tĩnh mạch</p>
						</td> 
						<td>
							<p><input type="checkbox"></p>
							<p><input type="checkbox"></p>
							<p><input type="checkbox"></p>
						</td>
					  </tr>
					  <tr>
						<td>Mời hội chẩn dinh dưỡng</td>
						<td class="right">
						<p>Có</p>
						<p>Không</p>
						</td> 
						<td>
							<p><input type="checkbox"></p>
							<p><input type="checkbox"></p>
						</td>
					  </tr>
					  <tr>
						<td>Tái đánh giá</td>
						<td class="right">
						<p>Sau 7 ngày (ở người không suy dinh dưỡng)</p>
						<p>Sau 3 ngày (ở người suy dinh dưỡng)</p>
						</td> 
						<td>
							<p><input type="checkbox"></p>
							<p><input type="checkbox"></p>
						</td>
					  </tr>
					  
					  
					  
					</table>
				  </div>
			  </div>

			
			
		</div>
	  
	  
	 <div class="row">
		<div class="col-md-offset-7 col-md-5">
			<label><span>Ngày<span>&emsp;<span>tháng<span>&emsp;<span>năm 20<span></label>
		</div>
	</div>
	<div class="row">
		
		<div class="col-md-offset-7 col-md-5">
			<label class="left"><b>BÁC SĨ KHÁM BỆNH</b><br>(Ký tên ghi rõ họ tên)</label>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
		</div>
		<div class="col-md-offset-7 col-md-5">
			<label class="left">Họ tên</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
	 </div>
	  
	 <hr>
	  
	<div class="row">
		<div class="col-md-12 center">
			<h3>HƯỚNG DẪN ĐÁNH GIÁ TÌNH TRẠNG DINH DƯỠNG</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<p><b>Đối tượng đánh giá:</b> Tất cả người bệnh nội trú đều cần được sàng lọc và đánh giá tình trạng dinh dưỡng để có kế hoạch can thiệp dinh dưỡng phù hợp.</p>
		<p><b>Thời gian thực hiện:</b> trong vòng 36 giờ sau nhập viện.</p>
		<p><b>Cán bộ thực hiện:</b> Bác sĩ điều trị.</p>
		<p><b>Thời gian tái đánh giá:</b></p>
		<p> - Ở người bệnh không suy dinh dưỡng: tái đánh giá sau mỗi tuần.</p>
		<p>- Ở người bệnh suy dinh dưỡng: tái đánh giá sau mỗi 3 ngày.</p>
		<p><b>Mời hội chẩn dinh dưỡng:</b> Do bác sĩ điều trị quyết định tùy theo từng trường hợp cụ thể.</p>
		<p><b>Ở bệnh nhân phẫu thuật từ loại 2 trở lên:</b> cần phải có kế hoạch can thiệp dinh dưỡng tiền phẫu.</p>
		<p><b>Một số lưu ý trong phần sàng lọc:</b></p>
		<p>- Nếu người bệnh không rõ sụt cần thì có thể hỏi xem người bệnh có các dấu hiệu khác như mặc quần áo rộng hơn, mang nhẫn rộng hơn ...</p>
		<p><b>Một số lưu ý trong phần đánh giá:</b></p>
		<p> - Tỷ lệ sụt cần = (cân nặng thường có - cân nặng hiện tại) x 100/ cân nặng thường có.</p>
		<p>- Lượng thức ăn: ước lượng qua hỏi bệnh sử.</p>
		<p><b>Các dấu hiệu tăng nguy cơ suy dinh dưỡng trên lâm sàng:</b></p>
		<p>- Người bệnh cao tuổi (>= 70 tuổi);</p>
		<p>- Albumin huyết thanh <35 g/l;</p>
		<p>- Số lượng tế bào Lympho < 1500/ mm3;</p>
		<p>- Rối loạn chức năng nuốt (khó nhai, khó nuốt, nuốt sặc).</p>
		
		
		<br/>
		<br/>
		<br/>
		<br/>
		<p>Phiếu đánh giá tình trạng dinh dưỡng do Trưng tâm Dinh dưỡng thành phố Hồ Chi Minh xây dựng với sự hỗ trợ của Quỹ Nhi đồng Liên hiệp quốc UNICEF tại Việt Nam.</p>
		</div>
	</div>
	

    </div><!-- /.container -->
	