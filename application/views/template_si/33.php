
    <div class="container" style=" overflow: auto; height: 100%; ">

	<div class="row">
		<div class="col-md-offset-9 col-md-3">
			<p>MS: 39 / BV-01</p>
			<label class="left">Số vào viện:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
	 </div>

	  
	 <div class="row">
		<div class="si-header col-md-12">
			<h3>BẢNG ĐÁNH GIÁ TUỔI THAI</h3>
		</div>
	  </div>

		<div class="row">
		<div class="col-md-6">
			<label class="left">Mã bảng đánh giá:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<label class="left">Mã bệnh nhân:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	
		</div>
	  
	  <div class="row">
		<div class="col-md-8">
			<label class="left">Họ tên bệnh nhân:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-3">
			<label class="left">Tuổi:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-1">
			<label class="left">Nam/Nữ:</label>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-4">
			<label class="left">Cân nặng lúc sanh:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Chiều cao:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Vòng đầu:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-12">
			<label class="left">Chỉ số Apgar:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-6">
			<label class="left">Ngày sinh:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-6">
			<label class="left">Tuổi lúc khám:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  
	  <div class="row">
		  <div class="col-md-offset-2 col-md-8 bd-all">
			  <div class="row">
				<div class="col-md-12">
					<h3 class="center">KẾT LUẬN</h3>
				</div>
			  </div>
			  <div class="row">
				<div class="col-md-6">
					<label class="left">Tuổi thai:</label>
				</div>
			  </div>
			  <div class="row">
				<div class="col-md-4">
					<label class="left">Cân nặng so với tuổi thai:</label>
				</div>
				<div class="col-md-8">
					<p><label><input type="checkbox">Lớn cân</label></p>
					<p><label><input type="checkbox">Đủ cân</label></p>
					<p><label><input type="checkbox">Nhẹ cân</label></p>
				</div>
			  </div>
			  <div class="row">
				<div class="col-md-6">
					<label class="left">Ngày khám:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-6">
					<label class="left">B.S khám - ký tên:</label>
					<span class="left2"><input type="text"/></span>
					<br>
				</div>
				
			  </div>
			  
		  </div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">
			<p>BIỂU ĐỒ LUBCHENCO (Adapted from Lubchnco, L.C., Hansman, C., and Boyd, E.:Pediatrics 34:403,1996:
Battaglia, F.C., and Lubchenco, L.C.: J.Pediatric 71.159.1967)</p>
		</div>
	  </div>
	  <div class="row">
		<div class="col-md-12">
			<h3 class="center">TUỔI THAI (TUẦN)</h3>
		</div>
	  </div>
	  
	  <div class="row">
		<div class="col-md-12">
			<p>MỨC ĐỘ TRƯỜNG THÀNH CỦA THẦN KINH CƠ (New Ballard Score - Reprpduced, with permission, from permission, from Ballard Jlc al: New Ballard Score, expanded to include extermely premaature infants. J Pediatr 1991:119:417. Neonatology 1994-4th cdition).
</p>
		</div>
	  </div>

<div class="row">
		  <div class="col-md-12 si-table-basic">
			<table style="width:100%">
			  <tr>
				<th rowspan="2">Dấu hiệu T.thành của TK cơ</th>
				<th colspan="7">Điểm</th> 
				
				<th>Ghi điểm</th>
			  </tr>
			  <tr>
				
				<td>-1</td>
				<td>0</td>
				<td>1</td>
				<td>2</td>
				<td>3</td>
				<td>4</td>
				<td>5</td>
				<td></td>
			  </tr>
				
				<tr>
					<td>Tư thế</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
			  </tr>
				<tr>
					<td>Góc cổ tay</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
			  </tr>
				<tr>
					<td>Góc khuỷu tay</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
			  </tr>
				<tr>
					<td>Góc khoeo chân</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
			  </tr>
				<tr>
					<td>Dấu khăn quàng</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
			  </tr>
				<tr>
					<td>Dấu gót tài</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
			  </tr>
				<tr>
				<td colspan="6"></td>
				<td colspan="2" class="right">Điểm tổng cộng</td>
				
				<td></td>
			  </tr>

				</table>
			</div>
		</div>

	  
	  <div class="row">
		<div class="col-md-12">
			<h4>MỨC ĐỘ TRƯỞNG THÀNH THỂ CHẤT:</h4>
		</div>
	  </div>
	  
	  
	<div class="row">
		  <div class="col-md-12 si-table-basic">
			<table style="width:100%">
			  <tr>
				<th rowspan="2">D.hiệu t.thành</th>
				<th colspan="7"></th> 
				
				<th>Ghi điểm</th>
			  </tr>
			  <tr>
				
				<td>-1</td>
				<td>0</td>
				<td>1</td>
				<td>2</td>
				<td>3</td>
				<td>4</td>
				<td>5</td>
				<td></td>
			  </tr>
			  <tr>
				<td>Da</td>
				<td>Trong suốt, ẩm ướt</td>
				<td>Trong suốt, đỏ nhầy</td>
				<td>Hồng mịn nhìn thấy máu</td>
				<td>Da dễ bong, hồn ban, thấy ít máu</td>
				<td>Nứt da, da xanh xao, hiếm thấy máu</td>
				<td>Bong da, nứt sâu, không thấy mạch máu</td>
				<td>Da dầy, bong da ở nếp gấp</td>
				<td></td>
			  </tr>
			  <tr>
				<td>Lông tơ</td>
				<td>Không có</td>
				<td>Thưa</td>
				<td>Nhiều</td>
				<td>Mỏng, mịn</td>
				<td>Có những vùng không có lông tơ</td>
				<td>Hầu hết không có lông tơ</td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>Lòng bàn chân</td>
				<td>Gót-ngón: 40-50 mm(-1) <40mm (2)</td>
				<td>> 50 mm không có chỉ chân</td>
				<td>Chỉ chân đỏ mờ nhạt</td>
				<td>Chỉ chân nằm ngang và ở phần trước</td>
				<td>Chỉ chân ở 2/3 trước</td>
				<td>Chỉ chân có toàn bộ bàn chân</td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>Vú</td>
				<td>Không nhận thấy</td>
				<td>Khó nhận thấy</td>
				<td>Quầng vú dẹt, không có mầm vú</td>
				<td>Quầng vú nhô 1-2mm, có mần vú</td>
				<td>Quầng vú 3-4mm, có mềm vú</td>
				<td>Quầng vú đầy 5-10 mm, có mần vú</td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>Mắt/Tai</td>
				<td>Mí mắt nhắm hờ (-1), nhắm chặt (-2)</td>
				<td>Mở mắt, vành tai dẹt và giữ nếp khi gấp</td>
				<td>Vành tai cong, mềm, độ đàn hồi tốt</td>
				<td>Vành tai hình dạng rõ, chắc, đàn hồi nhanh</td>
				<td>Vành tai cong rõ, mềm, độ đàn hồi tốt</td>
				<td>Sụn vành tai dầy, tai cứng</td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>Bộ phận sinh dục nam</td>
				<td>Bìu dái phẳng, không có nếp nhăn</td>
				<td>Bìu dái rỗng, nếp nhăn mờ nhạt</td>
				<td>Tinh hoàn ở ống bên trên, nếp nhăn rất ít.</td>
				<td>Tinh hoàn đang xuống, có vài nếp nhăn</td>
				<td>Tinh hoàn đã xuống bìu, nhiều nếp nhăn</td>
				<td>Tinh hoàn đã xuống hẳn, lủng lẳng, nếp nhăn sâu.</td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>Bộ phận sinh dục nữ</td>
				<td>Lộ âm vật và 2 môi mỏng</td>
				<td>Lộ âm vật và mô bé nhỏ</td>
				<td>Lộ âm vật và môi bé lớn</td>
				<td>Môi lớn và môi bé nhô đều</td>
				<td>Môi lớn lớn hơn môi bé</td>
				<td>Môi lớn che kín môi bé và âm vật</td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td colspan="6"></td>
				<td colspan="2" class="right">Điểm tổng cộng</td>
				
				<td></td>
			  </tr>
			  
			</table>
		  </div>
	  </div>

<div class="row">
		<div class="col-md-12">
			<h4>BẢNG ĐIỂM ĐÁNH GIÁ MỨC ĐỘ TRƯỞNG THÀNH CỦA TRẺ</h3>
	  </div>
</div>

<div class="row">
		  <div class="col-md-12 si-table-basic">
				<table style="width:100%">
					<tr>
						<td>Điểm</td>
						<td>-10</td>
						<td>-5</td>
						<td>0</td>
						<td>5</td>
						<td>10</td>
						<td>15</td>
						<td>20</td>
						<td>25</td>
						<td>30</td>
						<td>35</td>
						<td>40</td>
						<td>45</td>
						<td>50</td>
					</tr>
					<tr>
						<td>Tuần</td>
						<td>20</td>
						<td>22</td>
						<td>24</td>
						<td>26</td>
						<td>28</td>
						<td>30</td>
						<td>32</td>
						<td>34</td>
						<td>36</td>
						<td>38</td>
						<td>40</td>
						<td>42</td>
						<td>44</td>
					</tr>
				</table>
			</div>
		</div>

	

    </div><!-- /.container -->
	