
    <div class="container" style=" overflow: auto; height: 100%; ">

	 <div class="row">
		  <div class="si-header col-md-2">
			<p>
				<label class="left">Sở Y tế:</label>
				<span class="left2"><input type="text"/></span>
			</p>
			<p>
				<label class="left">BV:</label>
				<span class="left2"><input type="text"/></span>
			</p>
			<p>
				<label class="left">Khoa:</label>
				<span class="left2"><input type="text"/></span>
			</p>
			
		  </div>
		  <div class="si-header col-md-10">
			<h3>PHIẾU ĐÁNH GIÁ TÌNH TRẠNG DINH DƯỠNG</h3>
			<p>(Dùng cho phụ nữ mang thai)</p>
		  </div>
		 
		  
		  
	  </div>
	  
	  <div class="container">
		  <div class="row">
			<div class="col-md-6">
				<label class="left">Mã bệnh án</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-6">
				<label class="left">Mã bệnh nhân</label>
				<span class="left2"><input type="text"/></span>
			</div>
		
		  </div>

		  <div class="row">
			<div class="col-md-8">
				<label class="left">- Họ tên:</label>
				<span class="left2"><input type="text"/></span>
			</div>
			<div class="col-md-4">
				<label class="left">Tuổi:</label>
				<span class="left2"><input type="text"/></span>
			</div>
		
		  </div>
	  
		   
			
			<div class="row">
				<div class="col-md-3">
					<label class="left">- Tuổi thai:</label>
					<span class="left2"><input type="text"/></span>
				</div>
				<div class="col-md-1">
					<label class="left">tuần</label>
				</div>
				<div class="col-md-3">
					<label class="left">theo <input type="checkbox"> kinh cuối <input type="checkbox"></label>
					
				</div>
				<div class="col-md-5">
					<label class="left">Siêu âm 3 tháng đầu thai kỳ</label>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label class="left">- Chuẩn đoán:</label>
					<span class="left2"><input type="text"/></span>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4">
					<div class="row">
					<div class="col-sm-9">
						<label class="left">- Cân nặng vào viện:</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-sm-3">kg</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="row">
					<div class="col-sm-9">
						<label class="left">Chiều cao:</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-sm-3">cm</div>
					</div>
				</div>
				<div class="col-md-4">
					<label class="left">BMI trước mang thai:</label>
					<span class="left2"><input type="text"/></span>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4">
					<div class="row">
					<div class="col-sm-9">
						<label class="left">- Cân nặng hiện tại:</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-sm-3">kg</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="row">
					<div class="col-sm-9">
						<label class="left">Chu vi vòng cánh tay:</label>
						<span class="left2"><input type="text"/></span>
					</div>
					<div class="col-sm-3">cm</div>
					</div>
				</div>
				
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<h4>1. Đánh giá tình trạng dinh dưỡng:</h4>
				</div>
				
			</div>
			<div class="row">
				  <div class="col-md-12 si-table-basic">
					<table style="width:100%">
					  <tr>
						<td>BMI trước mang thai</td>
						<td class="right">
							<p>18,5-24,9<p>
							<p>>= 25,0<p>
							<p><18,5<p>
						</td> 
						<td class="right">
							<p><input type="checkbox">0 điểm</p>
							<p><input type="checkbox">1 điểm</p>
							<p><input type="checkbox">2 điểm</p>
						</td>
					  </tr>
					  <tr>
						<td>
							Chu vi<br/>
							vòng cánh tay
						</td>
						<td class="right">
							>= 23 cm<br/>
							<23 cm
						</td>
						<td class="right">
<input type="checkbox">0 điểm<br/>
<input type="checkbox">2 điểm						
						</td>
					  </tr>
					  <tr>
						<td>
Tốc độ tăng cân
						</td>
						<td class="right">
Tăng cân theo khuyến nghị<br>
Tăng cân trên, hoặc dưới mức khuyến nghị
						</td>
						<td class="right">
<input type="checkbox">0 điểm<br>
<input type="checkbox">1 điểm
						</td>
					  </tr>
					  <tr>
						<td>Bệnh kèm theo liên quan dinh dưỡng</td>
						<td class="right">
Không<br>
Tăng huyết áp, đái tháo đường, nghén<br>
nặng, thiếu máu dinh dưỡng, bệnh lý đường tiêu hóa...
						</td>
						<td class="right">
<input type="checkbox">0 điểm<br>
<input type="checkbox">1 điểm</td>
					  </tr>
					  <tr>
						<td>Kết luận</td>
						<td class="right">
					<2 điểm<br>
>= 2 điểm
						</td>
						<td class="right">
						<input type="checkbox"> Bình thường<br>
<input type="checkbox"> Có nguy cơ về dinh dưỡng
</td>
					  </tr>
					  
					  
					</table>
				  </div>
			  </div>
			  
			  
			  
			  <div class="row">
				<div class="col-md-12">
					<h4>2. Kế hoạch can thiệp:</h4>
				</div>
				
			</div>
			<div class="row">
				  <div class="col-md-12 si-table-basic">
					<table style="width:100%">
					  <tr>
						<td>Chỉ định chế độ ăn</td>
						<td class="right">
						Mã số:
						</td> 
						<td>
							
						</td>
					  </tr>
					  
					  <tr>
						<td>Đường nuôi ăn</td>
						<td class="right">
						<p>Đường miệng</p>
						<p>Ống thông</p>
						<p>Tĩnh mạch</p>
						</td> 
						<td>
							<p><input type="checkbox"></p>
							<p><input type="checkbox"></p>
							<p><input type="checkbox"></p>
						</td>
					  </tr>
					  <tr>
						<td>Mời hội chẩn dinh dưỡng</td>
						<td class="right">
						<p>Có</p>
						<p>Không</p>
						</td> 
						<td>
							<p><input type="checkbox"></p>
							<p><input type="checkbox"></p>
						</td>
					  </tr>
					  <tr>
						<td>Tái đánh giá</td>
						<td class="right">
						<p>Sau 7 ngày (ở người không suy dinh dưỡng)</p>
						<p>Sau 3 ngày (ở người suy dinh dưỡng)</p>
						</td> 
						<td>
							<p><input type="checkbox"></p>
							<p><input type="checkbox"></p>
						</td>
					  </tr>
					  
					  
					  
					</table>
				  </div>
			  </div>

			
			
		</div>
		<div class="row">
		<div class="col-md-12">
			<label>PNMT: phụ nữ mang thai</label>
		</div>
	</div> 
	  
	 <div class="row">
		<div class="col-md-offset-7 col-md-5">
			<label><span>Ngày<span>&emsp;<span>tháng<span>&emsp;<span>năm 20<span></label>
		</div>
	</div>
	<div class="row">
		
		<div class="col-md-offset-7 col-md-5">
			<label class="left"><b>BÁC SĨ KHÁM BỆNH</b><br>(Ký tên ghi rõ họ tên)</label>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
		</div>
		<div class="col-md-offset-7 col-md-5">
			<label class="left">Họ tên</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
	 </div>
	  
	 <hr>
	  
	<div class="row">
		<div class="col-md-12 center">
			<h3>HƯỚNG DẪN ĐÁNH GIÁ TÌNH TRẠNG DINH DƯỠNG CHO PHỤ NỮ MANG THAI</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<p><b>Đối tượng đánh giá:</b> Tất cả phụ nữ mang thai khám hoặc năm viện đều cần được đánh giá tình trạng dinh dưỡng để có kế hoạch can thiệp dinh dưỡng kịp thời.</p>
		<p><b>Thời gian thực hiện:</b>Trong lúc khám thai hoặc trong vòng 36 giờ sau nhập viện.</p>
		<p><b>Cán bộ thực hiện:</b> Bác sĩ điều trị.</p>
		<p><b>Thời gian tái đánh giá:</b></p>
		<p> - Ở phụ nữ mang thai không cần can thiệp dinh dưỡng: tái đánh giá sau mỗi tuần</p>
		<p>- Ở phụ nữ mang thai có can thiệp dinh dưỡng: tái đánh giá sau mỗi 3 ngày</p>
		<p><b>Mời hội chẩn dinh dưỡng:</b> Do bác sĩ điều trị quyết định tùy theo từng trường hợp cụ thể.</p>
		<p><b>Lưu ý:</b></p>
		<p>- Nếu phụ nữ mang thai không rõ cân nặng trước khi mang thai: sử dụng BMI trong lần khám thai đầu tiên trong 3 tháng đầu thai kỳ.</p>
		<p>- Bảng khuyến nghị tăng cần trong 6 tháng cuối thai kỳ ở phụ nữ mang thai:
		
		<div class="row">
				  <div class="col-md-12 si-table-basic">
					<table style="width:100%">
					  <tbody>
					  <tr>
							<th colspan="2">BMI trước mang thai</th>
							<th>Tăng cân thai kỳ (kg)</th>
							<th>Khuyến nghị tăng cân trong 6 tháng cuối thai kỳ theo tuần (kg)</th>
							<th>Khuyến nghị tăng cân trong 6 tháng cuối thai kỳ theo tháng (kg)</th>
							
					  </tr>
						<tr>
							<td>SDD</td>
							<td><18.5</td>
							<td>12.5-18</td>
							<td>0.4-0.6</td>
							<td>1.8- 2.7</td>
					  </tr>
						<tr>
							<td>Bình thường</td>
							<td>18.5-24.9</td>
							<td>11.5-16</td>
							<td>0.4-0.5-16</td>
							<td>1.8</td>
					  </tr>
						<tr>
							<td>Thừa cân</td>
							<td>25.0-29.9</td>
							<td>7-11.5</td>
							<td>0.2-0.3</td>
							<td>0.9</td>
					  </tr>
						<tr>
							<td>Béo phì</td>
							<td>>= 30.0</td>
							<td>5-9</td>
							<td>0.2-0.3</td>
							<td>0.7-0.9</td>
					  </tr>
					  
					  
					  
					</tbody></table>
				  </div>
			  </div>

		<br/>
		<br/>
		<br/>
		<br/>
		<p>Phiếu đánh giá tình trạng dinh dưỡng do Trưng tâm Dinh dưỡng thành phố Hồ Chi Minh xây dựng với sự hỗ trợ của Quỹ Nhi đồng Liên hiệp quốc UNICEF tại Việt Nam.</p>
		</div>
	</div>
	

    </div><!-- /.container -->
	