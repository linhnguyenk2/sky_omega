
    <div class="container" style=" overflow: auto; height: 100%; ">

	<div class="row">
		<div class="col-md-offset-9 col-md-3">
			<p>MS: 39 / BV-01</p>
			<label class="left">Số vào viện:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
	 </div>
	<!-- <div class="row">
		<div class="col-md-2">
			<label class="left">KHOA:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		
	 </div> -->
	  
	 <div class="row">
		  <div class="si-header col-md-12">
			<h3>PHIẾU CHĂM SÓC - THEO DÕI</h3>
		
			
		  </div>
		  
	  </div>

		<div class="row">
		<div class="col-md-4">
			<label class="left">Mã phiếu:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Mã bệnh nhân:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-4">
			<label class="left">Khoa:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		</div>
	  
	  <div class="row">
		<div class="col-md-6">
			<label class="left">Họ tên bệnh nhân:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-2">
			<label class="left">Ngày sinh:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-2">
			<label class="left">Trai / Gái</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-2">
			<label class="left">Cân nặng:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  
	  
	  <div class="row">
		<div class="col-md-2">
			<label class="left">Mã số:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-5">
			<label class="left">Ngày nhập viện:</label>
			<span class="left2"><input type="text"/></span>
		</div>
		<div class="col-md-5">
			<label class="left">Chẩn đoán:</label>
			<span class="left2"><input type="text"/></span>
		</div>
	  </div>
	  
	  
	<div class="row">
		  <div class="col-md-12 si-table-basic">
			<table style="width:100%">
			  <tr>
				<th>NGÀY </th>
				<th></th>
				<th>Giờ</th> 
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
			  </tr>
			  <tr>
				<td rowspan="4">Đánh giá</td>
				<td>Thần kinh</td>
				<td>
<pre>
Mức độ tri giác
a) Tỉnh
b) Kích thích có đáp ứng
c) Hôn mê
</pre>
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td rowspan="3">Sinh hiệu</td>
				<td>Nhịp thở/Cơn ngưng thở<br>SaO2 </td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>Nhịp tim/ Huyết áp<br>Màu da </td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>Thân nhiệt<br>/ Nhiệt độ lồng ấp (nếu có)</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td rowspan="11"">Theo dõi</td>
				<td rowspan="6">Có/Không</td>
				<td>Oxy/Dụng cụ</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  
			  
			  <tr>
				<td>CPAP   PEEP</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>Nội khí quản </td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  
			  <tr>
				<td>Bóp bóng</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  
			  <tr>
				<td>Thở máy</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  
			  <tr>
				<td>Hút đàm</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  
			  <tr>
				<td colspan="2">Chiếu đèn</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td colspan="2">Tư thế trẻ</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td colspan="2">Chăm sóc khác</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td colspan="2">Xét nghiệm đã thực hiện</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td colspan="2">Tên điều dưỡng/ Bác sĩ</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  
			  <!--for next paeg.-->
			  <tr>
				<td colspan="2">Giờ thực hiện</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  
			  <tr>
				<td rowspan="8">Thuốc + các loại dịch truyền</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>Dinh dưỡng</td>
				<td>
					<pre>
Loại sữa
Số lượng
Gavage (G)/ Miệng (M)
					</pre>
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  
			  <tr>
				<td rowspan="3">Xuất</td>
				<td>Phân: Số lần</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>Nước tiểu:</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td>Dịch dạ dày:</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td colspan="2">Tên Điều dưỡng thực hiện</td>
				
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			  <tr>
				<td colspan="11">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-11">
									<label class="left">Tổng nhập:</label>
									<label class="left">ca <input type="checkbox"/></label>
									<label class="left">24 giờ <input type="checkbox"/></label>
									<span class="left2"><input type="text"/></span>
								</div>
								<div class="col-xs-1">ml</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="row">
										<div class="col-xs-10">
											<label class="left">Dịch truyền:</label>
											<span class="left2"><input type="text"/></span>
										</div>
										<div class="col-xs-2">
											<label class="left">ml</label>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="row">
										<div class="col-xs-10">
											<label class="left">Máu:</label>
											<span class="left2"><input type="text"/></span>
										</div>
										<div class="col-xs-2">
											<label class="left">ml</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-11">
									<label class="left">Sữa:</label>
									<span class="left2"><input type="text"/></span>
								</div>
								<div class="col-xs-1">
									<label class="left">ml</label>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-11">
									<label class="left">Khác:</label>
									<span class="left2"><input type="text"/></span>
								</div>
								<div class="col-xs-1">
									<label class="left">ml</label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-11">
									<label class="left">Tổng nhập:</label>
									<label class="left">ca <input type="checkbox"/></label>
									<label class="left">24 giờ <input type="checkbox"/></label>
									<span class="left2"><input type="text"/></span>
								</div>
								<div class="col-xs-1">ml</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="row">
										<div class="col-xs-10">
											<label class="left">Phân:</label>
											<span class="left2"><input type="text"/></span>
										</div>
										<div class="col-xs-2">
											<label class="left">ml</label>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="row">
										<div class="col-xs-10">
											<label class="left">Máu:</label>
											<span class="left2"><input type="text"/></span>
										</div>
										<div class="col-xs-2">
											<label class="left">ml</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-11">
									<label class="left">Nước tiểu:</label>
									<span class="left2"><input type="text"/></span>
								</div>
								<div class="col-xs-1">
									<label class="left">ml</label>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="row">
										<div class="col-xs-12">
											<label class="left">Dịch dạ dày:</label>
											<span class="left2"><input type="text"/></span>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="row">
										<div class="col-xs-10">
											<label class="left">Khác:</label>
											<span class="left2"><input type="text"/></span>
										</div>
										<div class="col-xs-2">
											<label class="left">ml</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</td>
			  </tr>
			  <tr>
				<td colspan="11">
					<div class="row">
						<div class="col-md-12">
							<label class="left">Lưu ý:</label>
							<span class="left2"><input type="text"/></span>
						</div>
						<div class="col-md-12">
							<span class="left2"><input type="text"/></span>
						</div>
						<div class="col-md-12">
							<span class="left2"><input type="text"/></span>
						</div>
						<div class="col-md-12">
							<span class="left2"><input type="text"/></span>
						</div>
						<div class="col-md-12">
							<span class="left2"><input type="text"/></span>
						</div>
					</div>
				</td>
			  </tr>
			  <tr>
				<td colspan="11">
					<div class="row">
						<div class="col-md-12">
							<label class="left"><b>Lời dặn kế tiếp:</b></label>
							<span class="left2"><input type="text"/></span>
						</div>
						<div class="col-md-12">
							<label class="left">1. Y lệnh chưa thực hiện:</label>
							<span class="left2"><input type="text"/></span>
						</div>
						<div class="col-md-12">
							<label class="left">2. Các săn sóc và theo dõi đặc biệt:</label>
							<span class="left2"><input type="text"/></span>
						</div>
					</div>
				</td>
			  </tr>
			  <tr>
				<td colspan="11">
					<div class="row">
						<div class="col-md-12">
							<label class="left">3. Thuốc bàn giao</label>
							<span class="left2"><input type="text"/></span>
						</div>
						<div class="col-md-6">
							<label class="left">4. Số phim bàn giao</label>
							<span class="left2"><input type="text"/></span>
						</div>
						<div class="col-md-6">
							<label class="left">5. Khác</label>
							<span class="left2"><input type="text"/></span>
						</div>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
					</div>
				</td>
			  </tr>
			  
			</table>
		  </div>
	  </div>
	  

	

    </div><!-- /.container -->
	
	
	
	