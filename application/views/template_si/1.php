
<div class="container" style=" overflow: auto; height: 100%; ">

    <div class="si-header">
        <h1>PHIẾU ĐĂNG KÝ KHÁM BỆNH</h1>
        <h2>REGISTRATION FORM</h2>
    </div>
    <div class="row">
        <p>
            Nếu đăng ký khám cho trẻ, vui lòng đánh dấu v vào ô "Trẻ em" và điền thông tin của Ba/Mẹ vào mục "Người liên hệ trong trường hợp khẩn cấp". 
            Please tick the "Child" box to register for children and fill in parents infomation in "Emergency contact".
        </p>
    </div>

    <div class="row bg-subshow">
        <div class="col-md-8">
            <h4><b>THÔNG TIN KHÁCH HÀNG</b> / PERSONAL INFORMATION</h4>
        </div>
        <div class="col-md-4"><label class="pdt-10">
            <input type="checkbox" class="auto_for_clone_value">Trẻ em/ Child</label>
            <input type="text" value="" class="input-hindden">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label class="left">Mã phiếu:</label>
            <span class="left2"><input type="text"/></span>
        </div>
        <div class="col-md-6">
            <label class="left">Mã bệnh nhân:</label>
            <span class="left2"><input type="text"/></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label class="left">Họ tên / Full name:(*)</label>
            <!--<span class="left2"><input type="text" data-validation="custom length" data-validation-regexp="^([a-zA-Záàảạ]+\s)*[a-zA-Záàảạ]+$"  data-validation-length="3-200"/></span>-->
            <span class="left2"><input type="text" required/></span>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <label class="left">Ngày sinh / Date of birth:(*)</label>
            <span class="left2"><input type="text" class="datetimepicker-date"/></span>
        </div>
        <div class="col-md-6">
            <label class="left">Tình trạng hôn nhân / Marital Status:</label>
            <span class="left2"><input type="text"/></span>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <label>Giới tính / Gender:(*)</label>&emsp;&emsp;
            <label><input type="radio" name="gender"  value="nam" class="auto_for_clone_value"> Nam / Male</label>&emsp;
            <label><input type="radio" name="gender"  value="nu" class="auto_for_clone_value"> Nữ / Female</label>
            <!-- <label><input show="radio" type="radio" name="gender"  value="nu" class="auto_for_clone_value"> Nữ / Female</label> -->
            <!-- <label><input show="radio" type="radio" name="gender"  value="nam" class="auto_for_clone_value"> Nam / Male</label>&emsp; -->
            <input type="text" value="" class="input-hindden">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label class="left">Quốc tịch / Nationality:(*)</label>
            <!-- <span class="left2"><input type="text" class="country" data-validation="country"id="project" /></span> -->
            <img id="project-icon" src="https://jqueryui.com/resources/demos/autocomplete/images/jqueryui_32x32.png" class="ui-state-default" alt="">
            <input id="project" type="text">
            <input type="hidden" id="project-id">
            <p id="project-description"></p>

        </div>
        <div class="col-md-6">
            <label class="left">Số CMND / Passport Number:</label>
            <span class="left2"><input type="text" data-validation-regexp="^([a-zA-Z0-9]+)$" data-validation-length="7-10"/></span>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <label class="left">Địa chỉ liên hệ / Contact address:</label>
            <span class="left2"><input type="text" data-validation="length" data-validation-length="5-220"/></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label class="left">Địa chỉ email / Email address</label>
            <span class="left2"><input type="text" data-validation="email"/></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label class="left">Số điện thoại NR / Homephone:</label>
            <span class="left2"><input type="text" data-validation="length alphanumeric" data-validation-length="9-12"/></span>
        </div>
        <div class="col-md-6">
            <label class="left">Số điện thoại di động / Mobile:</label>
            <span class="left2"><input type="text"/></span>
        </div>

    </div>

    <div class="row">
        <b class="col-md-12">* Người liên hệ trong trường hợp khẩn cấp / Emergency contact:</b>
    </div>

    
    <div class="row">
        <div class="col-md-12">
            <label class="left">Họ tên / Full name:</label>
            <span class="left2"><input type="text"/></span>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <label class="left">Quan hệ / Relationship:</label>
            <span class="left2"><input type="text"/></span>
        </div>
        <div class="col-md-6">
            <label class="left">Số điện thoại / Phone number:</label>
            <span class="left2"><input type="text"/></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label class="left">Địa chỉ liên hệ / Contact address</label>
            <span class="left2"><input type="text"/></span>
        </div>
    </div>


    <div class="row bg-subshow">
        <div class="col-md-12"><h4><b>THÔNG TIN BẢO HIỂM</b> / HEALTH INSURANCE INFORMATION</h4></div>
    </div>
    <div class="row">
        <div class="col-md-4">Bảo hiểm tự nguyện/ private Insurance:</div>
        <div class="col-md-8">
            <label><input type="radio" name="radio_insurance" value="khong" class="auto_for_clone_value"> Không / No</label> <br>
            <label><input type="radio" name="radio_insurance" value="co" class="auto_for_clone_value"> Có / Yes</label>
            <input type="text" value="" class="input-hindden">
            <p>
                <label class="left">Tên bảo hiểm / Insurance Name:</label>
                <span class="left2"><input type="text"/></span>
            </p>
            <p>
                <span class="left2"><input type="text"/></span>
            </p>
        </div>
    </div>

    <div class="row">
        <b class="col-md-12">* Nếu cần xuất hóa đơn để tiền thanh toán, vui lòng bổ sung thông tin:</b>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label class="left">Tên Công ty:</label>
            <span class="left2"><input type="text"/></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label class="left">Địa chỉ Công ty:</label>
            <span class="left2"><input type="text"/></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label class="left">MST Công ty:</label>
            <span class="left2"><input type="text"/></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">Tôi cam kết sẽ chịu trách nhiệm thanh toán  mọi chi phí phát sinh không do Bảo hiểm chi trả.</div>
        <div class="col-md-12">I hereby acknowledge that will be responsible for all excluded payments in Health Insurance.</div>
    </div>

    <div class="row">
        <div class="col-md-offset-7 col-md-5">
            <label class="left">Ngày / Date (*)</label>
            <span class="left2"><input type="text" class="datetimepicker-datetoday"/></span>
        </div>
        <br/>
        <br/>
        <br/>
        <div class="col-md-offset-8 col-md-4">Chữ ký / Signature (*)<input type="text"/></div>
    </div>



</div><!-- /.container -->


<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
  <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->

<script>
  
  </script>