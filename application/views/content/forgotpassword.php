<!DOCTYPE html>
<html lang="en" class="bg-pink">

<head>
    <meta charset="utf-8" />
    <title>Đăng nhập | SIHospital Management System</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="<?php echo site_url(''); ?>assets/css/font.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo site_url(''); ?>assets/css/app.v1.css" type="text/css" />
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
</head>

<body class="">
    <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
        <div class="container aside-xxl">                         
            <a class="navbar-brand block"><img src="<?php echo site_url(''); ?>assets/images/logo.png" style="margin: 0 auto; height: 68px"> SIHospital</a>
            <section class="panel panel-default bg-white m-t-lg">
                <header class="panel-heading text-center"> <strong>Quyên Mật Khẩu</strong> </header>
                <form class="panel-body wrapper-lg">
                    <div class="form-group"> <label class="control-label">Xin vui lòng nhập địa chỉ Email để lấy lại mật khẩu</label> <input id="txtEmail" type="email" placeholder="" class="form-control input-lg"> </div>
                    <!--<div class="form-group"> <label class="control-label">Password</label> <input type="password" id="txtPassword" placeholder="Password" class="form-control input-lg"> </div>-->
                    <!--<div class="checkbox"> <label> <input type="checkbox"> Keep me logged in </label> </div>--> 
                    <a href="<?php echo site_url('/')?>" class="pull-right m-t-xs"><small>Đăng nhập?</small></a> 
                    <button id="btnSignIn" class="btn btn-primary">Xác nhận</button>                                        
                </form>
            </section>
        </div>
    </section>
    <!-- footer -->
    <footer id="footer">
        <div class="text-center padder">
            <p> <small>SIHospital<br>&copy; 2018</small> </p>
        </div>
    </footer>
    <!-- / footer -->
    <!-- Bootstrap -->
    <!-- App -->
    <script src="<?php echo site_url(''); ?>assets/js/app.v1.js"></script>
    <script src="<?php echo site_url(''); ?>assets/js/app.plugin.js"></script>
    <script src="<?php echo site_url(''); ?>assets/js/handle.js"></script>
</body>

</html>