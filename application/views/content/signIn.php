<!DOCTYPE html>
<html lang="en" class="bg-pink">

    <head>
        <meta charset="utf-8" />
        <title>Đăng nhập | SIHospital Management System</title>
        <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="assets/css/font.css" type="text/css" />
        <link rel="stylesheet" href="assets/css/app.v1.css" type="text/css" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
    </head>

    <body class="">
        <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
            <div class="container aside-xxl">                         
                <a class="navbar-brand block"><img src="assets/images/logo.png" style="margin: 0 auto; height: 68px"> SIHospital</a>
                <section class="panel panel-default bg-white m-t-lg">
                    <header class="panel-heading text-center"> <strong>Đăng Nhập Hệ Thống</strong> </header>
                    <form class="panel-body wrapper-lg">
                        <!--<div class="form-group"> <label class="control-label">Email</label> <input id="txtEmail" type="email" placeholder="" class="form-control input-lg" data-toggle="tooltip" data-placement="right" title=""> </div>-->
                        <div class="form-group"> <label>Tên đăng nhập</label> <input type="text" id="txtEmail" type="email" class="form-control parsley-validated" data-type="email" data-required="true"> </div>
                        <div class="form-group"> <label class="control-label">Mật Khẩu</label> <input type="password" id="txtPassword" placeholder="" class="form-control input-lg"> </div><div class="form-group"> <label class="control-label">Ngôn Ngữ</label> <select name="account" class="form-control m-b"> <option>Tiếng Việt</option> <option>English</option></select> </div>
                        <div class="checkbox"> 
                            <label> <input type="checkbox">Nhớ thông tin đăng nhập của tôi</label> 
                            <div id="showmessage_login" style="padding: 10px; padding-bottom: 0;display:none; ">
                                <p id="mserror1" class="text-info" style="display:none;">Password need> 4 character</p>
                                <p id="mserror2" class="text-info" style="display:none;">Password or email not correct</p>
                            </div>
                        </div> 
                        <a href="./forgotpassword/" class="pull-right m-t-xs"><small>Quên mật khẩu ?</small></a> <button id="btnSignIn" class="btn btn-primary">Đăng Nhập</button>
                        <input type="hidden" id="txtDeviceId" value="<?php echo $deviceId?>">
                    </form>
                </section>
            </div>
        </section>
        <!-- footer -->
        <footer id="footer">
            <div class="text-center padder">
                <p> <small>SIHospital<br>&copy; 2018</small> </p>
            </div>
        </footer>
        <!-- / footer -->
        <!-- Bootstrap -->
        <!-- App -->
        <script src="assets/js/app.v1.js"></script>
        <script src="assets/js/app.plugin.js"></script>
        <script src="assets/js/handle.js"></script>
        <script>
//            $(document).ready(function () {
//                $('form').on('submit', function (e) {
//                    return;
//                    // validation code here
//                    var email = $('#txtEmail').val();
//                    var pass  = $('#txtPassword').val();
//              /      /if()
//                    if (!valid) {
//                        e.preventDefault();
//                    }
//                });
//            });
        </script>
    </body>

</html>