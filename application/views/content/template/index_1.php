                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">                            
                            <div class="m-b-md">
                                <h3 class="m-b-none">Dashboard</h3> <small>Welcome back, John</small> </div>
                            <section class="panel panel-default">
                                <div class="row m-l-none m-r-none bg-light lter">
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x text-info"></i> <i class="fa fa-male fa-stack-1x text-white"></i> </span> <a class="clear" href="/index#"> <span class="h3 block m-t-xs"><strong>520</strong></span> <small class="text-muted text-uc">BN nội trú</small> </a> </div>
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light lt"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x text-warning"></i> <i class="fa fa-bug fa-stack-1x text-white"></i> <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#fff" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="2000" data-target="#bugs" data-update="3000" style="width: 50px; height: 50px; line-height: 50px;"><canvas width="50" height="50"></canvas></span> </span> <a class="clear" href="/index#"> <span class="h3 block m-t-xs"><strong id="bugs">68</strong></span> <small class="text-muted text-uc">BN đang chờ khám</small> </a> </div>
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x text-danger"></i> <i class="fa fa-fire-extinguisher fa-stack-1x text-white"></i> <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#f5f5f5" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="3000" data-target="#firers" data-update="5000" style="width: 50px; height: 50px; line-height: 50px;"><canvas width="50" height="50"></canvas></span> </span> <a class="clear" href="/index#"> <span class="h3 block m-t-xs"><strong id="firers">59</strong></span> <small class="text-muted text-uc">BN chuẩn bị xuất viện</small> </a> </div>
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light lt"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x icon-muted"></i> <i class="fa fa-clock-o fa-stack-1x text-white"></i> </span> <a class="clear" href="/index#"> <span class="h3 block m-t-xs"><strong>50</strong></span> <small class="text-muted text-uc">Số phòng trống</small> </a> </div>
                                </div>
                            </section>
                            <div class="row">
                                <div class="col-md-12">
                                    <section class="panel panel-default">
                                        <header class="panel-heading font-bold">Biểu đồ bệnh nhân khám theo tháng</header>
                                        <div class="panel-body">
                                            <div id="flot-1ine" style="height: 210px; padding: 0px; position: relative;"></div>
                                        </div>
                                        <footer class="panel-footer bg-white no-padder">
                                            <div class="row text-center no-gutter">
                                                <div class="col-xs-3 b-r b-light"> <span class="h4 font-bold m-t block">5,860</span> <small class="text-muted m-b block">Orders</small> </div>
                                                <div class="col-xs-3 b-r b-light"> <span class="h4 font-bold m-t block">10,450</span> <small class="text-muted m-b block">Sellings</small> </div>
                                                <div class="col-xs-3 b-r b-light"> <span class="h4 font-bold m-t block">21,230</span> <small class="text-muted m-b block">Items</small> </div>
                                                <div class="col-xs-3"> <span class="h4 font-bold m-t block">7,230</span> <small class="text-muted m-b block">Customers</small> </div>
                                            </div>
                                        </footer>
                                    </section>
                                </div>
                                <div class="col-md-12">
                                    <section class="panel panel-default">
                                        <header class="panel-heading font-bold">Statistics</header>
                                        <div class="panel-body">
                                            <div id="flot-1ine" style="height: 210px; padding: 0px; position: relative;"></div>
                                        </div>
                                        <footer class="panel-footer bg-white no-padder">
                                            <div class="row text-center no-gutter">
                                                <div class="col-xs-3 b-r b-light"> <span class="h4 font-bold m-t block">5,860</span> <small class="text-muted m-b block">Orders</small> </div>
                                                <div class="col-xs-3 b-r b-light"> <span class="h4 font-bold m-t block">10,450</span> <small class="text-muted m-b block">Sellings</small> </div>
                                                <div class="col-xs-3 b-r b-light"> <span class="h4 font-bold m-t block">21,230</span> <small class="text-muted m-b block">Items</small> </div>
                                                <div class="col-xs-3"> <span class="h4 font-bold m-t block">7,230</span> <small class="text-muted m-b block">Customers</small> </div>
                                            </div>
                                        </footer>
                                    </section>
                                </div>
                                <div class="col-md-12">
                                    <section class="panel panel-default">
                                        <header class="panel-heading font-bold">Biểu đồ bệnh nhân nội trú theo tháng</header>
                                        <div class="bg-light dk wrapper"> <span class="pull-right">Friday</span> <span class="h4">$540<br> <small class="text-muted">+1.05(2.15%)</small> </span>
                                            <div class="text-center m-b-n m-t-sm">
                                                <div class="sparkline" data-type="line" data-height="65" data-width="100%" data-line-width="2" data-line-color="#dddddd" data-spot-color="#bbbbbb" data-fill-color="" data-highlight-line-color="#fff" data-spot-radius="3" data-resize="true" values="280,320,220,385,450,320,345,250,250,250,400,380"></div>                                                
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div> <span class="text-muted">Total:</span> <span class="h3 block">$2500.00</span> </div>
                                            <div class="line pull-in"></div>
                                            <div class="row m-t-sm">
                                                <div class="col-xs-4"> <small class="text-muted block">Market</small> <span>$1500.00</span> </div>
                                                <div class="col-xs-4"> <small class="text-muted block">Referal</small> <span>$600.00</span> </div>
                                                <div class="col-xs-4"> <small class="text-muted block">Affiliate</small> <span>$400.00</span> </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="row" style="display: none;">
                                <div class="col-md-8">
                                    <h4 class="m-t-none">Todos</h4>
                                    <ul class="list-group gutter list-group-lg list-group-sp sortable">
                                        <li class="list-group-item box-shadow" draggable="true"> <a href="/index#" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i> </a> <span class="pull-left media-xs"> <i class="fa fa-sort icon-muted fa m-r-sm"></i> <a href="/index#todo-1" data-toggle="class:text-lt text-success" class="active"> <i class="fa fa-square-o fa-fw text"></i> <i class="fa fa-check-square-o fa-fw text-active text-success"></i> </a> </span>
                                            <div class="clear text-success text-lt" id="todo-1"> Browser compatibility </div>
                                        </li>
                                        <li class="list-group-item box-shadow" draggable="true"> <a href="/index#" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i> </a> <span class="pull-left media-xs"> <i class="fa fa-sort icon-muted fa m-r-sm"></i> <a href="/index#todo-2" data-toggle="class:text-lt text-danger"> <i class="fa fa-square-o fa-fw text"></i> <i class="fa fa-check-square-o fa-fw text-active text-danger"></i> </a> </span>
                                            <div class="clear" id="todo-2"> Looking for more example templates </div>
                                        </li>
                                        <li class="list-group-item box-shadow" draggable="true"> <a href="/index#" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i> </a> <span class="pull-left media-xs"> <i class="fa fa-sort icon-muted fa m-r-sm"></i> <a href="/index#todo-3" data-toggle="class:text-lt"> <i class="fa fa-square-o fa-fw text"></i> <i class="fa fa-check-square-o fa-fw text-active text-success"></i> </a> </span>
                                            <div class="clear" id="todo-3"> Customizing components </div>
                                        </li>
                                        <li class="list-group-item box-shadow" draggable="true"> <a href="/index#" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i> </a> <span class="pull-left media-xs"> <i class="fa fa-sort icon-muted fa m-r-sm"></i> <a href="/index#todo-4" data-toggle="class:text-lt"> <i class="fa fa-square-o fa-fw text"></i> <i class="fa fa-check-square-o fa-fw text-active text-success"></i> </a> </span>
                                            <div class="clear" id="todo-4"> The fastest way to get started </div>
                                        </li>
                                        <li class="list-group-item box-shadow" draggable="true"> <a href="/index#" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i> </a> <span class="pull-left media-xs"> <i class="fa fa-sort icon-muted fa m-r-sm"></i> <a href="/index#todo-5" data-toggle="class:text-lt"> <i class="fa fa-square-o fa-fw text"></i> <i class="fa fa-check-square-o fa-fw text-active text-success"></i> </a> </span>
                                            <div class="clear" id="todo-5"> HTML5 doctype required </div>
                                        </li>
                                        <li class="list-group-item box-shadow" draggable="true"> <a href="/index#" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i> </a> <span class="pull-left media-xs"> <i class="fa fa-sort icon-muted fa m-r-sm"></i> <a href="/index#todo-6" data-toggle="class:text-lt"> <i class="fa fa-square-o fa-fw text"></i> <i class="fa fa-check-square-o fa-fw text-active text-success"></i> </a> </span>
                                            <div class="clear" id="todo-6"> LessCSS compiling </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <section class="panel b-light">
                                        <header class="panel-heading bg-primary dker no-border"><strong>Calendar</strong></header>
                                        <div id="calendar" class="bg-primary m-l-n-xxs m-r-n-xxs"></div>
                                        <div class="list-group"> <a href="/index#" class="list-group-item text-ellipsis"> <span class="badge bg-danger">7:30</span> Meet a friend </a> <a href="/index#" class="list-group-item text-ellipsis"> <span class="badge bg-success">9:30</span> Have a kick off meeting with .inc company </a> <a href="/index#" class="list-group-item text-ellipsis"> <span class="badge bg-light">19:30</span> Milestone release </a> </div>
                                    </section>
                                </div>
                            </div>
                            <div style="display: none">
                                <div class="btn-group m-b" data-toggle="buttons"> <label class="btn btn-sm btn-default active"> <input type="radio" name="options" id="option1"> Timeline </label> <label class="btn btn-sm btn-default"> <input type="radio" name="options" id="option2"> Activity </label> </div>
                                <section class="comment-list block">
                                    <article id="comment-id-1" class="comment-item"> <span class="fa-stack pull-left m-l-xs"> <i class="fa fa-circle text-info fa-stack-2x"></i> <i class="fa fa-play-circle text-white fa-stack-1x"></i> </span>
                                        <section class="comment-body m-b-lg">
                                            <header> <a href="/index#"><strong>John smith</strong></a> shared a <a href="/index#" class="text-info">video</a> to you <span class="text-muted text-xs"> 24 minutes ago </span> </header>
                                            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam.</div>
                                        </section>
                                    </article>
                                    <!-- .comment-reply -->
                                    <article id="comment-id-2" class="comment-reply">
                                        <article class="comment-item"> <a class="pull-left thumb-sm"> <img src="./assets/images/avatar_default.jpg" class="img-circle"> </a>
                                            <section class="comment-body m-b-lg">
                                                <header> <a href="/index#"><strong>John smith</strong></a> <span class="text-muted text-xs"> 26 minutes ago </span> </header>
                                                <div> Morbi id neque quam. Aliquam.</div>
                                            </section>
                                        </article>
                                        <article class="comment-item"> <a class="pull-left thumb-sm"> <img src="./assets/images/avatar.jpg" class="img-circle"> </a>
                                            <section class="comment-body m-b-lg">
                                                <header> <a href="/index#"><strong>Mike</strong></a> <span class="text-muted text-xs"> 26 minutes ago </span> </header>
                                                <div>Good idea.</div>
                                            </section>
                                        </article>
                                    </article>
                                    <!-- / .comment-reply -->
                                    <article id="comment-id-2" class="comment-item"> <span class="fa-stack pull-left m-l-xs"> <i class="fa fa-circle text-danger fa-stack-2x"></i> <i class="fa fa-file-o text-white fa-stack-1x"></i> </span>
                                        <section class="comment-body m-b-lg">
                                            <header> <a href="/index#"><strong>John Doe</strong></a> <span class="text-muted text-xs"> 1 hour ago </span> </header>
                                            <div>Lorem ipsum dolor sit amet, consecteter adipiscing elit.</div>
                                        </section>
                                    </article>
                                    <article id="comment-id-2" class="comment-item"> <span class="fa-stack pull-left m-l-xs"> <i class="fa fa-circle text-success fa-stack-2x"></i> <i class="fa fa-check text-white fa-stack-1x"></i> </span>
                                        <section class="comment-body m-b-lg">
                                            <header> <a href="/index#"><strong>Jonathan</strong></a> completed a task <span class="text-muted text-xs"> 1 hour ago </span> </header>
                                            <div>Consecteter adipiscing elit.</div>
                                        </section>
                                    </article>
                                </section> <a href="/index#" class="btn btn-default btn-sm m-b"><i class="fa fa-plus icon-muted"></i> more</a> </div>
                        </section>
                    </section> <a href="/index#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>