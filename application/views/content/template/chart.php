<section id="content">
    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">UI kit</a></li>
                <li class="active">Chart</li>
            </ul>
            <div class="m-b-md">
                <div class="row">
                    <div class="col-sm-6">
                        <h3 class="m-b-none m-t-sm">Charts</h3> <small>Statistics &amp; graph information</small> </div>
                    <div class="col-sm-6">
                        <div class="text-right text-left-xs">
                            <div class="sparkline m-l m-r-lg pull-right" data-type="bar" data-height="35" data-bar-width="6" data-bar-spacing="2" data-bar-color="#fb6b5b"><canvas width="86" height="35" style="display: inline-block; width: 86px; height: 35px; vertical-align: top;"></canvas></div>
                            <div class="m-t-sm"> <span class="text-uc">New users</span>
                                <div class="h4"><strong>1,120,100</strong></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="panel panel-default">
                <header class="panel-heading font-bold">Site statistics</header>
                <div class="panel-body">
                    <div id="flot-1ine" style="height: 250px; padding: 0px; position: relative;"><canvas class="flot-base" width="1015" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1015px; height: 250px;"></canvas>
                        <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                            <div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 15px; text-align: center;">0</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 105px; text-align: center;">1</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 194px; text-align: center;">2</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 284px; text-align: center;">3</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 373px; text-align: center;">4</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 463px; text-align: center;">5</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 552px; text-align: center;">6</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 642px; text-align: center;">7</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 731px; text-align: center;">8</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 821px; text-align: center;">9</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 907px; text-align: center;">10</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 234px; left: 997px; text-align: center;">11</div>
                            </div>
                            <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                <div class="flot-tick-label tickLabel" style="position: absolute; top: 222px; left: 7px; text-align: right;">0</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; top: 178px; left: 7px; text-align: right;">5</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; top: 135px; left: 1px; text-align: right;">10</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; top: 91px; left: 1px; text-align: right;">15</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; top: 48px; left: 1px; text-align: right;">20</div>
                                <div class="flot-tick-label tickLabel" style="position: absolute; top: 5px; left: 1px; text-align: right;">25</div>
                            </div>
                        </div><canvas class="flot-overlay" width="1015" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1015px; height: 250px;"></canvas></div>
                </div>
                <footer class="panel-footer bg-white">
                    <div class="row text-center no-gutter">
                        <div class="col-xs-3 b-r b-light">
                            <p class="h3 font-bold m-t">5,860</p>
                            <p class="text-muted">Orders</p>
                        </div>
                        <div class="col-xs-3 b-r b-light">
                            <p class="h3 font-bold m-t">10,450</p>
                            <p class="text-muted">Sellings</p>
                        </div>
                        <div class="col-xs-3 b-r b-light">
                            <p class="h3 font-bold m-t">21,230</p>
                            <p class="text-muted">Items</p>
                        </div>
                        <div class="col-xs-3">
                            <p class="h3 font-bold m-t">7,230</p>
                            <p class="text-muted">Customers</p>
                        </div>
                    </div>
                </footer>
            </section>
            <div class="row">
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Multiple</header>
                        <div class="panel-body">
                            <div id="flot-chart" style="height: 240px; padding: 0px; position: relative;"><canvas class="flot-base" width="477" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 477px; height: 240px;"></canvas>
                                <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                                    <div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 14px; text-align: center;">0</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 89px; text-align: center;">1</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 165px; text-align: center;">2</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 240px; text-align: center;">3</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 315px; text-align: center;">4</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 391px; text-align: center;">5</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 466px; text-align: center;">6</div>
                                    </div>
                                    <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 213px; left: 6px; text-align: right;">0</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 182px; left: 6px; text-align: right;">5</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 152px; left: 0px; text-align: right;">10</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 122px; left: 0px; text-align: right;">15</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 91px; left: 0px; text-align: right;">20</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 61px; left: 0px; text-align: right;">25</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 31px; left: 0px; text-align: right;">30</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 1px; left: 0px; text-align: right;">35</div>
                                    </div>
                                </div><canvas class="flot-overlay" width="477" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 477px; height: 240px;"></canvas>
                                <div class="legend">
                                    <div style="position: absolute; width: 80px; height: 30px; top: 13px; right: 13px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div>
                                    <table style="position:absolute;top:13px;right:13px;;font-size:smaller;color:#545454">
                                        <tbody>
                                            <tr>
                                                <td class="legendColorBox">
                                                    <div style="border:1px solid #ccc;padding:1px">
                                                        <div style="width:4px;height:0;border:5px solid rgb(221,221,221);overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="legendLabel">Unique Visits</td>
                                            </tr>
                                            <tr>
                                                <td class="legendColorBox">
                                                    <div style="border:1px solid #ccc;padding:1px">
                                                        <div style="width:4px;height:0;border:5px solid rgb(137,203,78);overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="legendLabel">Page Views</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Real-time update</header>
                        <div class="panel-body">
                            <div id="flot-live" style="height: 240px; padding: 0px; position: relative;"><canvas class="flot-base" width="477" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 477px; height: 240px;"></canvas>
                                <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                                    <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 225px; left: 13px; text-align: right;">0</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 180px; left: 7px; text-align: right;">20</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 135px; left: 7px; text-align: right;">40</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 90px; left: 7px; text-align: right;">60</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 45px; left: 7px; text-align: right;">80</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 1px; left: 0px; text-align: right;">100</div>
                                    </div>
                                </div><canvas class="flot-overlay" width="477" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 477px; height: 240px;"></canvas></div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Vertical bar</header>
                        <div class="panel-body">
                            <div id="flot-bar" style="height: 240px; padding: 0px; position: relative;"><canvas class="flot-base" width="477" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 477px; height: 240px;"></canvas>
                                <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                                    <div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 43px; top: 225px; left: 35px; text-align: center;">10</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 43px; top: 225px; left: 86px; text-align: center;">15</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 43px; top: 225px; left: 138px; text-align: center;">20</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 43px; top: 225px; left: 189px; text-align: center;">25</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 43px; top: 225px; left: 241px; text-align: center;">30</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 43px; top: 225px; left: 292px; text-align: center;">35</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 43px; top: 225px; left: 343px; text-align: center;">40</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 43px; top: 225px; left: 395px; text-align: center;">45</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 43px; top: 225px; left: 446px; text-align: center;">50</div>
                                    </div>
                                    <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 213px; left: 13px; text-align: right;">0</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 170px; left: 7px; text-align: right;">25</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 128px; left: 7px; text-align: right;">50</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 85px; left: 7px; text-align: right;">75</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 43px; left: 0px; text-align: right;">100</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 1px; left: 0px; text-align: right;">125</div>
                                    </div>
                                </div><canvas class="flot-overlay" width="477" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 477px; height: 240px;"></canvas>
                                <div class="legend">
                                    <div style="position: absolute; width: 61px; height: 45px; right: 13px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div>
                                    <table style="position:absolute;right:13px;;font-size:smaller;color:#545454">
                                        <tbody>
                                            <tr>
                                                <td class="legendColorBox">
                                                    <div style="border:1px solid none;padding:1px">
                                                        <div style="width:4px;height:0;border:5px solid #6783b7;overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="legendLabel">Product 1</td>
                                            </tr>
                                            <tr>
                                                <td class="legendColorBox">
                                                    <div style="border:1px solid none;padding:1px">
                                                        <div style="width:4px;height:0;border:5px solid #4fcdb7;overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="legendLabel">Product 2</td>
                                            </tr>
                                            <tr>
                                                <td class="legendColorBox">
                                                    <div style="border:1px solid none;padding:1px">
                                                        <div style="width:4px;height:0;border:5px solid #8dd168;overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="legendLabel">Product 3</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Horizontal bar</header>
                        <div class="panel-body">
                            <div id="flot-bar-h" style="height: 240px; padding: 0px; position: relative;"><canvas class="flot-base" width="477" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 477px; height: 240px;"></canvas>
                                <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                                    <div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 14px; text-align: center;">0</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 86px; text-align: center;">20</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 161px; text-align: center;">40</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 236px; text-align: center;">60</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 311px; text-align: center;">80</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 383px; text-align: center;">100</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 225px; left: 458px; text-align: center;">120</div>
                                    </div>
                                    <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 213px; left: 6px; text-align: right;">5</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 177px; left: 0px; text-align: right;">10</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 142px; left: 0px; text-align: right;">15</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 107px; left: 0px; text-align: right;">20</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 71px; left: 0px; text-align: right;">25</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 36px; left: 0px; text-align: right;">30</div>
                                        <div class="flot-tick-label tickLabel" style="position: absolute; top: 1px; left: 0px; text-align: right;">35</div>
                                    </div>
                                </div><canvas class="flot-overlay" width="477" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 477px; height: 240px;"></canvas>
                                <div class="legend">
                                    <div style="position: absolute; width: 61px; height: 45px; right: 15px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div>
                                    <table style="position:absolute;right:15px;;font-size:smaller;color:#545454">
                                        <tbody>
                                            <tr>
                                                <td class="legendColorBox">
                                                    <div style="border:1px solid none;padding:1px">
                                                        <div style="width:4px;height:0;border:5px solid #6783b7;overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="legendLabel">Product 1</td>
                                            </tr>
                                            <tr>
                                                <td class="legendColorBox">
                                                    <div style="border:1px solid none;padding:1px">
                                                        <div style="width:4px;height:0;border:5px solid #4fcdb7;overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="legendLabel">Product 2</td>
                                            </tr>
                                            <tr>
                                                <td class="legendColorBox">
                                                    <div style="border:1px solid none;padding:1px">
                                                        <div style="width:4px;height:0;border:5px solid #8dd168;overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="legendLabel">Product 3</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Pie Chart</header>
                        <div class="panel-body">
                            <div id="flot-pie" style="height: 240px; padding: 0px; position: relative;"><span class="pieLabel" id="pieLabel0" style="position: absolute; top: 14px; left: 271px;"><div style="font-size:x-small;text-align:center;padding:2px;color:rgb(153,199,206);">Series1<br>16%</div></span><span class="pieLabel" id="pieLabel1" style="position: absolute; top: 205px; left: 242px;"><div style="font-size:x-small;text-align:center;padding:2px;color:rgb(153,153,153);">Series2<br>61%</div></span><span class="pieLabel" id="pieLabel2" style="position: absolute; top: 27px; left: 152px;"><div style="font-size:x-small;text-align:center;padding:2px;color:rgb(187,187,187);">Series3<br>23%</div></span></div>
                        </div>
                    </section>
                </div>
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Donut pie</header>
                        <div class="panel-body">
                            <div id="flot-pie-donut" style="height: 240px; padding: 0px; position: relative;"><canvas class="flot-base" width="477" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 477px; height: 240px;"></canvas><canvas class="flot-overlay" width="477" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 477px; height: 240px;"></canvas>
                                <div class="legend">
                                    <div style="position: absolute; width: 51px; height: 45px; top: 5px; right: 5px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div>
                                    <table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454">
                                        <tbody>
                                            <tr>
                                                <td class="legendColorBox">
                                                    <div style="border:1px solid #ccc;padding:1px">
                                                        <div style="width:4px;height:0;border:5px solid rgb(153,199,206);overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="legendLabel">Series1</td>
                                            </tr>
                                            <tr>
                                                <td class="legendColorBox">
                                                    <div style="border:1px solid #ccc;padding:1px">
                                                        <div style="width:4px;height:0;border:5px solid rgb(153,153,153);overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="legendLabel">Series2</td>
                                            </tr>
                                            <tr>
                                                <td class="legendColorBox">
                                                    <div style="border:1px solid #ccc;padding:1px">
                                                        <div style="width:4px;height:0;border:5px solid rgb(187,187,187);overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="legendLabel">Series3</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">
                    <section class="panel panel-default">
                        <header class="panel-heading"> Conversion </header>
                        <div class="panel-body text-center">
                            <h4>62.5<small> hrs</small></h4> <small class="text-muted block">Updated at 2 minutes ago</small>
                            <div class="inline">
                                <div class="easypiechart easyPieChart" data-percent="75" data-line-width="16" data-loop="false" data-size="188" style="width: 188px; height: 188px; line-height: 188px;"> <span class="h2 step">75</span>%
                                    <div class="easypie-text">New</div> <canvas width="188" height="188"></canvas></div>
                            </div>
                        </div>
                        <div class="panel-footer"><small>% of avarage rate of the Conversion</small></div>
                    </section>
                </div>
                <div class="col-lg-4">
                    <section class="panel panel-default">
                        <header class="panel-heading"> Bounce rate </header>
                        <div class="panel-body text-center">
                            <h4><small>last </small>12<small> hrs</small></h4> <small class="text-muted block">yesterday: 20%</small>
                            <div class="inline">
                                <div class="easypiechart easyPieChart" data-percent="25" data-line-width="6" data-loop="false" data-size="188" style="width: 188px; height: 188px; line-height: 188px;"> <span class="h2 step">25</span>%
                                    <div class="easypie-text">Today</div> <canvas width="188" height="188"></canvas></div>
                            </div>
                        </div>
                        <div class="panel-footer"><small>% of change</small></div>
                    </section>
                </div>
                <div class="col-lg-4">
                    <section class="panel panel-default">
                        <header class="panel-heading"> New visitors </header>
                        <div class="panel-body text-center">
                            <h4>3,450</h4> <small class="text-muted block">Worldwide visitors</small>
                            <div class="inline">
                                <div class="easypiechart easyPieChart" data-percent="60" data-line-width="30" data-track-color="#eee" data-bar-color="#afcf6f" data-scale-color="#fff" data-size="188" data-line-cap="butt" style="width: 188px; height: 188px; line-height: 188px;"> <span class="h2 step">60</span>%
                                    <div class="easypie-text">new visits</div> <canvas width="188" height="188"></canvas></div>
                            </div>
                        </div>
                        <div class="panel-footer"><small>% of avarage rate of the visits</small></div>
                    </section>
                </div>
            </div>
        </section>
    </section> 
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
