                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">                            
                            <div class="m-b-md">
                                <h3 class="m-b-none">Datagrid</h3>
                            </div>
                            <section class="panel panel-default">
                                <header class="panel-heading"> DataGrid <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data." data-original-title="" title=""></i> </header>
                                <div class="table-responsive">
                                    <table id="MyStretchGrid" class="table table-striped datagrid m-b-sm">
                                        
                                    </table>
                                </div>
                            </section>
                        </section>
                    </section> 
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
                </section>
                
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    