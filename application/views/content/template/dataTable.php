                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">                            
                            <div class="m-b-md">
                                <h3 class="m-b-none">Datatable</h3>
                            </div>
                            <section class="panel panel-default">
                                <header class="panel-heading"> DataTables <i class="fa fa-info-sign text-muted" data-toggle="tooltip" data-placement="bottom" data-title="ajax to load the data." data-original-title="" title=""></i> </header>
                                <div class="table-responsive">
                                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">                                        
                                        <table class="table table-striped m-b-none dataTable no-footer" data-ride="datatables" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="width: 1045px;">                                            
                                        </table>                                        
                                    </div>
                                </div>
                            </section>
                        </section>
                    </section> 
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
                </section>
                
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>