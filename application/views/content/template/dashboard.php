                <section id="content">
                    <section class="vbox">
                        <section class="scrollable">
                            <div class="wrapper-lg">
                                <h3 class="m-b-xs font-bold m-t-none">Welcome back, John</h3>
                                <div class="text-muted text-sm"><a href="#" class="text-primary font-bold">25</a> Tasks, <a href="#" class="text-primary font-bold">50</a> Messages</div>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active m-l-lg"><a href="#sales" data-toggle="tab">Sales</a></li>
                                <li><a href="#revenue" data-toggle="tab">Revenue</a></li>
                                <li><a href="#orders" data-toggle="tab">Orders</a></li>
                            </ul>
                            <div class="wrapper-lg bg-white b-b b-light">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="sales">
                                        <div id="flot-1ine" style="height: 240px; padding: 0px; position: relative;"><canvas class="flot-base" width="1017" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1017px; height: 240px;"></canvas>
                                            <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                                                <div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 15px; text-align: center;">0</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 105px; text-align: center;">1</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 194px; text-align: center;">2</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 284px; text-align: center;">3</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 374px; text-align: center;">4</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 464px; text-align: center;">5</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 553px; text-align: center;">6</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 643px; text-align: center;">7</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 733px; text-align: center;">8</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 823px; text-align: center;">9</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 909px; text-align: center;">10</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 999px; text-align: center;">11</div>
                                                </div>
                                                <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 211px; left: 7px; text-align: right;">0</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 170px; left: 7px; text-align: right;">5</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 129px; left: 1px; text-align: right;">10</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 87px; left: 1px; text-align: right;">15</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 46px; left: 1px; text-align: right;">20</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 5px; left: 1px; text-align: right;">25</div>
                                                </div>
                                            </div><canvas class="flot-overlay" width="1017" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1017px; height: 240px;"></canvas></div>
                                    </div>
                                    <div class="tab-pane" id="revenue">
                                        <div id="flot-chart" style="width: 100%; height: 240px; padding: 0px; position: relative;"><canvas class="flot-base" width="100" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100px; height: 240px;"></canvas>
                                            <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                                                <div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 14px; top: 240px; left: 8px; text-align: center;">0</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 14px; top: 240px; left: 22px; text-align: center;">1</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 14px; top: 240px; left: 36px; text-align: center;">2</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 14px; top: 240px; left: 50px; text-align: center;">3</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 14px; top: 240px; left: 64px; text-align: center;">4</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 14px; top: 240px; left: 78px; text-align: center;">5</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 14px; top: 240px; left: 92px; text-align: center;">6</div>
                                                </div>
                                                <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 232px; left: 0px; text-align: right;">0</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 217px; left: 0px; text-align: right;">2</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 202px; left: 0px; text-align: right;">4</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 187px; left: 0px; text-align: right;">6</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 172px; left: 0px; text-align: right;">8</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 157px; left: 0px; text-align: right;">10</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 142px; left: 0px; text-align: right;">12</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 127px; left: 0px; text-align: right;">14</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 113px; left: 0px; text-align: right;">16</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 98px; left: 0px; text-align: right;">18</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 83px; left: 0px; text-align: right;">20</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 68px; left: 0px; text-align: right;">22</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 53px; left: 0px; text-align: right;">24</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 38px; left: 0px; text-align: right;">26</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 23px; left: 0px; text-align: right;">28</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 8px; left: 0px; text-align: right;">30</div>
                                                </div>
                                            </div><canvas class="flot-overlay" width="100" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100px; height: 240px;"></canvas>
                                            <div class="legend">
                                                <div style="position: absolute; width: 0px; height: 0px; top: 13px; right: 13px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div>
                                                <table style="position:absolute;top:13px;right:13px;;font-size:smaller;color:#545454">
                                                    <tbody>
                                                        <tr>
                                                            <td class="legendColorBox">
                                                                <div style="border:1px solid #ccc;padding:1px">
                                                                    <div style="width:4px;height:0;border:5px solid rgb(221,221,221);overflow:hidden"></div>
                                                                </div>
                                                            </td>
                                                            <td class="legendLabel">Unique Visits</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="legendColorBox">
                                                                <div style="border:1px solid #ccc;padding:1px">
                                                                    <div style="width:4px;height:0;border:5px solid rgb(137,203,78);overflow:hidden"></div>
                                                                </div>
                                                            </td>
                                                            <td class="legendLabel">Page Views</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="orders">
                                        <div id="flot-live" style="width: 100%; height: 240px; padding: 0px; position: relative;"><canvas class="flot-base" width="100" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100px; height: 240px;"></canvas>
                                            <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                                                <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 232px; left: 0px; text-align: right;">0</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 187px; left: 0px; text-align: right;">20</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 142px; left: 0px; text-align: right;">40</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 98px; left: 0px; text-align: right;">60</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 53px; left: 0px; text-align: right;">80</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 8px; left: 0px; text-align: right;">100</div>
                                                </div>
                                            </div><canvas class="flot-overlay" width="100" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100px; height: 240px;"></canvas></div>
                                    </div>
                                </div>
                                <div class="row text-center no-gutter">
                                    <div class="col-xs-3 b-r b-light"> <span class="h4 font-bold m-t block">5,860</span> <small class="text-muted block">Orders</small> </div>
                                    <div class="col-xs-3 b-r b-light"> <span class="h4 font-bold m-t block">10,450</span> <small class="text-muted block">Sellings</small> </div>
                                    <div class="col-xs-3 b-r b-light"> <span class="h4 font-bold m-t block">21,230</span> <small class="text-muted block">Items</small> </div>
                                    <div class="col-xs-3"> <span class="h4 font-bold m-t block">7,230</span> <small class="text-muted block">Customers</small> </div>
                                </div>
                            </div>
                            <div class="hbox">
                                <aside>
                                    <div class="wrapper-lg">
                                        <h4 class="m-t-none font-bold">Tasks</h4>
                                        <div class="text-center">
                                            <div class="inline m">
                                                <div class="easypiechart text-success easyPieChart" data-percent="25" data-line-width="5" data-track-color="#f0f0f0" data-bar-color="#8ec165" data-rotate="270" data-scale-color="false" data-size="120" data-animate="2000" style="width: 120px; height: 120px; line-height: 120px;"> <span class="h2 step font-bold">25</span>%
                                                    <div class="easypie-text text-muted">task done</div> <canvas width="120" height="120"></canvas></div>
                                                <div class="font-bold m-t">Total 150</div>
                                            </div>
                                            <div class="inline m">
                                                <div class="easypiechart text-info easyPieChart" data-percent="65" data-line-width="5" data-track-color="#f0f0f0" data-bar-color="#4cc0c1" data-rotate="115" data-scale-color="false" data-size="120" data-animate="2000" style="width: 120px; height: 120px; line-height: 120px;"> <span class="h2 step font-bold">65</span>%
                                                    <div class="easypie-text text-muted">in progress</div> <canvas width="120" height="120"></canvas></div>
                                                <div class="font-bold m-t">Total 9</div>
                                            </div>
                                            <div class="inline m">
                                                <div class="easypiechart text-danger easyPieChart" data-percent="50" data-line-width="5" data-track-color="#f0f0f0" data-bar-color="#fb6b5b" data-rotate="180" data-scale-color="false" data-size="120" data-animate="2000" style="width: 120px; height: 120px; line-height: 120px;"> <span class="h2 step font-bold">50</span>%
                                                    <div class="easypie-text text-muted">remaining</div> <canvas width="120" height="120"></canvas></div>
                                                <div class="font-bold m-t">Total 25</div>
                                            </div>
                                        </div>
                                        <h4 class="font-bold m-t m-b">Todos</h4>
                                        <ul class="list-group gutter list-group-lg list-group-sp sortable">
                                            <li class="list-group-item box-shadow" draggable="true"> <a href="#" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i> </a> <span class="pull-left media-xs"> <i class="fa fa-sort icon-muted fa m-r-sm"></i> <a href="#todo-1" data-toggle="class:text-lt text-success" class="active"> <i class="fa fa-square-o fa-fw text"></i> <i class="fa fa-check-square-o fa-fw text-active text-success"></i> </a> </span>
                                                <div class="clear text-success text-lt" id="todo-1"> Browser compatibility </div>
                                            </li>
                                            <li class="list-group-item box-shadow" draggable="true"> <a href="#" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i> </a> <span class="pull-left media-xs"> <i class="fa fa-sort icon-muted fa m-r-sm"></i> <a href="#todo-2" data-toggle="class:text-lt text-danger"> <i class="fa fa-square-o fa-fw text"></i> <i class="fa fa-check-square-o fa-fw text-active text-danger"></i> </a> </span>
                                                <div class="clear" id="todo-2"> Looking for more example templates </div>
                                            </li>
                                            <li class="list-group-item box-shadow" draggable="true"> <a href="#" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i> </a> <span class="pull-left media-xs"> <i class="fa fa-sort icon-muted fa m-r-sm"></i> <a href="#todo-3" data-toggle="class:text-lt"> <i class="fa fa-square-o fa-fw text"></i> <i class="fa fa-check-square-o fa-fw text-active text-success"></i> </a> </span>
                                                <div class="clear" id="todo-3"> Customizing components </div>
                                            </li>
                                            <li class="list-group-item box-shadow" draggable="true"> <a href="#" class="pull-right" data-dismiss="alert"> <i class="fa fa-times icon-muted"></i> </a> <span class="pull-left media-xs"> <i class="fa fa-sort icon-muted fa m-r-sm"></i> <a href="#todo-4" data-toggle="class:text-lt"> <i class="fa fa-square-o fa-fw text"></i> <i class="fa fa-check-square-o fa-fw text-active text-success"></i> </a> </span>
                                                <div class="clear" id="todo-4"> The fastest way to get started </div>
                                            </li>
                                        </ul>
                                    </div>
                                </aside>                                
                            </div>
                        </section>
                    </section> 
                <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>