                <section id="content">
                    <section class="vbox si-content">
                        <section class="scrollable padder">    
                            <br>                        
                            <div class="m-b-md">
                                <h3 class="m-b-none">Bảng Điều Khiển</h3> <small>Xin chào </small> 
                            </div>
                            <section class="panel panel-default">
                                <div class="row m-l-none m-r-none bg-light lter">
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x text-info"></i> <i class="fa fa-user-md fa-stack-1x text-white"></i> </span> <a class="clear" href="#"> <span class="h3 block m-t-xs"><strong>100</strong></span> <small class="text-muted text-uc">Bệnh nhân đang nằm viện</small> </a> </div>
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light lt"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x text-warning"></i> <i class="fa fa-stethoscope fa-stack-1x text-white"></i>  </span> <a class="clear" href="#"> <span class="h3 block m-t-xs"><strong id="bugs">20968</strong></span> <small class="text-muted text-uc">Bệnh nhân đang chờ khám</small> </a> </div>
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x text-danger"></i> <i class="fa fa-exchange fa-stack-1x text-white"></i> </span> <a class="clear" href="#"> <span class="h3 block m-t-xs"><strong id="firers">13359</strong></span> <small class="text-muted text-uc">Bệnh nhân đang chờ xuất viện</small> </a> </div>
                                    <!--<div class="col-sm-6 col-md-3 padder-v b-r b-light"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x text-danger"></i> <i class="fa fa-exchange fa-stack-1x text-white"></i> <span class="easypiechart pos-abt easyPieChart" data-percent="100" data-line-width="4" data-track-color="#f5f5f5" data-scale-color="false" data-size="50" data-line-cap="butt" data-animate="3000" data-target="#firers" data-update="5000" style="width: 50px; height: 50px; line-height: 50px;"><canvas width="50" height="50"></canvas><canvas width="50" height="50"></canvas></span> </span> <a class="clear" href="#"> <span class="h3 block m-t-xs"><strong id="firers">13359</strong></span> <small class="text-muted text-uc">Bệnh nhân đang chờ xuất viện</small> </a> </div>-->
                                    <div class="col-sm-6 col-md-3 padder-v b-r b-light lt"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x icon-muted"></i> <i class="fa fa-rotate-left fa-stack-1x text-white"></i> </span> <a class="clear" href="#"> <span class="h3 block m-t-xs"><strong>10</strong></span> <small class="text-muted text-uc">Phòng nội trú đang trống</small> </a> </div>
                                </div>
                            </section>
                            <div class="row">
                                <div class="col-md-12">
                                    <section class="panel panel-default">
                                        <header class="panel-heading font-bold">Biểu đồ số lượng bệnh nhân nội trú trong 30 ngày</header>
                                        <div class="panel-body">
                                            <div id="flot-1ine" style="height: 210px; padding: 0px; position: relative;"><canvas class="flot-base" width="1151" height="210" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1151px; height: 210px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 53px; top: 194px; left: 15px; text-align: center;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 95px; top: 194px; left: 117px; text-align: center;">1</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 53px; top: 194px; left: 219px; text-align: center;">2</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 95px; top: 194px; left: 321px; text-align: center;">3</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 53px; top: 194px; left: 423px; text-align: center;">4</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 95px; top: 194px; left: 525px; text-align: center;">5</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 53px; top: 194px; left: 626px; text-align: center;">6</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 95px; top: 194px; left: 728px; text-align: center;">7</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 53px; top: 194px; left: 830px; text-align: center;">8</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 95px; top: 194px; left: 932px; text-align: center;">9</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 53px; top: 194px; left: 1031px; text-align: center;">10</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 95px; top: 194px; left: 1133px; text-align: center;">11</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 182px; left: 7px; text-align: right;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 146px; left: 7px; text-align: right;">5</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 111px; left: 1px; text-align: right;">10</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 75px; left: 1px; text-align: right;">15</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 40px; left: 1px; text-align: right;">20</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 5px; left: 1px; text-align: right;">25</div></div></div><canvas class="flot-overlay" width="1151" height="210" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1151px; height: 210px;"></canvas></div>
                                        </div>
                                        
                                    </section>
                                </div>
                                
                            </div>
<div class="row">
                                <div class="col-md-12">
                                    <section class="panel panel-default">
                                        <header class="panel-heading font-bold">Biểu đồ số lượng bệnh nhân ngoại trú trong 30 ngày</header>
                                        <div class="panel-body">
                                            <div class="flot-1ine" style="height: 240px; padding: 0px; position: relative;"><canvas class="flot-base" width="1017" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1017px; height: 240px;"></canvas>
                                            <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                                                <div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 15px; text-align: center;">0</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 105px; text-align: center;">1</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 194px; text-align: center;">2</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 284px; text-align: center;">3</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 374px; text-align: center;">4</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 464px; text-align: center;">5</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 553px; text-align: center;">6</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 643px; text-align: center;">7</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 733px; text-align: center;">8</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 823px; text-align: center;">9</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 909px; text-align: center;">10</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 84px; top: 224px; left: 999px; text-align: center;">11</div>
                                                </div>
                                                <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 211px; left: 7px; text-align: right;">0</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 170px; left: 7px; text-align: right;">5</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 129px; left: 1px; text-align: right;">10</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 87px; left: 1px; text-align: right;">15</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 46px; left: 1px; text-align: right;">20</div>
                                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 5px; left: 1px; text-align: right;">25</div>
                                                </div>
                                            </div><canvas class="flot-overlay" width="1017" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1017px; height: 240px;"></canvas></div>
                                        </div>
                                        
                                    </section>
                                </div>
                                
                            </div>

                            <div class="row">
                           <div class="col-md-6">
                              <section class="panel panel-default">
                                 <header class="panel-heading font-bold">Biểu đồ thống kê tình trạng loại phòng</header>
                                 <div class="panel-body">
                                    <div id="flot-bar-demo" style="height: 240px; padding: 0px; position: relative;">
                                       
                                    </div>
                                 </div>
                              </section>
                           </div>
                           <div class="col-md-6">
                              <section class="panel panel-default">
                                 <header class="panel-heading font-bold">Biểu đồ hiệu suất làm việc trong ngày 14/07/2018</header>
                                 <div class="panel-body">
                                    <div id="flot-bar-demo1" style="height: 240px; padding: 0px; position: relative;">
                                       
                                    </div>
                                 </div>
                              </section>
                           </div>
                           
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <section class="panel panel-default">
                                        <header class="panel-heading font-bold">Biểu đồ thống kê hiệu suất loại dịch vụ khám thai trong 30 ngày</header>
                                        <div class="panel-body">
                                            <div class="flot-3ine" style="height: 240px; padding: 0px; position: relative;">
                                            <canvas class="flot-base" width="1017" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1017px; height: 240px;"></canvas>
                                            <!-- <canvas class="flot-overlay" width="1017" height="240" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1017px; height: 240px;"></canvas></div> -->
                                        </div>
                                        
                                    </section>
                                </div>
                                
                            </div>
                            
                            
                        </section>
                    </section> <a href="/index#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
				</section>
                