<section id="content">
    <section class="vbox si-content" id="form_group" data-cat="list_materials">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li>
                    <a href="index">
                        <i class="fa fa-home"></i> Home</a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-th-list"></i> Các biểu mẫu</a>
                </li>
                <!-- <li class="active">list_materials</li> -->
                <li class="active"><?=$title?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">
            <div class="m-b-md">
                <!-- <h3 class="m-b-none">list_materials</h3> -->
                <h3 class="m-b-none"><?=$title?></h3>
            </div>


            <!-- <div class="doc-buttons" style="text-align: right;">
                <a href="#" class="btn btn-s-md btn-warning" data-toggle="modal" data-target="#myAdd" id="btn_add">
                    
                    <i class="fa fa-plus"></i> Thống kê</a>
                <a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myAdd" id="btn_add">
                    
                    <i class="fa fa-plus"></i> Thêm</a>
                <a href="#" class="btn btn-s-md btn-info disabled" data-toggle="modal" data-target="#myEdit" id="btn_edit">Sửa</a>
                <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete">Xóa</a>
                <!-- <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myPush" id="btn_publish">
                    <i class="fa fa-check"></i> Publish</a>
                <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myUnpush" id="btn_unpublish">UnPublish</a> -->
            </div -->

            <?php
            $this->load->view('form/template_event','');
            ?>

            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <div id="DataTables_Table_0" class="dataTables_wrapper no-footer">
                                <table data-example="example_more" class="table table-striped m-b-none" style="width:100%">
                                    <thead>
                                        <tr>
                                            <!-- <th style="">ID</th> -->
                                            <th></th>
                                            <th>Mã loại phòng</th>
                                            <th>Tên loại phòng</th>
                                            <th>Giá tiền</th>
                                            <th>Tiêu chuẩn ngày</th>
                                            <th>Loại phòng</th>
                                            <th>Số lượng phòng</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row" class="odd">
                                        <!-- <td>1</td> -->
                                        <td><input type="checkbox"></td>
                                        <td>1</td>
                                        <td>Phòng Đặc Biệt 1</td>
                                        <td>2.600.000</td>
                                        <td>6 Chai suối nhỏ</td>
                                        <td>2 giường</td>
                                        <td>2</td>
                                    </tr>
                                    <tr role="row" class="odd">
                                        <!-- <td>1</td> -->
                                        <td><input type="checkbox"></td>
                                        <td>2</td>
                                        <td>Phòng Đặc Biệt 2</td>
                                        <td>2.200.000</td>
                                        <td>6 Chai suối nhỏ</td>
                                        <td>2 giường</td>
                                        <td>4</td>
                                    </tr>
                                    <tr role="row" class="odd">
                                        <!-- <td>1</td> -->
                                        <td><input type="checkbox"></td>
                                        <td>3</td>
                                        <td>Phòng loại 1</td>
                                        <td>1.300.000</td>
                                        <td>4 Chai suối nhỏ</td>
                                        <td>1 giường</td>
                                        <td>4</td>
                                    </tr>
                                    <tr role="row" class="odd">
                                        <!-- <td>1</td> -->
                                        <td><input type="checkbox"></td>
                                        <td>4</td>
                                        <td>Phòng loại 2</td>
                                        <td>1.000.000</td>
                                        <td>4 Chai suối nhỏ</td>
                                        <td>2 giường</td>
                                        <td>4</td>
                                    </tr>
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </section>
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Chỉnh sửa &nbsp;</h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <?php echo $content_of_form;?>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning">Lưu & In</button>
                                    <button type="button" class="btn btn-primary">Lưu</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add" style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Thêm mới &nbsp;</h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <?php echo $content_of_form;?>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning">Lưu & In</button>
                                    <button type="button" class="btn btn-primary">Lưu</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="" id="delete" style="">
                    <div class="modal fade" id="myDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Delete</h4>
                                </div>
                                <div class="modal-body">

                                    <form class="form-horizontal" data-validate="parsley">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Delete ID</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="push" style="">
                    <div class="modal fade" id="myPush" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Push</h4>
                                </div>
                                <div class="modal-body">

                                    <form class="form-horizontal" data-validate="parsley">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Push ID</label>
                                            <div class="col-sm-9">
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Push</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="unpush" style="">
                    <div class="modal fade" id="myUnpush" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">UnPush</h4>
                                </div>
                                <div class="modal-body">

                                    <form class="form-horizontal" data-validate="parsley">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">UnPush ID</label>
                                            <div class="col-sm-9">
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">UnPush</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
</section>