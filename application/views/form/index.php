<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Các Biểu Mẫu</a></li>
                <li class="active"><?= $title;?></li>
                
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <h3 class="m-b-none"><?= $title;?></h3>
            </div>
			
			<div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <div id="DataTables_Table_0" class="dataTables_wrapper no-footer">                                        
								<table data-example="example_more" class="table table-striped m-b-none" style="width:100%"> <thead> <tr> <th style="">ID</th> <th></th> <th>Tên Quốc Gia</th> <th>Category</th></tr> </thead> 
								<tbody>
									<tr role="row" class="odd">
										<td>1</td><td><input type="checkbox"></td>
										<td>Phiếu đăng ký khám bệnh</td>
										<td>2018-04-29 00:00:00</td>
									</tr>
									<tr role="row" class="odd">
										<td>2</td><td><input type="checkbox"></td>
										<td>Phiếu khám nhũ</td>
										<td>2018-04-29 00:00:12</td>
									</tr>
									<tr role="row" class="odd">
										<td>3</td><td><input type="checkbox"></td>
										<td>Phiếu khám hiến muộn</td>
										<td>2018-04-29 00:00:12</td>
									</tr>
									<tr role="row" class="odd">
										<td>4</td><td><input type="checkbox"></td>
										<td>Phiếu khám bệnh mãn kinh</td>
										<td>2018-04-29 00:00:12</td>
									</tr>
									<tr role="row" class="odd">
										<td>5</td><td><input type="checkbox"></td>
										<td>Phiếu khám khh gia đình</td>
										<td>2018-04-29 00:00:12</td>
									</tr>
									<tr role="row" class="odd">
										<td>6</td><td><input type="checkbox"></td>
										<td>Phiếu khám phụ khoa</td>
										<td>2018-04-29 00:00:12</td>
									</tr>
									<tr role="row" class="odd">
										<td>7</td><td><input type="checkbox"></td>
										<td>Phiếu khám thai</td>
										<td>2018-04-29 00:00:12</td>
									</tr>
									<tr role="row" class="odd">
										<td>8</td><td><input type="checkbox"></td>
										<td></td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>9</td><td><input type="checkbox"></td>
										<td>Phiếu chỉ định dịch vụ</td>
										<td>2018-04-29 00:00:12</td>
									</tr>
									<tr role="row" class="odd">
										<td>10</td><td><input type="checkbox"></td>
										<td>Đơn thuốc</td>
										<td>2018-04-29 00:00:12</td>
									</tr>
									<tr role="row" class="odd">
										<td>11</td><td><input type="checkbox"></td>
										<td>TPCN - MP -VTYT</td>
										<td>2018-04-29 00:00:12</td>
									</tr>
									<tr role="row" class="odd">
										<td>12</td><td><input type="checkbox"></td>
										<td>Phiếu thu tiền tạm ứng điều trị nội trú</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>13</td><td><input type="checkbox"></td>
										<td>Phiếu thanh toán ra viện</td>
										<td></td>
									</tr>
									
									<tr role="row" class="odd">
										<td>14</td><td><input type="checkbox"></td>
										<td>Hóa đơn giá trị gia tăng</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>15</td><td><input type="checkbox"></td>
										<td>Xv giá phòng và giường nội trú điều trị bệnh nhân</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>16</td><td><input type="checkbox"></td>
										<td>Phiếu khám bệnh examination form</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>17</td><td><input type="checkbox"></td>
										<td>Đơn đề nghị thực hiện KTHTSS với phương pháp xin trứng</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>18</td><td><input type="checkbox"></td>
										<td>Bản cam kết</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>19</td><td><input type="checkbox"></td>
										<td>Đơn tự nguyện cho trứng</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>20</td><td><input type="checkbox"></td>
										<td>Đơn xin tinh trùng (vô danh)</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>21</td><td><input type="checkbox"></td>
										<td>Đơn đề nghị thực hiện kỹ thuật hỗ trợ sinh sản</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>22</td><td><input type="checkbox"></td>
										<td>Đơn tự nguyện cho tinh trùng</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>23</td><td><input type="checkbox"></td>
										<td>Tờ cam kết thực hiện thụ tinh trong ống nghiệm (ivf và icsi)</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>24</td><td><input type="checkbox"></td>
										<td>Hồ sơ theo dõi hỗ trợ sinh sản</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>25</td><td><input type="checkbox"></td>
										<td>Phác đồ điều trị</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>26</td><td><input type="checkbox"></td>
										<td>Bệnh án phụ khoa</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>27</td><td><input type="checkbox"></td>
										<td>Phiếu đánh giá tình trạng dinh dưỡng (Dùng cho người bệnh)</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>28</td><td><input type="checkbox"></td>
										<td>Phiếu chăm sóc</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>29</td><td><input type="checkbox"></td>
										<td>Tờ điều trị</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>30</td><td><input type="checkbox"></td>
										<td>Bệnh án sơ sinh</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>31</td><td><input type="checkbox"></td>
										<td>Phiếu thực hiện và công khai thuốc, dịch truyền</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>32</td><td><input type="checkbox"></td>
										<td>Phiếu chăm sóc - theo dõi</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>33</td><td><input type="checkbox"></td>
										<td>Bảng đánh giá tuổi thai</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>34</td><td><input type="checkbox"></td>
										<td>Bệnh án sản khoa</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>35</td><td><input type="checkbox"></td>
										<td>Phiếu đánh giá tình trạng dinh dưỡng (Dùng cho phụ nữ mang thai)</td>
										<td></td>
									</tr>
									<tr role="row" class="odd">
										<td>36</td><td><input type="checkbox"></td>
										<td>Hồ sơ nhân viên</td>
										<td></td>
									</tr>
									
								</tbody>
								</table>


                            </div>
                        </div>

                    </section>
					<!-- <a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myAdd" id="btn_add"><i class="fa fa-plus"></i> Thêm</a> -->
					
					<div class="" id="add" style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title" id="myModalLabel">View</h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <div id="content_ajax"></div>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
									<button type="button" class="btn btn-warning">Lưu & In</button>
									<button type="button" class="btn btn-primary">Lưu</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
					
                </div>
			</div>
				
		</section>
	</section>
<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>

<aside class="bg-light lter b-l aside-md hide" id="notes">
    <div class="wrapper">Notification</div>
</aside>
</section>
</section>
</section>


<style>
.dataTables_length{
    display: inline-block;
}
table.dataTable tbody tr td:first-child:hover{
	background-color: #f9f9f9!important;
	cursor: pointer;
}
</style>


<script>
	// alert('123')
	$('body ').on('click', '.table-responsive table tbody tr .sorting_1', (function () {
		//alert($(this).html());
		var id_page=$(this).html().trim();
		
		$('#myAdd').modal('show');
		$.ajax({
                url: 'template_si',
                type: 'GET',
                //dataType: "",
                data: {
                    //category_name: $('#category_name').attr('data-cat'),
                    'action': 'loadtemplate',
                    'template_id': id_page,
                },
                beforeSend: function () {

                },
                complete: function () {
                    //$('[data-example="example_more"]').DataTable().ajax.reload(null, false);
                    //table_category_all.page( 'last' ).draw( 'page' );
                    //                    table_category_all.ajax.reload(null, false);
                    //$('[data-example="example_more"]').DataTable(option_config_table);

                    //$('#myAdd').modal('toggle');
                },
                success: function (data) {
                    console.log(data);
					$('#content_ajax').html(data);
                }, error: function (data) {
                    console.log(data);
                    //glb_show_error_ajax(data)
                }
            })
	}));
</script>

