<?php
$lang_list = $list_lang;
$menu=$this->router->fetch_method();


    
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Tiếp nhận</a></li>
                <!-- <li class="active"><?= $title; ?></li> -->
                <li class="active"><?= $lang_list->line($menu); ?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title; ?></h3> -->
                <h3 class="m-b-none"><?= $lang_list->line($menu); ?></h3>
            </div>


            <div class="doc-buttons"> 

                <a  href="<?= site_url('list_patient_add')?>" class="btn btn-s-md btn-primary" ><i class="fa fa-plus"></i> Tạo Bệnh Nhân</a>

                <a style="display:none;" href="#" class="btn btn-s-md btn-info disabled" data-toggle="modal" data-target="#myEdit" id="btn_edit"><?= $lang_list->line('edit') ?></a> 

                <a style="display:none;" href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myPush" id="btn_publish"><i class="fa fa-check"></i> <?= $lang_list->line('publish') ?></a>

                <a style="display:none;" href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myUnpush" id="btn_unpublish"><?= $lang_list->line('unpublish') ?></a>

                <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete"><?= $lang_list->line('delete') ?></a>

            </div>

            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <div id="DataTables_Table_0" class="dataTables_wrapper no-footer">                                        
                                <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
                                <table data-link-api="api/v1/patient/patients"  data-example="example_more" class="table table-striped m-b-none" style="width:100%"> 
                                    <thead>
                                        <tr show-position="2" for-sub-class="go-to-link" for-onclick="patient_information"> 

                                            <th att-name="id" data-sort="asc" class="sortat sorting_asc"><input type="checkbox"/></th> 
                                            <th att-name="user_id.code" ><?= $lang_list->line('code_list_patient') ?></th> 
                                            <th att-name="user_id.fullname"><?= $lang_list->line('name_list_patient') ?></th>  
                                            <th att-name="user_id.passport"><?= $lang_list->line('cmnd_list_patient') ?></th>  
                                            <th att-name="user_id.birthday">Ngày sinh</th>  
                                            <th att-name="user_id.gender" select-static="gender"><?= $lang_list->line('gender_list_patient') ?></th>  
                                            <th att-name="user_id.address"><?= $lang_list->line('address_list_patient') ?></th>  
                                            <th att-name="user_id.ward_id.name">Phường</th>  
                                            <th att-name="user_id.district_id.name"><?= $lang_list->line('district_list_patient') ?></th>  
                                            <th att-name="user_id.province_id.name"><?= $lang_list->line('city_list_patient') ?></th>  

                                            <!-- <th att-name="stat"><?= $lang_list->line('status') ?></th>  -->
                                            <!-- <th att-name="order"><?= $lang_list->line('order') ?></th>  -->
                                        </tr> 
                                    </thead>
                                    <tbody> 
                                        <tr class="go-tol-link" onclick="location.href = './patient_information';">
                                            <td><input type="checkbox"></td>
                                            <td>001</td>
                                            <td>Trần Nguy</td>
                                            <td>022568789</td>
                                            <td>1/1/1995</td>
                                            <td>Nữ</td>
                                            <td>1 Nguyễn Trãi</td>
                                            <td>Hồ Chí Minh</td>
                                            <td>Quận 1</td>
                                            <td>Phường Nguyễn Cư Trinh</td>
                                            <!-- <td>Hiển thị</td> -->
                                            <!-- <td>1</td> -->
                                        </tr>
                                    </tbody> 
                                    <tfoot><tr> 

                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <!-- <th data-type-select="select-status"></th>  -->
                                            <!-- <th></th>  -->
                                        </tr></tfoot>
                                </table>
                                <div class="row">
                                        <div class="col-sm-6">
                                                <div class="dataTables_info" role="status" aria-live="polite">Showing page 1 of 1</div>
                                        </div>
                                        <div class="col-sm-6">
                                                <div class="dataTables_paginate paging_full_numbers"><a class="paginate_button first disabled" data-dt-idx="0" tabindex="0">First</a><a class="paginate_button previous disabled" data-dt-idx="1" tabindex="0">Previous</a><span><a class="paginate_button current" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next" data-dt-idx="7" tabindex="0">Next</a><a class="paginate_button last" data-dt-idx="8" tabindex="0">Last</a></div>
                                        </div>
                                </div>
                            </div>
                        </div>

                    </section>
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document" style="">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('edit_post') ?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('edit_post') ?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation') ?></label>
                                            <div class="col-sm-9"> 
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status') ?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order') ?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save_change') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add"  style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document" style="">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Thêm mới Bệnh Nhân</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="col-sm-12" data-validate="parsley">
                                        
                                        <?php echo $this->load->view('enclitic/template_thongtin_benhnhan',array('list_lang'=>$lang_list),true)?>
                                        
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php //echo $template_delete_push_unpush; ?>
                <?php $this->load->view('list/template_delete_push_unpush',['lang_list'=>$lang_list]);?>


            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>

<style>

</style>