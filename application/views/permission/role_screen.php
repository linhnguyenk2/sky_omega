<?php
$lang_list = $list_lang;



    
?>
<section id="content">
    <section class="hbox stretch vbox si-content" id="tabpermission_role_screen" data-cat="<?= $menu_name_screen ?>">
        <aside class="aside-md bg-white b-r" id="subNav">
            <div class="wrapper b-b header"><?= $lang_list->line('group_role') ?></div>
            <ul class="nav" id="list_role">
                <?php
                foreach ($list_role as $value) {
                    echo '<li class="b-b b-light"><a href="#" data-id=' . $value['id'] . '><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>' . $value['name'] . '</a></li>';
                }
                ?>
                <!-- <li class="b-b b-light"><a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Phasellus at ultricies</a></li>
                <li class="b-b b-light"><a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Malesuada augue</a></li>
                <li class="b-b b-light"><a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Donec eleifend</a></li>
                <li class="b-b b-light"><a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dapibus porta</a></li>
                <li class="b-b b-light"><a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dacus eu neque</a></li>
            </ul> -->
        </aside>
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-8 m-b-xs">
                            <div class="col-sm-12"><?= $lang_list->line('list_screen') ?></div>
                        </div>

                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <table data-example="example_more" class="table table-striped m-b-none" style="width:100%"> 
                                <thead> <tr> <th style="">ID</th> <th><?= $lang_list->line('name_router_main') ?></th><th><?= $lang_list->line('view') ?></th><th><?= $lang_list->line('add') ?></th><th><?= $lang_list->line('edit') ?></th><th><?= $lang_list->line('delete') ?></th><th>Push</th><th><?= $lang_list->line('unpublish') ?></th> </thead> <tbody> </tbody> 
                            </table>

                        </div>
                    </section>
                </section>

            </section>
        </aside>
    </section> 
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
