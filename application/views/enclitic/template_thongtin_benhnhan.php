
<!--start template_thongtin_benhnhan-->
<div class="form-group">
    <label >Mã bệnh nhân</label>
    <input type="input" class="form-control" placeholder="" value="BN001" attr-value="code">
    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
</div>
<div class="form-group">
    <label >Họ và tên</label>
    <input type="input" class="form-control" placeholder="" value="Trần Nguy" attr-value="fullname">
    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
</div>
<div class="form-group">
    <label >Ngày sinh</label>
    <input type="input" class="form-control datetimepicker-date" attr-value="birthday">
</div>
<div class="form-group">
    <label >Tình trạng hôn nhân</label>
    <select class="form-control" attr-value="is_married">
        <option value="0">Chưa lập gia đình</option>
        <option value="1">Đã lập gia đình</option>
    </select>
</div>
<div class="form-group">
    <label >Giới tính</label>
    <select class="form-control" attr-value="gender">
        <option value="0">Nữ</option>
        <option value="1">Nam</option>

    </select>
</div>
<div class="form-group">
    <label >Quốc tịch</label>
    <select class="form-control" api-link="api/v1/list/nations" attr-value="nationality_id">
        <option>Việt Nam</option>
        <option>Lào</option>

    </select>
</div>
<div class="form-group">
    <label >CMND/Passport</label>
    <input type="input" class="form-control" placeholder="" value="022568789" attr-value="passport">
</div>
<div class="row" id="info_address">
    
    <div class="col-sm-3">
        <div class="form-group">
            <label >Thành phố</label>
            <select class="form-control" api-link="api/v1/list/provinces" attr-value="province_id">
                <option>Hồ Chí Minh</option>
                <option>Hà Nội</option>

            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Quận</label>
            <select class="form-control"  api-link="api/v1/list/district" attr-value="district_id" auto-load="false">
                
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Phường</label>
            <select class="form-control" api-link="api/v1//list/ward" attr-value="ward_id" auto-load="false">
                                                        
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Địa chỉ</label>
            <input type="input" class="form-control" placeholder="" value="1 Nguyễn Trãi" attr-value="address">
        </div>
    </div>

</div>
<div class="form-group">
    <label>Email</label>
    <input type="input" class="form-control" placeholder="" value="trannguy@gmail.com" attr-value="email">
</div>
<div class="form-group">
    <label>Số điện thoại</label>
    <input type="input" class="form-control" placeholder="" value="0288895456" attr-value="home_phone">
</div>
<div class="form-group">
    <label>Số điện thoại di động</label>
    <input type="input" class="form-control" placeholder=""  value="090802568785" attr-value="mobile">
</div>

<div class="form-group">
    <label >Nghề nghiệp</label>
    <select class="form-control" api-link="api/v1/list/titles" attr-value="title_id">
        <option>Nhân viên văn phòng</option >
        <option>2</option>
    </select>
</div>


<!--end template_thongtin_benhnhan-->

<script>

//event for change provinces,district,ward
$(function(){
    var at_main_select_address='#info_address ';
    $('body').on('change',at_main_select_address+' select[attr-value="province_id"]',function(){
        var id_current=$(this).val();
        var id_first=$(this).attr('attt-vl');
        
        if(id_first!=''){
            id_current = id_first;
            $(this).attr('attt-vl','')
        }
        var at_province=$(this)
        var param='sih_list_provinces:id='+id_current;
        var link_api_change =$(at_main_select_address+' select[attr-value="district_id"]').attr('api-link')
        var link = link_base+link_api_change+'/?'+param;
        get_data_api(link, {}, function (statusresult, result) {
            var html = build_html_select_option_name(result.data)
            $(at_main_select_address+'select[attr-value="district_id"]').html(html);

            var vl_attr=$(at_main_select_address+'select[attr-value="district_id"]').attr('attt-vl');
            $(at_main_select_address+'select[attr-value="district_id"]').val(vl_attr);

            $(at_main_select_address+'select[attr-value="ward_id"]').html("");
            $(at_main_select_address+'select[attr-value="district_id"]').trigger('change');//trigger
        })
    })
    $('body').on('change',at_main_select_address+'select[attr-value="district_id"]',function(){
        var id_current=$(this).val();
        var id_first=$(this).attr('attt-vl');
        //alert('chage')
        if(id_first!=''){
            id_current = id_first;
            $(this).attr('attt-vl','')
        }
        var param='sih_list_districts:id='+id_current;
        var link_api_change =$(at_main_select_address+'select[attr-value="ward_id"]').attr('api-link')
        var link = link_base+link_api_change+'/?'+param;
        get_data_api(link, {}, function (statusresult, result) {
            var html = build_html_select_option_name(result.data)
            $(at_main_select_address+'select[attr-value="ward_id"]').html(html);

            var vl_attr=$(at_main_select_address+'select[attr-value="ward_id"]').attr('attt-vl');
            $(at_main_select_address+'select[attr-value="ward_id"]').val(vl_attr);
        })
    })
    //ward_id
})
</script>