<div class="form-group">
    <label>Mã bệnh nhân:</label> <span attr-show="user_id.code">BN001</span>
</div>
<div class="form-group">
    <label>Họ và tên:</label> <span attr-show="user_id.fullname">Trần Nguy</span>
</div>
<div class="form-group">
    <label>Ngày sinh:</label> <span attr-show="user_id.birthday">1/1/1995</span>
</div>
<div class="form-group">
    <label>Tình trạng hôn nhân:</label> <span attr-show="user_id.is_married" show-static="is_married">Đã kết hôn</span>
</div>
<div class="form-group">
    <label>Giới tính:</label> <span attr-show="user_id.gender" show-static="gender">Nữ</span>
</div>
<div class="form-group">
    <label>Quốc tịch:</label> <span attr-show="user_id.nationality_id.name">Việt Nam</span>
</div>
<div class="form-group">
    <label>CMND/Passport:</label> <span attr-show="user_id.passport">passport</span>
</div>
<!-- <div class="form-group">
    <label>Mã sổ:</label> <span attr-show="user_id.code">022568789</span>
</div> -->
<div class="form-group">
    <label>Địa chỉ:</label> <span attr-show="user_id.province_id.name">1 Nguyễn Trãi Phường 1 Quận 1 Tp.HCM</span>
</div>
<div class="form-group">
    <label>Email:</label> <span attr-show="user_id.email">trannguy@gmail.com</span>
</div>
<div class="form-group">
    <label>Số điện thoại:</label> <span attr-show="user_id.home_phone">0288895456</span>
</div>
<div class="form-group">
    <label>Số điện thoại di động:</label> <span attr-show="user_id.mobile">090802568785</span>
</div>
<div class="form-group">
    <label>Nghề nghiệp:</label> <span attr-show="user_id.title_id.name">Nhân viên văn phòng</span>
</div>