<?php
$lang_list = $list_lang;



    
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i>  Tiếp Nhận</a></li>
                <!-- <li class="active"><?= $title; ?></li> -->
                <li class="active"><?= $lang_list->line($menu); ?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title; ?></h3> -->
                <h3 class="m-b-none"><?= $lang_list->line($menu); ?></h3>
            </div>


            <div class="doc-buttons"> 

                <a style="display:none;" href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myAdd" id="btn_add"><i class="fa fa-plus"></i> <?= $lang_list->line('add') ?></a>

                <a style="display:none;" href="#" class="btn btn-s-md btn-info disabled" data-toggle="modal" data-target="#myEdit" id="btn_edit"><?= $lang_list->line('edit') ?></a> 

                <a style="display:none;" href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myPush" id="btn_publish"><i class="fa fa-check"></i> <?= $lang_list->line('publish') ?></a>

                <!--<a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShowTab" id="">Test</a>-->
                <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myUnpush" id="btn_unpublish">Tạo phiếu khám</a>

                <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete"><?= $lang_list->line('delete') ?></a>

            </div>

            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <div id="DataTables_Table_0" class="dataTables_wrapper no-footer">                                        
                                <table data-example="example_more" class="table table-striped m-b-none" style="width:100%"> 
                                    <thead>
                                        <tr show-position="2"> 

                                            <th att-name="id"><input type="checkbox"/></th> 
                                            <th att-name="">Mã phiếu khám</th> 
                                            <th att-name="">Mã bệnh nhân</th> 
                                            <th att-name="">Tên bệnh nhân</th> 
                                            <th att-name="">Bệnh lý</th>  
                                            <th att-name="">Thời gian hẹn</th> 
                                        </tr> 
                                    </thead>
                                    <tbody> 
                                        <tr data-toggle="modal" data-target="#myShowTab">
                                            <td><input type="checkbox"></td>
                                            <td>PK001</td>
                                            <td>BN001</td>
                                            <td>Trần Nguy</td>
                                            <td>Khám thai</td>
                                            <td>Đến trực tiếp</td>
                                        </tr>
                                        <tr data-toggle="modal" data-target="#myShowTab">
                                            <td><input type="checkbox"></td>
                                            <td>PK002</td>
                                            <td>BN002</td>
                                            <td>Trần Thi Nhu</td>
                                            <td>Đau Bụng</td>
                                            <td>17:00 19/09/2018</td>
                                        </tr>

                                    </tbody> 
                                    <tfoot><tr> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th data-type-select="select-status"></th> 
                                            <th></th> 
                                        </tr></tfoot>
                                </table>
                            </div>
                        </div>

                    </section>
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('edit_post') ?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('edit_post') ?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation') ?></label>
                                            <div class="col-sm-9"> 
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status') ?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order') ?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save_change') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add"  style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew') ?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('addnew') ?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation') ?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation') ?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status') ?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order') ?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="" id="show"  style="">
                    <div class="modal fade" id="myShowTab" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document" style=" width: 800px; ">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="modal-title text-center" id="myModalLabel">Đăng Ký Khám Bệnh</h3>
                                </div>
                                <div class="modal-body">
                                    <div>

                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#tab_tthc" aria-controls="tab_tthc" role="tab" data-toggle="tab">Thông tin hành chính</a></li>
                                            <li role="presentation"><a href="#tab_dhst" aria-controls="tab_dhst" role="tab" data-toggle="tab">Dấu hiệu sinh  tồn</a></li>
                                            <li role="presentation"><a href="#tab_dvcd" aria-controls="tab_dvcd" role="tab" data-toggle="tab">Dịch vụ chỉ định</a></li>
                                            <li role="presentation"><a href="#tab_balq" aria-controls="tab_balq" role="tab" data-toggle="tab">Bệnh án liên quan</a></li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active form-horizontal" id="tab_tthc">
                                                <br/>
                                                <div class="form-group"> 
                                                    <strong class="col-sm-12">Thông tin đặt hẹn <a class="btn btn-primary" role="button" data-toggle="collapse" href="#thongtin_dathen" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></strong>
                                                </div>
                                                <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_dathen">
                                                    <div class="form-group">
                                                        <label >Mã đặt hẹn</label>
                                                        <input type="input" class="form-control" placeholder="" value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <p >Người đặt hẹn: Nguyễn Thị Lan</p>
                                                        <p >SDT Người đặt hẹn: 0987456258</p>
                                                    </div>
                                                </div>
                                                <!--endsub-->
                                                <div class="form-group"> 
                                                    <strong class="col-sm-12">Thông tin bệnh nhân <a class="btn btn-primary" role="button" data-toggle="collapse" href="#thongtin_benhnhan" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></strong>
                                                </div>
                                                <div class="collapse in " aria-expanded="" id="thongtin_benhnhan">
                                                    <div class="col-sm-12">
                                                        <?php echo $this->load->view('enclitic/template_thongtin_benhnhan',array('list_lang'=>$lang_list),true)?>
                                                    </div>
                                                </div>
                                                <!--endsub-->
                                                <div class="form-group"> 
                                                    <strong class="col-sm-12">Thông tin bệnh lý <a href="#thongtin_benhly"  class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></strong>
                                                </div>
                                                <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_benhly">
                                                    <div class="form-group">
                                                        <label >Bệnh nhân cung cấp thông tin bênh lý</label>
                                                        <!--<input type="input" class="form-control" placeholder="" value="">-->
                                                    </div>
                                                    
                                                </div>
                                                <!--endsub-->
                                                <div class="form-group"> 
                                                    <strong class="col-sm-12">Thông tin thẻ bảo hiểm <a href="#thongtin_thebaohiem"  class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></strong>
                                                </div>
                                                <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_thebaohiem">
                                                    <div class="form-group">
                                                        <label >Số thẻ</label>
                                                        <input type="input" class="form-control" placeholder="" value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label> Ngày hiệu lực</label>
                                                        <select class="form-control" >
                                                            <option>Chọn ngày</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label> Ngày hết hạn</label>
                                                        <select class="form-control" >
                                                            <option>Chọn ngày</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label> Loại thẻ</label>
                                                        <select class="form-control" >
                                                            <option>Chọn loại thẻ</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label> Nhà cung cấp</label>
                                                        <select class="form-control" >
                                                            <option>Chọn nhà cung cấp</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label >Mệnh giá</label>
                                                        <input type="input" class="form-control" placeholder="" value="">
                                                    </div>
                                                </div>
                                                <!--endsub-->
                                                <div class="form-group"> 
                                                    <strong class="col-sm-12">Thông tin xuất hóa đơn công ty <a href="#thongtin_xuathoadoncongty"  class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></strong>
                                                </div>
                                                <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_xuathoadoncongty">
                                                    <div class="form-group">
                                                        <label >MST Công ty</label>
                                                        <input type="input" class="form-control" placeholder="" value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label >Tên công ty</label>
                                                        <input type="input" class="form-control" placeholder="" value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label >Địa chỉ công ty</label>
                                                        <input type="input" class="form-control" placeholder="" value="">
                                                    </div>
                                                    
                                                </div>
                                                <!--endsub-->
                                            </div>
                                            <!--endtab content-->
                                            <div role="tabpanel" class="tab-pane form-horizontal" id="tab_dhst">
                                                <br>
                                                <div class="form-group"> 
                                                    <label class="col-sm-12">Mạch</label>
                                                    <div class="col-sm-7"> <input type="text" value="Nguyễn Thị Lan" class="form-control">  </div>
                                                    <label class="col-sm-4">lần/phút</label>
                                                </div>
                                                <div class="form-group"> 
                                                    <label class="col-sm-12">Nhiệt độ</label>
                                                    <div class="col-sm-7"> <input type="text" value="37" class="form-control">  </div>
                                                    <label class="col-sm-4"><sup>o</sup>C</label>
                                                </div>
                                                <div class="form-group"> 
                                                    <label class="col-sm-12">Nhiệt độ</label>
                                                    <div class="col-sm-7"> <input type="text" value="37" class="form-control">  </div>
                                                    <label class="col-sm-4">mg/hg</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-12">Nhịp thở</label>
                                                    <div class="col-sm-7"> <input type="text" value="80" class="form-control">  </div>
                                                    <label class="col-sm-4">lần/phút</label>
                                                </div>
                                                <div class="form-group"> 
                                                    <label class="col-sm-12">Chiều cao</label>
                                                    <div class="col-sm-7"> <input type="text" value="150" class="form-control">  </div>
                                                    <label class="col-sm-4">cm</label>
                                                </div>
                                                <div class="form-group"> 
                                                    <label class="col-sm-12">Cân nặng</label>
                                                    <div class="col-sm-7"> <input type="text" value="50" class="form-control">  </div>
                                                    <label class="col-sm-4">kg</label>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane form-horizontal" id="tab_dvcd">
                                                <br>
                                                <button type="button" class="btn btn-danger disabled pull-right" data-dismiss="modal">Xóa</button>
                                                <div class="table-responsive">
                                                    <div id="DataTables_Table_1" class="dataTables_wrapper no-footer">                                        
                                                        <table data-example="example_more_test_1" class="table table-striped m-b-none" style="width:100%"> 
                                                            <thead>
                                                                <tr show-position="2"> 
                                                                    <th att-name="id"><input type="checkbox"/></th> 
                                                                    <th att-name="">Mã chỉ định</th> 
                                                                    <th att-name="">Tên dịch vu</th> 
                                                                    <th att-name="">Mã dịch vụ</th> 
                                                                    <th att-name="">Đơn giá</th> 
                                                                </tr> 
                                                            </thead>
                                                            <tbody> 
                                                                <tr data-toggle="modal" data-target="#myTaolichhen" >
                                                                    <td><input type="checkbox"></td>
                                                                    <td>CD001</td>
                                                                    <td>Khám tổng quát</td>
                                                                    <td>DV001</td>
                                                                    <td>120000</td>
                                                                </tr>
                                                                <tr data-toggle="modal" data-target="#myTaolichhen" >
                                                                    <td><input type="checkbox"></td>
                                                                    <td>CD002</td>
                                                                    <td>Siêu âm</td>
                                                                    <td>DV002</td>
                                                                    <td>200000</td>
                                                                </tr>									
                                                            </tbody> 
                                                            <tfoot></tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane form-horizontal" id="tab_balq">
                                                <br>
                                                <div class="form-group"> 
                                                    <label class="col-sm-12">Mã bệnh án</label>
                                                    <div class="col-sm-7"> 
                                                        <select class="form-control">
                                                            <option>Chọn mã bệnh án</option>
                                                            <option>Mã 01</option>
                                                            <option>Mã 02</option>
                                                        </select>  
                                                    </div>

                                                </div>
                                                <div class="form-group"> 
                                                    <label class="col-sm-12">Loại bệnh án: Bệnh án sản khoa</label>
                                                </div>
                                                <div class="form-group"> 
                                                    <label class="col-sm-12">Ngày tạo bênh án: 08/09/2018</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div style="clear: both"></div>
                                <br>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php echo $template_delete_push_unpush; ?>


            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
