
<!--start template_thongtin_benhnhan-->
<div class="form-group">
    <div class="row">
        
        <div class="col-sm-12"><label >Mã bệnh nhân</label></div>
        <div class="col-sm-8">
            <input type="input" id="layma_bn_moi_text" class="form-control" placeholder="" value="BN001" attr-value="code">
        </div>
        <div class="col-sm-4">
            <button type="button" class="btn btn-primary" id="layma_bn_moi" data-dismiss="modal">Lấy mã BN mới</button>
        </div>
    </div>
    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
</div>
<div class="form-group">
    <label >Họ và tên</label>
    <input type="input" class="form-control" placeholder="" value="Trần Nguy" attr-value="fullname">
    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
</div>
<div class="form-group">
    <label >Ngày sinh</label>
    <input type="input" class="form-control datetimepicker-date" attr-value="birthday">
</div>
<div class="form-group">
    <label >Tình trạng hôn nhân</label>
    <select class="form-control" attr-value="is_married">
        <option value="0">Chưa lập gia đình</option>
        <option value="1">Đã lập gia đình</option>
    </select>
</div>
<div class="form-group">
    <label >Giới tính</label>
    <select class="form-control" attr-value="gender">
        <option value="0">Nữ</option>
        <option value="1">Nam</option>

    </select>
</div>
<div class="form-group">
    <label >Quốc tịch</label>
    <select class="form-control" api-link="api/v1/list/nations" attr-value="nationality_id">
        <option>Việt Nam</option>
        <option>Lào</option>

    </select>
</div>
<div class="form-group">
    <label >CMND/Passport</label>
    <input type="input" class="form-control" placeholder="" value="022568789" attr-value="passport">
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label >Địa chỉ</label>
            <input type="input" class="form-control" placeholder="" value="1 Nguyễn Trãi" attr-value="address">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Phường</label>
            <select class="form-control" api-link="api/v1//list/ward" attr-value="ward_id">
                <option>Nguyễn Cư Trinh</option>
                <option>Phường 1</option>                                                
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Quận</label>
            <select class="form-control"  api-link="api/v1/list/district" attr-value="district_id">
                <option>Quận 1</option>
                <option>Quận 2</option>                                                
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Thành phố</label>
            <select class="form-control" api-link="api/v1/list/provinces" attr-value="province_id">
                <option>Hồ Chí Minh</option>
                <option>Hà Nội</option>

            </select>
        </div>
    </div>

</div>
<div class="form-group">
    <label>Email</label>
    <input type="input" class="form-control" placeholder="" value="trannguy@gmail.com" attr-value="email">
</div>
<div class="form-group">
    <label>Số điện thoại</label>
    <input type="input" class="form-control" placeholder="" value="0288895456" attr-value="home_phone">
</div>
<div class="form-group">
    <label>Số điện thoại di động</label>
    <input type="input" class="form-control" placeholder=""  value="090802568785" attr-value="mobile">
</div>

<div class="form-group">
    <label >Nghề nghiệp</label>
    <select class="form-control" api-link="api/v1/list/titles" attr-value="title_id">
        <option>Nhân viên văn phòng</option >
        <option>2</option>
    </select>
</div>


<!--end template_thongtin_benhnhan-->