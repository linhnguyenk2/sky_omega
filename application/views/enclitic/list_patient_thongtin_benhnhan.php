<div class="form-group">
    <label >Mã bệnh nhân</label>
    <input type="input" class="form-control" placeholder="" value="BN001">
    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
</div>
<div class="form-group">
    <label >Họ và tên</label>
    <input type="input" class="form-control" placeholder="" value="Trần Nguy">
    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
</div>
<div class="form-group">
    <label >Ngày sinh</label>
    <select class="form-control" >
        <option>1/1/1995</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
    </select>
</div>
<div class="form-group">
    <label >Tình trạng hôn nhân</label>
    <select class="form-control" >
        <option>Có</option>
        <option>Không</option>
    </select>
</div>
<div class="form-group">
    <label >Giới tính</label>
    <select class="form-control" >
        <option>Nữ</option>
        <option>Nam</option>

    </select>
</div>
<div class="form-group">
    <label >Quốc tịch</label>
    <select class="form-control" >
        <option>Việt Nam</option>
        <option>Lào</option>

    </select>
</div>
<div class="form-group">
    <label >CMND/Passport</label>
    <input type="input" class="form-control" placeholder="" value="022568789">
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label >Địa chỉ</label>
            <input type="input" class="form-control" placeholder="" value="1 Nguyễn Trãi">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Phường</label>
            <select class="form-control" >
                <option>Nguyễn Cư Trinh</option>
                <option>Phường 1</option>                                                </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Quận</label>
            <select class="form-control" >
                <option>Quận 1</option>
                <option>Quận 2</option>                                                </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Thành phố</label>
            <select class="form-control" >
                <option>Hồ Chí Minh</option>
                <option>Hà Nội</option>

            </select>
        </div>
    </div>

</div>
<div class="form-group">
    <label>Email</label>
    <input type="input" class="form-control" placeholder="" value="trannguy@gmail.com">
</div>
<div class="form-group">
    <label>Số điện thoại</label>
    <input type="input" class="form-control" placeholder="" value="0288895456">
</div>
<div class="form-group">
    <label>Số điện thoại di động</label>
    <input type="input" class="form-control" placeholder=""  value="090802568785">
</div>

<div class="form-group">
    <label >Nghề nghiệp</label>
    <select class="form-control" >
        <option>Nhân viên văn phòng</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
    </select>
</div>
<div class="form-group">
    <label><b>Người liên hệ trong trường hợp khẩn cấp</b></label>
</div>
<div class="form-group">
    <label >Mã bệnh nhân</label>
    <select class="form-control" >
        <option>BN002</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
    </select>
</div>
<div class="form-group">
    <label>Họ và tên</label>
    <input type="input" class="form-control" placeholder=""  value="Trần Tiến">
</div>
<div class="form-group">
    <label>Quan hệ với bệnh nhân</label>
    <select class="form-control" >
        <option>Vợ Chồng</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
    </select>
</div>
<div class="form-group">
    <label>Số điện thoại</label>
    <input type="input" class="form-control" placeholder=""  value="09085293741">
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label >Địa chỉ <sub style="color:#679fe5">Thêm nhanh</sub></label>
            <input type="input" class="form-control" placeholder="" value="1 Nguyễn Trãi">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Phường</label>
            <select class="form-control" >
                <option>Nguyễn Cư Trinh</option>
                <option>Phường 1</option>                                                </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Quận</label>
            <select class="form-control" >
                <option>Quận 1</option>
                <option>Quận 2</option>                                                </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Thành phố</label>
            <select class="form-control" >
                <option>Hồ Chí Minh</option>
                <option>Hà Nội</option>

            </select>
        </div>
    </div>

</div>
<div class="form-group">
    <label><b>Thông tin xuất hóa đơn công ty</b></label>
</div>
<div class="form-group">
    <label>Tên công ty</label>
    <input type="input" class="form-control" placeholder="" value="Dream company">
</div>
<div class="form-group">
    <label>MST công ty</label>
    <input type="input" class="form-control" placeholder="" value="789321741">
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label >Địa chỉ</label>
            <input type="input" class="form-control" placeholder="" value="2 Nguyễn Trãi">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Phường</label>
            <select class="form-control" >
                <option>Nguyễn Cư Trinh</option>
                <option>Phường 1</option>                                                </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Quận</label>
            <select class="form-control" >
                <option>Quận 1</option>
                <option>Quận 2</option>                                                </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label >Thành phố</label>
            <select class="form-control" >
                <option>Hồ Chí Minh</option>
                <option>Hà Nội</option>

            </select>
        </div>
    </div>

</div>