<?php

$lang_list = $list_lang;


$id_book_current = $id_book; 
$id_parent_current = $id_parent; 
//
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Ngoại Trú</a></li>
                <!-- <li class="active"><?= $title;?></li> -->
                <li class="active"><?= $lang_list->line('sokham_phukhoa1')?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title;?></h3> -->
                <h3 class="m-b-none"><?= $lang_list->line('sokham_phukhoa')?></h3>
            </div>


            

            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light" style="display: grid;">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default" attr-link-main="api/v1/patient/gynecolog_book" at-id="<?php echo (isset($id_book_current)?$id_book_current:'')?>" style="border: none;display: grid;" >
                        <div class="col-sm-12 text-center">
                            <!-- <h2>SỔ KHÁM PHỤ KHOA</h2> -->
                            <!--<h2><?= $lang_list->line('sokham_phukhoa')?></h2>-->
                        </div>
                        <div class="form-group"> 
                            <h4 class="col-sm-12"><?= $lang_list->line('skpk_thongtin_so')?> <a href="#thongtin_so" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>
                        <br>
                        
                            <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_so">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Mã sổ:</label> <span attr-show="code">000002</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Ngày lập sổ:</label> <span attr-show="created_at">20/10/2018</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <h4 class="col-sm-12"><?= $lang_list->line('skpk_tt_benhnhan')?> <a href="#thongtin_benhnhan" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                            </div>
                            <br>
                            <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_benhnhan">
                                <div class="col-sm-12">
                                    <?php echo $this->load->view('enclitic/template_thongtin_benhnhan_static',array('list_lang'=>$lang_list),true)?>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <h4 class="col-sm-12"><?= $lang_list->line('skpk_tt_phukhoa')?> <a href="#thongtin_phukhoa" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                            </div>
                            <br>
                            <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_phukhoa">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label >Tuổi thấy kinh</label>
                                        <input type="input" class="form-control" attr-name="age_have_menstrual" placeholder="" value="">
                                    </div>
                                    <div class="form-group">
                                        <label >Số ngày 1 chu kỳ kinh</label>
                                        <input type="input" class="form-control" attr-name="period" placeholder="" value="">
                                    </div>
                                    <div class="form-group">
                                        <label >Tình trạng kinh</label>
                                        <select class="form-control" attr-name="menstrual_status">
                                            <option value="0">Đều</option>
                                            <option value="1">Không đều</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label >Số ngày hành kinh</label>
                                        <input type="input" class="form-control" attr-name="day_of_menstrual" placeholder="" value="">
                                    </div>
                                    <div class="form-group">
                                        <label>Lượng kinh</label>
                                        <select class="form-control" attr-name="menstrual_amount">
                                            <option value="0">nhiều</option>
                                            <option value="1">vừa</option>
                                            <option value="2">ít</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Tuổi lập gia đình</label>
                                        <input type="input" class="form-control" attr-name="married_age" placeholder="" value="">
                                    </div>
                                    <div class="form-group">
                                        <label>Số lần có thai</label>
                                        <input type="input" class="form-control" attr-name="number_of_pregnancies" placeholder="" value="">
                                    </div>
                                    <div class="form-group">
                                        <label>Parity</label>
                                        <input type="input" class="form-control" attr-name="parity" placeholder="" value="">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="doc-buttons"> 
                                            <a href="#" class="btn btn-s-md btn-primary " id="save_book">Lưu</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
						<hr class="hr-show"/>
                        <div class="form-group"> 
                            <h4 class="col-sm-12">Bệnh sử gia đình <a href="#thongtin_giadinh" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>
                        <br>
                        <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_giadinh">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <div class="doc-buttons"> 
                                        <a href="#" class="btn btn-s-md btn-primary " data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Thêm</a>
                                        <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete">Xóa</a>
                                    </div>
                                    <div id="" class="dataTables_wrapper no-footer">                                        
                                        <div id="" class="dataTables_wrapper no-footer"><div class="row"><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="DataTables_Table_0"></label></div></div><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
                                        <table id="thongtin_giadinh" data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info"> 
                                            <thead>
                                                <tr show-position="2" role="row">
                                                    <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-sort="ascending"  style="width: 0px;"><input type="checkbox"></th>
                                                    <th att-name="" class="sorting" tabindex="0"  rowspan="1" colspan="1" style="width: 0px;">Tên bệnh</th>
                                                    <th att-name="" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Tình trạng</th>
                                                </tr> 
                                            </thead>
                                            <tbody>
                                                <tr role="row" class="odd">
                                                    <td class="sorting_1"><input type="checkbox"></td>
                                                    <td>Tim mạch</td>
                                                    <td>Hở van tim</td>
                                                </tr>
                                            </tbody> 
                                            <tfoot></tfoot>
                                        </table>
                                        <div class="row"><div class="col-sm-6"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div></div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-show"/>
                        <div class="form-group"> 
                            <h4 class="col-sm-12"><?= $lang_list->line('skpk_benhnoikhoa')?> <a href="#benh_noikhoa" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>
                        <br>
                        <div class="collapse in col-sm-12" aria-expanded="" id="benh_noikhoa">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <div class="doc-buttons"> 
                                        <a href="#" class="btn btn-s-md btn-primary " data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Thêm</a>
                                        <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete">Xóa</a>
                                    </div>
                                    <div id="" class="dataTables_wrapper no-footer">                                        
                                        <div id="" class="dataTables_wrapper no-footer"><div class="row"><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="DataTables_Table_0"></label></div></div><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
                                        <table id="skpk_benhnoikhoa" data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info"> 
                                            <thead>
                                                <tr show-position="2" role="row">
                                                    <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-sort="ascending"  style="width: 0px;"><input type="checkbox"></th>
                                                    <th att-name="" class="sorting" tabindex="0"  rowspan="1" colspan="1" style="width: 0px;">Tên bệnh</th>
                                                    <th att-name="" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Tình trạng</th>
                                                </tr> 
                                            </thead>
                                            <tbody>
                                                <tr role="row" class="odd">
                                                    <td class="sorting_1"><input type="checkbox"></td>
                                                    <td>Tim mạch</td>
                                                    <td>Hở van tim</td>
                                                </tr>
                                            </tbody> 
                                            <tfoot></tfoot>
                                        </table>
                                        <div class="row"><div class="col-sm-6"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div></div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<hr class="hr-show"/>
                        <div class="form-group"> 
                            <h4 class="col-sm-12"><?= $lang_list->line('skpk_lichsu_thamkham')?><a href="#history_examination" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>
                        <br>
                        <div class="collapse in col-sm-12" aria-expanded="" id="history_examination">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <div class="doc-buttons"> 
                                        <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>
                                        <a href="#" class="btn btn-s-md btn-primary" id="add_new_gynecological_examination" att-url-add="api/v1/patient/gynecolog_form">Thêm mới</a>
                                        <a href="#" class="btn btn-s-md btn-primary disabled edit_select" data-toggle="modal" data-target="#myAdd_menu1">Sửa</a>
                                        <a href="#" class="btn btn-s-md btn-danger disabled xoa_select" data-toggle="modal" data-target="#myDeleteEverything">Xóa</a>
                                    </div>
                                    <div id="" class="dataTables_wrapper no-footer">                                        
                                        <div id="" class="dataTables_wrapper no-footer"><div class="row"><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="DataTables_Table_0"></label></div></div><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
                                        <table id="medical_examination_form" data-link-api="api/v1/patient/medical_examination_form" data-para="type=1&medical_record_id=<?php echo (isset($id_parent_current)?$id_parent_current:'')?>" attr-media-record="<?=$id_parent_current?>" data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%" role="grid" aria-describedby="DataTables_Table_0_info"> 
                                            <thead>
                                                <tr show-position="2" role="row" attr-show-id="id"  for-sub-class="go-to-link" link-onclick="gynecological_examination">
                                                    <th att-name="id"><input type="checkbox"></th>
                                                    <th att-name="check_in_time" attr-fomart-show="HH:mm dd/mm/yyyy" data-sort="asc" class="sortat sorting_asc">Thời gian khám</th>
                                                    <th att-name="staff_id.user_id.fullname">Bác sĩ điều trị</th>
                                                    <th att-name="gynecolog_form_id.last_menstruation" attr-fomart-show="dd/mm/yyyy">Kỳ kinh cuối</th>
                                                    <th att-name="exam">Thăm khám</th>
                                                    <th att-name="diagnose">Chuẩn đoán</th>
                                                    <th att-name="tracking_treatment">Điều trị</th>
                                                    <th att-name="re_examination_date" attr-fomart-show="dd/mm/yyyy">Thời gian khám lại</th>
                                                </tr> 
                                            </thead>
                                            <tbody>
                                                <tr role="row" class="odd go-tol-link" onclick="location.href = './gynecological_examination';">
                                                    <td class="sorting_1"><input type="checkbox"></td>
                                                    <td>09:00 10/10/2018</td>
                                                    <td>Nguyễn Tuấn</td>
                                                    <td>07/08/2018</td>
                                                    <td>Thông tin bệnh sử</td>
                                                    <td>Thông tin chẩn đoán</td>
                                                    <td>Thông tin điều trị</td>
                                                    <td>20/11/2018</td>
                                                </tr>
                                            </tbody> 
                                            <tfoot></tfoot>
                                        </table>
                                        <div class="row"><div class="col-sm-6"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div></div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('edit_post')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('edit_post')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_departments')?></label>
                                            <div class="col-sm-9"> 
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                            </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('user_id_leader')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control datepicker-input parsley-validated" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save_change')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add"  style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('addnew')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <!-- <div class="line line-dashed line-lg pull-in"></div> -->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_departments')?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('user_id_leader')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                        <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save')?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                
                <?php echo $this->load->view('book/template_book_myDeleteEverything', array('list_lang' => $lang_list), true) ?>
                
                </section>
            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
