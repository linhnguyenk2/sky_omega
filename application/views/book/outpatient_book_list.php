<?php
$lang_list = $list_lang;
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Tiếp nhận</a></li>
                <!-- <li class="active"><?= $title; ?></li> -->
                <li class="active">Danh sách sổ khám phụ khoa</li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title; ?></h3> -->
                <h3 class="m-b-none">Danh sách sổ khám phụ khoa</h3>
            </div>




            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <div id="" class="dataTables_wrapper no-footer">

                                <div class="doc-buttons"> 

                                    <!--<a  href="<?= site_url('list_patient_add') ?>" class="btn btn-s-md btn-primary" ><i class="fa fa-plus"></i> Tạo Bệnh Nhân</a>-->
                                    <a  href="#" class="btn btn-s-md btn-primary"  data-toggle="modal" data-target="#myAdd" ><i class="fa fa-plus"></i> Thêm</a>

                                    <a style="" href="#" class="btn btn-s-md btn-info disabled" data-toggle="modal" data-target="#myEdit" id="btn_edit">Sửa</a> 

                                    <a style="display:none;" href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myPush" id="btn_publish"><i class="fa fa-check"></i> <?= $lang_list->line('publish') ?></a>

                                    <a style="display:none;" href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myUnpush" id="btn_unpublish"><?= $lang_list->line('unpublish') ?></a>

                                    <a href="#" class="btn btn-s-md btn-danger disabled xoa-element" data-toggle="modal" data-target="#myDelete" id="btn_delete">Xóa</a>

                                </div>

                                <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
                                <!-- <table id="outpatient_book_list" data-link-api="api/v1/patient/gynecolog_book" data-para="" data-example="example_more" class="table table-striped m-b-none" style="width:100%">  -->
                                <table id="outpatient_book_list" data-link-api="api/v1/patient/medical_record" data-para="type=1" data-example="example_more" class="table table-striped m-b-none" style="width:100%"> 
                                    <thead>
                                        <!--<tr show-position="2" for-sub-class="go-tol-link" for-onclick="patient_information">--> 
                                        <tr show-position="2" attr-show-id="gynecolog_book_id.id" attr-id-parent="id" for-sub-class="go-to-link"> 
                                        <!-- <tr show-position="2" attr-show-id="id">  -->

                                            <th att-name="id"><input type="checkbox"/></th> 
                                            <th att-name="code" data-sort="asc" class="sortat sorting_asc">Mã số</th> 
                                            <th att-name="patient_id.user_id.code">Mã bệnh nhân</th>
                                            <th att-name="patient_id.user_id.fullname">Tên bệnh nhân</th>
                                            <th att-name="patient_id.user_id.birthday">Ngày sinh</th>
                                            <th att-name="patient_id.user_id.address">Địa chỉ</th>
                                            <th att-name="patient_id.user_id.home_phone">Sdt</th>
                                            <th att-name="last_modified">Ngày khám cuối</th>
                                        </tr> 
                                    </thead>
                                    <tbody> 
                                        <tr>
                                            <td><input type="checkbox"></td>
                                            <td>001</td>
                                            <td>Trần Nguy</td>
                                            <td>022568789</td>
                                            <td>1/1/1995</td>
                                            <td>Nữ</td>
                                            <td>1 Nguyễn Trãi</td>
                                            <td>1 Nguyễn Trãi</td>
                                            <!-- <td>Hiển thị</td> -->
                                            <!-- <td>1</td> -->
                                        </tr>
                                    </tbody> 
                                    <tfoot><tr> 

                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <!-- <th data-type-select="select-status"></th>  -->
                                            <!-- <th></th>  -->
                                        </tr></tfoot>
                                </table>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="dataTables_info" role="status" aria-live="polite">Showing page 1 of 1</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_full_numbers"><a class="paginate_button first disabled" data-dt-idx="0" tabindex="0">First</a><a class="paginate_button previous disabled" data-dt-idx="1" tabindex="0">Previous</a><span><a class="paginate_button current" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next" data-dt-idx="7" tabindex="0">Next</a><a class="paginate_button last" data-dt-idx="8" tabindex="0">Last</a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- <?php //$this->load->view('book/template_popup_add_book',['lang_list'=>$lang_list]);   ?> -->
                            <!-- start add book -->
                            <div class="" id="add"  style="">
                                <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                                    <div class="modal-dialog" role="document" style="">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Thêm mới</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form class="col-sm-12" data-validate="parsley">
                                                    <div class="form-group">
                                                        <div class="row">

                                                            <div class="col-sm-12"><label>Mã bệnh nhân</label></div>
                                                            <div class="col-sm-8">
                                                                <input type="input" id="layma_bn_moi_text" class="form-control" placeholder="BN001" value="" attr-value="code">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <button type="button" class="btn btn-primary" id="layma_bn_moi">Tìm BN</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"> 
                                                        <h4 class="col-sm-12">Thông tin bệnh nhân<a href="#thongtin_benhnhan" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                                                    </div>
                                                    <br>
                                                    <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_benhnhan">
                                                        <div class="col-sm-12">
                                                            <?php echo $this->load->view('enclitic/template_thongtin_benhnhan_static', array('list_lang' => $lang_list), true) ?>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                                <button type="button" class="btn btn-primary"  id="them_so_kham" attr_link_save="api/v1/patient/gynecolog_book">Thêm Sổ Khám Khám Phụ Khoa</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end add book -->
                            <div class="" id="delete" style="">
                                <div class="modal fade" id="myDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Xóa</h4>
                                            </div>
                                            <div class="modal-body">

                                                <form class="form-horizontal" data-validate="parsley">
                                                    <div class="form-group"> 
                                                        <label class="col-sm-12 control-label more_show text-center">Xóa sổ khám</label>
                                                        <div class="col-sm-12 hide"> 
                                                            <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled>  
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                                <button type="button" class="btn btn-primary xoa-event">Xóa</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- end popup delete -->
                        </div>

                    </section>
                </div>



            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>

<style>

</style>