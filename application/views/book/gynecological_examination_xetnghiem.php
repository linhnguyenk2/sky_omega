<div class="table-responsive">
    <div class="doc-buttons"> 
        <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>-->
        <!-- <a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myXetnghiem" id="btn_add">Thêm</a> -->
        <!-- <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Sưa</a> -->
        <!-- <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete">Xóa</a> -->
    </div>
    <div id="" class="dataTables_wrapper no-footer">                                        
        <div id="" class="dataTables_wrapper no-footer">
            <div class="row" style="display:none;"><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="DataTables_Table_0"></label></div></div><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
            <table id="ds_xn" data-link-api="api/v1/patient/analysis_paper" data-para="" data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%" id="" role="grid" aria-describedby="DataTables_Table_0_info"> 
                <thead>
                    <tr show-position="2" role="row">
                        <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-sort="ascending" style="width: 0px;"><input type="checkbox"></th>
                        <th att-name="code" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Mã phiếu</th>
                        <th att-name="type" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Tên xét nghiệm</th>
                        <th att-name="staff_id.user_id.fullname" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Bác sĩ xét nghiệm</th>
                        <!-- <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Kết quả</th> -->
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Thời gian xét nghiệm</th>
                    </tr> 
                </thead>
                <tbody>
                    <tr role="row" class="odd">
                        <td class="sorting_1"><input type="checkbox"></td>
                        <td>XN001</td>
                        <td>Xét nghiệm máu</td>
                        <td>Nguyễn Tuấn</td>
                        <!-- <td>Kết quả</td> -->
                        <td>10/10/2018</td>
                    </tr>
                </tbody> 
                <tfoot></tfoot>
            </table>
            <div class="row"><div class="col-sm-6"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div></div></div></div>
    </div>
</div>


<div class="modal fade" id="myXetnghiem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Xét nghiệm</h4>
            </div>
            <div class="modal-body">

                <div class="col-sm-12">
                    
                </div>
                <!--</div>-->

                <!--</div>-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary save-pupop">Lưu</button>
            </div>
        </div>
    </div>
</div>