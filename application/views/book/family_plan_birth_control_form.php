<?php
$lang_list = $list_lang;

$id_in = $id_at;
    
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Ngoại Trú</a></li>
                <!-- <li class="active"><?= $title; ?></li> -->
                <li class="active">Phiếu hướng dẫn và theo dõi KHHGD</li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title; ?></h3> -->
                <h3 class="m-b-none">Phiếu hướng dẫn và theo dõi KHHGD</h3>
            </div>


            
            <div class="wrapper-lg bg-white b-b b-light" style="display: grid;">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default" style="border: none;" attr-link-main="api/v1/patient/medical_examination_form" at-id="<?= $id_at?>" attr-link-update="api/v1/patient/family_plan_birth_control_form" attr-link-update-id="" >
<!--                        <div class="col-sm-12 text-center">
                            <h2>THĂM KHÁM THAI</h2>
                        </div>-->
                        <div class="col-sm-12" id="info_thamkham_thai">
<!--                            <div class="form-group">
                                <label>Mã phiếu khám:</label> <label>PK001</label>
                            </div>-->
                            <div class="form-group">
                                <label >Ngày thực hiện KHHGD</label>
                                <input type="input" class="form-control datetimepicker-date" attr-fomart-show="dd/mm/yyyy" attr-fomart-save="dd/mm/yyyy-yyyy/mm/dd" attr-name="family_plan_birth_planing_form_id.date_perform" attr-save="date_perform" value="">
                                <input type="hidden" class="form-control" attr-name="type" attr-save="type" value="1">
                            </div>
                            <div class="form-group">
                                <label >Thời gian khám</label>
                                <input type="input" class="form-control datetimepicker" attr-fomart-show="HH:mm dd/mm/yyyy" attr-fomart-save="HH:mm dd/mm/yyyy-yyyy/mm/dd" attr-name="check_in_time" attr-save="check_in_time" value="">
                            </div>
                            <div class="form-group">
                                <label >Thăm khám</label>
                                <input type="input" class="form-control" attr-name="exam" attr-save="exam" placeholder="" value="">
                            </div>
                            <div class="form-group">
                                <label >Chuẩn đoán</label>
                                <input type="input" class="form-control" attr-name="diagnose" attr-save="diagnose" placeholder="" value="">
                            </div>
                            <div class="form-group">
                                <label >Thực hiện thủ thuật</label>
                                <input type="input" class="form-control" attr-name="family_plan_birth_planing_form_id.surgery" attr-save="surgery" placeholder="" value="">
                            </div>
                            <div class="form-group">
                                <label >Vô cảm</label>
                                <input type="input" class="form-control" attr-name="family_plan_birth_planing_form_id.anesthesia" attr-save="anesthesia" placeholder="" value="">
                            </div>
                            <div class="form-group">
                                <label >Lời dặn</label>
                                <input type="input" class="form-control" attr-name="" attr-save="" placeholder="" value="">
                            </div>
                            <div class="form-group">
                                <label >Ngày tái khám</label>
                                <input type="input" class="form-control datetimepicker-date" attr-fomart-show="dd/mm/yyyy" attr-fomart-save="dd/mm/yyyy-yyyy/mm/dd" attr-name="re_examination_date" attr-save="re_examination_date" value="">
                            </div>
                            <div class="form-group">
                                <label >Nội dung tái khám</label>
                                <input type="input" class="form-control" attr-name="family_plan_birth_planing_form_id.reexamination_reason" attr-save="reexamination_reason" placeholder="" value="">
                            </div>
                            
                            <div class="form-group">
                                <div class="doc-buttons"> 
                                    <a href="#" class="btn btn-s-md btn-primary " id="save_book">Lưu</a>
                                </div>
                            </div>

                        </div>

                        <div id="input_hidden">
                            <input type="hidden" id="id_in_patient_id">
                            <input type="hidden" id="id_in_staff_id">
                            <input type="hidden" id="id_in_medical_examination_form_id">
                        </div>  
						<hr class="hr-show"/>
		
                        <div class="col-sm-12 group-popup">
                            <h3>Danh sách chỉ định dịch vụ</h3>
                            <?php $this->load->view('book/gynecological_examination_chidinh_dichvu',array('lang_list'=>$lang_list,'id_in'=>$id_in)); ?>
                        </div>

                        
                        <hr class="hr-show"/>
                        <div class="col-sm-12 group-popup">
                            <h3>Danh sách xét nghiệm</h3>
                            <?php $this->load->view('book/gynecological_examination_xetnghiem',array('lang_list'=>$lang_list,'id_in'=>$id_in)); ?>
                        </div>
						<hr class="hr-show"/>
                        <div class="col-sm-12 group-popup">
                            <h3>Danh sách cận lâm sàn</h3>
                            <div class="table-responsive">
                                <div class="doc-buttons"> 
                                    <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>-->
                                    <!-- <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Thêm</a> -->
                                    <!-- <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Sưa</a> -->
                                    <!-- <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete">Xóa</a> -->
                                </div>
                                <div id="" class="dataTables_wrapper no-footer">                                        
                                    <div id="" class="dataTables_wrapper no-footer">
                                        <div class="row" style="display:none;"><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="DataTables_Table_0"></label></div></div><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
                                        <table id="ds_cls" data-link-api="api/v1/subclinical/subclinical_paper" data-para="" data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%" id="" role="grid" aria-describedby="DataTables_Table_0_info"> 
                                            <thead>
                                                <tr show-position="2" role="row">
                                                    <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-sort="ascending" style="width: 0px;"><input type="checkbox"></th>
                                                    <th att-name="code" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Mã phiếu</th>
                                                    <th att-name="" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Tên CLS</th>
                                                    <th att-name="patient_id.user_id.fullname" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Bác sĩ thực hiện</th>
                                                    <th att-name="" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Kết quả</th>
                                                    <!-- <th att-name="" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Thời gian thực hiện</th> -->
                                                </tr> 
                                            </thead>
                                            <tbody>
                                                <tr role="row" class="odd">
                                                    <td class="sorting_1"><input type="checkbox"></td>
                                                    <td>CLS001</td>
                                                    <td>Siêu âm</td>
                                                    <td>Nguyễn Tuấn</td>
                                                    <td>Kết quả</td>
                                                    <!-- <td>10/10/2018</td> -->
                                                </tr>
                                            </tbody> 
                                            <tfoot></tfoot>
                                        </table>
                                        <div class="row"><div class="col-sm-6"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div></div></div></div>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-show"/>
                        <div class="col-sm-12 group-popup">
                            <h3>Danh sách toa thuốc</h3>
                            <?php $this->load->view('book/gynecological_examination_danhsach_toathuoc',array('lang_list'=>$lang_list)); ?>
                        </div>
						<hr class="hr-show"/>
                        <div class="col-sm-12 group-popup">
                            <h3>Danh sách toa TPCN-MP-VTYT</h3>
                            <?php $this->load->view('book/gynecological_examination_tpcn_mp_vtyt',array('lang_list'=>$lang_list)); ?>
                        </div>

                    </section>
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('edit_post') ?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('edit_post') ?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_departments') ?></label>
                                            <div class="col-sm-9"> 
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                            </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('user_id_leader') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control datepicker-input parsley-validated" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status') ?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>

                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save_change') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add"  style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew') ?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('addnew') ?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <!-- <div class="line line-dashed line-lg pull-in"></div> -->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_departments') ?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>


                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('user_id_leader') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status') ?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>

                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php //echo $template_delete_push_unpush; ?>
                <?php $this->load->view('book/template_popup_delete_show_table',array('lang_list'=>$lang_list)); ?>


            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
