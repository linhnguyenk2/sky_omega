<?php
$lang_list = $list_lang;


$id_book_current = $id_book;
$id_parent_current = $id_parent;

if(!$id_book_current || !$id_parent_current){
	echo 'Not exit id get book_id or parent_id';return;
}
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Ngoại Trú</a></li>
                <!-- <li class="active"><?= $title; ?></li> -->
                <li class="active">Sổ Mãn Kinh</li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title; ?></h3> -->
                <h3 class="m-b-none">Sổ Mãn Kinh</h3>
            </div>



            <div class="col-sm-12" style="background: white;">
                <!--<div class="wrapper-lg bg-white b-b b-light" style="display: grid;">-->
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default" attr-link-main="api/v1/patient/menopause_book" at-id="<?php echo (isset($id_book_current) ? $id_book_current : '') ?>"  style="border: none;display: grid;">
                        <!--                        <div class="col-sm-12 text-center">
                                                    <h2><?= $lang_list->line('sokham_thai') ?></h2>
                                                </div>-->
                        <div class="form-group"> 
                            <h4 class="col-sm-12">Thông tin sổ <a href="#thongtin_so" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>
                        <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_so">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Mã sổ:</label><span attr-show="code">MANHKINH001</span>
                                </div>
                                <div class="form-group">
                                    <label>Ngày lập sổ:</label> <span attr-show="created_at">20/10/2018</span>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <h4 class="col-sm-12">Thông tin bệnh nhân <a href="#thongtin_benhnhan" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>
                        <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_benhnhan">
                            <div class="col-sm-12">
                            <?php echo $this->load->view('enclitic/template_thongtin_benhnhan_static', array('list_lang' => $lang_list), true) ?>
                            </div>
                        </div>

                        <div class="form-group"> 
                            <h4 class="col-sm-12">Tiền căn cá nhân <a href="#thongtin_tiencan_canhan" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>
                        <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_tiencan_canhan">
                            <div class="col-sm-12">

                                <div class="form-group">
                                    <label>Nội khoa</label>
                                    <input type="input" class="form-control" placeholder="" value="" attr-name="medical_history" attr-save="medical_history">
                                </div>
                                <div class="form-group">
                                    <label>Ngoại khoa</label>
                                    <input type="input" class="form-control" placeholder="" value="" attr-name="surgical_history" attr-save="surgical_history">
                                </div>
                                <div class="form-group">
                                    <label>Sản khoa</label>
                                    <input type="input" class="form-control" placeholder="" value="" attr-name="obstetrical_history" attr-save="obstetrical_history">
                                </div>

                            </div>
                        </div>

                        <div class="form-group"> 
                            <h4 class="col-sm-12">Tiền căn gia đình <a href="#thongtin_tiencan_giadinh" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>

                        <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_tiencan_giadinh">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Tiền căn gia đình</label>
                                    <input type="input" class="form-control" placeholder="" value="" attr-name="family_history" attr-save="family_history">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="doc-buttons"> 
                                        <a href="#" class="btn btn-s-md btn-primary " id="save_book">Lưu</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--</div>-->


                        <div class="form-group"> 
                            <h4 class="col-sm-12">Lịch sử tự theo dõi<a href="#history_examination" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>
                        <br>
                        <div class="collapse in row" aria-expanded="" id="history_examination">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <div class="doc-buttons"> 
                                        <!--a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a-->
                                        <!--<a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Thêm</a>-->
                                        <a href="#" class="btn btn-s-md btn-primary" id="add_new_gynecological_examination" att-url-add="api/v1/patient/menopause_form">Thêm mới</a>
                                        <a href="#" class="btn btn-s-md btn-primary disabled edit_select" data-toggle="modal" data-target="#myAdd_menu1">Sửa</a>
                                        <a href="#" class="btn btn-s-md btn-danger disabled xoa_select" data-toggle="modal" data-target="#myDeleteEverything">Xóa</a>
                                    </div>
                                    <div id="" class=" ">                                        
                                        <div id="" class="dataTables_wrapper no-footer"><div class="row"><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="DataTables_Table_0"></label></div></div><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
                                            <table id="medical_examination_form" data-link-api="api/v1/patient/medical_examination_form" data-para="type=4&medical_record_id=<?php echo (isset($id_parent_current) ? $id_parent_current : '') ?>" attr-media-record="<?= $id_parent_current ?>" data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%" role="grid" aria-describedby="DataTables_Table_0_info"> 
                                                <thead>
                                                    <tr show-position="2" role="row" attr-show-id="id" link-onclick="self_tracking_sheet"  for-sub-class="go-to-link">
                                                        <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-sort="ascending"  style="width: 0px;"><input type="checkbox"></th>
                                                        <th att-name="menopause_form_id.month_watch" class="sorting" tabindex="0"  rowspan="1" colspan="1" style="width: 0px;">Tháng theo dõi</th>
                                                        <th att-name="menopause_form_id.last_menstruation" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Ngày ra kinh</th>
                                                        <th att-name="menopause_form_id.lastday_of_menses" class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Ngày sạch kinh</th>

                                                    </tr> 
                                                </thead>
                                                <tbody>
                                                    <tr role="row" class="odd go-tol-link" onclick="location.href = './self_tracking_sheet';">
                                                        <td class="sorting_1"><input type="checkbox"></td>
                                                        <td>10/10/2018</td>
                                                        <td>Nguyễn Tuấn</td>
                                                        <td>07/08/2018</td>
                                                    </tr>
                                                </tbody> 
                                                <tfoot></tfoot>
                                            </table>
                                            <div class="row"><div class="col-sm-6"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div></div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
               
                        <?php echo $this->load->view('book/template_book_myDeleteEverything', array('list_lang' => $lang_list), true) ?>
                </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
