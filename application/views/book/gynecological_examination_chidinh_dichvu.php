
<div id="id_cddv_group" class="table-responsive">
    <div class="doc-buttons"> 
        <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>-->
        <a href="#" class="btn btn-s-md btn-primary add_show" data-toggle="modal" data-target="#myChidinhdichvu" >Thêm</a>
        <a href="#" class="btn btn-s-md btn-primary disabled edit_select" data-toggle="modal" data-target="#myChidinhdichvu">Sửa</a>
        <a href="#" class="btn btn-s-md btn-danger disabled xoa_select" data-toggle="modal" data-target="#myDeleteEverything">Xóa</a>
    </div>
    <div id="" class="dataTables_wrapper no-footer">                                        
        <div class="row" style="display:none;"><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="DataTables_Table_0"></label></div></div><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
        <table id="id_tb_cddv" data-link-api="api/v1/patient/specify_service_paper" data-para="medical_examination_form_id=<?php echo ($id_in) ? $id_in : '' ?>" data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%" id="" role="grid" aria-describedby="DataTables_Table_0_info"> 
            <thead>
                <tr show-position="2" role="row" for-sub-class="go-to-link">
                    <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-sort="ascending" style="width: 0px;"><input type="checkbox"></th>
                    <th att-name="code" >Mã phiếu</th>
                    <th att-name="service_id.name" >Dịch vụ chỉ định</th>
                    <th att-name="staff_id.user_id.fullname" >Bác sĩ chỉ định</th>
                    <th att-name="reason">Dùng cho chuẩn đoán</th>
                    <th att-name="quantity">Số lượng</th>
                    <th att-name="service_location_id.name">Nơi thực hiện</th>
                    <th att-name="stat" select-static="specify_service">Trạng thái xử lý dịch vụ</th>
                </tr>
            </thead>
            <tbody>
                <tr role="row" class="odd">
                    <td class="sorting_1"><input type="checkbox"></td>
                    <td>PN001</td>
                    <td>Xét nghiệm máu</td>
                    <td>Nguyễn Tuấn</td>
                    <td>Thai Kỳ</td>
                    <td></td>
                    <td></td>
                    <td>Chưa xử lý</td>
                </tr>
            </tbody> 
            <tfoot>
            </tfoot>
        </table>
        <div class="row">
            <div class="col-sm-6"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="myChidinhdichvu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Chỉ định dịch vụ</h4>
            </div>
            <div class="modal-body">

                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Dịch vụ</label>
                        <!-- <select class="form-control" api-link="api/v1/list/service_type" attr-save="type"> -->
                        <select class="form-control" api-link="api/v1/service/service" id="select-service-service" attr-save="service_id">
                            <option value="1">Xét nghiệm máu</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Số lượng</label>
                        <input type="hidden" id="hidden_type_is_edit_service">
                        <input type="hidden" id="hidden_type_edit_service_id">
                        <input type="hidden" id="hidden_id_of_service">
                        <input type="input" class="form-control" placeholder=""  attr-save="quantity">
                        
                        <input type="hidden" class="form-control" placeholder="" value=""  attr-save="patient_id">
                        <input type="hidden" class="form-control" placeholder="" value=""  attr-save="staff_id">
                        <input type="hidden" class="form-control" placeholder="" value=""  attr-save="medical_examination_form_id">
                    </div>
                    <div class="form-group">
                        <label>Dùng cho chuẩn đoán</label>
                        <input type="input" class="form-control" placeholder=""  attr-save="reason">
                    </div>
                    <div class="form-group">
                        <label>Nơi thực hiện</label>
                        <!-- <select class="form-control" api-link="api/v1/list/clinic" attr-save="clinic_id"> -->
                        <select class="form-control" id="select-service-location" api-link="api/v1/service/service_location" attr-save="service_location_id">
                            <option value="1">Phòng 6A</option>
                        </select>
                    </div>
                </div>
                <!--</div>-->

                <!--</div>-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary save-pupop-service">Lưu</button>
            </div>
        </div>
    </div>
</div>