<?php
$lang_list = $list_lang;

$id_in = $id_at;
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Ngoại Trú</a></li>
                <!-- <li class="active"><?= $title; ?></li> -->
                <li class="active">Phiếu tự theo dõi</li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title; ?></h3> -->
                <h3 class="m-b-none">Phiếu tự theo dõi</h3>
            </div>


            
            <div class="wrapper-lg bg-white b-b b-light" style="display: grid;">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default" attr-link-main="api/v1/patient/medical_examination_form" at-id="<?= $id_at?>" attr-link-update="api/v1/patient/menopause_form" attr-link-update-id=""  style="border: none;">
                        <div class="col-sm-12" id="info-phieu-tu-theodoi">
                            
                            <div class="form-group">
                                <label >Tháng theo dõi</label>
                                <input type="input" class="form-control datetimepicker-date" attr-name="menopause_form_id.month_watch" attr-save="month_watch"/>
                                
                            </div>
                            <div class="form-group">
                                <label >Ngày ra kinh</label>
                                <input type="input" class="form-control datetimepicker-date" attr-fomart-show="dd/mm/yyyy" attr-fomart-save="dd/mm/yyyy-yyyy/mm/dd" placeholder="" value="" attr-name="menopause_form_id.firstday_of_menses" attr-save="firstday_of_menses">
                            </div>
                            <div class="form-group">
                                <label >Ngày sạch kinh</label>
                                <input type="input" class="form-control datetimepicker-date" attr-fomart-show="dd/mm/yyyy" attr-fomart-save="dd/mm/yyyy-yyyy/mm/dd" placeholder="" value="" attr-name="menopause_form_id.lastday_of_menses" attr-save="lastday_of_menses">
                            </div>
                            <div class="form-group">
                                <label >Cáu gắt</label>
                                <select class="form-control" attr-name="menopause_form_id.irritability" attr-save="irritability">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label >Buồn vô cớ</label>
                                <select class="form-control" attr-name="menopause_form_id.depression" attr-save="depression">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label >Hồi hộp</label>
                                <select class="form-control" attr-name="menopause_form_id.palpitation" attr-save="palpitation">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label >Quên lẫn</label>
                                <select class="form-control" attr-name="menopause_form_id.poor_concerntration" attr-save="poor_concerntration">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label >Bốc hỏa</label>
                                <select class="form-control" attr-name="menopause_form_id.hot_flushes" attr-save="hot_flushes">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label >Nhức đầu</label>
                                <select class="form-control" attr-name="menopause_form_id.headache" attr-save="headache">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label >Toát mồ hôi đêm</label>
                                <select class="form-control" attr-name="menopause_form_id.night_sweat" attr-save="night_sweat">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label >Mất ngủ</label>
                                <select class="form-control" attr-name="menopause_form_id.insomnia" attr-save="insomnia">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label >Khô da tóc</label>
                                <select class="form-control" attr-name="menopause_form_id.dry_skin" attr-save="dry_skin">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label >Đau lưng</label>
                                <select class="form-control" attr-name="menopause_form_id.backache" attr-save="backache">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label >Đau xương khớp</label>
                                <select class="form-control" attr-name="menopause_form_id.joint_pains" attr-save="joint_pains">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label >Khô âm đạo</label>
                                <select class="form-control" attr-name="menopause_form_id.vaginal_dryness" attr-save="vaginal_dryness">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label >Hút thuốc</label>
                                <select class="form-control" attr-name="menopause_form_id.smoking" attr-save="smoking">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label >Uống rượu</label>
                                <select class="form-control" attr-name="menopause_form_id.drinking" attr-save="drinking">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label >Thể dục</label>
                                <select class="form-control" attr-name="menopause_form_id.gymmastics" attr-save="gymmastics">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label >Uống sữa</label>
                                <select class="form-control" attr-name="menopause_form_id.milk_drinking" attr-save="milk_drinking">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label >Ăn đồ biển</label>
                                <select class="form-control" attr-name="menopause_form_id.seafood_eating" attr-save="seafood_eating">
                                    <option value="0">Không</option>
                                    <option value="1">Đôi khi</option>
                                    <option value="2">Nhiều</option>
                                </select>
                            </div>
							<div class="form-group">
                                <label >Số lần tiểu/ngày</label>
                                <input type="input" class="form-control" placeholder="" value="" attr-name="menopause_form_id.urinations_day" attr-save="urinations_day">
                            </div>
							<div class="form-group">
                                <label >Số lần giao hợp/tháng</label>
                                <input type="input" class="form-control" placeholder="" value="" attr-name="menopause_form_id.sex_month" attr-save="sex_month">
                            </div>
							
							
                            <div class="form-group">
                                <div class="doc-buttons"> 
                                    <a href="#" class="btn btn-s-md btn-primary " id="save_book">Lưu</a>
                                </div>
                            </div>

                        </div>

                    </section>
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('edit_post') ?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('edit_post') ?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_departments') ?></label>
                                            <div class="col-sm-9"> 
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                            </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('user_id_leader') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control datepicker-input parsley-validated" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status') ?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>

                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save_change') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add"  style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew') ?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('addnew') ?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <!-- <div class="line line-dashed line-lg pull-in"></div> -->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_departments') ?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>


                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('user_id_leader') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status') ?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>

                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php echo $template_delete_push_unpush; ?>


            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
