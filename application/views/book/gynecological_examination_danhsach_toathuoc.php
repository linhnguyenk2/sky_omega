<div class="">
    <div id="group_ListDonThuoc" class="table-responsive">
        <div class="doc-buttons">
            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>-->
            <a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myDanhsachthuoc" id="add_myDanhsachthuoc">Thêm</a>
            <a href="#" class="btn btn-s-md btn-primary disabled edit_select" data-toggle="modal" data-target="#myDanhsachthuoc">Sửa</a>
            <a href="#" class="btn btn-s-md btn-danger disabled xoa_select" data-toggle="modal" data-target="#myDeleteEverything">Xóa</a>
        </div>
        <div class="dataTables_wrapper no-footer">
            <div class="row" style="display:none;">
                <div class="col-sm-6">
                    <div id="" class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="DataTables_Table_0"></label></div>
                </div>
                <div class="col-sm-6">
                    <div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length"
                                aria-controls="DataTables_Table_0" class="">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> entries</label>
                        </div>
                </div>
            </div>
            <table id="medicine" data-link-api="api/v1/patient/prescription_paper" data-para=""
                data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%"
                id="" role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr show-position="2" role="row" for-sub-class="go-to-link">
                        <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-sort="ascending"
                            style="width: 0px;"><input type="checkbox"></th>
                        <th att-name="code" class="sorting" tabindex="0" rowspan="1"
                            colspan="1" style="width: 0px;">Mã toa thuốc</th>
                        <!-- <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 0px;">Danh sách thuốc</th> -->
                        <th att-name="medical_examination_form_id.re_examination_date" attr-fomart-show="HH:mm dd/mm/yyyy">Thời gian kê toa</th>
                        <th att-name="staff_id.user_id.fullname">Bác sĩ kê toa</th>
                    </tr>
                </thead>
                <tbody>
                    <tr role="row" class="odd">
                        <td class="sorting_1"><input type="checkbox"></td>
                        <!-- <td>Thuốc 001</td> -->
                        <td>Panadon 10 viên</td>
                        <td>10:10 1010/2018</td>
                        <td>Nguyễn Tuấn</td>
                    </tr>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
            <div class="row">
                <div class="col-sm-6">
                    <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing
                        1 to 1 of 1 entries</div>
                </div>
                <div class="col-sm-6">
                    <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled"
                            aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a
                                class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1"
                                tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0"
                            data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myDanhsachthuoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Mã toa thuốc: <span id="text-ma-toathuoc">TOATHUOC001</span></h4>
                </div>
                <div class="modal-body">
                    
                    <div class="table-responsive">
                        <h4>Danh sách thuốc</h4>
                        <div class="doc-buttons">
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>-->
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Thêm</a>-->
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Sưa</a>-->
                            <a href="#" class="btn btn-s-md btn-danger disabled xoa_select" data-toggle="modal"
                                data-target="#myDeleteEverything">Xóa</a>
                        </div>
                        <div id="" class="dataTables_wrapper no-footer">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input
                                                type="search" class="" aria-controls="DataTables_Table_0"></label></div>
                                </div>
                                <div class="col-sm-6" style="display:none">
                                    <div class="dataTables_length" id="DataTables_Table_0_length"><label>Show
                                            <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0"
                                                class="">
                                                <option value="0">10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select> entries</label></div>
                                </div>
                            </div>
                            <div class="list_input_hidden">
                                <input type="hidden" id="in_tb_prescription_paper_id" value="">
                            </div>
                            <table id="id_tb_medicine_in_prescription_paper_1" autoload="false" data-link-api="api/v1/product/medicine_in_prescription_paper"
                                data-para="" data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer"
                                style="width:100%" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                <thead>
                                    <tr show-position="2" role="row">
                                        <th att-name="id" class="sorting_asc"><input type="checkbox"></th>
                                        <th att-name="product_id.code">Mã thuốc</th>
                                        <th att-name="product_id.name" att-save="product_id">Tên thuốc</th>
                                        <th att-name="quantity" att-save="quantity">Số lượng </th>
                                        <th att-name="product_id.unit_id.name" att-save="">Đơn vị</th>
                                        <th att-name="usages" att-save="usages">Liều dùng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1"><input type="checkbox"></td>
                                        <td>Thuốc 001</td>
                                        <td>Thuốc panadon</td>
                                        <td>10</td>
                                        <td>Viên</td>
                                        <td>Ngày 3 lần, trước khi ăn</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr role="row" class="odd">
                                        <th att-edit="false"></th>
                                        <th att-edit="false"></th>
                                        <th att-edit="false" data-type-select="api/v1/product/medicine_product" data-show="name"></th>
                                        <th><input type="text"/></th>
                                        <!-- <th att-edit="false" data-type-select="api/v1/list/units" data-show="name"></th> -->
                                        <th att-edit="false"></th>
                                        <th><input type="text" /></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="row" style="display:none;">
                                <div class="col-sm-6">
                                    <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing
                                        1 to 1 of 1 entries</div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a
                                            class="paginate_button previous disabled" aria-controls="DataTables_Table_0"
                                            data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a
                                                class="paginate_button current" aria-controls="DataTables_Table_0"
                                                data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled"
                                            aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!--</div>-->

                    <!--</div>-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <!--<button type="button" class="btn btn-primary"><?= $lang_list->line('save') ?></button>-->
                </div>
            </div>
        </div>
    </div>


</div>

