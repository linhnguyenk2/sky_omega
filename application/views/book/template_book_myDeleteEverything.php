<!-- show model delete everything table -->
<div class="" id="" style="">
    <div class="modal fade" id="myDeleteEverything" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         data-backdrop="static" data-keyboard="false" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Xóa</h4>
                </div>
                <div class="modal-body">

                    <!-- <form class="form-horizontal" data-validate="parsley"> -->
                    <div class="form-group">
                        <input type="text" class="show-mess-delete" id="txt-delete-text-show" disabled value="Xóa">
                        <input type="hidden" id="txt-delete-url">
                        <input type="hidden" id="txt-delete-at-table">
                        <input type="hidden" id="txt-delete-value">
                    </div>
                    <!-- </form> -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary xoa-event">Xóa</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end show model delete table -->