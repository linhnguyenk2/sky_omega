<div class="">
    <div id="group_tb_tpcn_mp_vtyt" class="table-responsive">
        <div class="doc-buttons">
            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>-->
            <a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myTpcn_mp_vtyt" id="add_myTpcn_mp_vtyt">Thêm</a>
            <a href="#" class="btn btn-s-md btn-primary disabled edit_select" data-toggle="modal" data-target="#myTpcn_mp_vtyt">Sửa</a>
            <a href="#" class="btn btn-s-md btn-danger disabled xoa_select" data-toggle="modal" data-target="#myDeleteEverything">Xóa</a>
        </div>
        <div id="" class="dataTables_wrapper no-footer">
            <div class="row" style="display:none;">
                <div class="col-sm-6">
                    <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search"
                                class="" aria-controls="DataTables_Table_0"></label></div>
                </div>
                <div class="col-sm-6">
                    <div class="dataTables_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0"
                                class="">
                                <option value="0">0</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                        </select> entries</label></div>
                </div>
            </div>
            <table id="id_tb_tpcn" data-link-api="api/v1/patient/tpcn_mp_vtyt_paper" data-para=""
                data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%"
                role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr show-position="2" role="row" for-sub-class="go-to-link">
                        <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-sort="ascending"
                            style="width: 0px;"><input type="checkbox"></th>
                        <th att-name="code">Mã
                            toa</th>
                        <!-- <th>Danh sách TPCN-MP-VTYT</th> -->
                        <th att-name="medical_examination_form_id.re_examination_date" attr-fomart-show="HH:mm dd/mm/yyyy">Thời gian kê toa</th>
                        <th att-name="staff_id.user_id.fullname">Bác sĩ kê toa</th>
                    </tr>
                </thead>
                <tbody>
                    <tr role="row" class="odd">
                        <td class="sorting_1"><input type="checkbox"></td>
                        <!-- <td>Thuốc 001</td> -->
                        <td>Vitamin E 10 viên Thorakao 1 tuýt Bông gạc: 10 miếng</td>
                        <td>10:10 10/10/2018</td>
                        <td>Nguyễn Tuấn</td>
                    </tr>
                </tbody>
                <tfoot></tfoot>
            </table>
            <div class="row" style="">
                <div class="col-sm-6">
                    <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing
                        1 to 1 of 1 entries</div>
                </div>
                <div class="col-sm-6">
                    <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled"
                            aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a
                                class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1"
                                tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0"
                            data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="hidden">
        <input type="hidden" id="in_tb_tpcn_mp_vtyt_paper_id" value=""/>
    </div>

    <div class="modal fade" id="myTpcn_mp_vtyt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Mã toa TPCN - VTYT - MP: <span id="text-ma-tpcn-mp-vtyt"></span></h4>
                </div>
                <div class="modal-body">
                    <h4>Danh sách TPCN</h4>
                    <div class="table-responsive">
                        
                        <div class="doc-buttons">
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>-->
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Thêm</a>-->
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Sưa</a>-->
                            <a href="#" class="btn btn-s-md btn-danger disabled xoa_select" data-toggle="modal" data-target="#myDeleteEverything">Xóa</a>
                        </div>
                        <div id="" class="dataTables_wrapper no-footer">
                            
                            <div class="row" style="display:none;">
                                <div class="col-sm-6">
                                    <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input
                                                type="search" class="" aria-controls="DataTables_Table_0"></label></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="dataTables_length" id="DataTables_Table_0_length"><label>Show
                                            <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0"
                                                class="">
                                                <option value="0">0</option>
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select> entries</label></div>
                                </div>
                            </div>
                            <div class="list_input_hidden">
                                <input type="hidden" id="in_tb_tpcn_prescription_paper_id" value="">
                            </div>
                            <table id="id_tb_tpcn_in_tpcn_mp_vtyt_paper" autoload="false" data-link-api="api/v1/product/product_in_tpcn_mp_vtyt_paper"
                            data-para="product_group_id_type=2" 
                            data-link-api-save="api/v1/product/product_in_tpcn_mp_vtyt_paper" 
                            data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer"
                                style="width:100%" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                <thead>
                                    <tr show-position="2" role="row">
                                        <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1"
                                            aria-sort="ascending" style="width: 0px;"><input type="checkbox"></th>
                                        <th att-name="product_id.code">Mã TPCN</th>
                                        <th att-name="product_id.name" att-save="product_id">Tên
                                            TPCN</th>
                                        <th att-name="quantity" att-save="quantity">Số
                                            lượng </th>
                                        <th att-name="product_id.unit_id.name">Đơn vị</th>
                                        <th att-name="usages" att-save="usages">Liều
                                            dùng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1"><input type="checkbox"></td>
                                        <td>TPCN001</td>
                                        <td>Vitamin E</td>
                                        <td>10</td>
                                        <td>Viên</td>
                                        <td>Ngày 3 lần, trước khi ăn</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th att-edit="false"></th>
                                        <th att-edit="false"></th>
                                        <!-- <th att-edit="false" data-type-select="api/v1/product/functional_food_product" data-show="code"></th> -->
                                        <th att-edit="false" data-type-select="api/v1/product/product?product_group_id=2" data-show="code"></th>
                                        <th><input type="text" /></th>
                                        <th att-edit="false"></th>
                                        <th><input type="text" /></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="row" style="display:none;">
                                <div class="col-sm-6">
                                    <div class="dataTables_info" id="DataTables_Table_0_info" role="status"
                                        aria-live="polite">Showing
                                        1 to 1 of 1 entries</div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a
                                            class="paginate_button previous disabled" aria-controls="DataTables_Table_0"
                                            data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a
                                                class="paginate_button current" aria-controls="DataTables_Table_0"
                                                data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled"
                                            aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <h4>Danh sách MP</h4>
                        <div class="doc-buttons">
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>-->
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Thêm</a>-->
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Sưa</a>-->
                            <a href="#" class="btn btn-s-md btn-danger disabled xoa_select" data-toggle="modal" data-target="#myDeleteEverything"
                                id="btn_delete">Xóa</a>
                        </div>
                        <div id="" class="dataTables_wrapper no-footer">
                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
                                <div class="row" style="display:none;">
                                    <div class="col-sm-6">
                                        <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input
                                                    type="search" class="" aria-controls="DataTables_Table_0"></label></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_length" id="DataTables_Table_0_length"><label>Show
                                                <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0"
                                                    class="">
                                                    <option value="0">0</option>
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select> entries</label></div>
                                    </div>
                                </div>
                                <table id="id_tb_mp_in_tpcn_mp_vtyt_paper" autoload="false" data-link-api="api/v1/product/product_in_tpcn_mp_vtyt_paper" 
                                data-para="product_group_id_type=5"
                                data-link-api-save="api/v1/product/product_in_tpcn_mp_vtyt_paper"  
                                class="table table-striped m-b-none dataTable no-footer"
                                    style="width:100%" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                    <!-- api/v1/product/cosmetic_product -->
                                    <thead>
                                        <tr show-position="2" role="row">
                                            <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1"
                                                aria-sort="ascending" style="width: 0px;"><input type="checkbox"></th>
                                            <th att-name="product_id.code">Mã MP</th>
                                            <!-- <th att-name="cosmetic_id.name" att-save="cosmetic_id">Tên MP</th> -->
                                            <th att-name="product_id.name" att-save="product_id">Tên MP</th>
                                            <th att-name="quantity" att-save="quantity">Số
                                                lượng </th>
                                            <th att-name="product_id.unit_id.name">Đơn vị</th>
                                            <th att-name="usages" att-save="usages">Liều
                                                dùng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1"><input type="checkbox"></td>
                                            <td>MP001</td>
                                            <td>Thorakao</td>
                                            <td>1</td>
                                            <td>Tuýp</td>
                                            <td>Ngày thoa 3 lần</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr role="row" class="odd">
                                            <th att-edit="false"></th>
                                            <th att-edit="false"></th>
                                            <!-- <th att-edit="false" data-type-select="api/v1/product/cosmetic_product" data-show="code"></th> -->
                                            <!-- <th att-edit="false" data-type-select="api/v1/product/product?product_group_id=5" data-show="code"></th> -->
                                            <th att-edit="false" data-type-select="api/v1/product/product?product_group_id=5" data-show="name"></th>
                                            <th><input type="text" /></th>
                                            <th att-edit="false"></th>
                                            <th><input type="text" /></th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="row" style="display:none;">
                                    <div class="col-sm-6">
                                        <div class="dataTables_info" id="DataTables_Table_0_info" role="status"
                                            aria-live="polite">Showing
                                            1 to 1 of 1 entries</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a
                                                class="paginate_button previous disabled" aria-controls="DataTables_Table_0"
                                                data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a
                                                    class="paginate_button current" aria-controls="DataTables_Table_0"
                                                    data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled"
                                                aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <h4>Danh sách VTYT</h4>
                        <div class="doc-buttons">
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>-->
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Thêm</a>-->
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Sưa</a>-->
                            <a href="#" class="btn btn-s-md btn-danger disabled xoa_select" data-toggle="modal" data-target="#myDeleteEverything"
                                >Xóa</a>
                        </div>
                        <div id="DataTables_Table_1" class="dataTables_wrapper no-footer">
                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
                                <div class="row" style="display:none;">
                                    <div class="col-sm-6">
                                        <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input
                                                    type="search" class="" aria-controls="DataTables_Table_0"></label></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_length" id="DataTables_Table_0_length"><label>Show
                                                <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0"
                                                    class="">
                                                    <option value="0">0</option>
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select> entries</label></div>
                                    </div>
                                </div>
                                <table id="id_tb_vtyt_in_tpcn_mp_vtyt_paper" autoload="false" data-link-api="api/v1/product/product_in_tpcn_mp_vtyt_paper"
                                data-para="product_group_id_type=3" 
                                data-link-api-save="api/v1/product/product_in_tpcn_mp_vtyt_paper" 
                                class="table table-striped m-b-none dataTable no-footer"
                                    style="width:100%" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                    <!-- api/v1/product/medical_supply -->
                                    <thead>
                                        <tr show-position="2" role="row">
                                            <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1"
                                                aria-sort="ascending" style="width: 0px;"><input type="checkbox"></th>
                                            <th att-name="product_id.code">Mã VTYT</th>
                                            <!-- <th att-name="medical_supplies_id.code" att-save="medical_supplies_id">Tên VTYT</th> -->
                                            <th att-name="product_id.name" att-save="product_id">Tên VTYT</th>
                                            <th att-name="quantity" att-save="quantity">Số
                                                lượng </th>
                                            <th att-name="product_id.unit_id.name">Đơn vị</th>
                                            <th att-name="usages" att-save="usages">Cách
                                                dùng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1"><input type="checkbox"></td>
                                            <td>VTYT001</td>
                                            <td>Bông gạc</td>
                                            <td>10</td>
                                            <td>Miếng</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th att-edit="false"></th>
                                            <th att-edit="false"></th>
                                            <!-- <th att-edit="false"  data-type-select="api/v1/product/medical_supply" data-show="code"></th> -->
                                            <th att-edit="false" data-type-select="api/v1/product/product?product_group_id=3" data-show="name"></th>
                                            <th><input type="text" /></th>
                                            <th att-edit="false"></th>
                                            <th><input type="text" /></th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="row" style="display:none;">
                                    <div class="col-sm-6">
                                        <div class="dataTables_info" id="DataTables_Table_0_info" role="status"
                                            aria-live="polite">Showing
                                            1 to 1 of 1 entries</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a
                                                class="paginate_button previous disabled" aria-controls="DataTables_Table_0"
                                                data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a
                                                    class="paginate_button current" aria-controls="DataTables_Table_0"
                                                    data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled"
                                                aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--</div>-->

                    <!--</div>-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <!--<button type="button" class="btn btn-primary"><?= $lang_list->line('save') ?></button>-->
                </div>
            </div>
        </div>
    </div>
</div>

<style>
body .modal-backdrop.fade.in {
    z-index: 0 !important;
}
</style>