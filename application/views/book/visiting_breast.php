<?php
$lang_list = $list_lang;

$id_at = $id_at;
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Ngoại Trú</a></li>
                <!-- <li class="active"><?= $title; ?></li> -->
                <li class="active">Thăm Khám Nhủ - Chi Tiết</li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title; ?></h3> -->
                <h3 class="m-b-none">Thăm Khám Nhủ - Chi Tiết</h3>
            </div>




            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="col-sm-12" style="background: white;">
                <!--<div class="wrapper-lg bg-white b-b b-light" style="display: grid;">-->
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default" attr-link-main="api/v1/patient/medical_examination_form" at-id="<?= $id_at ?>" attr-link-update="api/v1/patient/breast_examination_form" style="border: none;display: grid;">
                        <!--                        <div class="col-sm-12 text-center">
                                                     <h2>SỔ KHÁM PHỤ KHOA</h2> 
                                                    <h2></h2>
                                                </div>-->


                        <div id="info_visiting_breast" class="col-sm-12">

                            <!-- <div class="form-group">
                                <label >Bệnh sử</label>
                                <input type="input" class="form-control" attr-name="exam" attr-save="exam">
                            </div> -->
                            <div class="form-group">
                                <label >Chuẩn đoán</label>
                                <input type="input" class="form-control" attr-name="diagnose" attr-save="diagnose">
                            </div>
                            <div class="form-group">
                                <label >Thăm khám</label>
                                <input type="input" class="form-control" attr-name="exam" attr-save="exam">
                            </div>
                            <div class="form-group">
                                <label >Điều trị</label>
                                <input type="input" class="form-control" attr-name="tracking_treatment" attr-save="tracking_treatment">
                            </div>
                            <div class="form-group">
                                <label >Ngày tái khám</label>
                                <input type="input" class="form-control datetimepicker-date" attr-name="re_examination_date" attr-save="re_examination_date">
                            </div>

                        </div>


                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="doc-buttons"> 
                                    <a href="#" class="btn btn-s-md btn-primary " data-toggle="modal" id="save_book">Lưu</a>
                                </div>
                            </div>
                        </div>
                
                <div id="input_hidden">
                    <input type="hidden" id="id_in_patient_id">
                    <input type="hidden" id="id_in_staff_id">
                    <input type="hidden" id="id_in_medical_examination_form_id">
                </div>      
                <hr class="hr-show"/>
                <div class="col-sm-12 group-popup">
                    <h3>Danh sách chỉ định dịch vụ</h3>
                    <?php $this->load->view('book/gynecological_examination_chidinh_dichvu',array('lang_list'=>$lang_list,'id_in'=>$id_at)); ?>
                </div>
                
				<hr class="hr-show"/>
                <div class="col-sm-12 group-popup">
                    <h3>Danh sách xét nghiệm</h3>
                    <div class="table-responsive">
                        <div class="doc-buttons" style="display:none;"> 
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>-->
                            <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Thêm</a>
                            <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Sưa</a>
                            <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete">Xóa</a>
                        </div>
                        <div id="" class="dataTables_wrapper no-footer">                                        
                            <!-- <div id="" class="dataTables_wrapper no-footer"><div class="row"><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="DataTables_Table_0"></label></div></div><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div> -->
                                <table id="ds_xn" data-link-api="api/v1/patient/analysis_paper" data-para="" data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%" role="grid" aria-describedby="DataTables_Table_0_info"> 
                                    <thead>
                                        <tr show-position="2" role="row">
                                            <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-sort="ascending" style="width: 0px;"><input type="checkbox"></th>
                                            <th att-name="" >Mã phiếu</th>
                                            <th att-name="" >Tên xét nghiệm</th>
                                            <th att-name="" >Bác sĩ xét nghiệm</th>
                                            <th att-name="" >Kết quả</th>
                                            <th att-name="" >Thời gian xét nghiệm</th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1"><input type="checkbox"></td>
                                            <td>XN001</td>
                                            <td>Xét nghiệm máu</td>
                                            <td>Nguyễn Tuấn</td>
                                            <td>Kết quả</td>
                                            <td>10/10/2018</td>
                                        </tr>
                                    </tbody> 
                                    <tfoot></tfoot>
                                </table>
                                <div class="row"><div class="col-sm-6"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div></div></div></div>
                        </div>
                    <!-- </div> -->
                </div>
				<hr class="hr-show"/>
                <div class="col-sm-12 group-popup">
                    <h3>Danh sách cận lâm sàn</h3>
                    <div class="table-responsive">
                        <div class="doc-buttons" style="display:none;"> 
                            <!--<a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>-->
                            <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Thêm</a>
                            <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">Sưa</a>
                            <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete">Xóa</a>
                        </div>
                        <div id="" class="dataTables_wrapper no-footer">                                        
                            <div id="" class="dataTables_wrapper no-footer">
                            <div class="row" style="display:none;"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="DataTables_Table_0"></label></div></div><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
                                <table id="ds_cls" data-link-api="api/v1/subclinical/subclinical_paper" data-para="" data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%" role="grid" aria-describedby="DataTables_Table_0_info"> 
                                    <thead>
                                        <tr show-position="2" role="row">
                                            <th att-name="id" class="sorting_asc" tabindex="0" rowspan="1" colspan="1" aria-sort="ascending" style="width: 0px;"><input type="checkbox"></th>
                                            <th att-name="" >Mã phiếu</th>
                                            <th att-name="" >Tên CLS</th>
                                            <th att-name="" >Bác sĩ thực hiện</th>
                                            <th att-name="" >Kết quả</th>
                                            <th att-name="" >Thời gian thực hiện</th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1"><input type="checkbox"></td>
                                            <td>CLS001</td>
                                            <td>Siêu âm</td>
                                            <td>Nguyễn Tuấn</td>
                                            <td>Kết quả</td>
                                            <td>10/10/2018</td>
                                        </tr>
                                    </tbody> 
                                    <tfoot></tfoot>
                                </table>
                                <div class="row"><div class="col-sm-6"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div></div></div></div>
                        </div>
                    </div>
                </div>
				<hr class="hr-show"/>
                <div class="col-sm-12 group-popup">
                    <h3>Danh sách toa thuốc</h3>
                    <?php $this->load->view('book/gynecological_examination_danhsach_toathuoc', array('lang_list' => $lang_list)); ?>
                </div>
				<hr class="hr-show"/>
                <div class="col-sm-12 group-popup">
                    <h3>Danh sách toa TPCN-MP-VTYT</h3>
                    <?php $this->load->view('book/gynecological_examination_tpcn_mp_vtyt', array('lang_list' => $lang_list)); ?>
                </div>
                        
                <!-- load popup delete popup for all use -->
                <?php $this->load->view('book/template_popup_delete_show_table',array('lang_list'=>$lang_list)); ?>  
                
                
            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
