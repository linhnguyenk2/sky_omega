<?php
$lang_list = $list_lang;

$id_book_current = $id_book;
$id_parent_current = $id_parent;
?>

<section id="content">
   <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
      <header class="header bg-white b-b b-light">
         <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#"><i class="fa fa-th-list"></i> Ngoại Trú</a></li>
            <!-- <li class="active"><?= $title; ?></li> -->
            <li class="active"><?= $lang_list->line('sokham_thai') ?></li>
         </ul>
      </header>
      <section class="scrollable wrapper">
         <div class="m-b-md">
            <!-- <h3 class="m-b-none"><?= $title; ?></h3> -->
            <h3 class="m-b-none"><?= $lang_list->line('sokham_thai') ?></h3>
         </div>
         <div class="col-sm-12" style="background: white;">
            <div class="tab-pane active" id="index">
               <section class="panel panel-default" style="border: none;" attr-link-main="api/v1/patient/prenatal_book" at-id="<?php echo (isset($id_book_current) ? $id_book_current : '') ?>">
                  <div class="form-group">
                     <h4 class="col-sm-12"><?= $lang_list->line('skt_tt_benhnhan') ?> <a href="#thongtin_benhnhan" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                  </div>
                  <br>
                  <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_benhnhan">
                     <div class="col-sm-12">
                        <?php echo $this->load->view('enclitic/template_thongtin_benhnhan_static', array('list_lang' => $lang_list), true) ?>
                     </div>
                  </div>
                  <div class="form-group">
                     <h4 class="col-sm-12"><?= $lang_list->line('skt_tt_thaisan') ?> <a href="#thongtin_thaisan" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                  </div>
                  <br>
                  <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_thaisan">
                     <div class="col-sm-12">
                        <div class="form-group">
                           <label >Ngày kinh cuối</label>
                           <input type="input" class="form-control datetimepicker-date" attr-name="last_period" attr-fomart-show="dd/mm/yyyy" attr-fomart-save="dd/mm/yyyy-yyyy/mm/dd" attr-save="last_period" placeholder="" value="">
                        </div>
                        <div class="form-group">
                           <label >Ngày dự sanh</label>
                           <input type="input" class="form-control datetimepicker-date" attr-name="expected_date_confinement" attr-fomart-show="dd/mm/yyyy" attr-fomart-save="dd/mm/yyyy-yyyy/mm/dd" attr-save="expected_date_confinement" placeholder="" value="">
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <h4 class="col-sm-12"><?= $lang_list->line('skt_tt_phukhoa') ?> <a href="#thongtin_phukhoa" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                  </div>
                  <br>
                  <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_phukhoa">
                     <div class="col-sm-12">
                        <div class="form-group">
                           <label >Tuổi thấy kinh</label>
                           <input type="input" class="form-control" attr-name="age_first_menes" attr-save="age_first_menes" placeholder="" value="">
                        </div>
                        <div class="form-group">
                           <label >Số ngày 1 chu kỳ kinh</label>
                           <input type="input" class="form-control" attr-name="menstrual_cycle" attr-save="menstrual_cycle" placeholder="" value="">
                        </div>
                        <div class="form-group">
                           <label >Tình trạng kinh</label>
                           <select class="form-control" attr-name="menstrual_status" attr-save="menstrual_status">
                              <option value="1">Đều</option>
                              <option value="2">Không đều</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label >Số ngày hành kinh</label>
                           <input type="input" class="form-control" attr-name="duration_menses" attr-save="duration_menses" placeholder="" value="">
                        </div>
                        <div class="form-group">
                           <label >Lượng kinh</label>
                           <select class="form-control" attr-name="menstrual_flow" attr-save="menstrual_flow">
                              <option value="1">nhiều</option>
                              <option value="2">vừa</option>
                              <option value="3">ít</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label >Tuổi lập gia đình</label>
                           <input type="input" class="form-control" attr-name="age_marriage" attr-save="age_marriage" placeholder="" value="">
                        </div>
                        <!--                                <div class="form-group">
                           <label >Số lần có thai</label>
                           <input type="input" class="form-control" placeholder="" value="">
                           </div>-->
                        <!--                                <div class="form-group">
                           <label >Parity</label>
                           <input type="input" class="form-control" placeholder="" value="">
                           </div>-->
                     </div>
                  </div>
                  <div class="form-group">
                     <h4 class="col-sm-12"><?= $lang_list->line('skt_thongtin_khac') ?> <a href="#thongtin_khac" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                  </div>
                  <br>
                  <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_khac">
                     <div class="col-sm-12">
                        <div class="form-group">
                           <label >BW</label>
                           <input type="input" class="form-control" placeholder="" value="" attr-name="bw" attr-save="bw">
                        </div>
                        <div class="form-group">
                           <label >HIV</label>
                           <input type="input" class="form-control" placeholder="" value="" attr-name="hiv" attr-save="hiv">
                        </div>
                        <div class="form-group">
                           <label >HbsAg</label>
                           <input type="input" class="form-control" placeholder="" value="" attr-name="hbsag" attr-save="hbsag">
                        </div>
                        <div class="form-group">
                           <label >Hemoglobin</label>
                           <input type="input" class="form-control" placeholder="" value="" attr-name="hemoglobin" attr-save="hemoglobin">
                        </div>
                        <div class="form-group">
                           <label >HCT</label>
                           <input type="input" class="form-control" placeholder="" value="" attr-name="hct" attr-save="hct">
                        </div>
                        <div class="form-group">
                           <label >Nhóm máu</label>
                           <input type="input" class="form-control" placeholder="" value="" attr-name="blood_group" attr-save="blood_group">
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <div class="col-sm-12">
                        <div class="form-group">
                           <div class="doc-buttons"> 
                              <a href="#" class="btn btn-s-md btn-primary " id="save_book">Lưu</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <h4 class="col-sm-12"><?= $lang_list->line('skt_lichsu_thamkham') ?><a href="#history_examination" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                  </div>
                  <br>
                  <div class="collapse in row" aria-expanded="" id="history_examination">
                     <div class="col-sm-12">
                        <div class="table-responsive">
                           <div class="doc-buttons"> 
                              <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd_menu1" id="btn_add">In</a>
                              <a href="#" class="btn btn-s-md btn-primary" id="add_new_gynecological_examination" att-url-add="api/v1/patient/prenatal_form">Thêm mới</a>
                              <a href="#" class="btn btn-s-md btn-primary disabled edit_select" data-toggle="modal" data-target="#myAdd_menu1">Sửa</a>
                              <a href="#" class="btn btn-s-md btn-danger disabled xoa_select" data-toggle="modal" data-target="#myDeleteEverything">Xóa</a>
                           </div>
                           <div class=" ">
                              <div class="dataTables_wrapper no-footer">
                                 <div class="row">
                                    <div class="col-sm-6">
                                       <div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="DataTables_Table_0"></label></div>
                                    </div>
                                    <div class="col-sm-6">
                                       <div class="dataTables_length" id="DataTables_Table_0_length">
                                          <label>
                                             Show 
                                             <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                             </select>
                                             entries
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                                 <table id="medical_examination_form" data-link-api="api/v1/patient/medical_examination_form" data-para="type=2&medical_record_id=<?php echo (isset($id_parent_current) ? $id_parent_current : '') ?>" attr-media-record="<?= $id_parent_current ?>" data-example="example_more_demo_1" class="table table-striped m-b-none dataTable no-footer" style="width:100%" role="grid" aria-describedby="DataTables_Table_0_info">
                                    <thead>
                                       <tr show-position="2" role="row" attr-show-id="id"  for-sub-class="go-to-link" link-onclick="pregnancy_examination">
                                          <th att-name="id"><input type="checkbox"></th>
                                          <th att-name="check_in_time" attr-fomart-show="dd/mm/yyyy">Ngày khám</th>
                                          <th att-name="staff_id.user_id.fullname">Bác sĩ khám</th>
                                          <th att-name="prenatal_form_id.weight" >Cân nặng</th>
                                          <th att-name="prenatal_form_id.blood_pressure" >Huyết áp</th>
                                          <th att-name="prenatal_form_id.urine_albumin" >Albumin Niệu</th>
                                          <th att-name="prenatal_form_id.urine_glucose" >Glucose Niệu</th>
                                          <th att-name="prenatal_form_id.edema" >Phù</th>
                                          <th att-name="prenatal_form_id.fh" >Chiều cao tử cung</th>
                                          <th att-name="prenatal_form_id.fhr" >Tim thai</th>
                                          <th att-name="prenatal_form_id.presentation" >Ngôi thai</th>
                                          <th att-name="exam" >Theo dõi và điều trị</th>
                                          <th att-name="prenatal_form_id.remarks" >Ghi chú</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr role="row" class="odd go-tol-link" onclick="location.href = './pregnancy_examination';">
                                          <td class="sorting_1"><input type="checkbox"></td>
                                          <td>10/10/2018</td>
                                          <td>Nguyễn Tuấn</td>
                                          <td>07/08/2018</td>
                                          <td>Thông tin bệnh sử</td>
                                          <td>Thông tin chẩn đoán</td>
                                          <td>20/10/2018</td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                       </tr>
                                    </tbody>
                                    <tfoot></tfoot>
                                 </table>
                                 <div class="row">
                                    <div class="col-sm-6">
                                       <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div>
                                    </div>
                                    <div class="col-sm-6">
                                       <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><a class="paginate_button previous disabled" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" id="DataTables_Table_0_previous">Previous</a><span><a class="paginate_button current" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next disabled" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" id="DataTables_Table_0_next">Next</a></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               <!--</section>-->
               <?php echo $this->load->view('book/template_book_myDeleteEverything', array('list_lang' => $lang_list), true) ?>
                
            </div>
         </div>
      </section>
   </section>
</section>
<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>

<style>
    #medical_examination_form{
        overflow: auto;
        display: block;
    }
</style>