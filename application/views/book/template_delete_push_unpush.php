<?php
$lang_list = $list_lang;
?>

<div class="" id="delete" style="">
    <div class="modal fade" id="myDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Xóa</h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal" data-validate="parsley">
                        <div class="form-group"> 
                            <label class="col-sm-12 control-label more_show text-center">Xóa sổ khám</label>
                            <div class="col-sm-12 hide"> 
                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled>  
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary xoa-event">Xóa</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="" id="push" style="">
    <div class="modal fade" id="myPush" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('publish')?></h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal" data-validate="parsley">
                        <div class="form-group"> 
                            <label class="col-sm-12 control-label one_show text-center"><?=  $lang_list->line('event_publish_one') .' '.$lang_list->line('for_'.$menu) ,': <span class="at_change"></span> '.$lang_list->line('event_publish_one_change')?> </label>
                            <label class="col-sm-12 control-label more_show text-center"><?=  $lang_list->line('event_publish_more') .' '.$lang_list->line('for_'.$menu) ,' '.$lang_list->line('event_publish_more_change')?> </label>
                            <div class="col-sm-12 hide" > 
                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled>  
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save')?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="" id="unpush" style="">
    <div class="modal fade" id="myUnpush" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('unpublish')?></h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal" data-validate="parsley">
                        <div class="form-group"> 
                            <label class="col-sm-12 control-label one_show text-center"><?=  $lang_list->line('event_unpublish_one') .' '.$lang_list->line('for_'.$menu) ,': <span class="at_change"></span> '.$lang_list->line('event_unpublish_one_change')?></label>
                            <label class="col-sm-12 control-label more_show text-center"><?=  $lang_list->line('event_unpublish_more') .' '.$lang_list->line('for_'.$menu) ,' '.$lang_list->line('event_unpublish_more_change')?></label>
                            <div class="col-sm-12 hide"> 
                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled>  
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary">Xóa</button>
                </div>
            </div>
        </div>
    </div>
</div>