<?php

$lang_list = $list_lang;




?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Bệnh nhân</a></li>
                <!-- <li><a href="#"><i class="fa fa-th-list"></i> Danh sách bệnh nhân</a></li> -->
                <!-- <li class="active"><?= $title;?></li> -->
                <li class="active"><?= $lang_list->line($menu);?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title;?></h3> -->
                <h3 class="m-b-none"><?= $lang_list->line($menu);?></h3>
            </div>



            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">
                    <div style="float:left"><p>Họ Tên: Trần Nguy, Sinh ngày: 1/1/1995, Địa chỉ: 1 Nguyễn Trãi Quận 1 Tp.HCM</p></div>
                    <div style="float:right"> Mã bệnh nhân: BN001<br>Mã bệnh án: BA001<br>Tạo ngày: 10/9/2018</div>
                    <div style="clear:both"></div>
                    <br>
                    <section class="panel panel-default">

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Thông tin cá nhân</a></li>
                            <li><a data-toggle="tab" href="#menu1">Quản lý bệnh nhân</a></li>
                            <li><a data-toggle="tab" href="#menu2">Tình trạng ra viện</a></li>
                            <li><a data-toggle="tab" href="#menu3">Chuẩn đoán</a></li>
                            <li><a data-toggle="tab" href="#menu4">Chi tiết bệnh án</a></li>
                            <li><a data-toggle="tab" href="#menu5">Danh sách phiếu</a></li>
                            </ul>

                            <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <br/>
                                <!-- <h3>Thông tin bệnh nhân</h3> -->
                                <div class="row" style=" padding: 10px; ">
                                
                                <form class=" col-sm-12">
                                
                                    <?php echo $this->load->view('enclitic/template_thongtin_benhnhan',array('list_lang'=>$lang_list),true)?>
                                    
                                    <button type="submit" style="float:right" class="btn btn-primary">Cập nhật thông tin</button>
                                </form>
                                </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <!-- <h3>Danh sách bệnh án</h3> -->
                                <form class="form-horizontal" data-validate="parsley">
                                    <br/>
                                    
                                    <div class="form-group row"> 
                                        <label class="col-sm-2 control-label">Thời gian nhập viện:</label>
                                        <div class="col-sm-3"> 
                                            <span class="left2"><input type="text" class="datetimepicker" style=" width: 100%; padding: 4px; " placeholder="Chọn ngày tháng, giờ phút"/></span>
                                        </div>
                                    </div>
                                    <div class="form-group row"> 
                                        <label class="col-sm-2 control-label">Thời gian ra viện:</label>
                                        <div class="col-sm-3"> 
                                        <span class="left2"><input type="text" class="datetimepicker" style=" width: 100%; padding: 4px; " placeholder="Chọn ngày tháng, giờ phút"/></span>
                                        </div>
                                        <label class="col-sm-2 control-label">Cách ra viện:</label>
                                        <div class="col-sm-3"> 
                                            <select class="form-control" >
                                                <option>Cách ra viện</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group"> 
                                        <label class="col-sm-2 control-label">Tổng số ngày điều trị: <br><sub style="color:#679fe5">Tự động tính</sub></label>
                                        <div class="col-sm-3"> 
                                            <input type="text" class="form-control parsley-validated" data-required="" placeholder=""  value="0">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row"> 
                                        <label class="col-sm-2 control-label">Nơi giới thiệu:</label>
                                        <div class="col-sm-3"> 
                                            <select class="form-control" >
                                                <option>Chọn nơi giới thiệu</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row"> 
                                        <label class="col-sm-2 control-label">Trực tiếp vào khoa:</label>
                                        <div class="col-sm-3"> 
                                            <select class="form-control" >
                                                <option>Chọn khoa</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group"> 
                                        <label class="col-sm-2 control-label">Vào viện do bệnh này lần thứ:</label>
                                        <div class="col-sm-3"> 
                                            <input type="text" class="form-control parsley-validated" data-required="" placeholder="" value="0">
                                        </div>
                                    </div>
                                    <hr>
                                    <p> &nbsp;&nbsp;<b>Danh sách chuyển khoa</b></p>
                                        <div class="table-responsive">
                                        <div class="doc-buttons"> 
                                            <!-- <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd" id="btn_add"><?= $lang_list->line('add')?></a> -->
                                            <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete">Xóa Chuyển Khoa</a>
                                        </div>
                                        <div id="DataTables_Table_2" class="dataTables_wrapper no-footer">                                        
                                            <table data-example="example_more_demo_2" class="table table-striped m-b-none" style="width:100%"> 
                                            <thead>
                                                <tr show-position="2"> 
                                                    <th att-name="id"><input type="checkbox"/></th> 
                                                    <th att-name="">Khoa</th> 
                                                    <th att-name="">Thời gian vào khoa</th> 
                                                    <th att-name="">Thời gian rời khoa</th> 
                                                    <th att-name="">Số ngày lưu trữ</th> 
                                                    
                                                </tr> 
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="checkbox"></td>
                                                    <td>Khoa cấp cứu</td>
                                                    <td>18:00 01/01/2018</td>
                                                    <td>20:00 15/3/2018</td>
                                                    <td>73 ngày</td>
                                                </tr>
                                            </tbody> 
                                            <tfoot><tr> 
                                                </tr></tfoot>
                                            </table>
                                        </div>
                                        <br>
                                        <div class="form-group row"> 
                                            <label class="col-sm-2 control-label">Chuyển viện:</label>
                                            <div class="col-sm-3"> 
                                                <select class="form-control" >
                                                    <option>Chọn tuyến chuyển viện</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                </select>
                                            </div>
                                            <label class="col-sm-2 control-label">Đến</label>
                                            <input type="input" class="col-sm-3" placeholder="Tên bệnh viên" value="">
                                        </div>
                                        
                                        <!-- <div class="row"> -->
                                            <!-- <div class="form-group">
                                                <label >Quốc tịch</label>
                                                <select class="form-control" id="exampleFormControlSelect1">
                                                    <option>Việt Nam</option>
                                                    <option>Lào</option>
                                                    
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label >CMND/Passport</label>
                                                <input type="input" class="form-control" placeholder="" value="022568789">
                                            </div>
                                        </div> -->
                                        <br>
                                        <a href="#" class="btn btn-s-md btn-primary" style="float:right">Cập nhật thông tin</a>
                                        <br>
                                    </div>
                                    
                                </form>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <h3>Menu 2</h3>
                                <p>Some content in menu 2.</p>
                            </div>
                            <div id="menu3" class="tab-pane fade">
                                <h3>Menu 3</h3>
                                <p>Some content in menu 3.</p>
                            </div>
                            <div id="menu4" class="tab-pane fade">
                                <h3>Menu 4</h3>
                                <p>Some content in menu 4.</p>
                            </div>
                            <div id="menu5" class="tab-pane fade">
                                <!-- <h3>Danh sách thẻ bảo hiểm</h3> -->
                                <div class="table-responsive">
                                    <div class="doc-buttons"> 
                                        <!-- <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd" id="btn_add"><?= $lang_list->line('add')?></a> -->
                                        <!-- <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete"><?= $lang_list->line('delete')?></a> -->
                                    </div>
                                    <div id="DataTables_Table_3" class="dataTables_wrapper no-footer">                                        
                                        <table data-example="example_more_demo_3" class="table table-striped m-b-none" style="width:100%"> 
                                        <thead>
                                            <tr show-position="2"> 
                                                <th att-name="id"><input type="checkbox"/></th> 
                                                <th att-name="">Mã phiếu</th> 
                                                <th att-name="">Loại phiếu</th> 
                                                <th att-name="">Ngày lập phiếu</th> 
                                                <th att-name="">Mã bệnh nhân</th> 
                                                <th att-name="">Tên bệnh nhân</th> 
                                            </tr> 
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type="checkbox"></td>
                                                <td>KH001</td>
                                                <td>Khám phụ khoa</td>
                                                <td>12/09/2018</td>
                                                <td>BN001</td>
                                                <td>Trần Nguy</td>
                                            </tr>
                                            
                                        </tbody> 
                                        <tfoot><tr> 
                                            </tr></tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            </div>

                        <br>
                        <br>

                    </section>
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('edit_post')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('edit_post')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation')?></label>
                                            <div class="col-sm-9"> 
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order')?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save_change')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add"  style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('addnew')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order')?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save')?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="" id="payment_to_hospital"  style="">
                    <div class="modal fade" id="myPayment_to_hospital" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document" style=" width: 69%; min-width: 500px; ">
                            <div class="modal-content">
                                <div class="modal-header" style="text-align:center">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="modal-title" id="myModalLabel">Thanh toán ra viện</h3>
                                    <p>9/9/2018<p>
                                </div>
                                <div class="modal-body">
                                    <div class="table-responsive">
                                        <p><b>Bệnh nhân:</b> Nguyễn Trang <b>Mã Bệnh nhân:</b> BN001</p>
                                        <div class="doc-buttons"> 
                                            <!-- <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd" id="btn_add"><?= $lang_list->line('add')?></a> -->
                                            <!-- <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete"><?= $lang_list->line('delete')?></a> -->
                                        </div>
                                        <div id="DataTables_Table_4" class="dataTables_wrapper no-footer">                                        
                                            <table data-example="example_more_demo_4" class="table table-striped m-b-none" style="width:100%"> 
                                            <thead>
                                                <tr show-position="2"> 
                                                    <th att-name="id"><input type="checkbox"/></th> 
                                                    <th att-name="">STT</th> 
                                                    <th att-name="">Tên dịch vụ/Hàng hóa</th> 
                                                    <th att-name="">Đơn vị tính</th> 
                                                    <th att-name="">Số lượng</th> 
                                                    <th att-name="">Đơn giá</th> 
                                                    <th att-name="">VAT</th> 
                                                    <th att-name="">Thành tiền</th> 
                                                </tr> 
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="checkbox"></td>
                                                    <td>1</td>
                                                    <td>Sanh mổ trọn gói</td>
                                                    <td>Lần</td>
                                                    <td>1</td>
                                                    <td>20.000.000</td>
                                                    <td>5%</td>
                                                    <td>21.000.000</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox"></td>
                                                    <td>1</td>
                                                    <td>Chăm sóc sau sinh</td>
                                                    <td>Lần</td>
                                                    <td>1</td>
                                                    <td>10.000.000</td>
                                                    <td>5%</td>
                                                    <td>10.500.000</td>
                                                </tr>
                                            </tbody> 
                                            <tfoot><tr> 
                                                    <!-- <td colspan="8" >Tổng tiền:</td> -->
                                                </tr>
                                                </tfoot>
                                            </table>
                                            <div style=" text-align: right; font-size: 15px; font-weight: bold; ">Tổng tiền: 31.500.000</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <!-- <button type="button" class="btn btn-primary"><?= $lang_list->line('save')?></button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $template_delete_push_unpush; ?>


            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>

<style>
.tab-pane>h3{
    text-align:center;
}
</style>