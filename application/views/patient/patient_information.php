<?php

$lang_list = $list_lang;




?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Bệnh nhân</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> Danh sách bệnh nhân</a></li>
                <!-- <li class="active"><?= $title;?></li> -->
                <li class="active"><?= $lang_list->line($menu);?></li>
            </ul>
        </header>
        <input type="hidden" id="patient_id_get" value="<?=$patient_id_get?>" />
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title;?></h3> -->
                <h3 class="m-b-none"><?= $lang_list->line($menu);?></h3>
            </div>



            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Thông tin cá nhân</a></li>
                            <li><a data-toggle="tab" href="#menu1">Bệnh án nội trú</a></li>
                            <li><a data-toggle="tab" href="#menu5">Sổ khám ngoại trú</a></li>
                            <li><a data-toggle="tab" href="#menu2">Hồ sơ thanh toán</a></li>
                            <li><a data-toggle="tab" href="#menu3">Thông tin bảo hiểm</a></li>
                            <!-- <li><a data-toggle="tab" href="#menu4">Hỏi và đáp</a></li> -->
                            </ul>

                            <div class="tab-content">
                            <div id="home" class="tab-pane fade in active thongtin_benhnhan">
                                <br/>
                                <!-- <h3>Thông tin bệnh nhân</h3> -->
                                <div class="row" style=" padding: 10px;">
                                
                                    <form class="col-sm-12 patients" patient_id_get="<?= $patient_id_get?>" link-api="api/v1/patient/patients/<?= $patient_id_get?>">

                                        <?php echo $this->load->view('enclitic/template_thongtin_benhnhan',array('list_lang'=>$lang_list),true)?>

                                        <button type="submit" style="float:right" class="btn btn-primary">Cập nhật thông tin</button>
                                    </form>
                                    <div style="clear:both"></div>
                                    <div class="col-sm-12 group-popup" id="patient_information_bill">
                                        <div class="form-group">
                                            <h3>Thông tin xuất hóa đơn công ty</h3>
                                        </div>
                                        <?php echo $this->load->view('patient/patient_information_thongtin_xuathoadon',array('list_lang'=>$lang_list),true)?>
                                    </div>
                                    <div class="col-sm-12 group-popup" id="patient_emergency_info">
                                        <div class="form-group">
                                            <h3>Người liên hệ trong trường hợp khẩn cấp</h3>
                                        </div>
                                        <?php echo $this->load->view('patient/patient_information_nguoilienhe_khancap',array('list_lang'=>$lang_list),true)?>
                                    </div>
                                    
                                </div>
                            </div>
                            <div id="menu1" class="tab-pane fade patient_infomation_menu1">
                                <?php $data = $this->load->view('patient/patient_infomation_menu1',array('langlist'=>$lang_list),true);
                                    echo $data;
                                ?>
                            </div>
                            <div id="menu2" class="tab-pane fade patient_infomation_menu2">
                                <?php $data = $this->load->view('patient/patient_infomation_menu2',array('langlist'=>$lang_list),true);
                                    echo $data;
                                ?>
                            </div>
                            <div id="menu5" class="tab-pane fade patient_infomation_menu5">
                                <?php $data = $this->load->view('patient/patient_infomation_menu5',array('langlist'=>$lang_list),true);
                                    echo $data;
                                ?>
                            </div>
                            <div id="menu3" class="tab-pane fade patient_infomation_menu3">
                                <?php $data = $this->load->view('patient/patient_infomation_menu3',array('langlist'=>$lang_list),true);
                                    echo $data;
                                ?>
                            </div>
                            <div id="menu4" class="tab-pane fade">
                                <h3>Menu 4</h3>
                                <p>Some content in menu 4.</p>
                            </div>
                            </div>

                        <br>
                        <br>

                    </section>
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('edit_post')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('edit_post')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation')?></label>
                                            <div class="col-sm-9"> 
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order')?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save_change')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add"  style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('addnew')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order')?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save')?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                
                <?php echo $template_delete_push_unpush; ?>


            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>

<style>
.tab-pane>h3{
    text-align:center;
}

</style>