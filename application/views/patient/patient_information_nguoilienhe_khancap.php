<div class="table-responsive">
    <div id="" class="dataTables_wrapper no-footer">   
        <div class="doc-buttons"> 
            <a style="" href="#" class="btn btn-s-md btn-primary add_post" data-toggle="modal" data-target="#myThongtin_nguoilienhe"><i class="fa fa-plus"></i> Thêm</a>
            <a  href="#" class="btn btn-s-md btn-info disabled edit_select" data-toggle="modal" data-target="#myThongtin_nguoilienhe">Sửa</a> 
            <a href="#" style="display:none;" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myPush"><i class="fa fa-check"></i> Hiển thị</a>
            <a href="#" style="display:none;" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myUnpush" >Ẩn</a>
            <a href="#" class="btn btn-s-md btn-danger disabled xoa-element" data-toggle="modal" data-target="#myXoa_Thongtin_nguoilienhe" >Xóa</a>
        </div>
        <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
        <table id="patient_emergency_contact" data-link-api="api/v1/patient/patient_emergency_contact" data-para="sih_patients:id=<?php echo (isset($patient_id_get))?$patient_id_get:'' ?>" data-example="example_more" class="table table-striped m-b-none" style="width:100%"> 
            <thead>
                <tr show-position="2"> 
                    <th att-name="id" class="sorting_disabled"><input type="checkbox"/></th> 
                    <th att-name="name" data-sort="asc" class="sortat sorting_asc">Họ và tên</th> 
                    <th att-name="tel" att-requirement="true">Số điện thoại</th>  
                    <th att-name="address,district_id.name,province_id.name" att-multi-show="true">Địa chỉ</th> 
                    <th att-name="emergency_contact_relationship_id.name">QH BN</th>
                    <th att-name="patient_emergency_id.user_id.code">Mã bệnh nhân của người quan hệ</th> 
                </tr> 
            </thead>
            <tbody></tbody> 
            <tfoot>
<!--                <tr>
                    <th></th> 
                    <th></th> 
                    <th><input type="text"></th> 
                    <th><input type="text"></th> 
                    <th><input type="text"></th> 
                    <th><input type="text"></th> 
                </tr>-->
            </tfoot>
        </table>
        <div class="row">
            <div class="col-sm-6">
                <div class="dataTables_info" role="status" aria-live="polite">Showing page 1 of 1</div>
            </div>
            <div class="col-sm-6">
                <div class="dataTables_paginate paging_full_numbers"><a class="paginate_button first disabled" data-dt-idx="0" tabindex="0">First</a><a class="paginate_button previous disabled" data-dt-idx="1" tabindex="0">Previous</a><span><a class="paginate_button current" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next" data-dt-idx="7" tabindex="0">Next</a><a class="paginate_button last" data-dt-idx="8" tabindex="0">Last</a></div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myThongtin_nguoilienhe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Thêm mới người liên hệ khẩn cấp</h4>
            </div>
            <div class="modal-body">
                <!--<div class="row">-->
                <!--<div class="col-sm-7 col-sm-offset-2">-->
                <form class="form-horizontal" data-validate="parsley">
                    <input type="hidden" attr-name-load="patient_id.id" attr-save="patient_id" class="form-control" value="<?php echo (isset($patient_id_get))?$patient_id_get:'' ?>">  
                    <input type="hidden" attr-name-load="id" value="" class="form-control ">
                    
                    <div class="form-group"> <label class="col-sm-3 control-label">Mã bệnh nhân của người quan hệ</label>
                        <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder="" id="text_of_id_patient"> </div>
                    </div>
                    
                    <div class="line line-dashed line-lg pull-in"></div>
                    <div class="form-group"> <label class="col-sm-3 control-label">Họ và tên</label>
                        <input type="hidden" attr-patient-show="id" attr-save="patient_emergency_id" value="" class="form-control">
                        <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" attr-name-load="name" attr-patient-show="fullname" attr-save="name"> </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>
                    <div class="form-group"> <label class="col-sm-3 control-label">Quan hệ với bệnh nhân</label>
                        <div class="col-sm-9"> 
                            <!-- <select class="form-control" api-link="api/v1/list/patient_relationship" attr-name-load="emergency_contact_relationship_id.id" attr-save="emergency_contact_relationship_id"> -->
                            <select class="form-control emergency_contact_relationship_type" attr-name-load="emergency_contact_relationship_id.id" attr-save="emergency_contact_relationship_id">
                            </select>
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>
                    <div class="form-group"> <label class="col-sm-3 control-label">Số điện thoại</label>
                        <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control" attr-name-load="tel" attr-patient-show="mobile" attr-save="tel" placeholder=""> </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            
                            
                            <div class="col-sm-3">
                                <!--<div class="form-group">-->
                                    <label >Thành phố</label>
                                    <select class="form-control" api-link="api/v1/list/provinces" attr-name-load="province_id.id" attr-save="province_id" attr-patient-show="province_id.id">
                                        <option>Hồ Chí Minh</option>
                                        <option>Hà Nội</option>

                                    </select>
                                <!--</div>-->
                            </div>
                            <div class="col-sm-3">
                                <!--<div class="form-group">-->
                                    <label >Quận</label>
                                    <select class="form-control" api-link="api/v1/list/district" auto-load="false" attr-name-load="district_id.id" attr-save="district_id" attr-patient-show="district_id.id">
                                       
                                    </select>
                                <!--</div>-->
                            </div>

                            <div class="col-sm-3">
                                <!--<div class="form-group">-->
                                    <label >Địa chỉ</label>
                                    <input type="input" class="form-control" attr-save="address" auto-load="false" attr-name-load="address" attr-patient-show="address">
                                <!--</div>-->
                            </div>

                        </div>

                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary save-event">Lưu</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myXoa_Thongtin_nguoilienhe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Xóa</h4>
            </div>
            <div class="modal-body">

                <form class="form-horizontal" data-validate="parsley">
                    <div class="form-group"> 
                        <label class="col-sm-12 control-label one_show text-center">Bạn có chắc chắn muốn xóa Thông tin xuất hóa đơn công ty: <span class="at_change"></span> đã chọn?</label>
                        <label class="col-sm-12 control-label more_show display_none text-center">Bạn có chắc chắn muốn xóa các Thông Tin Bệnh Nhân đã được chọn?</label>
                        <div class="col-sm-12 hide"> 
                            <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled="">  
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-danger xoa-event">Xóa</button>
            </div>
        </div>
    </div>
</div>

<script>

//event for change provinces,district,ward
$(function(){
    var at_main_select_address='#myThongtin_nguoilienhe ';
    $('body').on('change',at_main_select_address+' select[attr-save="province_id"]',function(){
        var id_current=$(this).val();
        var param='sih_list_provinces:id='+id_current;
        var link_api_change =$(at_main_select_address+' select[attr-save="district_id"]').attr('api-link')
        var link = link_base+link_api_change+'/?'+param;
        get_data_api(link, {}, function (statusresult, result) {
            var html = build_html_select_option_name(result.data)
            $(at_main_select_address+'select[attr-save="district_id"]').html(html);
            var vl_attr=$(at_main_select_address+'select[attr-save="district_id"]').attr('attt-vl');
            $(at_main_select_address+'select[attr-save="district_id"]').val(vl_attr);
            // $(at_main_select_address+'select[attr-save="ward_id"]').html("");
        })
    })
    // $('body').on('change',at_main_select_address+'select[attr-value="district_id"]',function(){
    //     var id_current=$(this).val();
    //     var param='sih_list_districts:id='+id_current;
    //     var link_api_change =$(at_main_select_address+'select[attr-value="ward_id"]').attr('api-link')
    //     var link = link_base+link_api_change+'/?'+param;
    //     get_data_api(link, {}, function (statusresult, result) {
    //         var html = build_html_select_option_name(result.data)
    //         $(at_main_select_address+'select[attr-value="ward_id"]').html(html);
    //     })
    // })
    //ward_id
    $('.emergency_contact_relationship_type').html(_select_static_buld_select('emergency_contact_relationship_type'))
})
</script>