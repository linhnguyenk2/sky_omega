<?php
$lang_list = $list_lang;
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">
            <input type="hidden" value="<?=$patient_id_get?>" id="patient_id_get">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> <?= $lang_list->line('breadcrumb_patient')?></a></li>
                <!-- <li class="active"><?= $title;?></li> -->
                <li class="active"><?= $lang_list->line('list_register_services')?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title;?></h3> -->
                <h3 class="m-b-none"><?= $lang_list->line('list_register_services')?></h3>
            </div>


            

            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light" style="display: grid;">
                <div class="tab-pane active" id="index">
                    <section id="add_services_id" class="panel panel-default" style="border: none;display: grid;" >
                        <div class="col-sm-12 text-center">
                        </div>
                        <div class="form-group"> 
                            <h4 class="col-sm-12"><?= $lang_list->line('info_patient')?> <a href="#thongtin_so" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>
                        <br>
                        
                            <div class="collapse in col-sm-12" aria-expanded="" id="add_services_form">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><?= $lang_list->line('id_patient')?></label> 
                                        <span attr-show="code">
                                        	<a href="<?=base_url()?>list_patient_add" class="btn btn-primary"><?= $lang_list->line('add_patient')?></a>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <select disabled attr-name="patient_id" api-link="api/v1/patient/patients" class="form-control max-w-25 " name="patient_id" attr-value="patient_id" id="patient_id">
                                            <option value=""></option>
										</select>
                                    </div>
                                   
									<div class="form-group">
									    <label><?= $lang_list->line('name_patient')?>:</label> <span id="name_patient"></span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('birth_day')?>:</label> <span d="birth_day"></span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('marital_status')?>:</label> <span id="marital_status"></span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('gender')?>:</label> <span id="gender"></span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('nationality')?>:</label> <span id="nationality"></span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('identity_card')?>:</label> <span id="identity_card"></span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('address_patient')?>:</label> <span id="address_patient"></span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('email_patient')?>:</label> <span id="email_patient"></span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('phone_patient')?>:</label> <span id="phone_patient"></span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('cell_phone_patient')?>:</label> <span id="cell_phone_patient"></span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('job_patient')?>:</label> <span attr-show="job_patient"></span>
									</div>
                                </div>
                            </div>
                
                            <div class="form-group"> 
                                <h4 class="col-sm-12"><?= $lang_list->line('info_register_services')?> <a href="#thongtin_phukhoa" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                            </div>
                            <br>
                            <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_phukhoa">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label ><?= $lang_list->line('type_services')?></label>
                                        <select api-link="api/v1/service/service"
                                        attr-value="type_services" attr-name="service_id" class="form-control max-w-25 " name="type_services" id="type_services">
										
										</select>
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('quantity')?></label>
                                        <input type="input" id="quantity" name="quantity" class="form-control max-w-25" attr-name="quantity" placeholder="" value="">
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('reason_registration')?></label>
                                        <input type="input" id="reason" name="reason" class="form-control max-w-25" attr-name="reason" placeholder="" value="">
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('at_room')?></label>
                                        <select api-link="api/v1/service/location_in_service"
                                        attr-value="service_location_id" attr-name="service_location_id" class="form-control max-w-25" name="at_room" id="at_room">
										 
										</select>
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('price_type')?></label>
                                        <select attr-name="price_type" class="form-control max-w-25" name="price_type" id="price_type">
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                          <option value="4">4</option>

                                        </select>
                                    </div>
                                    <div class="form-group ">
                                        <label><?= $lang_list->line('execution_time')?></label>
                                        <input id="datatimepicker" type="input" name="datetimepicker" readonly class="max-w-25 form-control datetimepicker" attr-fomart-show="HH:mm dd/mm/yyyy" attr-fomart-save="HH:mm dd/mm/yyyy-yyyy/mm/dd" attr-name="appointment_time" value="">
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="doc-buttons"> 
                                            <a href="#" class="btn btn-s-md btn-primary " id="editRegisterservices"><?= $lang_list->line('edit')?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </section>
            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
<style>
	.max-w-25
	{
		max-width: 25%;
	}
</style>
