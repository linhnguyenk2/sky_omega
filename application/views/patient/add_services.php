<?php

$lang_list = $list_lang;

?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> <?= $lang_list->line('breadcrumb_patient')?></a></li>
                <!-- <li class="active"><?= $title;?></li> -->
                <li class="active"><?= $lang_list->line('list_register_services')?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title;?></h3> -->
                <h3 class="m-b-none"><?= $lang_list->line('list_register_services')?></h3>
            </div>


            

            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light" style="display: grid;">
                <div class="tab-pane active" id="index">
                    <section id="add_services_id" class="panel panel-default" attr-link-main="api/v1/patient/gynecolog_book" style="border: none;display: grid;" >
                        <div class="col-sm-12 text-center">
                        </div>
                        <div class="form-group"> 
                            <h4 class="col-sm-12"><?= $lang_list->line('info_patient')?> <a href="#thongtin_so" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>
                        <br>
                        
                            <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_so">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><?= $lang_list->line('id_patient')?></label> 
                                        <span attr-show="code">
                                        	<a href="<?=base_url()?>list_patient_add" class="btn btn-primary"><?= $lang_list->line('add_patient')?></a>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <select attr-name="patient_id" class="form-control max-w-25 " name="patient_id" id="patient_id">
										  <option value="chọn mã bệnh nhân">chọn mã bệnh nhân</option>
										  <option value="2">2</option>
										  <option value="3">3</option>
										  <option value="4">4</option>
										</select>
                                    </div>
                                   
									<div class="form-group">
									    <label><?= $lang_list->line('name_patient')?>:</label> <span attr-show="user_id.fullname">Trần Nguy</span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('birth_day')?>:</label> <span attr-show="user_id.birthday">1/1/1995</span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('marital_status')?>:</label> <span attr-show="user_id.is_married">Đã kết hôn</span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('sex')?>:</label> <span attr-show="user_id.gender">Nữ</span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('nationality')?>:</label> <span attr-show="user_id.nationality_id.name">Việt Nam</span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('identity_card')?>:</label> <span attr-show="user_id.code">passport</span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('address_patient')?>:</label> <span attr-show="user_id.province_id.name">1 Nguyễn Trãi Phường 1 Quận 1 Tp.HCM</span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('email_patient')?>:</label> <span attr-show="user_id.email">trannguy@gmail.com</span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('phone_patient')?>:</label> <span attr-show="user_id.home_phone">0288895456</span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('cell_phone_patient')?>:</label> <span attr-show="user_id.mobile">090802568785</span>
									</div>
									<div class="form-group">
									    <label><?= $lang_list->line('job_patient')?>:</label> <span attr-show="user_id.title_id.name">Nhân viên văn phòng</span>
									</div>
                                </div>
                            </div>
                
                            <div class="form-group"> 
                                <h4 class="col-sm-12"><?= $lang_list->line('info_register_services')?> <a href="#thongtin_phukhoa" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                            </div>
                            <br>
                            <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_phukhoa">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label ><?= $lang_list->line('type_services')?></label>
                                        <select attr-name="services_id" class="form-control max-w-25 " name="type_services" id="type_services">
										  <option value="Loại dịch vụ">Loại dịch vụ</option>
										  <option value="2">2</option>
										  <option value="3">3</option>
										  <option value="4">4</option>
										</select>
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('quantity')?></label>
                                        <input type="input" name="quantity" class="form-control max-w-25" attr-name="quantity" placeholder="" value="">
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('reason_registration')?></label>
                                        <input type="input" name="reason" class="form-control max-w-25" attr-name="reason" placeholder="" value="">
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('at_room')?></label>
                                        <select attr-name="service_location_id" class="form-control max-w-25" name="at_room" id="at_room">
										  <option value="chọn mã bệnh nhân">Phòng 6A</option>
										  <option value="2">2</option>
										  <option value="3">3</option>
										  <option value="4">4</option>
										</select>
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('price_type')?></label>
                                        <select attr-name="price_type" class="form-control max-w-25" name="price_type" id="price_type">
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                          <option value="4">4</option>

                                        </select>
                                    </div>
                                    <div class="form-group ">
                                        <label><?= $lang_list->line('execution_time')?></label>
                                        <input type="input" name="datetimepicker" readonly class="max-w-25 form-control datetimepicker" attr-fomart-show="HH:mm dd/mm/yyyy" attr-fomart-save="HH:mm dd/mm/yyyy-yyyy/mm/dd" attr-name="appointment_time" value="">
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="doc-buttons"> 
                                            <a href="#" class="btn btn-s-md btn-primary " id="addRegisterservices"><?= $lang_list->line('add')?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </section>
            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
<style>
	.max-w-25
	{
		max-width: 25%;
	}
</style>
