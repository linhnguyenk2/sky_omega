<?php 
$lang_list = $langlist;
//die;
?>
<h3>Bệnh án nội trú</h3>
<div class="table-responsive">
    <div class="doc-buttons"> 
        <a href="#" class="btn btn-s-md btn-primary " data-toggle="modal" data-target="#myAdd_menu1" id="btn_add"><?= $lang_list->line('add') ?></a>
        <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete"><?= $lang_list->line('delete') ?></a>
    </div>
    <div id="" class="dataTables_wrapper no-footer">   
        <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
        <table id="medical_record" id_table="#medical_record" data-link-api="api/v1/patient/medical_record" data-para="sih_patients:id=<?= $patient_id_get?>&type*=101;102;103;104" data-example="example_more" class="table table-striped m-b-none" style="width:100%" page="1" limit="10" sumpage="1"> 
        <!--<table data-example="example_more_demo_1" class="table table-striped m-b-none" style="width:100%">--> 
            <thead>
                <tr show-position="2"> 
                    <th att-name="id"><input type="checkbox"/></th> 
                    <th att-name="code">Mã bệnh án</th> 
                    <th>Loại bệnh án</th> 
                    <th att-name="patient_id.user_id.code">Mã bệnh nhân</th> 
                    <th att-name="patient_id.user_id.fullname">Tên bệnh nhân</th> 
                    <th att-name="last_modified">Ngày tạo bệnh án</th> 
                </tr> 
            </thead>
            <tbody>
                <tr>
                    <td><input type="checkbox"></td>
                    <td>BA001</td>
                    <td>Bệnh án sản</td>
                    <td>BN001</td>
                    <td>Nguyễn Trang</td>
                    <td>9/9/2018</td>
                </tr>

            </tbody> 
            <tfoot></tfoot>
        </table>
        <div class="row">
            <div class="col-sm-6">
                <div class="dataTables_info" role="status" aria-live="polite">Showing page 1 of 1</div>
            </div>
            <div class="col-sm-6">
                <div class="dataTables_paginate paging_full_numbers"><a class="paginate_button first disabled" data-dt-idx="0" tabindex="0">First</a><a class="paginate_button previous disabled" data-dt-idx="1" tabindex="0">Previous</a><span><a class="paginate_button current" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next" data-dt-idx="7" tabindex="0">Next</a><a class="paginate_button last" data-dt-idx="8" tabindex="0">Last</a></div>
            </div>
        </div>
    </div>
</div>
<div>
    <div class="modal fade" id="myAdd_menu1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew') ?> Bệnh án nội trú</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" data-validate="parsley">
<!--                                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation') ?></label>
                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                        </div>
                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation') ?></label>
                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                        </div>
                        <div class="line line-dashed line-lg pull-in"></div>
                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status') ?></label>
                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                        </div>-->
                        <div class="line line-dashed line-lg pull-in"></div>
                        <div class="form-group"> <label class="col-sm-3 control-label">Loại bệnh án</label>
                            <div class="col-sm-9"> 
                                <select class="form-control" >
                                    <option>Bệnh án phụ khoa</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>