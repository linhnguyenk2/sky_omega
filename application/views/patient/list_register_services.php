<?php

$lang_list = $list_lang;
$menu=$this->router->fetch_method();
?>
<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> <?= $lang_list->line('breadcrumb_patient')?></a></li>
                <!-- <li class="active"><?= $title;?></li> -->
                <li class="active"><?= $lang_list->line('list_register_services');?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title;?></h3> -->
                <h3 class="m-b-none"><?= $lang_list->line('list_register_services');?></h3>
            </div>


            <div class="doc-buttons">
                         <a style="display:none;" href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myPush" id="btn_publish"><i class="fa fa-check"></i> <?= $lang_list->line('publish') ?></a>

                        <a style="display:none;" href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myUnpush" id="btn_unpublish"><?= $lang_list->line('unpublish') ?></a>
                         <a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#openModal" id="btn_add">
                         <i class="fa fa-plus"></i> Thêm</a>
                         <a href="#" class="btn btn-s-md btn-info disabled" data-toggle="modal" data-target="#openModal" id="btn_edit">Chỉnh sửa</a>
                        <a href="#" class="btn btn-s-md btn-danger disabled xoa-element" data-toggle="modal" data-target="#myDelete" id="btn_delete">Xóa</a>
            </div>
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <div id="DataTables_Table_0" class="dataTables_wrapper no-footer">                                        
                            <div class="row"><div class="col-sm-6">
                                <div class="dataTables_filter"><label><?echo $lang_list->line('search')?>:<input type="search" class=""></label>
                                </div>

                                <div class="dataTables_filter"><label><?echo $lang_list->line('datetimepicker')?>:<input type="input" readonly class="list_register_services_datetimepicker form-control datetimepicker-datetoday" attr-fomart-show="dd/mm/yyyy" attr-fomart-save="dd/mm/yyyy-yyyy/mm/dd" attr-name="check_in_time" value=""></label>
                                </div>
       

                            </div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div>
                            </div>

                            <table data-link-api="api/v1/patient/specify_service_paper?staff_id_type=400" data-para="" class="table table-striped m-b-none" style="width:100%" id="list_register_services"> 
                            <thead> <tr  show-position="2"> 
                            <th att-name="id"><input type="checkbox"/></th> 
                            <th att-name="code" att-save="code" data-sort="asc" class="sortat sorting_asc"><?= $lang_list->line('code_titles')?></th> 
                            <th att-name="patient_id.id" att-save="name" att-requirement="true"><?= $lang_list->line('code_patient')?></th> 
                            <th att-name="patient_id.user_id.fullname" att-save="note"><?= $lang_list->line('name_patient')?></th> 
                            <th att-name="service_id.service_type_id.name" att-save="stat"><?= $lang_list->line('type_services')?></th> 
                            <th att-name="reason" att-save="orders"><?= $lang_list->line('reason')?></th> 
                            <th att-name="staff_id.user_id.fullname" att-save="orders"><?= $lang_list->line('author')?></th>
                            <th att-name="appointment_time" att-save="orders"><?= $lang_list->line('time')?></th>
                        </tr> 

                            </thead> 
                            <tbody> </tbody>
                            <tfoot></tfoot>
                            </table>
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="dataTables_info" role="status" aria-live="polite">Showing 1 to 10 of 46 entries</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_full_numbers" ><a class="paginate_button first disabled"  data-dt-idx="0" tabindex="0" >First</a><a class="paginate_button previous disabled" data-dt-idx="1" tabindex="0">Previous</a><span><a class="paginate_button current" data-dt-idx="2" tabindex="0">1</a><a class="paginate_button "  data-dt-idx="3" tabindex="0">2</a><a class="paginate_button " data-dt-idx="4" tabindex="0">3</a><a class="paginate_button " data-dt-idx="5" tabindex="0">4</a><a class="paginate_button " data-dt-idx="6" tabindex="0">5</a></span><a class="paginate_button next" data-dt-idx="7" tabindex="0">Next</a><a class="paginate_button last" data-dt-idx="8" tabindex="0">Last</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>
                </div>
                <div class="" id="edit"  style="">
                                    <div class="modal fade" id="openModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                                        <div class="modal-dialog" role="document" style="">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form class="col-sm-12 permission_delete_user popupdeleteuser" data-validate="parsley">
                                                    <div class="col-sm-12 text-center">
                        </div>
                        <div class="form-group"> 
                            <h4 class="col-sm-12"><?= $lang_list->line('info_patient')?> <a href="#thongtin_so" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                        </div>
                        <br>
                        
                            <div class="collapse in col-sm-12" aria-expanded="" id="add_services_form">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><?= $lang_list->line('id_patient')?></label> 
                                        <span attr-show="code">
                                            <a href="<?=base_url()?>list_patient_add" class="btn btn-primary"><?= $lang_list->line('add_patient')?></a>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <select attr-name="patient_id" api-link="api/v1/patient/patients" class="form-control max-w-25 " name="patient_id" attr-value="patient_id" id="patient_id">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label><?= $lang_list->line('name_patient')?>:</label> <span id="name_patient"></span>
                                    </div>
                                    <div class="form-group">
                                        <label><?= $lang_list->line('birth_day')?>:</label> <span d="birth_day"></span>
                                    </div>
                                    <div class="form-group">
                                        <label><?= $lang_list->line('marital_status')?>:</label> <span id="marital_status"></span>
                                    </div>
                                    <div class="form-group">
                                        <label><?= $lang_list->line('gender')?>:</label> <span id="gender">Nữ</span>
                                    </div>
                                    <div class="form-group">
                                        <label><?= $lang_list->line('nationality')?>:</label> <span id="nationality"></span>
                                    </div>
                                    <div class="form-group">
                                        <label><?= $lang_list->line('identity_card')?>:</label> <span id="identity_card"></span>
                                    </div>
                                    <div class="form-group">
                                        <label><?= $lang_list->line('address_patient')?>:</label> <span id="address_patient"></span>
                                    </div>
                                    <div class="form-group">
                                        <label><?= $lang_list->line('email_patient')?>:</label> <span id="email_patient"></span>
                                    </div>
                                    <div class="form-group">
                                        <label><?= $lang_list->line('phone_patient')?>:</label> <span id="phone_patient"></span>
                                    </div>
                                    <div class="form-group">
                                        <label><?= $lang_list->line('cell_phone_patient')?>:</label> <span id="cell_phone_patient"></span>
                                    </div>
                                    <div class="form-group">
                                        <label><?= $lang_list->line('job_patient')?>:</label> <span attr-show="job_patient">Nhân viên văn phòng</span>
                                    </div>
                                </div>
                            </div>
                
                            <div class="form-group"> 
                                <h4 class="col-sm-12"><?= $lang_list->line('info_register_services')?> <a href="#thongtin_phukhoa" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-fw fa-angle-up"></i></a></h4>
                            </div>
                            <br>
                            <div class="collapse in col-sm-12" aria-expanded="" id="thongtin_phukhoa">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label ><?= $lang_list->line('type_services')?></label>
                                        <select api-link="api/v1/service/service"
                                        attr-value="type_services" attr-name="service_id" class="form-control max-w-25 " name="type_services" id="type_services">
                                        
                                        </select>
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('quantity')?></label>
                                        <input type="input" name="quantity" class="form-control max-w-25" id="quantity" attr-name="quantity" placeholder="" value="">
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('reason_registration')?></label>
                                        <input type="input" name="reason" class="form-control max-w-25" id="reason" attr-name="reason" placeholder="" value="">
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('at_room')?></label>
                                        <select api-link="api/v1/service/location_in_service"
                                        attr-value="service_location_id" attr-name="service_location_id" class="form-control max-w-25" name="at_room" id="at_room">
                                         
                                        </select>
                                    </div>
                                    <div class="form-group ">
                                        <label ><?= $lang_list->line('price_type')?></label>
                                        <select attr-name="price_type" class="form-control max-w-25" name="price_type" id="price_type">
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                          <option value="4">4</option>

                                        </select>
                                    </div>
                                    <div class="form-group ">
                                        <label><?= $lang_list->line('execution_time')?></label>
                                        <input id="datatimepicker" type="input" name="datetimepicker" readonly class="max-w-25 form-control datetimepicker" attr-fomart-show="HH:mm dd/mm/yyyy" attr-fomart-save="HH:mm dd/mm/yyyy-yyyy/mm/dd" attr-name="appointment_time" value="">
                                    </div>
                                    
                                </div>
                            </div>
                            
    
                                                        <br>
                                                       

                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-primary"  id="editRegisterservices"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                <?php $this->load->view('list/template_delete_push_unpush',['lang_list'=>$lang_list]);?>


            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
<style type="text/css">
    .list_register_services_datetimepicker
    {
        width: 120px;
    }
        .max-w-25
    {
        max-width: 40%;
    }
</style>
