<?php
$lang_list = $langlist;
//die;
?>
<h3>Danh sách hồ sơ Thanh toán</h3>
<div class="table-responsive">
    <div class="doc-buttons"> 
        <!-- <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd" id="btn_add"><?= $lang_list->line('add') ?></a> -->
        <!-- <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete"><?= $lang_list->line('delete') ?></a> -->
    </div>
    <div id="DataTables_Table_2" class="dataTables_wrapper no-footer">                                        
        <table data-example="example_more_demo_2" class="table table-striped m-b-none" style="width:100%"> 
            <thead>
                <tr show-position="2"> 
                    <th att-name="id"><input type="checkbox"/></th> 
                    <th att-name="">Mã thanh toán</th> 
                    <th att-name="">Loại thanh toán</th> 
                    <th att-name="">Mã bệnh nhân</th> 
                    <th att-name="">Tên bệnh nhân</th> 
                    <th att-name="">Ngày tạo</th> 
                    <th att-name="">Tổng tiền</th> 
                </tr> 
            </thead>
            <tbody>
                <tr data-toggle="modal" data-target="#myPayment_to_hospital">
                    <td><input type="checkbox"></td>
                    <td>TT001</td>
                    <td>Thanh toán ra viện</td>
                    <td>BN001</td>
                    <td>Nguyễn Trang</td>
                    <td>9/9/2018</td>
                    <td>31,500,000</td>
                </tr>

            </tbody> 
            <tfoot><tr> 
                </tr></tfoot>
        </table>
    </div>
</div>
<!--s-model-->
<div class="" id="payment_to_hospital"  style="">
    <div class="modal fade" id="myPayment_to_hospital" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document" style=" width: 69%; min-width: 500px; ">
            <div class="modal-content">
                <div class="modal-header" style="text-align:center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel">Thanh toán ra viện</h3>
                    <p>9/9/2018<p>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <p><b>Bệnh nhân:</b> Nguyễn Trang <b>Mã Bệnh nhân:</b> BN001</p>
                        <div class="doc-buttons"> 
                            <!-- <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myAdd" id="btn_add"><?= $lang_list->line('add') ?></a> -->
                            <!-- <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete"><?= $lang_list->line('delete') ?></a> -->
                        </div>
                        <div id="DataTables_Table_4" class="dataTables_wrapper no-footer">                                        
                            <table data-example="example_more_demo_4" class="table table-striped m-b-none" style="width:100%"> 
                                <thead>
                                    <tr show-position="2"> 
                                        <th att-name="id"><input type="checkbox"/></th> 
                                        <th att-name="">STT</th> 
                                        <th att-name="">Tên dịch vụ/Hàng hóa</th> 
                                        <th att-name="">Đơn vị tính</th> 
                                        <th att-name="">Số lượng</th> 
                                        <th att-name="">Đơn giá</th> 
                                        <th att-name="">VAT</th> 
                                        <th att-name="">Thành tiền</th> 
                                    </tr> 
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="checkbox"></td>
                                        <td>1</td>
                                        <td>Sanh mổ trọn gói</td>
                                        <td>Lần</td>
                                        <td>1</td>
                                        <td>20.000.000</td>
                                        <td>5%</td>
                                        <td>21.000.000</td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox"></td>
                                        <td>1</td>
                                        <td>Chăm sóc sau sinh</td>
                                        <td>Lần</td>
                                        <td>1</td>
                                        <td>10.000.000</td>
                                        <td>5%</td>
                                        <td>10.500.000</td>
                                    </tr>
                                </tbody> 
                                <tfoot><tr> 
                                        <!-- <td colspan="8" >Tổng tiền:</td> -->
                                    </tr>
                                </tfoot>
                            </table>
                            <div style=" text-align: right; font-size: 15px; font-weight: bold; ">Tổng tiền: 31.500.000</div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                    <!-- <button type="button" class="btn btn-primary"><?= $lang_list->line('save') ?></button> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--e-model-->