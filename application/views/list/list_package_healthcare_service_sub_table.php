<div class="grouplistsub">
    <h3>Danh sách cận lâm sàn trong gói</h3>
    <div class="table-responsive">
        <div class="dataTables_wrapper">
            <div class="doc-buttons"> 
                <a href="#" class="btn btn-s-md btn-danger disabled xoa-element">Xóa</a>
            </div>
            <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
            <table data-link-api="api/v1/list/subclinical_in_package_healthcare_service" data-link-select="api/v1/list/subclinical" data-info-save="list_subclinical_in_package_healthcare_service" class="table table-striped m-b-none" > 
                <thead> 
                    <tr show-position="2">
                        <th class="sorting_disabled"><input type="checkbox"/></th> 
                        <th att-name="subclinical_id" att-sub-name="code" data-sort="asc" class="sortat sorting_asc">Mã CLS</th> 
                        <th att-name="subclinical_id" att-sub-name="name" att-save="subclinical_id">Tên CLS</th> 
                        <th att-name="quantity" att-save="quantity">Số lần dùng dịch vụ</th> 
                    </tr> 
                </thead> 
                <tbody> </tbody> 
                <tfoot><tr>
                        <th></th> 
                        <th></th> 
                        <th class="data-type-select form-group subclinical" at-select="subclinical"></th>
                        <th><input type="text" value=""></th>
                    </tr></tfoot>
            </table>
        </div>
    </div>
</div>
<div class="grouplistsub">
    <h3>Danh sách thuốc trong gói</h3>
    <div class="table-responsive">
        <div class="dataTables_wrapper">
            <div class="doc-buttons"> 
                <a href="#" class="btn btn-s-md btn-danger disabled xoa-element">Xóa</a>
            </div>
            <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
            <table data-link-api="api/v1/list/medicine_in_package_healthcare_service" data-link-select="api/v1/list/medicine" data-info-save="list_medicine_in_package_healthcare_service" class="table table-striped m-b-none" > 
                <thead> 
                    <tr show-position="2"> 
                        <th class="sorting_disabled"><input type="checkbox"/></th> 
                        <th att-name="medicine_id" att-sub-name="code" data-sort="asc" class="sortat sorting_asc">Mã thuốc</th> 
                        <th att-name="medicine_id" att-sub-name="name" att-requirement="true" att-save="medicine_id">Tên thuốc</th> 
                        <th att-name="medicine_id" att-sub-name="unit_id_name" att-requirement="true" att-save="unit_medicine_id">Đơn vị</th> 
                        <th att-name="quantity" att-requirement="true" att-save="quantity">Số lượng</th> 
                    </tr> 
                </thead> 
                <tbody> </tbody> 
                <tfoot><tr>
                        <th></th> 
                        <th></th> 
                        <th class="data-type-select form-group medicine" at-select="medicine"></th>
                        <th></th>
                        <th><input type="text" value=""></th>
                    </tr></tfoot>
            </table>
        </div>
    </div>
</div>
<div class="grouplistsub">
    <h3>Danh sách vật tư y tế trong gói</h3>
    <div class="table-responsive">
        <div class="dataTables_wrapper">
            <div class="doc-buttons"> 
                <a href="#" class="btn btn-s-md btn-danger disabled xoa-element">Xóa</a>
            </div>
            <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
            <table data-link-api="api/v1/list/medical_supplies_in_package_healthcare_service" data-link-select="api/v1/list/medical_supplies" data-info-save="list_medical_supplies_in_package_healthcare_service" class="table table-striped m-b-none" > 
                <thead> 
                    <tr show-position="2"> 
                        <th class="sorting_disabled"><input type="checkbox"/></th> 
                        <th att-name="medical_supplies_id" att-sub-name="code" data-sort="asc" class="sortat sorting_asc">Mã VTYT</th> 
                        <th att-name="medical_supplies_id" att-sub-name="name" att-requirement="true"  att-save="medical_supplies_id">Tên VTYT</th> 
                        <th att-name="quantity" att-requirement="true"  att-save="quantity">Số lượng</th> 
                    </tr> 
                </thead> 
                <tbody> </tbody> 
                <tfoot><tr>
                        <th></th> 
                        <th></th> 
                        <th  class="data-type-select form-group medical_supplies"  at-select="medical_supplies"><input type="text"></th>
                        <th><input type="text" value=""></th>
                    </tr></tfoot>
            </table>
        </div>
    </div>
</div>
<div class="grouplistsub">
    <h3>Danh sách dịch vụ trong gói</h3>
    <div class="table-responsive">
        <div class="dataTables_wrapper">
            <div class="doc-buttons"> 
                 <a href="#" class="btn btn-s-md btn-danger disabled xoa-element">Xóa</a>
            </div>
            <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
            <table data-link-api="api/v1/list/service_in_package_healthcare_service" data-link-select="api/v1/list/service" data-info-save="list_service_in_package_healthcare_service" class="table table-striped m-b-none" > 
                <thead> 
                    <tr show-position="2"> 
                        <th class="sorting_disabled"><input type="checkbox"/></th> 
                        <th att-name="healthcare_service_id" att-sub-name="code" data-sort="asc" class="sortat sorting_asc">Mã dịch vụ</th> 
                        <th att-name="healthcare_service_id" att-sub-name="name" att-requirement="true" att-save="healthcare_service_id">Tên dịch vụ</th> 
                        <th att-name="quantity" att-requirement="true" att-save="quantity">Số lần dùng DV</th> 
                    </tr> 
                </thead> 
                <tbody> </tbody> 
                <tfoot><tr>
                        <th></th> 
                        <th></th> 
                        <th  class="data-type-select form-group service" at-select="service"></th>
                        <th><input type="text" value=""></th>
                    </tr></tfoot>
            </table>
        </div>
    </div>
</div>
<div class="grouplistsub">
    <h3>Danh sách lưu trú trong gói</h3>
    <div class="table-responsive">
        <div class="dataTables_wrapper">
            <div class="doc-buttons"> 
                 <a href="#" class="btn btn-s-md btn-danger disabled xoa-element">Xóa</a>
            </div>
            <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
            <table data-link-api="api/v1/list/room_type_in_package_healthcare_service" data-link-select="api/v1/list/patient_room_type" data-info-save="list_room_type_in_package_healthcare_service"  class="table table-striped m-b-none" > 
                <thead> 
                    <tr show-position="2"> 
                        <th class="sorting_disabled"><input type="checkbox"/></th> 
                        <th att-name="patient_room_type_id" att-sub-name="code" data-sort="asc" class="sortat sorting_asc">Mã loại phòng</th> 
                        <th att-name="patient_room_type_id" att-sub-name="name"  att-save="patient_room_type_id">Tên loại phòng</th> 
                        <th att-name="quantity" att-requirement="true" att-save="quantity">Số đêm</th> 
                    </tr> 
                </thead> 
                <tbody> </tbody> 
                <tfoot><tr>
                        <th></th> 
                        <th></th> 
                        <th class="data-type-select form-group patient_room_type" at-select="patient_room_type"></th>
                        <th><input type="text" value=""></th>
                    </tr></tfoot>
            </table>
        </div>
    </div>
</div>
