<?php

$lang_list = $list_lang;




?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> <?= $lang_list->line('list')?></a></li>
                <!-- <li class="active"><?= $title;?></li> -->
                <li class="active"><?= $lang_list->line($menu);?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title;?></h3> -->
                <h3 class="m-b-none"><?= $lang_list->line($menu);?></h3>
            </div>


            <div class="doc-buttons"> 
                        <a style="display:none;" href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myAdd" id="btn_add"><i class="fa fa-plus"></i> <?= $lang_list->line('add')?></a>
                        <a style="display:none;" href="#" class="btn btn-s-md btn-info disabled" data-toggle="modal" data-target="#myEdit" id="btn_edit"><?= $lang_list->line('edit')?></a> 
                        <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myPush" id="btn_publish"><i class="fa fa-check"></i> <?= $lang_list->line('publish')?></a>
                        <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myUnpush" id="btn_unpublish"><?= $lang_list->line('unpublish')?></a>
                        <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete"><?= $lang_list->line('delete')?></a>
                
            </div>

            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">
                        <div class="table-responsive">
                        <div id="DataTables_Table_0" class="dataTables_wrapper no-footer">
                                <table data-example="example_more" class="table table-striped m-b-none" style="width:100%">
                                    <thead>
                                        <tr>
                                            
                                            <th></th>
                                            <th><?= $lang_list->line('name_medicine'); ?></th>
                                            <th><?= $lang_list->line('code_medicine'); ?></th>
                                            <th><?= $lang_list->line('compound_name_medicine'); ?></th>
                                            <th><?= $lang_list->line('way_in_medicine'); ?></th>
                                            <th><?= $lang_list->line('specification_medicine'); ?></th>
                                            <th><?= $lang_list->line('unit_medicine'); ?></th>
                                            <th><?= $lang_list->line('unit_price_medicine'); ?></th>
                                            <th style="width: 360px;"><?= $lang_list->line('explain_medicine'); ?></th>
                                            <th att-name="vat"><?= $lang_list->line('vat')?></th> 
                                            <th><?= $lang_list->line('note_medicine'); ?></th>
                                            <th><?= $lang_list->line('point_medicine'); ?></th>
                                            <th><?= $lang_list->line('contraindications_medicine'); ?></th>
                                            <th><?= $lang_list->line('the_drug_is_not_combined_medicine'); ?></th>
                                            <th><?= $lang_list->line('subordinate_units_medicine'); ?></th>
                                            <th><?= $lang_list->line('status'); ?></th>
                                            <th><?= $lang_list->line('order'); ?></th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row" class="odd">
                                        <!-- <td>1</td> -->
                                        <td><input type="checkbox"></td>
                                        <td>Panadol</td>
										<td>MT01</td>
                                        <td>Panadol</td>
                                        <td>Uống</td>
                                        <td>H/80</td>
                                        <td>Viên</td>
                                        <td>2000</td>
                                        <td>Thuốc an thần</td>
                                        <td>5</td>
                                        <td>Gây buồn ngủ</td>
                                        <td>1-2 tuổi: 1 viên <br>5-6 tuổi: 1 viên <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow01" id="btn_show01"><?= $lang_list->line('edit')?></a></td>
                                        <td>Phụ nữ có thai<br>Cho con bú <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow02" id="btn_show02"><?= $lang_list->line('edit')?></a></td>
                                        <td>Paracetamon<br>UP-C <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow03" id="btn_show03"><?= $lang_list->line('edit')?></a></td>
                                        <td>1 hộp: 100 viên<br>1 vĩ: 10 viên <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow04" id="btn_show04"><?= $lang_list->line('edit')?></a></td>
                                        <td>Hiện</td>
                                        <td>1</td>
                                        
                                    </tr>
									<tr role="row" class="odd">
                                        <!-- <td>1</td> -->
                                        <td><input type="checkbox"></td>
                                        <td>Panadol xanh giảm đau</td>
                                        <td>MT02</td>
										<td>Panadol</td>
                                        <td>Uống</td>
                                        <td>H/80</td>
                                        <td>Viên</td>
                                        <td>2000</td>
                                        <td>Thuốc giảm đau</td>
                                        <td>5</td>
                                        <td>Gây buồn ngủ</td>
                                        <td>1-2 tuổi: 1 viên <br>5-6 tuổi: 1 viên <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow01" id="btn_show01"><?= $lang_list->line('edit')?></a></td>
                                        <td>Phụ nữ có thai<br>Cho con bú <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow02" id="btn_show02"><?= $lang_list->line('edit')?></a></td>
                                        <td>Paracetamon<br>UP-C <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow03" id="btn_show03"><?= $lang_list->line('edit')?></a></td>
                                        <td>1 hộp: 100 viên<br>1 vĩ: 10 viên <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow04" id="btn_show04"><?= $lang_list->line('edit')?></a></td>
                                        <td>Hiện</td>
                                        <td>2</td>
                                        
                                    </tr>
									<tr role="row" class="odd">
                                        <!-- <td>1</td> -->
                                        <td><input type="checkbox"></td>
                                        <td>Panadol xanh lá cảm cúm</td>
                                        <td>MT03</td>
										<td>Panadol</td>
                                        <td>Uống</td>
                                        <td>H/80</td>
                                        <td>Viên</td>
                                        <td>2000</td>
                                        <td>Thuốc giảm đau</td>
                                        <td>5</td>
                                        <td>Gây buồn ngủ</td>
                                        <td>1-2 tuổi: 1 viên <br>5-6 tuổi: 1 viên <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow01" id="btn_show01"><?= $lang_list->line('edit')?></a></td>
                                        <td>Phụ nữ có thai<br>Cho con bú <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow02" id="btn_show02"><?= $lang_list->line('edit')?></a></td>
                                        <td>Paracetamon<br>UP-C <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow03" id="btn_show03"><?= $lang_list->line('edit')?></a></td>
                                        <td>1 hộp: 100 viên<br>1 vĩ: 10 viên <br><a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myShow04" id="btn_show04"><?= $lang_list->line('edit')?></a></td>
                                        <td>Hiện</td>
                                        <td>2</td>
                                        
                                    </tr>
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </section>
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('edit_post')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('edit_post')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation')?></label>
                                            <div class="col-sm-9"> 
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order')?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save_change')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add"  style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('addnew')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order')?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save')?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="" id="show01" style="">
                    <div class="modal fade" id="myShow01" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Chỉ định</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" data-validate="parsley">
                                        
                                        <div class="form-group row s-form-chidinh">
                                            <div class="col-sm-1"><label for="">Từ</label></div>
                                            <div class="col-sm-1">
                                                <select class="form-control" id="">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-1"><label for="">tuổi</label></div>
                                            <div class="col-sm-1"><label for="">Đến</label></div>
                                            <div class="col-sm-1">
                                                <select class="form-control" id="">
                                                <option>1</option>
                                                <option selected>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-1"><label for="">tuổi</label></div>
                                            <div class="col-sm-1 s-padding-0"><label for="">Chỉ định</label></div>
                                            <div class="col-sm-2"><input type="text" class="form-control"/></div>
                                            <div class="col-sm-1"><label for="">Viên</label></div>
                                            <div class="col-sm-1"><button class="btn btn-danger">Xóa</button></div>
                                        </div>
                                        <div class="form-group row s-form-chidinh">
                                            <div class="col-sm-1"><label for="">Từ</label></div>
                                            <div class="col-sm-1">
                                                <select class="form-control" id="">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option selected>5</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-1"><label for="">tuổi</label></div>
                                            <div class="col-sm-1"><label for="">Đến</label></div>
                                            <div class="col-sm-1">
                                                <select class="form-control" id="">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option selected>6</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-1"><label for="">tuổi</label></div>
                                            <div class="col-sm-1 s-padding-0"><label for="">Chỉ định</label></div>
                                            <div class="col-sm-2"><input type="text" class="form-control"/></div>
                                            <div class="col-sm-1"><label for="">Viên</label></div>
                                            <div class="col-sm-1"><button class="btn btn-danger">Xóa</button></div>
                                        </div>
                                        <div class="form-group row s-form-chidinh">
                                            <div class="col-sm-1"><label for="">Từ</label></div>
                                            <div class="col-sm-1">
                                                <select class="form-control" id="">
                                                <option></option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-1"><label for="">tuổi</label></div>
                                            <div class="col-sm-1"><label for="">Đến</label></div>
                                            <div class="col-sm-1">
                                                <select class="form-control" id="">
                                                <option></option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-1"><label for="">tuổi</label></div>
                                            <div class="col-sm-1 s-padding-0"><label for="">Chỉ định</label></div>
                                            <div class="col-sm-2"><input type="text" class="form-control"/></div>
                                            <div class="col-sm-1"><label for="">Viên</label></div>
                                            <div class="col-sm-1"><button class="btn btn-primary">Thêm</button></div>
                                        </div>
                                       
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button> -->
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="show02" style="">
                    <div class="modal fade" id="myShow02" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Chống chỉ định</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" data-validate="parsley">
                                        
                                        
                                        <div class="form-group row s-form-chongchidinh">
                                            
                                            <div class="col-sm-10">
                                                <select class="form-control" id="">
                                                <option>Phụ nữ cho con bú</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                </select>
                                            </div>
                                            
                                            <div class="col-sm-2"><button class="btn btn-danger">Xóa</button></div>
                                        </div>
                                        <div class="form-group row s-form-chongchidinh">
                                            
                                            <div class="col-sm-10">
                                                <select class="form-control" id="">
                                                <option>Phụ nữ có thai</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                </select>
                                            </div>
                                            
                                            <div class="col-sm-2"><button class="btn btn-danger">Xóa</button></div>
                                        </div>
                                        <div class="form-group row s-form-chongchidinh">
                                            
                                            <div class="col-sm-10">
                                                <select class="form-control" id="">
                                                <option>Chọn chống chỉ định</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                </select>
                                            </div>
                                            
                                            <div class="col-sm-2"><button class="btn btn-primary">Thêm</button></div>
                                        </div>
                                        
                                       
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button> -->
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="show03" style="">
                    <div class="modal fade" id="myShow03" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Thuốc không dùng chung</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" data-validate="parsley">
                                        
                                        
                                        <div class="form-group row s-form-chongchidinh">
                                            
                                            <div class="col-sm-10">
                                                <select class="form-control" id="">
                                                <option>Paracetamon</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                </select>
                                            </div>
                                            
                                            <div class="col-sm-2"><button class="btn btn-danger">Xóa</button></div>
                                        </div>
                                        <div class="form-group row s-form-chongchidinh">
                                            
                                            <div class="col-sm-10">
                                                <select class="form-control" id="">
                                                <option>UP-C</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                </select>
                                            </div>
                                            
                                            <div class="col-sm-2"><button class="btn btn-danger">Xóa</button></div>
                                        </div>
                                        <div class="form-group row s-form-chongchidinh">
                                            
                                            <div class="col-sm-10">
                                                <select class="form-control" id="">
                                                <option>Chọn chống chỉ định</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                </select>
                                            </div>
                                            
                                            <div class="col-sm-2"><button class="btn btn-primary">Thêm</button></div>
                                        </div>
                                        
                                       
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button> -->
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="show04" style="">
                    <div class="modal fade" id="myShow04" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Đơn vị phụ</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" data-validate="parsley">
                                        
                                        
                                        <div class="form-group row s-form-donviphu">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-1"><label for="">Từ</label></div>
                                            <div class="col-sm-2"><input class="form-control" value="Hộp"></div>
                                            <div class="col-sm-1"><label for="">Đến</label></div>
                                            <div class="col-sm-2"><input class="form-control" value="100"></div>
                                            <div class="col-sm-1"><label for="">Viên</label></div>
                                            <div class="col-sm-2"><button class="btn btn-danger">Xóa</button></div>
                                        </div>
                                        <div class="form-group row s-form-donviphu">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-1"><label for="">Từ</label></div>
                                            <div class="col-sm-2"><input class="form-control" value="Vĩ"></div>
                                            <div class="col-sm-1"><label for="">Đến</label></div>
                                            <div class="col-sm-2"><input class="form-control" value="10"></div>
                                            <div class="col-sm-1"><label for="">Viên</label></div>
                                            <div class="col-sm-2"><button class="btn btn-danger">Xóa</button></div>
                                        </div>
                                        <div class="form-group row s-form-donviphu">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-1"><label for="">Từ</label></div>
                                            <div class="col-sm-2"><input class="form-control"></div>
                                            <div class="col-sm-1"><label for="">Đến</label></div>
                                            <div class="col-sm-2"><input class="form-control"></div>
                                            <div class="col-sm-1"><label for="">Viên</label></div>
                                            <div class="col-sm-2"><button class="btn btn-primary">Thêm</button></div>
                                        </div>
                                        
                                        
                                       
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button> -->
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $template_delete_push_unpush; ?>


            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>

<style>
.dataTables_length {
    display: inline-block;
}
.s-form-chidinh .col-sm-1{
    /* padding:2px; */
}
.s-form-chidinh button,
.s-form-chongchidinh button,
.s-form-donviphu button
{
    min-width:63px;
}
.s-form-chidinh select{
    width: 56px;
    padding-left: 5px;
    margin-left: -17px;
    margin-top: -4px;
}
.s-form-chidinh input{
    width:100%;
}
.s-padding-0{
    padding:0;
}
</style>