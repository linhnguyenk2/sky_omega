<?php

$lang_list = $list_lang;




?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> <?= $lang_list->line('list')?></a></li>
                <!-- <li class="active"><?= $title;?></li> -->
                <li class="active"><?= $lang_list->line($menu);?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title;?></h3> -->
                <h3 class="m-b-none"><?= $lang_list->line($menu);?></h3>
            </div>


            <div class="doc-buttons"> 
            
                        <a style="display:none;" href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myAdd" id="btn_add"><i class="fa fa-plus"></i> <?= $lang_list->line('add')?></a>
                        
                        <a style="display:none;" href="#" class="btn btn-s-md btn-info disabled" data-toggle="modal" data-target="#myEdit" id="btn_edit"><?= $lang_list->line('edit')?></a> 
                        
                        <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myPush" id="btn_publish"><i class="fa fa-check"></i> <?= $lang_list->line('publish')?></a>
                        
                        <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myUnpush" id="btn_unpublish"><?= $lang_list->line('unpublish')?></a>
                        
                        <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete"><?= $lang_list->line('delete')?></a>
                        
            </div>

            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <div id="DataTables_Table_0" class="dataTables_wrapper no-footer">                                        
                                <table data-example="example_more" class="table table-striped m-b-none" style="width:100%"> 
                                <thead>
                                    <tr show-position="2"> 
                                        
                                        <th att-name="id"><input type="checkbox"/></th> 
                                        <th att-name="code"><?= $lang_list->line('code_list_place_introduction')?></th> 
                                        <th att-name="name"><?= $lang_list->line('name_list_place_introduction')?></th>  
                                        <th att-name="stat"><?= $lang_list->line('status')?></th> 
                                        <th att-name="order"><?= $lang_list->line('order')?></th> 
                                    </tr> 
                                </thead>
                                <tbody> 
									<tr>
										<td><input type="checkbox"></td>
										<td>001</td>
										<td>Cơ quan y tế</td>
										<td>Hiển thị</td>
										<td>1</td>
									</tr>
									<tr>
										<td><input type="checkbox"></td>
										<td>002</td>
										<td>Trạm y tế phường xã</td>
										<td>Hiển thị</td>
										<td>2</td>
									</tr>
									
								</tbody> 
                                <tfoot><tr> 
                                        <th></th> 
                                        <th></th> 
                                        <th></th> 
                                        <th></th> 
                                        <th data-type-select="select-status"></th> 
                                        <th></th> 
                                    </tr></tfoot>
                                </table>
                            </div>
                        </div>

                    </section>
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('edit_post')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('edit_post')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation')?></label>
                                            <div class="col-sm-9"> 
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order')?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save_change')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add"  style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('addnew')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order')?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save')?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php echo $template_delete_push_unpush; ?>


            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>
