<?php
$lang_list = $list_lang;



    
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> <?= $lang_list->line('list') ?></a></li>
                <!-- <li class="active"><?= $title; ?></li> -->
                <li class="active"><?= $lang_list->line($menu); ?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title; ?></h3> -->
                <h3 class="m-b-none"><?= $lang_list->line($menu); ?></h3>
            </div>


            <div class="doc-buttons"> 

                <a style="" href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myAdd" id="btn_add"><i class="fa fa-plus"></i> <?= $lang_list->line('add') ?></a>

                <a style="" href="#" class="btn btn-s-md btn-info disabled" data-toggle="modal" data-target="#myEdit" id="btn_edit"><?= $lang_list->line('edit') ?></a> 

                <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myPush" id="btn_publish"><i class="fa fa-check"></i> <?= $lang_list->line('publish') ?></a>

                <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myUnpush" id="btn_unpublish"><?= $lang_list->line('unpublish') ?></a>

                <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete"><?= $lang_list->line('delete') ?></a>

            </div>

            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <div id="DataTables_Table_0" class="dataTables_wrapper no-footer">    
                                <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
                                <table data-link-api="api/v1/list/package_healthcare_service" data-example="example_more" class="table table-striped m-b-none" style="width:100%"> 
                                    <thead>
                                        <tr show-position="2"> 
                                            <th att-name="id" class="sorting_disabled"><input type="checkbox"/></th> 
                                            <th att-name="code" data-sort="asc" class="sortat sorting_asc"><?= $lang_list->line('code_package_healthcare_service') ?></th> 
                                            <th att-name="name"><?= $lang_list->line('name_package_healthcare_service') ?></th>  
                                            <th att-name="package_healthcare_service_type_id"><?= $lang_list->line('package_healthcare_service_type_id_package_healthcare_service') ?></th>  
                                            <th att-name="price"><?= $lang_list->line('price_package_healthcare_service') ?></th> 
                                            <th att-name="price_foreigner"><?= $lang_list->line('price_foreigner_package_healthcare_service') ?></th> 
                                            <th att-name="stat"><?= $lang_list->line('status') ?></th> 
                                            <th att-name="orders"><?= $lang_list->line('order') ?></th> 
                                        </tr> 
                                    </thead>
                                    <tbody> </tbody> 
                                    <tfoot><tr> 
                                            <th></th> 
                                            <th att-edit="false"></th> 
                                            <th att-edit="false"></th> 
                                            <th att-edit="false" data-type-select="package_healthcare_service_type"></th>
                                            <th att-edit="false"></th> 
                                            <th att-edit="false"></th> 
                                            <th att-edit="false" data-type-select="select-status"></th> 
                                            <th att-edit="false"></th> 
                                        </tr></tfoot>
                                </table>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="dataTables_info" role="status" aria-live="polite">Showing 1 to 10 of 46 entries</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_full_numbers"><a class="paginate_button first disabled" data-dt-idx="0" tabindex="0">First</a><a class="paginate_button previous disabled" data-dt-idx="1" tabindex="0">Previous</a><span><a class="paginate_button current" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next" data-dt-idx="7" tabindex="0">Next</a><a class="paginate_button last" data-dt-idx="8" tabindex="0">Last</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('edit_post') ?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('edit_post') ?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_package_healthcare_service') ?></label>
                                            <div class="col-sm-9">
                                                <input type="hidden" class="form-control parsley-validated" sh-cl="id" data-required="true" placeholder="required">  
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="text" class="form-control parsley-validated" sh-cl="code" data-required="true" placeholder="required">  
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_package_healthcare_service') ?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" sh-cl="name" placeholder=""> </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('package_healthcare_service_type_id_package_healthcare_service') ?></label>
                                            <div class="col-sm-9"> 
                                                <select class="select-type-head form-control"></select>
                                                <input type="text" class="form-control input-have-select parsley-validated hide" data-required="true" sh-cl="package_healthcare_service_type_id" placeholder="required">  
                                            </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('price_package_healthcare_service') ?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" sh-cl="price"  placeholder="required">  </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('price_foreigner_package_healthcare_service') ?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" sh-cl="price_foreigner" placeholder="required">  </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status') ?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" sh-cl="stat" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order') ?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" sh-cl="orders" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <button id="EditaddRow" style="display:none">EditaddRow</button>
                                    <!--h4>Thông Tin Khám Chữa Bệnh (Dịch Vụ)</h4>
                                                                        <div class="table-responsive">
                                                                                <div id="DataTables_Table_1" class="dataTables_wrapper no-footer">                                        
                                                                                        <table data-example="example_more_edit" class="table table-striped m-b-none" style="width:100%"> 
                                                                                                <thead> <tr show-position="2"> 
                                                                                                <th style="display:none">ID</th> 
                                                                                                <th att-name="id"><input type="checkbox"/></th> 
                                                                                                <th att-name="code"><?= $lang_list->line('code_healthcare') ?></th> 
                                                                                                <th att-name="name"><?= $lang_list->line('name_healthcare') ?></th> 
                                                                                                <th att-name="healthcare_type_id"><?= $lang_list->line('type_healthcare') ?></th> 
                                                                                                <th att-name=""><?= $lang_list->line('group_healthcare') ?></th> 
                                                                                                <th att-name="unit"><?= $lang_list->line('unit_healthcare') ?></th> 
                                                                                                <th att-edit-name="number"><?= $lang_list->line('quantity_healthcare') ?></th> 
                                                                                                <th att-name="price_in_hour"><?= $lang_list->line('price_in_hour_healthcare') ?></th> 
                                                                                                <th att-name="price_overtime"><?= $lang_list->line('price_overtime_healthcare') ?></th> 
                                                                                                <th att-name="price_holiday"><?= $lang_list->line('price_holiday_healthcare') ?></th> 
                                                                                                <th att-name="price_foreigner"><?= $lang_list->line('price_foreigner_healthcare') ?></th> 
                                                                                                <th att-name="note"><?= $lang_list->line('note') ?></th> 
                                                                                                <th att-edit-name="stat"><?= $lang_list->line('status') ?></th> 
                                                                                                <th att-edit-name="order"><?= $lang_list->line('order') ?></th> 
                                                                                                </tr> 
                                                                                                </thead>
                                                                                                <tbody> </tbody>
                                                                                                <tfoot>
                                                                                                    <tr>
                                                                                                        <th style="display:none" att-edit="false"></th>
                                                                                                        <th att-edit="false"></th>
                                                                                                        <th att-edit="false" data-trigge="change" data-attr="code">load</th>
                                                                                                        <th att-edit="false" data-trigge="change" data-attr="name">load</th>
                                                                                                        <th att-edit="false"></th>
                                                                                                        <th att-edit="false"></th>
                                                                                                        <th att-edit="false"></th>
                                                                                                        <th><input type="text" placeholder="<?= $lang_list->line('quantity_healthcare') ?>"></th>
                                                                                                        <th att-edit="false"></th>
                                                                                                        <th att-edit="false"></th>
                                                                                                        <th att-edit="false"></th>
                                                                                                        <th att-edit="false"></th>
                                                                                                        <th att-edit="false"></th>
                                                                                                        <th att-edit="false" data-type-select="select-status"><input type="text" placeholder="<?= $lang_list->line('status') ?>"></th>
                                                                                                        <th><input type="text" placeholder="<?= $lang_list->line('order') ?>"></th>
                                                                                                    </tr>
                                                                                                </tfoot>
                                                                                        </table>
                                            <button type="button" class="xoa_select btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDeletehealthcare">Xóa</button>
                                                                                </div>          
                                                                        </div-->
                                    <!--</div>-->

                                    <!--</div>-->
                                    <?php echo $template_list_package_healthcare_service_sub_table; ?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save_change') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add"  style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew') ?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('addnew') ?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_package_healthcare_service') ?></label>
                                            <div class="col-sm-9"> <input type="text" sh-cl="code" class="form-control parsley-validated">  </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_package_healthcare_service') ?></label>
                                            <div class="col-sm-9"> <input type="text" sh-cl="name" class="form-control parsley-validated" placeholder="required">  </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('package_healthcare_service_type_id_package_healthcare_service') ?></label>
                                            <div class="col-sm-9">
                                                <select class="select-type-head form-control"></select>
                                                <input type="text" sh-cl="package_healthcare_service_type_id" class="form-control input-have-select parsley-validated hide" placeholder="required">  
                                            </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('price_package_healthcare_service') ?></label>
                                            <div class="col-sm-9"> <input type="text" sh-cl="price" class="form-control parsley-validated" placeholder="required">  </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('price_foreigner_package_healthcare_service') ?></label>
                                            <div class="col-sm-9"> <input type="text" sh-cl="price_foreigner" class="form-control parsley-validated" placeholder="required">  </div>
                                        </div>

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status') ?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox"  sh-cl="stat" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order') ?></label>
                                            <div class="col-sm-9"> <input type="number" sh-cl="orders" data-notblank="true" class="form-control parsley-validated" placeholder="" value="0"> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <button id="AddaddRow" style="display:none">Add row</button>
                                    <!--h4>Thông Tin Khám Chữa Bệnh (Dịch Vụ)</h4>
                                        <div class="table-responsive">
                                            <div id="DataTables_Table_2" class="dataTables_wrapper no-footer">                                        
                                                    <table data-example="example_more_add" class="table table-striped m-b-none" style="width:100%"> 
                                                            <thead> <tr show-position="2"> 
                                                            <th style="display:none">ID</th> 
                                                            <th att-name="id"><input type="checkbox"/></th> 
                                                            <th att-name="code"><?= $lang_list->line('code_healthcare') ?></th> 
                                                            <th att-name="name"><?= $lang_list->line('name_healthcare') ?></th> 
                                                            <th att-name="healthcare_type_id"><?= $lang_list->line('type_healthcare') ?></th> 
                                                            <th att-name=""><?= $lang_list->line('group_healthcare') ?></th> 
                                                            <th att-name="unit"><?= $lang_list->line('unit_healthcare') ?></th> 
                                                            <th ><?= $lang_list->line('quantity_healthcare') ?></th> 
                                                            <th att-name="price_in_hour"><?= $lang_list->line('price_in_hour_healthcare') ?></th> 
                                                            <th att-name="price_overtime"><?= $lang_list->line('price_overtime_healthcare') ?></th> 
                                                            <th att-name="price_holiday"><?= $lang_list->line('price_holiday_healthcare') ?></th> 
                                                            <th att-name="price_foreigner"><?= $lang_list->line('price_foreigner_healthcare') ?></th> 
                                                            <th att-name="note"><?= $lang_list->line('note') ?></th> 
                                                            <th><?= $lang_list->line('status') ?></th> 
                                                            <th><?= $lang_list->line('order') ?></th> 
                                                            </tr> 
                                                            </thead>
                                                            <tbody> </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <th att-edit="false"></th>
                                                                    <th att-edit="false" data-trigge="change" data-attr="code">load</th>
                                                                    <th att-edit="false" data-trigge="change" data-attr="name">load</th>
                                                                    <th att-edit="false"></th>
                                                                    <th att-edit="false"></th>
                                                                    <th att-edit="false"></th>
                                                                    <th><input type="text" placeholder="<?= $lang_list->line('quantity_healthcare') ?>"></th>
                                                                    <th att-edit="false"></th>
                                                                    <th att-edit="false"></th>
                                                                    <th att-edit="false"></th>
                                                                    <th att-edit="false"></th>
                                                                    <th att-edit="false"></th>
                                                                    <th data-type-select="select-status"><input type="text" placeholder="<?= $lang_list->line('status') ?>"></th>
                                                                    <th><input type="text" placeholder="<?= $lang_list->line('order') ?>"></th>
                                                                </tr>
                                                            </tfoot>
                                                    </table>
                                                    <button type="button" class="xoa_select btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDeletehealthcare">Xóa</button>
                                            </div>          
                                    </div-->
                                    <!--</div>-->

                                    <!--</div>-->
                                    <?php echo $template_list_package_healthcare_service_sub_table; ?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close') ?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php echo $template_delete_push_unpush; ?>

                <!-- for start popup healthcare  -->
                <div class="" id="delete_healthcare" style="">
                    <div class="modal fade" id="myDeletehealthcare" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Xóa</h4>
                                </div>
                                <div class="modal-body">

                                    <form class="form-horizontal" data-validate="parsley">
                                        <div class="form-group"> 
                                            <!-- <label class="col-sm-12 control-label one_show text-center">Bạn có chắc chắn muốn xóa  Gói Khám Bệnh - Sanh: <span class="at_change"></span> đã chọn?</label> -->
                                            <!-- <label class="col-sm-12 control-label more_show text-center">Bạn có chắc chắn muốn xóa các Gói Khám Bệnh - Sanh đã được chọn?</label> -->
                                            <label class="col-sm-12 control-label one_show text-center"><?= $lang_list->line('event_delete_one') . ' ' . $lang_list->line('for_list_healthcare_service'), ': <span class="at_change"></span> ' . $lang_list->line('event_delete_one_change') ?></label>
                                            <label class="col-sm-12 control-label more_show text-center"><?= $lang_list->line('event_delete_more') . ' ' . $lang_list->line('for_list_healthcare_service'), ' ' . $lang_list->line('event_delete_more_change') ?></label>
                                            <div class="col-sm-12 hide"> 
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled>  
                                            </div>
                                        </div>
                                        <input id="name_table" name-table='' type="hidden">
                                    </form>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" id="no-headthcare-click" data-dismiss="modal">No</button>
                                    <button type="button" class="btn btn-primary"  id="yes-headthcare-click">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- for end popup healthcare  -->


            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>

<style>
    .modal-dialog{
        /*width:90%;*/
        /*max-width: 800px;*/
    }
    #DataTables_Table_2 tbody tr td:first-child{
        display: none!important;
    }
    #DataTables_Table_1 tbody tr td:first-child{
        display: none!important;
    }
</style>
