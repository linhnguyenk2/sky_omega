<div class="modal-over">
    <div class="modal-center animated fadeInUp text-center" style="width:200px;margin:-80px 0 0 -100px;">
        <div class="thumb-md"><img src="assets/images/avatar.jpg" class="img-circle b-a b-light b-3x"></div>
        <p class="text-white h4 m-t m-b"></p>
        <div class="input-group"> 
            <input id="old_password" type="password" class="form-control text-sm" placeholder="Enter pwd to continue" data-toggle="tooltip" title="">
            <input id="old_email" type="hidden" class="form-control text-sm" placeholder="" value="">
            <span class="input-group-btn"> <button class="btn btn-success" type="button" id="relogin"><i class="fa fa-arrow-right"></i></button> </span> </div>
    </div>
</div>