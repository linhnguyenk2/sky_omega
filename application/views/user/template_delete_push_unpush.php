<div class="" id="delete" style="">
    <div class="modal fade" id="myDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete</h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal" data-validate="parsley">
                        <div class="form-group"> <label class="col-sm-3 control-label">Delete ID</label>
                            <div class="col-sm-9"> 
                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled>  
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="" id="push" style="">
    <div class="modal fade" id="myPush" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Push</h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal" data-validate="parsley">
                        <div class="form-group"> <label class="col-sm-3 control-label">Push ID</label>
                            <div class="col-sm-9"> 
                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled>  
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Push</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="" id="unpush" style="">
    <div class="modal fade" id="myUnpush" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">UnPush</h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal" data-validate="parsley">
                        <div class="form-group"> <label class="col-sm-3 control-label">UnPush ID</label>
                            <div class="col-sm-9"> 
                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required" disabled>  
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">UnPush</button>
                </div>
            </div>
        </div>
    </div>
</div>