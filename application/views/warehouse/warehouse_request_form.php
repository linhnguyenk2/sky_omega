<?php
$lang_list = $list_lang;
?>

<section id="content">
	<section class="vbox si-content" id="category_name" data-cat="<?php echo $menu ?>">
		<header class="header bg-white b-b b-light">

			<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
				<li><a href="index"><i class="fa fa-home"></i> <?= $lang_list->line('home') ?></a></li>
				<li><a href="#"><i class="fa fa-th-list"></i> <?= $lang_list->line('warehouse_title') ?></a></li>
				<!-- Replace with name of warehouse -->
				<li><a href="#"><i class="fa fa-th-list"></i> Kho tong</a></li>
				<li class="active"><?= $lang_list->line('view_title') ?></li>
			</ul>
		</header>
		<section class="scrollable wrapper">
			<div class="m-b-md">
				<h3 class="m-b-none"><?= $lang_list->line('view_title') ?></h3>
			</div>

			<div class="wrapper-lg bg-white b-b b-light">
				<div class="tab-pane active" id="index">
					<div class="form-group">
						<label><?php echo $lang_list->line('form_created_by_name') ?></label>
						<select class="form-control">
							<option value="">-Chọn Phiếu -</option>
							<option value="1">A</option>
							<option value="2">B</option>
						</select>
					</div>

					<div class="form-group">
						<label><?php echo $lang_list->line('reason') ?></label>
						<textarea></textarea>
					</div>

					<div class="form-group">
						<label><?php echo $lang_list->line('chief_staff') ?></label>
						<select class="form-control">
							<option value="">-Chọn Phiếu -</option>
							<option value="1">A</option>
							<option value="2">B</option>
						</select>
					</div>

					<div id="" class="dataTables_wrapper no-footer">

						<div class="doc-buttons">
							<a href="#" class="btn btn-s-md btn-primary"><i
										class="fa fa-plus"></i> <?= $lang_list->line('button_save_temporary') ?></a>
							<a href="#" class="btn btn-s-md btn-danger"><i
										class="fa fa-check"></i> <?php echo $lang_list->line('button_publish') ?></a>
						</div>
					</div>

					<h3><?php echo $lang_list->line('search_products') ?></h3>
					<!-- Table -->
					<section class="panel panel-default">
						<div class="table-responsive">
							<div id="" class="dataTables_wrapper no-footer">

								<div class="doc-buttons">
									<a href="#" class="btn btn-s-md btn-primary" data-toggle="modal"
									   data-target="#myAdd"><i class="fa fa-plus"></i> <?= $lang_list->line('add') ?>
									</a>

									<a style="" href="#" class="btn btn-s-md btn-info disabled" data-toggle="modal"
									   data-target="#myEdit" id="btn_edit"> <?php echo $lang_list->line('edit') ?></a>

									<a style="display:none;" href="#" class="btn btn-s-md btn-primary disabled"
									   data-toggle="modal" data-target="#myPush" id="btn_publish"><i
												class="fa fa-check"></i> <?php echo $lang_list->line('publish') ?></a>

									<a style="display:none;" href="#" class="btn btn-s-md btn-primary disabled"
									   data-toggle="modal" data-target="#myUnpush"
									   id="btn_unpublish"><?php echo $lang_list->line('unpublish') ?></a>

									<a href="#" class="btn btn-s-md btn-danger disabled xoa-element" data-toggle="modal"
									   data-target="#myDelete"
									   id="btn_delete"> <?php echo $lang_list->line('delete') ?></a>

								</div>

								<div class="row">
									<div class="col-sm-6">
										<div class="dataTables_filter"><label>Search:<input type="search"
																							class=""></label></div>
									</div>
									<div class="col-sm-6">
										<div class="dataTables_length"><label>Show <select class="select-page-option">
													<option value="10">10</option>
													<option value="25">25</option>
													<option value="50">50</option>
													<option value="100">100</option>
												</select> entries</label></div>
									</div>
								</div>

								<table id="warehouse_list" data-link-api="api/v1/warehouse/warehouse"
									   data-param="type=1" data-example="example_more"
									   class="table table-striped m-b-none" style="width:100%">
									<thead>
									<tr show-position="2" attr-show-id="warehouse_list.id" attr-id-parent="id">
										<th att-name="id"><input type="checkbox"/></th>
										<th att-name="name"><?php echo $lang_list->line('product_code') ?></th>
										<th att-name="type"><?php echo $lang_list->line('product_name') ?></th>
										<th att-name="type"><?php echo $lang_list->line('product_type') ?></th>
										<th att-name="type"><?php echo $lang_list->line('product_group') ?></th>
										<th att-name="type"><?php echo $lang_list->line('product_request_number') ?></th>
										<th att-name="type"><?php echo $lang_list->line('product_accept_number') ?></th>
										<th att-name="type"><?php echo $lang_list->line('product_unit') ?></th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td><input type="checkbox"></td>
										<td>SP001</td>
										<td>Thuoc ABV</td>
										<td>ABC</td>
										<td>A</td>
										<td>1</td>
										<td>1</td>
										<td>kg</td>
									</tr>

									<tr>
										<td><input type="checkbox"></td>
										<td>SP001</td>
										<td>Thuoc ABX</td>
										<td>ABC</td>
										<td>A</td>
										<td>1</td>
										<td>1</td>
										<td>viên</td>
									</tr>
									</tbody>
									<tfoot>
									<tr>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
									</tfoot>
								</table>
								<div class="row">
									<div class="col-sm-6">
										<div class="dataTables_info" role="status"
											 aria-live="polite">Showing page 1 of 1</div>
									</div>
									<div class="col-sm-6">
										<div class="dataTables_paginate paging_full_numbers"><a
													class="paginate_button first disabled" data-dt-idx="0"
													tabindex="0">First</a><a
													class="paginate_button previous disabled" data-dt-idx="1"
													tabindex="0">Previous</a><span><a class="paginate_button current"
																					  data-dt-idx="1" tabindex="0">1</a></span><a
													class="paginate_button next" data-dt-idx="7" tabindex="0">Next</a><a
													class="paginate_button last" data-dt-idx="8" tabindex="0">Last</a>
										</div>
									</div>
								</div>
							</div>

						</div>

					</section>
				</div>

			</div>

		</section>
	</section>
	<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>


	<!-- Modal popup add new -->
	<div class="" id="add" style="">
		<div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			 data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
									aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew') ?></h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-3 control-label"><?= $lang_list->line('product_name') ?></label>
								<div class="col-sm-9">
									<select class="select-type-head form-control"></select>
									<input type="text" sh-cl="" class="form-control input-have-select hide">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label"><?= $lang_list->line('product_request_number') ?></label>
								<div class="col-sm-9">
									<input type="text" sh-cl="name" class="form-control">
									<span><?= $lang_list->line('default_unit') ?></span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label"><?= $lang_list->line('product_accept_number') ?></label>
								<div class="col-sm-9">
									<input type="text" sh-cl="name" class="form-control">
								</div>
							</div>
						</form>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default"
								data-dismiss="modal"><?= $lang_list->line('close') ?></button>

						<button type="button" class="btn btn-primary"><?= $lang_list->line('save') ?></button>
					</div>
				</div>
			</div>
		</div>

	</div>

</section>


<style>

</style>