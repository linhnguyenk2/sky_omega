<?php

$lang_list = $list_lang;
$menu=$this->router->fetch_method();



?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?= $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> <?= $lang_list->line('list')?></a></li>
                <!-- <li class="active"><?= $title;?></li> -->
                <li class="active">Danh sách thuốc</li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <!-- <h3 class="m-b-none"><?= $title;?></h3> -->
                <h3 class="m-b-none">Danh sách thuốc</h3>
            </div>


            <div class="doc-buttons"> 

                <a style="display:none;" href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#myAdd" id="btn_add"><i class="fa fa-plus"></i> <?= $lang_list->line('add') ?></a>

                <a style="display:none;" href="#" class="btn btn-s-md btn-info disabled" data-toggle="modal" data-target="#myEdit" id="btn_edit"><?= $lang_list->line('edit') ?></a> 

                <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myPush" id="btn_publish"><i class="fa fa-check"></i> <?= $lang_list->line('publish') ?></a>

                <a href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myUnpush" id="btn_unpublish"><?= $lang_list->line('unpublish') ?></a>

                <a href="#" class="btn btn-s-md btn-danger disabled" data-toggle="modal" data-target="#myDelete" id="btn_delete"><?= $lang_list->line('delete') ?></a>

            </div>

            <!-- Modal -->


            <!--            <ul class="nav nav-tabs">
                            <li class="active m-l-lg"><a href="#index" data-toggle="tab">Index</a></li>
                            <li><a href="#edit" data-toggle="tab">Edit</a></li>
                            <li><a href="#add" data-toggle="tab">Add</a></li>
                        </ul>-->
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">


                    <section class="panel panel-default">
                        <div class="table-responsive">
                        <div id="DataTables_Table_0" class="dataTables_wrapper no-footer">
                                <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>
                                    <table data-link-api="api/v1/product/product" data-para="sih_product_group:type=1" data-link-api-save="api/v1/product/medicine_product" data-example="example_more" class="table table-striped m-b-none" style="width:100%">
                                    <thead>
                                        <tr show-position="2" attr-show-id="medicine_id.id">
                                            
                                            <th att-name="id" class="sorting_disabled"></th>
                                            <th att-name="code" att-save="code" data-sort="asc" class="sortat sorting_asc"><?= $lang_list->line('code_medicine'); ?></th>
                                            <th att-name="name" att-save="name" att-requirement="true"><?= $lang_list->line('name_medicine'); ?></th>
                                            <th att-name="medicine_id.chemical_compound" att-save="chemical_compound"><?= $lang_list->line('compound_name_medicine'); ?></th>
                                            <th att-name="medicine_id.way_in" att-save="way_in"><?= $lang_list->line('way_in_medicine'); ?></th>
                                            <!--<th att-name="id"><?= $lang_list->line('specification_medicine'); ?></th>-->
                                            <th att-name="unit_id.name" att-save="unit_id"><?= $lang_list->line('unit_medicine'); ?></th>
                                            <th att-name="price" att-save="price"><?= $lang_list->line('unit_price_medicine'); ?></th>
                                            <th att-name="description" att-save="description" style="width: 360px;"><?= $lang_list->line('explain_medicine'); ?></th>
                                            <th att-name="vat" att-save="vat" att-name="vat"><?= $lang_list->line('vat')?></th> 
                                            <th att-name="note" att-save="note"><?= $lang_list->line('note_medicine'); ?></th>
                                            <th class="sorting_disabled"><?= $lang_list->line('point_medicine'); ?></th>
                                            <th class="sorting_disabled"><?= $lang_list->line('contraindications_medicine'); ?></th>
                                            <th class="sorting_disabled"><?= $lang_list->line('the_drug_is_not_combined_medicine'); ?></th>
                                            <th class="sorting_disabled"><?= $lang_list->line('subordinate_units_medicine'); ?></th>
                                            <th att-name="stat" att-save="stat" select-static="stat" att-name="stat"><?= $lang_list->line('status'); ?></th>
                                            <th att-name="orders" att-save="orders"><?= $lang_list->line('order'); ?></th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot><tr>
                                            <th></th> 
                                            <th></th> 
                                            <th><input type="text"></th>
                                            <th><input type="text"></th>
                                            <th><input type="text"></th>
                                            <th data-type-select="api/v1/list/units"></th>
                                            <th><input type="text"></th>
                                            <th><input type="text"></th>
                                            <th><input type="text"></th>
                                            <th><input type="text"></th>
                                            <th modelshow="myShow01" att-edit="false"></th>
                                            <th modelshow="myShow02" att-edit="false"></th>
                                            <th modelshow="myShow03" att-edit="false"></th>
                                            <th modelshow="myShow04" att-edit="false"></th>
                                            <th data-type-select="select-status"></th>
                                            <th><input type="text"></th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="dataTables_info" role="status" aria-live="polite">Showing 1 to 10 of 46 entries</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_full_numbers" ><a class="paginate_button first disabled"  data-dt-idx="0" tabindex="0" >First</a><a class="paginate_button previous disabled" data-dt-idx="1" tabindex="0">Previous</a><span><a class="paginate_button current" data-dt-idx="2" tabindex="0">1</a><a class="paginate_button "  data-dt-idx="3" tabindex="0">2</a><a class="paginate_button " data-dt-idx="4" tabindex="0">3</a><a class="paginate_button " data-dt-idx="5" tabindex="0">4</a><a class="paginate_button " data-dt-idx="6" tabindex="0">5</a></span><a class="paginate_button next" data-dt-idx="7" tabindex="0">Next</a><a class="paginate_button last" data-dt-idx="8" tabindex="0">Last</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>
                </div>
                <div class="" id="edit" style="">

                    <div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('edit_post')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('edit_post')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation')?></label>
                                            <div class="col-sm-9"> 
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="hidden" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                                <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order')?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save_change')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="add"  style="">
                    <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?= $lang_list->line('addnew')?></h4>
                                </div>
                                <div class="modal-body">
                                    <!--<div class="row">-->
                                    <!--<div class="col-sm-7 col-sm-offset-2">-->
                                    <form class="form-horizontal" data-validate="parsley">
                                        <!--<section class="panel panel-default">-->
                                            <!--<header class="panel-heading"> <strong><?= $lang_list->line('addnew')?></strong> </header>-->
                                        <!--<div class="panel-body">-->
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('code_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('name_nation')?></label>
                                            <div class="col-sm-9"> <input type="text" class="form-control parsley-validated" data-required="true" placeholder="required">  </div>
                                        </div>
                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('date_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="input-sm input-s datepicker-input form-control" data-date-format="yyyy-mm-dd" data-required="true" placeholder="required"> </div>
                                        </div> -->

                                        <!-- <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('people_create')?></label>
                                            <div class="col-sm-9"> <input type="text" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div> -->

                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('status')?></label>
                                            <div class="col-sm-9"> <label class="switch"> <input type="checkbox" checked=""> <span></span> </label> </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-3 control-label"><?= $lang_list->line('order')?></label>
                                            <div class="col-sm-9"> <input type="number" data-notblank="true" class="form-control parsley-validated" placeholder=""> </div>
                                        </div>

                                        <!--</div>-->
                                        <!--<footer class="panel-footer text-right bg-light lter"> <button type="submit" class="btn btn-success btn-s-xs">Submit</button> </footer>-->
                                        <!--</section>-->
                                    </form>
                                    <!--</div>-->

                                    <!--</div>-->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                    <button type="button" class="btn btn-primary"><?= $lang_list->line('save')?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="" id="show01" style="">
                    <div class="modal fade" id="myShow01" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Chỉ định</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal" data-link-api-main="api/v1/list/appointed_in_medicine" data-at-id="" data-validate="parsley">
                                        <div class="show-body">
                                            

                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="show-footer">
                                            <div class="form-group row s-form-chidinh at_id">
                                                <div class="col-sm-1"><label for="">Từ</label></div>
                                                <div class="col-sm-1">
                                                    <select class="form-control at_from_age" data-col="from_age" type-select-tem="age" id="">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-1"><label for="">tuổi</label></div>
                                                <div class="col-sm-1"><label for="">Đến</label></div>
                                                <div class="col-sm-1">
                                                    <select class="form-control at_to_age" data-col="to_age" type-select-tem="age" id="">
                                                    <option></option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-1"><label for="">tuổi</label></div>
                                                <div class="col-sm-1 s-padding-0"><label for="">Chỉ định</label></div>
                                                <div class="col-sm-2"><input type="text" class="form-control at_dose" data-col="dose"/></div>
                                                <div class="col-sm-1"><label for="">Viên</label></div>
                                                <div class="col-sm-1"><button class="btn btn-primary">Thêm</button></div>
                                            </div>

                                        </div>
                                        
                                       
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button> -->
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="show02" style="">
                    <div class="modal fade" id="myShow02" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Chống chỉ định</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal" data-link-api-main="api/v1/list/contraindication_in_medicine" data-link-api-sub="api/v1/list/contraindication" data-at-id="" data-validate="parsley">
                                        
                                        <div class="show-body">
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="show-footer">
                                            <div class="form-group row s-form-chongchidinh at_id">
                                                <div class="col-sm-10">
                                                    <select class="form-control at_contraindication_id" data-col="contraindication_id" type-select-tem="contraindication" id="">
                                                    <option>Chọn chống chỉ định</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2"><button class="btn btn-primary">Thêm</button></div>
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button> -->
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="show03" style="">
                    <div class="modal fade" id="myShow03" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel1">Thuốc không dùng chung</h4>
                                </div>
                                <div class="modal-body">
                                    <!-- <div class="form-horizontal" data-link-api-main="api/v1/list/not_combine_medicine" data-link-api-sub="api/v1/list/medicine" data-at-id="" data-at-name="origin_medicine_id" data-validate="parsley"> -->
                                    <div class="form-horizontal" data-link-api-main="http://192.168.1.215/projectsky/api/v1/product/medicine_product" data-link-api-save="api/v1/list/not_combine_medicine" data-link-api-sub="api/v1/product/product?sih_product_group:type=1" data-at-id="" data-at-name="origin_medicine_id" data-validate="parsley">
                                        <!-- danh sach thuoc hong dung chung cua 1 thuoc  -->
                                        <!-- http://192.168.1.215/projectsky/api/v1/product/medicine_product/?medicine_id=9 -->
                                        <div class="show-body">
                                            
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="show-footer">
                                            <div class="form-group row s-form-thockhongkethop at_id">
                                                <div class="col-sm-10">
                                                    <select class="form-control at_not_combine_medicine_id" data-col="not_combine_medicine_id" type-select-tem="product?sih_product_group:type=1"  type-attr-show="medicine_id.id" type-attr-load="medicine_id.list_not_combine_medicine">
                                                        <option>Chọn chống chỉ định</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2"><button class="btn btn-primary">Thêm</button></div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button> -->
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" id="show04" style="">
                    <div class="modal fade" id="myShow04" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Đơn vị phụ</h4>
                                </div>
                                <div class="modal-body">
                                    <!-- <div class="form-horizontal" data-link-api-main="api/v1/list/unit_medicine" data-link-api-sub="api/v1/list/units" data-at-id="" data-validate="parsley"> -->
                                    <div class="form-horizontal" data-link-api-main="api/v1/product/product_unit_exchange" data-link-api-sub="api/v1/list/units" data-at-id="" data-validate="parsley">
                                        
                                        <div class="show-body">
                                            
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="show-footer">
                                            
                                            <div class="form-group row s-form-donviphu at_id">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-1"><label for="">Một</label></div>
                                                <div class="col-sm-2">
                                                <!-- type-attr-show="medicine_id.id" type-attr-load="not_combine_medicine_id.id" -->
                                                    <select class="form-control at_unit_exchange_id" data-col="unit_exchange_id" type-select-tem="units" type-attr-show="id" type-attr-load="unit_exchange_id.id">
                                                        <option>Chọn chống chỉ định</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-1"><label for="">Gồm</label></div>
                                                <div class="col-sm-2"><input type="text" class="form-control at_exchange_quality" data-col="exchange_quality" ></div>
                                                <div class="col-sm-1"><span class="at_unit_id" data-col="unit_id"></span><input type="hidden" class="form-control parent_unit_medicine_id" data-col="parent_unit_medicine_id" value="1"></div>
                                                <div class="col-sm-2"><button class="btn btn-primary">Thêm</button></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?= $lang_list->line('close')?></button> -->
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= $lang_list->line('close')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php //echo $template_delete_push_unpush; ?>
                <?php $this->load->view('list/template_delete_push_unpush',['lang_list'=>$lang_list]);?>

            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>

<style>
.dataTables_length {
    display: inline-block;
}
.s-form-chidinh .col-sm-1{
    /* padding:2px; */
}
.s-form-chidinh button,
.s-form-chongchidinh button,
.s-form-donviphu button
{
    min-width:63px;
}
.s-form-chidinh select{
    width: 56px;
    padding-left: 5px;
    margin-left: -17px;
    margin-top: -4px;
}
.s-form-chidinh input{
    width:100%;
}
.s-padding-0{
    padding:0;
}
</style>