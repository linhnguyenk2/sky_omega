<?php
$lang_list = $list_lang;
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?php echo $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> <?= $lang_list->line('home') ?></a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> <?= $lang_list->line('warehouse_title') ?></a></li>
                <li class="active"><?= $lang_list->line('view_title') ?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">         
            <div class="m-b-md">
                <h3 class="m-b-none"><?= $lang_list->line('view_title') ?></h3>
            </div>

            <ul class="nav nav-tabs">
                <li><a href="<?= site_url('warehouse_product_in_package_list') ?>"><?= $lang_list->line('inventories') ?></a></li>
                <li><a href="<?= site_url('warehouse_export_form_list') ?>"><?= $lang_list->line('warehouse_export') ?></a></li>
                <li class="active"><a href="#warehouse_import"><?= $lang_list->line('warehouse_import') ?></a></li>
                <li><a href="<?= site_url('warehouse_sale_form_list') ?>"><?= $lang_list->line('warehouse_sale') ?></a></li>
                <li><a href="<?= site_url('warehouse_use_form_list') ?>"><?= $lang_list->line('warehouse_use') ?></a></li>
                <li><a href="<?= site_url('warehouse_check_form_list') ?>"><?= $lang_list->line('warehouse_check') ?></a></li>
            </ul>

            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">

                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <div id="" class="dataTables_wrapper no-footer">

                                <div class="doc-buttons">
                                    <a  href="#" class="btn btn-s-md btn-primary"  data-toggle="modal" data-target="#myAdd" ><i class="fa fa-plus"></i> <?= $lang_list->line('add') ?></a>

                                    <a style="" href="#" class="btn btn-s-md btn-info disabled" data-toggle="modal" data-target="#myEdit" id="btn_edit"> <?php echo $lang_list->line('edit') ?></a>

                                    <a style="display:none;" href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myPush" id="btn_publish"><i class="fa fa-check"></i> <?php echo $lang_list->line('publish') ?></a>

                                    <a style="display:none;" href="#" class="btn btn-s-md btn-primary disabled" data-toggle="modal" data-target="#myUnpush" id="btn_unpublish"><?php echo $lang_list->line('unpublish') ?></a>

                                    <a href="#" class="btn btn-s-md btn-danger disabled xoa-element" data-toggle="modal" data-target="#myDelete" id="btn_delete"> <?php echo $lang_list->line('delete') ?></a>

                                </div>

                                <div class="row"><div class="col-sm-6"><div class="dataTables_filter"><label>Search:<input type="search" class=""></label></div></div><div class="col-sm-6"><div class="dataTables_length"><label>Show <select class="select-page-option"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div></div>

                                <table id="warehouse_list" data-link-api="api/v1/warehouse/warehouse" data-param="type=1" data-example="example_more" class="table table-striped m-b-none" style="width:100%">
                                    <thead>

                                        <tr show-position="2" attr-show-id="warehouse_list.id" attr-id-parent="id">
                                            <th att-name="id"><input type="checkbox"/></th> 
                                            <th att-name="name"><?php echo $lang_list->line('form_code') ?></th>
                                            <th att-name="type"><?php echo $lang_list->line('form_created_by_name') ?></th>
                                            <th att-name="type"><?php echo $lang_list->line('form_creation_date') ?></th>
                                            <th att-name="type"><?php echo $lang_list->line('form_content') ?></th>
                                        </tr> 
                                    </thead>
                                    <tbody> 
                                        <tr>
                                            <td><input type="checkbox"></td>
											<td><a href="<?php echo site_url('warehouse_import_form') . '/' . 1?>">XK001</a></td>
                                            <td>Nguyen A</td>
                                            <td>10/12/2012</td>
                                            <td>Ban thuoc A</td>
                                        </tr>

                                        <tr>
                                            <td><input type="checkbox"></td>
											<td><a href="<?php echo site_url('warehouse_import_form') . '/' . 1?>">XK002</a></td>
                                            <td>Nguyen A</td>
                                            <td>10/12/2012</td>
                                            <td>Ban thuoc A</td>
                                        </tr>
                                    </tbody> 
                                    <tfoot>
                                        <tr>
                                            <th></th> 
                                            <th></th> 
                                            <th></th> 
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="dataTables_info" role="status" aria-live="polite">Showing page 1 of 1</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_full_numbers"><a class="paginate_button first disabled" data-dt-idx="0" tabindex="0">First</a><a class="paginate_button previous disabled" data-dt-idx="1" tabindex="0">Previous</a><span><a class="paginate_button current" data-dt-idx="1" tabindex="0">1</a></span><a class="paginate_button next" data-dt-idx="7" tabindex="0">Next</a><a class="paginate_button last" data-dt-idx="8" tabindex="0">Last</a></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </section>
                </div>

            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a> 
</section>

<style>

</style>