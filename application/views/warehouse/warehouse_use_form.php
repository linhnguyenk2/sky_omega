<?php
$lang_list = $list_lang;
?>

<section id="content">
    <section class="vbox si-content" id="category_name" data-cat="<?php echo $menu ?>">
        <header class="header bg-white b-b b-light">

            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index"><i class="fa fa-home"></i> <?= $lang_list->line('home') ?></a></li>
                <li><a href="#"><i class="fa fa-th-list"></i> <?= $lang_list->line('warehouse_title') ?></a></li>
                <!-- Replace with name of warehouse -->
                <li><a href="#"><i class="fa fa-th-list"></i> Kho tong</a></li>
                <li class="active"><?= $lang_list->line('view_title') ?></li>
            </ul>
        </header>
        <section class="scrollable wrapper">
            <div class="m-b-md">
                <h3 class="m-b-none"><?= $lang_list->line('view_title') ?></h3>
            </div>

            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-pane active" id="index">
                    <div class="form-group">
                        <label><?php echo $lang_list->line('form_created_by_name') ?></label>
                        <p>Nhan vien A</p>
                    </div>

                    <div class="form-group">
                        <label><?php echo $lang_list->line('form_creation_date') ?></label>
                        <p>11:00 12/12/2012</p>
                    </div>

                    <div class="form-group">
                        <label><?php echo $lang_list->line('form_used_service_name') ?></label>
                        <p>Khám bệnh</p>
                    </div>

                    <div id="" class="dataTables_wrapper no-footer">

                        <div class="doc-buttons">
                            <a href="#" class="btn btn-s-md btn-danger"><?= $lang_list->line('button_exit') ?></a>
                        </div>
                    </div>

                    <h4><?= $lang_list->line('sale_products') ?></h4>
                    <!-- Table -->
                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <div id="" class="dataTables_wrapper no-footer">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="dataTables_filter"><label>Search:<input type="search"
                                                                                            class=""></label></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_length"><label>Show <select class="select-page-option">
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select> entries</label></div>
                                    </div>
                                </div>

                                <table id="warehouse_list" data-link-api="api/v1/warehouse/warehouse"
                                       data-param="type=1" data-example="example_more"
                                       class="table table-striped m-b-none" style="width:100%">
                                    <thead>
                                    <tr show-position="2" attr-show-id="warehouse_list.id" attr-id-parent="id">
                                        <th att-name="id"><input type="checkbox"/></th>
                                        <th att-name="name"><?php echo $lang_list->line('product_code') ?></th>
                                        <th att-name="type"><?php echo $lang_list->line('product_name') ?></th>
                                        <th att-name="type"><?php echo $lang_list->line('product_type') ?></th>
                                        <th att-name="type"><?php echo $lang_list->line('product_group') ?></th>
                                        <th att-name="type"><?php echo $lang_list->line('product_number') ?></th>
                                        <th att-name="type"><?php echo $lang_list->line('product_unit') ?></th>
                                        <th att-name="type"><?php echo $lang_list->line('product_expired_date') ?></th>
                                        <th att-name="type"><?php echo $lang_list->line('product_package_code') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><input type="checkbox"></td>
                                        <td>SP001</td>
                                        <td>Thuoc ABV</td>
                                        <td>ABC</td>
                                        <td>A</td>
                                        <td>1</td>
                                        <td>Lit</td>
                                        <td>11/12/2020</td>
                                        <td>132</td>
                                    </tr>

                                    <tr>
                                        <td><input type="checkbox"></td>
                                        <td>SP001</td>
                                        <td>Thuoc ABX</td>
                                        <td>ABC</td>
                                        <td>A</td>
                                        <td>1</td>
                                        <td>Vien</td>
                                        <td>11/12/2020</td>
                                        <td>1</td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="dataTables_info" role="status"
                                             aria-live="polite">Showing page 1 of 1</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_full_numbers"><a
                                                    class="paginate_button first disabled" data-dt-idx="0" tabindex="0">First</a><a
                                                    class="paginate_button previous disabled" data-dt-idx="1"
                                                    tabindex="0">Previous</a><span><a class="paginate_button current"
                                                                                      data-dt-idx="1" tabindex="0">1</a></span><a
                                                    class="paginate_button next" data-dt-idx="7" tabindex="0">Next</a><a
                                                    class="paginate_button last" data-dt-idx="8" tabindex="0">Last</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </section>
                </div>

            </div>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
</section>

<style>

</style>