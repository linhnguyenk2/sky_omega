
<!-- aside -->
<aside class="bg-dark lter b-r aside-md hidden-print hidden-xs" id="nav">
    <section class="vbox">
        <section class="w-f scrollable">
            <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0"
                 data-size="5px" data-color="#333333" style="overflow: hidden; width: auto; height: auto;">
                <!-- nav -->
                <nav class="nav-primary hidden-xs">
                    <ul class="nav" style="display:block;">
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Dashboard')?>">
                            <a href="index"> <i class="fa fa-th icon"> <b class="bg-info"></b> </i> <span>Dashboard</span> </a>
                        </li>
                        <!-- Quản lý kho -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Quản lý kho')?>">
                            <a href="#pages" class="">
                                <i class="fa fa-cog icon"></i>
                                <b class="bg-primary"></b>
                                <span class="pull-right">
                                    <i class="fa fa-angle-down text"></i><i
                                            class="fa fa-angle-up text-active"></i>
                                </span>
                                <span>Quản lý kho</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách kho')?>">
                                    <a href="<?php echo site_url('warehouse_list') ?>">
                                        <i class="fa fa-angle-right"></i>
                                        <span>Danh sách kho</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách mỹ phẩm')?>">
                                    <a href="<?php echo site_url('cosmetic_list') ?>">
                                        <i class="fa fa-angle-right"></i>
                                        <span>Danh sách mỹ phẩm</span>
                                    </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách phiếu yêu cầu xuất')?>">
                                    <a href="<?php echo site_url('warehouse_request_form_list') ?>">
                                        <i class="fa fa-angle-right"></i>
                                        <span>Danh sách phiếu yêu cầu xuất</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách thực phẩm chức năng')?>">
                                    <a href="<?php echo site_url('list_functional_foods') ?>">
                                        <i class="fa fa-angle-right"></i>
                                        <span>Danh sách thực phẩm chức năng</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách thuốc')?>">
                                    <a href="<?php echo site_url('/medicine_product') ?>">
                                        <i class="fa fa-angle-right"></i>
                                        <span>Danh sách thuốc</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách vật tư y tế')?>">
                                    <a href="<?php echo site_url('/medical_supply') ?>">
                                        <i class="fa fa-angle-right"></i>
                                        <span>Danh sách vật tư y tế</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Hệ Thống -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Hệ Thống')?>">
                            <a href="#pages" class="">
                                <i class="fa fa-cog icon"></i>
                                <b class="bg-primary"></b>
                                <span class="pull-right">
                                    <i class="fa fa-angle-down text"></i><i
                                            class="fa fa-angle-up text-active"></i>
                                </span>
                                <span>Hệ Thống</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Bảng điều khiển chính')?>">
                                    <a href="#">
                                        <i class="fa fa-angle-right"></i>
                                        <span>Bảng điều khiển chính</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Thông tin tài khoản')?>">
                                    <a href="profile">
                                        <i class="fa fa-angle-right"></i>
                                        <span>Thông tin tài khoản</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Cài đặt chung')?>">
                                    <a href="#">
                                        <i class="fa fa-angle-right"></i>
                                        <span>Cài đặt chung</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- Tiếp Nhận -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Tiếp Nhận')?>">
                            <a href="#" class="">
                                <i class="icon-tn icon"></i>
                                <b class="bg-primary"></b>
                                <span class="pull-right">
                                    <i class="fa fa-angle-down text"></i>
                                    <i class="fa fa-angle-up text-active"></i>
                                </span>
                                <span>Tiếp Nhận</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Sách Bệnh Nhân')?>"><a href="<?php echo site_url('list_patient') ?>">
                                        <i class="fa fa-angle-right"></i>
                                        <span>Danh Sách Bệnh Nhân</span>
                                    </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Bệnh Nhân - thêm mới')?>"><a href="<?php echo site_url('list_patient_add') ?>">
                                        <i class="fa fa-angle-right"></i>
                                        <span>Bệnh Nhân - thêm mới</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Sách Đặt Hẹn')?>"><a href="<?php echo site_url('list_set_time') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Sách Đặt Hẹn</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Sách Phiếu Khám')?>">
                                    <a href="<?php echo site_url('list_examination') ?>">
                                        <i class="fa fa-angle-right"></i><span>Danh Sách Phiếu Khám</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Bệnh Nhân -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Bệnh Nhân')?>">
                            <a href="#" class=""> <i class="icon-tn icon"> </i>
                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <b class="bg-primary"></b> <i class="fa fa-angle-up text-active"></i> </span>
                                <span>Bệnh Nhân</span> </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Bệnh Án Sản Khoa')?>"><a href="<?php echo site_url('maternity_hospital') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Bệnh Án Sản Khoa</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Thông Tin Bệnh Nhân (chi tiết)')?>"><a href="<?php echo site_url('patient_information') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Thông Tin Bệnh Nhân (chi tiết)</span>
                                    </a>
                                </li>
                                 <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách đăng kí dịch vụ lễ tân')?>"><a href="<?php echo site_url('list_register_services') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh sách đăng kí dịch vụ lễ tân</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Ngoại Trú -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Ngoại Trú')?>">
                            <a href="#pages" class=""> <i class="icon-nt icon"> </i> <b class="bg-primary"></b>
                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span>
                                <span>Ngoại Trú</span> </a>
                            <ul class="nav lt">

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Sách Sổ Khám Phụ Khoa')?>"><a href="<?php echo site_url('outpatient_book_list') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Sách Sổ Khám Phụ Khoa</span> </a>
                                </li>
                                <!--li id="<?php echo $this->apppermissionhelper->getMenuId('Sổ Khám Phụ Khoa - Chi Tiết')?>"><a href="<?php echo site_url('outpatient_book_detail') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Sổ Khám Phụ Khoa - Chi Tiết</span> </a>
                                </li-->
                                <!--li id="<?php echo $this->apppermissionhelper->getMenuId('Thăm Khám Phụ Khoa - Chi Tiết')?>"><a href="<?php echo site_url('gynecological_examination') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Thăm Khám Phụ Khoa - Chi Tiết</span>
                                    </a>
                                </li-->
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Sách Sổ Khám Nhủ')?>"><a href="<?php echo site_url('book_of_examinations_list') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Sách Sổ Khám Nhủ</span> </a>
                                </li>
                                <!--li id="<?php echo $this->apppermissionhelper->getMenuId('Sổ Khám Nhủ - Chi Tiết')?>"><a href="<?php echo site_url('book_of_examinations') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Sổ Khám Nhủ - Chi Tiết</span> </a>
                                </li-->
                                <!--li id="<?php echo $this->apppermissionhelper->getMenuId('Sổ Khám Nhủ - Chi Tiết')?>"><a href="<?php echo site_url('visiting_breast') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Thăm Khám Nhủ - Chi Tiết</span> </a>
                                </li-->
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Sách Sổ Khám Thai')?>"><a href="<?php echo site_url('pregnancy_checklist_list') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Sách Sổ Khám Thai</span> </a>
                                </li>
                                <!--li id="<?php echo $this->apppermissionhelper->getMenuId('Sổ Khám Thai - Chi Tiết')?>"><a href="<?php echo site_url('pregnancy_checklist') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Sổ Khám Thai - Chi Tiết</span> </a>
                                </li-->
                                <!--li id="<?php echo $this->apppermissionhelper->getMenuId('Thăm Khám Thai - Chi Tiết')?>"><a href="<?php echo site_url('pregnancy_examination') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Thăm Khám Thai - Chi Tiết</span> </a>
                                </li-->
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Sách Sổ Mãn Kinh')?>"><a href="<?php echo site_url('the_manchuria_book_list') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Sách Sổ Mãn Kinh</span> </a>
                                </li>
								<li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Sách Sổ KHHGD')?>"><a href="<?php echo site_url('family_planing_book_list') ?>"> <i
												class="fa fa-angle-right"></i>
									<span>Danh Sách Sổ KHHGD</span> </a>
                                </li>
                                <!--li id="<?php echo $this->apppermissionhelper->getMenuId('Sổ Mãn Kinh<')?>"><a href="<?php echo site_url('the_manchuria_book') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Sổ Mãn Kinh</span> </a>
                                </li-->
                                <!--li id="<?php echo $this->apppermissionhelper->getMenuId('Phần Tự Theo Dõi')?>"><a href="<?php echo site_url('self_tracking_sheet') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Phần Tự Theo Dõi</span>
                                    </a>
                                </li-->
                            </ul>
                        </li>

                        <!-- Chẩn đoán hình ảnh -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Chẩn đoán hình ảnh')?>">
                            <a href="#pages" class=""> <i class="icon-cdha icon">
                                </i> <b class="bg-primary"></b> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span> <span>Chẩn đoán hình ảnh</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách chẩn đoán hình ảnh')?>"><a href="#"> <i class="fa fa-angle-right"></i> <span>Danh sách chẩn đoán hình ảnh</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Xét nghiệm & CLS -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Xét nghiệm & CLS')?>">
                            <a href="#pages" class=""> <i class="fa fa-flask icon">
                                </i> <b class="bg-primary"></b> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span> <span>Xét nghiệm & CLS</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách phiếu chỉ định dịch vụ')?>"><a href="test_list_service_designation"> <i class="fa fa-angle-right"></i>
                                        <span>Danh sách phiếu chỉ định dịch vụ</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách phiếu xét nghiệm')?>"><a href="test_list_form_check"> <i class="fa fa-angle-right"></i> <span>Danh sách phiếu xét nghiệm</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách phiếu cận lâm sàng')?>"><a href="test_list_subclinical"> <i class="fa fa-angle-right"></i> <span>Danh sách phiếu cận lâm sàng</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Phẫu thuật & thủ thuật -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Phẫu thuật & thủ thuật')?>">
                            <a href="#pages" class=""> <i class="icon-pttt icon">
                                </i> <b class="bg-primary"></b> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span> <span>Phẫu thuật & thủ thuật</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách phẫu thuật & thủ thuật')?>"><a href="#"> <i class="fa fa-angle-right"></i> <span>Danh sách phẫu thuật & thủ thuật</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Điều trị nội trú -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Điều trị nội trú')?>">
                            <a href="#pages" class=""> <i class="icon-dtnt icon">
                                </i> <b class="bg-primary"></b> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span> <span>Điều trị nội trú</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách điều trị nội trú')?>"><a href="#"> <i class="fa fa-angle-right"></i> <span>Danh sách điều trị nội trú</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Cấp phát thuốc -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Cấp phát thuốc')?>">
                            <a href="#pages" class=""> <i class="icon-cpt icon">
                                </i> <b class="bg-primary"></b> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span>
                                <span>Cấp phát thuốc</span> </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách cấp phát thuốc')?>"><a href="#"> <i class="fa fa-angle-right"></i> <span>Danh sách cấp phát thuốc</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Viện phí -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Viện phí')?>">
                            <a href="#pages" class=""> <i class="icon-vp icon"> </i> <b class="bg-primary"></b>
                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span>
                                <span>Viện phí</span> </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách viện phí')?>"><a href="#"> <i class="fa fa-angle-right"></i>
                                        <span>Danh sách viện phí</span> </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Quản lý dược -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Quản lý dược')?>">
                            <a href="#pages" class=""> <i class="fa fa-medkit icon">
                                </i> <b class="bg-primary"></b> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span>
                                <span>Quản lý dược</span> </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách quản lý dược')?>"><a href="#"> <i class="fa fa-angle-right"></i>
                                        <span>Danh sách quản lý dược</span> </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Quản lý mua hàng -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Quản lý mua hàng')?>">
                            <a href="#pages" class=""> <i class="icon-qlmh icon">
                                </i> <b class="bg-primary"></b> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span> <span>Quản lý mua hàng</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách quản lý mua hàng')?>"><a href="#"> <i class="fa fa-angle-right"></i> <span>Danh sách quản lý mua hàng</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Quản lý trang thiết bị -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Quản lý trang thiết bị')?>">
                            <a href="#pages" class=""> <i class="icon-qlttb icon">
                                </i> <b class="bg-primary"></b> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span> <span>Quản lý trang thiết bị</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách quản lý trang thiết bị')?>"><a href="#"> <i class="fa fa-angle-right"></i> <span>Danh sách quản lý trang thiết bị</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Quản lý nhân sự -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Quản lý nhân sự')?>">
                            <a href="#" class=""> <i class="fa fa-users icon"> </i> <b class="bg-primary"></b>
                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span> <span>Quản lý nhân sự</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách nhân sự')?>"><a href="manage_user"> <i class="fa fa-angle-right"></i> <span>Danh sách nhân sự</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Quản lý phân quyền -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Quản lý phân quyền')?>">
                            <a href="#" class=""> <i class="fa fa-users icon"> </i> <b class="bg-primary"></b>
                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span> <span>Quản lý phân quyền</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Quản lý role')?>"><a href="permission_role"> <i class="fa fa-angle-right"></i> <span>Quản lý role</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Quản lý màn hình')?>"><a href="permission_screen"> <i class="fa fa-angle-right"></i> <span>Quản lý màn hình</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Quản lý role và màn hình')?>"><a href="permission_role_screen"> <i class="fa fa-angle-right"></i> <span>Quản lý role và màn hình</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách menu bị khóa người dùng')?>"><a href="<?=base_url()?>delete_user"> <i class="fa fa-angle-right"></i> <span>Danh sách menu bị khóa người dùng</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Quản lý căn tin -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Quản lý căn tin')?>">
                            <a href="#pages" class=""> <i class="icon-qlct icon">
                                </i> <b class="bg-primary"></b> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span> <span>Quản lý căn tin</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách quản lý căn tin')?>"><a href="#"> <i class="fa fa-angle-right"></i> <span>Danh sách quản lý căn tin</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Báo cáo thống kê -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Báo cáo thống kê')?>">
                            <a href="#pages" class=""> <i class="fa fa-bar-chart-o icon"> <b
                                            class="bg-primary"></b> </i> <b class="bg-primary"></b> <span class="pull-right"> <i
                                            class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span> <span>Báo cáo thống kê</span>
                            </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh sách báo cáo thống kê')?>"><a href="#"> <i class="fa fa-angle-right"></i> <span>Danh sách báo cáo thống kê</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Danh Mục -->
                        <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục')?>">
                            <a href="#category" class="">
                                <i class="fa fa-th-list icon"> </i>
                                <b class="bg-primary"></b>
                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i> </span>
                                <span>Danh Mục</span> </a>
                            <ul class="nav lt">
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Quốc Gia')?>"><a href="<?php echo site_url('list_nations') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Quốc Gia</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('xxxxx')?>"><a href="<?php echo site_url('list_foreigner') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Ngoại Kiều</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Thành Phố')?>"><a href="<?php echo site_url('list_provinces') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Thành Phố</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Quận Huyệ')?>"><a href="<?php echo site_url('list_districts') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Quận Huyện</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Phường Xã')?>"><a href="<?php echo site_url('list_wards') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Phường Xã</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Nghề Nghiệp')?>"><a href="<?php echo site_url('list_titles') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Nghề Nghiệp</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục khoa/phòng')?>"><a href="<?php echo site_url('list_departments') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục khoa/phòng</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Lý Do Xuất Viện')?>"><a href="<?php echo site_url('list_reason_discharge') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Lý Do Xuất Viện</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Lý Do Chuyển Viện')?>"><a href="<?php echo site_url('list_reason_transfer') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Lý Do Chuyển Viện</span>
                                    </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Sách Vật Dụng')?>"><a href="<?php echo site_url('list_materials') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Vật Dụng</span>
                                    </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Sách Tỷ Giá')?>"><a href="<?php echo site_url('list_currencies') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Sách Tỷ Giá</span>
                                    </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Dân Tộc')?>"><a href="<?php echo site_url('list_folks') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Dân Tộc</span> </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Phòng Khám')?>"><a href="<?php echo site_url('list_clinic') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Phòng Khám</span> </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Nhóm Khám Chữa Bệnh')?>"><a href="<?php echo site_url('list_healthcare_group') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Nhóm Khám Chữa Bệnh</span>
                                    </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Loại Khám Chữa Bệnh')?>"><a href="<?php echo site_url('list_healthcare_type') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Loại Khám Chữa Bệnh</span>
                                    </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Khám Chữa Bệnh')?>"><a href="<?php echo site_url('list_healthcare') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Khám Chữa Bệnh</span> </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Nhóm Dịch Vụ')?>"><a href="<?php echo site_url('list_service_group') ?>"> <i
                                        class="fa fa-angle-right"></i>
                                        <span>Danh Mục Nhóm Dịch Vụ</span> </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Loại Dịch Vụ')?>"><a href="<?php echo site_url('list_service_type') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Loại Dịch Vụ</span> </a>
                                </li>
                                
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Dịch Vụ')?>"><a href="<?php echo site_url('list_service') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Dịch Vụ</span>
                                    </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Loại Gói Khám Bệnh - Sanh')?>"><a href="<?php echo site_url('list_package_healthcare_service_type') ?>">
                                        <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Loại Gói Khám Bệnh - Sanh</span>
                                    </a>
                                </li>

                                <li id="<?php echo $this->apppermissionhelper->getMenuId('xxxxx')?>"><a href="<?php echo site_url('list_package_healthcare_service') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Gói Khám Bệnh - Sanh</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Thuốc')?>"><a href="<?php echo site_url('list_medicine') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Thuốc</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Chống Chỉ Định')?>"><a href="<?php echo site_url('list_contraindication') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Chống Chỉ Định</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Công Ty Bảo Hiểm')?>"><a href="<?php echo site_url('list_insurrance_company') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Công Ty Bảo Hiểm</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Loại Thẻ Bảo Hiểm')?>"><a href="<?php echo site_url('list_health_insurrance_card') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Loại Thẻ Bảo Hiểm</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Nhà Cung Cấp')?>"><a href="<?php echo site_url('list_supllier') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Nhà Cung Cấp</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Loại Bệnh Án')?>"><a href="<?php echo site_url('list_medical_record') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Loại Bệnh Án</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Mối Quan Hệ Giữa Các Bệnh Nhân')?>"><a href="<?php echo site_url('list_relationship_between_patient') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Mối Quan Hệ Giữa Các Bệnh Nhân</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Nơi Chuyển Viện')?>"><a href="<?php echo site_url('list_place_transfer') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Nơi Chuyển Viện</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Phương Tiện Chuyển Viện')?>"><a href="<?php echo site_url('list_transport_vehicle') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Phương Tiện Chuyển Viện</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Nơi Giới Thiệu')?>"><a href="<?php echo site_url('list_place_introduction') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Nơi Giới Thiệu</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Phương Pháp Sanh')?>"><a href="<?php echo site_url('list_method_birth') ?>"> <i
                                                class="fa fa-angle-right"></i>
                                        <span>Danh Mục Phương Pháp Sanh</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Hệ Bệnh')?>"><a href="<?php echo site_url('list_chapter_icd10') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Hệ Bệnh</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Nhóm Bệnh')?>"><a href="<?php echo site_url('list_group_icd10') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Nhóm Bệnh</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Bệnh')?>"><a href="<?php echo site_url('list_disease_icd10') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Bệnh</span> </a>
                                </li>
                                
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Vật Tư Y Tế')?>"><a href="<?php echo site_url('list_medical_supplies') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Vật Tư Y Tế</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Loại Phòng')?>"><a href="<?php echo site_url('list_patient_room_type') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Loại Phòng</span>
                                    </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Phòng')?>"><a href="<?php echo site_url('list_patient_room') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Phòng</span> </a>
                                </li>
                                <li id="<?php echo $this->apppermissionhelper->getMenuId('Danh Mục Giường')?>"><a href="<?php echo site_url('list_patient_bed') ?>"> <i
                                                class="fa fa-angle-right"></i> <span>Danh Mục Giường</span> </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- / nav -->
            </div>
        </section>
    </section>
</aside>
<!-- /.aside -->

<script>
    var link_base ='<?= site_url()?>';
</script>