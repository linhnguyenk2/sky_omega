<?php
$userCurrent = $this->session->userdata('user');

$idiom_langguage = $this->session->userdata('language');
if(!$idiom_langguage){
    $idiom_langguage='vietnam';
    $this->session->set_userdata('language', $idiom_langguage);
}
// var_dump($idiom_langguage);die;

?>

<!-- nav/topbar -->
<body class="" style="">
    <section class="vbox">
        <header class="bg-dark dk header navbar navbar-fixed-top-xs">
            <div class="navbar-header aside-md"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html"> <i class="fa fa-bars"></i> </a> <a href="#" class="navbar-brand" data-toggle="fullscreen"><img src="<?php echo site_url('assets/images/logo.png')?>" class="m-r-sm">SIHospital</a> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user"> <i class="fa fa-cog"></i> </a> </div>            
            <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user">
                <li class="hidden-xs"> <a href="#" class="dropdown-toggle dk" data-toggle="dropdown"> <i class="fa fa-bell"></i> <span class="badge badge-sm up bg-danger m-l-n-sm count" style="display: inline-block;">3</span> </a>
                    <section class="dropdown-menu aside-xl">
                        <section class="panel bg-white">
                            <header class="panel-heading b-light bg-light"> <strong>You have <span class="count" style="display: inline;">3</span> notifications</strong> </header>
                            <div class="list-group list-group-alt animated fadeInRight"><a href="#" class="media list-group-item" style="display: block;"><span class="pull-left thumb-sm text-center"><i class="fa fa-envelope-o fa-2x text-success"></i></span><span class="media-body block m-b-none">Sophi sent you a email<br><small class="text-muted">1 minutes ago</small></span></a> <a href="#" class="media list-group-item"> <span class="pull-left thumb-sm"> <img src="<?php echo site_url('assets/images/avatar.jpg')?>" alt="John said" class="img-circle"> </span> <span class="media-body block m-b-none"> Use awesome animate.css<br> <small class="text-muted">10 minutes ago</small> </span> </a> <a href="#" class="media list-group-item"> <span class="media-body block m-b-none"> 1.0 initial released<br> <small class="text-muted">1 hour ago</small> </span> </a> </div>
                            <footer class="panel-footer text-sm"> <a href="#" class="pull-right"><i class="fa fa-cog"></i></a> <a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a> </footer>
                        </section>
                    </section>
                </li>
                <li class="dropdown hidden-xs"> <a href="#" class="dropdown-toggle dker" data-toggle="dropdown"><i class="fa fa-fw fa-search"></i></a>
                    <section class="dropdown-menu aside-xl animated fadeInUp">
                        <section class="panel bg-white">
                            <form role="search">
                                <div class="form-group wrapper m-b-none">
                                    <div class="input-group"> <input type="text" class="form-control" placeholder="Search"> <span class="input-group-btn"> <button type="submit" class="btn btn-info btn-icon"><i class="fa fa-search"></i></button> </span> </div>
                                </div>
                            </form>
                        </section>
                    </section>
                </li>
                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Language<b class="caret"></b> </a>
                    <ul class="dropdown-menu animated fadeInRight"> <span class="arrow top"></span>
                        <?php if($idiom_langguage=='english'){?>
                            <li class="active"> <a href="?lang=english">English</a> </li>
                            <li> <a href="?lang=vietnam">VietNam</a> </li>
                        <?php }else{?>
                            <li> <a href="?lang=english">English</a> </li>
                            <li class="active"> <a href="?lang=vietnam">VietNam</a> </li>
                        <?php }?>
                    </ul>
                </li>
                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left"> <img src="<?php echo (isset($userCurrent->user_id->avatar)) ? site_url($userCurrent->user_id->avatar) : ''?>" style="height: 30px; width: 100%;"> </span> <?php echo (isset($userCurrent->user_id->username)) ? $userCurrent->user_id->username : ''?> <b class="caret"></b> </a>

                    <ul class="dropdown-menu animated fadeInRight"> <span class="arrow top"></span>
                        <li> <a href="#">Settings</a> </li>
                        <li> <a href="profile">Profile</a> </li>
                        <li> <a href="mail"> <span class="badge bg-danger pull-right">3</span> Notifications </a> </li>
                        <li> <a href="portlet">Help</a> </li>
                        <li class="divider"></li>
                        <li id="btnSignOut" > <a  data-toggle="ajaxModal">Logout</a> </li>
                    </ul>
                </li>
            </ul>
            <?php if($this->session->flashdata('item')){ ?>
                <div class="alert alert-success" onclick="this.style.display = 'none'" style="position: absolute; top: 57px; z-index: 1; height: 76px; float: right; right: 14px;">
                    <h4><?php echo $this->session->flashdata('item'); ?></h4>
                </div>
          <?php } ?>
                <div class="alert alert-success" id="show-glb-message" onclick="this.style.display = 'none'" style="display:none; position: absolute; top: 57px; z-index: 1; height: 76px; float: right; right: 14px;">
                    <h4></h4>
                </div>

        </header>
<!-- end nav/topbar -->