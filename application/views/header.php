<!DOCTYPE html>
<html lang="en" class="app js no-touch no-android chrome no-firefox no-iemobile no-ie no-ie10 no-ie11 no-ios no-ios7 ipad">

<head>
	<link rel="icon" href="<?= base_url('assets/images/ico.webp');?>" type="image/png"/>
	<link rel="shortcut icon" href="<?= base_url('assets/images/ico.webp');?>" type="image/png"/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
    <title><?=$title;?> | SIHospital Management System</title>    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <link href='<?= base_url("assets/js/datetimepicker/jquery.datetimepicker.min.css");?>' rel='stylesheet'>    
    <?php if(isset($css) && count($css) > 0) {
      foreach ($css as $link) {
        echo "<link href='$link' rel='stylesheet'>";
      }
    }
    ?>

    <?php $user = $this->session->userdata('user'); ?>
    <input type="hidden" class="session-id" value="<?php echo $user->session_id ?>">
    <input type="hidden" class="device-id" value="<?php echo $user->device_id ?>">
    <input type="hidden" class="user_id" value="<?php echo $user->user_id->id?>">
      <script src="<?= base_url('assets/js/app.v1.js')?>" type="text/javascript"></script>
    <script src="<?= base_url('assets/js/main.js')?>" type="text/javascript"></script>    
    <script src="<?= base_url('assets/js/datetimepicker/jquery.datetimepicker.full.min.js')?>" type="text/javascript"></script>   
	<script src="<?= base_url('assets/js/select_static.js')?>" type="text/javascript"></script>    	
</head>