<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once 'model/QueryBuilder.php';

class MY_Model extends CI_Model
{

    /**
     * entityManager
     *
     * @var entityManager
     */
    public $entityManager;

    /**
     * list Foreign Key use when table have Foreign key - default = array()
     *
     * @var array
     */
    public $listForeignKey;

    /**
     * table name
     *
     * @var string
     */
    public $mainTableName;

    /**
     * Entity Class Name
     *
     * @var string
     */
    public $mainEntityClassName;

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Doctrine');
        $this->entityManager = $this->doctrine->entityManager();
    }

    /**
     * Method to get an item
     *
     * @access public
     *
     * @param string $id
     *            primary key
     *
     * @return object An object of data items on success, false on failure.
     */
    public function get($id, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;
        $entity        = $entityManager->find($this->getMainTableName(), $id);

        $data = new stdClass();

        if (!empty($entity))
        {
            $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
        }

        return $data;
    }

    /**
     * Method to get a list of items.
     *
     * @access public
     *
     * @param object $apiGetMethodParamObject ApiGetMethodParamObject
     * @param array  $listReferredColumn      referred column
     *
     * @return object An list of object of data items on success, null on failure.
     */
    public function getList($apiGetMethodParamObject, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;

        $data = array();

        $queryData = $this->getQueryForGetList($apiGetMethodParamObject);
        $query     = $entityManager->createQuery($queryData);

        if ($apiGetMethodParamObject->itemEachPage > 0) {
            $offset = $apiGetMethodParamObject->pageNumber * $apiGetMethodParamObject->itemEachPage;
            $query->setMaxResults($apiGetMethodParamObject->itemEachPage);
            $query->setFirstResult($offset);
        }
        $listEntity = $query->getResult();
        foreach ($listEntity as $entity) {
            $data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
        }

        $pageInfo = new stdClass();
        {
            $queryDataCount = $this->getQueryForGetList($apiGetMethodParamObject, true);
            $countResult    = $entityManager->createQuery($queryDataCount)->getResult();

            $sizeAllResult = $countResult[0][1]; // TODO:GET COUNT BUT MUST BE CLEAR HERE

            $pageInfo->sumpage = (string)(ceil((int)$sizeAllResult / ($apiGetMethodParamObject->itemEachPage > 0 ? $apiGetMethodParamObject->itemEachPage : 1)));
            $pageInfo->current = (string)($apiGetMethodParamObject->pageNumber + 1);
            $pageInfo->limit   = (string)($apiGetMethodParamObject->itemEachPage);
        }

        $result       = new stdClass();
        $result->data = $data;
        $result->page = $pageInfo;

        return $result;
    }

    /**
     * Method to insert item.
     *
     * @access public
     *
     * @param array $listParam
     *            item
     *
     * @return object An object of data items on success, false on failure.
     */
    public function postEvent($listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $entityManager = $this->entityManager;

        $mainEntityClassName = $this->getMainEntityClassName();
        $entityObject        = new $mainEntityClassName();

        $listParam['created_at'] = new DateTime("now");

        if (isset($listParam['begin_date'])) {
            $listParam['begin_date'] = new DateTime($listParam['begin_date']);
        }

        if (isset($listParam['expiration_date'])) {
            $listParam['expiration_date'] = new DateTime($listParam['expiration_date']);
        }

        if (isset($listParam['examination_date'])) {
            $listParam['examination_date'] = new DateTime($listParam['examination_date']);
        }

        if (isset($listParam['injection_date'])) {
            $listParam['injection_date'] = new DateTime($listParam['injection_date']);
        }

        // Add temp - to get code - stt = 0
        if (isset($listParam['stat']) && $listParam['stat'] == 0) {
            if ( ! empty($listParam)) {
                foreach ($listParam as $key => $value) {
                    // Is this Id exist in foreign table
                    if (!$this->isItemIsForeignKey($key)) {
                        $methodSet = $this->getMethodNameInEntity($key);
                        // Check method
                        if (method_exists($entityObject, $methodSet)) {
                            $entityObject->$methodSet($value);
                        }
                    }
                }
            }
        }
        elseif ( ! empty($listParam)) {
            foreach ($listParam as $key => $value) {
                // Is this Id exist in foreign table
                if ($this->isItemIsForeignKey($key)) {
                    $listForeignKey = $this->getListForeignKey();
                    $tableName      = $listForeignKey[$key];
                    $value          = $entityManager->find($tableName, $value);

                    if (empty($value)) {
                        return null;
                    }
                }

                $methodSet = $this->getMethodNameInEntity($key);
                // Check method
                if (method_exists($entityObject, $methodSet)) {
                    $entityObject->$methodSet($value);
                }
            }
        }

        $entityManager->persist($entityObject);
        $entityManager->flush();

        $lastInsertId = $entityObject->getId();
        $entity       = $entityManager->find($this->getMainTableName(), $lastInsertId);

        if ($entityObject->isHaveCodeField()) {
            $newCode = $this->getCodeWithID($lastInsertId);
            $entity->setCode($newCode);
            $entityManager->flush();
        }

        $data = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);

        return $data;
    }

    /**
     * Method to update multi item.
     *
     * @access public
     *
     * @param string $ids
     *            list of id 1_2_12
     * @param array  $listParam
     *            item
     *
     * @return array
     */
    public function putMultiEvent($ids, $listParam, $listReferredColumn = array(), $listSelfReferredColumn = array())
    {
        $data = array();

        if ( ! empty($ids) && ! empty($listParam)) {
            unset($listParam['id']);

            if (isset($listParam['last_modified'])) {
                $listParam['last_modified'] = new DateTime("now");
            }

            if (isset($listParam['begin_date'])) {
                $listParam['begin_date'] = new DateTime("now");
            }

            if (isset($listParam['expiration_date'])) {
                $listParam['expiration_date'] = new DateTime("now");
            }

            if (isset($listParam['examination_date'])) {
                $listParam['examination_date'] = new DateTime($listParam['examination_date']);
            }

            if (isset($listParam['injection_date'])) {
                $listParam['injection_date'] = new DateTime($listParam['injection_date']);
            }

            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $data = array();

                        return $data;
                    } else {
                        foreach ($listParam as $key => $value) {
                            if ($this->isItemIsForeignKey($key)) {
                                $listForeignKey = $this->getListForeignKey();
                                $tableName      = $listForeignKey[$key];
                                $value          = $entityManager->find($tableName, $value);

                                if (empty($value)) {
                                    $entityManager->getConnection()->rollBack();
                                    $data = array();

                                    return $data;
                                }
                            }

                            $methodSet = $this->getMethodNameInEntity($key);
                            $entity->$methodSet($value);
                        }

                        $entityManager->flush();

                        $data[] = $entity->buildObject($listReferredColumn, $listSelfReferredColumn);
                    }
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $data = array();
            }
        } else {
            $data = array();
        }

        return $data;
    }

    /**
     * Method to delete multi item.
     *
     * @access public
     *
     * @param array $ids
     *            list of id [1,2,3]
     *
     * @return boolean
     */
    public function deleteMultiEvent($ids)
    {
        $status = true;

        if ( ! empty($ids)) {
            $entityManager = $this->entityManager;
            $entityManager->getConnection()->beginTransaction();
            $entityManager->getConnection()->setAutoCommit(false);

            try {
                foreach ($ids as $id) {
                    $entity = $entityManager->find($this->getMainTableName(), $id);

                    // Return 11000 not found this item
                    if (empty($entity)) {
                        $entityManager->getConnection()->rollBack();
                        $status = false;
                        break;
                    }

                    $entityManager->remove($entity);
                    $entityManager->flush();
                }

                $entityManager->getConnection()->commit();
                $entityManager->close();
            } catch (Exception $e) {
                $entityManager->getConnection()->rollBack();
                $status = false;
            }
        } else {
            $status = false;
        }

        return $status;
    }

    /**
     * get Query for Get List
     *
     * @access public
     *
     * @param object  $apiGetMethodParamObject
     * @param boolean $countMode
     *
     * @return string The DQL field(s)
     */
    public function getQueryForGetList($apiGetMethodParamObject, $countMode = false)
    {
        $alphas       = range('A', 'Z');
        $anphaCount   = 0;
        $queryBuilder = $this->getQueryBuilder($countMode);

        $OrderClause = new Order($apiGetMethodParamObject->sortBy, $apiGetMethodParamObject->sortDirection);

        $mainTableQueryObjectTableName       = $this->mainTableName;
        $mainTableQueryObjectEntityClassName = $this->mainEntityClassName;
        $mainTableQueryObjectTableAlias      = $alphas[$anphaCount++];
        $mainTableQueryObject                = new TableQueryObject($mainTableQueryObjectTableName,
            $mainTableQueryObjectTableAlias, $mainTableQueryObjectEntityClassName, true);
        $mainTableQueryObject->addOrderByCondition($OrderClause);
        $mainTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);

        $listJoinTableQueryObject = [];

        foreach ($this->listForeignKey as $foreignKey => $tableName) {
            $joinTableQueryObjectTable           = $tableName;
            $joinTableQueryObjectEntityClassName = $tableName;
            $joinTableQueryObjectTableAlias      = $alphas[$anphaCount++];

            $joinTableQueryObject = new TableQueryObject($joinTableQueryObjectTable, $joinTableQueryObjectTableAlias,
                $joinTableQueryObjectEntityClassName, false);
            $joinTableQueryObject->addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject);
            $joinTableQueryObject->addKeywordInApiGetMethodParamObject($apiGetMethodParamObject);

            $columnJoin = new CollumJoin($joinTableQueryObjectTableAlias, $foreignKey);
            $mainTableQueryObject->addCollumJoin($columnJoin);

            $listJoinTableQueryObject[] = $joinTableQueryObject;
        }

        $DQL = $queryBuilder->getDQL($mainTableQueryObject, $listJoinTableQueryObject,
            $apiGetMethodParamObject->keyword);

        return $DQL;
    }

    protected function getQueryBuilder($countMode)
    {
        return $countMode ? new QueryCountBuilder() : new QueryBuilder();
    }

    /**
     * get Main Table Name
     *
     * @access public
     * @return string
     */
    public function getMainTableName()
    {
        return $this->mainTableName;
    }

    /**
     * get Main Entity Class Name
     *
     * @access public
     * @return string
     */
    public function getMainEntityClassName()
    {
        return $this->mainEntityClassName;
    }

    /**
     * get List Foreign Key
     *
     * @access public
     * @return array : [ForeignKey_ID]=> [Table name For ForeignKey]
     */
    public function getListForeignKey()
    {
        return $this->listForeignKey;
    }

    /**
     * get Method Name InEntity
     *
     * @access public
     *
     * @param string $fieldName
     *            filed name (code, name)
     *
     * @return string
     */
    public function getMethodNameInEntity($fieldName)
    {
        $methodSet = 'set' . ucfirst($fieldName);

        return $methodSet;
    }

    /**
     * get Code With ID
     *
     * @access public
     *
     * @param int $id
     *            The unique identifier for the object
     *
     * @return string code
     */
    public function getCodeWithID($id)
    {
        return sprintf("%06d", $id);
    }

    /**
     * check is Item Is Foreign Key
     *
     * @access public
     *
     * @param int $key
     *            foreign key
     *
     * @return string code
     */
    public function isItemIsForeignKey($key)
    {
        $isFound        = false;
        $listForeignKey = $this->getListForeignKey();

        foreach ($listForeignKey as $foreignKey => $tableName) {
            if ($key == $foreignKey) {
                $isFound = true;
            }
        }

        return $isFound;
    }

    public function getInnerJoinCodition()
    {
        return [
            "sih_list_insurance_company" => "name"
        ];
    }
}
