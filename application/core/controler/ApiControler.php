<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ApiController extends MY_Controller
{

    const API_STATUS_OK = "10000";

    const API_STATUS_EMPTY = "11000";

    const API_STATUS_AUTH_FAIL = "12000";

    const API_STATUS_MISSING_PARAM_LIST = "12001";

    const API_STATUS_ADDING_FOREIGN_KEY_ERROR = "12002";

    const API_STATUS_DELETE_FAIL = "12003";

    const API_STATUS_UPDATE_FAIL = "12004";

    const API_STATUS_DELETE_FOREIGN_KEY_ERROR = "12005";

    const CONTENT_TYPE = "Content-type: application/json; charset=utf-8";

    /**
     * model
     *
     * @var mixed
     */
    protected $model;

    /**
     * main Model Class Name
     *
     * @var string
     */
    protected $mainModelClassName;

    /**
     * list Param Need Valid
     *
     * @var array
     */
    protected $listParamNeedValid = array();

    /**
     * list Foreign key in this table
     *
     * @var array
     */
    protected $listReferredColumn = array();

    /**
     *  list Foreign key not in this table
     *
     * @var array
     */
    protected $listSelfReferredColumn = array();

    /**
     * Constructor
     *
     * @access public
     *
     *
     */
    public function __construct()
    {
        parent::__construct();

        header($this::CONTENT_TYPE);

        if ( ! $this->checkPermissionSession()) {
            $status = $this->getStatusObject($this::API_STATUS_AUTH_FAIL);
            set_status_header(StatusCodes::HTTP_UNAUTHORIZED);
            exit($this->getJsonText(null, $status));
        } else {
            $mainModelClassName = $this->getMainModelClassName();

            if ( ! empty($mainModelClassName)) {
                $this->loadModel();
                $this->model = new $mainModelClassName();
            }
        }
    }

    /**
     * getList
     *
     * @return void
     */
    public function getList()
    {
        $apiGetMethodParamObject = new ApiGetMethodParamObject();
        $result                  = $this->model->getList($apiGetMethodParamObject, $this->getListReferredColumn(),
            $this->getListSelfReferredColumn());
        $this->printJson($result->data, null, $result->page);
    }

    /**
     * postData
     *
     * @return void
     */
    public function postData()
    {
        $listParamInvalid = $this->getListParamInvalid($_POST);

        $flagAddTemp = 0;
        // Add temp - to get code - stt = 0
        if (isset($_POST['stat']) && $_POST['stat'] == 0) {
            $flagAddTemp = 1;
        }

        if ( ! empty($listParamInvalid) && $flagAddTemp == 0) {
            $status = $this->getStatusObject($this::API_STATUS_MISSING_PARAM_LIST, json_encode($listParamInvalid));
            $this->printJson(null, $status);
        } else {
            $item = [];
            // Format data before post
            foreach ($_POST as $key => $value) {
                $item[$key] = $value;
            }
            $result = $this->model->postEvent($item, $this->getListReferredColumn(),
                $this->getListSelfReferredColumn());
            $this->printJson($result, null);
        }
    }

    /**
     * putData
     *
     * @param string $ids
     *            list of id 1_2_12
     *
     * @return void
     */
    public function putData($ids)
    {
        parse_str(file_get_contents("php://input"), $data);
        $item = $this->formatBaseDataPut($data);
        foreach ($data as $key => $value) {
            $item[$key] = $value;
        }
        $listId = explode("_", $ids);
        $result = $this->model->putMultiEvent($listId, $item, $this->getListReferredColumn(),
            $this->getListSelfReferredColumn());
        // Success
        if (is_array($result) && ! empty($result)) {
            $this->printJson($result, $this->getStatusObject($this::API_STATUS_OK));
        } // Fail
        else {
            $this->printJson(null, $this->getStatusObject($this::API_STATUS_UPDATE_FAIL));
        }
    }

    /**
     * Get
     *
     * @param int $id
     *            primary key
     *
     * @return void
     */
    public function get($id)
    {
        $result = $this->model->get($id, $this->getListReferredColumn(), $this->getListSelfReferredColumn());

        $this->printJson($result);
    }

    /**
     * deleteData
     *
     * @param string $ids
     *            primary key 1_2_3
     *
     * @return void
     */
    public function deleteData($ids)
    {
        $listId = explode("_", $ids);
        $result = $this->model->deleteMultiEvent($listId);
        // Success
        if ($result) {
            $this->printJson(null, $this->getStatusObject($this::API_STATUS_OK));
        } // Fail
        else {
            $this->printJson(null, $this->getStatusObject($this::API_STATUS_DELETE_FAIL));
        }
    }

    protected function printJson($data = null, $statusObject = null, $pageObject = null)
    {
        if ($statusObject === null) {
            $statusObject = $this->getStatusObject($data === null || empty($data) ? $this::API_STATUS_EMPTY : $this::API_STATUS_OK);
        }
        $this->output->set_output($this->getJsonText($data, $statusObject, $pageObject));
    }

    protected function getStatusObject($code, $mess = "")
    {
        if ($mess == "") {
            $messenger = "";
            switch ($code) {
                case $this::API_STATUS_OK:
                    $messenger = "OK";
                    break;
                case $this::API_STATUS_EMPTY:
                    $messenger = "Empty";
                    break;
                case $this::API_STATUS_AUTH_FAIL:
                    $messenger = "Authenticate fail!";
                    break;
                case $this::API_STATUS_MISSING_PARAM_LIST:
                    $messenger = "Missing param list!";
                    break;
                case $this::API_STATUS_ADDING_FOREIGN_KEY_ERROR:
                    $messenger = "Adding foreign key fail";
                    break;
                case $this::API_STATUS_UPDATE_FAIL:
                    $messenger = "Update fail!";
                    break;
                case $this::API_STATUS_DELETE_FAIL:
                    $messenger = "Delete fail!";
                    break;
                case $this::API_STATUS_DELETE_FOREIGN_KEY_ERROR:
                    $messenger = "Cannot delete a parent row: a foreign key constraint fails";
                    break;
                default:
                    $messenger = "Unknow";
            }
        } else {
            $messenger = $mess;
        }
        $status = [
            'return_code' => $code,
            'message'     => $messenger
        ];

        return $status;
    }

    private function updateStatusHttpHeader($statusObject)
    {
        $this->output->set_status_header(200);
    }

    private function getJsonText($data, $statusObject, $pageObject = null)
    {
        $response = [
            'status' => $statusObject,
            'data'   => $data === null ? "" : $data
        ];
        if ($pageObject != null) {
            $response['page'] = $pageObject;
        }

        return json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    /**
     * Method check permission of this session
     * Does user have permission to acccess this api
     *
     *
     * @access public
     *
     * @param string $sessionId      session
     * @param string $deviceId       deviceId: UUID of device - save in cookie\
     * @param int    $isUpdateExpiry update expiry time: 0 not update, 1 update
     *
     * @return boolean
     */
    private function checkPermissionSession()
    {
        $this->load->model('list/User_Action_Model');
        $this->load->model('list/Api_Permission_Model');
        // $routePath
        $routePath = $this->router->fetch_class();
        // 1- user is login
        if ($routePath == 'API_L_1') {
            return true;
        }

        // 2- user not login


        // Get session userdata
        $user_data      = $this->session->get_userdata('user');
        $isSessionValid = false;

       /* var_dump($_SERVER['HTTP_SESSION_ID']);
        var_dump($_SERVER['HTTP_DEVICE_ID']);die;*/

        if (isset($_SERVER['HTTP_SESSION_ID']) && isset($_SERVER['HTTP_DEVICE_ID'])) {
            $sessionId = $_SERVER['HTTP_SESSION_ID'];
            $deviceId  = $_SERVER['HTTP_DEVICE_ID'];

            $userActionModel = new User_Action_Model;

            // Check session valid
            $isSessionValid = $userActionModel->isSessionValid($sessionId, $deviceId, 1);

            // Check permission
            if ($isSessionValid) {
                // Get user ID
                $userId          = $userActionModel->getUserKey($sessionId, $deviceId);
                $permissionModel = new Api_Permission_Model;

                $isPermission = $permissionModel->isHavePermission($userId->user_id->type, $routePath, $_SERVER['REQUEST_METHOD']);

                if ($isPermission) {
                    $isSessionValid = false;
                }
            }
        }

        // return $isSessionValid;
        return true;
    }

    /**
     * format Base DataPut
     *
     * @access protected
     *
     *
     * @return array
     */
    protected function formatBaseDataPut($data)
    {
        $item = array();

        if (isset($data['name'])) {
            $item['name'] = $data['name'];
        }

        if (isset($data['code'])) {
            $item['code'] = $data['code'];
        }

        if (isset($data['orders'])) {
            $item['orders'] = $data['orders'];
        }

        if (isset($data['stat'])) {
            $item['stat'] = $data['stat'];
        }

        return $item;
    }

    /**
     * set MainModel Class Name
     *
     * @access public
     *
     *
     * @return void
     */
    protected function setMainModelClassName($mainModelClassName)
    {
        $this->mainModelClassName = $mainModelClassName;
    }

    /**
     * get MainModel Class Name
     *
     * @access public
     *
     *
     * @return string
     */
    protected function getMainModelClassName()
    {
        return $this->mainModelClassName;
    }

    /**
     * load model
     *
     * @access public
     *
     *
     * @return void
     */
    protected function loadModel()
    {
        if ( ! empty($this->mainModelClassName)) {
            $this->load->model('list/' . $this->mainModelClassName);
        }
    }

    /**
     * get List Param Need Valid - ex: foreign key
     *
     * @access public
     *
     *
     * @return array
     */
    protected function getListParamNeedValid()
    {
        return $this->listParamNeedValid;
    }

    /**
     * set List Param Need Valid - ex: foreign key
     *
     * @access public
     *
     * @param array $listParamNeedValid
     *            list Param Need Valid - required column
     *
     * @return void
     */
    protected function setListParamNeedValid($listParamNeedValid)
    {
        $this->listParamNeedValid = $listParamNeedValid;
    }

    /**
     * set list Referred Column - example: get combined medicine id look at API_L_41
     *
     * @access public
     *
     * @param array $listReferredColumn
     *            list Referred Column
     *
     * @return void
     */
    protected function setListReferredColumn($listReferredColumn)
    {
        $this->listReferredColumn = $listReferredColumn;
    }

    /**
     * set list self Referred Column - example: get combined medicine id look at API_L_41
     *
     * @access public
     *
     * @param array $listSelfReferredColumn
     *            list self Referred Column
     *
     * @return void
     */
    protected function setListSelfReferredColumn($listSelfReferredColumn)
    {
        $this->listSelfReferredColumn = $listSelfReferredColumn;
    }

    /**
     * Check validate, array input with required array
     *
     * @param array $data
     *            list Param Need Valid - required column
     *
     * @return array
     */
    protected function getListParamInvalid($data)
    {
        $isValidate         = 0;
        $listParamNeedValid = $this->getListParamNeedValid();
        $listParamInvalid   = [];
        foreach ($listParamNeedValid as $paramNeedValid) {
            if (empty($data[$paramNeedValid])) {
                $listParamInvalid[] = $paramNeedValid;
            }
        }

        return $listParamInvalid;
    }

    /**
     * getListReferredColumn
     *
     * @return array
     */
    protected function getListReferredColumn()
    {
        return $this->listReferredColumn;
    }

    /**
     * getListSelfReferredColumn
     *
     * @return array
     */
    protected function getListSelfReferredColumn()
    {
        return $this->listSelfReferredColumn;
    }
}

class StatusCodes
{

    // [Informational 1xx]
    const HTTP_CONTINUE = 100;

    const HTTP_SWITCHING_PROTOCOLS = 101;

    // [Successful 2xx]
    const HTTP_OK = 200;

    const HTTP_CREATED = 201;

    const HTTP_ACCEPTED = 202;

    const HTTP_NONAUTHORITATIVE_INFORMATION = 203;

    const HTTP_NO_CONTENT = 204;

    const HTTP_RESET_CONTENT = 205;

    const HTTP_PARTIAL_CONTENT = 206;

    // [Redirection 3xx]
    const HTTP_MULTIPLE_CHOICES = 300;

    const HTTP_MOVED_PERMANENTLY = 301;

    const HTTP_FOUND = 302;

    const HTTP_SEE_OTHER = 303;

    const HTTP_NOT_MODIFIED = 304;

    const HTTP_USE_PROXY = 305;

    const HTTP_UNUSED = 306;

    const HTTP_TEMPORARY_REDIRECT = 307;

    // [Client Error 4xx]
    const errorCodesBeginAt = 400;

    const HTTP_BAD_REQUEST = 400;

    const HTTP_UNAUTHORIZED = 401;

    const HTTP_PAYMENT_REQUIRED = 402;

    const HTTP_FORBIDDEN = 403;

    const HTTP_NOT_FOUND = 404;

    const HTTP_METHOD_NOT_ALLOWED = 405;

    const HTTP_NOT_ACCEPTABLE = 406;

    const HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;

    const HTTP_REQUEST_TIMEOUT = 408;

    const HTTP_CONFLICT = 409;

    const HTTP_GONE = 410;

    const HTTP_LENGTH_REQUIRED = 411;

    const HTTP_PRECONDITION_FAILED = 412;

    const HTTP_REQUEST_ENTITY_TOO_LARGE = 413;

    const HTTP_REQUEST_URI_TOO_LONG = 414;

    const HTTP_UNSUPPORTED_MEDIA_TYPE = 415;

    const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;

    const HTTP_EXPECTATION_FAILED = 417;

    // [Server Error 5xx]
    const HTTP_INTERNAL_SERVER_ERROR = 500;

    const HTTP_NOT_IMPLEMENTED = 501;

    const HTTP_BAD_GATEWAY = 502;

    const HTTP_SERVICE_UNAVAILABLE = 503;

    const HTTP_GATEWAY_TIMEOUT = 504;

    const HTTP_VERSION_NOT_SUPPORTED = 505;
}

class ApiGetMethodParamObject
{

    public $keyword;

    public $pageNumber;

    public $itemEachPage;

    public $sortBy;

    public $sortDirection;

    public $listFilter = [];

    public $listKeywordExtend = [];

    private static $LIST_BASIC_PARAM = [
        'keyword',
        'page_number',
        'item_in_page',
        'sort_by',
        'sort_direction',
        'list_keyword_extend'
    ];

    function __construct()
    {
        $this->buildBasicParam();
        $this->buildListParamFilter();
    }

    private function buildBasicParam()
    {
        $get     = $_GET;
        $keyword = '';
        if (isset($get['keyword'])) {
            $keyword = $get['keyword'];
        }
        $page_number = null;
        if (isset($get['page_number'])) {
            $page_number = $get['page_number'];
        }
        $item_in_page = null;
        if (isset($get['item_in_page'])) {
            $item_in_page = $get['item_in_page'];
        }
        $sort_by = null;
        if (isset($get['sort_by'])) {
            $sort_by = $get['sort_by'];
        }
        $sort_direction = null;
        if (isset($get['sort_direction'])) {
            $sort_direction = $get['sort_direction'];
        }

        $listKeywordExtend = [];
        if (isset($get['list_keyword_extend'])) {
            $listKeywordExtend = $get['list_keyword_extend'];
        }

        if (empty($page_number) || $page_number == 1 || $page_number == 0) {
            $page_number = 0;
        } else {
            $page_number = $page_number - 1;
        }
        if (empty($item_in_page)) {
           /* $item_in_page = 10;*/
        } else {
            $item_in_page = (int)$item_in_page;
        }
        if (empty($sort_by)) {
            $sort_by = 'id';
        }
        if (empty($sort_direction)) {
            $sort_direction = 'asc';
        }

        $this->keyword           = $keyword;
        $this->pageNumber        = $page_number;
        $this->itemEachPage      = $item_in_page;
        $this->sortBy            = $sort_by;
        $this->sortDirection     = $sort_direction;
        $this->listKeywordExtend = $listKeywordExtend;
    }

    private function buildListParamFilter()
    {
        $listGetParam = $_GET;
        foreach ($listGetParam as $key => $value) {
            if (in_array($key, $this::$LIST_BASIC_PARAM)) {
                unset($listGetParam[$key]);
            }
        }
        $this->listFilter = $listGetParam;
    }
}