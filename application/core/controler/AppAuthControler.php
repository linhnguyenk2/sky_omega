<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AppAuthControler extends MY_Controller {

	protected $listMenuGrantAccess = [];

	protected $listActionGrantAccess = [];

	function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata('user');
		if (empty($user))
		{
			redirect('/');
		}
		else
		{
			if (isset($_GET['lang']))
			{
				$this->set_language();
			}
			// Load init language file
			$this->lang->load('init_lang', $this->getCurrentLanguageUserChoose());
			$this->loadLangFile();
		}
	}

	private function set_language()
	{
		if (isset($_GET['lang']))
		{
			$set_lang  = $_GET['lang'];
			$list_lang = [
				'english',
				'vietnam'
			];
			// default just set lang vietnam
			$this->config->set_item('language', 'vietnam');
			$current_lang = $this->session->userdata('language');
			if ( ! in_array($set_lang, $list_lang))
			{
				$set_lang = ! $current_lang ? $list_lang[0] : $current_lang;
			}
			$this->session->set_userdata('language', $set_lang);
		}
	}

	private function loadLangFile()
	{
		$languageFileName = $this->getLanguageFileName();
		if (empty($languageFileName) == FALSE)
		{
			$this->lang->load($this->getLanguageFileName(), $this->getCurrentLanguageUserChoose());
		}
	}

	protected function get_css_js_basic()
	{
		$data        = [];
		$data['js']  = $this->getListJavascript();
		$data['css'] = $this->getListCSS();

		return $data;
	}

	protected function getTemplate($dataInput = array())
	{
		$dataCSS_JS = $this->get_css_js_basic();
		$data       = array_merge($dataCSS_JS, $dataInput);
		// $this->lang from CI lib defaut
		$data['list_lang'] = $this->lang;
		// list lang content for view
		// Set view
		$data['view'] = array(
			'header'  => "header",
			'topbar'  => "nav/topbar",
			'menu'    => "nav/leftbar_v2",
			'content' => $this->getContentViewName(),
			'footer'  => "footer"
		);

		$data['title']        = $this->getTitle();
		$data['menu']         = $this->getMenu();
		$data['activeMenuId'] = $this->getActiveMenuId();
		$data['route_path']   = $this->router->fetch_method();

		$html = '';

      /*  $user = $this->session->userdata('user');

        $html .=  '<input type="hidden" class="session-id" value="' . $user->session_id . '">';
        $html .=  '<input type="hidden" class="device-id" value="' . $user->device_id . '">';*/

        if (isset($data['view']['header']) && ! empty($data['view']['header']))
		{
			$html .= $this->load->view($data['view']['header'], $data, TRUE);
		}

		if (isset($data['view']['topbar']) && ! empty($data['view']['topbar']))
		{
			$html .= $this->load->view($data['view']['topbar'], $data, TRUE);
		}

		$html .= "<section><section class=\"hbox stretch\">";

		$html .= $this->apppermissionhelper->getMenuForUser($this->getCurrentUserId(), $data);

		if (isset($data['view']['content']))
		{
			// $html .= $this->load->view($data['view']['content'], $data, true);
			$html .= $this->apppermissionhelper->getContentForUser($this->getCurrentUserId(), $data);
		}

        if (isset($data['view']['footer']))
        {
            $html .= $this->load->view($data['view']['footer'], $data, TRUE);
        }

		return $html;
	}

	protected function getListJavascript()
	{
		return array(
			base_url('assets/js/datatables/jquery.dataTables.min.js'), // dataTable
			base_url('assets/js/datepicker/bootstrap-datepicker.js'), // date_input
			base_url('assets/js/handle.js'),
			base_url('assets/js/app.plugin.js'),
			base_url('assets/js/api/wraper.js'),
			base_url('assets/js/api/status_code.js'),
			base_url('assets/js/api/select_basic.js'),
			base_url('assets/js/api/create_page.js'),
                    
			base_url('assets/js/fomart_static.js')
		);
	}

	protected function getListCSS()
	{
		return array(
			base_url('assets/css/font.css'),
			base_url('assets/css/app.v1.css'),
			base_url('assets/css/style.css'), // customer css style
			base_url('assets/js/datatables/datatables.css'), // dataTable
			base_url('assets/js/datepicker/datepicker.css'), // date_input
			base_url('assets/css/header.css'),
			base_url('assets/css/template_si.css'),
			base_url('assets/css/header.css'),
			base_url('assets/css/si_input_form.css'),
			base_url('assets/css/api_list.css')
		);
	}

	protected function getTitle()
	{
		return "";
	}

	protected function getActiveMenuId()
	{
		return "";
	}

	protected function getMenu()
	{
		return "index";
	}

	protected function getContentViewName()
	{
		return "content/template/index";
	}

	protected function getLanguageFileName()
	{
		return '';
	}

	protected function getCurrentLanguageUserChoose()
	{
		$language = $this->session->userdata('language');
		if (empty($language))
			$language = 'vietnam';

		return $language;
	}

	protected function getCurrentUserId()
	{
		$userId = NULL;
		if (isset($this->session->userdata['user']->user_id->id))
		{
			$userId = $this->session->userdata['user']->user_id->id;
		}

		return $userId;
	}
}