<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TableQueryObject
{

    public $isMainTable;

    public $table;

    public $tableAlias;

    public $entityClassName;

    public $listCollumJoin = [];

    public $listCollumKeyWord = [];

    public $listWhereCondition = [];

    public $listOrderCondition = [];

    function __construct($table, $tableAlias, $entityClassName, $isMainTable = false)
    {
        $this->isMainTable     = $isMainTable;
        $this->table           = $table;
        $this->tableAlias      = $tableAlias;
        $this->entityClassName = $entityClassName;
        $this->buildCollumKeywordForMainTable();
    }

    public function addCollumJoin($collumJoin)
    {
        $this->listCollumJoin[] = $collumJoin;
        unset($this->listCollumKeyWord[array_search($collumJoin->foreignKeyCollum, $this->listCollumKeyWord)]);
    }

    public function addCollumKeyword($collumKeyword)
    {
        $this->listCollumKeyWord[] = $collumKeyword;
    }

    public function addWhereCondition($clause)
    {
        $this->listWhereCondition[] = $clause;
    }

    public function addListWhereCondition($listClause)
    {
        $this->listWhereCondition = array_merge($this->listWhereCondition, $listClause);
    }

    public function addOrderByCondition($order)
    {
        $this->listOrderCondition[] = $order;
    }

    public function buildCollumKeywordForMainTable()
    {
        if ($this->isMainTable) {
            $entity       = new $this->entityClassName();
            $listProperty = $entity->buildListProperties();
            unset($listProperty["id"]);
            unset($listProperty["orders"]);
            unset($listProperty["stat"]);
            unset($listProperty["created_at"]);
            $this->listCollumKeyWord = array_keys($listProperty);
        }
    }

    public function addWhereConditionInApiGetMethodParamObject($apiGetMethodParamObject)
    {
        foreach ($apiGetMethodParamObject->listFilter as $onGuestingFilter => $value) {
            if ($this->isMainTable) {
                if (strpos($onGuestingFilter, ":") === false) {
                    $entity = new $this->entityClassName();
                    if ($entity->isValidProperty(str_replace("*", "",
                        $onGuestingFilter))) { // remove * in case search OR
                        $listClause = Clause::buildListClause($onGuestingFilter, $value);
                        $this->addListWhereCondition($listClause);
                    }
                }
            } else {
                if (strpos($onGuestingFilter, ":") !== false) {
                    $tableName    = explode(":", $onGuestingFilter)[0];
                    $propertyName = explode(":", $onGuestingFilter)[1];
                    if ($tableName == $this->table) {
                        $entity = new $this->entityClassName();
                        if ($entity->isValidProperty(str_replace("*", "",
                            $propertyName))) { // remove * in case search OR
                            $listClause               = Clause::buildListClause($propertyName, $value);
                            $this->listWhereCondition = array();
                            $this->addListWhereCondition($listClause);

                        }
                    }
                }
            }
        }
    }

    public function addKeywordInApiGetMethodParamObject($apiGetMethodParamObject)
    {
        foreach ($apiGetMethodParamObject->listKeywordExtend as $onGuestingKeywordTable) {
            if ($this->isMainTable) {
                // DO NOTHING, MAIN TABLE ALREADY AUTOMATIC ADD LIST KEYWORD
            } else {
                if (strpos($onGuestingKeywordTable, ":") !== false) {
                    $entity       = new $this->entityClassName();
                    $tableName    = explode(":", $onGuestingKeywordTable)[0];
                    $propertyName = explode(":", $onGuestingKeywordTable)[1];
                    if ($tableName == $this->table) {
                        if ($entity->isValidProperty($propertyName)) {
                            $this->addCollumKeyword($propertyName);
                        }
                    }
                }
            }
        }
    }
}

class CollumJoin
{

    public $foreignKeyCollum;

    public $tableJoinAlias;

    function __construct($tableJoinAlias, $foreignKeyCollum)
    {
        $this->tableJoinAlias   = $tableJoinAlias;
        $this->foreignKeyCollum = $foreignKeyCollum;
    }
}

class Clause
{

    private $condition;

    private $result;

    private $compareChar;

    private $beginquotation = "";

    private $endquotation = "";

    function __construct($condition, $result, $beginquotation = "", $endquotation = "")
    {
        $this->beginquotation = $beginquotation;
        $this->endquotation = $endquotation;
        $this->condition = $condition;
        $this->result    = $result;
        if (strpos($condition, '*') !== false) {
            $this->compareChar = "\nOR ";
        } else {
            $this->compareChar .= "\nAND ";
        }
    }

    public static function buildListClause($condition, $result)
    {
        $listClause = [];

        if (strpos($result, ";") !== false) {
            $listResult = explode(";", $result);
            foreach ($listResult as $itemResult) {
                if ($listResult[0] == $itemResult) {
                    $listClause[] = new Clause($condition, $itemResult, "(","");
                }
                else if (end($listResult) == $itemResult) {
                    $condition = str_replace("*", "", $condition);
                    $listClause[] = new Clause($condition, $itemResult, "",")");
                }
                else
                {
                    $listClause[] = new Clause($condition, $itemResult, "","");
                }
            }
        } else {
            $listClause[] = new Clause($condition, $result, "","");
        }

        return $listClause;
    }

    public function getCondition()
    {
        return str_replace("*", "", $this->condition);
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getCompareChar()
    {
        return $this->compareChar;
    }

    public function getBeginQuotation()
    {
        return $this->beginquotation;
    }
    public function getEndQuotation()
    {
        return $this->endquotation;
    }
}

class Order
{

    public $collumName;

    public $direction;

    function __construct($collumName, $direction)
    {
        $this->collumName = $collumName;
        $this->direction  = $direction;
    }
}

class AdditionalInjection
{

    public $injectJoinClause = "";

    public $injectWhereClause = "";

    public $injectKeyWordClause = "";
}

class QueryBuilder
{

    protected $mainTableQueryObject;

    protected $listJoinTableQueryObject;

    protected $keyWord = "";

    protected $additionalInjectionObject = null;

    public function getDQL(
        $mainTableQueryObject,
        $listJoinTableQueryObject = [],
        $keyword = "",
        $additionalInjectionObject = null
    ) {
        $this->mainTableQueryObject      = $mainTableQueryObject;
        $this->listJoinTableQueryObject  = $listJoinTableQueryObject;
        $this->keyWord                   = $keyword;
        $this->additionalInjectionObject = $additionalInjectionObject;

        $DQL = "";
        $DQL .= $this->buildSelectClause();
        $DQL .= $this->buildJoinClause();
        $DQL .= $this->buildWhereClause();
        $DQL .= $this->buildOrderbyCondition();

        return $DQL;
    }

    // $queryData = 'SELECT p FROM sih_list_health_insurance_card p JOIN p.insurance_company_id k WHERE '.'k.name LIKE :keyword ' . 'or p.id LIKE :keyword ' . 'OR p.name LIKE :keyword ' . 'OR p.code LIKE :keyword ' . 'OR p.stat LIKE :keyword ' . 'ORDER BY ' . $orderBy . ' ' . $sortDirection;
    public function buildSelectClause()
    {
        $mainTable      = $this->mainTableQueryObject->table;
        $mainTableAlias = $this->mainTableQueryObject->tableAlias;
        $select         = "select $mainTableAlias from $mainTable $mainTableAlias \n";

        return $select;
    }

    public function buildJoinClause()
    {
        $mainTable       = $this->mainTableQueryObject->table;
        $mainTableAlias  = $this->mainTableQueryObject->tableAlias;
        $listTable       = array_merge([
            $this->mainTableQueryObject
        ], $this->listJoinTableQueryObject);
        $sizeOfJoinTable = sizeof($listTable);
        $join            = "";
        for ($i = 0; $i < $sizeOfJoinTable - 1; $i++) {
            // last item never join
            $currentJoinTableQueryObject = $listTable[$i];
            $listCurrentCollumJoin       = $currentJoinTableQueryObject->listCollumJoin;
            foreach ($listCurrentCollumJoin as $collumJoin) {
                $join .= "LEFT JOIN  $currentJoinTableQueryObject->tableAlias.$collumJoin->foreignKeyCollum $collumJoin->tableJoinAlias \n";
            }
        }
        if ($this->additionalInjectionObject != null) {
            $join .= $this->additionalInjectionObject->injectJoinClause;
        }

        return $join;
    }

    public function buildWhereClause()
    {
        $where          = "";
        $whereKeyWord   = $this->buildWhereKeyWordClause();
        $whereCondition = $this->buildWhereCondition();

        $where = $whereKeyWord;

        if ( ! empty($whereKeyWord) && ! empty($whereCondition)) {
            $where .= "\nand";
        } else {
            $where .= "";
        }

        $where .= $whereCondition;

        // $where = $whereKeyWord . (! empty($whereKeyWord) && ! empty($whereCondition)) ? "\nand" : "" . $whereCondition;

        if ( ! empty($where)) {
            $where = "where\n" . $where;
        }

        return $where;
    }

    public function buildWhereKeyWordClause()
    {
        $whereClause = "";
        if ( ! empty($this->keyWord)) {
            $mainTable        = $this->mainTableQueryObject->table;
            $mainTableAlias   = $this->mainTableQueryObject->tableAlias;
            $listTable        = array_merge([
                $this->mainTableQueryObject
            ], $this->listJoinTableQueryObject);
            $sizeOfWhereTable = sizeof($listTable);

            $listKeywordCondition = [];

            foreach ($listTable as $table) {
                foreach ($table->listCollumKeyWord as $collumn) {
                    // Bug need fix as soon as possible
                    if ($collumn != 'list_contraindication_in_medicine' &&
                        $collumn != 'list_not_combine_medicine' &&
                        $collumn != 'list_appointed_in_medicine' &&
                        $collumn != 'list_patient_emergency_contact' &&
                        $collumn != 'list_unit_medicine') {
                        $listKeywordCondition[] = "$table->tableAlias.$collumn  LIKE '%$this->keyWord%' ";
                        $listKeywordCondition[] = " OR \n";
                    }
                }
            }

            if ($this->additionalInjectionObject != null && empty($this->additionalInjectionObject->injectKeyWordClause) == false) {
                $listKeywordCondition[] = $this->additionalInjectionObject->$injectKeyWordClause;
            } else {
                // Remove last item OR
                array_pop($listKeywordCondition);
            }

            if ( ! empty($listKeywordCondition)) {
                $whereClause = "(\n" . implode($listKeywordCondition) . "\n)";
            }
        }

        return $whereClause;
    }

    public function buildWhereCondition()
    {
        $listTableQueryObject = array_merge([
            $this->mainTableQueryObject
        ], $this->listJoinTableQueryObject);
        $whereClause          = [];
        foreach ($listTableQueryObject as $tableQueryObject) {
            foreach ($tableQueryObject->listWhereCondition as $whereCondition) {
                $tableAlias    = $tableQueryObject->tableAlias;
                $whereClause[] = "\n" . $whereCondition->getBeginQuotation() . $tableAlias . "." . $whereCondition->getCondition() . " = '" . $whereCondition->getResult() . "'" . $whereCondition->getEndQuotation();
                $whereClause[] = $whereCondition->getCompareChar();
            }
        }

        if ($this->additionalInjectionObject != null && empty($this->additionalInjectionObject->injectWhereClause) == false) {
            $whereClause[] .= $this->additionalInjectionObject->injectWhereClause;
        } else {
            array_pop($whereClause); // remove last CompareChar
        }

        $whereClauseString = "";
        if ( ! empty($whereClause)) {

            $whereClauseString = "(" . implode("", $whereClause) . "\n)";
        }

        return $whereClauseString;
    }

    public function buildOrderbyCondition()
    {
        $listTableQueryObject = array_merge([
            $this->mainTableQueryObject
        ], $this->listJoinTableQueryObject);
        $orderClause          = "";

        foreach ($listTableQueryObject as $tableQueryObject) {
            foreach ($tableQueryObject->listOrderCondition as $orderCondition) {
                if (empty($orderClause)) {
                    $orderClause .= "\nORDER BY ";
                } else {
                    $orderClause .= ", ";
                }
                $tableAlias  = $tableQueryObject->tableAlias;
                $orderClause .= "\n" . $tableAlias . "." . $orderCondition->collumName . " " . $orderCondition->direction;
            }
        }

        return $orderClause;
    }
}

class QueryCountBuilder extends QueryBuilder
{

    public function buildSelectClause()
    {
        $mainTable      = $this->mainTableQueryObject->table;
        $mainTableAlias = $this->mainTableQueryObject->tableAlias;
        $select         = "select count($mainTableAlias.id) from $mainTable $mainTableAlias \n";

        return $select;
    }
}