-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2018 at 12:56 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sihospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `list_healthcare_group`
--

CREATE TABLE IF NOT EXISTS `list_healthcare_group` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `list_healthcare_type`
--

CREATE TABLE IF NOT EXISTS `list_healthcare_type` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  `healthcare_group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sih_form1`
--

CREATE TABLE IF NOT EXISTS `sih_form1` (
  `f1Id` int(11) NOT NULL,
  `f1MaPhieu` int(11) DEFAULT NULL,
  `f1MaBenhNhan` int(11) DEFAULT NULL,
  `f1Child` tinyint(1) DEFAULT NULL,
  `f1Name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `f1Birthday` date NOT NULL,
  `f1Mari` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1Gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `f1Nation` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `f1CMND` int(9) DEFAULT NULL,
  `f1Address` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1Email` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1Homephone` int(20) DEFAULT NULL,
  `f1Mobile` int(20) DEFAULT NULL,
  `f1EmerFullName` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1EmerRelation` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1EmerPhone` int(20) DEFAULT NULL,
  `f1EmerAddress` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1PrivaInsuran` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1HavePrivaInsuran` tinyint(1) DEFAULT NULL,
  `f1OrderNameCompany` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1OrderAddressCompany` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1OrderMST` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1DateCreate` date NOT NULL,
  `f1Signal` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_clinic`
--

CREATE TABLE IF NOT EXISTS `sih_list_clinic` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_departments` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_clinic`
--

INSERT INTO `sih_list_clinic` (`id`, `code`, `name`, `id_departments`, `location`, `phone_number`, `note`, `created_at`, `stat`, `order`) VALUES
(1, 'mp8', 'phong 2', 4, 'd', '', 'note', '2018-08-22 06:40:45', 0, 1),
(2, 'mp2', 'phong 3', 11, 'khu 1', '090123123123', 'note 2', '2018-08-22 10:02:08', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_currencies`
--

CREATE TABLE IF NOT EXISTS `sih_list_currencies` (
  `id` int(11) NOT NULL,
  `code_from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `code_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `begin_date` datetime DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_currencies`
--

INSERT INTO `sih_list_currencies` (`id`, `code_from`, `rate`, `created_at`, `stat`, `code_to`, `begin_date`, `order`) VALUES
(1, 'USD 3', '20000', '2018-06-05 00:00:00', 1, 'USD2223', '0000-00-00 00:00:00', 1),
(2, 'USD h', '23000', '0000-00-00 00:00:00', 1, 'VND', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_departments`
--

CREATE TABLE IF NOT EXISTS `sih_list_departments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Khoa/phòng ban';

--
-- Dumping data for table `sih_list_departments`
--

INSERT INTO `sih_list_departments` (`id`, `name`, `code`, `created_at`, `stat`, `note`, `order`) VALUES
(1, 'Phòng ban 4', '2', '2018-05-21 00:00:00', 1, 'tesst note', 4),
(3, 'Phong ban test', '2', '2018-05-21 00:00:00', 1, NULL, 0),
(4, 'Phòng kế toán 2', '22', '2018-05-21 00:00:00', 1, NULL, 0),
(5, 'Phòng đa khoa', '2', '2018-05-21 00:00:00', 1, NULL, 0),
(6, 'Phòng hồi sức', '1', '2018-05-21 00:00:00', 1, NULL, 0),
(7, 'Phòng kế toán', '1', '2018-05-21 00:00:00', 0, NULL, 0),
(8, 'Phòng thu ngân', '1', '2018-05-21 00:00:00', 1, NULL, 0),
(9, 'Phòng nhập viện 1', '1', '2018-05-21 00:00:00', 0, NULL, 0),
(10, 'Phòng xuất viện', '1', '2018-05-21 00:00:00', 1, NULL, 0),
(11, 'Phòng y tế', '1', '2018-05-21 00:00:00', 1, NULL, 0),
(12, 'Phòng điều dưỡng', '1', '2018-05-21 00:00:00', 1, NULL, 0),
(13, 'Phòng công nghệ thông tin', '1', '2018-05-21 00:00:00', 1, NULL, 0),
(14, 'Phòng quản lý chất lượng', '1', '2018-05-21 00:00:00', 1, NULL, 0),
(15, 'Phòng hành chính quản trị', '1', '2018-05-21 00:00:00', 1, NULL, 0),
(16, 'Khoa Dị Ứng', '0', '2018-05-20 00:00:00', 1, NULL, 0),
(17, 'Khoa Chấn Thương Sọ Não', '1', '2018-05-21 00:00:00', 1, NULL, 0),
(18, 'Phòng hồi sức chức năng', '1', '2018-05-21 00:00:00', 1, NULL, 0),
(19, 'Phòng Răng hàm mặt', '1', '2018-05-21 00:00:00', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_districts`
--

CREATE TABLE IF NOT EXISTS `sih_list_districts` (
  `id` int(11) NOT NULL,
  `id_provinces` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user created',
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Quận/huyện';

--
-- Dumping data for table `sih_list_districts`
--

INSERT INTO `sih_list_districts` (`id`, `id_provinces`, `name`, `created_at`, `created_by`, `stat`, `order`, `code`) VALUES
(1, 2, 'Quan 1', '2018-04-16 02:10:17', '1', 1, 4, 'n1'),
(3, 2, 'Quận 2 Test', '0000-00-00 00:00:00', '1', 1, 2, NULL),
(4, 5, 'quan 1_1', '0000-00-00 00:00:00', '0', 1, 22, NULL),
(5, 4, 'Phường 2', '0000-00-00 00:00:00', '0', 1, 0, NULL),
(6, 4, 'Phuong 3', '0000-00-00 00:00:00', '0', 0, 1, NULL),
(7, 6, 'Phuong 4', '0000-00-00 00:00:00', '0', 1, 2, 'm2'),
(8, 6, 'Phuong 5', '0000-00-00 00:00:00', '0', 0, 3, 'c5'),
(9, 6, 'Phuong 6', '0000-00-00 00:00:00', '0', 0, 3, NULL),
(10, 5, 'Phuong 7', '0000-00-00 00:00:00', '0', 0, 3, NULL),
(11, 5, 'Phuong 8', '0000-00-00 00:00:00', '0', 0, 3, NULL),
(12, 6, 'Phuong 9', '0000-00-00 00:00:00', '0', 0, 3, NULL),
(13, 4, 'Phuong 10', '0000-00-00 00:00:00', '0', 0, 3, NULL),
(14, 5, 'Phuong 11', '0000-00-00 00:00:00', '0', 0, 3, NULL),
(15, 4, 'Phuong 12', '0000-00-00 00:00:00', '0', 0, 3, NULL),
(16, 4, 'Phuong 13', '0000-00-00 00:00:00', '0', 0, 3, NULL),
(17, 5, 'Phuong 14', '0000-00-00 00:00:00', '0', 0, 3, NULL),
(18, 2, 'Phuong 15-1', '0000-00-00 00:00:00', '0', 0, 3, NULL),
(19, 5, 'Phuong 16', '0000-00-00 00:00:00', '0', 0, 3, NULL),
(20, 0, '1', NULL, '', 2, 0, 'qw'),
(21, 0, 'eer', NULL, '', 0, 0, 'q2');

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_folks`
--

CREATE TABLE IF NOT EXISTS `sih_list_folks` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `stat` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_folks`
--

INSERT INTO `sih_list_folks` (`id`, `code`, `name`, `created_at`, `order`, `stat`) VALUES
(1, 'kinh tes', 'kinh e', '2018-08-21 06:08:53', 1, 1),
(2, 'km 2', 'Khơ me 1', '2018-08-21 06:09:07', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_group_therapys`
--

CREATE TABLE IF NOT EXISTS `sih_list_group_therapys` (
  `id` int(11) NOT NULL,
  `ma_kham` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Nhóm Khám Chữa Bệnh';

--
-- Dumping data for table `sih_list_group_therapys`
--

INSERT INTO `sih_list_group_therapys` (`id`, `ma_kham`, `name`, `date`, `stat`) VALUES
(1, 'ma 03', 'test-3', '2018-05-01 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_healthcare`
--

CREATE TABLE IF NOT EXISTS `sih_list_healthcare` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `healthcare_type_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_overtime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_in_hour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_holiday` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_foreigner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_healthcare`
--

INSERT INTO `sih_list_healthcare` (`id`, `code`, `healthcare_type_id`, `name`, `unit`, `price_overtime`, `price_in_hour`, `price_holiday`, `price_foreigner`, `note`, `created_at`, `stat`, `order`) VALUES
(1, 'm1', 4, 'nhom 1', 'lần', '8', '6', '10', '12', 'note', '0000-00-00 00:00:00', 1, 1),
(2, 'm2', 4, 'nhom 2', 'sóo', '6', '5', '7', '8', '', '0000-00-00 00:00:00', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_healthcare_group`
--

CREATE TABLE IF NOT EXISTS `sih_list_healthcare_group` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_healthcare_group`
--

INSERT INTO `sih_list_healthcare_group` (`id`, `code`, `name`, `created_at`, `stat`, `order`) VALUES
(1, 'm23', 'nhóm KCB 1,1', '0000-00-00 00:00:00', 1, 1),
(2, 'm2', 'nhóm KCB 2', '0000-00-00 00:00:00', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_healthcare_type`
--

CREATE TABLE IF NOT EXISTS `sih_list_healthcare_type` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `healthcare_group_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_healthcare_type`
--

INSERT INTO `sih_list_healthcare_type` (`id`, `code`, `name`, `healthcare_group_id`, `created_at`, `stat`, `order`) VALUES
(1, 'lkcb 1', 'loại khám cb 1', 2, '0000-00-00 00:00:00', 1, 1),
(2, 'lkcb 2', 'loại khám cb 2', 1, '0000-00-00 00:00:00', 0, 2),
(3, 'lkcb3', 'loại khám cb 3', 1, '2018-08-23 09:57:03', 1, 3),
(4, 'lkcb 3', 'loại khám 4-4', 2, '2018-08-23 09:57:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_materials`
--

CREATE TABLE IF NOT EXISTS `sih_list_materials` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `order` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh Mục Vật Tư';

--
-- Dumping data for table `sih_list_materials`
--

INSERT INTO `sih_list_materials` (`id`, `code`, `name`, `price`, `stat`, `created_at`, `order`) VALUES
(1, 'ma 02', 'quan ao2', '123', 1, '0000-00-00 00:00:00', 2),
(2, 'ma 01', 'ch', '12022', 0, '0000-00-00 00:00:00', 0),
(3, 'm2', 'test', '1222', 1, '0000-00-00 00:00:00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_midwives`
--

CREATE TABLE IF NOT EXISTS `sih_list_midwives` (
  `id` int(11) NOT NULL,
  `ma_ho_sinh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phong_ban` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Nữ Hộ Sinh';

--
-- Dumping data for table `sih_list_midwives`
--

INSERT INTO `sih_list_midwives` (`id`, `ma_ho_sinh`, `name`, `phong_ban`, `created_at`, `stat`) VALUES
(1, 'm12', 'chu thi c2', '5', '2018-05-07 00:00:00', 1),
(2, 'm2', 'Nu ho sinh', '9', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_nations`
--

CREATE TABLE IF NOT EXISTS `sih_list_nations` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_nations`
--

INSERT INTO `sih_list_nations` (`id`, `code`, `name`, `created_at`, `stat`, `order`) VALUES
(1, 'vn', 'Việt Nam test', '0000-00-00 00:00:00', 1, 4),
(2, 'hk', 'Hong kong', '2018-08-16 08:35:57', 1, 0),
(3, 'tq', 'Trung quoc', '2018-08-16 05:49:01', 1, 2),
(26, 'usa', 'Mi', '2018-08-16 08:36:04', 1, 0),
(27, 'sg', 'Sin ga pore', '2018-08-16 08:36:10', 1, 5),
(28, 'la', 'Lao', '2018-08-16 09:35:55', 1, 9),
(29, 'jsdf', 'estet 8', '2018-08-16 10:18:43', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_provinces`
--

CREATE TABLE IF NOT EXISTS `sih_list_provinces` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_nation` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Tỉnh thành';

--
-- Dumping data for table `sih_list_provinces`
--

INSERT INTO `sih_list_provinces` (`id`, `name`, `created_at`, `stat`, `order`, `code`, `id_nation`) VALUES
(2, 'Ho Chi  3zzd2', '1991-02-00 00:00:00', 1, 2, 'm2', 3),
(4, 'Vung tau 123dfd', '0000-00-00 00:00:00', 1, 1, 'vt1đ', 1),
(5, 'Tỉnh 2', '2018-02-12 00:00:00', 1, 2, '', 1),
(6, 'HK name 1', '2018-02-12 00:00:00', 1, 2, 'a2', 2),
(7, 'nghe an', '0000-00-00 00:00:00', 0, 2, 'na', 1),
(8, 'Tiền Giang', '0000-00-00 00:00:00', 1, 1, 'tg', 1),
(9, 'Hòa Bình', '0000-00-00 00:00:00', 0, 1, 'hb', 1),
(10, 'Hải Phòng', '0000-00-00 00:00:00', 1, 1, 'hp', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_reason_discharge`
--

CREATE TABLE IF NOT EXISTS `sih_list_reason_discharge` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_reason_discharge`
--

INSERT INTO `sih_list_reason_discharge` (`id`, `code`, `name`, `note`, `created_at`, `stat`, `order`) VALUES
(1, '2243', 'ly do 2', 'note 2222', '0000-00-00 00:00:00', 1, 1),
(2, '1', 'ly do 1', 'note 1', '0000-00-00 00:00:00', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_reason_transfer`
--

CREATE TABLE IF NOT EXISTS `sih_list_reason_transfer` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_reason_transfer`
--

INSERT INTO `sih_list_reason_transfer` (`id`, `code`, `name`, `note`, `created_at`, `stat`, `order`) VALUES
(1, 'm10', 'ma 112', 'bnnn', '0000-00-00 00:00:00', 1, 9),
(2, 'm23', 'ma 2', 'note', '0000-00-00 00:00:00', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_titles`
--

CREATE TABLE IF NOT EXISTS `sih_list_titles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Chức danh';

--
-- Dumping data for table `sih_list_titles`
--

INSERT INTO `sih_list_titles` (`id`, `name`, `created_at`, `code`, `stat`, `note`, `order`) VALUES
(1, 'Kỹ thuật 2', '2018-05-09 00:00:00', 'm2', 1, 'note tét change', 1),
(2, 'Phó khoa phẩu thuật', '2018-05-04 00:00:00', '1', 1, NULL, 1),
(3, 'Trưởng khoa nội trú', '2018-05-04 00:00:00', '1', 1, NULL, 1),
(4, 'Phó khoa nội trú', '2018-05-04 00:00:00', '1', 1, NULL, 1),
(5, 'Bác sỹ nội trú', '2018-05-04 00:00:00', '1', 1, NULL, 1),
(6, 'Bác sỹ phẫu thuật', '2018-05-04 00:00:00', '1', 1, NULL, 0),
(7, 'Giám đốc', '2018-05-04 00:00:00', '1', 1, NULL, 0),
(8, 'Phó giám đốc', '2018-05-04 00:00:00', 'm1', 1, NULL, 1),
(9, 'Tester', '2018-04-29 00:00:00', '1', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_towns`
--

CREATE TABLE IF NOT EXISTS `sih_list_towns` (
  `id` int(11) NOT NULL,
  `id_districts` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục phường/xã';

--
-- Dumping data for table `sih_list_towns`
--

INSERT INTO `sih_list_towns` (`id`, `id_districts`, `name`, `created_at`, `created_by`, `stat`, `order`) VALUES
(2, 1, 'phường cầu kho 6', '0000-00-00 00:00:00', '1', 1, 2),
(3, 1, 'phuwonfg 1', '0000-00-00 00:00:00', '1', 0, 2),
(4, 1, 'Phuong 121', '0000-00-00 00:00:00', '1', 0, 1),
(5, 1, 'Quan 6', '0000-00-00 00:00:00', '1', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_vendors`
--

CREATE TABLE IF NOT EXISTS `sih_list_vendors` (
  `id` int(11) NOT NULL,
  `vendor_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `second_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_quick` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_province` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_ncc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_settlement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_tax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_limit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `bank_account` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_modules` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_vendors`
--

INSERT INTO `sih_list_vendors` (`id`, `vendor_code`, `second_code`, `vendor_name`, `name_quick`, `address`, `city_province`, `nation`, `phone`, `fax`, `group_ncc`, `group_settlement`, `group_tax`, `curency_code`, `credit_limit`, `tax_code`, `email`, `website`, `contact`, `contact_phone`, `date_create`, `user`, `bank_account`, `bank_name`, `bank_address`, `bank_modules`) VALUES
(2, 't4', 't112333', 'nha cung cap 1', 'hhj', 'jhjhj', 'jjhj', 'hjhj', '8798', '8798', 'd', 'jh', 'jhj', 'hjh', 'jh', 'jh', 'jh@gasd.cio', 'jhkhj', 'jhjk', '89798', '0000-00-00 00:00:00', 0, 'jh', 'jhj', 'jhjh', '22'),
(3, '123', 'aq', 'nha cung cap 2', 'ds', 'ds2', '2123', '213', 'h', 'h', 'ádf', 'jj', 'jhj', 'jh', 'h', 'j', 'hjh', 'h', 'h', 'h', '0000-00-00 00:00:00', 0, 'hjh', 'j', 'dfas', 'jhg');

-- --------------------------------------------------------

--
-- Table structure for table `sih_reason_for_discharge`
--

CREATE TABLE IF NOT EXISTS `sih_reason_for_discharge` (
  `id` int(11) NOT NULL,
  `ma` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lý Do Xuất Viện';

--
-- Dumping data for table `sih_reason_for_discharge`
--

INSERT INTO `sih_reason_for_discharge` (`id`, `ma`, `name`, `note`, `date`, `stat`) VALUES
(1, '324324', 'kajsdf', 'hgh', '2018-04-03 00:00:00', 0),
(2, 'test 133', 'te3st', 'sdfd', '0000-00-00 00:00:00', 1),
(3, 'test2', 'te3st', 'sdfd', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_reason_for_transfer`
--

CREATE TABLE IF NOT EXISTS `sih_reason_for_transfer` (
  `id` int(11) NOT NULL,
  `ma` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lý Do Chuyển Viện';

--
-- Dumping data for table `sih_reason_for_transfer`
--

INSERT INTO `sih_reason_for_transfer` (`id`, `ma`, `name`, `note`, `date`, `stat`) VALUES
(1, 'ma23', 'test23', 'note1', '2018-05-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_roles`
--

CREATE TABLE IF NOT EXISTS `sih_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Quyền người dùng';

--
-- Dumping data for table `sih_roles`
--

INSERT INTO `sih_roles` (`id`, `name`, `created_at`, `created_by`, `stat`) VALUES
(1, 'Admin', '2018-06-18 00:00:00', '0', 1),
(2, 'Manage money 122', '2018-04-19 01:00:00', '1', 1),
(3, 'Group Role 1', '0000-00-00 00:00:00', '1', 0),
(4, 'Group Role 2', '0000-00-00 00:00:00', '1', 1),
(5, 'Group Role 32', '0000-00-00 00:00:00', '1', 1),
(6, 'Group Role 4', '0000-00-00 00:00:00', '1', 1),
(7, 'Group Role 5-1', '0000-00-00 00:00:00', '1', 1),
(8, 'Group Role 6', '0000-00-00 00:00:00', '1', 1),
(9, 'Group Role 7', '0000-00-00 00:00:00', '1', 1),
(10, 'Group Role 8', '0000-00-00 00:00:00', '1', 1),
(12, 'Test role2', '2018-07-03 00:00:00', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sih_roles_screens`
--

CREATE TABLE IF NOT EXISTS `sih_roles_screens` (
  `id` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_screens` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sih_role_screens`
--

CREATE TABLE IF NOT EXISTS `sih_role_screens` (
  `id` int(11) NOT NULL,
  `id_role` int(11) NOT NULL COMMENT 'Mã role',
  `id_screens` int(11) NOT NULL COMMENT 'Mã màn hình',
  `created_at` datetime NOT NULL COMMENT 'Thời điểm tạo',
  `created_by` int(11) NOT NULL COMMENT 'Người tạo',
  `stat` int(11) NOT NULL COMMENT 'O: Open; C: Close'
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng map quyền - màn hình';

--
-- Dumping data for table `sih_role_screens`
--

INSERT INTO `sih_role_screens` (`id`, `id_role`, `id_screens`, `created_at`, `created_by`, `stat`) VALUES
(8, 1, 1, '2018-04-20 11:24:41', 1, 1),
(9, 1, 4, '2018-04-20 11:24:52', 1, 1),
(10, 1, 7, '2018-04-20 11:28:15', 1, 1),
(11, 1, 6, '2018-04-20 11:34:58', 1, 1),
(12, 1, 5, '2018-04-20 11:37:35', 1, 1),
(13, 1, 3, '2018-04-20 11:41:13', 1, 1),
(14, 1, 2, '2018-04-20 11:41:15', 1, 1),
(15, 1, 8, '2018-04-20 12:06:50', 1, 1),
(16, 2, 6, '2018-04-20 12:06:55', 1, 1),
(17, 2, 1, '2018-04-20 12:07:00', 1, 1),
(18, 1, 12, '2018-04-20 12:11:21', 1, 1),
(19, 2, 2, '2018-04-23 06:12:19', 1, 1),
(20, 2, 3, '2018-04-23 06:12:21', 1, 1),
(21, 2, 4, '2018-04-23 06:12:22', 1, 1),
(22, 2, 5, '2018-04-23 06:12:22', 1, 1),
(23, 2, 7, '2018-04-23 06:12:23', 1, 1),
(24, 2, 8, '2018-04-23 06:12:24', 1, 1),
(25, 1, 13, '2018-04-23 08:23:55', 1, 1),
(26, 1, 16, '2018-04-23 09:21:29', 1, 1),
(27, 1, 15, '2018-04-23 09:21:29', 1, 1),
(28, 1, 14, '2018-04-23 09:21:30', 1, 1),
(29, 1, 17, '2018-04-23 10:10:55', 1, 1),
(30, 1, 20, '2018-04-23 10:12:50', 1, 1),
(31, 1, 18, '2018-04-23 10:13:33', 1, 1),
(32, 1, 11, '2018-04-23 10:22:38', 1, 1),
(33, 1, 19, '2018-04-23 11:00:07', 1, 1),
(34, 2, 17, '2018-04-23 11:05:07', 1, 1),
(35, 1, 21, '2018-04-24 11:54:27', 1, 1),
(36, 1, 22, '2018-04-24 11:54:40', 1, 1),
(37, 2, 34, '2018-04-24 12:37:12', 1, 1),
(38, 1, 28, '2018-04-26 04:57:16', 1, 1),
(39, 1, 29, '2018-04-26 04:57:46', 1, 1),
(40, 1, 35, '2018-04-26 04:58:03', 1, 1),
(41, 1, 36, '2018-04-26 04:58:04', 1, 1),
(42, 1, 42, '2018-04-26 05:25:39', 1, 1),
(43, 1, 43, '2018-04-26 05:25:52', 1, 1),
(44, 1, 44, '2018-04-26 05:28:59', 1, 1),
(45, 1, 45, '2018-04-26 05:29:00', 1, 1),
(46, 1, 46, '2018-04-26 05:29:00', 1, 1),
(47, 1, 47, '2018-04-26 05:29:01', 1, 1),
(48, 1, 48, '2018-04-26 05:59:48', 1, 1),
(49, 1, 49, '2018-04-26 06:01:35', 1, 1),
(50, 1, 50, '2018-04-26 06:03:10', 1, 1),
(51, 1, 51, '2018-04-26 06:03:10', 1, 1),
(52, 1, 34, '2018-04-26 06:03:51', 1, 1),
(53, 1, 33, '2018-04-26 06:03:51', 1, 1),
(54, 1, 31, '2018-04-26 06:03:56', 1, 1),
(55, 1, 32, '2018-04-26 06:03:56', 1, 1),
(56, 1, 30, '2018-04-26 06:03:56', 1, 1),
(57, 1, 27, '2018-04-26 06:03:58', 1, 1),
(58, 1, 26, '2018-04-26 06:03:59', 1, 1),
(59, 1, 24, '2018-04-26 06:04:00', 1, 1),
(60, 1, 25, '2018-04-26 06:04:01', 1, 1),
(61, 1, 23, '2018-04-26 06:04:01', 1, 1),
(62, 1, 41, '2018-04-26 06:04:06', 1, 1),
(63, 1, 40, '2018-04-26 06:04:07', 1, 1),
(64, 1, 38, '2018-04-26 06:04:08', 1, 1),
(65, 1, 39, '2018-04-26 06:04:08', 1, 1),
(66, 1, 37, '2018-04-26 06:04:09', 1, 1),
(67, 1, 52, '2018-04-26 11:03:54', 1, 1),
(68, 1, 53, '2018-05-02 09:26:16', 1, 1),
(69, 1, 54, '2018-05-02 09:26:17', 1, 1),
(70, 1, 56, '2018-05-02 09:26:18', 1, 1),
(71, 1, 57, '2018-05-02 09:26:19', 1, 1),
(72, 1, 55, '2018-05-02 09:26:19', 1, 1),
(73, 1, 58, '2018-05-02 09:26:20', 1, 1),
(74, 1, 59, '2018-05-02 09:26:20', 1, 1),
(75, 1, 63, '2018-05-03 12:16:29', 1, 1),
(76, 1, 62, '2018-05-03 12:24:52', 1, 1),
(77, 1, 60, '2018-05-03 12:24:54', 1, 1),
(78, 1, 61, '2018-05-03 12:25:04', 1, 1),
(79, 4, 2, '2018-05-07 09:34:45', 1, 1),
(80, 1, 65, '2018-05-09 10:04:21', 1, 1),
(81, 1, 64, '2018-05-09 10:15:40', 1, 1),
(82, 1, 66, '2018-05-09 10:30:28', 1, 1),
(83, 1, 67, '2018-05-09 10:52:10', 1, 1),
(84, 1, 68, '2018-05-09 10:52:12', 1, 1),
(85, 1, 69, '2018-05-09 10:52:13', 1, 1),
(86, 1, 70, '2018-05-09 10:52:15', 1, 1),
(87, 2, 21, '2018-05-11 12:38:12', 1, 1),
(88, 2, 22, '2018-05-11 12:38:13', 1, 1),
(89, 1, 71, '2018-05-21 07:14:22', 1, 1),
(90, 1, 72, '2018-05-21 07:14:22', 1, 1),
(91, 1, 73, '2018-05-21 07:14:23', 1, 1),
(92, 1, 74, '2018-05-21 07:14:24', 1, 1),
(93, 1, 75, '2018-05-21 07:14:25', 1, 1),
(94, 1, 76, '2018-05-21 07:14:26', 1, 1),
(95, 1, 77, '2018-05-21 07:14:27', 1, 1),
(96, 2, 15, '2018-05-25 12:34:59', 3, 1),
(97, 2, 20, '2018-05-25 12:35:00', 3, 1),
(98, 2, 18, '2018-05-25 12:35:01', 3, 1),
(99, 2, 61, '2018-05-25 12:40:06', 3, 1),
(100, 2, 43, '2018-05-25 12:40:06', 3, 1),
(101, 2, 64, '2018-05-25 12:40:08', 3, 1),
(102, 2, 65, '2018-05-25 12:40:08', 3, 1),
(103, 2, 28, '2018-05-25 12:40:18', 3, 1),
(104, 2, 29, '2018-05-25 12:40:18', 3, 1),
(105, 2, 35, '2018-05-25 12:40:19', 3, 1),
(106, 2, 36, '2018-05-25 12:40:19', 3, 1),
(107, 2, 53, '2018-05-25 12:40:20', 3, 1),
(108, 2, 54, '2018-05-25 12:40:20', 3, 1),
(109, 2, 71, '2018-05-25 12:40:21', 3, 1),
(110, 2, 72, '2018-05-25 12:40:21', 3, 1),
(112, 2, 16, '2018-05-25 12:48:48', 3, 1),
(113, 2, 62, '2018-05-25 12:59:46', 3, 1),
(114, 2, 60, '2018-05-25 13:00:30', 3, 1),
(115, 2, 13, '2018-05-25 13:35:43', 1, 1),
(116, 1, 106, '2018-05-29 10:15:12', 1, 1),
(117, 1, 107, '2018-05-29 10:15:12', 1, 1),
(118, 1, 99, '2018-05-29 10:15:13', 1, 1),
(119, 1, 100, '2018-05-29 10:15:13', 1, 1),
(120, 1, 85, '2018-05-29 10:15:14', 1, 1),
(121, 1, 86, '2018-05-29 10:15:14', 1, 1),
(122, 1, 78, '2018-05-29 10:15:16', 1, 1),
(123, 1, 79, '2018-05-29 10:15:16', 1, 1),
(124, 1, 92, '2018-05-29 10:16:41', 1, 1),
(125, 1, 93, '2018-05-29 10:16:42', 1, 1),
(126, 1, 94, '2018-05-29 10:16:44', 1, 1),
(127, 1, 87, '2018-05-29 10:16:48', 1, 1),
(128, 1, 80, '2018-05-29 10:16:50', 1, 1),
(129, 1, 101, '2018-05-29 10:16:51', 1, 1),
(130, 1, 108, '2018-05-29 10:16:51', 1, 1),
(131, 1, 81, '2018-05-30 08:26:44', 1, 1),
(132, 1, 88, '2018-05-30 08:26:46', 1, 1),
(133, 1, 95, '2018-05-30 08:26:47', 1, 1),
(134, 1, 102, '2018-05-30 08:26:47', 1, 1),
(135, 1, 109, '2018-05-30 08:26:48', 1, 1),
(136, 1, 82, '2018-05-30 08:26:49', 1, 1),
(137, 1, 89, '2018-05-30 08:26:50', 1, 1),
(138, 1, 96, '2018-05-30 08:26:51', 1, 1),
(139, 1, 103, '2018-05-30 08:26:52', 1, 1),
(140, 1, 110, '2018-05-30 08:26:52', 1, 1),
(141, 1, 83, '2018-05-30 08:26:53', 1, 1),
(142, 1, 90, '2018-05-30 08:26:54', 1, 1),
(143, 1, 97, '2018-05-30 08:26:57', 1, 1),
(144, 1, 104, '2018-05-30 08:26:58', 1, 1),
(145, 1, 84, '2018-05-30 08:26:58', 1, 1),
(146, 1, 111, '2018-05-30 08:26:58', 1, 1),
(147, 1, 91, '2018-05-30 08:26:59', 1, 1),
(148, 1, 98, '2018-05-30 08:27:00', 1, 1),
(149, 1, 105, '2018-05-30 08:27:00', 1, 1),
(150, 1, 112, '2018-05-30 08:27:01', 1, 1),
(151, 1, 113, '2018-06-04 12:16:24', 1, 1),
(152, 1, 119, '2018-06-04 12:16:24', 1, 1),
(153, 1, 114, '2018-06-04 12:16:25', 1, 1),
(154, 1, 115, '2018-06-04 12:16:26', 1, 1),
(155, 1, 116, '2018-06-04 12:16:27', 1, 1),
(156, 1, 117, '2018-06-04 12:16:27', 1, 1),
(157, 1, 118, '2018-06-04 12:16:28', 1, 1),
(158, 1, 120, '2018-06-05 09:55:46', 1, 1),
(159, 1, 123, '2018-06-05 09:55:46', 1, 1),
(160, 1, 121, '2018-06-05 09:55:46', 1, 1),
(161, 1, 122, '2018-06-05 09:55:47', 1, 1),
(162, 1, 124, '2018-06-05 09:55:48', 1, 1),
(163, 1, 125, '2018-06-05 09:55:49', 1, 1),
(164, 1, 126, '2018-06-05 09:55:50', 1, 1),
(165, 3, 13, '2018-06-20 05:17:29', 1, 1),
(166, 3, 2, '2018-06-20 05:17:29', 1, 1),
(167, 4, 13, '2018-06-22 11:58:44', 1, 1),
(168, 4, 15, '2018-06-22 11:58:45', 1, 1),
(169, 4, 20, '2018-06-22 11:58:45', 1, 1),
(170, 4, 16, '2018-06-22 11:58:46', 1, 1),
(171, 4, 17, '2018-06-22 11:58:46', 1, 1),
(172, 4, 21, '2018-06-22 11:58:47', 1, 1),
(173, 4, 22, '2018-06-22 11:58:47', 1, 1),
(174, 4, 28, '2018-06-22 11:58:48', 1, 1),
(175, 4, 29, '2018-06-22 11:58:48', 1, 1),
(176, 4, 35, '2018-06-22 11:58:54', 1, 1),
(177, 4, 36, '2018-06-22 11:58:54', 1, 1),
(178, 5, 16, '2018-06-22 11:58:57', 1, 1),
(179, 5, 17, '2018-06-22 11:58:57', 1, 1),
(180, 8, 21, '2018-06-29 09:14:52', 1, 1),
(181, 8, 22, '2018-06-29 09:14:52', 1, 1),
(182, 9, 21, '2018-06-29 09:14:55', 1, 1),
(183, 9, 22, '2018-06-29 09:14:55', 1, 1),
(184, 10, 28, '2018-06-29 09:14:58', 1, 1),
(185, 10, 29, '2018-06-29 09:14:58', 1, 1),
(186, 4, 18, '2018-08-06 12:38:32', 1, 1),
(187, 4, 48, '2018-08-06 12:38:38', 1, 1),
(188, 4, 53, '2018-08-06 12:38:41', 1, 1),
(189, 4, 54, '2018-08-06 12:38:41', 1, 1),
(190, 4, 61, '2018-08-06 12:38:43', 1, 1),
(191, 4, 43, '2018-08-06 12:38:43', 1, 1),
(192, 4, 64, '2018-08-06 12:38:45', 1, 1),
(193, 4, 65, '2018-08-06 12:38:45', 1, 1),
(194, 4, 4, '2018-08-06 12:38:48', 1, 1),
(195, 1, 129, '2018-08-21 06:03:54', 1, 1),
(196, 1, 130, '2018-08-21 06:03:54', 1, 1),
(197, 1, 131, '2018-08-21 06:03:55', 1, 1),
(198, 1, 132, '2018-08-21 06:04:01', 1, 1),
(199, 1, 133, '2018-08-21 06:04:03', 1, 1),
(200, 1, 134, '2018-08-21 06:04:06', 1, 1),
(201, 1, 135, '2018-08-21 06:04:08', 1, 1),
(202, 1, 136, '2018-08-22 06:29:58', 1, 1),
(203, 1, 137, '2018-08-22 06:29:58', 1, 1),
(204, 1, 138, '2018-08-22 06:30:00', 1, 1),
(205, 1, 139, '2018-08-22 06:30:02', 1, 1),
(206, 1, 140, '2018-08-22 06:30:04', 1, 1),
(207, 1, 141, '2018-08-22 06:30:06', 1, 1),
(208, 1, 142, '2018-08-22 06:30:08', 1, 1),
(209, 1, 143, '2018-08-22 08:43:37', 1, 1),
(210, 1, 146, '2018-08-22 08:43:38', 1, 1),
(211, 1, 150, '2018-08-22 08:43:39', 1, 1),
(212, 1, 151, '2018-08-22 08:43:39', 1, 1),
(213, 1, 144, '2018-08-22 08:43:41', 1, 1),
(214, 1, 152, '2018-08-22 08:43:43', 1, 1),
(215, 1, 153, '2018-08-22 08:43:45', 1, 1),
(216, 1, 145, '2018-08-22 08:43:48', 1, 1),
(217, 1, 149, '2018-08-22 08:43:50', 1, 1),
(218, 1, 154, '2018-08-22 08:43:52', 1, 1),
(219, 1, 155, '2018-08-22 08:43:54', 1, 1),
(220, 1, 147, '2018-08-22 08:43:56', 1, 1),
(221, 1, 148, '2018-08-22 08:43:59', 1, 1),
(222, 1, 156, '2018-08-22 08:44:01', 1, 1),
(223, 1, 157, '2018-08-23 09:25:28', 1, 1),
(224, 1, 160, '2018-08-23 09:25:28', 1, 1),
(225, 1, 158, '2018-08-23 09:25:30', 1, 1),
(226, 1, 159, '2018-08-23 09:25:32', 1, 1),
(227, 1, 161, '2018-08-23 09:25:34', 1, 1),
(228, 1, 162, '2018-08-23 09:25:36', 1, 1),
(229, 1, 163, '2018-08-23 09:25:38', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_screens`
--

CREATE TABLE IF NOT EXISTS `sih_screens` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `routed_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `ico` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh sách màn hình';

--
-- Dumping data for table `sih_screens`
--

INSERT INTO `sih_screens` (`id`, `title`, `route`, `routed_id`, `created_at`, `created_by`, `stat`, `ico`) VALUES
(2, 'permission_role-get', 'permission_role/get', '62_13', '2018-04-19 00:00:00', '1', 1, ''),
(3, 'permission_role-add', 'permission_role/add', '62_13', '2018-04-20 00:00:00', '1', 1, ''),
(4, 'permission_role-edit', 'permission_role/edit', '62_13', '2018-04-19 00:00:00', '1', 1, ''),
(5, 'permission_role-delete', 'permission_role/delete', '62_13', '2018-04-20 00:00:00', '1', 1, ''),
(6, 'permission_role-push', 'permission_role/push', '62_13', '2018-04-19 00:00:00', '1', 1, ''),
(7, 'permission_role-unpush', 'permission_role/unpush', '62_13', '2018-04-20 00:00:00', '1', 1, ''),
(13, 'Quản lý role', 'permission_role', '62', '2018-04-19 00:00:00', '1', 1, ''),
(14, 'Dashboard123', 'index', '0', '0000-00-00 00:00:00', '0', 1, 'fa fa-th'),
(15, 'Quản lý role và màn hình', 'permission_role_screen', '62', '0000-00-00 00:00:00', '0', 1, ''),
(16, 'Quản lý màn hình', 'permission_screen', '62', '0000-00-00 00:00:00', '0', 1, ''),
(17, 'permission_screen/get', 'permission_screen/get', '62_16', '0000-00-00 00:00:00', '0', 1, ''),
(18, 'permission_role_screen/edit', 'permission_role_screen/edit', '62_15', '0000-00-00 00:00:00', '0', 1, ''),
(19, 'permission_screen/add', 'permission_screen/add', '62_16', '0000-00-00 00:00:00', '0', 1, ''),
(20, 'permission_role_screen/get', 'permission_role_screen/get', '62_15', '0000-00-00 00:00:00', '0', 1, ''),
(21, 'Danh sách quốc gia', 'list_nations', '60', '0000-00-00 00:00:00', '0', 1, ''),
(22, 'list_nations/get', 'list_nations/get', '60_21', '0000-00-00 00:00:00', '0', 1, ''),
(23, 'list_nations/add', 'list_nations/add', '60_21', '0000-00-00 00:00:00', '0', 1, ''),
(24, 'list_nations/edit', 'list_nations/edit', '60_21', '0000-00-00 00:00:00', '0', 1, ''),
(25, 'list_nations/delete', 'list_nations/delete', '60_21', '0000-00-00 00:00:00', '0', 1, ''),
(26, 'list_nations/push', 'list_nations/push', '60_21', '0000-00-00 00:00:00', '0', 1, ''),
(27, 'list_nations/unpush', 'list_nations/unpush', '60_21', '0000-00-00 00:00:00', '0', 1, ''),
(28, 'Danh sách tỉnh thành', 'list_provinces', '60', '0000-00-00 00:00:00', '0', 1, ''),
(29, 'list_provinces/get', 'list_provinces/get', '60_28', '0000-00-00 00:00:00', '0', 1, ''),
(30, 'list_provinces/add', 'list_provinces/add', '60_28', '0000-00-00 00:00:00', '0', 1, ''),
(31, 'list_provinces/edit', 'list_provinces/edit', '60_28', '0000-00-00 00:00:00', '0', 1, ''),
(32, 'list_provinces/delete', 'list_provinces/delete', '60_28', '0000-00-00 00:00:00', '0', 1, ''),
(33, 'list_provinces/push', 'list_provinces/push', '60_28', '0000-00-00 00:00:00', '0', 1, ''),
(34, 'list_provinces/unpush', 'list_provinces/unpush', '60_28', '0000-00-00 00:00:00', '0', 1, ''),
(35, 'Danh sách quận huyện', 'list_districts', '60', '2018-05-17 00:00:00', '0', 1, ''),
(36, 'list_districts/get', 'list_districts/get', '60_35', '0000-00-00 00:00:00', '0', 1, ''),
(37, 'list_districts/add', 'list_districts/add', '60_35', '0000-00-00 00:00:00', '0', 1, ''),
(38, 'list_districts/edit', 'list_districts/edit', '60_35', '0000-00-00 00:00:00', '0', 1, ''),
(39, 'list_districts/delete', 'list_districts/delete', '60_35', '0000-00-00 00:00:00', '0', 1, ''),
(40, 'list_districts/push', 'list_districts/push', '60_35', '0000-00-00 00:00:00', '0', 1, ''),
(41, 'list_districts/unpush', 'list_districts/unpush', '60_35', '0000-00-00 00:00:00', '0', 1, ''),
(42, 'Quản lý nhân sự', '', '0', '0000-00-00 00:00:00', '0', 1, 'fa fa-users'),
(43, 'manage_user/get', 'manage_user/get', '42_61', '0000-00-00 00:00:00', '0', 1, ''),
(44, 'manage_user/add', 'manage_user/add', '42_61', '0000-00-00 00:00:00', '1', 1, ''),
(45, 'manage_user/edit', 'manage_user/edit', '42_61', '0000-00-00 00:00:00', '1', 1, ''),
(46, 'manage_user/delete', 'manage_user/delete', '42_61', '0000-00-00 00:00:00', '1', 1, ''),
(47, 'manage_user/push', 'manage_user/push', '42_61', '0000-00-00 00:00:00', '1', 1, ''),
(48, 'permission_screen/edit', 'permission_screen/edit', '62_16', '0000-00-00 00:00:00', '1', 1, ''),
(49, 'manage_user/unpush', 'manage_user/unpush', '42_61', '0000-00-00 00:00:00', '1', 1, ''),
(50, 'permission_screen/push', 'permission_screen/push', '62_16', '0000-00-00 00:00:00', '1', 1, ''),
(51, 'permission_screen/unpush', 'permission_screen/unpush', '62_16', '0000-00-00 00:00:00', '1', 1, ''),
(52, 'profile', 'profile', '0', '0000-00-00 00:00:00', '1', 1, ''),
(53, 'Danh sách phường xã', 'list_towns', '60', '2018-05-17 00:00:00', '1', 1, ''),
(54, 'Danh sách tỉnh thành get', 'list_towns/get', '60_53', '0000-00-00 00:00:00', '0', 1, ''),
(55, 'Danh sách tỉnh thành add', 'list_towns/add', '60_53', '0000-00-00 00:00:00', '0', 1, ''),
(56, 'Danh sách tỉnh thành edit', 'list_towns/edit', '60_53', '0000-00-00 00:00:00', '0', 1, ''),
(57, 'Danh sách tỉnh thành delete', 'list_towns/delete', '60_53', '0000-00-00 00:00:00', '0', 1, ''),
(58, 'Danh sách tỉnh thành push', 'list_towns/push', '60_53', '0000-00-00 00:00:00', '0', 1, ''),
(59, 'Danh sách tỉnh thành unpush', 'list_towns/unpush', '60_53', '0000-00-00 00:00:00', '0', 1, ''),
(60, 'Danh mục', '', '0', '0000-00-00 00:00:00', '1', 1, 'fa fa-th-list'),
(61, 'Danh sách nhân sự', 'manage_user', '42', '0000-00-00 00:00:00', '0', 1, ''),
(62, 'Quản lý phân quyền', '', '0', '0000-00-00 00:00:00', '0', 1, 'fa fa-users'),
(63, 'permission_screen/delete', 'permission_screen/delete', '62_16', '0000-00-00 00:00:00', '0', 1, ''),
(64, 'Danh sách chức vụ/nghề nghiệp', 'list_titles', '60', '0000-00-00 00:00:00', '1', 1, ''),
(65, 'Danh mục chức vụ get', 'list_titles/get', '60_64', '0000-00-00 00:00:00', '1', 1, ''),
(66, 'Danh mục chức vụ add', 'list_titles/add', '60_64', '0000-00-00 00:00:00', '1', 1, ''),
(67, 'Danh mục chức vụ edit', 'list_titles/edit', '60_64', '0000-00-00 00:00:00', '1', 1, ''),
(68, 'Danh mục chức vụ delete', 'list_titles/delete', '60_64', '0000-00-00 00:00:00', '1', 1, ''),
(69, 'Danh mục chức vụ push', 'list_titles/push', '60_64', '0000-00-00 00:00:00', '1', 1, ''),
(70, 'Danh mục chức vụ unpush', 'list_titles/unpush', '60_64', '0000-00-00 00:00:00', '1', 1, ''),
(71, 'Danh sách phòng ban', 'list_departments', '60', '2018-05-21 00:00:00', '1', 1, ''),
(72, 'Danh sách phòng ban/get', 'list_departments/get', '60_71', '2018-05-21 00:00:00', '1', 1, ''),
(73, 'Danh sách phòng ban/add', 'list_departments/add', '60_71', '2018-05-21 00:00:00', '1', 1, ''),
(74, 'Danh sách phòng ban/edit', 'list_departments/edit', '60_71', '2018-05-21 00:00:00', '1', 1, ''),
(75, 'Danh sách phòng ban/delete', 'list_departments/delete', '60_71', '2018-05-21 00:00:00', '1', 1, ''),
(76, 'Danh sách phòng ban/push', 'list_departments/push', '60_71', '2018-05-21 00:00:00', '1', 1, ''),
(77, 'Danh sách phòng ban/unpush', 'list_departments/unpush', '60_71', '2018-05-21 00:00:00', '1', 1, ''),
(78, 'Nữ Hộ Sinh', 'list_midwives', '60', '2018-05-29 00:00:00', '1', 1, ''),
(79, 'get', 'list_midwives/get', '60_78', '2018-05-29 00:00:00', '1', 1, ''),
(80, 'add', 'list_midwives/add', '60_78', '2018-05-29 00:00:00', '1', 1, ''),
(81, 'edit', 'list_midwives/edit', '60_78', '2018-05-29 00:00:00', '1', 1, ''),
(82, 'delete', 'list_midwives/delete', '60_78', '2018-05-29 00:00:00', '1', 1, ''),
(83, 'push', 'list_midwives/push', '60_78', '2018-05-29 00:00:00', '1', 1, ''),
(84, 'unpush', 'list_midwives/unpush', '60_78', '2018-05-29 00:00:00', '1', 1, ''),
(85, 'Danh Mục Vật Tư', 'list_materials', '60', '2018-05-29 00:00:00', '1', 1, ''),
(86, 'get', 'list_materials/get', '60_85', '2018-05-29 00:00:00', '1', 1, ''),
(87, 'add', 'list_materials/add', '60_85', '2018-05-29 00:00:00', '1', 1, ''),
(88, 'edit', 'list_materials/edit', '60_85', '2018-05-29 00:00:00', '1', 1, ''),
(89, 'delete', 'list_materials/delete', '60_85', '2018-05-29 00:00:00', '1', 1, ''),
(90, 'push', 'list_materials/push', '60_85', '2018-05-29 00:00:00', '1', 1, ''),
(91, 'unpush', 'list_materials/unpush', '60_85', '2018-05-29 00:00:00', '1', 1, ''),
(92, 'Nhóm Khám Chữa Bệnh', 'list_group_therapys', '60', '2018-05-29 00:00:00', '1', 1, ''),
(93, 'get', 'list_group_therapys/get', '60_92', '2018-05-29 00:00:00', '1', 1, ''),
(94, 'add', 'list_group_therapys/add', '60_92', '2018-05-29 00:00:00', '1', 1, ''),
(95, 'edit', 'list_group_therapys/edit', '60_92', '2018-05-29 00:00:00', '1', 1, ''),
(96, 'delete', 'list_group_therapys/delete', '60_92', '2018-05-29 00:00:00', '1', 1, ''),
(97, 'push', 'list_group_therapys/push', '60_92', '2018-05-29 00:00:00', '1', 1, ''),
(98, 'unpush', 'list_group_therapys/unpush', '60_92', '2018-05-29 00:00:00', '1', 1, ''),
(99, 'Lý Do Chuyển Viện', 'list_reason_transfer', '60', '2018-05-29 00:00:00', '1', 1, ''),
(100, 'get', 'list_reason_transfer/get', '60_99', '2018-05-29 00:00:00', '1', 1, ''),
(101, 'add', 'list_reason_transfer/add', '60_99', '2018-05-29 00:00:00', '1', 1, ''),
(102, 'edit', 'list_reason_transfer/edit', '60_99', '2018-05-29 00:00:00', '1', 1, ''),
(103, 'delete', 'list_reason_transfer/delete', '60_99', '2018-05-29 00:00:00', '1', 1, ''),
(104, 'push', 'list_reason_transfer/push', '60_99', '2018-05-29 00:00:00', '1', 1, ''),
(105, 'unpush', 'list_reason_transfer/unpush', '60_99', '2018-05-29 00:00:00', '1', 1, ''),
(106, 'Lý Do Xuất Viện', 'list_reason_discharge', '60', '2018-05-29 00:00:00', '1', 1, ''),
(107, 'get', 'list_reason_discharge/get', '60_106', '2018-05-29 00:00:00', '1', 1, ''),
(108, 'add', 'list_reason_discharge/add', '60_106', '2018-05-29 00:00:00', '1', 1, ''),
(109, 'edit', 'list_reason_discharge/edit', '60_106', '2018-05-29 00:00:00', '1', 1, ''),
(110, 'delete', 'list_reason_discharge/delete', '60_106', '2018-05-29 00:00:00', '1', 1, ''),
(111, 'push', 'list_reason_discharge/push', '60_106', '2018-05-29 00:00:00', '1', 1, ''),
(112, 'unpush', 'list_reason_discharge/unpush', '60_106', '2018-05-29 00:00:00', '1', 1, ''),
(113, 'Danh mục nhà cung cấp', 'list_vendors', '60', '2018-06-04 00:00:00', '1', 1, ''),
(114, 'add', 'list_vendors/add', '60_113', '2018-06-04 00:00:00', '1', 1, ''),
(115, 'edit', 'list_vendors/edit', '60_113', '2018-06-04 00:00:00', '1', 1, ''),
(116, 'delete', 'list_vendors/delete', '60_113', '2018-06-04 00:00:00', '1', 1, ''),
(117, 'push', 'list_vendors/push', '60_113', '2018-06-04 00:00:00', '1', 1, ''),
(118, 'unpush', 'list_vendors/unpush', '60_113', '2018-06-04 00:00:00', '1', 1, ''),
(119, 'get', 'list_vendors/get', '60_113', '2018-06-04 00:00:00', '1', 1, ''),
(120, 'Danh sách tiền tệ', 'list_currencies', '60', '2018-06-05 00:00:00', '1', 1, ''),
(121, 'add', 'list_currencies/add', '60_120', '2018-06-05 00:00:00', '1', 1, ''),
(122, 'edit', 'list_currencies/edit', '60_120', '2018-06-05 00:00:00', '1', 1, ''),
(123, 'get', 'list_currencies/get', '60_120', '2018-06-05 00:00:00', '1', 1, ''),
(124, 'delete', 'list_currencies/delete', '60_120', '2018-06-05 00:00:00', '1', 1, ''),
(125, 'push', 'list_currencies/push', '60_120', '2018-06-05 00:00:00', '1', 1, ''),
(126, 'unpush', 'list_currencies/unpush', '60_120', '2018-06-05 00:00:00', '1', 1, ''),
(127, 'tét', '8923', '123', '0000-00-00 00:00:00', '0', 1, ''),
(128, 'tét', '8923', '123', '0000-00-00 00:00:00', '0', 1, ''),
(129, 'Danh sách dân tộc', 'list_folks', '60', '0000-00-00 00:00:00', '1', 1, ''),
(130, 'list_folks/get', 'list_folks/get', '60_129', '0000-00-00 00:00:00', '1', 1, ''),
(131, 'list_folks/add', 'list_folks/add', '60_129', '0000-00-00 00:00:00', '1', 1, ''),
(132, 'list_folks/edit', 'list_folks/edit', '60_129', '0000-00-00 00:00:00', '1', 1, ''),
(133, 'list_folks/delete', 'list_folks/delete', '60_129', '0000-00-00 00:00:00', '1', 1, ''),
(134, 'list_folks/push', 'list_folks/push', '60_129', '0000-00-00 00:00:00', '1', 1, ''),
(135, 'list_folks/unpush', 'list_folks/unpush', '60_129', '0000-00-00 00:00:00', '1', 1, ''),
(136, 'Danh Mục Phòng Khám', 'list_clinic', '60', '0000-00-00 00:00:00', '1', 1, ''),
(137, 'list_clinic/get', 'list_clinic/get', '60_136', '0000-00-00 00:00:00', '1', 1, ''),
(138, 'list_clinic/add', 'list_clinic/add', '60_136', '0000-00-00 00:00:00', '1', 1, ''),
(139, 'list_clinic/delete', 'list_clinic/delete', '60_136', '0000-00-00 00:00:00', '1', 1, ''),
(140, 'list_clinic/push', 'list_clinic/push', '60_136', '0000-00-00 00:00:00', '1', 1, ''),
(141, 'list_clinic/unpush', 'list_clinic/unpush', '60_136', '0000-00-00 00:00:00', '1', 1, ''),
(142, 'list_clinic/edit', 'list_clinic/edit', '60_136', '0000-00-00 00:00:00', '1', 1, ''),
(143, 'Danh mục loại khám chữa bệnh', 'list_healthcare_type', '60', '0000-00-00 00:00:00', '1', 1, ''),
(144, 'list_healthcare_type/add', 'list_healthcare_type/add', '60_143', '0000-00-00 00:00:00', '1', 1, ''),
(145, 'list_healthcare_type/edit', 'list_healthcare_type/edit', '60_143', '0000-00-00 00:00:00', '1', 1, ''),
(146, 'list_healthcare_type/get', 'list_healthcare_type/get', '60_143', '0000-00-00 00:00:00', '1', 1, ''),
(147, 'list_healthcare_type/push', 'list_healthcare_type/push', '60_143', '0000-00-00 00:00:00', '1', 1, ''),
(148, 'list_healthcare_type/unpush', 'list_healthcare_type/unpush', '60_143', '0000-00-00 00:00:00', '1', 1, ''),
(149, 'list_healthcare_type/delete', 'list_healthcare_type/delete', '60_143', '0000-00-00 00:00:00', '1', 1, ''),
(150, 'Danh mục nhóm khám chữa bệnh', 'list_healthcare_group', '60', '0000-00-00 00:00:00', '1', 1, ''),
(151, 'list_healthcare_group/get', 'list_healthcare_group/get', '60_150', '0000-00-00 00:00:00', '1', 1, ''),
(152, 'list_healthcare_group/add', 'list_healthcare_group/add', '60_150', '0000-00-00 00:00:00', '1', 1, ''),
(153, 'list_healthcare_group/edit', 'list_healthcare_group/edit', '60_150', '0000-00-00 00:00:00', '1', 1, ''),
(154, 'list_healthcare_group/delete', 'list_healthcare_group/delete', '60_150', '0000-00-00 00:00:00', '1', 1, ''),
(155, 'list_healthcare_group/push', 'list_healthcare_group/push', '60_150', '0000-00-00 00:00:00', '1', 1, ''),
(156, 'list_healthcare_group/unpush', 'list_healthcare_group/unpush', '60_150', '0000-00-00 00:00:00', '1', 1, ''),
(157, 'Danh mục khám chữa bệnh', 'list_healthcare', '60', '0000-00-00 00:00:00', '1', 1, ''),
(158, 'list_healthcare/add', 'list_healthcare/add', '60_157', '0000-00-00 00:00:00', '1', 1, ''),
(159, 'list_healthcare/edit', 'list_healthcare/edit', '60_157', '0000-00-00 00:00:00', '1', 1, ''),
(160, 'list_healthcare/get', 'list_healthcare/get', '60_157', '0000-00-00 00:00:00', '1', 1, ''),
(161, 'list_healthcare/delete', 'list_healthcare/delete', '60_157', '0000-00-00 00:00:00', '1', 1, ''),
(162, 'list_healthcare/push', 'list_healthcare/push', '60_157', '0000-00-00 00:00:00', '1', 1, ''),
(163, 'list_healthcare/unpush', 'list_healthcare/unpush', '60_157', '0000-00-00 00:00:00', '1', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `sih_users`
--

CREATE TABLE IF NOT EXISTS `sih_users` (
  `id` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_list_departments` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_users`
--

INSERT INTO `sih_users` (`id`, `id_role`, `id_list_departments`, `name`, `email`, `phone`, `password`, `avatar`, `created_at`, `created_by`, `stat`) VALUES
(1, 1, 6, 'Admin John 3', 'john@ea.com', '10213123123', 'e10adc3949ba59abbe56e057f20f883e', 'uploads/avatas/1_15247335295.jpeg', '2018-02-09 00:00:00', '0', 1),
(2, 2, 2, 'Khoa 22', 'khoaxb2@gmail.com', '0987789987', 'fcea920f7412b5da7be0cf42b8c93759', '', '2017-11-28 14:27:01', '0', 1),
(3, 4, 4, 'testuser', 'ch2au@gmail.com', '1923123', 'e10adc3949ba59abbe56e057f20f883e', '', '0000-00-00 00:00:00', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_user_profiles`
--

CREATE TABLE IF NOT EXISTS `sih_user_profiles` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_list_departments` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relative_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro` longtext COLLATE utf8_unicode_ci NOT NULL,
  `degree` longtext COLLATE utf8_unicode_ci NOT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_user_profiles`
--

INSERT INTO `sih_user_profiles` (`id`, `id_user`, `id_list_departments`, `birthday`, `address`, `relative_contact`, `intro`, `degree`, `update_at`) VALUES
(1, 1, 0, '2000-09-18', '19 Phó Đức Chính', '', 'giới thiệu bằng cấp', '', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `list_healthcare_group`
--
ALTER TABLE `list_healthcare_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `list_healthcare_type`
--
ALTER TABLE `list_healthcare_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_form1`
--
ALTER TABLE `sih_form1`
  ADD PRIMARY KEY (`f1Id`);

--
-- Indexes for table `sih_list_clinic`
--
ALTER TABLE `sih_list_clinic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_currencies`
--
ALTER TABLE `sih_list_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_departments`
--
ALTER TABLE `sih_list_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_districts`
--
ALTER TABLE `sih_list_districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_folks`
--
ALTER TABLE `sih_list_folks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_group_therapys`
--
ALTER TABLE `sih_list_group_therapys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_healthcare`
--
ALTER TABLE `sih_list_healthcare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_healthcare_group`
--
ALTER TABLE `sih_list_healthcare_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_healthcare_type`
--
ALTER TABLE `sih_list_healthcare_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_materials`
--
ALTER TABLE `sih_list_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_midwives`
--
ALTER TABLE `sih_list_midwives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_nations`
--
ALTER TABLE `sih_list_nations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_provinces`
--
ALTER TABLE `sih_list_provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_reason_discharge`
--
ALTER TABLE `sih_list_reason_discharge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_reason_transfer`
--
ALTER TABLE `sih_list_reason_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_titles`
--
ALTER TABLE `sih_list_titles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_towns`
--
ALTER TABLE `sih_list_towns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_list_vendors`
--
ALTER TABLE `sih_list_vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_reason_for_discharge`
--
ALTER TABLE `sih_reason_for_discharge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_reason_for_transfer`
--
ALTER TABLE `sih_reason_for_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_roles`
--
ALTER TABLE `sih_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_roles_screens`
--
ALTER TABLE `sih_roles_screens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_role_screens`
--
ALTER TABLE `sih_role_screens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_screens`
--
ALTER TABLE `sih_screens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_users`
--
ALTER TABLE `sih_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_user_profiles`
--
ALTER TABLE `sih_user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `list_healthcare_group`
--
ALTER TABLE `list_healthcare_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `list_healthcare_type`
--
ALTER TABLE `list_healthcare_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sih_form1`
--
ALTER TABLE `sih_form1`
  MODIFY `f1Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sih_list_clinic`
--
ALTER TABLE `sih_list_clinic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sih_list_currencies`
--
ALTER TABLE `sih_list_currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sih_list_departments`
--
ALTER TABLE `sih_list_departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `sih_list_districts`
--
ALTER TABLE `sih_list_districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `sih_list_folks`
--
ALTER TABLE `sih_list_folks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sih_list_group_therapys`
--
ALTER TABLE `sih_list_group_therapys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sih_list_healthcare`
--
ALTER TABLE `sih_list_healthcare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sih_list_healthcare_group`
--
ALTER TABLE `sih_list_healthcare_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sih_list_healthcare_type`
--
ALTER TABLE `sih_list_healthcare_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sih_list_materials`
--
ALTER TABLE `sih_list_materials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sih_list_midwives`
--
ALTER TABLE `sih_list_midwives`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sih_list_nations`
--
ALTER TABLE `sih_list_nations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `sih_list_provinces`
--
ALTER TABLE `sih_list_provinces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sih_list_reason_discharge`
--
ALTER TABLE `sih_list_reason_discharge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sih_list_reason_transfer`
--
ALTER TABLE `sih_list_reason_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sih_list_titles`
--
ALTER TABLE `sih_list_titles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sih_list_towns`
--
ALTER TABLE `sih_list_towns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sih_list_vendors`
--
ALTER TABLE `sih_list_vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sih_reason_for_discharge`
--
ALTER TABLE `sih_reason_for_discharge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sih_reason_for_transfer`
--
ALTER TABLE `sih_reason_for_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sih_roles`
--
ALTER TABLE `sih_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `sih_roles_screens`
--
ALTER TABLE `sih_roles_screens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sih_role_screens`
--
ALTER TABLE `sih_role_screens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=230;
--
-- AUTO_INCREMENT for table `sih_screens`
--
ALTER TABLE `sih_screens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=164;
--
-- AUTO_INCREMENT for table `sih_users`
--
ALTER TABLE `sih_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sih_user_profiles`
--
ALTER TABLE `sih_user_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
