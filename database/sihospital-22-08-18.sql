-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: projectsky_20180823
-- ------------------------------------------------------
-- Server version	5.6.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `list_healthcare_group`
--

DROP TABLE IF EXISTS `list_healthcare_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_healthcare_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `list_healthcare_group`
--

LOCK TABLES `list_healthcare_group` WRITE;
/*!40000 ALTER TABLE `list_healthcare_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_healthcare_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `list_healthcare_type`
--

DROP TABLE IF EXISTS `list_healthcare_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_healthcare_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  `healthcare_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `list_healthcare_type`
--

LOCK TABLES `list_healthcare_type` WRITE;
/*!40000 ALTER TABLE `list_healthcare_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_healthcare_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_form1`
--

DROP TABLE IF EXISTS `sih_form1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_form1` (
  `f1Id` int(11) NOT NULL AUTO_INCREMENT,
  `f1MaPhieu` int(11) DEFAULT NULL,
  `f1MaBenhNhan` int(11) DEFAULT NULL,
  `f1Child` tinyint(1) DEFAULT NULL,
  `f1Name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `f1Birthday` date NOT NULL,
  `f1Mari` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1Gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `f1Nation` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `f1CMND` int(9) DEFAULT NULL,
  `f1Address` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1Email` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1Homephone` int(20) DEFAULT NULL,
  `f1Mobile` int(20) DEFAULT NULL,
  `f1EmerFullName` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1EmerRelation` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1EmerPhone` int(20) DEFAULT NULL,
  `f1EmerAddress` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1PrivaInsuran` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1HavePrivaInsuran` tinyint(1) DEFAULT NULL,
  `f1OrderNameCompany` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1OrderAddressCompany` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1OrderMST` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1DateCreate` date NOT NULL,
  `f1Signal` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`f1Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_form1`
--

LOCK TABLES `sih_form1` WRITE;
/*!40000 ALTER TABLE `sih_form1` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_form1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_clinic`
--

DROP TABLE IF EXISTS `sih_list_clinic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_clinic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_departments` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_clinic`
--

LOCK TABLES `sih_list_clinic` WRITE;
/*!40000 ALTER TABLE `sih_list_clinic` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_list_clinic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_currencies`
--

DROP TABLE IF EXISTS `sih_list_currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `code_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `begin_date` datetime DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_currencies`
--

LOCK TABLES `sih_list_currencies` WRITE;
/*!40000 ALTER TABLE `sih_list_currencies` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_list_currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_departments`
--

DROP TABLE IF EXISTS `sih_list_departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Khoa/phòng ban';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_departments`
--

LOCK TABLES `sih_list_departments` WRITE;
/*!40000 ALTER TABLE `sih_list_departments` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_list_departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_districts`
--

DROP TABLE IF EXISTS `sih_list_districts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_provinces` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user created',
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Quận/huyện';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_districts`
--

LOCK TABLES `sih_list_districts` WRITE;
/*!40000 ALTER TABLE `sih_list_districts` DISABLE KEYS */;
INSERT INTO `sih_list_districts` VALUES (1,1,'Quận 1',NULL,'',1,0,'Quận 1');
/*!40000 ALTER TABLE `sih_list_districts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_folks`
--

DROP TABLE IF EXISTS `sih_list_folks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_folks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `stat` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_folks`
--

LOCK TABLES `sih_list_folks` WRITE;
/*!40000 ALTER TABLE `sih_list_folks` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_list_folks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_group_therapys`
--

DROP TABLE IF EXISTS `sih_list_group_therapys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_group_therapys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ma_kham` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Nhóm Khám Chữa Bệnh';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_group_therapys`
--

LOCK TABLES `sih_list_group_therapys` WRITE;
/*!40000 ALTER TABLE `sih_list_group_therapys` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_list_group_therapys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_healthcare_group`
--

DROP TABLE IF EXISTS `sih_list_healthcare_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_healthcare_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_healthcare_group`
--

LOCK TABLES `sih_list_healthcare_group` WRITE;
/*!40000 ALTER TABLE `sih_list_healthcare_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_list_healthcare_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_healthcare_type`
--

DROP TABLE IF EXISTS `sih_list_healthcare_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_healthcare_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `healthcare_group_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_healthcare_type`
--

LOCK TABLES `sih_list_healthcare_type` WRITE;
/*!40000 ALTER TABLE `sih_list_healthcare_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_list_healthcare_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_materials`
--

DROP TABLE IF EXISTS `sih_list_materials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_materials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh Mục Vật Tư';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_materials`
--

LOCK TABLES `sih_list_materials` WRITE;
/*!40000 ALTER TABLE `sih_list_materials` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_list_materials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_midwives`
--

DROP TABLE IF EXISTS `sih_list_midwives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_midwives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ma_ho_sinh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phong_ban` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Nữ Hộ Sinh';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_midwives`
--

LOCK TABLES `sih_list_midwives` WRITE;
/*!40000 ALTER TABLE `sih_list_midwives` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_list_midwives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_nations`
--

DROP TABLE IF EXISTS `sih_list_nations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_nations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_nations`
--

LOCK TABLES `sih_list_nations` WRITE;
/*!40000 ALTER TABLE `sih_list_nations` DISABLE KEYS */;
INSERT INTO `sih_list_nations` VALUES (1,'QG1','Việt nam','2018-08-23 11:34:57',1,1);
/*!40000 ALTER TABLE `sih_list_nations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_provinces`
--

DROP TABLE IF EXISTS `sih_list_provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_nation` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Tỉnh thành';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_provinces`
--

LOCK TABLES `sih_list_provinces` WRITE;
/*!40000 ALTER TABLE `sih_list_provinces` DISABLE KEYS */;
INSERT INTO `sih_list_provinces` VALUES (1,'HỒ Chí Minh','0000-00-00 00:00:00',NULL,1,1,'Tp1',1);
/*!40000 ALTER TABLE `sih_list_provinces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_titles`
--

DROP TABLE IF EXISTS `sih_list_titles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_titles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Chức danh';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_titles`
--

LOCK TABLES `sih_list_titles` WRITE;
/*!40000 ALTER TABLE `sih_list_titles` DISABLE KEYS */;
INSERT INTO `sih_list_titles` VALUES (1,'Bác sĩ','0000-00-00 00:00:00','Nghề 1',1,'',1),(2,'Y tá','0000-00-00 00:00:00','Nghề 2',1,'hỗ trợ bác sĩ',2);
/*!40000 ALTER TABLE `sih_list_titles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_towns`
--

DROP TABLE IF EXISTS `sih_list_towns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_towns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_districts` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'filter when show',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục phường/xã';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_towns`
--

LOCK TABLES `sih_list_towns` WRITE;
/*!40000 ALTER TABLE `sih_list_towns` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_list_towns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_list_vendors`
--

DROP TABLE IF EXISTS `sih_list_vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_list_vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `second_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_quick` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_province` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_ncc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_settlement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_tax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_limit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `bank_account` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_modules` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_list_vendors`
--

LOCK TABLES `sih_list_vendors` WRITE;
/*!40000 ALTER TABLE `sih_list_vendors` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_list_vendors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_reason_for_discharge`
--

DROP TABLE IF EXISTS `sih_reason_for_discharge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_reason_for_discharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ma` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lý Do Xuất Viện';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_reason_for_discharge`
--

LOCK TABLES `sih_reason_for_discharge` WRITE;
/*!40000 ALTER TABLE `sih_reason_for_discharge` DISABLE KEYS */;
INSERT INTO `sih_reason_for_discharge` VALUES (1,'324324','kajsdf','hgh','2018-04-03 00:00:00',0),(2,'test 133','te3st','sdfd','0000-00-00 00:00:00',1),(3,'test2','te3st','sdfd','0000-00-00 00:00:00',1);
/*!40000 ALTER TABLE `sih_reason_for_discharge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_reason_for_transfer`
--

DROP TABLE IF EXISTS `sih_reason_for_transfer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_reason_for_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ma` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lý Do Chuyển Viện';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_reason_for_transfer`
--

LOCK TABLES `sih_reason_for_transfer` WRITE;
/*!40000 ALTER TABLE `sih_reason_for_transfer` DISABLE KEYS */;
INSERT INTO `sih_reason_for_transfer` VALUES (1,'ma23','test23','note1','2018-05-01 00:00:00',1);
/*!40000 ALTER TABLE `sih_reason_for_transfer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_role_screens`
--

DROP TABLE IF EXISTS `sih_role_screens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_role_screens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) NOT NULL COMMENT 'Mã role',
  `id_screens` int(11) NOT NULL COMMENT 'Mã màn hình',
  `created_at` datetime NOT NULL COMMENT 'Thời điểm tạo',
  `created_by` int(11) NOT NULL COMMENT 'Người tạo',
  `stat` int(11) NOT NULL COMMENT 'O: Open; C: Close',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng map quyền - màn hình';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_role_screens`
--

LOCK TABLES `sih_role_screens` WRITE;
/*!40000 ALTER TABLE `sih_role_screens` DISABLE KEYS */;
INSERT INTO `sih_role_screens` VALUES (8,1,1,'2018-04-20 11:24:41',1,1),(9,1,4,'2018-04-20 11:24:52',1,1),(10,1,7,'2018-04-20 11:28:15',1,1),(11,1,6,'2018-04-20 11:34:58',1,1),(12,1,5,'2018-04-20 11:37:35',1,1),(13,1,3,'2018-04-20 11:41:13',1,1),(14,1,2,'2018-04-20 11:41:15',1,1),(15,1,8,'2018-04-20 12:06:50',1,1),(16,2,6,'2018-04-20 12:06:55',1,1),(17,2,1,'2018-04-20 12:07:00',1,1),(18,1,12,'2018-04-20 12:11:21',1,1),(19,2,2,'2018-04-23 06:12:19',1,1),(20,2,3,'2018-04-23 06:12:21',1,1),(21,2,4,'2018-04-23 06:12:22',1,1),(22,2,5,'2018-04-23 06:12:22',1,1),(23,2,7,'2018-04-23 06:12:23',1,1),(24,2,8,'2018-04-23 06:12:24',1,1),(25,1,13,'2018-04-23 08:23:55',1,1),(26,1,16,'2018-04-23 09:21:29',1,1),(27,1,15,'2018-04-23 09:21:29',1,1),(28,1,14,'2018-04-23 09:21:30',1,1),(29,1,17,'2018-04-23 10:10:55',1,1),(30,1,20,'2018-04-23 10:12:50',1,1),(31,1,18,'2018-04-23 10:13:33',1,1),(32,1,11,'2018-04-23 10:22:38',1,1),(33,1,19,'2018-04-23 11:00:07',1,1),(34,2,17,'2018-04-23 11:05:07',1,1),(35,1,21,'2018-04-24 11:54:27',1,1),(36,1,22,'2018-04-24 11:54:40',1,1),(37,2,34,'2018-04-24 12:37:12',1,1),(38,1,28,'2018-04-26 04:57:16',1,1),(39,1,29,'2018-04-26 04:57:46',1,1),(40,1,35,'2018-04-26 04:58:03',1,1),(41,1,36,'2018-04-26 04:58:04',1,1),(42,1,42,'2018-04-26 05:25:39',1,1),(43,1,43,'2018-04-26 05:25:52',1,1),(44,1,44,'2018-04-26 05:28:59',1,1),(45,1,45,'2018-04-26 05:29:00',1,1),(46,1,46,'2018-04-26 05:29:00',1,1),(47,1,47,'2018-04-26 05:29:01',1,1),(48,1,48,'2018-04-26 05:59:48',1,1),(49,1,49,'2018-04-26 06:01:35',1,1),(50,1,50,'2018-04-26 06:03:10',1,1),(51,1,51,'2018-04-26 06:03:10',1,1),(52,1,34,'2018-04-26 06:03:51',1,1),(53,1,33,'2018-04-26 06:03:51',1,1),(54,1,31,'2018-04-26 06:03:56',1,1),(55,1,32,'2018-04-26 06:03:56',1,1),(56,1,30,'2018-04-26 06:03:56',1,1),(57,1,27,'2018-04-26 06:03:58',1,1),(58,1,26,'2018-04-26 06:03:59',1,1),(59,1,24,'2018-04-26 06:04:00',1,1),(60,1,25,'2018-04-26 06:04:01',1,1),(61,1,23,'2018-04-26 06:04:01',1,1),(62,1,41,'2018-04-26 06:04:06',1,1),(63,1,40,'2018-04-26 06:04:07',1,1),(64,1,38,'2018-04-26 06:04:08',1,1),(65,1,39,'2018-04-26 06:04:08',1,1),(66,1,37,'2018-04-26 06:04:09',1,1),(67,1,52,'2018-04-26 11:03:54',1,1),(68,1,53,'2018-05-02 09:26:16',1,1),(69,1,54,'2018-05-02 09:26:17',1,1),(70,1,56,'2018-05-02 09:26:18',1,1),(71,1,57,'2018-05-02 09:26:19',1,1),(72,1,55,'2018-05-02 09:26:19',1,1),(73,1,58,'2018-05-02 09:26:20',1,1),(74,1,59,'2018-05-02 09:26:20',1,1),(75,1,63,'2018-05-03 12:16:29',1,1),(76,1,62,'2018-05-03 12:24:52',1,1),(77,1,60,'2018-05-03 12:24:54',1,1),(78,1,61,'2018-05-03 12:25:04',1,1),(79,4,2,'2018-05-07 09:34:45',1,1),(80,1,65,'2018-05-09 10:04:21',1,1),(81,1,64,'2018-05-09 10:15:40',1,1),(82,1,66,'2018-05-09 10:30:28',1,1),(83,1,67,'2018-05-09 10:52:10',1,1),(84,1,68,'2018-05-09 10:52:12',1,1),(85,1,69,'2018-05-09 10:52:13',1,1),(86,1,70,'2018-05-09 10:52:15',1,1),(87,2,21,'2018-05-11 12:38:12',1,1),(88,2,22,'2018-05-11 12:38:13',1,1),(89,1,71,'2018-05-21 07:14:22',1,1),(90,1,72,'2018-05-21 07:14:22',1,1),(91,1,73,'2018-05-21 07:14:23',1,1),(92,1,74,'2018-05-21 07:14:24',1,1),(93,1,75,'2018-05-21 07:14:25',1,1),(94,1,76,'2018-05-21 07:14:26',1,1),(95,1,77,'2018-05-21 07:14:27',1,1),(96,2,15,'2018-05-25 12:34:59',3,1),(97,2,20,'2018-05-25 12:35:00',3,1),(98,2,18,'2018-05-25 12:35:01',3,1),(99,2,61,'2018-05-25 12:40:06',3,1),(100,2,43,'2018-05-25 12:40:06',3,1),(101,2,64,'2018-05-25 12:40:08',3,1),(102,2,65,'2018-05-25 12:40:08',3,1),(103,2,28,'2018-05-25 12:40:18',3,1),(104,2,29,'2018-05-25 12:40:18',3,1),(105,2,35,'2018-05-25 12:40:19',3,1),(106,2,36,'2018-05-25 12:40:19',3,1),(107,2,53,'2018-05-25 12:40:20',3,1),(108,2,54,'2018-05-25 12:40:20',3,1),(109,2,71,'2018-05-25 12:40:21',3,1),(110,2,72,'2018-05-25 12:40:21',3,1),(112,2,16,'2018-05-25 12:48:48',3,1),(113,2,62,'2018-05-25 12:59:46',3,1),(114,2,60,'2018-05-25 13:00:30',3,1),(115,2,13,'2018-05-25 13:35:43',1,1),(116,1,106,'2018-05-29 10:15:12',1,1),(117,1,107,'2018-05-29 10:15:12',1,1),(118,1,99,'2018-05-29 10:15:13',1,1),(119,1,100,'2018-05-29 10:15:13',1,1),(120,1,85,'2018-05-29 10:15:14',1,1),(121,1,86,'2018-05-29 10:15:14',1,1),(122,1,78,'2018-05-29 10:15:16',1,1),(123,1,79,'2018-05-29 10:15:16',1,1),(124,1,92,'2018-05-29 10:16:41',1,1),(125,1,93,'2018-05-29 10:16:42',1,1),(126,1,94,'2018-05-29 10:16:44',1,1),(127,1,87,'2018-05-29 10:16:48',1,1),(128,1,80,'2018-05-29 10:16:50',1,1),(129,1,101,'2018-05-29 10:16:51',1,1),(130,1,108,'2018-05-29 10:16:51',1,1),(131,1,81,'2018-05-30 08:26:44',1,1),(132,1,88,'2018-05-30 08:26:46',1,1),(133,1,95,'2018-05-30 08:26:47',1,1),(134,1,102,'2018-05-30 08:26:47',1,1),(135,1,109,'2018-05-30 08:26:48',1,1),(136,1,82,'2018-05-30 08:26:49',1,1),(137,1,89,'2018-05-30 08:26:50',1,1),(138,1,96,'2018-05-30 08:26:51',1,1),(139,1,103,'2018-05-30 08:26:52',1,1),(140,1,110,'2018-05-30 08:26:52',1,1),(141,1,83,'2018-05-30 08:26:53',1,1),(142,1,90,'2018-05-30 08:26:54',1,1),(143,1,97,'2018-05-30 08:26:57',1,1),(144,1,104,'2018-05-30 08:26:58',1,1),(145,1,84,'2018-05-30 08:26:58',1,1),(146,1,111,'2018-05-30 08:26:58',1,1),(147,1,91,'2018-05-30 08:26:59',1,1),(148,1,98,'2018-05-30 08:27:00',1,1),(149,1,105,'2018-05-30 08:27:00',1,1),(150,1,112,'2018-05-30 08:27:01',1,1),(151,1,113,'2018-06-04 12:16:24',1,1),(152,1,119,'2018-06-04 12:16:24',1,1),(153,1,114,'2018-06-04 12:16:25',1,1),(154,1,115,'2018-06-04 12:16:26',1,1),(155,1,116,'2018-06-04 12:16:27',1,1),(156,1,117,'2018-06-04 12:16:27',1,1),(157,1,118,'2018-06-04 12:16:28',1,1),(158,1,120,'2018-06-05 09:55:46',1,1),(159,1,123,'2018-06-05 09:55:46',1,1),(160,1,121,'2018-06-05 09:55:46',1,1),(161,1,122,'2018-06-05 09:55:47',1,1),(162,1,124,'2018-06-05 09:55:48',1,1),(163,1,125,'2018-06-05 09:55:49',1,1),(164,1,126,'2018-06-05 09:55:50',1,1),(165,3,13,'2018-06-20 05:17:29',1,1),(166,3,2,'2018-06-20 05:17:29',1,1),(167,4,13,'2018-06-22 11:58:44',1,1),(168,4,15,'2018-06-22 11:58:45',1,1),(169,4,20,'2018-06-22 11:58:45',1,1),(170,4,16,'2018-06-22 11:58:46',1,1),(171,4,17,'2018-06-22 11:58:46',1,1),(172,4,21,'2018-06-22 11:58:47',1,1),(173,4,22,'2018-06-22 11:58:47',1,1),(174,4,28,'2018-06-22 11:58:48',1,1),(175,4,29,'2018-06-22 11:58:48',1,1),(176,4,35,'2018-06-22 11:58:54',1,1),(177,4,36,'2018-06-22 11:58:54',1,1),(178,5,16,'2018-06-22 11:58:57',1,1),(179,5,17,'2018-06-22 11:58:57',1,1),(180,8,21,'2018-06-29 09:14:52',1,1),(181,8,22,'2018-06-29 09:14:52',1,1),(182,9,21,'2018-06-29 09:14:55',1,1),(183,9,22,'2018-06-29 09:14:55',1,1),(184,10,28,'2018-06-29 09:14:58',1,1),(185,10,29,'2018-06-29 09:14:58',1,1),(186,4,18,'2018-08-06 12:38:32',1,1),(187,4,48,'2018-08-06 12:38:38',1,1),(188,4,53,'2018-08-06 12:38:41',1,1),(189,4,54,'2018-08-06 12:38:41',1,1),(190,4,61,'2018-08-06 12:38:43',1,1),(191,4,43,'2018-08-06 12:38:43',1,1),(192,4,64,'2018-08-06 12:38:45',1,1),(193,4,65,'2018-08-06 12:38:45',1,1),(194,4,4,'2018-08-06 12:38:48',1,1),(195,1,129,'2018-08-21 06:03:54',1,1),(196,1,130,'2018-08-21 06:03:54',1,1),(197,1,131,'2018-08-21 06:03:55',1,1),(198,1,132,'2018-08-21 06:04:01',1,1),(199,1,133,'2018-08-21 06:04:03',1,1),(200,1,134,'2018-08-21 06:04:06',1,1),(201,1,135,'2018-08-21 06:04:08',1,1),(202,1,136,'2018-08-22 06:29:58',1,1),(203,1,137,'2018-08-22 06:29:58',1,1),(204,1,138,'2018-08-22 06:30:00',1,1),(205,1,139,'2018-08-22 06:30:02',1,1),(206,1,140,'2018-08-22 06:30:04',1,1),(207,1,141,'2018-08-22 06:30:06',1,1),(208,1,142,'2018-08-22 06:30:08',1,1),(209,1,143,'2018-08-22 08:43:37',1,1),(210,1,146,'2018-08-22 08:43:38',1,1),(211,1,150,'2018-08-22 08:43:39',1,1),(212,1,151,'2018-08-22 08:43:39',1,1),(213,1,144,'2018-08-22 08:43:41',1,1),(214,1,152,'2018-08-22 08:43:43',1,1),(215,1,153,'2018-08-22 08:43:45',1,1),(216,1,145,'2018-08-22 08:43:48',1,1),(217,1,149,'2018-08-22 08:43:50',1,1),(218,1,154,'2018-08-22 08:43:52',1,1),(219,1,155,'2018-08-22 08:43:54',1,1),(220,1,147,'2018-08-22 08:43:56',1,1),(221,1,148,'2018-08-22 08:43:59',1,1),(222,1,156,'2018-08-22 08:44:01',1,1);
/*!40000 ALTER TABLE `sih_role_screens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_roles`
--

DROP TABLE IF EXISTS `sih_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Quyền người dùng';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_roles`
--

LOCK TABLES `sih_roles` WRITE;
/*!40000 ALTER TABLE `sih_roles` DISABLE KEYS */;
INSERT INTO `sih_roles` VALUES (1,'Admin','2018-06-18 00:00:00','0',1),(2,'Manage money 122','2018-04-19 01:00:00','1',1),(3,'Group Role 1','0000-00-00 00:00:00','1',0),(4,'Group Role 2','0000-00-00 00:00:00','1',1),(5,'Group Role 32','0000-00-00 00:00:00','1',1),(6,'Group Role 4','0000-00-00 00:00:00','1',1),(7,'Group Role 5-1','0000-00-00 00:00:00','1',1),(8,'Group Role 6','0000-00-00 00:00:00','1',1),(9,'Group Role 7','0000-00-00 00:00:00','1',1),(10,'Group Role 8','0000-00-00 00:00:00','1',1),(12,'Test role2','2018-07-03 00:00:00','1',0);
/*!40000 ALTER TABLE `sih_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_roles_screens`
--

DROP TABLE IF EXISTS `sih_roles_screens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_roles_screens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) NOT NULL,
  `id_screens` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_roles_screens`
--

LOCK TABLES `sih_roles_screens` WRITE;
/*!40000 ALTER TABLE `sih_roles_screens` DISABLE KEYS */;
/*!40000 ALTER TABLE `sih_roles_screens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_screens`
--

DROP TABLE IF EXISTS `sih_screens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_screens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `routed_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  `ico` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh sách màn hình';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_screens`
--

LOCK TABLES `sih_screens` WRITE;
/*!40000 ALTER TABLE `sih_screens` DISABLE KEYS */;
INSERT INTO `sih_screens` VALUES (2,'permission_role-get','permission_role/get','62_13','2018-04-19 00:00:00','1',1,''),(3,'permission_role-add','permission_role/add','62_13','2018-04-20 00:00:00','1',1,''),(4,'permission_role-edit','permission_role/edit','62_13','2018-04-19 00:00:00','1',1,''),(5,'permission_role-delete','permission_role/delete','62_13','2018-04-20 00:00:00','1',1,''),(6,'permission_role-push','permission_role/push','62_13','2018-04-19 00:00:00','1',1,''),(7,'permission_role-unpush','permission_role/unpush','62_13','2018-04-20 00:00:00','1',1,''),(13,'Quản lý role','permission_role','62','2018-04-19 00:00:00','1',1,''),(14,'Dashboard123','index','0','0000-00-00 00:00:00','0',1,'fa fa-th'),(15,'Quản lý role và màn hình','permission_role_screen','62','0000-00-00 00:00:00','0',1,''),(16,'Quản lý màn hình','permission_screen','62','0000-00-00 00:00:00','0',1,''),(17,'permission_screen/get','permission_screen/get','62_16','0000-00-00 00:00:00','0',1,''),(18,'permission_role_screen/edit','permission_role_screen/edit','62_15','0000-00-00 00:00:00','0',1,''),(19,'permission_screen/add','permission_screen/add','62_16','0000-00-00 00:00:00','0',1,''),(20,'permission_role_screen/get','permission_role_screen/get','62_15','0000-00-00 00:00:00','0',1,''),(21,'Danh sách quốc gia','list_nations','60','0000-00-00 00:00:00','0',1,''),(22,'list_nations/get','list_nations/get','60_21','0000-00-00 00:00:00','0',1,''),(23,'list_nations/add','list_nations/add','60_21','0000-00-00 00:00:00','0',1,''),(24,'list_nations/edit','list_nations/edit','60_21','0000-00-00 00:00:00','0',1,''),(25,'list_nations/delete','list_nations/delete','60_21','0000-00-00 00:00:00','0',1,''),(26,'list_nations/push','list_nations/push','60_21','0000-00-00 00:00:00','0',1,''),(27,'list_nations/unpush','list_nations/unpush','60_21','0000-00-00 00:00:00','0',1,''),(28,'Danh sách tỉnh thành','list_provinces','60','0000-00-00 00:00:00','0',1,''),(29,'list_provinces/get','list_provinces/get','60_28','0000-00-00 00:00:00','0',1,''),(30,'list_provinces/add','list_provinces/add','60_28','0000-00-00 00:00:00','0',1,''),(31,'list_provinces/edit','list_provinces/edit','60_28','0000-00-00 00:00:00','0',1,''),(32,'list_provinces/delete','list_provinces/delete','60_28','0000-00-00 00:00:00','0',1,''),(33,'list_provinces/push','list_provinces/push','60_28','0000-00-00 00:00:00','0',1,''),(34,'list_provinces/unpush','list_provinces/unpush','60_28','0000-00-00 00:00:00','0',1,''),(35,'Danh sách quận huyện','list_districts','60','2018-05-17 00:00:00','0',1,''),(36,'list_districts/get','list_districts/get','60_35','0000-00-00 00:00:00','0',1,''),(37,'list_districts/add','list_districts/add','60_35','0000-00-00 00:00:00','0',1,''),(38,'list_districts/edit','list_districts/edit','60_35','0000-00-00 00:00:00','0',1,''),(39,'list_districts/delete','list_districts/delete','60_35','0000-00-00 00:00:00','0',1,''),(40,'list_districts/push','list_districts/push','60_35','0000-00-00 00:00:00','0',1,''),(41,'list_districts/unpush','list_districts/unpush','60_35','0000-00-00 00:00:00','0',1,''),(42,'Quản lý nhân sự','','0','0000-00-00 00:00:00','0',1,'fa fa-users'),(43,'manage_user/get','manage_user/get','42_61','0000-00-00 00:00:00','0',1,''),(44,'manage_user/add','manage_user/add','42_61','0000-00-00 00:00:00','1',1,''),(45,'manage_user/edit','manage_user/edit','42_61','0000-00-00 00:00:00','1',1,''),(46,'manage_user/delete','manage_user/delete','42_61','0000-00-00 00:00:00','1',1,''),(47,'manage_user/push','manage_user/push','42_61','0000-00-00 00:00:00','1',1,''),(48,'permission_screen/edit','permission_screen/edit','62_16','0000-00-00 00:00:00','1',1,''),(49,'manage_user/unpush','manage_user/unpush','42_61','0000-00-00 00:00:00','1',1,''),(50,'permission_screen/push','permission_screen/push','62_16','0000-00-00 00:00:00','1',1,''),(51,'permission_screen/unpush','permission_screen/unpush','62_16','0000-00-00 00:00:00','1',1,''),(52,'profile','profile','0','0000-00-00 00:00:00','1',1,''),(53,'Danh sách phường xã','list_towns','60','2018-05-17 00:00:00','1',1,''),(54,'Danh sách tỉnh thành get','list_towns/get','60_53','0000-00-00 00:00:00','0',1,''),(55,'Danh sách tỉnh thành add','list_towns/add','60_53','0000-00-00 00:00:00','0',1,''),(56,'Danh sách tỉnh thành edit','list_towns/edit','60_53','0000-00-00 00:00:00','0',1,''),(57,'Danh sách tỉnh thành delete','list_towns/delete','60_53','0000-00-00 00:00:00','0',1,''),(58,'Danh sách tỉnh thành push','list_towns/push','60_53','0000-00-00 00:00:00','0',1,''),(59,'Danh sách tỉnh thành unpush','list_towns/unpush','60_53','0000-00-00 00:00:00','0',1,''),(60,'Danh mục','','0','0000-00-00 00:00:00','1',1,'fa fa-th-list'),(61,'Danh sách nhân sự','manage_user','42','0000-00-00 00:00:00','0',1,''),(62,'Quản lý phân quyền','','0','0000-00-00 00:00:00','0',1,'fa fa-users'),(63,'permission_screen/delete','permission_screen/delete','62_16','0000-00-00 00:00:00','0',1,''),(64,'Danh sách chức vụ/nghề nghiệp','list_titles','60','0000-00-00 00:00:00','1',1,''),(65,'Danh mục chức vụ get','list_titles/get','60_64','0000-00-00 00:00:00','1',1,''),(66,'Danh mục chức vụ add','list_titles/add','60_64','0000-00-00 00:00:00','1',1,''),(67,'Danh mục chức vụ edit','list_titles/edit','60_64','0000-00-00 00:00:00','1',1,''),(68,'Danh mục chức vụ delete','list_titles/delete','60_64','0000-00-00 00:00:00','1',1,''),(69,'Danh mục chức vụ push','list_titles/push','60_64','0000-00-00 00:00:00','1',1,''),(70,'Danh mục chức vụ unpush','list_titles/unpush','60_64','0000-00-00 00:00:00','1',1,''),(71,'Danh sách phòng ban','list_departments','60','2018-05-21 00:00:00','1',1,''),(72,'Danh sách phòng ban/get','list_departments/get','60_71','2018-05-21 00:00:00','1',1,''),(73,'Danh sách phòng ban/add','list_departments/add','60_71','2018-05-21 00:00:00','1',1,''),(74,'Danh sách phòng ban/edit','list_departments/edit','60_71','2018-05-21 00:00:00','1',1,''),(75,'Danh sách phòng ban/delete','list_departments/delete','60_71','2018-05-21 00:00:00','1',1,''),(76,'Danh sách phòng ban/push','list_departments/push','60_71','2018-05-21 00:00:00','1',1,''),(77,'Danh sách phòng ban/unpush','list_departments/unpush','60_71','2018-05-21 00:00:00','1',1,''),(78,'Nữ Hộ Sinh','list_midwives','60','2018-05-29 00:00:00','1',1,''),(79,'get','list_midwives/get','60_78','2018-05-29 00:00:00','1',1,''),(80,'add','list_midwives/add','60_78','2018-05-29 00:00:00','1',1,''),(81,'edit','list_midwives/edit','60_78','2018-05-29 00:00:00','1',1,''),(82,'delete','list_midwives/delete','60_78','2018-05-29 00:00:00','1',1,''),(83,'push','list_midwives/push','60_78','2018-05-29 00:00:00','1',1,''),(84,'unpush','list_midwives/unpush','60_78','2018-05-29 00:00:00','1',1,''),(85,'Danh Mục Vật Tư','list_materials','60','2018-05-29 00:00:00','1',1,''),(86,'get','list_materials/get','60_85','2018-05-29 00:00:00','1',1,''),(87,'add','list_materials/add','60_85','2018-05-29 00:00:00','1',1,''),(88,'edit','list_materials/edit','60_85','2018-05-29 00:00:00','1',1,''),(89,'delete','list_materials/delete','60_85','2018-05-29 00:00:00','1',1,''),(90,'push','list_materials/push','60_85','2018-05-29 00:00:00','1',1,''),(91,'unpush','list_materials/unpush','60_85','2018-05-29 00:00:00','1',1,''),(92,'Nhóm Khám Chữa Bệnh','list_group_therapys','60','2018-05-29 00:00:00','1',1,''),(93,'get','list_group_therapys/get','60_92','2018-05-29 00:00:00','1',1,''),(94,'add','list_group_therapys/add','60_92','2018-05-29 00:00:00','1',1,''),(95,'edit','list_group_therapys/edit','60_92','2018-05-29 00:00:00','1',1,''),(96,'delete','list_group_therapys/delete','60_92','2018-05-29 00:00:00','1',1,''),(97,'push','list_group_therapys/push','60_92','2018-05-29 00:00:00','1',1,''),(98,'unpush','list_group_therapys/unpush','60_92','2018-05-29 00:00:00','1',1,''),(99,'Lý Do Chuyển Viện','reason_for_transfer','60','2018-05-29 00:00:00','1',1,''),(100,'get','reason_for_transfer/get','60_99','2018-05-29 00:00:00','1',1,''),(101,'add','reason_for_transfer/add','60_99','2018-05-29 00:00:00','1',1,''),(102,'edit','reason_for_transfer/edit','60_99','2018-05-29 00:00:00','1',1,''),(103,'delete','reason_for_transfer/delete','60_99','2018-05-29 00:00:00','1',1,''),(104,'push','reason_for_transfer/push','60_99','2018-05-29 00:00:00','1',1,''),(105,'unpush','reason_for_transfer/unpush','60_99','2018-05-29 00:00:00','1',1,''),(106,'Lý Do Xuất Viện','reason_for_discharge','60','2018-05-29 00:00:00','1',1,''),(107,'get','reason_for_discharge/get','60_106','2018-05-29 00:00:00','1',1,''),(108,'add','reason_for_discharge/add','60_106','2018-05-29 00:00:00','1',1,''),(109,'edit','reason_for_discharge/edit','60_106','2018-05-29 00:00:00','1',1,''),(110,'delete','reason_for_discharge/delete','60_106','2018-05-29 00:00:00','1',1,''),(111,'push','reason_for_discharge/push','60_106','2018-05-29 00:00:00','1',1,''),(112,'unpush','reason_for_discharge/unpush','60_106','2018-05-29 00:00:00','1',1,''),(113,'Danh mục nhà cung cấp','list_vendors','60','2018-06-04 00:00:00','1',1,''),(114,'add','list_vendors/add','60_113','2018-06-04 00:00:00','1',1,''),(115,'edit','list_vendors/edit','60_113','2018-06-04 00:00:00','1',1,''),(116,'delete','list_vendors/delete','60_113','2018-06-04 00:00:00','1',1,''),(117,'push','list_vendors/push','60_113','2018-06-04 00:00:00','1',1,''),(118,'unpush','list_vendors/unpush','60_113','2018-06-04 00:00:00','1',1,''),(119,'get','list_vendors/get','60_113','2018-06-04 00:00:00','1',1,''),(120,'Danh sách tiền tệ','list_currencies','60','2018-06-05 00:00:00','1',1,''),(121,'add','list_currencies/add','60_120','2018-06-05 00:00:00','1',1,''),(122,'edit','list_currencies/edit','60_120','2018-06-05 00:00:00','1',1,''),(123,'get','list_currencies/get','60_120','2018-06-05 00:00:00','1',1,''),(124,'delete','list_currencies/delete','60_120','2018-06-05 00:00:00','1',1,''),(125,'push','list_currencies/push','60_120','2018-06-05 00:00:00','1',1,''),(126,'unpush','list_currencies/unpush','60_120','2018-06-05 00:00:00','1',1,''),(127,'tét','8923','123','0000-00-00 00:00:00','0',1,''),(128,'tét','8923','123','0000-00-00 00:00:00','0',1,''),(129,'Danh sách dân tộc','list_folks','60','0000-00-00 00:00:00','1',1,''),(130,'list_folks/get','list_folks/get','60_129','0000-00-00 00:00:00','1',1,''),(131,'list_folks/add','list_folks/add','60_129','0000-00-00 00:00:00','1',1,''),(132,'list_folks/edit','list_folks/edit','60_129','0000-00-00 00:00:00','1',1,''),(133,'list_folks/delete','list_folks/delete','60_129','0000-00-00 00:00:00','1',1,''),(134,'list_folks/push','list_folks/push','60_129','0000-00-00 00:00:00','1',1,''),(135,'list_folks/unpush','list_folks/unpush','60_129','0000-00-00 00:00:00','1',1,''),(136,'Danh Mục Phòng Khám','list_clinic','60','0000-00-00 00:00:00','1',1,''),(137,'list_clinic/get','list_clinic/get','60_136','0000-00-00 00:00:00','1',1,''),(138,'list_clinic/add','list_clinic/add','60_136','0000-00-00 00:00:00','1',1,''),(139,'list_clinic/delete','list_clinic/delete','60_136','0000-00-00 00:00:00','1',1,''),(140,'list_clinic/push','list_clinic/push','60_136','0000-00-00 00:00:00','1',1,''),(141,'list_clinic/unpush','list_clinic/unpush','60_136','0000-00-00 00:00:00','1',1,''),(142,'list_clinic/edit','list_clinic/edit','60_136','0000-00-00 00:00:00','1',1,''),(143,'Danh mục loại khám chữa bệnh','list_healthcare_type','60','0000-00-00 00:00:00','1',1,''),(144,'list_healthcare_type/add','list_healthcare_type/add','60_143','0000-00-00 00:00:00','1',1,''),(145,'list_healthcare_type/edit','list_healthcare_type/edit','60_143','0000-00-00 00:00:00','1',1,''),(146,'list_healthcare_type/get','list_healthcare_type/get','60_143','0000-00-00 00:00:00','1',1,''),(147,'list_healthcare_type/push','list_healthcare_type/push','60_143','0000-00-00 00:00:00','1',1,''),(148,'list_healthcare_type/unpush','list_healthcare_type/unpush','60_143','0000-00-00 00:00:00','1',1,''),(149,'list_healthcare_type/delete','list_healthcare_type/delete','60_143','0000-00-00 00:00:00','1',1,''),(150,'Danh mục nhóm khám chữa bệnh','list_healthcare_group','60','0000-00-00 00:00:00','1',1,''),(151,'list_healthcare_group/get','list_healthcare_group/get','60_150','0000-00-00 00:00:00','1',1,''),(152,'list_healthcare_group/add','list_healthcare_group/add','60_150','0000-00-00 00:00:00','1',1,''),(153,'list_healthcare_group/edit','list_healthcare_group/edit','60_150','0000-00-00 00:00:00','1',1,''),(154,'list_healthcare_group/delete','list_healthcare_group/delete','60_150','0000-00-00 00:00:00','1',1,''),(155,'list_healthcare_group/push','list_healthcare_group/push','60_150','0000-00-00 00:00:00','1',1,''),(156,'list_healthcare_group/unpush','list_healthcare_group/unpush','60_150','0000-00-00 00:00:00','1',1,'');
/*!40000 ALTER TABLE `sih_screens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_user_profiles`
--

DROP TABLE IF EXISTS `sih_user_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_list_departments` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relative_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro` longtext COLLATE utf8_unicode_ci NOT NULL,
  `degree` longtext COLLATE utf8_unicode_ci NOT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_user_profiles`
--

LOCK TABLES `sih_user_profiles` WRITE;
/*!40000 ALTER TABLE `sih_user_profiles` DISABLE KEYS */;
INSERT INTO `sih_user_profiles` VALUES (1,1,0,'2000-09-18','19 Phó Đức Chính','','giới thiệu bằng cấp','','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `sih_user_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sih_users`
--

DROP TABLE IF EXISTS `sih_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sih_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) NOT NULL,
  `id_list_departments` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sih_users`
--

LOCK TABLES `sih_users` WRITE;
/*!40000 ALTER TABLE `sih_users` DISABLE KEYS */;
INSERT INTO `sih_users` VALUES (1,1,6,'Admin John 3','john@ea.com','10213123123','e10adc3949ba59abbe56e057f20f883e','uploads/avatas/1_15247335295.jpeg','2018-02-09 00:00:00','0',1),(2,2,2,'Khoa 22','khoaxb2@gmail.com','0987789987','fcea920f7412b5da7be0cf42b8c93759','','2017-11-28 14:27:01','0',1),(3,4,4,'testuser','ch2au@gmail.com','1923123','e10adc3949ba59abbe56e057f20f883e','','0000-00-00 00:00:00','0',1);
/*!40000 ALTER TABLE `sih_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'projectsky_20180823'
--

--
-- Dumping routines for database 'projectsky_20180823'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-23 11:44:21
