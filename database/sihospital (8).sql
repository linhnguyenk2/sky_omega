-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2018 at 06:17 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sihospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `sih_form1`
--

CREATE TABLE IF NOT EXISTS `sih_form1` (
  `f1Id` int(11) NOT NULL,
  `f1MaPhieu` int(11) DEFAULT NULL,
  `f1MaBenhNhan` int(11) DEFAULT NULL,
  `f1Child` tinyint(1) DEFAULT NULL,
  `f1Name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `f1Birthday` date NOT NULL,
  `f1Mari` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1Gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `f1Nation` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `f1CMND` int(9) DEFAULT NULL,
  `f1Address` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1Email` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1Homephone` int(20) DEFAULT NULL,
  `f1Mobile` int(20) DEFAULT NULL,
  `f1EmerFullName` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1EmerRelation` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1EmerPhone` int(20) DEFAULT NULL,
  `f1EmerAddress` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1PrivaInsuran` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1HavePrivaInsuran` tinyint(1) DEFAULT NULL,
  `f1OrderNameCompany` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1OrderAddressCompany` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1OrderMST` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1DateCreate` date NOT NULL,
  `f1Signal` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_currencys`
--

CREATE TABLE IF NOT EXISTS `sih_list_currencys` (
  `lcId` int(11) NOT NULL,
  `lcCode` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Mã tiền',
  `lcRate` int(11) DEFAULT NULL COMMENT 'Tỉ giá',
  `lcCreatedAt` datetime DEFAULT NULL,
  `lcStat` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_currencys`
--

INSERT INTO `sih_list_currencys` (`lcId`, `lcCode`, `lcRate`, `lcCreatedAt`, `lcStat`) VALUES
(1, 'USD', 20000, '2018-06-05 00:00:00', 'O');

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_departments`
--

CREATE TABLE IF NOT EXISTS `sih_list_departments` (
  `ldId` tinyint(4) NOT NULL,
  `ldName` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên khoa/phòng ban',
  `uId` int(11) DEFAULT NULL COMMENT 'Mã user trưởng/người đứng đầu khoa/phòng ban',
  `ldCreatedAt` datetime NOT NULL COMMENT 'Thời điểm tạo',
  `ldCreatedBy` int(11) NOT NULL COMMENT 'Người tạo',
  `ldStat` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'O: Open; C: Close'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Khoa/phòng ban';

--
-- Dumping data for table `sih_list_departments`
--

INSERT INTO `sih_list_departments` (`ldId`, `ldName`, `uId`, `ldCreatedAt`, `ldCreatedBy`, `ldStat`) VALUES
(1, 'Phòng ban 1', 1, '2018-05-21 00:00:00', 1, 'C'),
(3, 'Phong ban test', 1, '2018-05-21 00:00:00', 1, 'O'),
(4, 'Phòng kế toán', 1, '2018-05-21 00:00:00', 1, 'o'),
(5, 'Phòng đa khoa', 1, '2018-05-21 00:00:00', 1, 'o'),
(6, 'Phòng hồi sức', 1, '2018-05-21 00:00:00', 1, 'o'),
(7, 'Phòng kế toán', 1, '2018-05-21 00:00:00', 1, 'o'),
(8, 'Phòng thu ngân', 1, '2018-05-21 00:00:00', 1, 'o'),
(9, 'Phòng nhập viện', 1, '2018-05-21 00:00:00', 1, 'o'),
(10, 'Phòng xuất viện', 1, '2018-05-21 00:00:00', 1, 'o'),
(11, 'Phòng y tế', 1, '2018-05-21 00:00:00', 1, 'o'),
(12, 'Phòng điều dưỡng', 1, '2018-05-21 00:00:00', 1, 'o'),
(13, 'Phòng công nghệ thông tin', 1, '2018-05-21 00:00:00', 1, 'o'),
(14, 'Phòng quản lý chất lượng', 1, '2018-05-21 00:00:00', 1, 'o'),
(15, 'Phòng hành chính quản trị', 1, '2018-05-21 00:00:00', 1, 'o'),
(16, 'Khoa Dị Ứng', 0, '2018-05-20 00:00:00', 0, 'O'),
(17, 'Khoa Chấn Thương Sọ Não', 1, '2018-05-21 00:00:00', 1, 'o'),
(18, 'Phòng hồi sức chức năng', 1, '2018-05-21 00:00:00', 1, 'o'),
(19, 'Phòng Răng hàm mặt', 1, '2018-05-21 00:00:00', 1, 'o');

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_districts`
--

CREATE TABLE IF NOT EXISTS `sih_list_districts` (
  `ldId` int(11) NOT NULL,
  `lpId` int(11) NOT NULL COMMENT 'Mã tỉnh',
  `ldName` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên quận/huyện',
  `ldCreatedAt` datetime NOT NULL COMMENT 'Thời điểm tạo',
  `ldCreatedBy` int(11) NOT NULL COMMENT 'Người tạo',
  `ldStat` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'O: Open; C: Close',
  `ldOrder` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Quận/huyện';

--
-- Dumping data for table `sih_list_districts`
--

INSERT INTO `sih_list_districts` (`ldId`, `lpId`, `ldName`, `ldCreatedAt`, `ldCreatedBy`, `ldStat`, `ldOrder`) VALUES
(1, 1, 'Quan 1-2', '2018-04-16 02:10:17', 1, 'C', 4),
(3, 1, 'Quận 3', '0000-00-00 00:00:00', 1, 'o', 2),
(4, 3, '', '0000-00-00 00:00:00', 0, 'o', 22),
(5, 1, 'Phường 2', '0000-00-00 00:00:00', 0, 'o', 0),
(6, 1, 'Phuong 3', '0000-00-00 00:00:00', 0, 'o', 1),
(7, 1, 'Phuong 4', '0000-00-00 00:00:00', 0, 'o', 2),
(8, 1, 'Phuong 5', '0000-00-00 00:00:00', 0, 'o', 3),
(9, 1, 'Phuong 6', '0000-00-00 00:00:00', 0, 'o', 3),
(10, 1, 'Phuong 7', '0000-00-00 00:00:00', 0, 'o', 3),
(11, 1, 'Phuong 8', '0000-00-00 00:00:00', 0, 'o', 3),
(12, 1, 'Phuong 9', '0000-00-00 00:00:00', 0, 'o', 3),
(13, 1, 'Phuong 10', '0000-00-00 00:00:00', 0, 'o', 3),
(14, 1, 'Phuong 11', '0000-00-00 00:00:00', 0, 'o', 3),
(15, 1, 'Phuong 12', '0000-00-00 00:00:00', 0, 'o', 3),
(16, 1, 'Phuong 13', '0000-00-00 00:00:00', 0, 'C', 3),
(17, 1, 'Phuong 14', '0000-00-00 00:00:00', 0, 'C', 3),
(18, 1, 'Phuong 15-1', '0000-00-00 00:00:00', 0, 'C', 3),
(19, 1, 'Phuong 16', '0000-00-00 00:00:00', 0, 'o', 3);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_group_therapys`
--

CREATE TABLE IF NOT EXISTS `sih_list_group_therapys` (
  `lgtId` int(11) NOT NULL,
  `lgtMaKham` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lgtNam` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lgtDate` datetime DEFAULT NULL,
  `lgtStat` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Nhóm Khám Chữa Bệnh';

--
-- Dumping data for table `sih_list_group_therapys`
--

INSERT INTO `sih_list_group_therapys` (`lgtId`, `lgtMaKham`, `lgtNam`, `lgtDate`, `lgtStat`) VALUES
(1, 'ma 01', 'test-3', '2018-05-01 00:00:00', 'O');

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_materials`
--

CREATE TABLE IF NOT EXISTS `sih_list_materials` (
  `lmId` int(11) NOT NULL,
  `lmMaqa` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmPrice` int(14) DEFAULT NULL,
  `lmPrice_eng` int(14) DEFAULT NULL,
  `lmDate` datetime DEFAULT NULL,
  `lmUser` int(15) DEFAULT NULL,
  `lmStat` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh Mục Vật Tư';

--
-- Dumping data for table `sih_list_materials`
--

INSERT INTO `sih_list_materials` (`lmId`, `lmMaqa`, `lmName`, `lmPrice`, `lmPrice_eng`, `lmDate`, `lmUser`, `lmStat`) VALUES
(1, 'ma 01', 'quan ao1', 123, 12345, '2018-05-01 00:00:00', 1, 'O');

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_midwives`
--

CREATE TABLE IF NOT EXISTS `sih_list_midwives` (
  `lmId` int(11) NOT NULL,
  `lmMaHoSinh` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmPhongBan` int(11) DEFAULT NULL,
  `lmDate` datetime DEFAULT NULL,
  `lmStat` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Nữ Hộ Sinh';

--
-- Dumping data for table `sih_list_midwives`
--

INSERT INTO `sih_list_midwives` (`lmId`, `lmMaHoSinh`, `lmName`, `lmPhongBan`, `lmDate`, `lmStat`) VALUES
(1, 'm1', 'chu thi c2', 0, '2018-05-07 00:00:00', 'O');

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_nations`
--

CREATE TABLE IF NOT EXISTS `sih_list_nations` (
  `lnId` int(11) NOT NULL,
  `lnName` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên quốc gia',
  `lnCreatedAt` datetime NOT NULL COMMENT 'Thời điểm tạo',
  `lnCreatedBy` int(11) NOT NULL COMMENT 'Người tạo',
  `lnStat` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'O: Open; C: Close',
  `lnOrder` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Quốc gia';

--
-- Dumping data for table `sih_list_nations`
--

INSERT INTO `sih_list_nations` (`lnId`, `lnName`, `lnCreatedAt`, `lnCreatedBy`, `lnStat`, `lnOrder`) VALUES
(1, 'Việt Nam 123', '2018-06-19 00:00:00', 1, 'C', 111112),
(2, 'Thai Lan', '0000-00-00 00:00:00', 1, 'C', 0),
(4, 'China', '0000-00-00 00:00:00', 1, 'O', 3),
(5, 'test', '2018-05-08 00:00:00', 2, 'O', 0),
(6, 'Hong Kong', '2018-05-07 00:00:00', 1, 'O', 1),
(7, 'Băng La Đét', '2018-04-29 00:00:00', 1, 'O', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_provinces`
--

CREATE TABLE IF NOT EXISTS `sih_list_provinces` (
  `lpId` int(11) NOT NULL,
  `lpName` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên tỉnh/tp',
  `lpCreatedAt` datetime NOT NULL COMMENT 'Thời điểm tạo',
  `lpCreatedBy` int(11) NOT NULL COMMENT 'Người tạo',
  `lpStat` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'O: Open; C: Close',
  `lpOrder` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Tỉnh thành';

--
-- Dumping data for table `sih_list_provinces`
--

INSERT INTO `sih_list_provinces` (`lpId`, `lpName`, `lpCreatedAt`, `lpCreatedBy`, `lpStat`, `lpOrder`) VALUES
(2, 'Ho Chi Minh123', '0000-00-00 00:00:00', 1, 'O', 2),
(3, 'Nghệ Án Việt Nảm', '0000-00-00 00:00:00', 1, 'C', 1),
(4, 'Vung tau', '0000-00-00 00:00:00', 1, 'C', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_titles`
--

CREATE TABLE IF NOT EXISTS `sih_list_titles` (
  `ltId` int(11) NOT NULL,
  `ltName` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'VD: Bác sĩ',
  `ltCreatedAt` datetime NOT NULL COMMENT 'Thời điểm tạo',
  `ltCreatedBy` int(11) NOT NULL COMMENT 'Người tạo',
  `ltStat` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'O: Open; C: Close'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục Chức danh';

--
-- Dumping data for table `sih_list_titles`
--

INSERT INTO `sih_list_titles` (`ltId`, `ltName`, `ltCreatedAt`, `ltCreatedBy`, `ltStat`) VALUES
(1, 'Kỹ thuật ', '2018-05-09 00:00:00', 1, 'O'),
(2, 'Phó khoa phẩu thuật', '2018-05-04 00:00:00', 1, 'O'),
(3, 'Trưởng khoa nội trú', '2018-05-04 00:00:00', 1, 'O'),
(4, 'Phó khoa nội trú', '2018-05-04 00:00:00', 1, 'O'),
(5, 'Bác sỹ nội trú', '2018-05-04 00:00:00', 1, 'O'),
(6, 'Bác sỹ phẫu thuật', '2018-05-04 00:00:00', 1, 'O'),
(7, 'Giám đốc', '2018-05-04 00:00:00', 1, 'O'),
(8, 'Phó giám đốc', '2018-05-04 00:00:00', 1, 'O'),
(9, 'Tester', '2018-04-29 00:00:00', 1, 'O');

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_towns`
--

CREATE TABLE IF NOT EXISTS `sih_list_towns` (
  `ltId` int(11) NOT NULL,
  `ldId` int(11) NOT NULL COMMENT 'Mã quận/huyện',
  `ltName` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT 'Tên phường/xã',
  `ltCreatedAt` datetime NOT NULL COMMENT 'Thời điểm tạo',
  `ltCreatedBy` int(11) NOT NULL COMMENT 'Người tạo',
  `ltStat` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'O: Open; C: Close',
  `ltOrder` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh mục phường/xã';

--
-- Dumping data for table `sih_list_towns`
--

INSERT INTO `sih_list_towns` (`ltId`, `ldId`, `ltName`, `ltCreatedAt`, `ltCreatedBy`, `ltStat`, `ltOrder`) VALUES
(2, 1, 'phường cầu kho 4', '0000-00-00 00:00:00', 1, 'O', 2),
(3, 1, 'phuwonfg 1', '0000-00-00 00:00:00', 1, 'o', 2),
(4, 1, 'Phuong 121', '0000-00-00 00:00:00', 1, 'o', 1),
(5, 1, 'Quan 6', '0000-00-00 00:00:00', 1, 'O', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_list_vendors`
--

CREATE TABLE IF NOT EXISTS `sih_list_vendors` (
  `lvId` int(11) NOT NULL,
  `lvVendorCode` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `lvSecondCode` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvVendorName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvNameQuick` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvAddress` text COLLATE utf8_unicode_ci,
  `lvCityProvince` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvNation` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvPhone` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvFax` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvGroupNcc` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvGroupSettlement` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvGroupTax` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvCurrencyCode` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvCreditLimit` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvTaxCode` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvEmail` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvWebsite` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvContact` text COLLATE utf8_unicode_ci,
  `lvContactPhone` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvDateCreate` datetime DEFAULT NULL,
  `lvUser` int(11) DEFAULT NULL,
  `lvBankAccount` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvBankName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lvBankAddress` text COLLATE utf8_unicode_ci,
  `lvModules` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_list_vendors`
--

INSERT INTO `sih_list_vendors` (`lvId`, `lvVendorCode`, `lvSecondCode`, `lvVendorName`, `lvNameQuick`, `lvAddress`, `lvCityProvince`, `lvNation`, `lvPhone`, `lvFax`, `lvGroupNcc`, `lvGroupSettlement`, `lvGroupTax`, `lvCurrencyCode`, `lvCreditLimit`, `lvTaxCode`, `lvEmail`, `lvWebsite`, `lvContact`, `lvContactPhone`, `lvDateCreate`, `lvUser`, `lvBankAccount`, `lvBankName`, `lvBankAddress`, `lvModules`) VALUES
(2, 't3', 't11', 'lkasdj', 'hhj', 'jhjhj', 'jjhj', 'hjhj', '8798', '8798', 'd', 'jh', 'jhj', 'hjh', 'jh', 'jh', 'jh@gasd.cio', 'jhkhj', 'jhjk', '89798', '0000-00-00 00:00:00', 0, 'jh', 'jhj', 'jhjh', '22');

-- --------------------------------------------------------

--
-- Table structure for table `sih_reason_for_discharge`
--

CREATE TABLE IF NOT EXISTS `sih_reason_for_discharge` (
  `rfdId` int(11) NOT NULL,
  `rfdMa` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rfdName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rfdNote` text COLLATE utf8_unicode_ci,
  `rfdDate` datetime DEFAULT NULL,
  `rfdStat` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lý Do Xuất Viện';

--
-- Dumping data for table `sih_reason_for_discharge`
--

INSERT INTO `sih_reason_for_discharge` (`rfdId`, `rfdMa`, `rfdName`, `rfdNote`, `rfdDate`, `rfdStat`) VALUES
(1, 'Ma01.10', 'u vu2', 'Huynh Thi Ngoc Hogn', '2018-04-03 00:00:00', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `sih_reason_for_transfer`
--

CREATE TABLE IF NOT EXISTS `sih_reason_for_transfer` (
  `rftId` int(11) NOT NULL,
  `rftMa` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rftName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rftNote` text COLLATE utf8_unicode_ci,
  `rftDate` datetime DEFAULT NULL,
  `rftStat` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Lý Do Chuyển Viện';

--
-- Dumping data for table `sih_reason_for_transfer`
--

INSERT INTO `sih_reason_for_transfer` (`rftId`, `rftMa`, `rftName`, `rftNote`, `rftDate`, `rftStat`) VALUES
(1, 'test1', 'test1', 'note1', '2018-05-01 00:00:00', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `sih_roles`
--

CREATE TABLE IF NOT EXISTS `sih_roles` (
  `rId` int(11) NOT NULL,
  `rName` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'VD: Quyền Admin',
  `rCreatedAt` datetime NOT NULL COMMENT 'Thời điểm tạo',
  `rCreatedBy` int(11) NOT NULL COMMENT 'Người tạo',
  `rStat` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'O: Open; C: Close'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Quyền người dùng';

--
-- Dumping data for table `sih_roles`
--

INSERT INTO `sih_roles` (`rId`, `rName`, `rCreatedAt`, `rCreatedBy`, `rStat`) VALUES
(1, 'Admin', '2018-06-18 00:00:00', 0, 'o'),
(2, 'Manage money 122', '2018-04-19 01:00:00', 1, 'C'),
(3, 'Group Role 1', '0000-00-00 00:00:00', 1, 'C'),
(4, 'Group Role 2', '0000-00-00 00:00:00', 1, 'o'),
(5, 'Group Role 32', '0000-00-00 00:00:00', 1, 'C'),
(6, 'Group Role 4', '0000-00-00 00:00:00', 1, 'o'),
(7, 'Group Role 5-1', '0000-00-00 00:00:00', 1, 'o'),
(8, 'Group Role 6', '0000-00-00 00:00:00', 1, 'o'),
(9, 'Group Role 7', '0000-00-00 00:00:00', 1, 'o'),
(10, 'Group Role 8', '0000-00-00 00:00:00', 1, 'o'),
(12, 'Test role', '2018-07-03 00:00:00', 1, 'o');

-- --------------------------------------------------------

--
-- Table structure for table `sih_role_screens`
--

CREATE TABLE IF NOT EXISTS `sih_role_screens` (
  `rsId` int(11) NOT NULL,
  `rId` int(11) NOT NULL COMMENT 'Mã role',
  `sId` int(11) NOT NULL COMMENT 'Mã màn hình',
  `rsCreatedAt` datetime NOT NULL COMMENT 'Thời điểm tạo',
  `rsCreatedBy` int(11) NOT NULL COMMENT 'Người tạo',
  `rsStat` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'O: Open; C: Close'
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng map quyền - màn hình';

--
-- Dumping data for table `sih_role_screens`
--

INSERT INTO `sih_role_screens` (`rsId`, `rId`, `sId`, `rsCreatedAt`, `rsCreatedBy`, `rsStat`) VALUES
(8, 1, 1, '2018-04-20 11:24:41', 1, 'O'),
(9, 1, 4, '2018-04-20 11:24:52', 1, 'O'),
(10, 1, 7, '2018-04-20 11:28:15', 1, 'O'),
(11, 1, 6, '2018-04-20 11:34:58', 1, 'O'),
(12, 1, 5, '2018-04-20 11:37:35', 1, 'O'),
(13, 1, 3, '2018-04-20 11:41:13', 1, 'O'),
(14, 1, 2, '2018-04-20 11:41:15', 1, 'O'),
(15, 1, 8, '2018-04-20 12:06:50', 1, 'C'),
(16, 2, 6, '2018-04-20 12:06:55', 1, 'C'),
(17, 2, 1, '2018-04-20 12:07:00', 1, 'O'),
(18, 1, 12, '2018-04-20 12:11:21', 1, 'O'),
(19, 2, 2, '2018-04-23 06:12:19', 1, 'C'),
(20, 2, 3, '2018-04-23 06:12:21', 1, 'C'),
(21, 2, 4, '2018-04-23 06:12:22', 1, 'C'),
(22, 2, 5, '2018-04-23 06:12:22', 1, 'C'),
(23, 2, 7, '2018-04-23 06:12:23', 1, 'C'),
(24, 2, 8, '2018-04-23 06:12:24', 1, 'C'),
(25, 1, 13, '2018-04-23 08:23:55', 1, 'O'),
(26, 1, 16, '2018-04-23 09:21:29', 1, 'O'),
(27, 1, 15, '2018-04-23 09:21:29', 1, 'O'),
(28, 1, 14, '2018-04-23 09:21:30', 1, 'O'),
(29, 1, 17, '2018-04-23 10:10:55', 1, 'O'),
(30, 1, 20, '2018-04-23 10:12:50', 1, 'O'),
(31, 1, 18, '2018-04-23 10:13:33', 1, 'O'),
(32, 1, 11, '2018-04-23 10:22:38', 1, 'C'),
(33, 1, 19, '2018-04-23 11:00:07', 1, 'O'),
(34, 2, 17, '2018-04-23 11:05:07', 1, 'C'),
(35, 1, 21, '2018-04-24 11:54:27', 1, 'O'),
(36, 1, 22, '2018-04-24 11:54:40', 1, 'O'),
(37, 2, 34, '2018-04-24 12:37:12', 1, 'C'),
(38, 1, 28, '2018-04-26 04:57:16', 1, 'O'),
(39, 1, 29, '2018-04-26 04:57:46', 1, 'O'),
(40, 1, 35, '2018-04-26 04:58:03', 1, 'O'),
(41, 1, 36, '2018-04-26 04:58:04', 1, 'O'),
(42, 1, 42, '2018-04-26 05:25:39', 1, 'O'),
(43, 1, 43, '2018-04-26 05:25:52', 1, 'O'),
(44, 1, 44, '2018-04-26 05:28:59', 1, 'O'),
(45, 1, 45, '2018-04-26 05:29:00', 1, 'O'),
(46, 1, 46, '2018-04-26 05:29:00', 1, 'O'),
(47, 1, 47, '2018-04-26 05:29:01', 1, 'O'),
(48, 1, 48, '2018-04-26 05:59:48', 1, 'O'),
(49, 1, 49, '2018-04-26 06:01:35', 1, 'O'),
(50, 1, 50, '2018-04-26 06:03:10', 1, 'O'),
(51, 1, 51, '2018-04-26 06:03:10', 1, 'O'),
(52, 1, 34, '2018-04-26 06:03:51', 1, 'O'),
(53, 1, 33, '2018-04-26 06:03:51', 1, 'O'),
(54, 1, 31, '2018-04-26 06:03:56', 1, 'O'),
(55, 1, 32, '2018-04-26 06:03:56', 1, 'O'),
(56, 1, 30, '2018-04-26 06:03:56', 1, 'O'),
(57, 1, 27, '2018-04-26 06:03:58', 1, 'O'),
(58, 1, 26, '2018-04-26 06:03:59', 1, 'O'),
(59, 1, 24, '2018-04-26 06:04:00', 1, 'O'),
(60, 1, 25, '2018-04-26 06:04:01', 1, 'O'),
(61, 1, 23, '2018-04-26 06:04:01', 1, 'O'),
(62, 1, 41, '2018-04-26 06:04:06', 1, 'O'),
(63, 1, 40, '2018-04-26 06:04:07', 1, 'O'),
(64, 1, 38, '2018-04-26 06:04:08', 1, 'O'),
(65, 1, 39, '2018-04-26 06:04:08', 1, 'O'),
(66, 1, 37, '2018-04-26 06:04:09', 1, 'O'),
(67, 1, 52, '2018-04-26 11:03:54', 1, 'O'),
(68, 1, 53, '2018-05-02 09:26:16', 1, 'O'),
(69, 1, 54, '2018-05-02 09:26:17', 1, 'O'),
(70, 1, 56, '2018-05-02 09:26:18', 1, 'O'),
(71, 1, 57, '2018-05-02 09:26:19', 1, 'O'),
(72, 1, 55, '2018-05-02 09:26:19', 1, 'O'),
(73, 1, 58, '2018-05-02 09:26:20', 1, 'O'),
(74, 1, 59, '2018-05-02 09:26:20', 1, 'O'),
(75, 1, 63, '2018-05-03 12:16:29', 1, 'O'),
(76, 1, 62, '2018-05-03 12:24:52', 1, 'O'),
(77, 1, 60, '2018-05-03 12:24:54', 1, 'O'),
(78, 1, 61, '2018-05-03 12:25:04', 1, 'O'),
(79, 4, 2, '2018-05-07 09:34:45', 1, 'O'),
(80, 1, 65, '2018-05-09 10:04:21', 1, 'O'),
(81, 1, 64, '2018-05-09 10:15:40', 1, 'O'),
(82, 1, 66, '2018-05-09 10:30:28', 1, 'O'),
(83, 1, 67, '2018-05-09 10:52:10', 1, 'O'),
(84, 1, 68, '2018-05-09 10:52:12', 1, 'O'),
(85, 1, 69, '2018-05-09 10:52:13', 1, 'O'),
(86, 1, 70, '2018-05-09 10:52:15', 1, 'O'),
(87, 2, 21, '2018-05-11 12:38:12', 1, 'O'),
(88, 2, 22, '2018-05-11 12:38:13', 1, 'O'),
(89, 1, 71, '2018-05-21 07:14:22', 1, 'O'),
(90, 1, 72, '2018-05-21 07:14:22', 1, 'O'),
(91, 1, 73, '2018-05-21 07:14:23', 1, 'O'),
(92, 1, 74, '2018-05-21 07:14:24', 1, 'O'),
(93, 1, 75, '2018-05-21 07:14:25', 1, 'O'),
(94, 1, 76, '2018-05-21 07:14:26', 1, 'O'),
(95, 1, 77, '2018-05-21 07:14:27', 1, 'O'),
(96, 2, 15, '2018-05-25 12:34:59', 3, 'O'),
(97, 2, 20, '2018-05-25 12:35:00', 3, 'O'),
(98, 2, 18, '2018-05-25 12:35:01', 3, 'C'),
(99, 2, 61, '2018-05-25 12:40:06', 3, 'C'),
(100, 2, 43, '2018-05-25 12:40:06', 3, 'C'),
(101, 2, 64, '2018-05-25 12:40:08', 3, 'C'),
(102, 2, 65, '2018-05-25 12:40:08', 3, 'C'),
(103, 2, 28, '2018-05-25 12:40:18', 3, 'O'),
(104, 2, 29, '2018-05-25 12:40:18', 3, 'O'),
(105, 2, 35, '2018-05-25 12:40:19', 3, 'O'),
(106, 2, 36, '2018-05-25 12:40:19', 3, 'O'),
(107, 2, 53, '2018-05-25 12:40:20', 3, 'C'),
(108, 2, 54, '2018-05-25 12:40:20', 3, 'C'),
(109, 2, 71, '2018-05-25 12:40:21', 3, 'C'),
(110, 2, 72, '2018-05-25 12:40:21', 3, 'C'),
(112, 2, 16, '2018-05-25 12:48:48', 3, 'C'),
(113, 2, 62, '2018-05-25 12:59:46', 3, 'O'),
(114, 2, 60, '2018-05-25 13:00:30', 3, 'O'),
(115, 2, 13, '2018-05-25 13:35:43', 1, 'C'),
(116, 1, 106, '2018-05-29 10:15:12', 1, 'O'),
(117, 1, 107, '2018-05-29 10:15:12', 1, 'O'),
(118, 1, 99, '2018-05-29 10:15:13', 1, 'O'),
(119, 1, 100, '2018-05-29 10:15:13', 1, 'O'),
(120, 1, 85, '2018-05-29 10:15:14', 1, 'O'),
(121, 1, 86, '2018-05-29 10:15:14', 1, 'O'),
(122, 1, 78, '2018-05-29 10:15:16', 1, 'O'),
(123, 1, 79, '2018-05-29 10:15:16', 1, 'O'),
(124, 1, 92, '2018-05-29 10:16:41', 1, 'O'),
(125, 1, 93, '2018-05-29 10:16:42', 1, 'O'),
(126, 1, 94, '2018-05-29 10:16:44', 1, 'O'),
(127, 1, 87, '2018-05-29 10:16:48', 1, 'O'),
(128, 1, 80, '2018-05-29 10:16:50', 1, 'O'),
(129, 1, 101, '2018-05-29 10:16:51', 1, 'O'),
(130, 1, 108, '2018-05-29 10:16:51', 1, 'O'),
(131, 1, 81, '2018-05-30 08:26:44', 1, 'O'),
(132, 1, 88, '2018-05-30 08:26:46', 1, 'O'),
(133, 1, 95, '2018-05-30 08:26:47', 1, 'O'),
(134, 1, 102, '2018-05-30 08:26:47', 1, 'O'),
(135, 1, 109, '2018-05-30 08:26:48', 1, 'O'),
(136, 1, 82, '2018-05-30 08:26:49', 1, 'O'),
(137, 1, 89, '2018-05-30 08:26:50', 1, 'O'),
(138, 1, 96, '2018-05-30 08:26:51', 1, 'O'),
(139, 1, 103, '2018-05-30 08:26:52', 1, 'O'),
(140, 1, 110, '2018-05-30 08:26:52', 1, 'O'),
(141, 1, 83, '2018-05-30 08:26:53', 1, 'O'),
(142, 1, 90, '2018-05-30 08:26:54', 1, 'O'),
(143, 1, 97, '2018-05-30 08:26:57', 1, 'O'),
(144, 1, 104, '2018-05-30 08:26:58', 1, 'O'),
(145, 1, 84, '2018-05-30 08:26:58', 1, 'O'),
(146, 1, 111, '2018-05-30 08:26:58', 1, 'O'),
(147, 1, 91, '2018-05-30 08:26:59', 1, 'O'),
(148, 1, 98, '2018-05-30 08:27:00', 1, 'O'),
(149, 1, 105, '2018-05-30 08:27:00', 1, 'O'),
(150, 1, 112, '2018-05-30 08:27:01', 1, 'O'),
(151, 1, 113, '2018-06-04 12:16:24', 1, 'O'),
(152, 1, 119, '2018-06-04 12:16:24', 1, 'O'),
(153, 1, 114, '2018-06-04 12:16:25', 1, 'O'),
(154, 1, 115, '2018-06-04 12:16:26', 1, 'O'),
(155, 1, 116, '2018-06-04 12:16:27', 1, 'O'),
(156, 1, 117, '2018-06-04 12:16:27', 1, 'O'),
(157, 1, 118, '2018-06-04 12:16:28', 1, 'O'),
(158, 1, 120, '2018-06-05 09:55:46', 1, 'O'),
(159, 1, 123, '2018-06-05 09:55:46', 1, 'O'),
(160, 1, 121, '2018-06-05 09:55:46', 1, 'O'),
(161, 1, 122, '2018-06-05 09:55:47', 1, 'O'),
(162, 1, 124, '2018-06-05 09:55:48', 1, 'O'),
(163, 1, 125, '2018-06-05 09:55:49', 1, 'O'),
(164, 1, 126, '2018-06-05 09:55:50', 1, 'O'),
(165, 3, 13, '2018-06-20 05:17:29', 1, 'C'),
(166, 3, 2, '2018-06-20 05:17:29', 1, 'C'),
(167, 4, 13, '2018-06-22 11:58:44', 1, 'O'),
(168, 4, 15, '2018-06-22 11:58:45', 1, 'O'),
(169, 4, 20, '2018-06-22 11:58:45', 1, 'O'),
(170, 4, 16, '2018-06-22 11:58:46', 1, 'O'),
(171, 4, 17, '2018-06-22 11:58:46', 1, 'O'),
(172, 4, 21, '2018-06-22 11:58:47', 1, 'O'),
(173, 4, 22, '2018-06-22 11:58:47', 1, 'O'),
(174, 4, 28, '2018-06-22 11:58:48', 1, 'O'),
(175, 4, 29, '2018-06-22 11:58:48', 1, 'O'),
(176, 4, 35, '2018-06-22 11:58:54', 1, 'O'),
(177, 4, 36, '2018-06-22 11:58:54', 1, 'O'),
(178, 5, 16, '2018-06-22 11:58:57', 1, 'O'),
(179, 5, 17, '2018-06-22 11:58:57', 1, 'O'),
(180, 8, 21, '2018-06-29 09:14:52', 1, 'O'),
(181, 8, 22, '2018-06-29 09:14:52', 1, 'O'),
(182, 9, 21, '2018-06-29 09:14:55', 1, 'O'),
(183, 9, 22, '2018-06-29 09:14:55', 1, 'O'),
(184, 10, 28, '2018-06-29 09:14:58', 1, 'O'),
(185, 10, 29, '2018-06-29 09:14:58', 1, 'O');

-- --------------------------------------------------------

--
-- Table structure for table `sih_screens`
--

CREATE TABLE IF NOT EXISTS `sih_screens` (
  `sId` int(11) NOT NULL,
  `sTitle` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tiêu đề của trang',
  `sRoute` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Đường dẫn đến trang',
  `sRouteId` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sCreatedAt` datetime NOT NULL COMMENT 'Thời điểm tạo',
  `sCreatedBy` int(11) NOT NULL COMMENT 'Người tạo',
  `sStat` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'O: Open; C: Close',
  `sIco` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Danh sách màn hình';

--
-- Dumping data for table `sih_screens`
--

INSERT INTO `sih_screens` (`sId`, `sTitle`, `sRoute`, `sRouteId`, `sCreatedAt`, `sCreatedBy`, `sStat`, `sIco`) VALUES
(2, 'permission_role-get', 'permission_role/get', '62_13', '2018-04-19 00:00:00', 1, 'O', ''),
(3, 'permission_role-add', 'permission_role/add', '62_13', '2018-04-20 00:00:00', 1, 'O', NULL),
(4, 'permission_role-edit', 'permission_role/edit', '62_13', '2018-04-19 00:00:00', 1, 'O', NULL),
(5, 'permission_role-delete', 'permission_role/delete', '62_13', '2018-04-20 00:00:00', 1, 'O', NULL),
(6, 'permission_role-push', 'permission_role/push', '62_13', '2018-04-19 00:00:00', 1, 'O', NULL),
(7, 'permission_role-unpush', 'permission_role/unpush', '62_13', '2018-04-20 00:00:00', 1, 'O', NULL),
(13, 'Quản lý role', 'permission_role', '62', '2018-04-19 00:00:00', 1, 'O', NULL),
(14, 'Dashboard', 'index', '0', '0000-00-00 00:00:00', 0, 'O', 'fa fa-th'),
(15, 'Quản lý role và màn hình', 'permission_role_screen', '62', '0000-00-00 00:00:00', 0, 'O', NULL),
(16, 'Quản lý màn hình', 'permission_screen', '62', '0000-00-00 00:00:00', 0, 'O', NULL),
(17, 'permission_screen/get', 'permission_screen/get', '62_16', '0000-00-00 00:00:00', 0, 'O', NULL),
(18, 'permission_role_screen/edit', 'permission_role_screen/edit', '62_15', '0000-00-00 00:00:00', 0, 'O', NULL),
(19, 'permission_screen/add', 'permission_screen/add', '62_16', '0000-00-00 00:00:00', 0, 'O', NULL),
(20, 'permission_role_screen/get', 'permission_role_screen/get', '62_15', '0000-00-00 00:00:00', 0, 'O', NULL),
(21, 'Danh sách quốc gia', 'list_nations', '60', '0000-00-00 00:00:00', 0, 'o', NULL),
(22, 'list_nations/get', 'list_nations/get', '60_21', '0000-00-00 00:00:00', 0, 'o', NULL),
(23, 'list_nations/add', 'list_nations/add', '60_21', '0000-00-00 00:00:00', 0, 'o', NULL),
(24, 'list_nations/edit', 'list_nations/edit', '60_21', '0000-00-00 00:00:00', 0, 'o', NULL),
(25, 'list_nations/delete', 'list_nations/delete', '60_21', '0000-00-00 00:00:00', 0, 'o', NULL),
(26, 'list_nations/push', 'list_nations/push', '60_21', '0000-00-00 00:00:00', 0, 'o', NULL),
(27, 'list_nations/unpush', 'list_nations/unpush', '60_21', '0000-00-00 00:00:00', 0, 'o', NULL),
(28, 'Danh sách tỉnh thành', 'list_provinces', '60', '0000-00-00 00:00:00', 0, 'o', NULL),
(29, 'list_provinces/get', 'list_provinces/get', '60_28', '0000-00-00 00:00:00', 0, 'o', NULL),
(30, 'list_provinces/add', 'list_provinces/add', '60_28', '0000-00-00 00:00:00', 0, 'o', NULL),
(31, 'list_provinces/edit', 'list_provinces/edit', '60_28', '0000-00-00 00:00:00', 0, 'o', NULL),
(32, 'list_provinces/delete', 'list_provinces/delete', '60_28', '0000-00-00 00:00:00', 0, 'o', NULL),
(33, 'list_provinces/push', 'list_provinces/push', '60_28', '0000-00-00 00:00:00', 0, 'o', NULL),
(34, 'list_provinces/unpush', 'list_provinces/unpush', '60_28', '0000-00-00 00:00:00', 0, 'o', NULL),
(35, 'Danh sách quận huyện', 'list_districts', '60', '2018-05-17 00:00:00', 0, 'O', ''),
(36, 'list_districts/get', 'list_districts/get', '60_35', '0000-00-00 00:00:00', 0, 'o', NULL),
(37, 'list_districts/add', 'list_districts/add', '60_35', '0000-00-00 00:00:00', 0, 'o', NULL),
(38, 'list_districts/edit', 'list_districts/edit', '60_35', '0000-00-00 00:00:00', 0, 'o', NULL),
(39, 'list_districts/delete', 'list_districts/delete', '60_35', '0000-00-00 00:00:00', 0, 'o', NULL),
(40, 'list_districts/push', 'list_districts/push', '60_35', '0000-00-00 00:00:00', 0, 'o', NULL),
(41, 'list_districts/unpush', 'list_districts/unpush', '60_35', '0000-00-00 00:00:00', 0, 'o', NULL),
(42, 'Quản lý nhân sự', '', '0', '0000-00-00 00:00:00', 0, 'O', 'fa fa-users'),
(43, 'manage_user/get', 'manage_user/get', '42_61', '0000-00-00 00:00:00', 0, 'O', NULL),
(44, 'manage_user/add', 'manage_user/add', '42_61', '0000-00-00 00:00:00', 1, 'O', NULL),
(45, 'manage_user/edit', 'manage_user/edit', '42_61', '0000-00-00 00:00:00', 1, 'O', NULL),
(46, 'manage_user/delete', 'manage_user/delete', '42_61', '0000-00-00 00:00:00', 1, 'O', NULL),
(47, 'manage_user/push', 'manage_user/push', '42_61', '0000-00-00 00:00:00', 1, 'O', NULL),
(48, 'permission_screen/edit', 'permission_screen/edit', '62_16', '0000-00-00 00:00:00', 1, 'O', NULL),
(49, 'manage_user/unpush', 'manage_user/unpush', '42_61', '0000-00-00 00:00:00', 1, 'O', NULL),
(50, 'permission_screen/push', 'permission_screen/push', '62_16', '0000-00-00 00:00:00', 1, 'O', NULL),
(51, 'permission_screen/unpush', 'permission_screen/unpush', '62_16', '0000-00-00 00:00:00', 1, 'O', NULL),
(52, 'profile', 'profile', '0', '0000-00-00 00:00:00', 1, 'O', NULL),
(53, 'Danh sách phường xã', 'list_towns', '60', '2018-05-17 00:00:00', 1, 'O', ''),
(54, 'Danh sách tỉnh thành get', 'list_towns/get', '60_53', '0000-00-00 00:00:00', 0, 'O', NULL),
(55, 'Danh sách tỉnh thành add', 'list_towns/add', '60_53', '0000-00-00 00:00:00', 0, 'O', NULL),
(56, 'Danh sách tỉnh thành edit', 'list_towns/edit', '60_53', '0000-00-00 00:00:00', 0, 'O', NULL),
(57, 'Danh sách tỉnh thành delete', 'list_towns/delete', '60_53', '0000-00-00 00:00:00', 0, 'O', NULL),
(58, 'Danh sách tỉnh thành push', 'list_towns/push', '60_53', '0000-00-00 00:00:00', 0, 'O', NULL),
(59, 'Danh sách tỉnh thành unpush', 'list_towns/unpush', '60_53', '0000-00-00 00:00:00', 0, 'O', NULL),
(60, 'Danh mục', '', '0', '0000-00-00 00:00:00', 1, 'O', 'fa fa-th-list'),
(61, 'Danh sách nhân sự', 'manage_user', '42', '0000-00-00 00:00:00', 0, 'O', NULL),
(62, 'Quản lý phân quyền', '', '0', '0000-00-00 00:00:00', 0, 'O', 'fa fa-users'),
(63, 'permission_screen/delete', 'permission_screen/delete', '62_16', '0000-00-00 00:00:00', 0, 'O', NULL),
(64, 'Danh sách chức vụ/nghề nghiệp', 'list_titles', '60', '0000-00-00 00:00:00', 1, 'O', ''),
(65, 'Danh mục chức vụ get', 'list_titles/get', '60_64', '0000-00-00 00:00:00', 1, 'O', NULL),
(66, 'Danh mục chức vụ add', 'list_titles/add', '60_64', '0000-00-00 00:00:00', 1, 'O', NULL),
(67, 'Danh mục chức vụ edit', 'list_titles/edit', '60_64', '0000-00-00 00:00:00', 1, 'O', NULL),
(68, 'Danh mục chức vụ delete', 'list_titles/delete', '60_64', '0000-00-00 00:00:00', 1, 'O', NULL),
(69, 'Danh mục chức vụ push', 'list_titles/push', '60_64', '0000-00-00 00:00:00', 1, 'O', NULL),
(70, 'Danh mục chức vụ unpush', 'list_titles/unpush', '60_64', '0000-00-00 00:00:00', 1, 'O', NULL),
(71, 'Danh sách phòng ban', 'list_departments', '60', '2018-05-21 00:00:00', 1, 'O', ''),
(72, 'Danh sách phòng ban/get', 'list_departments/get', '60_71', '2018-05-21 00:00:00', 1, 'O', ''),
(73, 'Danh sách phòng ban/add', 'list_departments/add', '60_71', '2018-05-21 00:00:00', 1, 'O', ''),
(74, 'Danh sách phòng ban/edit', 'list_departments/edit', '60_71', '2018-05-21 00:00:00', 1, 'O', ''),
(75, 'Danh sách phòng ban/delete', 'list_departments/delete', '60_71', '2018-05-21 00:00:00', 1, 'O', ''),
(76, 'Danh sách phòng ban/push', 'list_departments/push', '60_71', '2018-05-21 00:00:00', 1, 'O', ''),
(77, 'Danh sách phòng ban/unpush', 'list_departments/unpush', '60_71', '2018-05-21 00:00:00', 1, 'O', ''),
(78, 'Nữ Hộ Sinh', 'list_midwives', '60', '2018-05-29 00:00:00', 1, 'O', ''),
(79, 'get', 'list_midwives/get', '60_78', '2018-05-29 00:00:00', 1, 'O', ''),
(80, 'add', 'list_midwives/add', '60_78', '2018-05-29 00:00:00', 1, 'O', ''),
(81, 'edit', 'list_midwives/edit', '60_78', '2018-05-29 00:00:00', 1, 'O', ''),
(82, 'delete', 'list_midwives/delete', '60_78', '2018-05-29 00:00:00', 1, 'O', ''),
(83, 'push', 'list_midwives/push', '60_78', '2018-05-29 00:00:00', 1, 'O', ''),
(84, 'unpush', 'list_midwives/unpush', '60_78', '2018-05-29 00:00:00', 1, 'O', ''),
(85, 'Danh Mục Vật Tư', 'list_materials', '60', '2018-05-29 00:00:00', 1, 'O', ''),
(86, 'get', 'list_materials/get', '60_85', '2018-05-29 00:00:00', 1, 'O', ''),
(87, 'add', 'list_materials/add', '60_85', '2018-05-29 00:00:00', 1, 'O', ''),
(88, 'edit', 'list_materials/edit', '60_85', '2018-05-29 00:00:00', 1, 'O', ''),
(89, 'delete', 'list_materials/delete', '60_85', '2018-05-29 00:00:00', 1, 'O', ''),
(90, 'push', 'list_materials/push', '60_85', '2018-05-29 00:00:00', 1, 'O', ''),
(91, 'unpush', 'list_materials/unpush', '60_85', '2018-05-29 00:00:00', 1, 'O', ''),
(92, 'Nhóm Khám Chữa Bệnh', 'list_group_therapys', '60', '2018-05-29 00:00:00', 1, 'O', ''),
(93, 'get', 'list_group_therapys/get', '60_92', '2018-05-29 00:00:00', 1, 'O', ''),
(94, 'add', 'list_group_therapys/add', '60_92', '2018-05-29 00:00:00', 1, 'O', ''),
(95, 'edit', 'list_group_therapys/edit', '60_92', '2018-05-29 00:00:00', 1, 'O', ''),
(96, 'delete', 'list_group_therapys/delete', '60_92', '2018-05-29 00:00:00', 1, 'O', ''),
(97, 'push', 'list_group_therapys/push', '60_92', '2018-05-29 00:00:00', 1, 'O', ''),
(98, 'unpush', 'list_group_therapys/unpush', '60_92', '2018-05-29 00:00:00', 1, 'O', ''),
(99, 'Lý Do Chuyển Viện', 'reason_for_transfer', '60', '2018-05-29 00:00:00', 1, 'O', ''),
(100, 'get', 'reason_for_transfer/get', '60_99', '2018-05-29 00:00:00', 1, 'O', ''),
(101, 'add', 'reason_for_transfer/add', '60_99', '2018-05-29 00:00:00', 1, 'O', ''),
(102, 'edit', 'reason_for_transfer/edit', '60_99', '2018-05-29 00:00:00', 1, 'O', ''),
(103, 'delete', 'reason_for_transfer/delete', '60_99', '2018-05-29 00:00:00', 1, 'O', ''),
(104, 'push', 'reason_for_transfer/push', '60_99', '2018-05-29 00:00:00', 1, 'O', ''),
(105, 'unpush', 'reason_for_transfer/unpush', '60_99', '2018-05-29 00:00:00', 1, 'O', ''),
(106, 'Lý Do Xuất Viện', 'reason_for_discharge', '60', '2018-05-29 00:00:00', 1, 'O', ''),
(107, 'get', 'reason_for_discharge/get', '60_106', '2018-05-29 00:00:00', 1, 'O', ''),
(108, 'add', 'reason_for_discharge/add', '60_106', '2018-05-29 00:00:00', 1, 'O', ''),
(109, 'edit', 'reason_for_discharge/edit', '60_106', '2018-05-29 00:00:00', 1, 'O', ''),
(110, 'delete', 'reason_for_discharge/delete', '60_106', '2018-05-29 00:00:00', 1, 'O', ''),
(111, 'push', 'reason_for_discharge/push', '60_106', '2018-05-29 00:00:00', 1, 'O', ''),
(112, 'unpush', 'reason_for_discharge/unpush', '60_106', '2018-05-29 00:00:00', 1, 'O', ''),
(113, 'Danh mục nhà cung cấp', 'list_vendors', '60', '2018-06-04 00:00:00', 1, 'O', ''),
(114, 'add', 'list_vendors/add', '60_113', '2018-06-04 00:00:00', 1, 'O', ''),
(115, 'edit', 'list_vendors/edit', '60_113', '2018-06-04 00:00:00', 1, 'O', ''),
(116, 'delete', 'list_vendors/delete', '60_113', '2018-06-04 00:00:00', 1, 'O', ''),
(117, 'push', 'list_vendors/push', '60_113', '2018-06-04 00:00:00', 1, 'C', ''),
(118, 'unpush', 'list_vendors/unpush', '60_113', '2018-06-04 00:00:00', 1, 'C', ''),
(119, 'get', 'list_vendors/get', '60_113', '2018-06-04 00:00:00', 1, 'O', ''),
(120, 'Danh sách tiền tệ', 'list_currencys', '60', '2018-06-05 00:00:00', 1, 'O', ''),
(121, 'add', 'list_currencys/add', '60_120', '2018-06-05 00:00:00', 1, 'O', ''),
(122, 'edit', 'list_currencys/edit', '60_120', '2018-06-05 00:00:00', 1, 'O', ''),
(123, 'get', 'list_currencys/get', '60_120', '2018-06-05 00:00:00', 1, 'O', ''),
(124, 'delete', 'list_currencys/delete', '60_120', '2018-06-05 00:00:00', 1, 'O', ''),
(125, 'push', 'list_currencys/push', '60_120', '2018-06-05 00:00:00', 1, 'O', ''),
(126, 'unpush', 'list_currencys/unpush', '60_120', '2018-06-05 00:00:00', 1, 'O', '');

-- --------------------------------------------------------

--
-- Table structure for table `sih_users`
--

CREATE TABLE IF NOT EXISTS `sih_users` (
  `uId` int(11) NOT NULL,
  `rId` int(11) DEFAULT NULL COMMENT 'Mã quyền',
  `ltId` int(11) DEFAULT NULL COMMENT 'Mã chức danh',
  `uName` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uEmail` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `uPhone` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `uPassword` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `uAvatar` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uCreatedAt` datetime NOT NULL,
  `uCreatedBy` int(11) NOT NULL COMMENT 'Người tạo',
  `uStat` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'O: Open; C: Close'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_users`
--

INSERT INTO `sih_users` (`uId`, `rId`, `ltId`, `uName`, `uEmail`, `uPhone`, `uPassword`, `uAvatar`, `uCreatedAt`, `uCreatedBy`, `uStat`) VALUES
(1, 1, 7, 'Admin John 3', 'john@ea.com', '10213123123', 'e10adc3949ba59abbe56e057f20f883e', 'uploads/avatas/1_15247335295.jpeg', '2018-02-09 00:00:00', 0, 'O'),
(2, 2, 2, 'Khoa 1', 'khoaxt1@gmail.com', '0987789987', 'fcea920f7412b5da7be0cf42b8c93759', '', '2017-11-28 14:27:01', 0, 'O'),
(3, 2, 4, 'testuser', 'ch2au@gmail.com', '987123789123', 'e10adc3949ba59abbe56e057f20f883e', '', '0000-00-00 00:00:00', 0, 'O');

-- --------------------------------------------------------

--
-- Table structure for table `sih_user_profiles`
--

CREATE TABLE IF NOT EXISTS `sih_user_profiles` (
  `upId` int(11) NOT NULL,
  `uId` int(11) NOT NULL COMMENT 'Mã người dùng',
  `ldId` tinyint(4) DEFAULT NULL COMMENT 'Mã khoa/phòng ban',
  `upBirthday` date DEFAULT NULL COMMENT 'Ngày sinh',
  `upAddress` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Địa chỉ nhà',
  `upRelativeContact` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '0908123456 - A Tuấn - Chồng',
  `upIntro` text COLLATE utf8_unicode_ci COMMENT 'Giới thiệu + bằng cấp',
  `upDegree` text COLLATE utf8_unicode_ci COMMENT 'Đường dẫn tới hình ảnh bằng cấp, chứng chỉ',
  `upUpdatedAt` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sih_user_profiles`
--

INSERT INTO `sih_user_profiles` (`upId`, `uId`, `ldId`, `upBirthday`, `upAddress`, `upRelativeContact`, `upIntro`, `upDegree`, `upUpdatedAt`) VALUES
(1, 1, NULL, '2000-09-18', '19 Phó Đức Chính', NULL, 'giới thiệu bằng cấp', NULL, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sih_form1`
--
ALTER TABLE `sih_form1`
  ADD PRIMARY KEY (`f1Id`);

--
-- Indexes for table `sih_list_currencys`
--
ALTER TABLE `sih_list_currencys`
  ADD PRIMARY KEY (`lcId`);

--
-- Indexes for table `sih_list_departments`
--
ALTER TABLE `sih_list_departments`
  ADD PRIMARY KEY (`ldId`);

--
-- Indexes for table `sih_list_districts`
--
ALTER TABLE `sih_list_districts`
  ADD PRIMARY KEY (`ldId`);

--
-- Indexes for table `sih_list_group_therapys`
--
ALTER TABLE `sih_list_group_therapys`
  ADD PRIMARY KEY (`lgtId`);

--
-- Indexes for table `sih_list_materials`
--
ALTER TABLE `sih_list_materials`
  ADD PRIMARY KEY (`lmId`);

--
-- Indexes for table `sih_list_midwives`
--
ALTER TABLE `sih_list_midwives`
  ADD PRIMARY KEY (`lmId`);

--
-- Indexes for table `sih_list_nations`
--
ALTER TABLE `sih_list_nations`
  ADD PRIMARY KEY (`lnId`);

--
-- Indexes for table `sih_list_provinces`
--
ALTER TABLE `sih_list_provinces`
  ADD PRIMARY KEY (`lpId`);

--
-- Indexes for table `sih_list_titles`
--
ALTER TABLE `sih_list_titles`
  ADD PRIMARY KEY (`ltId`);

--
-- Indexes for table `sih_list_towns`
--
ALTER TABLE `sih_list_towns`
  ADD PRIMARY KEY (`ltId`), ADD KEY `ldId` (`ldId`);

--
-- Indexes for table `sih_list_vendors`
--
ALTER TABLE `sih_list_vendors`
  ADD PRIMARY KEY (`lvId`);

--
-- Indexes for table `sih_reason_for_discharge`
--
ALTER TABLE `sih_reason_for_discharge`
  ADD PRIMARY KEY (`rfdId`);

--
-- Indexes for table `sih_reason_for_transfer`
--
ALTER TABLE `sih_reason_for_transfer`
  ADD PRIMARY KEY (`rftId`);

--
-- Indexes for table `sih_roles`
--
ALTER TABLE `sih_roles`
  ADD PRIMARY KEY (`rId`);

--
-- Indexes for table `sih_role_screens`
--
ALTER TABLE `sih_role_screens`
  ADD PRIMARY KEY (`rsId`);

--
-- Indexes for table `sih_screens`
--
ALTER TABLE `sih_screens`
  ADD PRIMARY KEY (`sId`);

--
-- Indexes for table `sih_users`
--
ALTER TABLE `sih_users`
  ADD PRIMARY KEY (`uId`);

--
-- Indexes for table `sih_user_profiles`
--
ALTER TABLE `sih_user_profiles`
  ADD PRIMARY KEY (`upId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sih_form1`
--
ALTER TABLE `sih_form1`
  MODIFY `f1Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sih_list_currencys`
--
ALTER TABLE `sih_list_currencys`
  MODIFY `lcId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sih_list_departments`
--
ALTER TABLE `sih_list_departments`
  MODIFY `ldId` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `sih_list_districts`
--
ALTER TABLE `sih_list_districts`
  MODIFY `ldId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `sih_list_group_therapys`
--
ALTER TABLE `sih_list_group_therapys`
  MODIFY `lgtId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sih_list_materials`
--
ALTER TABLE `sih_list_materials`
  MODIFY `lmId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sih_list_midwives`
--
ALTER TABLE `sih_list_midwives`
  MODIFY `lmId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sih_list_nations`
--
ALTER TABLE `sih_list_nations`
  MODIFY `lnId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sih_list_provinces`
--
ALTER TABLE `sih_list_provinces`
  MODIFY `lpId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sih_list_titles`
--
ALTER TABLE `sih_list_titles`
  MODIFY `ltId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sih_list_towns`
--
ALTER TABLE `sih_list_towns`
  MODIFY `ltId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sih_list_vendors`
--
ALTER TABLE `sih_list_vendors`
  MODIFY `lvId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sih_reason_for_discharge`
--
ALTER TABLE `sih_reason_for_discharge`
  MODIFY `rfdId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sih_reason_for_transfer`
--
ALTER TABLE `sih_reason_for_transfer`
  MODIFY `rftId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sih_roles`
--
ALTER TABLE `sih_roles`
  MODIFY `rId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `sih_role_screens`
--
ALTER TABLE `sih_role_screens`
  MODIFY `rsId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=186;
--
-- AUTO_INCREMENT for table `sih_screens`
--
ALTER TABLE `sih_screens`
  MODIFY `sId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT for table `sih_users`
--
ALTER TABLE `sih_users`
  MODIFY `uId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sih_user_profiles`
--
ALTER TABLE `sih_user_profiles`
  MODIFY `upId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `sih_list_towns`
--
ALTER TABLE `sih_list_towns`
ADD CONSTRAINT `sih_list_towns_ibfk_1` FOREIGN KEY (`ldId`) REFERENCES `sih_list_districts` (`ldId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
