jQuery(document).ready(function() {
    $('[data-example="example_more"]').DataTable(
		{
			"pageLength": 100
		}
    );
    
    
	
    
    $('body ').on('click', '.table-responsive table tbody input[type="checkbox"]', (function () {
        var c_current = 0;
        $('[data-example="example_more"] td input[type="checkbox"]').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                c_current++;
            }
        });
        //alert(c_current);
        show_hide_button(c_current);
    }));

    
    $('body ').on('click', 'input.auto_for_clone_value:checkbox,input.auto_for_clone_value:radio', (function () {
        //alert('sf')
        //alert(this.value)
        var value_input  =this.value;
        if($(this).attr('type')=='checkbox'){
            if(this.checked == true){
                //alert('checked')
                value_input ='t';//true
            }else{
                //alert('not chekck')
                value_input ='';
            }
        }
        //alert(value_input)
        $(this).closest('div').children('input').first().val(value_input);
    }));

    function show_hide_button(c_current) {
        if (c_current == 0) {
            //Do stuff
            //alert('hello')
            $('#btn_edit').addClass('disabled');
            $('#btn_publish').addClass('disabled');
            $('#btn_unpublish').addClass('disabled');
            $('#btn_delete').addClass('disabled');
        }
        if (c_current == 1) {
            $('#btn_edit').removeClass('disabled');
            $('#btn_publish').removeClass('disabled');
            $('#btn_unpublish').removeClass('disabled');
            $('#btn_delete').removeClass('disabled');
        }
        if (c_current > 1) {
            $('#btn_edit').addClass('disabled');
            $('#btn_publish').removeClass('disabled');
            $('#btn_unpublish').removeClass('disabled');
            $('#btn_delete').removeClass('disabled');
        }
    }
	
} );

