jQuery(document).ready(function() {
    var table1 = $('[data-example="example_more_demo_1"]').DataTable(
		{
            "pageLength": 100,
            "sDom": "<'row'<'col-sm-6'f><'col-sm-6'l>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
        },
        
    );
    var table2 = $('[data-example="example_more_demo_2"]').DataTable(
		{
            "pageLength": 100,
            "sDom": "<'row'<'col-sm-6'f><'col-sm-6'l>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
        },
        
    );
    var table3 = $('[data-example="example_more_demo_3"]').DataTable(
		{
            "pageLength": 100,
            "sDom": "<'row'<'col-sm-6'f><'col-sm-6'l>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
        },
        
    );
    var table4 = $('[data-example="example_more_demo_4"]').DataTable(
		{
            "pageLength": 100,
            "sDom": "<'row'<'col-sm-6'f><'col-sm-6'l>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
        },
        
    );
})