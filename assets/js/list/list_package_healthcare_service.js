
var table_category_all;
var option_config_table;

var glb_data_json_healthcare;

+function ($) {
    "use strict";

    $(function () {

        $('#datetimepicker1').datepicker();


        

        call_load_tfooter();
        function call_load_tfooter(){

            $('#category_name [data-example="example_more"] tfoot th').each( function (i) {
                
                if(i==0 || $(this).attr('att-edit') == 'false'){
                    $(this).html('<input type="text" class="hide" />');
                    // $(this).html('');
                }else{
                    var get_tite = $(this).attr('data-type-select');
                    var content_show='';
                    if(get_tite != undefined){
                        //for select
                        var content_default = template_status_vi[get_tite];
                        content_show = '<select class="select-at-table">'+content_default+'</select>';
                        content_show +='<input type="text" class="hide" />'
                    }else{
                        //for text
                        var title = $('#category_name [data-example="example_more"] thead th').eq( $(this).index() ).text();
                        content_show=  '<input type="text" placeholder="Add '+title+'" data-index="'+i+'" />';
                    }
                    $(this).html(content_show);
                }
                
            });
        }

        $('body ').on('focusout change', '#category_name .table-responsive [data-example="example_more"] tbody tr td .select-at-table', (function (e) {
            var col = $(this).closest('td').parent('tr').children('td').index($(this).closest('td'));
            var vl = $(this).val();
            $('#category_name .table-responsive [data-example="example_more"] tfoot th').eq( col ).find('.select-at-table').val(vl);
            $('#category_name .table-responsive [data-example="example_more"] tfoot th').eq( col ).find('.select-at-table').trigger('change');
        }))
        $('body ').on('focusout change', '#category_name .table-responsive [data-example="example_more"] tfoot th .select-at-table', (function (e) {
            var col = $(this).closest('th').parent('tr').children('th').index($(this).closest('th'));
            var vl = $(this).val();
            var attr = $(this).closest('th').attr('data-trigger');
            if(attr !=undefined){
                var attr_value= "th[data-type-select*='"+attr+"']";
                $('#category_name [data-example="example_more"] tfoot .select-at-table option').show();
                $('#category_name [data-example="example_more"] tfoot '+attr_value+' .select-at-table option[att-forent=' + vl + "]").hide();
            }
            
            //alert(vl)
            $(this).closest('th').find('input').val(vl);
        }))
        //when text change-> select update?
        

        $('#category_name [data-example="example_more"] td span:nth-child(1)').click(function () {
            //alert('edit');
            var findlisttd = $(this).closest('tr').find('td');
            var leng = $(findlisttd).length;
            var list = [];
            $(findlisttd).each(function (index) {
                if (index != (leng - 1)) {
                    //console.log(index,leng)
                    console.log($(this).html()); //value of td in row different column edit/delete.
                    list.push($(this).html());
                }
            });
            get_allinptu_edit(list);
            $('.nav-tabs a[href="#edit"]').tab('show');
            //console.log(a);
        });
        //        $('#category_name [data-example="example_more"] td:nth-child(2)').click(function(){
        //            alert('delete in working');
        //            
        //
        //        });

        //        $('body').on('click', '#DataTables_Table_0 .sorting_1', (function () {
        //            alert('delete in working');
        //        }));

        $('body ').on('click', '#category_name .table-responsive [data-example="example_more"] tbody #onchange', (function (e) {
            e.preventDefault();
            e.stopPropagation();
        }));
        $('body ').on('click', '#category_name .table-responsive [data-example="example_more"] tbody #onchange_add', (function (e) {
            e.preventDefault();
            e.stopPropagation();
        }));
        $('body ').on('click', '#category_name .table-responsive [data-example="example_more"] tbody #onchange_edit', (function (e) {
            e.preventDefault();
            e.stopPropagation();
        }));
        $('body ').on('keyup focusout', '#category_name .table-responsive [data-example="example_more"] tbody #onchange', (function (e) {
            console.log(e)
            e.preventDefault();
            e.stopPropagation();
            if(e.which == 13 || e.type=='focusout') {
                //alert('You pressed enter! save data');
                var datacurrent = $(this).val(); //value of text input /select/checkbook/datatime/date                
                console.log(datacurrent);
                //var id_datacurrent = $(this).closest('tr').find('td:first').text();
                var id_datacurrent = $(this).closest('tr').attr('id_colum');
                // console.log(id_datacurrent);
                var positionat = $(this).parent().index(); //get index curent in td
                //positionat = parseInt(positionat)-1;
                // console.log(positionat);
                //save data
                var data_first={};
                var data_save ={};

                var data_send1={};
                var colum_id1 = $('#category_name .table-responsive [data-example="example_more"] thead tr').find('th').eq(0).attr('att-name');//get attribute column name current edit
                var colum_id2 = $('#category_name .table-responsive [data-example="example_more"] thead tr').find('th').eq(positionat).attr('att-name');//get attribute column name current edit
                if(!colum_id1){
                    console.error('Not set attibute for id save data');
                    colum_id1=0;
                    return;
                }
                if(!colum_id2){
                    console.error('Not set attibute for id save data');
                    colum_id1=1;
                    return;
                }
                data_send1[colum_id1]=id_datacurrent;
                data_send1[colum_id2]=datacurrent;
                data_save = data_send1;

                var data_first={};
                data_first[colum_id1]=id_datacurrent;
                data_first[colum_id2]=value_first;
                
                var at_positioninfo=this;
                if(value_first==datacurrent){
                    if(is_onchange==true){
                        $(at_positioninfo).closest('td').text(datacurrent);
                        is_onchange=false;
                    }
                    return;
                }
                
                var result= save_edit_list(data_save,data_first,function(data){
                    // alert('callbac')
                    console.log(data)
                    if(is_onchange==true){
                        $(at_positioninfo).closest('td').text(datacurrent);
                        is_onchange=false;
                    }
                });
            }
            if (e.keyCode == 27) { // escape key maps to keycode `27`
                // <DO YOUR WORK HERE>
                // alert('y pressed key esc')
                $(this).closest('td').text(value_first);
                // value_first='';
                if(is_onchange==true){
                    is_onchange=false;
                }
            }
        }));
        $('body ').on('keyup focusout', '#category_name .table-responsive [data-example="example_more_add"] tbody #onchange_add', (function (e) {
            console.log(e)
            e.preventDefault();
            e.stopPropagation();
            if(e.which == 13 || e.type=='focusout') {
                console.log('changeinput');
                var datacurrent = $(this).val(); //value of text input /select/checkbook/datatime/date                
                // var id_datacurrent = $(this).closest('tr').attr('id_colum');
                // var positionat = $(this).parent().index(); //get index curent in td
                
                //var at_positioninfo=this;
                // if(value_first_add==datacurrent){
                //     if(is_onchange_add==true){
                //         $(at_positioninfo).closest('td').text(datacurrent);
                //         is_onchange_add=false;
                //     }
                //     return;
                // }
                is_onchange_add=false;
                $(this).closest('td').html(datacurrent);
                
            }
            if (e.keyCode == 27) { // escape key maps to keycode `27`
                $(this).closest('td').text(value_first_add);
                if(is_onchange_add==true){
                    is_onchange_add=false;
                }
            }
        }));

        $('body ').on('keyup focusout', '#category_name .table-responsive [data-example="example_more_edit"] tbody #onchange_edit', (function (e) {
            //console.log(e)
            e.preventDefault();
            e.stopPropagation();
            if(e.which == 13 || e.type=='focusout') {
                //console.log('changeinput');
                var datacurrent = $(this).val(); //value of text input /select/checkbook/datatime/date                
                // var id_datacurrent = $(this).closest('tr').attr('id_colum');
                 var positionat = $(this).parent().index(); //get index curent in td
                
                //var at_positioninfo=this;
                // if(value_first_add==datacurrent){
                //     if(is_onchange_add==true){
                //         $(at_positioninfo).closest('td').text(datacurrent);
                //         is_onchange_add=false;
                //     }
                //     return;
                // }
                var info_id = $(this).closest('tr').attr('data-newedit');
                // var info_name = $(this).closest('tr').attr('id_colum');
                var col_name = $('#category_name .table-responsive [data-example="example_more_edit"] thead tr').find('th').eq(positionat).attr('att-edit-name');
                is_onchange_edit=false;
                $(this).closest('td').html(datacurrent);
                var list_in ={};
                var tb ='list_service_in_package_healthcare';
                list_in[tb]={};
                list_in[tb]['id']=info_id;
                list_in[tb][col_name]=datacurrent;
                save_edit_one_column(list_in,function(){
                    reload_edit_table();
                });
                
            }
            if (e.keyCode == 27) { // escape key maps to keycode `27`
                $(this).closest('td').text(value_first_edit);
                if(is_onchange_edit==true){
                    is_onchange_edit=false;
                }
            }
        }));

        function save_edit_one_column(list_in,callback){
            //id_edit_package_healthcare=list_in[tb]['id'];
            $.ajax({
                url: $('#category_name').attr('data-cat') + '/edit',
                type: 'POST',
                dataType: "json",
                data: {
                    //category_name: $('#category_name').attr('data-cat'),
                    'input': list_in,
                    'type':'edit_save_by_tb_col',
                },
                beforeSend: function () {

                },
                complete: function () {
                    //$('#category_name [data-example="example_more"]').DataTable().ajax.reload(function(json){after_reload_ajax_datatable(json)},false);
                    //$('#myEdit').modal('toggle');
                    //show_hide_button(0);
                    //reload_main_table();
                    callback('true')
                    
                    //save_edit_example_more_edit(id_edit_package_healthcare)
                },
                success: function (data) {
                    console.log(data);

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            })
        }

        /*
        $('body ').on('focusout', '#category_name .table-responsive [data-example="example_more"] tbody #onchange', (function (e) {
            alert('go out')
            // e.preventDefault();
            // e.stopPropagation();
            $(this).delay(800).fadeIn();
            if(is_onchange==true){
                is_onchange=false;
                //$(this).html('<input type="text" id="onchange" value="'+datacurrent+'">');

                var datacurrent = $(this).val();
                $(this).closest('td').text(datacurrent);
            }
        }));
        */
        $('body ').on('mouseleave', '#category_name .table-responsive [data-example="example_more"] tbody #onchange', (function (e) {
            return;
            // e.preventDefault();
            // e.stopPropagation();
            // alert('move over');
            //delay( 800 )
            // $(this).closest('td');
            // $(this).delay("slow");
            $(this).delay(800).fadeIn();
            if(is_onchange==true){
                is_onchange=false;
                //$(this).html('<input type="text" id="onchange" value="'+datacurrent+'">');

                var datacurrent = $(this).val();
                $(this).closest('td').text(datacurrent);
            }
        }));
        var value_first=''; //text/ select value
        var value_first_add=''; //text/ select value
        var value_first_edit=''; //text/ select value
        var value_first_html=''; //select html first
        var value_first_html_add=''; //select html first
        var value_first_html_edit=''; //select html first
        var is_onchange=false;
        $('body ').on('dblclick', '#category_name .table-responsive [data-example="example_more"] tbody tr td', (function (e) {
            e.preventDefault();
            // console.log($(this).html());
            // console.log($(this).attr('type'));
            if ($(this).html() == '<input type="checkbox">') {
                return;
            }
            var col = $(this).parent().children().index($(this));
            var gettemplate =$('#category_name .table-responsive [data-example="example_more"] tfoot tr th').eq(col);
            if($(gettemplate).attr('att-edit') == 'false'){
                return
            }
            if(is_onchange==true){
                return;
            }
            // console.log(this.value())
            // console.log(this.text())
            console.log($(this).html());
            var content_html = $(this).html();
            var atr = $(content_html).attr('att-value');
            //alert('kjahsdkjfhakjsdf')
            var datacurrent;
            
            //return;
            if(atr !=undefined){
                var col = $(this).parent().children().index($(this));
                //alert(col)
                var gettemplate =$('#category_name .table-responsive [data-example="example_more"] tfoot tr th').eq(col).html();
                //alert(gettemplate);
                //gettemplate ='<select>'+gettemplate+'</select>';
                $(this).html(gettemplate);
                $(this).find('select').val(atr);
                // $(this)$('#selectBox option').eq(atr).prop('selected', true);
                // $(this).find('option').eq(atr).prop('selected', true);
                // $(this).val("3");
                datacurrent = atr;
                value_first_html = content_html;
            }else{
                datacurrent = $(this).text();
                $(this).html('<input type="text" id="onchange" value="'+datacurrent+'">');
                $( "#onchange" ).focus();
            }
            value_first=datacurrent;
            is_onchange=true;
        }));
        $('body ').on('keyup focusout', '#category_name .table-responsive [data-example="example_more"] tbody tr td select', (function (e) {
            e.preventDefault();
            e.stopPropagation();
            if(e.which == 13 || e.type=='focusout') {
                var datacurrent = $(this).val(); //value of text input /select/checkbook/datatime/date
                // var datacurrent_text = $(this).text();
                var datacurrent_text = $(this).find('option:selected').text();
                //console.log(datacurrent);
                //var id_datacurrent = $(this).closest('tr').find('td:first').text();
                var id_datacurrent = $(this).closest('tr').attr('id_colum');
                // console.log(id_datacurrent);
                var positionat = $(this).parent().index(); //get index curent in td
                //positionat = parseInt(positionat)-1;
                // console.log(positionat);
                //save data
                var data_first={};
                var data_save ={};

                var data_send1={};
                var colum_id1 = $('#category_name .table-responsive [data-example="example_more"] thead tr').find('th').eq(0).attr('att-name');//get attribute column name current edit
                var colum_id2 = $('#category_name .table-responsive [data-example="example_more"] thead tr').find('th').eq(positionat).attr('att-name');//get attribute column name current edit
                if(!colum_id1){
                    console.error('Not set attibute for id save data');
                    colum_id1=0;
                }
                if(!colum_id2){
                    console.error('Not set attibute for id save data');
                    colum_id1=1;
                }
                data_send1[colum_id1]=id_datacurrent;
                data_send1[colum_id2]=datacurrent;
                data_save = data_send1;

                var data_first={};
                data_first[colum_id1]=id_datacurrent;
                data_first[colum_id2]=value_first;
                
                var at_positioninfo=this;
                if(value_first==datacurrent){
                    if(is_onchange==true){
                        $(this).closest('td').html(value_first_html);
                        is_onchange=false;
                    }
                    return;
                }
                var result= save_edit_list(data_save,data_first,function(data){
                    // alert('callbac')
                    console.log(data)
                    if(is_onchange==true){
                        // $(at_positioninfo).closest('td').text(datacurrent);
                        var add_show ='<span att-value="'+datacurrent+'">'+datacurrent_text+'</span>';
		                $(at_positioninfo).closest('td').html(add_show);
                        is_onchange=false;
                    }
                });
            }
            if (e.keyCode == 27) { // escape key maps to keycode `27`
                // <DO YOUR WORK HERE>
                // alert('y pressed key esc')
                // $(this).closest('td').text(value_first);
                $(this).closest('td').html(value_first_html);
                // value_first='';
                if(is_onchange==true){
                    is_onchange=false;
                }
            }
        }));
        $('body ').on('click', '#category_name .table-responsive [data-example="example_more"] thead input[type="checkbox"]', (function () {
            var sThisVal = $(this).prop('checked');
            $('#category_name .table-responsive [data-example="example_more"] tbody input[type="checkbox"]').each(function () {
                 $(this).prop('checked',sThisVal);
            });
            show_hide_button();
        }));
        
        $('body ').on('change', '#category_name .table-responsive [data-example="example_more"] tbody input[type="checkbox"]', (function () {
            show_hide_button();
        }));

        $('body ').on('keyup', '#category_name .table-responsive [data-example="example_more"] tfoot tr th', (function (e) {
            e.preventDefault();
            e.stopPropagation();
            //enter 
            if(e.which == 13) {
                var list_data={};
                $('#category_name .table-responsive [data-example="example_more"] tfoot tr th input').each(function (index, value) {
                    var index_text =index;
                    if(get_attribute_name_column(index_text) != 0 && get_attribute_name_column(index_text) != undefined){
                        list_data[get_attribute_name_column(index_text)]=$(this).val();
                    }
                    
                });
                save_new_data(list_data,function(a){
                    $('#category_name .table-responsive [data-example="example_more"] tfoot tr th input').each(function (index, value) {
                        var get_attr_clas=$(this).closest('th').attr('data-type-select');
                        if(get_attr_clas==undefined){
                            $(this).val('');
                        }
                    });
                });
            }
            if(e.which == 27) {

            }
        }));

        var is_onchange_add =false;
        $('body ').on('dblclick', '#category_name table[data-example="example_more_add"]  tbody tr td', (function (e) {
            e.preventDefault();
            if ($(this).html() == '<input type="checkbox">') {
                return;
            }
            var col = $(this).parent().children().index($(this));
            var gettemplate =$('#category_name table[data-example="example_more_add"] tfoot tr th').eq(col);
            if($(gettemplate).attr('att-edit') == 'false'){
                return
            }
            if(is_onchange_add==true){
                return;
            }
            var content_html = $(this).html();
            var atr = $(content_html).attr('att-value');
            var datacurrent;
            //alert(atr)
            if(atr !=undefined){
                //var col = $(this).parent().children().index($(this));
                var gettemplate =$('#category_name table[data-example="example_more_add"] tfoot tr th').eq(col).html();
                $(this).html(gettemplate);
                if(col==13){
                    $(this).find('select').val(atr);
                }
                //$(this).find("select option[stt='"+atr+"']").prop('selected', true);
                datacurrent = atr;
                value_first_html_add = content_html;
            }else{
                datacurrent = $(this).text();
                $(this).html('<input type="text" id="onchange_add" value="'+datacurrent+'">');
                $( "#onchange_add" ).focus();
            }
            value_first_add=datacurrent;
            is_onchange_add=true;
        }));

        var is_onchange_edit =false;
        $('body ').on('dblclick', '#category_name table[data-example="example_more_edit"]  tbody tr td', (function (e) {
            e.preventDefault();
            if ($(this).html() == '<input type="checkbox">') {
                return;
            }
            var col = $(this).parent().children().index($(this));
            var gettemplate =$('#category_name table[data-example="example_more_edit"] tfoot tr th').eq(col);
            if($(gettemplate).attr('att-edit') == 'false'){
                return
            }
            if(is_onchange_edit==true){
                return;
            }
            var content_html = $(this).html();
            var atr = $(content_html).attr('att-value');
            var datacurrent;
            //alert(atr)
            if(atr !=undefined){
                //var col = $(this).parent().children().index($(this));
                var gettemplate =$('#category_name table[data-example="example_more_edit"] tfoot tr th').eq(col).html();
                $(this).html(gettemplate);
                if(col==13){
                    $(this).find('select').val(atr);
                }
                //$(this).find("select option[stt='"+atr+"']").prop('selected', true);
                datacurrent = atr;
                value_first_html_edit = content_html;
            }else{
                datacurrent = $(this).text();
                $(this).html('<input type="text" id="onchange_edit" value="'+datacurrent+'">');
                $( "#onchange_edit" ).focus();
            }
            value_first_edit=datacurrent;
            is_onchange_edit=true;
        }));

        //need colum
        function save_new_data(data,callback){
            $.ajax({
                // url: 'list_api/add',
                url: $('#category_name').attr('data-cat') + '/add',
                type: 'POST',
                dataType: "json",
                data: {
                    //category_name: $('#category_name').attr('data-cat'),
                    'input': data,
                    'type':'need_column'
                },
                beforeSend: function () {
                    show_hide_datatable_loading(1)
                },
                complete: function () {
                    callback('test')
                    //$('#category_name [data-example="example_more"]').DataTable().ajax.reload(function(json){after_reload_ajax_datatable(json)},false);
                    reload_main_table();
                    show_hide_datatable_loading(0)
                    //table_category_all.page( 'last' ).draw( 'page' );
                    //                    table_category_all.ajax.reload(null, false);
                    //$('#category_name [data-example="example_more"]').DataTable(option_config_table);
                    
                    //$('#myAdd').modal('toggle');
                },
                success: function (data) {
                    console.log(data);
                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            })
        }

        function show_hide_button() {
            var c_current = 0;
            var c_current_leng = 0;
            $('#category_name .table-responsive [data-example="example_more"] tbody input[type="checkbox"]').each(function () {
                c_current_leng++;
                // console.log(this)
                // console.log($(this))
                // console.log($(this).prop('checked'))
                var sThisVal = $(this).prop('checked');
                // var sThisVal = (this.checked ? $(this).val() : "");
                // console.log(sThisVal)
                if (sThisVal) {
                    c_current++;
                }
            });
            var s_tt=false;
            if(c_current_leng==c_current){
                //auto check thread
                s_tt=true;
            }else{
                //off for it
                s_tt=false;
            }
            $('#category_name .table-responsive [data-example="example_more"] thead input[type="checkbox"]').prop('checked',s_tt);
            // alert(c_current)
            if (c_current == 0) {
                //Do stuff
                //alert('hello')
                $('#btn_edit').addClass('disabled');
                $('#btn_publish').addClass('disabled');
                $('#btn_unpublish').addClass('disabled');
                $('#btn_delete').addClass('disabled');
            }
            if (c_current == 1) {
                $('#btn_edit').removeClass('disabled');
                $('#btn_publish').removeClass('disabled');
                $('#btn_unpublish').removeClass('disabled');
                $('#btn_delete').removeClass('disabled');
            }
            if (c_current > 1) {
                $('#btn_edit').addClass('disabled');
                $('#btn_publish').removeClass('disabled');
                $('#btn_unpublish').removeClass('disabled');
                $('#btn_delete').removeClass('disabled');
            }
        }
        //use for table
        //input need [data-example="example_more_add"]/[data-example="example_more_edit"]
        function show_hide_button_healthcare(type) {
            var at_id ='[data-example="example_more_add"]'; //default add
            if(type !=undefined){
                at_id = type;
            }
            //alert(at_id)
            var c_current = 0;
            //var c_current_leng = 0;
            //$('#category_name .table-responsive [data-example="example_more_add"] tbody input[type="checkbox"]').each(function () {
            $('#category_name .table-responsive '+at_id+' tbody input[type="checkbox"]').each(function () {
                var sThisVal = $(this).prop('checked');
                if (sThisVal) {
                    c_current++;
                }
            });
            if (c_current == 0) {
                $('.table-responsive '+at_id).closest('.table-responsive').find('.xoa_select').addClass('disabled');
            }
            if (c_current >= 1) {
                $('.table-responsive '+at_id).closest('.table-responsive').find('.xoa_select').removeClass('disabled');
            }
        }
        //add event input checkbox
        $('body ').on('click', '#category_name .table-responsive [data-example="example_more_add"] thead input[type="checkbox"]', (function () {
            var sThisVal = $(this).prop('checked');
            $('#category_name .table-responsive [data-example="example_more_add"] tbody input[type="checkbox"]').each(function () {
                 $(this).prop('checked',sThisVal);
            });
            show_hide_button_healthcare('[data-example="example_more_add"]');
        }));
        
        $('body ').on('change', '#category_name .table-responsive [data-example="example_more_add"] tbody input[type="checkbox"]', (function () {
            show_hide_button_healthcare('[data-example="example_more_add"]');
        }));
        //edit event input checkbox
        $('body ').on('click', '#category_name .table-responsive [data-example="example_more_edit"] thead input[type="checkbox"]', (function () {
            var sThisVal = $(this).prop('checked');
            $('#category_name .table-responsive [data-example="example_more_edit"] tbody input[type="checkbox"]').each(function () {
                 $(this).prop('checked',sThisVal);
            });
            show_hide_button_healthcare('[data-example="example_more_edit"]');
        }));
        
        $('body ').on('change', '#category_name .table-responsive [data-example="example_more_edit"] tbody input[type="checkbox"]', (function () {
            show_hide_button_healthcare('[data-example="example_more_edit"]');
        }));
        //button xoa_select event
        $('body ').on('click', '.xoa_select', (function (e) {
            e.preventDefault();
            e.stopPropagation();

            var list_id = [];
            var list_all = [];
            var list_name = [];
            var mainid =$(this).closest('div').find('table td input'); //checkbox
            var name_table =$(this).closest('div').find('table').attr('data-example')
            var type_event_submit = 'addtable';
            mainid.each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal =='on') {
                    var list_id = $(this).closest('tr').attr('data-newadd');
                    var list_id_edit = $(this).closest('tr').attr('data-newedit');
                    var temp = '';
                    if(list_id !=undefined){
                        temp = list_id
                    }else{
                        temp = list_id_edit
                        type_event_submit = 'edittable'
                    }
                    list_all.push(temp);
                    var showat=2;
                    if($(this).closest('table').find('thead tr').attr('show-position')){
                        showat =$(this).closest('table').find('thead tr').attr('show-position');
                    }
                    list_name.push($(this).closest('tr').find('td').eq(showat).text());
                }
            });
            
            if (list_all[0]) {
                var more_one= '.one_show';
                if(list_all.length>1){
                    more_one='.more_show';
                }
                
                get_allinptu_delete_healthcare_add_edit(type_event_submit,name_table,list_all.join(),more_one,list_name[0]);
            }

        }));
        



        /**
         * When Click Edit in gui just get list data from current row to show.
         */
        //$('data-target="#myEdit"').on('click', '.sorting_1' , (function(){
        var glb_edit_id_click=''; 
        $('body ').on('click', '#category_name [data-target="#myEdit"]', (function () {
            //alert('Edit event');
            $('#category_name [data-example="example_more"] td input').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal) {
                    var findlisttd = $(this).closest('tr').find('td');
                    var leng = $(findlisttd).length;
                    var list = [];
                    var at = $(this).closest('tr').attr('id_colum');
                    glb_edit_id_click =at;
                    list.push(at);//push id first
                    $(findlisttd).each(function (index) {
                        //if (index != (leng - 1)) {
                        //console.log(index,leng)
                        //console.log($(this).html()); //value of td in row different column edit/delete.
                        list.push($(this).html());
                        //}
                    });
                    //console.log(list);
                    reload_edit_table();
                    get_allinptu_edit(list);

                    return;
                }
                //console.log(sThisVal)
            });
            //load data edit_table
            //reload_edit_table();

        }));
        $('body ').on('click', '#category_name [data-target="#myDelete"]', (function () {
            //alert('Edit event');
            var list_id = [];
            var list_all = [];
            var list_name = [];
            $('#category_name [data-example="example_more"] td input').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal) {
                    //var list_id = $(this).closest('tr').find('td').first().html();
                    var list_id = $(this).closest('tr').attr('id_colum');
                    list_all.push(list_id);
                    var showat=2;
                    if($(this).closest('table').find('thead tr').attr('show-position')){
                        showat =$(this).closest('table').find('thead tr').attr('show-position');
                    }
                    // list_name.push($(this).closest('tr').find('td').eq(2).text());
                    list_name.push($(this).closest('tr').find('td').eq(showat).text());
                }
            });

            if (list_all[0]) {
                var more_one= '.one_show';
                if(list_all.length>1){
                    more_one='.more_show';
                }
                get_allinptu_delete(list_all.join(),more_one,list_name[0]);
            }

        }));
        $('body ').on('click', '#category_name [data-target="#myPush"]', (function () {
            //alert('Edit event');
            var list_id = [];
            var list_all = [];
            var list_name = [];
            $('#category_name [data-example="example_more"] td input').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal) {
                    //var list_id = $(this).closest('tr').find('td').first().html();
                    var list_id = $(this).closest('tr').attr('id_colum');
                    list_all.push(list_id);
                    var showat=2;
                    if($(this).closest('table').find('thead tr').attr('show-position')){
                        showat =$(this).closest('table').find('thead tr').attr('show-position');
                    }
                    // list_name.push($(this).closest('tr').find('td').eq(2).text());
                    list_name.push($(this).closest('tr').find('td').eq(showat).text());
                }
            });

            if (list_all[0]) {
                var more_one= '.one_show';
                if(list_all.length>1){
                    more_one='.more_show';
                }
                get_allinptu_push(list_all.join(),more_one,list_name[0]);
            }

        }));
        $('body ').on('click', '#category_name [data-target="#myUnpush"]', (function () {
            //alert('Edit event');
            var list_id = [];
            var list_all = [];
            var list_name = [];
            $('#category_name [data-example="example_more"] td input').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal) {
                    //var list_id = $(this).closest('tr').find('td').first().html();
                    var list_id = $(this).closest('tr').attr('id_colum');
                    list_all.push(list_id);
                    // list_name.push($(this).closest('tr').find('td').eq(2).text());
                    var showat=2;
                    if($(this).closest('table').find('thead tr').attr('show-position')){
                        showat =$(this).closest('table').find('thead tr').attr('show-position');
                    }
                    // list_name.push($(this).closest('tr').find('td').eq(2).text());
                    list_name.push($(this).closest('tr').find('td').eq(showat).text());
                }
            });

            if (list_all[0]) {
                var more_one= '.one_show';
                if(list_all.length>1){
                    more_one='.more_show';
                }
                get_allinptu_unpush(list_all.join(),more_one,list_name[0]);
            }

        }));



        function get_allinptu_edit(list) {
            $('#edit form input').each(function (index) {

                if ($(this).attr('type') == 'checkbox') {
                    if (list[index] == 'Bật' || list[index] == 1 || list[index] == 'O'  || list[index] == 'o' || list[index] == true) {
                        $(this)[0].checked = true;
                    } else {
                        $(this).attr('checked', false);
                    }
                } else {
                    if($(list[index]).attr('att-value')!=undefined){
                        $(this).closest('div').find('select').val($(list[index]).attr('att-value'));
                        $(this).val($(list[index]).attr('att-value'));
                    }else{
                        $(this).val(list[index]);                        
                    }
                }
            })
        }
        function get_allinptu_delete(list,more_one,list_name) {
            $('#delete form input').val(list); //get id
            $('#delete form .at_change').text(list_name); //show text when change event

            $('#delete form .form-group .control-label ').hide();
            $('#delete form .form-group '+more_one).show();

        }
        function get_allinptu_push(list,more_one,list_name) {
            $('#push form input').val(list);
            $('#push form .at_change').text(list_name);

            $('#push form .form-group .control-label ').hide();
            $('#push form .form-group '+more_one).show();

        }
        function get_allinptu_unpush(list,more_one,list_name) {
            $('#unpush form input').val(list);
            $('#unpush form .at_change').text(list_name);

            $('#unpush form .form-group .control-label ').hide();
            $('#unpush form .form-group '+more_one).show();

        }
        function get_allinptu_delete_healthcare_add_edit(type_event_submit,name_table,list,more_one,list_name) {
            $('#myDeletehealthcare').addClass('in');
            $('#myDeletehealthcare').show();

            $('#delete_healthcare form input').val(list); //get id
            $('#delete_healthcare #name_table').attr('name-table',name_table); //get name table
            $('#delete_healthcare #name_table').attr('type_event_submit',type_event_submit); //get name table
            $('#delete_healthcare form .at_change').text(list_name); //show text when change event

            $('#delete_healthcare form .form-group .control-label ').hide();
            $('#delete_healthcare form .form-group '+more_one).show();
        }
        $('#no-headthcare-click').click(function(){
            event_no_healthcare();
        })
        $('#yes-headthcare-click').click(function(){
            event_yes_healthcare();
        })
        function event_no_healthcare(){
            $('#myDeletehealthcare').removeClass('in');
            $('#myDeletehealthcare').hide();
        }
        function event_yes_healthcare(){
            $('#myDeletehealthcare').removeClass('in');
            $('#myDeletehealthcare').hide();

            var name_table = $('#delete_healthcare #name_table').attr('name-table');
            var type_event_submit = $('#delete_healthcare #name_table').attr('type_event_submit');
            //var mainid =$(this).closest('.dataTables_wrapper').find('table td input')
            //var mainid =$('category_name [data-example="example_more_edit"]').closest('.dataTables_wrapper').find('table td input')
            
            if(type_event_submit =='addtable'){
                var mainid =$('#category_name [data-example="'+name_table+'"]').closest('.dataTables_wrapper').find('table td input')
                mainid.each(function () {
                    var sThisVal = (this.checked ? $(this).val() : "");
                    if (sThisVal =='on') {
                        //var list_id = $(this).closest('tr').attr('data-newadd');
                        if(name_table=='example_more_add'){
                            table_category_all_add.row( $(this).closest('tr') ).remove().draw( false );
                            //table_category_all_add.row('.row-select').remove().draw( false );
                            //if($(this).closest('tr').hasClass('.row-select')){
                            
                        }else if(name_table=='example_more_edit'){
                            //var row = table_category_all_edit.row( $(this).closest('tr') );
                            //table_category_all_edit.row('.row-select').remove().draw( false );
                            table_category_all_edit.row( $(this).closest('tr') ).remove().draw( false );
                        }
                        // var rowNode = row.node();
                        // rowNode.remove();
                        // rowNode.draw();;
                    }
                });
            }else{
                //edittable
                //ajax delete
                var list_need_delete = $('#delete_healthcare form input').val();
                
                $.ajax({
                    url: $('#category_name').attr('data-cat') + '/edit',
                    type: 'DELETE',
                    dataType: "json",
                    data: {
                        //category_name: $('#category_name').attr('data-cat'),
                        'list': list_need_delete,
                        'type':'delete_in_tb',
                        'type_table':'list_service_in_package_healthcare',
                    },
                    beforeSend: function () {
    
                    },
                    complete: function () {
                        reload_edit_table();
                    },
                    success: function (data) {
                        console.log(data);
    
                    }, error: function (data) {
                        console.log(data);
                        glb_show_error_ajax(data)
                    }
                })
            }
            
            
        }


        var get_url = $(this).attr('href');
        option_config_table = {
            //             paging: false,
            //            searching: false,
            "processing": true,
            //"scrollX": true,
            "serverSide": true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },

            "ajax": {
                "url": $('body #category_name').attr('data-cat') + '/get',
                "dataSrc": function ( json ) {
                    after_reload_ajax_datatable(json.selectTem);
                    return json.data
                },
                "type": "POST",
                async: true, 
                cache: false,
                //                destroy: true,
                //                saveState: false,
                "data": function (d) {
                    //csrfName=csrfHash,
                    //d[csrfName]=csrfHash,
                    //d.push({csrfName:csrfHash}),
                    //d.category_name = $('#category_name').attr('data-cat'); //set at leftbar.php
                    if(is_onchange==true){
                        is_onchange=false;
                    }
                }
                , dataType: "json"
                , "error": function (data) {
                    table_category_all.processing(false);
                    // console.log(data);
                    // console.log(data.statusText);
                    // if (data.statusText == 'error') {
                    //     //no internet
                    //     glb_show_message('No internet');
                    // } else {
                    //     glb_show_message(data.responseJSON.message); //in main.js
                    // }
                    glb_show_error_ajax(data)
                },
            },
            
            "sDom": "<'row'<'col-sm-6'f><'col-sm-6'l>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",
            /*	
             columns: [
             { data: "0" },
             {
             "class":          "details-control",
             "orderable":      false,
             "data":           null,
             "defaultContent": "<input type='checkbox'>"
             },
             { data: "2" },
             { data: "3" },
             { data: "4" },
             { data: "5" },
             { data: "6" }
             ],
             */


            //columns: get_list_colum_render(),
            'createdRow': function( row, data, dataIndex ) {
                console.log(data)
                $(row).attr('id_colum', data[0]);
            },
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ 1 ],
                    "visible": true,
                    "searchable": false,
                    "orderable": false,
                },
                {
                "targets": 1,
                "data": null,
                "defaultContent": "<input type='checkbox'>"
            }
            ],
            "initComplete": function (settings, json) {
                //after_reload_ajax_datatable(json)
                
            }
        };
        function after_reload_ajax_datatable(data_select){
            $.each(data_select, function(index, item) {
                console.log(index, item);//return;
                var str_temat='';
                /*
                if(index=='select-status'){
                    var arr = [];
                    for (var key in item) {
                        if (item.hasOwnProperty(key)) {
                            arr.push(item[key]);
                        }
                    }
                    item = (arr.sort());
                }
                */
               console.log(item);
               console.log('item111');
                    $.each(item, function(index_sub, item_sub) {
                        //onsole.log('item_sub')
                        var type = typeof item_sub;
                        if(type =='string'){
                            str_temat +='<option value="'+index_sub+'">'+item_sub+'</option>';
                        }
                        if(type =='object'){
                            str_temat +='<option value="'+index_sub+'" att-forent="'+item_sub[1]+'">'+item_sub[0]+'</option>';
                        }
                        // console.log(type)
                        // console.log(item_sub.isArray)
                        
                    })
                
               
                
                template_status_vi[index]=str_temat;
            });
            call_load_tfooter();
            get_select_kcb();
        }

        table_category_all = $('#category_name [data-example="example_more"]').DataTable(option_config_table);
        

        //data-dismiss="modal"
        $('body ').on('click', '#category_name #add .modal-footer button[data-dismiss!="modal"]', (function () {
            var list_in = {};
            var list_temp={};
            $('#add .modal-content form input').each(function (index) {
                var at = $(this).val();
                var tb =$(this).attr('sh-tb');
                var cl =$(this).attr('sh-cl');
                if(tb!=undefined){
                    if ($(this).attr('type') == 'checkbox') {
                        at = $(this).eq(0).attr("checked") ? 1 : 0;
                    }
                    if(list_in[tb] == undefined){
                        list_in[tb]={}
                    }
                    list_in[tb][cl]=at;
                }
            });

            $.ajax({
                url: $('#category_name').attr('data-cat') + '/add',
                type: 'POST',
                dataType: "json",
                data: {
                    'input': list_in,
                    'type':'add_save_by_tb_col'
                },
                beforeSend: function () {

                },
                complete: function (data) {
                    //console.log(data)
                    //console.log(data.responseJSON.status)
                    //console.log(data.responseJSON.msg[0])
                    
                    if(data.responseJSON.status =='ok'){
                        var new_id_packethead = data.responseJSON.msg[0];
                        save_add_example_more(new_id_packethead);
                    }
                    
                },
                success: function (data) {
                }, error: function (data) {
                    glb_show_error_ajax(data)
                }
            }
            )
        }));

        //reload ajax table main after add/edit/delete/push/unpush
        function reload_main_table(){
            $('#category_name [data-example="example_more"]').DataTable().ajax.reload(function(json){after_reload_ajax_datatable(json)},false);
        }
        function reload_edit_table(){
            $('#category_name [data-example="example_more_edit"]').DataTable().ajax.reload(function(json){},false);
        }



        $('body ').on('click', '#category_name #edit .modal-footer button[data-dismiss!="modal"]', (function () {
            // var list_in = [];
            var list_in={};
            var id_edit_package_healthcare=''; //variable id
            var tb;
            $('#edit .modal-content form input').each(function (index) {
                /*
                if (index == 0){
                    var at = $(this).val();
                    var at = $(this).closest('tr').attr('id_colum');
                }

                if ($(this).attr('type') == 'checkbox') {

                    var at = $(this).is(":checked") ?'1':'0'
                }else{
                    var at = $(this).val();
                }
                list_in.push(at);
                */
               var at = $(this).val();
                tb =$(this).attr('sh-tb');
                var cl =$(this).attr('sh-cl');
                if(tb!=undefined){
                    if ($(this).attr('type') == 'checkbox') {
                        at = $(this).eq(0).attr("checked") ? 1 : 0;
                    }
                    if(list_in[tb] == undefined){
                        list_in[tb]={}
                    }
                    list_in[tb][cl]=at;
                }
            });

            id_edit_package_healthcare=list_in[tb]['id'];

            $.ajax({
                url: $('#category_name').attr('data-cat') + '/edit',
                type: 'POST',
                dataType: "json",
                data: {
                    //category_name: $('#category_name').attr('data-cat'),
                    'input': list_in,
                    'type':'edit_save_by_tb_col',
                },
                beforeSend: function () {

                },
                complete: function () {
                    //$('#category_name [data-example="example_more"]').DataTable().ajax.reload(function(json){after_reload_ajax_datatable(json)},false);
                    $('#myEdit').modal('toggle');
                    show_hide_button(0);
                    reload_main_table();
                    
                    //save_edit_example_more_edit(id_edit_package_healthcare)
                },
                success: function (data) {
                    console.log(data);

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            })
        }));
        var is_loading_save=false;
        function save_edit_list(data_save,data_first,callback){
            if(is_loading_save){
                return;                
            }
            is_loading_save=true;
            $.ajax({
                url: $('#category_name').attr('data-cat') + '/edit',
                type: 'POST',
                dataType: "json",
                data: {
                    category_name: $('#category_name').attr('data-cat'),
                    'input': data_save,
                    'input_first': data_first,
                    'type_submit':'edit_just_colum'
                },
                beforeSend: function () {
                    show_hide_datatable_loading(1);
                    // console.log('beforeSend');
                },
                complete: function () {
                    show_hide_datatable_loading(0);
                    console.log('complete');
                    is_loading_save=false;
                    callback('complete')
                },
                success: function (data) {
                    console.log('success');
                    // console.log(data);
                    callback('success')

                }, error: function (data) {
                    // console.log(data);
                    console.log('error');
                    callback('error')
                    
                    glb_show_error_ajax(data)
                }
            })
        }
        // var is_ajax_save_edit=false;
        // function save_edit_tb_list(data_save,data_first,callback){
        //     if(is_ajax_save_edit){
        //         return;                
        //     }
        //     is_ajax_save_edit=true;
        //     $.ajax({
        //         url: $('#category_name').attr('data-cat') + '/edit',
        //         type: 'POST',
        //         dataType: "json",
        //         data: {
        //             category_name: $('#category_name').attr('data-cat'),
        //             'input': data_save,
        //             'input_first': data_first,
        //             'type_submit':'edit_just_colum'
        //         },
        //         beforeSend: function () {
        //             show_hide_datatable_loading(1);
        //         },
        //         complete: function () {
        //             show_hide_datatable_loading(0);
        //             is_ajax_save_edit=false;
        //             callback('complete')
        //         },
        //         success: function (data) {
        //             callback('success')

        //         }, error: function (data) {
        //             callback('error')
        //             glb_show_error_ajax(data)
        //         }
        //     })
        // }
        function save_add_example_more(id_add_package_healthcare){
            var list_in=[];
            var list_temp={};
            var list_temp_f={};
            var inttang=0;
            $('#category_name table[data-example="example_more_add"] tbody tr').each(function (index_tr) {
                list_temp={};
                list_temp_f={};
                list_temp_f['list_service_in_package_healthcare']={};

                //list_temp['id'] = $(this).find('td').eq('0').html();
                list_temp['id'] = '';

                list_temp['package_healthcare_service_id'] = id_add_package_healthcare;
                list_temp['healthcare_service_id'] = $(this).find('td').eq('2').find('span').attr('attr-data');
                list_temp['number'] = $(this).find('td').eq('7').html();
                list_temp['stat'] = $(this).find('td').eq('13').find('span').attr('attr-data');
                list_temp['order'] = $(this).find('td').eq('14').html();
                list_temp_f['list_service_in_package_healthcare'] = list_temp;

                list_in[inttang] = (list_temp_f);
                inttang++;
            });

            $.ajax({
                url: $('#category_name').attr('data-cat') + '/add',
                type: 'POST',
                dataType: "json",
                data: {
                    //category_name: $('#category_name').attr('data-cat'),
                    'input': list_in,
                    'type':'add_save_by_tb_col_more',
                },
                beforeSend: function () {

                },
                complete: function () {
                    reload_main_table();
                    $('#myAdd').modal('toggle');
                    $('#add .modal-content form input').each(function (index) {
                        $(this).val('');
                    })
                    table_category_all_add.clear().draw();
                },
                success: function (data) {
                    console.log(data);

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            });
        }
        //off not use
        function save_edit_example_more_edit(id_edit_package_healthcare){
            var list_in=[];
            var list_temp={};
            var list_temp_f={};
            var inttang=0;
            $('#category_name table[data-example="example_more_edit"] tbody tr').each(function (index_tr) {
                list_temp={};
                list_temp_f={};
                list_temp_f['list_service_in_package_healthcare']={};

                list_temp['id'] = $(this).find('td').eq('0').html();
                // list_temp['id'] = '';

                list_temp['package_healthcare_service_id'] = id_edit_package_healthcare;
                list_temp['healthcare_service_id'] = $(this).find('td').eq('2').find('span').attr('attr-data');
                list_temp['number'] = $(this).find('td').eq('7').html();
                list_temp['stat'] = $(this).find('td').eq('13').find('span').attr('attr-data');
                list_temp['order'] = $(this).find('td').eq('14').html();
                list_temp_f['list_service_in_package_healthcare'] = list_temp;

                list_in[inttang] = (list_temp_f);
                inttang++;
            });

            $.ajax({
                url: $('#category_name').attr('data-cat') + '/edit',
                type: 'POST',
                dataType: "json",
                data: {
                    //category_name: $('#category_name').attr('data-cat'),
                    'input': list_in,
                    'type':'edit_save_by_tb_col_more',
                },
                beforeSend: function () {

                },
                complete: function () {
                    //$('#category_name [data-example="example_more"]').DataTable().ajax.reload(function(json){after_reload_ajax_datatable(json)},false);

                    // $('#myEdit').modal('toggle');
                    // show_hide_button(0);
                },
                success: function (data) {
                    console.log(data);

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            });
        }

        $('body ').on('click', '#category_name #delete .modal-footer button[data-dismiss!="modal"]', (function () {
            var list_in = $('#delete .modal-content input').val();

            $.ajax({
                url: $('#category_name').attr('data-cat') + '/delete',
                type: 'POST',
                dataType: "json",
                data: {
                    category_name: $('#category_name').attr('data-cat'),
                    'input': list_in,
                },
                beforeSend: function () {

                },
                complete: function () {
                    //$('#category_name [data-example="example_more"]').DataTable().ajax.reload(function(json){after_reload_ajax_datatable(json)},false);
                    reload_main_table();
                    $('#myDelete').modal('toggle');
                    show_hide_button(0);//set all off button
                },
                success: function (data) {
                    console.log(data);

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            }
            )
        }));

        $('body ').on('click', '#category_name #push .modal-footer button[data-dismiss!="modal"]', (function () {
            var list_in = $('#push .modal-content input').val();

            $.ajax({
                url: $('#category_name').attr('data-cat') + '/push',
                type: 'POST',
                dataType: "json",
                data: {
                    category_name: $('#category_name').attr('data-cat'),
                    'input': list_in,
                },
                beforeSend: function () {

                },
                complete: function () {
                    //$('#category_name [data-example="example_more"]').DataTable().ajax.reload(function(json){after_reload_ajax_datatable(json)},false);
                    reload_main_table();
                    $('#myPush').modal('toggle');
                    show_hide_button(0);//set all off button
                },
                success: function (data) {
                    console.log(data);

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            }
            )
        }));
        $('body ').on('click', '#category_name #unpush .modal-footer button[data-dismiss!="modal"]', (function () {
            var list_in = $('#unpush .modal-content input').val();

            $.ajax({
                url: $('#category_name').attr('data-cat') + '/unpush',
                type: 'POST',
                dataType: "json",
                data: {
                    category_name: $('#category_name').attr('data-cat'),
                    'input': list_in,
                },
                beforeSend: function () {

                },
                complete: function () {
                    //$('#category_name [data-example="example_more"]').DataTable().ajax.reload(function(json){after_reload_ajax_datatable(json)},false);
                    reload_main_table();
                    $('#myUnpush').modal('toggle');
                    show_hide_button(0);//set all off button
                },
                success: function (data) {
                    console.log(data);

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            }
            )
        }));


        //        $('#add .switch input').on('switchChange.bootstrapSwitch', function(event, state) {
        //            alert('change')
        //            if (state)
        //            {
        //                // Checked
        //                alert('change t')
        //            }else
        //            {
        //                // Is not checked
        //                alert('change f')
        //            }
        //        });
        
        //nomal function
        
        function get_select_kcb(){
            $.ajax({
                url: $('#category_name').attr('data-cat') + '/add',
                type: 'GET',
                dataType: "json",
                data: {
                    category_name: $('#category_name').attr('data-cat'),
                    'action': 'get_list_healthcare',
                },
                beforeSend: function () {

                },
                complete: function (data) {
                    console.log(data)
                    //$('#category_name [data-example="example_more"]').DataTable().ajax.reload(function(json){after_reload_ajax_datatable(json)},false);
                    //$('#myUnpush').modal('toggle');
                    //show_hide_button(0);//set all off button                    
                    ////update for table edit, add and load resert
                    var list_for={};
                    list_for['name']='';
                    list_for['code']='';
                    
                    //var list_for['code']='';
                    glb_data_json_healthcare = data.responseJSON;
                    console.log('data_json');
                    console.log(glb_data_json_healthcare);
                    
                    for (var property1 in glb_data_json_healthcare) {
                        //string1 = string1 + data_json[property1];
                        list_for['name'] +='<option stt="'+property1+'" value="'+glb_data_json_healthcare[property1]['id']+'">'+glb_data_json_healthcare[property1]['name']+'</option>';
                        list_for['code'] +='<option stt="'+property1+'" value="'+glb_data_json_healthcare[property1]['id']+'">'+glb_data_json_healthcare[property1]['code']+'</option>';
                      }
                      //console.log(list_for);
                      list_for['name'] = '<select class="select-at-table">'+list_for['name']+'</select><input type="text" class="hide" />';
                      list_for['code'] = '<select class="select-at-table">'+list_for['code']+'</select><input type="text" class="hide" />';
                    
                    $('#category_name [data-example="example_more_add"] tfoot th').each(function (i) {
                        var get_tite = $(this).attr('data-attr');
                        var content_show = '';
                        if (get_tite != undefined) {
                            content_show = list_for[get_tite];
                            $(this).html(content_show);
                        }
                        var get_tite = $(this).attr('data-type-select');
                        if(get_tite != undefined){
                            var content_default = template_status_vi[get_tite];
                            content_show = '<select class="select-at-table">'+content_default+'</select>';
                            content_show +='<input type="text" class="hide" />';
                            $(this).html(content_show);
                        }
                    });
                    $('#category_name [data-example="example_more_edit"] tfoot th').each(function (i) {
                        var get_tite = $(this).attr('data-attr');
                        var content_show = '';
                        if (get_tite != undefined) {
                            content_show = list_for[get_tite];
                            $(this).html(content_show);
                        }
                        var get_tite = $(this).attr('data-type-select');
                        if(get_tite != undefined){
                            var content_default = template_status_vi[get_tite];
                            content_show = '<select class="select-at-table">'+content_default+'</select>';
                            content_show +='<input type="text" class="hide" />';
                            $(this).html(content_show);
                        }
                    });
                    
                    var tem0='select-package-healthcare-service-type';
                    var temp = template_status_vi[tem0];
                    $('.select-type-head').html(temp);
                    $('.select-type-head').trigger('change');

                },
                success: function (data) {
                    console.log(data);
                    
                    

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            });
        }

        $('body ').on('focusout change', '.select-type-head', (function (e) {
            //var col = $(this).closest('td').parent('tr').children('td').index($(this).closest('td'));
            var vl = $(this).val();
            // var datatrig = $(this).closest('div').attr('data-trigge');
            // //alert(datatrig)
            // if(datatrig =='change'){
            //     $(this).closest('tr').find('th[data-trigge] .select-at-table').val(vl);
            // }
            $(this).closest('div').find('input').val(vl);
        }))

        
        //table -> tbody 
        $('body ').on('change', '#category_name table[data-example="example_more_add"] tbody tr td .select-at-table', (function (e) {
            //var col = $(this).closest('td').parent('tr').children('td').index($(this).closest('td'));
            var vl = $(this).val();
            var datatrig = $(this).closest('td').attr('data-trigge');
            //alert(datatrig)
            if(datatrig =='change'){
                $(this).closest('tr').find('th[data-trigge] .select-at-table').val(vl);
            }
            $(this).closest('td').find('input').val(vl);
            //alert('123')
        }))
        $('body ').on('keyup focusout', '#category_name table[data-example="example_more_add"] tbody tr td select', (function (e) {
            e.preventDefault();
            e.stopPropagation();
            if(e.which == 13 || e.type=='focusout') {
                //alert('change')
                console.log("example_more_add")
                is_onchange_add=false;
                var positionat = $(this).parent().index();
                //alert(positionat)
                // $(this).closest('td').html(value_first_html_add);
                var vl_text = $(this).find('option:selected').text();
                var vl_vl = $(this).find('option:selected').val();
                var vl_attr_stt = $(this).find('option:selected').attr('stt');
                //var id_datacurrent = $(this).closest('tr').attr('id_colum');
                var new_vl='';
                if(positionat==13){
                    new_vl = '<span att-value="'+vl_vl+'">'+vl_text+'</span>';
                }else{
                    new_vl = '<span att-value="'+vl_attr_stt+'">'+vl_text+'</span>';
                }
                $(this).closest('td').html(new_vl);
                /*
                var datacurrent = $(this).val(); //value of text input /select/checkbook/datatime/date
                var datacurrent_text = $(this).find('option:selected').text();
                var id_datacurrent = $(this).closest('tr').attr('id_colum');
                var positionat = $(this).parent().index(); //get index curent in td
                var data_first={};
                var data_save ={};
                var data_send1={};
                var colum_id1 = $('#category_name .table-responsive table thead tr').find('th').eq(0).attr('att-name');//get attribute column name current edit
                var colum_id2 = $('#category_name .table-responsive table thead tr').find('th').eq(positionat).attr('att-name');//get attribute column name current edit
                if(!colum_id1){
                    console.error('Not set attibute for id save data');
                    colum_id1=0;
                }
                if(!colum_id2){
                    console.error('Not set attibute for id save data');
                    colum_id1=1;
                }
                data_send1[colum_id1]=id_datacurrent;
                data_send1[colum_id2]=datacurrent;
                data_save = data_send1;
        
                var data_first={};
                data_first[colum_id1]=id_datacurrent;
                data_first[colum_id2]=value_first;
                
                var at_positioninfo=this;
                if(value_first==datacurrent){
                    if(is_onchange_add==true){
                        $(this).closest('td').html(value_first_html);
                        is_onchange_add=false;
                    }
                    return;
                }
                */
                /*
                var result= save_edit_list(data_save,data_first,function(data){
                    console.log(data)
                    if(is_onchange_add==true){
                        var add_show ='<span att-value="'+datacurrent+'">'+datacurrent_text+'</span>';
                        $(at_positioninfo).closest('td').html(add_show);
                        is_onchange_add=false;
                    }
                });
                */
            }
            if (e.keyCode == 27) {
                $(this).closest('td').html(value_first_html_add);
                if(is_onchange_add==true){
                    is_onchange_add=false;
                }
            }
        }));
        $('body ').on('focusout change', '#category_name table[data-example="example_more_edit"] tbody tr td .select-at-table', (function (e) {
            //var col = $(this).closest('td').parent('tr').children('td').index($(this).closest('td'));
            var vl = $(this).val();
            var datatrig = $(this).closest('td').attr('data-trigge');
            
            if(datatrig =='change'){
                $(this).closest('tr').find('th[data-trigge] .select-at-table').val(vl);
            }
            $(this).closest('td').find('input').val(vl);
            //alert('123')
        }))
        // table >tfoot 
        $('body ').on('focusout change', '#category_name table[data-example="example_more_add"] tfoot th .select-at-table, #category_name table[data-example="example_more_edit"] tfoot th .select-at-table', (function (e) {
            //var col = $(this).closest('th').parent('tr').children('th').index($(this).closest('th'));
            var vl = $(this).val();
           // $(this).closest('tr').find('.select-at-table').val(vl);
            var datatrig = $(this).closest('th').attr('data-trigge');
            //alert(datatrig)
            if(datatrig =='change'){
                $(this).closest('tr').find('th[data-trigge] .select-at-table').val(vl);
            }
            $(this).closest('th').find('input').val(vl);
        }));
        var tang_add_tr=1;
        //add new row for table add
        $('body ').on('keyup', '#category_name table[data-example="example_more_add"] tfoot tr th', (function (e) {
            e.preventDefault();
            e.stopPropagation();
            //enter 
            if(e.which == 13) {
                var atselsect_stt = $(this).closest('tfoot').find('tr th .select-at-table option:selected').attr('stt');
                var list_value =[];
                var list_value_test ={};
                var select_show_vl={};
                var list_value_data =glb_data_json_healthcare[atselsect_stt]
                /*
                $(this).closest('tfoot').find('tr th').each(function (index, value) {
                    var vlth =$(this).html();
                    var attr = get_attribute_name_column_add(index);
                    console.log(attr)
                    if(vlth==''){
                        
                        var tem =glb_data_json_healthcare[atselsect_stt][attr];
                        if(tem ==undefined){tem='';}
                        list_value.push(tem);
                        list_value_test[attr]=tem;
                    }else{
                        
                        if($(this).find('select').length){
                            var str_select;
                            //select_show_vl[index] = $(this).find('select option:selected').val();
                            if(index==2 || index==3){
                                select_show_vl[index] = $(this).find('select option:selected').attr('stt');
                                str_select ='<span att-value="'+$(this).find('select option:selected').attr('stt')+'">'+$(this).find('select option:selected').text()+'</span>';
                            }else{
                                select_show_vl[index] = $(this).find('select option:selected').val();
                                str_select ='<span att-value="'+$(this).find('select option:selected').val()+'">'+$(this).find('select option:selected').text()+'</span>';
                            }
                            list_value.push(str_select);
                            list_value_test[attr]=str_select;
                        }else{
                            if($(this).find('input')){
                                list_value.push($(this).find('input').val());
                                list_value_test[attr]=$(this).find('input').val();
                                select_show_vl[index] = $(this).find('input').val();
                            }
                        }
                    }
                });
                */
                

                var tg=0;
                console.log(list_value_test)
                console.log(list_value)
                var at=$('#category_name table[data-example="example_more_add"] tfoot tr').find('th').eq(12);
                var str_select ='<span att-value="'+at.find('select option:selected').val()+'">'+at.find('select option:selected').text()+'</span>';
                var at1=$('#category_name table[data-example="example_more_add"] tfoot tr').find('th').eq(6).find('input').val();
                var at2=$('#category_name table[data-example="example_more_add"] tfoot tr').find('th').eq(13).find('input').val();
                var newrow = table_category_all_add.row.add( [
                    '',
                    '<input type="checkbox">',
                    '<span attr-data="'+list_value_data.id+'">'+list_value_data.code+'</a>',
                    list_value_data.name,
                    list_value_data.type,
                    list_value_data.group,
                    list_value_data.unit,
                    at1,
                    list_value_data.price_in_hour,
                    list_value_data.price_overtime,
                    list_value_data.price_holiday,
                    list_value_data.price_foreigner,
                    list_value_data.note,
                    
                    str_select,
                    at2,

                ] ).draw( false ).node();
                /*
                var newrow = table_category_all_add.row.add( [
                    list_value[tg],
                    '<input type="checkbox">',
                    '<span attr-data="'+select_show_vl[tg+1]+'">'+list_value[tg+1]+'</a>',
                    list_value[tg+2],
                    list_value[tg+3],
                    list_value[tg+4],
                    list_value[tg+5],
                    list_value[tg+6],
                    list_value[tg+7],
                    list_value[tg+8],
                    list_value[tg+9],
                    list_value[tg+10],
                    list_value[tg+11],
                    list_value[tg+12],
                    // '<span attr-data="'+select_show_vl[13]+'">'+list_value[13]+'</a>',
                    list_value[tg+13]

                ] ).draw( false ).node();
                */
                //<span att-value="0">Ẩn</span>
                $(newrow).attr('data-newadd',tang_add_tr);
                //$(newrow).addClass('row-select');
                tang_add_tr++;
            }
            if(e.which == 27) {

            }
        }));
        var tang_edit_tr=1;
        $('body ').on('keyup', '#category_name table[data-example="example_more_edit"] tfoot tr th', (function (e) {
            e.preventDefault();
            e.stopPropagation();
            //enter 
            if(e.which == 13) {
                    var atselsect_stt = $(this).closest('tfoot').find('tr th .select-at-table option:selected').attr('stt');
                    var list_value =[];
                    var select_show_vl={};
                    $(this).closest('tfoot').find('tr th').each(function (index, value) {
                        var vlth =$(this).html();
                        if(vlth==''){
                            var attr = get_attribute_name_column_add(index);
                            var tem =glb_data_json_healthcare[atselsect_stt][attr];
                            if(tem ==undefined){tem='';}
                            list_value.push(tem);
                        }else{
                            if($(this).find('select').length){
                                //list_value.push($(this).find('select option:selected').text());
                                var str_select ='<span att-value="'+$(this).find('select option:selected').attr('stt')+'">'+$(this).find('select option:selected').text()+'</span>';
                                list_value.push(str_select);
                                //select_show_vl = $(this).find('select option:selected').val();
                                if(index==2){
                                    //select_show_vl[index] = $(this).find('select option:selected').attr('stt');
                                    select_show_vl[index] = $(this).find('select option:selected').val();
                                }else{
                                    select_show_vl[index] = $(this).find('select option:selected').val();
                                }
                                
                            }else{
                                if($(this).find('input')){
                                    list_value.push($(this).find('input').val());
                                    select_show_vl[index] = $(this).find('input').val();
                                }
                            }
                        }
                    });
                    /*
                    //alert('123456789')
                    console.log(list_value);
                    //add new
                    var newrow = table_category_all_edit.row.add( [
                        list_value[1],
                        '<input type="checkbox">',
                        '<span attr-data="'+select_show_vl[2]+'">'+list_value[2]+'</a>',
                        list_value[3],
                        list_value[4],
                        list_value[5],
                        list_value[6],
                        list_value[7],
                        list_value[8],
                        list_value[9],
                        list_value[10],
                        list_value[11],
                        list_value[12],
                        // list_value[13],
                        '<span attr-data="'+select_show_vl[13]+'">'+list_value[13]+'</a>',
                        list_value[14]

                    ] ).draw( false ).node();
                    $(newrow).attr('data-newadd',tang_edit_tr);
                    //$(newrow).addClass('row-select');
                    tang_edit_tr++;
                    */


                    //save new sub head
                    var list_in = {};
                    var list_temp={};
                    var tb ='list_service_in_package_healthcare';
                    //list_in[tb]['']=select_show_vl[2];
                    list_in[tb] ={};
                    list_in[tb]['number']=select_show_vl[7];
                    list_in[tb]['package_healthcare_service_id']=glb_edit_id_click;
                    list_in[tb]['healthcare_service_id']=select_show_vl[2];
                    list_in[tb]['stat']=select_show_vl[13];
                    list_in[tb]['order']=select_show_vl[14];
                    console.log(select_show_vl)

                    $.ajax({
                        url: $('#category_name').attr('data-cat') + '/add',
                        type: 'POST',
                        dataType: "json",
                        data: {
                            'input': list_in,
                            'type':'add_save_by_tb_col'
                        },
                        beforeSend: function () {

                        },
                        complete: function (data) {
                            //console.log(data)
                            //console.log(data.responseJSON.status)
                            //console.log(data.responseJSON.msg[0])
                            
                            if(data.responseJSON.status =='ok'){
                                reload_edit_table();
                                // alert('save ok')
                                // reload_edit_table();
                                //$('#myEdit').modal('toggle');
                                //var new_id_packethead = data.responseJSON.msg[0];
                                //save_add_example_more(new_id_packethead);
                            }
                            
                        },
                        success: function (data) {
                        }, error: function (data) {
                            glb_show_error_ajax(data)
                        }
                    }
                    )
            }
            if(e.which == 27) {

            }
        }));

        //------ start function for add here
        var option_config_table_add ={
            //             paging: false,
            //            searching: false,
            "processing": true,
            //"scrollX": true,
            "serverSide": false,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            /*
            "ajax": {
                "url": $('body #category_name').attr('data-cat') + '/get',
                "dataSrc": function ( json ) {
                    after_reload_ajax_datatable(json.selectTem);
                    return json.data
                },
                "type": "POST",
                async: true, 
                cache: false,
                //                destroy: true,
                //                saveState: false,
                "data": function (d) {
                   
                    if(is_onchange==true){
                        is_onchange=false;
                    }
                }
                , dataType: "json"
                , "error": function (data) {
                    table_category_all.processing(false);
                    
                    glb_show_error_ajax(data)
                },
            },
            */
            
            "sDom": "<'row'<'col-sm-6'f><'col-sm-6'l>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",
            /*	
             columns: [
             { data: "0" },
             {
             "class":          "details-control",
             "orderable":      false,
             "data":           null,
             "defaultContent": "<input type='checkbox'>"
             },
             { data: "2" },
             { data: "3" },
             { data: "4" },
             { data: "5" },
             { data: "6" }
             ],
             */
        
        
            //columns: get_list_colum_render(),
            'createdRow': function( row, data, dataIndex ) {
                //$(row).attr('id_colum', data[0]);
            },
            "columnDefs": [
                // {
                //     "targets": [ 0 ],
                //     "visible": false,
                //     "searchable": false
                // },
                // {
                //     "targets": [ 1 ],
                //     "visible": true,
                //     "searchable": false,
                //     "orderable": false,
                // },
                // {
                // "targets": 1,
                // "data": null,
                // "defaultContent": "<input type='checkbox'>"
                // }
            ],
            "initComplete": function (settings, json) {
                //after_reload_ajax_datatable(json)
            }
        };
        var table_category_all_add = $('#category_name [data-example="example_more_add"]').DataTable(option_config_table_add);
        
        var tang=0;
        $('#AddaddRow').on( 'click', function () {
            get_select_kcb();
            tang++;
            table_category_all_add.row.add( [
                'data1'+tang,
                '<input type="checkbox">',
                'data3'+tang+2,
                'data4'+tang+3,
                'data5'+tang,
                'data6'+tang,
                'data7'+tang,
                'data8'+tang,
                'data9'+tang,
                'data9'+tang,
                'data10'+tang,
                'data11'+tang,
                'data12'+tang*3,
                'data13'+tang*2,
                'data14'+tang,
                
            ] ).draw( false );
      
        });
        //------ end function for add here
        

        //------ start function for edit EDIThere
        var option_config_table_edit ={
            "paging": true,
            "info":     false,
            // searching: false,
            "processing": true,
            //"scrollX": true,
            "serverSide": true,
            "deferLoading": 0,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            
            "ajax": {
                "url": $('body #category_name').attr('data-cat') + '/edit',
                "dataSrc": function ( json ) {
                    //after_reload_ajax_datatable(json.selectTem);
                    return json.data
                },
                "type": "GET",
                async: false, 
                cache: true,
                //                destroy: true,
                //                saveState: false,
                "data": function (d) {
                    d.type ='get_datatable',
                    d.table ='list_service_in_package_healthcare',
                    d.id_get = glb_edit_id_click
                    // if(is_onchange==true){
                    //     is_onchange=false;
                    // }
                }
                , dataType: "json"
                , "error": function (data) {
                    //table_category_all.processing(false);
                    
                    glb_show_error_ajax(data)
                },
            },
            
            
            "sDom": "<'row'<'col-sm-6'f><'col-sm-6'l>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",
            
             columns: [
                // { "targets": [0], "searchable": false, "orderable": false, "visible": true },
                { "orderable": "true" },
                { "orderable": false },
                { "orderable": "true" },
                { "orderable": "true" },
                { "orderable": "true" },
                { "orderable": "true" },
                { "orderable": "true" },
                { "orderable": "true" },
                { "orderable": "true" },
                { "orderable": "true" },
                { "orderable": "true" },
                { "orderable": "true" },
                { "orderable": "true" },
                { "orderable": "true" },
                { "orderable": "true" },
            //     {
            //     "class":          "details-control",
            //     "orderable":      false,
            //     "data":           null,
            //     "defaultContent": "<input type='checkbox'>"
            //     },
            //     { data: "2" },
            //     { data: "3" },
            //     { data: "4" },
            //     { data: "5" },
            //     { data: "6" }
             ],
             
        
        
            //columns: get_list_colum_render(),
            'createdRow': function( row, data, dataIndex ) {
                $(row).attr('data-newedit', data[0]);
                //$(row).attr('data-newadd-old', dataIndex);
                console.log(row, data, dataIndex)
            },
            "columnDefs": [
                // {
                //     "targets": [ 0 ],
                //     "visible": false,
                //     "searchable": false
                // },
                // {
                //     "targets": [ 1 ],
                //     "visible": true,
                //     "searchable": false,
                //     "orderable": false,
                // },
                {
                    "targets": 0,
                    "data": 0,
                    "defaultContent": "<input type='checkbox'>"
                    },
                {
                    "targets": 1,
                    // "data": 1,
                    "data": function ( row, type, val, meta ) {
                        return "<input type='checkbox' data-att='"+row[1]+"'>";
                    },
                    //"defaultContent": data+'__123'
                    },
                {
                "targets": 2,
                //"data": 2,
                "data": function(row, type, val, meta){
                    var attr='code';
                    return data_glb_data_json_healthcare_fidid(row[2],attr);
                    // console.log(glb_data_json_healthcare)
                    // console.log(row)
                    // if(glb_data_json_healthcare){
                    //     if(glb_data_json_healthcare[row[2]] !=undefined){
                    //         if(glb_data_json_healthcare[row[2]][attr] != undefined){
                    //             return glb_data_json_healthcare[row[2]][attr];
                    //         }else{
                    //             return row[2];
                    //         }
                    //     }else{
                    //         return row[2];
                    //     }
                    // }else{
                    //    alert('not defile') 
                    // }
                    
                    
                    
                },
                "defaultContent": "<input type='checkbox'>"
                },
                // {
                //     "targets": 4,
                //     "data": '',
                //     "defaultContent": "<input type='checkbox'>"
                // },
                {
                    "targets": 3,
                    // "data": 3,
                    "data": function(row, type, val, meta){
                        var attr='name';
                        return data_glb_data_json_healthcare_fidid(row[2],attr);
                    },
                    "defaultContent": ""
                },
                {
                    "targets": 4,
                    "data": function(row, type, val, meta){
                        //var attr='healthcare_type_id';
                        //return data_glb_data_json_healthcare(row[2],attr);
                        var attr='type';
                        return data_glb_data_json_healthcare_fidid(row[2],attr);
                    },
                    "defaultContent": ""
                },
                { "targets": 5,
                    "data": function(row, type, val, meta){
                        var attr='group';
                        return data_glb_data_json_healthcare_fidid(row[2],attr);
                    } ,"defaultContent": "" 
                },
                {   "targets": 6,
                    "data": function(row, type, val, meta){
                        var attr='unit';
                        
                        return data_glb_data_json_healthcare_fidid(row[2],attr);
                    },
                },
                { "targets": 7,"data": 7,"defaultContent": "" },
                // {   "targets": 7,
                //     "data": function(row, type, val, meta){
                //         var attr='price_in_hour';
                //         return data_glb_data_json_healthcare(row[2],attr);
                //     },
                // },
                {   "targets": 8,
                    "data": function(row, type, val, meta){
                        var attr='price_overtime';
                        return data_glb_data_json_healthcare_fidid(row[2],attr);
                    },
                },
                {   "targets": 9,
                    "data": function(row, type, val, meta){
                        var attr='price_holiday';
                        return data_glb_data_json_healthcare_fidid(row[2],attr);
                    },
                },
                {   "targets": 10,
                    "data": function(row, type, val, meta){
                        var attr='price_foreigner';
                        return data_glb_data_json_healthcare_fidid(row[2],attr);
                    },
                },
                {   "targets": 11,
                    "data": function(row, type, val, meta){
                        var attr='note';
                        return data_glb_data_json_healthcare_fidid(row[2],attr);
                    },
                },
                
                // { "targets": 9,"data": 9,"defaultContent": "" },
                // { "targets": 10,"data": 10,"defaultContent": "" },
                // { "targets": 11,"data": 11,"defaultContent": "" },
                { "targets": 12,"data": 12,"defaultContent": "" },
                {
                    "targets": 13,
                    "data": 13,
                    "defaultContent": "<input type='checkbox'>"
                },
                {
                    "targets": 14,
                    "data": 14,
                    "defaultContent": "<input type='checkbox'>"
                },
            ],
            "initComplete": function (settings, json) {
                //after_reload_ajax_datatable(json)
                
            }
        };
        var table_category_all_edit = $('#category_name [data-example="example_more_edit"]').DataTable(option_config_table_edit);
        
        var tang=0;
        $('#EditaddRow').on( 'click', function () {
            //reload_edit_table();
            //return;
            //save_edit_example_more_edit()
            //return;
            //get_select_kcb();
            tang++;
            table_category_all_edit.row.add( [
                'data1'+tang,
                '<input type="checkbox">',
                'data3'+tang+2,
                'data4'+tang+3,
                'data5'+tang,
                'data6'+tang,
                'data7'+tang,
                'data8'+tang,
                'data9'+tang,
                'data9'+tang,
                'data10'+tang,
                'data11'+tang,
                'data12'+tang*3,
                'data13'+tang*2,
                'data14'+tang,
                
            ] ).draw( false );
      
        });

        //------ end function for edit_here


        function data_glb_data_json_healthcare(position,at){
            if(glb_data_json_healthcare){
                if(glb_data_json_healthcare[position] !=undefined){
                    if(glb_data_json_healthcare[position][at] != undefined){
                        return glb_data_json_healthcare[position][at];
                    }else{
                        return position;
                    }
                }else{
                    return position;
                }
            }else{
               //alert('not defile') 
            }
        }
        function data_glb_data_json_healthcare_fidid(atid,at){
          var result = glb_data_json_healthcare.filter(function(item) {
            return item.id ==atid;
          });
           console.log(result,atid)
          if(result.length>0){
            if(undefined!=result[0][at])
            return result[0][at];
          }else{
            return '';
          }
            
        }


        function get_attribute_name_column(position){
            return $('#category_name .table-responsive [data-example="example_more"] thead tr').find('th').eq(position).attr('att-name');
        }
        function get_attribute_name_column_add(position){
            return $('#category_name [data-example="example_more_add"] thead tr').find('th').eq(position).attr('att-name');
        }
        function get_attribute_name_column_edit(position){
            return $('#category_name [data-example="example_more_edit"] thead tr').find('th').eq(position).attr('att-name');
        }

        function show_hide_datatable_loading(value){
            if(value==true || value==1){
                $('.dataTables_processing').show();
            }else{
                $('.dataTables_processing').hide();
            }
        }


    });


    //list template set static for load ding first 
    var template_status_vi={};
    template_status_vi['select-status']='<option value="1">Hiển thị</option><option value="0">Ẩn</option>';
    template_status_vi['select-nations']='<option value="0">vietnam</option><option value="1">trung quoc</option>';

}(window.jQuery);

