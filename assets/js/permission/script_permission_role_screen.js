var table_category_all;
var option_config_table;
var name_router;
var role_have_screen = [];
var id_role_active = -1;

$(document).ready(function () {
    // $('body').on('click', '#avata_user', function (e) {
    //     e.preventDefault();
    // });
    //get list role list_role.
    
    var list_sub_detail=[];

    $('#list_role>li>a').click(function () {
        id_role_active = $(this).attr('data-id');
        $('#list_role>li').removeClass('active');
        $(this).closest('li').addClass('active');
        //alert(id)
        //return;
        $.ajax({
            url: 'permission_role_screen/get',
            type: 'POST',
            dataType: "json",
            data: {
                //category_name:$('#category_name').attr('data-cat'),
                // 'layout': 'none',
                'action':'get_role_group',
                'id_role': id_role_active
            },
            beforeSend: function () {
                table_category_all.processing( true );
            },
            complete: function () {
                table_category_all.processing( false );
                //alert('success')

                //table_category_all.ajax.reload( null, false );
                //$('#myEdit').modal().hide();
                //$('#myEdit').modal('toggle'); 

            },
            success: function (data) {
                //$('#tabpermission_role_screen [data-example="example_more"]').DataTable(option_config_table);

                //var par_data = JSON.parse(data);
                var par_data = data;
                //console.log(par_data);
                role_have_screen = [];
                if (par_data !== null) {
                    for (var i = 0; i < par_data.length; i++) {
                        role_have_screen.push(par_data[i]['id_screens']);
                    }
                }
                table_category_all.ajax.reload(null, false);
                //console.log(role_have_screen);

            }, error: function (data) {
                console.log(data);
                glb_show_error_ajax(data)
            }
        });
    });

    //load table 
    //name_router = $('#tabpermission_role_screen').attr('data-cat');
    option_config_table = {
        //             paging: false,
        //            searching: false,
        "processing": true,
        //"scrollX": true,
        "serverSide": true,
        destroy: true,
        searching: false,
        "language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },

        //"ajax": "category_api",
        "ajax": {
            //"url": "manage_user_api",
            // url: name_router + '/get',
            url: 'permission_role_screen/get',
            //"url":name_router ,
            "type": "POST",
            //                "draw":0, //not why
            async: true, //not why
            cache: false, //not why
            //                destroy: true,
            //                saveState: false,
            dataType: "json",
            "data": function (d) {
                d.action='get_list_main_screen'
                //csrfName=csrfHash,
                //d[csrfName]=csrfHash,
                //d.push({csrfName:csrfHash}),
                //d.type_submit = $('#type_submit').attr('data-cat'); //set at leftbar.php

            },
            "error": function (data) {
                table_category_all.processing( false );
                console.log(data);
                glb_show_error_ajax(data)
                
            },complete:function(json){
                list_sub_detail = json.responseJSON.submore;
            }
            
        },
        //            deferLoading: true,
        //"bProcessing": true,
        "sDom": "<'row'<'col-sm-6'f><'col-sm-6'l>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
        "sPaginationType": "full_numbers",
        /*	
         columns: [
         { data: "0" },
         {
         "class":          "details-control",
         "orderable":      false,
         "data":           null,
         "defaultContent": "<input type='checkbox'>"
         },
         { data: "2" },
         { data: "3" },
         { data: "4" },
         { data: "5" },
         { data: "6" }
         ],
         */


        //columns: get_list_colum_render(),
        "columnDefs": [
        
            {
            "targets": 1,
            "data": function (data) {
                
                return '<a href="'+data[2]+'">'+data[1]+'</a>';
            },
            "defaultContent": ''
        },
            {
                "targets": 2,
               "data": function(data){
                //get/view
                var id_at;
                if(list_sub_detail[data[0]]){
                    id_at = list_sub_detail[data[0]]['get'];
                    if(!id_at){
                        return '';
                    }
                    var show_id ='data_id_event='+id_at;
                    
                    if(role_have_screen.indexOf(id_at)>=0){
                        return "<input type='checkbox' checked "+show_id+" type_action='view'>";
                    }else{
                        return "<input type='checkbox' "+show_id+"  type_action='view'>";
                    }
                }else{
                    return;
                }
            },
                "defaultContent": "<input type='checkbox'>"
            },
            {
                "targets": 3,
                "data": function(data){
                    //add
                    var id_at;
                    if(list_sub_detail[data[0]]){
                        id_at = list_sub_detail[data[0]]['add'];
                        if(!id_at){
                            return '';
                        }
                        var show_id ='data_id_event='+id_at;
                        //console.log(role_have_screen.indexOf(id_at),id_at)
                        if(role_have_screen.indexOf(id_at)>=0){
                            return "<input type='checkbox' checked "+show_id+">";
                        }else{
                            return "<input type='checkbox' "+show_id+">";
                        }
                    }else{
                        return;
                    }
                    
                },
                "defaultContent": "<input type='checkbox'>"
            },
            {
                "targets": 4,
                "data": function(data){
                    //edit
                    var id_at;
                    if(list_sub_detail[data[0]]){
                        id_at = list_sub_detail[data[0]]['edit'];
                        if(!id_at){
                            return '';
                        }
                        var show_id ='data_id_event='+id_at;
                        if(role_have_screen.indexOf(id_at)>=0){
                            return "<input type='checkbox' checked "+show_id+">";
                        }else{
                            return "<input type='checkbox' "+show_id+">";
                        }
                    }else{
                        return;
                    }
                },
                "defaultContent": "<input type='checkbox'>"
            },
            {
                "targets": 5,
                "data": function(data){
                    //delete
                    var id_at;
                    if(list_sub_detail[data[0]]){
                        id_at = list_sub_detail[data[0]]['delete'];
                        if(!id_at){
                            return '';
                        }
                        var show_id ='data_id_event='+id_at;
                        if(role_have_screen.indexOf(id_at)>=0){
                            return "<input type='checkbox' checked "+show_id+">";
                        }else{
                            return "<input type='checkbox' "+show_id+">";
                        }
                    }else{
                        return;
                    }
                },
                "defaultContent": "<input type='checkbox'>"
            },
            {
                "targets": 6,
                "data": function(data){
                    //push
                    var id_at;
                    if(list_sub_detail[data[0]]){
                        id_at = list_sub_detail[data[0]]['push'];
                        if(!id_at){
                            return '';
                        }
                        var show_id ='data_id_event='+id_at;
                        if(role_have_screen.indexOf(id_at)>=0){
                            return "<input type='checkbox' checked "+show_id+">";
                        }else{
                            return "<input type='checkbox' "+show_id+">";
                        }
                    }else{
                        return;
                    }
                },
                "defaultContent": "<input type='checkbox'>"
            },
            {
                "targets": 7,
                "data": function(data){
                    //unpush
                    var id_at;
                    if(list_sub_detail[data[0]]){
                        id_at = list_sub_detail[data[0]]['unpush'];
                        if(!id_at){
                            return '';
                        }
                        var show_id ='data_id_event='+id_at;
                        if(role_have_screen.indexOf(id_at)>=0){
                            
                            return "<input type='checkbox' checked "+show_id+">";
                        }else{
                            return "<input type='checkbox' "+show_id+">";
                        }
                    }else{
                        return;
                    }
                },
                "defaultContent": "<input type='checkbox'>"
            },
        ],
        "initComplete": function (settings, json) {
            //console.log(json.submore);
            
            //console.log(json.csrfName);
            //console.log(json.csrfHash);
            //$('div.loading').remove();
            //csrfName = json.csrfName;
            //csrfHash = json.csrfHash;
        }
    };
    table_category_all = $('#tabpermission_role_screen [data-example="example_more"]').DataTable(option_config_table);

    $('body ').on('click', '#tabpermission_role_screen .table-responsive table tbody input[type="checkbox"]', (function () {
        //alert('ádf');
        // var c_current = 0;
        // $('#tabpermission_role_screen [data-example="example_more"] td input[type="checkbox"]').each(function () {
        if (id_role_active == -1) {
            alert('Need select role')
            return;
        }
        
        var checked = false;
        var sThisVal = (this.checked ? $(this).val() : "");
        if (sThisVal) {
            checked = true;
        }
        var id_screen_main = $(this).closest('tr').find('td').first().html();
        //alert(id_line)
        let id_line = $(this).attr('data_id_event');
        let type_view = $(this).attr('type_action'); //jsut for view
        if (type_view === undefined) {
            type_view='';
        }
        
        $.ajax({
            url: 'permission_role_screen/edit',//add, push/unpush
            type: 'POST',
            dataType: "json",

            data: {
                //category_name:$('#category_name').attr('data-cat'),
                // 'layout': 'none',
                'id_role': id_role_active,
                'id_screen': id_line,
                'id_screen_main': id_screen_main,
                'type_view': type_view, //view/''
                'checked': checked,
            },
            beforeSend: function () {
                table_category_all.processing( true );
            },
            complete: function () {
                //alert('success')

                //table_category_all.ajax.reload( null, false );
                //$('#myEdit').modal().hide();
                //$('#myEdit').modal('toggle'); 
                table_category_all.processing( false );
                //sau khi update xong thi can update lai list role cua admin (reload list role of current user-for show checked)
                $('#list_role>.active>a').trigger('click');
            },
            success: function (data) {
                //$('#tabpermission_role_screen [data-example="example_more"]').DataTable(option_config_table);
                
                //table_category_all.ajax.reload( null, false );
                //console.log(role_have_screen);

            }, error: function (data) {
                console.log(data);
                glb_show_error_ajax(data)
            }
        })
    }));

    $('#list_role>li>a:first').trigger('click');

})