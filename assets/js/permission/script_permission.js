var table_category_all;
var option_config_table;
var name_router;
//alert(name_router)

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
+function ($) {
    "use strict";
    
    $(function () {
        name_router =$('#permission_name').attr('data-cat');    
        option_config_table ={
//             paging: false,
//            searching: false,
        destroy: true,
        searching: true,
            "processing": true,
            //"scrollX": true,
            "serverSide": true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
 
            //"ajax": "category_api",
            "ajax": {
                //"url": "manage_user_api",
                url: name_router+'/get',
                //"url":name_router ,
                "type": "POST",
//                "draw":0, //not why
               async: true, //not why
               cache: false, //not why
//                destroy: true,
//                saveState: false,
                "data": function (d ) {
                    //csrfName=csrfHash,
                    //d[csrfName]=csrfHash,
                    //d.push({csrfName:csrfHash}),
                    //d.type_submit = $('#type_submit').attr('data-cat'); //set at leftbar.php
                   
                }
                ,dataType: "json"
                ,"error": function (data) {
                    table_category_all.processing( false );
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            },
//            deferLoading: true,
            //"bProcessing": true,
            "sDom": "<'row'<'col-sm-6'f><'col-sm-6'l>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",
            /*	
             columns: [
             { data: "0" },
             {
             "class":          "details-control",
             "orderable":      false,
             "data":           null,
             "defaultContent": "<input type='checkbox'>"
             },
             { data: "2" },
             { data: "3" },
             { data: "4" },
             { data: "5" },
             { data: "6" }
             ],
             */


            //columns: get_list_colum_render(),
            "columnDefs": [ {
                "targets": 1,
                "data": null,
                "defaultContent": "<input type='checkbox'>"
                } 
            ],
            "initComplete": function( settings, json ) {
                //console.log(json.csrfName);
                //console.log(json.csrfHash);
                //$('div.loading').remove();
                //csrfName = json.csrfName;
                //csrfHash = json.csrfHash;
              }
        };
        table_category_all = $('#permission_name [data-example="example_more"]').DataTable(option_config_table);

        //after page load false- ----------------------------
        //alert('test');
        $('#permission_name [data-example="example_more"] td span:nth-child(1)').click(function () {
            //alert('edit');
            var findlisttd = $(this).closest('tr').find('td');
            var leng = $(findlisttd).length;
            var list = [];
            $(findlisttd).each(function (index) {
                if (index != (leng - 1)) {
                    //console.log(index,leng)
                    console.log($(this).html()); //value of td in row different column edit/delete.
                    list.push($(this).html());
                }
            });
            get_allinptu_edit(list);
            $('.nav-tabs a[href="#edit"]').tab('show');
            //console.log(a);
        });
//        $('[data-example="example_more"] td:nth-child(2)').click(function(){
//            alert('delete in working');
//            
//
//        });

//        $('body').on('click', '#DataTables_Table_0 .sorting_1', (function () {
//            alert('delete in working');
//        }));

        $('body ').on('click', '#permission_name .table-responsive table tbody input[type="checkbox"]', (function () {
            var c_current = 0;
            $('[data-example="example_more"] td input[type="checkbox"]').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal) {
                    c_current++;
                }
            });
            //alert(c_current);
            show_hide_button(c_current);
        }));

        function show_hide_button(c_current) {
            if (c_current == 0) {
                //Do stuff
                //alert('hello')
                $('#btn_edit').addClass('disabled');
                $('#btn_publish').addClass('disabled');
                $('#btn_unpublish').addClass('disabled');
                $('#btn_delete').addClass('disabled');
            }
            if (c_current == 1) {
                $('#btn_edit').removeClass('disabled');
                $('#btn_publish').removeClass('disabled');
                $('#btn_unpublish').removeClass('disabled');
                $('#btn_delete').removeClass('disabled');
            }
            if (c_current > 1) {
                $('#btn_edit').addClass('disabled');
                $('#btn_publish').removeClass('disabled');
                $('#btn_unpublish').removeClass('disabled');
                $('#btn_delete').removeClass('disabled');
            }
        }

        /**
         * When Click Edit in gui just get list data from current row to show.
         */
//$('data-target="#myEdit"').on('click', '.sorting_1' , (function(){
        $('body ').on('click', '#permission_name [data-target="#myEdit"]', (function () {
            //alert('Edit event');
            $('#permission_name [data-example="example_more"] td input').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal) {
                    var findlisttd = $(this).closest('tr').find('td');
                    var leng = $(findlisttd).length;
                    var list = [];
                    $(findlisttd).each(function (index) {
                        //if (index != (leng - 1)) {
                        //console.log(index,leng)
                        //console.log($(this).html()); //value of td in row different column edit/delete.
                        list.push($(this).html());
                        //}
                    });
                    //console.log(list);
                    get_allinptu_edit(list);

                    return;
                }
                //console.log(sThisVal)
            });

        }));
        $('body ').on('click', '#permission_name [data-target="#myDelete"]', (function () {
            //alert('Edit event');
            var list_id = [];
            var list_all = [];
            $('#permission_name [data-example="example_more"] td input').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal) {
                    var list_id = $(this).closest('tr').find('td').first().html();
                    list_all.push(list_id);
                }
            });

            if (list_all[0]) {
                get_allinptu_delete(list_all.join());
            }

        }));
        $('body ').on('click', '#permission_name [data-target="#myPush"]', (function () {
            //alert('Edit event');
            var list_id = [];
            var list_all = [];
            $('#permission_name [data-example="example_more"] td input').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal) {
                    var list_id = $(this).closest('tr').find('td').first().html();
                    list_all.push(list_id);
                }
            });

            if (list_all[0]) {
                get_allinptu_push(list_all.join());
            }

        }));
        $('body ').on('click', '#permission_name [data-target="#myUnpush"]', (function () {
            //alert('Edit event');
            var list_id = [];
            var list_all = [];
            $('#permission_name [data-example="example_more"] td input').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal) {
                    var list_id = $(this).closest('tr').find('td').first().html();
                    list_all.push(list_id);
                }
            });

            if (list_all[0]) {
                get_allinptu_unpush(list_all.join());
            }

        }));



        function get_allinptu_edit(list) {
            $('#permission_name #edit form input').each(function (index) {

                if ($(this).attr('type') == 'checkbox') {
                    if (list[index] == 'Bật' || list[index] == 1) {
                        $(this)[0].checked = true;
//                        $(this).closest('.switch').find('span').addClass('checked');
                        // $(this).checked;
                        //alert('set')
                    } else {
                        $(this).attr('checked', false);
//                        $(this).closest('.switch').find('span').removeClass('checked');
                    }
                } else {
                    $(this).val(list[index]);
                }
                //console.log();
            })
        }
        function get_allinptu_delete(list) {
            $('#delete form input').val(list);

        }
        function get_allinptu_push(list) {
            $('#push form input').val(list);

        }
        function get_allinptu_unpush(list) {
            $('#unpush form input').val(list);

        }
        ///[name!='newsletter']
        //data-dismiss="modal"
        $('body ').on('click', '#permission_name #add .modal-footer button[data-dismiss!="modal"]', (function () {
            //$('#add .modal-footer button[data-dismiss!="modal"] ').click(function () {
            //alert('sa');
            var list_in = [];
            $('#add .modal-content input').each(function (index) {
                if ($(this).attr('type') == 'checkbox') {

                    var curr = $(this).eq(0).attr("checked") ? 1 : 0;
                    //alert(curr);
                }
                var at = $(this).val();
                console.log(at);
                list_in.push(at);
            });

            $.ajax({
                url: name_router+'/add',
                type: 'POST',
                dataType: "json",
                data: {
                    type_submit: $('#type_submit').attr('data-cat'),
                    'input': list_in,
                },
                beforeSend: function () {

                },
                complete: function () {
                    $('#permission_name [data-example="example_more"]').DataTable().ajax.reload(null, false);
                    //table_category_all.page( 'last' ).draw( 'page' );
//                    table_category_all.ajax.reload(null, false);
                    //$('[data-example="example_more"]').DataTable(option_config_table);

                    $('#myAdd').modal('toggle');
                },
                success: function (data) {
                    console.log(data);
                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            }
            )
        }));
        $('body ').on('click', '#permission_name #edit .modal-footer button[data-dismiss!="modal"]', (function () {
            //$('#edit .modal-footer button[data-dismiss!="modal"] ').click(function () {
            //alert('sa');
            var list_in = [];
            $('#edit .modal-content input').each(function (index) {
                if ($(this).attr('type') == 'checkbox') {

                    var curr = $(this).eq(0).attr("checked") ? 1 : 0;
                    //alert(curr);
                }
                if (index == 1)
                    return; //load bo input chua checkbok select edit.
                var at = $(this).val();
                //console.log(at);
                list_in.push(at);
            });

            $.ajax({
                url: name_router+'/edit',
                type: 'POST',
                dataType: "json",
                data: {
                    type_submit: $('#type_submit').attr('data-cat'),
                    'input': list_in,
                },
                beforeSend: function () {

                },
                complete: function () {
                    //alert('success')
                    //table_category_all.draw( true );
                    $('[data-example="example_more"]').DataTable().ajax.reload(null, false);

                    //$('[data-example="example_more"]').DataTable(option_config_table);
//                    table_category_all.ajax.reload(function(){
//                        alert('12')
////                        table_category_all.order([ 1, 'asc' ]).draw();
//                        table_category_all.order.listener( '#sorter', 0 ).draw();
//                    }, true);

                    //table_category_all.draw();;

                    //$('#myEdit').modal().hide();
                    $('#myEdit').modal('toggle');
                    show_hide_button(0);//set all off button

                },
                success: function (data) {
                    console.log(data);

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            }
            )
        }));
        $('body ').on('click', '#permission_name #delete .modal-footer button[data-dismiss!="modal"]', (function () {
            var list_in = $('#delete .modal-content input').val();

            $.ajax({
                url: name_router+'/delete',
                type: 'POST',
                dataType: "json",
                data: {
                    type_submit: $('#type_submit').attr('data-cat'),
                    'input': list_in,
                },
                beforeSend: function () {

                },
                complete: function () {
                    $('[data-example="example_more"]').DataTable().ajax.reload(null, false);
                    $('#myDelete').modal('toggle');
                    show_hide_button(0);//set all off button
                },
                success: function (data) {
                    console.log(data);

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            }
            )
        }));

        $('body ').on('click', '#permission_name #push .modal-footer button[data-dismiss!="modal"]', (function () {
            var list_in = $('#push .modal-content input').val();

            $.ajax({
                url: name_router+'/push',
                type: 'POST',
                dataType: "json",
                data: {
                    type_submit: $('#type_submit').attr('data-cat'),
                    'input': list_in,
                },
                beforeSend: function () {

                },
                complete: function () {
                    $('[data-example="example_more"]').DataTable().ajax.reload(null, false);
                    $('#myPush').modal('toggle');
                    show_hide_button(0);//set all off button
                },
                success: function (data) {
                    console.log(data);

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            }
            )
        }));
        $('body ').on('click', '#permission_name #unpush .modal-footer button[data-dismiss!="modal"]', (function () {
            var list_in = $('#unpush .modal-content input').val();

            $.ajax({
                url: name_router+'/unpush',
                type: 'POST',
                dataType: "json",
                data: {
                    type_submit: $('#type_submit').attr('data-cat'),
                    'input': list_in,
                },
                beforeSend: function () {

                },
                complete: function () {
                    $('[data-example="example_more"]').DataTable().ajax.reload(null, false);
                    $('#myUnpush').modal('toggle');
                    show_hide_button(0);//set all off button
                },
                success: function (data) {
                    console.log(data);

                }, error: function (data) {
                    console.log(data);
                    glb_show_error_ajax(data)
                }
            }
            )
        }));


//        $('#add .switch input').on('switchChange.bootstrapSwitch', function(event, state) {
//            alert('change')
//            if (state)
//            {
//                // Checked
//                alert('change t')
//            }else
//            {
//                // Is not checked
//                alert('change f')
//            }
//        });

        //chỉ các menu trong list_cateogry_menu khi click thì ajax tại từng tab đó.
        $('.nav #list_user_menu ul li a').click(function (event) {
            alert('script-permission.js load')
            event.preventDefault();
            //alert($(this).attr('href'));
            var get_url = $(this).attr('href');
            if (get_url != '#') {
                window.history.pushState('page2', 'Title', get_url);
                $.ajax({
                    url: get_url,
                    type: 'GET',
                    //dataType: "json",

                    data: {
                        //category_name:$('#category_name').attr('data-cat'),
                        'layout': 'none',
                    },
                    beforeSend: function () {

                    },
                    complete: function () {
                        //alert('success')

                        //table_category_all.ajax.reload( null, false );
                        //$('#myEdit').modal().hide();
                        //$('#myEdit').modal('toggle'); 

                    },
                    success: function (data) {
                        //alert('change data');
                        $('section#content').replaceWith($(data));

                        //table_category_all.columns().dataSrc();
                        //(table_category_all.fnDraw());
                        //console.log(table_category_all.ajax);
                        //table_category_all.ajax.params();
                        //$('[data-example="example_more"]').empty();
                        $('[data-example="example_more"]').DataTable(option_config_table);
                        //table_category_all.ajax.reload(null, true);
                        //console.log(data);

                    }, error: function (data) {
                        console.log(data);
                        glb_show_error_ajax(data)
                    }
                });
            }
        });
    });
}(window.jQuery);