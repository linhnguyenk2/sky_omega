$(document).ready(function () {
    'use strict';

    $("#btnChangePass").on('click', function (e) {
        e.preventDefault();

        if ($("#txtPassword1").val() != $("#txtPassword2").val()) {
            $('#form-change-pass .input-error').delay(500).fadeIn(1000);
            return;
        } else {
            $('#form-change-pass .input-error').fadeOut(500);
        }

        var data = {
            password: $("#txtChangePass1").val(),
        };

        $.ajax({
            type: 'POST',
            url: './index.php/util/changePass',
            data: data,
            success: function(res) {
                var data = JSON.parse(res);
                if (data.ok != '0') {
                    window.location.reload();
                }
            }
        });
    });

    $("#btnSignIn").on('click', function (e) {
        var data = {
            username: $("#txtEmail").val(),
            password: $("#txtPassword").val(),
            device_id: $("#txtDeviceId").val()
        };

        $.ajax({
            type: 'POST',
            url: 'api/v1/login/login',
            data: data,
            success: function(res) {
                var data = JSON.parse(JSON.stringify(res));

                // empty data - login false
                if (!data.data) {
                    $('#showmessage_login, #showmessage_login #mserror2 ').show();
                    setTimeout(() => {
                            $('#showmessage_login, #showmessage_login #mserror2').hide();
                    }, 3000);
                }
                else {
                    document.location.href = '/dashboard';
                }

                /*if (data.ok != '0') {
                    document.location.href = '/dashboard'
                }else{

                    $('#showmessage_login, #showmessage_login #mserror2 ').show();
                    setTimeout(() => {
                        $('#showmessage_login, #showmessage_login #mserror2').hide();
                    }, 3000);
                }*/
            }
        });


        e.preventDefault();
        return false;
    });

    $("#btnSignOut").on('click', function (e) {
        $.ajax({
            type: 'POST',
            url: './user/signOut',
            data: null,
            success: function(res) {
                var data = JSON.parse(JSON.stringify(res));
                if (data.ok != '0') {
                    window.location.href = './';
                }
            }
        });

        e.preventDefault();
        return false;
    });

    $("#forgotPassword").on('click', function (e) {
        e.preventDefault();

        if ($('#txtEmail').val() == '') {
            $('#modal-login').modal('hide');
            $('#modalTitle').html('Vui lòng nhập địa chỉ email. Chúng tôi sẽ gửi mật khẩu mới đến địa chỉ này.');
            $('#myModal').modal('show');
            return;
        }

        if (!isValidEmail($('#txtEmail').val())) {
            $('#modal-login').modal('hide');
            $('#modalTitle').html('Địa chỉ email không hợp lệ.');
            $('#myModal').modal('show');
            return;
        }

        $.ajax({
            type: 'POST',
            url: './index.php/util/genPass',
            data: {email: $('#txtEmail').val()},
            success: function(res) {
                var data = JSON.parse(res);
                if (data.ok != '0') {
                    $('#modal-login').modal('hide');
                    $('#modalTitle').html('Mật khẩu mới đã được gửi vào email của bạn. Vui lòng kiểm tra thư.');
                    $('#myModal').modal('show');
                } else {
                    $('#modal-login').modal('hide');
                    $('#modalTitle').html('Tài khoản với email này chưa được tạo.');
                    $('#myModal').modal('show');
                }
            }
        });
    });

    function formatMoney(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    function isValidEmail(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }
});
