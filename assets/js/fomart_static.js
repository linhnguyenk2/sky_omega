/**
 * api return format datetime yyyy-mm-dd H:i:s
 * show front end default dd/mm/yyyy or HH:mm dd/mm/yyyy
 */

function _formart_static(type, value) {
    console.log('in format_static_'+value);
    var str;
    switch (type) {
        case 'dd/mm/yyyy':
            str= _type_fomart_date1(type, value);
            break;
        case 'HH:mm dd/mm/yyyy': 
            str= _type_fomart_date3(type, value);
            break;
        case 'dd/mm/yyyy-yyyy/mm/dd': 
            //convert use for save
            str= _type_fomart_date_save_1(type, value);
            break;
        case 'HH:mm dd/mm/yyyy-yyyy/mm/dd': 
            //convert use for save
            str= _type_fomart_date_save_2(type, value);
            break;
        default:
            str= _type_fomart_date1(type, value);
    }
    console.log('out format_static_'+str)
    return str
}
function _type_fomart_date1(type, value) {
    var date;
    date = new Date();
    if(value){
        date = new Date(value);
    }
    var at  = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    return at
}
function _type_fomart_date3(type, value) {
    var date;
    date = new Date();
    if(value){
        date = new Date(value);
    }
    var at  = date.getHours() + ':' + date.getMinutes() + ' ' + date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    return at
}
function _type_fomart_date_save_1(type, value) {
    var numbers = value.split("/");
    var date = new Date(numbers[2], numbers[1]-1, numbers[0]);
    return date.getFullYear()+ '-' + (date.getMonth() + 1) + '-' + date.getDate();
}
function _type_fomart_date_save_2(type, value) {
    var all = value.split(" ");
    var time = all[0].split(":");
    var numbers = all[1].split("/");
    var date = new Date(numbers[2], numbers[1]-1, numbers[0]);
    return date.getFullYear()+ '-' + (date.getMonth() + 1) + '-' + date.getDate()+' '+time[0]+":"+time[1]+":"+"00";
}
function _get_currenttime()
{
    var date = new Date()
    return date.getFullYear()+ '-' + (date.getMonth() + 1) + '-' + date.getDate()+' '+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
}