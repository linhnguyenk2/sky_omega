var link_api = link_base;

$(document).ready(function () {
    'use strict';

    $("#save_book").click(function (e) {
        e.preventDefault();
        var at = $('#info_thamkham_phukhoa').find('.form-control');
        var data={};
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-save');
            var type = $(this).prop("tagName");
            if (attr) {
                var value_inpt = $(this).val()
                var attr_format = $(this).attr('attr-fomart-save');
                if (attr_format) {
                    value_inpt = _formart_static(attr_format, value_inpt)
                }
                data[attr]=value_inpt
                if (type == 'SELECT') {
                }
            }
        })
        console.log(data)
        // var linkapi =$(this).closest('section.panel-default').attr('attr-link-main');
        var linkapi =$(this).closest('section.panel-default').attr('attr-link-update');
        var at_id = $('section.panel-default').attr('attr-link-update-id');
        var link = link_api+linkapi+'/'+at_id
        //var user_id_get =$(this).attr('user_id_get');
        //link = link_base+link
        put_data_api(link,data,function(statusresult, result){
            console.log('save ok')
            glb_show_message('save success')
        });
    })

    $('body ').on('click', 'table tbody tr td:nth-child(2)', (function () {
        var id = $(this).closest('tr').attr('id_colum');
        var link_part = $(this).closest('table').find('thead tr').attr('link-onclick');
        if(id && link_part){
            // var link =link_api+'outpatient_book_detail/'+id;
            var link =link_api+link_part+'/'+id;
            window.location.href=link
        }
    }));


    load_data()
})

var curent_gynecolog_form_id='';
var curent_patient_id='';
var curent_staff_id='';

function load_data(){
    var linkapi = $('section.panel-default').attr('attr-link-main');
    var at_id = $('section.panel-default').attr('at-id');
    var link = link_api+linkapi+'/'+at_id
    get_data_api(link, {}, function (statusresult, result) {
        show_data_for_all(result.data);
    })
}

function show_data_for_all(data){
   
    if(data.length<1){
        alert('Not exit');
        return;
    }

    var id_of_save=data.gynecolog_form_id.id;
    
    curent_gynecolog_form_id =data.gynecolog_form_id.id;
    curent_patient_id =data.patient_id.id;
    curent_staff_id =data.staff_id.id;
    curent_medical_record_id =data.medical_record_id.id;
    curent_medical_examination_form_id =data.id;
    
    $('#input_hidden #id_in_patient_id').val(curent_patient_id);
    $('#input_hidden #id_in_staff_id').val(curent_staff_id);
    $('#input_hidden #id_in_medical_examination_form_id').val(curent_medical_examination_form_id);
    
    $('#in_tb_prescription_paper_id').val(curent_patient_id)

    $(".form-control[attr-save='patient_id']").val(curent_patient_id);
    $(".form-control[attr-save='staff_id']").val(data.staff_id.id);
    $(".form-control[attr-save='medical_examination_form_id']").val(data.id);
    var at_current =$(".form-control[attr-save='staff_id']");

    $('section.panel-default').attr('attr-link-update-id',id_of_save);


    var data_at=data
    var list_info = $('#info_thamkham_phukhoa').find('.form-control');
	$.each(list_info,function(index,value){
		var attr = $(this).attr('attr-name');
        var at = resolve_keys_object(attr,data_at)
		if(attr && at){
                    var attr_format = $(this).attr('attr-fomart-show');
                    if (attr_format) {
                        at = _formart_static(attr_format, at)
                        $(this).val(at);
                    } else {
                        $(this).val(at);
                    }
		}
    })

    load_list_table();
}


//load list sub table
function load_list_table(){
    var list_tb = $('section.panel-default').find('table')
    $.each(list_tb,function(idnex,value){
        load_1_table(this)
    })
}

function load_1_table(table){
    var table_id =  $(table).attr('id')
    var attr_autoload  =$(table).attr('autoload')
    if(!$(table).attr('data-link-api') || attr_autoload=='false'){
        return;
    }
    //var param_request ='gynecolog_form_id='+curent_gynecolog_form_id +'&patient_id='+curent_patient_id;
    var param_request =$('#id_tb_cddv').attr('data-para');
    //if(table_id=='id_tb_cddv'){
        //alert(param_request)
        $(table).attr('data-para',param_request);
    //}
    //link_of_table = link_base + $(table).attr('data-link-api')+'/?'+param_request;
    link_of_table = link_base + $(table).attr('data-link-api')+'/?'+param_request;
    loadTableInt('#'+table_id,link_of_table);
}

function load_list_select(){
    //select class="form-control" api-link="api/v1/list/service_type" attr-save="type"

}

function load_th_select_footer(tem_table){
    var at = $(tem_table).find('tfoot tr th');
    $.each(at,function(){
        var attr = $(this).attr('data-type-select');
        if(attr){
            alert('12345')
        }
    })
}