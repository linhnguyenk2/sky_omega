
//assets\js\book\gynecological_examination_tpcn.js
//need gynecological_examination_function_table.js

$(document).ready(function () {
    'use strict';

    var temtable_tpcn_mp_vtyt = '#myTpcn_mp_vtyt table';
    //var at_list_table_group_tpcn_mp_vtyt='#myTpcn_mp_vtyt';

    var at_table_tpcn = '#id_tb_tpcn_in_tpcn_mp_vtyt_paper';
    var at_table_mp = '#id_tb_mp_in_tpcn_mp_vtyt_paper';
    var at_table_vtyt = '#id_tb_vtyt_in_tpcn_mp_vtyt_paper';
    //load list first select
    tb_load_select_first(temtable_tpcn_mp_vtyt);

    /// table 1
    //start table toa thuoc
    $('body ').on('change', '#group_tb_tpcn_mp_vtyt tbody input[type="checkbox"]', (function () {
        console.log('#group_tb_tpcn_mp_vtyt checkbox-show hide')
        var tem_tb = $('#group_tb_tpcn_mp_vtyt').find('table').eq(0);
        show_hide_button_table(tem_tb);
        show_hide_button_edit(tem_tb);
    }));
    
    
     $('body ').on('click', '#group_tb_tpcn_mp_vtyt tbody tr td', (function () {
        var colindex = $(this).parent().children().index($(this));
        if (colindex == 0)
            return;
        var id = $(this).closest('tr').attr('id_colum');
        $(this).closest('tr').find('td').eq(0).find('input').prop('checked', true);
        $('#group_tb_tpcn_mp_vtyt .edit_select').trigger('click');
       // $('#hidden_type_edit_service_id').val(id);
    }));
    
    $('body ').on('click', '#group_tb_tpcn_mp_vtyt .edit_select', (function () {
        console.log('#group_tb_tpcn_mp_vtyt edit')
        var list_id = [];
        var mainid = $(this).closest('.table-responsive').find('table td input'); //checkbox
        var id_code = '';
        mainid.each(function (index, value) {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal == 'on') {
                var tem_id = $(this).closest('tr').attr('id_colum');
                id_code = $(this).closest('tr').find('td').eq(1).text();
                list_id.push(tem_id)
            }
        });
        $('#text-ma-tpcn-mp-vtyt').text(id_code)
        $('#in_tb_tpcn_mp_vtyt_paper_id').val(list_id[0]);

        emptyTable(at_table_mp);
        emptyTable(at_table_tpcn);
        emptyTable(at_table_vtyt);

        //load table sub diffirent
        var link_of_table;
        
        link_of_table = link_base + $(at_table_mp).attr('data-link-api');
        
        var at_para = $(at_table_mp).attr('data-para');
        at_para = at_para+'&sih_tpcn_mp_vtyt_paper:id=' + list_id[0]
        $(at_table_mp).attr('data-para', at_para)
        loadTableInt(at_table_mp, link_of_table + '/?'+at_para);
        
        link_of_table = link_base + $(at_table_tpcn).attr('data-link-api');
        at_para = $(at_table_tpcn).attr('data-para');
        at_para = at_para+'&sih_tpcn_mp_vtyt_paper:id=' + list_id[0]
        $(at_table_tpcn).attr('data-para', at_para)
        loadTableInt(at_table_tpcn, link_of_table + '/?'+at_para);
        
        link_of_table = link_base + $(at_table_vtyt).attr('data-link-api');
        at_para = $(at_table_vtyt).attr('data-para');
        at_para = at_para+'&sih_tpcn_mp_vtyt_paper:id=' + list_id[0]
        $(at_table_vtyt).attr('data-para', at_para)
        loadTableInt(at_table_vtyt, link_of_table + '/?'+at_para);

    }));
    //end table 1

    //event click check input in head --
    $('body ').on('click', temtable_tpcn_mp_vtyt + ' thead input[type="checkbox"]', (function () {
        //set all check
        console.log('click checkbox')
        var sThisVal = $(this).prop('checked');
        $(temtable_tpcn_mp_vtyt + ' tbody input[type="checkbox"]').each(function () {
            $(this).prop('checked', sThisVal);
        });
        show_hide_button_table(temtable_tpcn_mp_vtyt);
    }));
    //event click check input in content
    $('body ').on('change', temtable_tpcn_mp_vtyt + ' tbody input[type="checkbox"]', (function () {
        show_hide_button_table(temtable_tpcn_mp_vtyt);
        console.log('click checkbox '+temtable_tpcn_mp_vtyt)
    }));

    //event diffirent, get new a code of thuoc
    $('#add_myTpcn_mp_vtyt').click(function () {
        console.log('click #add_myTpcn_mp_vtyt')
        //add new
        var get_tb = $(this).closest('.table-responsive').find('table');
        var link = get_tb.attr('data-link-api');
        var data = {}
        data.patient_id = curent_patient_id;
        data.staff_id = curent_staff_id;
        data.medical_examination_form_id = curent_medical_examination_form_id;

        add_danhsach_tpch_mp_vtyt(link_base + link, data, function (status, info) {
            var id_new = info.data.id;
            var id_code = info.data.code;
            //$('#text-ma-toathuoc').text(id_code)

            $('#text-ma-tpcn-mp-vtyt').text(id_code) //show title popup
            $('#in_tb_tpcn_mp_vtyt_paper_id').val(id_new);

            //empty table after add id

            //at_table = '#id_tb_tpcn_in_tpcn_mp_vtyt_paper';
            var at_para = $(at_table_tpcn).attr('data-para');
            $(at_table_tpcn).attr('data-para', at_para+'&sih_tpcn_mp_vtyt_paper:id=' + id_new)
            emptyTable(at_table_tpcn);

            //at_table = '#id_tb_mp_in_tpcn_mp_vtyt_paper';
            at_para = $(at_table_mp).attr('data-para');
            $(at_table_mp).attr('data-para', at_para+'&sih_tpcn_mp_vtyt_paper:id=' + id_new)
            emptyTable(at_table_mp);

            //at_table = '#id_tb_vtyt_in_tpcn_mp_vtyt_paper';
            at_para = $(at_table_vtyt).attr('data-para');
            $(at_table_vtyt).attr('data-para', at_para+'&sih_tpcn_mp_vtyt_paper:id=' + id_new)
            emptyTable(at_table_vtyt);

            var link_of_table =link_base+ $(get_tb).attr('data-link-api')+'/?'+$(get_tb).attr('data-para');
            var table_id = $(get_tb).attr('id')
            loadTableInt('#'+table_id,link_of_table);

        });
    });

    //event add new a data at_table_tpcn
    $('body ').on('keyup', at_table_tpcn + ' tfoot tr th', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        console.log('enter add at_table_tpcn')
        if (e.which == 13) {
            var data = {};
            data.tpcn_mp_vtyt_paper_id = $('#in_tb_tpcn_mp_vtyt_paper_id').val();
            data.product_group_id='2';
            data.type='2';
            add_row_table_3(at_table_tpcn, data);
        }
        if (e.which == 27) {

        }
    }));
    //event add new a data at_table_mp
    $('body ').on('keyup', at_table_mp + ' tfoot tr th', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        console.log('enter add at_table_mp')
        if (e.which == 13) {
            var data = {};
            data.tpcn_mp_vtyt_paper_id = $('#in_tb_tpcn_mp_vtyt_paper_id').val();
            data.product_group_id='5';
            data.type='5';
            add_row_table_3(at_table_mp, data);
        }
        if (e.which == 27) {

        }
    }));
    //event add new a data at_table_vtyt
    $('body ').on('keyup', at_table_vtyt + ' tfoot tr th', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        console.log('enter add at_table_vtyt')
        if (e.which == 13) {
            var data = {};
            data.tpcn_mp_vtyt_paper_id = $('#in_tb_tpcn_mp_vtyt_paper_id').val();
            data.product_group_id='3';
            data.type='3';
            add_row_table_3(at_table_vtyt, data);
        }
        if (e.which == 27) {

        }
    }));

    //event double click
    //for 3 table
    var value_first_group_tpcn= '';
    var value_first_html_group_tpcn = '';
    var is_onchange_group_tpcn = false;
    $('body ').on('dblclick', temtable_tpcn_mp_vtyt + ' tbody tr td', (function (e) {
        console.log('event double click for 3 table')
        e.preventDefault();
        if ($(this).html() == '<input type="checkbox">') {
            return;
        }
        var col = $(this).parent().children().index($(this));
        var get_curent_tb_id  ='#'+$(this).closest('table').attr('id');
        var gettemplate = $(get_curent_tb_id + ' tfoot tr th').eq(col);
        if ($(gettemplate).attr('att-edit') == 'false') {
            return;
        }
        if (is_onchange_group_tpcn == true) {
            return;
        }
        var content_html = $(this).html();
        var atr= $(content_html).attr('att-value');
        
        var datacurrent;
        value_first_html_group_tpcn = content_html;
        var gettemplate = $(get_curent_tb_id + ' tfoot tr th').eq(col).html();
        var have_select = $(gettemplate).hasClass('select-at-table');
        if (atr != undefined) {
            var col = $(this).parent().children().index($(this));
            $(this).html(gettemplate);
            $(this).find('select').val(atr);
            datacurrent = atr;

        } else if(have_select==true){
            
            var col = $(this).parent().children().index($(this));
            $(this).html(gettemplate);
            //$(this).find('select').val(atr);
            datacurrent = atr;
        }else {
            datacurrent = $(this).text();
            $(this).html('<input type="text" id="onchange" value="' + datacurrent + '">');
            $("#onchange").focus();
        }
        value_first_group_tpcn= datacurrent;
        is_onchange_group_tpcn = true;
    }));

    //event save edit
    //for 3 table
    $('body ').on('keyup focusout', temtable_tpcn_mp_vtyt + ' tbody #onchange', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        console.log('#onchange keyup or focusout for 3 table')
        
        if (is_onchange_group_tpcn == false) {
            return
        }
        //alert('onchange_save')
        if (e.which == 13 || e.type == 'focusout') {
            var get_curent_tb_id  ='#'+$(this).closest('table').attr('id')
            var datacurrent = $(this).val();
            var id_datacurrent = $(this).closest('tr').attr('id_colum');
            var positionat = $(this).parent().index();
            var data_first = {};
            var data_save = {};

            var data_send1 = {};
            var colum_id1 = $(get_curent_tb_id + ' thead tr').find('th').eq(0).attr('att-name');
            // var colum_id2 = $(temtable + ' thead tr').find('th').eq(positionat).attr('att-name');
            var colum_id2 = $(get_curent_tb_id + ' thead tr').find('th').eq(positionat).attr('att-save');
            console.log(colum_id1,colum_id2)
            if (!colum_id1) {
                console.error('Not set attibute for id save data');
                colum_id1 = 0;
                return;
            }
            if (!colum_id2) {
                console.error('Not set attibute for id save data');
                colum_id1 = 1;
                return;
            }
            data_send1[colum_id1] = id_datacurrent;
            data_send1[colum_id2] = datacurrent;
            data_save = data_send1;
            console.log(data_save)
            var data_first = {};
            data_first[colum_id1] = id_datacurrent;
            data_first[colum_id2] = value_first_group_tpcn;

            var at_positioninfo = this;
            if (value_first_group_tpcn== datacurrent) {
                if (is_onchange_group_tpcn == true) {
                    if ($(value_first_html_group_tpcn).attr('att-value') != undefined) {
                        $(at_positioninfo).closest('td').html(value_first_html_group_tpcn);
                    } else {
                        $(at_positioninfo).closest('td').text(datacurrent);
                    }
                    is_onchange_group_tpcn = false;
                }
                //not change value
                return;
            }
           // var link = link_of_table + '/' + id_datacurrent;
            //console.log(data_save, link)
            //var at_table = '#id_tb_medicine_in_prescription_paper_1';
            var at_table = get_curent_tb_id;
            var link_of_table =link_base + $(at_table).attr('data-link-api');
            var link_of_table_send = link_of_table+'/'+id_datacurrent;
            var data_para = $(at_table).attr('data-para');
            
            tb_put_data_api(link_of_table_send,data_save,function(){
                if (is_onchange_group_tpcn == true) {
                    is_onchange_group_tpcn = false;
                }
                loadTableInt(at_table,link_of_table+"/?"+data_para);
                $(this).closest('td').html(datacurrent);
            })
        }
        if (e.keyCode == 27) {
            if ($(value_first_html_group_tpcn).attr('att-value') != undefined) {
                $(this).closest('td').html(value_first_html_group_tpcn);
            } else {
                $(this).closest('td').text(value_first_group_tpcn);
            }
            if (is_onchange_group_tpcn == true) {
                is_onchange_group_tpcn = false;
            }
        }
    }));

    //use function 
    //select delete click
    //event xoa



    /*
    $('body ').on('change','#group_tb_tpcn_mp_vtyt tbody input[type="checkbox"]', (function () {
        console.log('#group_tb_tpcn_mp_vtyt checkbox-show hide')
        var tem_tb = $('#group_tb_tpcn_mp_vtyt').find('table').eq(0);
        show_hide_button_table(tem_tb);
        show_hide_button_edit(tem_tb);
    }));
    */
})



function add_danhsach_tpch_mp_vtyt(link, data, callback) {
    tb_post_data_api(link, data, function (status, result) {
        callback(status, result)
    })
}