//gynecological_examination_chidinhdichvu.js

$(document).ready(function () {
    'use strict';
    
    //load list select add
    load_list_select_in_table_has_popup('#myChidinhdichvu');
    var at_cddv_group = '#id_cddv_group';
    var at_tb_cddv = '#id_tb_cddv';

    $('body ').on('click',at_cddv_group+ ' .add_show', (function () {
        $('#hidden_type_is_edit_service').val(0);
        $('#hidden_id_of_service').val('');
//        $("#myChidinhdichvu input[attr-save='patient_id']").val("");
//        $("#myChidinhdichvu input[attr-save='staff_id']").val("");
//        $("#myChidinhdichvu input[attr-save='medical_examination_form_id']").val("");
        $("#myChidinhdichvu input[attr-save='quantity']").val("");
        $("#myChidinhdichvu input[attr-save='reason']").val("");
//        $("#myChidinhdichvu #select-service-service").val("").change();
//        $("#myChidinhdichvu #select-service-location").val("").change();
        
    }));
    $('body ').on('click', at_cddv_group+' tbody tr td', (function () {
        //alert('24132')
        var colindex = $(this).parent().children().index($(this));
        if (colindex == 0)
            return;
        var id = $(this).closest('tr').attr('id_colum');
        $(this).closest('tr').find('td').eq(0).find('input').prop('checked', true);
        $(at_cddv_group+ ' .edit_select').trigger('click');
        $('#hidden_type_edit_service_id').val(id);

    }));
    $('body ').on('click',at_cddv_group+ ' .edit_select', (function () {
        var list_checkbox = $(at_cddv_group).find('tbody input[type="checkbox"]');
        var url_api = $(this).closest('.group-popup').find('table').attr('data-link-api');
        if(list_checkbox.length>1){
            var tem_id = $('#hidden_type_edit_service_id').val();
            var link_get = link_base +url_api+'/'+tem_id;
            set_input_myChidinhdichvu(link_get,tem_id)
        }else{
            $.each(list_checkbox, function () {
                var sThisVal = $(this).prop('checked');
                if (sThisVal) {
                    var tem_id = $(this).closest('tr').attr('id_colum');

                    var link_get = link_base +url_api+'/'+tem_id;
                    set_input_myChidinhdichvu(link_get,tem_id)
    //                get_data_api(link_get, {}, function (statusresult, result) {
    //                    $('#hidden_type_is_edit_service').val(1);
    //                    $('#hidden_id_of_service').val(tem_id);
    //                    
    //                    if(result.data!=null){
    //                        var tem_dt=result.data
    //                        $("#myChidinhdichvu input[attr-save='patient_id']").val(tem_dt.patient_id.id);
    //                        $("#myChidinhdichvu input[attr-save='staff_id']").val(tem_dt.staff_id.id);
    //                        $("#myChidinhdichvu input[attr-save='medical_examination_form_id']").val(tem_dt.medical_examination_form_id.id);
    //                        $("#myChidinhdichvu input[attr-save='quantity']").val(tem_dt.quantity);
    //                        $("#myChidinhdichvu input[attr-save='reason']").val(tem_dt.reason);
    //                        $("#myChidinhdichvu #select-service-service").val(tem_dt.service_id.id).change();
    //                        $("#myChidinhdichvu #select-service-location").val(tem_dt.service_location_id.id).change();
    //                    }
    //                })
                }
            });
        }
    }));
    
    function set_input_myChidinhdichvu(link_get,tem_id){
        get_data_api(link_get, {}, function (statusresult, result) {
            $('#hidden_type_is_edit_service').val(1);
            $('#hidden_id_of_service').val(tem_id);

            if(result.data!=null){
                var tem_dt=result.data
                $("#myChidinhdichvu input[attr-save='staff_id']").val(tem_dt.staff_id.id);
                $("#myChidinhdichvu input[attr-save='quantity']").val(tem_dt.quantity);
                $("#myChidinhdichvu input[attr-save='reason']").val(tem_dt.reason);
                $("#myChidinhdichvu #select-service-service").val(tem_dt.service_id.id).change();
                $("#myChidinhdichvu #select-service-location").val(tem_dt.service_location_id.id).change();
                $("#myChidinhdichvu input[attr-save='patient_id']").val((tem_dt.patient_id.id)?tem_dt.patient_id.id:'');
                $("#myChidinhdichvu input[attr-save='medical_examination_form_id']").val(tem_dt.medical_examination_form_id.id);

            }
        })
    }
    
    $('#myChidinhdichvu .save-pupop-service').click(function(e){
        e.preventDefault();
        console.log('save-pupop-service chidinhdichvu')
        
        var url_api = $(this).closest('.group-popup').find('table').attr('data-link-api');
        var param_request ='gynecolog_form_id='+curent_gynecolog_form_id +'&patient_id='+curent_patient_id;
        var data={};
        var list_add = $(this).closest('.modal-content').find('.modal-body .form-control');
        $.each(list_add,function(index,value){
            var attr =$(this).attr('attr-save');
            if(attr){
                var vl = $(this).val()
                data[attr]=vl
            }
        })
        var type_save = $('#hidden_type_is_edit_service').val();
        var id_edit = $('#hidden_id_of_service').val();
        if(type_save==1){
            url_api +='/'+id_edit
            put_data_api(link_base+url_api,data,function(status,result){
                $('#myChidinhdichvu').modal('toggle');

                var link_at = $(at_tb_cddv).attr('data-link-api');
                var data_para = $(at_tb_cddv).attr('data-para');
                loadTableInt(at_tb_cddv,link_base+link_at+"/?"+data_para);
            })
        }else{
            post_data_api(link_base+url_api,data,function(status,result){
                $('#myChidinhdichvu').modal('toggle');

                var link_at = $(at_tb_cddv).attr('data-link-api');
                var data_para = $(at_tb_cddv).attr('data-para');
                loadTableInt(at_tb_cddv,link_base+link_at+"/?"+data_para);
            })
        }
        
    })
    
    

    $('body ').on('change', at_tb_cddv+' tbody input[type="checkbox"]', (function () {
        console.log(at_tb_cddv+' checkbox-show hide')
        
        var tem_tb = $(at_cddv_group).find('table').eq(0);
        show_hide_button_table(tem_tb);
        show_hide_button_edit(tem_tb);
    }));

})

/**
 * function get option for select in popup model
 * @param {id main} at_tb_id 
 */
function load_list_select_in_table_has_popup(at_tb_id){
    var list_tb = $(at_tb_id).find('.modal-body .form-group select')
    $.each(list_tb, function (idnex, value) {
        var info_select = _get_select_info(this)
        if (info_select.api_link) {
            var link_of_select = link_base + info_select.api_link;
            get_data_api(link_of_select, {}, function (statusresult, result) {
                console.log(statusresult, result)
                var content = _build_html_select_option_name(result.data);
                $(list_tb[idnex]).html(content);
                //two_load_list_select_popup_thongtin_benhnhan(info_select, result.data);
            })
        }
    })
}

function _build_html_select_option_name(select_data) {
    var tem_ct = '';
    for (var i = 0; i < select_data.length; i++) {
        tem_ct += '<option value="' + select_data[i].id + '">' + select_data[i].name + '</option>';
    }
    return tem_ct;
}

function _get_select_info(at) {
    var info = {};
    var link_api = $(at).attr('api-link')
    if (link_api) {
        info.api_link = link_api
        var tem = info.api_link.split("/");
        info.at_select_name = tem[tem.length - 1];
    }
    return info
}