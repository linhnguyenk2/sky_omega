$(document).ready(function () {
    'use strict';

    $('body ').on('click', '#history_examination .doc-buttons .edit_select', (function () {
        var list_checkbox = $('#history_examination').find('tbody input[type="checkbox"]');
        $.each(list_checkbox, function () {
            var sThisVal = $(this).prop('checked');
            if (sThisVal) {
                $(this).closest('tr').find('td').eq(3).trigger('click')
            }
        });
    }));
    $('body ').on('change', '#history_examination tbody input[type="checkbox"]', (function () {
        var tem_tb = $('#history_examination table');
        //show_hide_button_table(tem_tb);
        show_hide_button_edit_lichsuthamkham(tem_tb);
        show_hide_button_table_delete_lichsuthamkham(tem_tb)
    }));

    $('body ').on('click', '#history_examination .xoa_select', (function (e) {
        console.log('event delete .xoa_select')
        //alert('show data for delete')
        var id_delete_show = $(this).attr('data-target'); //at id

        var list_id = [];
        var mainid = $(this).closest('.table-responsive').find('table td input'); //checkbox
        var at_table = $(this).closest('.table-responsive').find('table').eq(0).attr('id');
        //alert(at_table)
        var link_table = link_base + $(this).closest('.table-responsive').find('table').attr('data-link-api');
        mainid.each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal == 'on') {
                var tem_id = $(this).closest('tr').attr('id_colum');
                list_id.push(tem_id)
            }
        });
        var str_id = list_id.join("_")
        $('#txt-delete-text-show').attr('value', 'Bạn có muốn xóa chọn lựa?');
        $('#txt-delete-at-table').val(at_table);
        $('#txt-delete-url').val(link_table);
        $('#txt-delete-value').val(str_id);

    }));
   

})

function show_hide_button_edit_lichsuthamkham(temp_table) {
    var c_current = 0;
    var list_checkbox = $(temp_table).find('tbody input[type="checkbox"]');
    $.each(list_checkbox, function () {
        var sThisVal = $(this).prop('checked');
        if (sThisVal) {
            c_current++;
        }
    });
    if (c_current != 1) {
        $(temp_table).closest('.table-responsive').find('.doc-buttons .edit_select').addClass('disabled');
    }
    if (c_current == 1) {
        $(temp_table).closest('.table-responsive').find('.doc-buttons .edit_select').removeClass('disabled');
    }
}
function show_hide_button_table_delete_lichsuthamkham(temp_table) {
    var c_current = 0;
    var list_checkbox = $(temp_table).find('tbody input[type="checkbox"]');
    $.each(list_checkbox, function () {
        var sThisVal = $(this).prop('checked');
        if (sThisVal) {
            c_current++;
        }
    });
    if (c_current == 0) {
        $(temp_table).closest('.table-responsive').find('.doc-buttons .xoa_select').addClass('disabled');
    }
    if (c_current >= 1) {
        $(temp_table).closest('.table-responsive').find('.doc-buttons .xoa_select').removeClass('disabled');
    }
}
    