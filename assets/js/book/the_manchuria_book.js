var link_api = link_base;

var current_medical_record_id='';
var current_patient_id='';
var current_staff_id='';

$(document).ready(function () {
    'use strict';

    //thongtin_phukhoa
    //thongtin_giadinh
    $("#save_book").click(function (e) {
        e.preventDefault();
        console.log('click #save_book')
        var at = $('#thongtin_tiencan_canhan,#thongtin_tiencan_giadinh').find('.form-control');
        var data={};
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-save');
            var type = $(this).prop("tagName");
            if (attr) {
                var value_inpt = $(this).val()
                data[attr]=value_inpt
                if (type == 'SELECT') {
                }
            }
        })
        
        var linkapi =$(this).closest('section.panel-default').attr('attr-link-main');
        var at_id = $('section.panel-default').attr('at-id');
        var link = link_api+linkapi+'/'+at_id
        //var user_id_get =$(this).attr('user_id_get');
        //link = link_base+link
        put_data_api(link,data,function(statusresult, result){
            glb_show_message('save success')
        });
    });
    
    $('body ').on('click', 'table tbody tr td', (function () {
        console.log('td at 2 click goto link')
        var colindex= $(this).parent().children().index($(this));
        if(colindex==0)
                return;
        var id = $(this).closest('tr').attr('id_colum');
        var link_part = $(this).closest('table').find('thead tr').attr('link-onclick');
        if(id && link_part){
            // var link =link_api+'outpatient_book_detail/'+id;
            var link =link_api+link_part+'/'+id;
            window.location.href=link
        }
    }));
    var current_is_run_ajax=0;
    $('#add_new_gynecological_examination').click(function(){
        console.log('#add_new_gynecological_examination add new lich su tham kham')
        
        if(current_is_run_ajax==1){
            return
        }
        current_is_run_ajax=1;
        var link =$(this).attr('att-url-add');
        link =link_base +link;
        
        var data ={}
        data.medical_record_id=current_medical_record_id;
        data.patient_id=current_patient_id;
        data.staff_id=1;
        data.type='4';//phu khoa
        //data.stat='0';//phu khoa

        post_data_api(link,data,function(status,result){
            console.log(status,result);
            var at_new_id=result.data.id;
            var path='self_tracking_sheet';
            var linkgo =link_api+path+'/'+at_new_id;
            window.location.href=linkgo
        })
        
    });
    load_data()
})

function load_data(){
    var linkapi = $('section.panel-default').attr('attr-link-main');
    var at_id = $('section.panel-default').attr('at-id');
    var link = link_api+linkapi+'/'+at_id
    get_data_api(link, {}, function (statusresult, result) {
        show_data_for_all(result.data);
    })
}
function show_data_for_all(data){
   console.log('set content for text,span')
    if(data.length<1){
        alert('Not exit');
        return;
    }

    current_medical_record_id=$('#medical_examination_form').attr('attr-media-record');
    //current_patient_id=data.patient_id.id;
    current_patient_id=resolve_keys_object('patient_id.id',data);
    //current_staff_id=data.patient_id.user_id.id;
    current_staff_id=resolve_keys_object('patient_id.user_id.id',data);
    //update para link


    var data_at=data
    //show for #thongtin_so
    //#thongtin_benhnhan
    var list_info = $('#thongtin_so').find('span');
	$.each(list_info,function(index,value){
		var attr = $(this).attr('attr-show');
		var at = resolve_keys_object(attr,data_at)
		if(attr){
			$(this).html(at);
		}
    })
    var list_info1 = $('#thongtin_benhnhan').find('span');
	// $.each(list_info1,function(index,value){
    //     //alert('1')
    //     var attr = $(this).attr('attr-show');
    //     var data_user=data_at.patient_id
	// 	var at = resolve_keys_object(attr,data_user)
	// 	if(attr){
	// 		$(this).html(at);
	// 	}
    // })
    $.each(list_info1,function(index,value){
        //alert('1')
        var attr = $(this).attr('attr-show');
        var data_user=data_at.patient_id
        
        var show_static = $(this).attr('show-static');
        var at = resolve_keys_object(attr,data_user);
        if(attr){
            if(show_static){
                var text=_select_static_search_to_string(show_static,at)
               $(this).html(text);
            }else{
               $(this).html(at);
            }
       }
    })

    //var data_at
    var list_info1 = $('#thongtin_tiencan_canhan,#thongtin_tiencan_giadinh').find('.form-control');
	$.each(list_info1,function(index,value){
        //alert('1')
        var attr = $(this).attr('attr-name');
		var at = resolve_keys_object(attr,data_at)
		if(attr && at){
			$(this).val(at);
		}
    })

    load_list_table();
}



//load list sub table
function load_list_table(){
    var list_tb = $('section.panel-default').find('table')
    $.each(list_tb,function(idnex,value){
        load_1_table(this)
    })
}
function load_1_table(table){
    var table_id =  $(table).attr('id')
    if(!$(table).attr('data-link-api')){
        return;
    }
    link_of_table = link_base + $(table).attr('data-link-api')+'/?'+$(table).attr('data-para');
    loadTableInt('#'+table_id,link_of_table);
}