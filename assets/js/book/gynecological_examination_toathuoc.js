//assets\js\book\gynecological_examination_toathuoc.js

//build first test table need

//var link_base = link_base;
var tb_v_data_select_basic={};

$(document).ready(function () {
    'use strict';
    var temtable_donthuoc ='#myDanhsachthuoc table';
    //load list first select
    tb_load_select_first(temtable_donthuoc);
    /// table 1
    //start table toa thuoc
    $('body ').on('change','#group_ListDonThuoc tbody input[type="checkbox"]', (function () {
        console.log('#group_ListDonThuoc checkbox-show hide')
        var tem_tb = $('#group_ListDonThuoc').find('table').eq(0);
        show_hide_button_table(tem_tb);
        show_hide_button_edit(tem_tb);
    }));
    $('body ').on('click','#group_ListDonThuoc .edit_select', (function () {
        console.log('#group_ListDonThuoc edit')
        var list_id = [];
        var mainid =$(this).closest('.table-responsive').find('table td input'); //checkbox
        var id_code='';
        mainid.each(function (index,value) {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal =='on') {
                var tem_id = $(this).closest('tr').attr('id_colum');
                id_code = $(this).closest('tr').find('td').eq(1).text();
                list_id.push(tem_id)
            }
        });
        $('#text-ma-toathuoc').text(id_code)
        //can set cung ca variable hinde o day
        $('#in_tb_prescription_paper_id').val(list_id[0]);

        var at_table = '#id_tb_medicine_in_prescription_paper_1';
        emptyTable(at_table);
        
        //load table sub diffirent
        var link_of_table =link_base + $(at_table).attr('data-link-api');
        //alert('load tabe')
        $('#id_tb_medicine_in_prescription_paper_1').attr('data-para','sih_prescription_paper:id='+list_id[0])
        loadTableInt(at_table,link_of_table+'/?sih_prescription_paper:id='+list_id[0]);
        
    }));
    //end
    
    $('body ').on('click', '#group_ListDonThuoc tbody tr td', (function () {
        var colindex = $(this).parent().children().index($(this));
        if (colindex == 0)
            return;
        var id = $(this).closest('tr').attr('id_colum');
        $(this).closest('tr').find('td').eq(0).find('input').prop('checked', true);
        $('#group_ListDonThuoc .edit_select').trigger('click');
       // $('#hidden_type_edit_service_id').val(id);
    }));

    //check all
    // event cho table 1
    ///////////////////////////////////
    //event click check input in head --
    $('body ').on('click', temtable_donthuoc + ' thead input[type="checkbox"]', (function () {
        //set all check
        console.log('click checkbox')
        var sThisVal = $(this).prop('checked');
        $(temtable_donthuoc + ' tbody input[type="checkbox"]').each(function () {
            $(this).prop('checked', sThisVal);
        });
        show_hide_button_table(temtable_donthuoc);
    }));
    //event click check input in content
    $('body ').on('change', temtable_donthuoc + ' tbody input[type="checkbox"]', (function () {
        //just checkbox one
        show_hide_button_table(temtable_donthuoc);
    }));
    
    //event diffirent, get new a code of thuoc
    $('#add_myDanhsachthuoc').click(function(){
        console.log('click #add_myDanhsachthuoc')
        //add new
        var get_tb = $(this).closest('.table-responsive').find('table');
        var link =get_tb.attr('data-link-api');
        var data={}
        
        
        
        
//        data.patient_id = curent_patient_id;
//        data.staff_id = curent_staff_id;
//        data.medical_examination_form_id = curent_medical_examination_form_id;
//        
        data.patient_id = $('#input_hidden #id_in_patient_id').val();
        data.staff_id = $('#input_hidden #id_in_staff_id').val();
        data.medical_examination_form_id = $('#input_hidden #id_in_medical_examination_form_id').val();
        var it_me = $(this);
        add_danhsach_toathuoc(link_base+ link,data,function(status,info){
            var id_new = info.data.id;
            var id_code = info.data.code;
            $('#text-ma-toathuoc').text(id_code) //show title popup
            $('#in_tb_prescription_paper_id').val(id_new);

            var at_table = '#id_tb_medicine_in_prescription_paper_1';
            $(at_table).attr('data-para','sih_prescription_paper:id='+id_new)
            emptyTable(at_table);

            //reload table
            var link_of_table =link_base+ $(it_me).closest('.group-popup').find('table').attr('data-link-api')+'/?'+
                $(it_me).closest('.group-popup').find('table').attr('data-para');
            var table_id = $(it_me).closest('.group-popup').find('table').attr('id')
            loadTableInt('#'+table_id,link_of_table);

        });
    })

    //event add new a data
    $('body ').on('keyup', temtable_donthuoc + ' tfoot tr th', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        console.log('enter add temtable_donthuoc')
        if (e.which == 13) {
            var data={};

            data.prescription_paper_id=$('#in_tb_prescription_paper_id').val();
            add_row_table_2('#id_tb_medicine_in_prescription_paper_1',data);
        }
        if (e.which == 27) {

        }
    }));

    //event double click
    var value_first = '';
    var value_first_html = '';
    var is_onchange = false;
    $('body ').on('dblclick', temtable_donthuoc + ' tbody tr td', (function (e) {
        console.log('event double click temtable_donthuoc')
        e.preventDefault();
        if ($(this).html() == '<input type="checkbox">') {
            return;
        }
        var col = $(this).parent().children().index($(this));
        var gettemplate = $(temtable_donthuoc + ' tfoot tr th').eq(col);
        if ($(gettemplate).attr('att-edit') == 'false') {
            return;
        }
        if (is_onchange == true) {
            return;
        }
        var content_html = $(this).html();
        var atr= $(content_html).attr('att-value');
        
        var datacurrent;
        value_first_html = content_html;
        var gettemplate = $(temtable_donthuoc + ' tfoot tr th').eq(col).html();
        var have_select = $(gettemplate).hasClass('select-at-table');
        if (atr != undefined) {
            var col = $(this).parent().children().index($(this));
            $(this).html(gettemplate);
            $(this).find('select').val(atr);
            datacurrent = atr;

        } else if(have_select==true){
            
            var col = $(this).parent().children().index($(this));
            $(this).html(gettemplate);
            //$(this).find('select').val(atr);
            datacurrent = atr;
        }else {
            datacurrent = $(this).text();
            $(this).html('<input type="text" id="onchange" value="' + datacurrent + '">');
            $("#onchange").focus();
        }
        value_first = datacurrent;
        is_onchange = true;
    }));

    //event save edit
    $('body ').on('keyup focusout', temtable_donthuoc + ' tbody #onchange', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        console.log('#onchange keyup or focusout temtable_donthuoc')
        
        if (is_onchange == false) {
            return
        }
        //alert('onchange_save')
        if (e.which == 13 || e.type == 'focusout') {
            var datacurrent = $(this).val();
            var id_datacurrent = $(this).closest('tr').attr('id_colum');
            var positionat = $(this).parent().index();
            var data_first = {};
            var data_save = {};

            var data_send1 = {};
            var colum_id1 = $(temtable_donthuoc + ' thead tr').find('th').eq(0).attr('att-name');
            // var colum_id2 = $(temtable + ' thead tr').find('th').eq(positionat).attr('att-name');
            var colum_id2 = $(temtable_donthuoc + ' thead tr').find('th').eq(positionat).attr('att-save');
            console.log(colum_id1,colum_id2)
            if (!colum_id1) {
                console.error('Not set attibute for id save data');
                colum_id1 = 0;
                return;
            }
            if (!colum_id2) {
                console.error('Not set attibute for id save data');
                colum_id1 = 1;
                return;
            }
            data_send1[colum_id1] = id_datacurrent;
            data_send1[colum_id2] = datacurrent;
            data_save = data_send1;
            console.log(data_save)
            var data_first = {};
            data_first[colum_id1] = id_datacurrent;
            data_first[colum_id2] = value_first;

            var at_positioninfo = this;
            if (value_first == datacurrent) {
                if (is_onchange == true) {
                    if ($(value_first_html).attr('att-value') != undefined) {
                        $(at_positioninfo).closest('td').html(value_first_html);
                    } else {
                        $(at_positioninfo).closest('td').text(datacurrent);
                    }
                    is_onchange = false;
                }
                //not change value
                return;
            }
           // var link = link_of_table + '/' + id_datacurrent;
            //console.log(data_save, link)
            var at_table = '#id_tb_medicine_in_prescription_paper_1';
            var link_of_table =link_base + $(at_table).attr('data-link-api');
            var link_of_table_send = link_of_table+'/'+id_datacurrent;
            //alert('load tabe')
            var data_para = $(at_table).attr('data-para');
            
            tb_put_data_api(link_of_table_send,data_save,function(){
                if (is_onchange == true) {
                    is_onchange = false;
                }
                loadTableInt(at_table,link_of_table+"/?"+data_para);
                $(this).closest('td').html(datacurrent);
            })
        }
        if (e.keyCode == 27) {
            if ($(value_first_html).attr('att-value') != undefined) {
                $(this).closest('td').html(value_first_html);
            } else {
                $(this).closest('td').text(value_first);
            }
            if (is_onchange == true) {
                is_onchange = false;
            }
        }
    }));
    
    // use function_table file.js
    //function delete click
    //event xoa
})


function add_danhsach_toathuoc(link,data,callback){
    tb_post_data_api(link,data,function(status,result){
        //alert('123')
        callback(status,result)
    })
}