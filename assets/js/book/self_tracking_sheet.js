
$(document).ready(function () {
    'use strict';
    
    $("#save_book").click(function (e) {
        e.preventDefault();
        var at = $('#info-phieu-tu-theodoi').find('.form-control');
        var data={};
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-save');
            var type = $(this).prop("tagName");
            if (attr) {
                var value_inpt = $(this).val()
                var attr_format = $(this).attr('attr-fomart-save');
                if (attr_format) {
                    value_inpt = _formart_static(attr_format, value_inpt)
                } 
                data[attr]=value_inpt
                if (type == 'SELECT') {
                }
            }
        })
        console.log(data)
        // var linkapi =$(this).closest('section.panel-default').attr('attr-link-main');
        var linkapi =$(this).closest('section.panel-default').attr('attr-link-update');
        var at_id = $('section.panel-default').attr('attr-link-update-id');
        var link = link_base+linkapi+'/'+at_id
        //var user_id_get =$(this).attr('user_id_get');
        //link = link_base+link
        put_data_api(link,data,function(statusresult, result){
            console.log('save ok')
            glb_show_message('save success')
        });
    })

    load_data()

})

function load_data(){
    var linkapi = $('section.panel-default').attr('attr-link-main');
    var at_id = $('section.panel-default').attr('at-id');
    var link = link_base+linkapi+'/'+at_id
    get_data_api(link, {}, function (statusresult, result) {
        show_data_for_all(result.data);
    })
}

function show_data_for_all(data){
   
    if(data.length<1){
        alert('Not exit');
        return;
    }

    var id_of_save=data.menopause_form_id.id; //phieu
    
    curent_gynecolog_form_id =data.gynecolog_form_id.id;
    curent_patient_id =data.patient_id.id;
    curent_staff_id =data.staff_id.id;
    curent_medical_record_id =data.medical_record_id.id;
    curent_medical_examination_form_id =data.id;
    
    $('#input_hidden #id_in_patient_id').val(curent_patient_id);
    $('#input_hidden #id_in_staff_id').val(curent_staff_id);
    $('#input_hidden #id_in_medical_examination_form_id').val(curent_medical_examination_form_id);
    
    $('#in_tb_prescription_paper_id').val(curent_patient_id)

    $(".form-control[attr-save='patient_id']").val(curent_patient_id);
    $(".form-control[attr-save='staff_id']").val(data.staff_id.id);
    $(".form-control[attr-save='medical_examination_form_id']").val(data.id);
    var at_current =$(".form-control[attr-save='staff_id']");

    console.log(data.menopause_form_id.id)
    $('section.panel-default').attr('attr-link-update-id',id_of_save);


    var data_at=data
    var list_info = $('#info-phieu-tu-theodoi').find('.form-control');
	$.each(list_info,function(index,value){
        var attr = $(this).attr('attr-name');
        if(attr)
        var at = resolve_keys_object(attr,data_at)
		if(attr && at){
			var attr_format = $(this).attr('attr-fomart-show');
                        if (attr_format) {
                            at = _formart_static(attr_format, at)
                            $(this).val(at);
                        } else {
                            $(this).val(at);
                        }
		}
    })

    //load_list_table();
}