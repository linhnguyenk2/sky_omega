var link_api = link_base;

var current_medical_record_id = '';
var current_patient_id = '';
var current_staff_id = '';

$(document).ready(function () {
    'use strict';

    //thongtin_phukhoa
    //thongtin_giadinh
    $("#save_book").click(function (e) {
        e.preventDefault();
        var at = $('#thongtin_phukhoa,#thongtin_thaisan,#thongtin_khac').find('.form-control');
        var data = {};
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-save');
            var type = $(this).prop("tagName");
            if (attr) {
                var value_inpt = $(this).val()
                var attr_format = $(this).attr('attr-fomart-save');
                if (attr_format) {
                    value_inpt = _formart_static(attr_format, value_inpt)
                }
                data[attr] = value_inpt
                if (type == 'SELECT') {
                }
            }
        })
        console.log(data)
        var linkapi = $(this).closest('section.panel-default').attr('attr-link-main');
        var at_id = $('section.panel-default').attr('at-id');
        var link = link_api + linkapi + '/' + at_id
        //var user_id_get =$(this).attr('user_id_get');
        //link = link_base+link
        put_data_api(link, data, function (statusresult, result) {
            console.log('save ok')
            glb_show_message('save success')
        });
    })

    $('body ').on('click', 'table tbody tr td', (function () {
        var colindex = $(this).parent().children().index($(this));
        if (colindex == 0)
            return;
        console.log('td at 2 click goto link')
        var id = $(this).closest('tr').attr('id_colum');

        var link_part = $(this).closest('table').find('thead tr').attr('link-onclick');
        if (id && link_part) {
            // var link =link_api+'outpatient_book_detail/'+id;
            var link = link_api + link_part + '/' + id;
            window.location.href = link
        }
    }));

    var current_is_run_ajax = 0;
    $('#add_new_gynecological_examination').click(function () {
        console.log('#add_new_gynecological_examination add new lich su tham kham')

        if (current_is_run_ajax == 1) {
            return
        }
        current_is_run_ajax = 1;
        var link = $(this).attr('att-url-add');
        link = link_base + link;

        var data = {}
        data.medical_record_id = current_medical_record_id;
        data.patient_id = current_patient_id;
        data.staff_id = current_staff_id;
        data.type = '2';//phu khoa
        //data.stat='0';//phu khoa

        post_data_api(link, data, function (status, result) {
            console.log(status, result);
            var at_new_id = result.data.id;
            var path = 'pregnancy_examination';
            var linkgo = link_api + path + '/' + at_new_id;
            window.location.href = linkgo
        })

    });

    load_data()
})

function load_data() {
    var linkapi = $('section.panel-default').attr('attr-link-main');
    var at_id = $('section.panel-default').attr('at-id');
    var link = link_api + linkapi + '/' + at_id
    get_data_api(link, {}, function (statusresult, result) {
        show_data_for_all(result.data);
    })
}
function show_data_for_all(data) {

    if (data.length < 1) {
        alert('Not exit');
        return;
    }
    var data_at = data
    //show for #thongtin_so
    //#thongtin_benhnhan
    // var list_info = $('#thongtin_so').find('span');
    // $.each(list_info,function(index,value){
    // 	var attr = $(this).attr('attr-show');
    // 	var at = resolve_keys_object(attr,data_at)
    //     console.log(attr,at)
    // 	if(attr && at){
    // 		$(this).html(at);
    // 	}
    // })




    current_medical_record_id = $('#medical_examination_form').attr('attr-media-record');
    current_patient_id = data.patient_id.id;
    current_staff_id = 1;


    var list_info1 = $('#thongtin_benhnhan').find('span');
    // $.each(list_info1, function (index, value) {
    //     //alert('1')
    //     var attr = $(this).attr('attr-show');
    //     var data_user = data_at.patient_id
    //     var at = resolve_keys_object(attr, data_user)
    //     console.log(attr, at)
    //     if (attr) {
    //         $(this).html(at);
    //     }
    // })
    $.each(list_info1,function(index,value){
        //alert('1')
        var attr = $(this).attr('attr-show');
        var data_user=data_at.patient_id
        
        var show_static = $(this).attr('show-static');
        var at = resolve_keys_object(attr,data_user);
        if(attr){
            if(show_static){
                var text=_select_static_search_to_string(show_static,at)
               $(this).html(text);
            }else{
               $(this).html(at);
            }
       }
    })

    //var data_at
    var list_info1 = $('#thongtin_thaisan').find('.form-control');
    $.each(list_info1, function (index, value) {
        //alert('1')
        
        var attr = $(this).attr('attr-name');
        
        var at = resolve_keys_object(attr, data_at)
        console.log(attr, at)
        if (attr) {
            var attr_format = $(this).attr('attr-fomart-show');
            if (attr_format) {
                at = _formart_static(attr_format, at)
                $(this).val(at);
            } else {
                $(this).val(at);
            }
        }
    })
    var list_info1 = $('#thongtin_phukhoa').find('.form-control');
    $.each(list_info1, function (index, value) {
        //alert('1')
        var attr = $(this).attr('attr-name');
        var at = resolve_keys_object(attr, data_at)
        console.log(attr, at)
        if (attr) {
            $(this).val(at);
        }
    })
    var list_info1 = $('#thongtin_khac').find('.form-control');
    $.each(list_info1, function (index, value) {
        //alert('1')
        var attr = $(this).attr('attr-name');
        var at = resolve_keys_object(attr, data_at)
        console.log(attr, at)
        if (attr) {
            $(this).val(at);
        }
    })

    load_list_table();
}

//load list sub table
function load_list_table() {
    var list_tb = $('section.panel-default').find('table')
    $.each(list_tb, function (idnex, value) {
        load_1_table(this)
    })
}
function load_1_table(table) {
    var table_id = $(table).attr('id')
    if (!$(table).attr('data-link-api')) {
        return;
    }
    link_of_table = link_base + $(table).attr('data-link-api') + '/?' + $(table).attr('data-para');
    loadTableInt('#' + table_id, link_of_table);
}