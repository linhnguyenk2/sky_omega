
$(document).ready(function () {
    'use strict';
    //select delete click

    $('body ').on('click', '.xoa_select', (function (e) {
        console.log('event delete .xoa_select')
        //alert('show data for delete')
        var id_delete_show = $(this).attr('data-target'); //at id

        var list_id = [];
        var mainid = $(this).closest('.table-responsive').find('table td input'); //checkbox
        var at_table = $(this).closest('.table-responsive').find('table').eq(0).attr('id');
        //alert(at_table)
        var link_table = link_base + $(this).closest('.table-responsive').find('table').attr('data-link-api');
        mainid.each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal == 'on') {
                var tem_id = $(this).closest('tr').attr('id_colum');
                list_id.push(tem_id)
            }
        });
        var str_id = _get_parameter_array_to_str_link(list_id)
        $('#txt-delete-text-show').val('Bạn có muốn xóa các chọn lựa?');
        $('#txt-delete-at-table').val(at_table);
        $('#txt-delete-url').val(link_table);
        $('#txt-delete-value').val(str_id);

    }));
    //event xoa
    $('body ').on('click', '.xoa-event', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        console.log('.xoa-event')
        var link_table = $('#txt-delete-url').val();
        var str_id = $('#txt-delete-value').val();
        var table_id = $('#txt-delete-at-table').val();
        //alert(link_table)
        tb_delete_data_api(link_table + '/' + str_id, {}, function (status, info) {
            //alert('delete done')
            $('#myDeleteEverything').modal('hide');
            //reload table
            var link_of_table = link_base + $('#' + table_id).attr('data-link-api');
            var infor_request = $('#' + table_id).attr('data-para');
            console.log(infor_request)
            loadTableInt('#' + table_id, link_of_table + '/?' + infor_request);

        })
    }));
    //////////////////////////////////
})
//end document ready

function _get_parameter_str_to_array(str) {
    return str.split("&");
}
function _get_parameter_array_to_str(arr) {
    return arr.join("&");
}
function _get_parameter_array_to_str_link(arr) {
    return arr.join("_");
}
function _get_data_para(temp_table) {
    return $(temp_table).attr('data-para');
}
function _set_data_para(temp_table, value) {
    $(temp_table).attr('data-para', value);
}


function tb_load_select_first(temp_table) {
    $(temp_table + ' tfoot tr th').each(function (index, value) {
        var get_tite = $(this).attr('data-type-select');
        if (get_tite != undefined) {
            var info = tb_get_select_info(get_tite)
            var data = {};
            tb_get_data_api(link_base + info.api_link, data, function (status, result) {
                //alert(info.at_select_name)
                tb_v_data_select_basic[info.at_select_name] = result.data;
                tb_load_footer_th_select(info)
            })
        }
    })
}

function tb_load_footer_th_select(info) {
    var current = $('tfoot tr th[data-type-select="' + info.api_link + '"]');
    var col = $(current).parent().children().index($(current));

    var temp_table = $(current).closest('table');
    var text = tb_load_build_table(temp_table, col, info);
    $(current).html(text);
}

function tb_load_build_table(temp_table, index, info) {
    var data = tb_v_data_select_basic[info.at_select_name];
    return tb_object_toselect_option(temp_table, data, index);
}

function tb_object_toselect_option(tem_table, inobject, at) {
    var content = '';
    content += '<select class="select-at-table">';
    if (inobject) {
        for (var i = 0; i < inobject.length; i++) {
            //var show= get_attribute_tfoot_select_datashow(tem_table,at);
            var show = false;
            if (show) {
                content += '<option value="' + inobject[i].id + '">' + inobject[i][show] + '</option>';
            } else {
                content += '<option value="' + inobject[i].id + '">' + inobject[i].name + '</option>';
            }
        }
    }
    content += '</select>';
    return content;
}

function tb_get_select_info(link_tem) {
    var info = {};
    if (link_tem) {
        info.api_link = link_tem
        var tem = link_tem.split("/");
        info.at_select_name = tem[tem.length - 1];
    }
    return info;
}


//functon for api

///connect API for table
var connectApi = new APIConnect();
function tb_get_data_api(link_in, data, callback) {
    connectApi.getList(link_in, data, function (statusresult, result) {
        if (_tb_check_api_result(statusresult, result)) {
            callback(statusresult, result)
        }
    });
}
function tb_post_data_api(link_in, data, callback) {
    connectApi.addEvent(link_in, data, function (statusresult, result) {
        if (_tb_check_api_result(statusresult, result)) {
            callback(statusresult, result)
        }
    });
}
function tb_put_data_api(link_in, data, callback) {
    connectApi.updateEvent(link_in, data, function (statusresult, result) {
        if (_tb_check_api_result(statusresult, result)) {
            callback(statusresult, result)
        }
    });
}
function tb_delete_data_api(link_in, data, callback) {
    connectApi.deleteEvent(link_in, data, function (statusresult, result) {
        if (_tb_check_api_result(statusresult, result)) {
            callback(statusresult, result)
        }
    });
}
function _tb_check_api_result(statusresult, result) {
    if (statusresult == http_status.HTTP_OK) {
        if (result.status.return_code == api_status.API_STATUS_OK || result.status.return_code == api_status.API_STATUS_EMPTY) {
            return true;
        } else {
            glb_show_message(result.status.message + " : " + result.status.return_code)
            return false;
        }
    } else {
        glb_show_message(result.statusText + " : " + statusresult)
        return false;
    }
}
//end connect API


//function for page