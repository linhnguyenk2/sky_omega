//load list sub table
var link_api = link_base;
var patient_id='';

$(document).ready(function () {
    'use strict';
    
    $('#layma_bn_moi').click(function(e){
    	var link ='api/v1/patient/patients/?sih_user:code=';
    	var info = $('#layma_bn_moi_text').val();
    	link=link+info;
    	get_data_api(link, {}, function (statusresult, result) {
            show_data_in_tab_info(result.data);
        })
    })
    $('#them_so_kham').click(function(e){
    	var link_set = $(this).attr('attr_link_save');
    	
    	var link =link_api +'/'+link_set
    	if(patient_id==''){
    		return;
    	}
    	post_data_api(link, {patient_id:patient_id}, function (statusresult, result) {
            $('#myAdd').modal('toggle');//close
	        load_list_table()
	    })
    })

    //xoa event
    $('.wrapper .xoa-event').click(function(e){
        e.preventDefault();
        var link =link_api+$(this).closest('.table-responsive').find('table').attr('data-link-api')
        //var list_in = $(this).closest('.modal-content').find('.form-control')
        
        var list_id=[]
        
        var at_table_input = $(this).closest('.table-responsive').find(' tbody td input');
        $(at_table_input).each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var at_id =$(this).closest('tr').attr('id_colum');
                list_id.push(at_id);
            }
        });
        var list_text=list_id.join('_')
        var data={};
        var current = $(this)
        delete_at(data, link+'/'+list_text, function(statusresult, result){
            var id_tem = current.closest('.modal').attr('id');
            $('#'+id_tem).modal('toggle');//close
            var table_current = current.closest('.table-responsive').eq(0).find('table');
            load_1_table(table_current)
        })
        
    })
    $('body ').on('click', 'table tbody tr td:nth-child(2)', (function () {
        var id = $(this).closest('tr').attr('id_colum');
        var link =link_api+'pregnancy_checklist/'+id;
        window.location.href=link
    }));
    $('body ').on('click', 'table tbody input[type="checkbox"]', (function () {

        check_set_button_delete_sub(this);
    }));
    //click at position 2 of table go_to_link
    $('body ').on('click', '#pregnancy_checklist_list tbody tr td', (function () {
     
		
		var id = $(this).closest('tr').attr('id_colum');
		var colindex= $(this).parent().children().index($(this));
		if(colindex==0)
			return;
        var list_id = id.split("_");
        if(list_id.length==2){
            var link =link_api+'pregnancy_checklist/?book_id='+list_id[0]+'&'+'parent_id='+list_id[1];
            window.location.href=link
        }else{
            alert('Not have book_id or parent_id')
        }
    }));
    empty_temp();
})
show_hind_button_add('#them_so_kham',1)

function empty_temp(){
	var list_info = $('#thongtin_benhnhan').find('span');
	$.each(list_info,function(index,value){
		$(this).html('');
	})
}

function show_data_in_tab_info(data){
	empty_temp()
	show_hind_button_add('#them_so_kham',1)

	if(data.length<1){
		alert('Not exit');
		return
	}
	var data_at = data[0];
	patient_id = data_at.id;
	var list_info = $('#thongtin_benhnhan').find('span');
	$.each(list_info,function(index,value){

		var attr = $(this).attr('attr-show');
		var at = resolve_keys_object(attr,data_at)
		show_hind_button_add('#them_so_kham',0)
		if(attr && at){
			$(this).html(at);
		}
	})
}
function show_hind_button_add(at_id,status){
	if(status==1){
		$(at_id).addClass('disabled');
	}else{
		$(at_id).removeClass('disabled');
	}
}

load_list_table()
function load_list_table(){
    var list_tb = $('.table-responsive').find('table')
    $.each(list_tb,function(idnex,value){
        load_1_table(this)
    })
}
function load_1_table(table){
    
    var table_id =  $(table).attr('id')
    link_of_table = link_base + $(table).attr('data-link-api')+'/?'+$(table).attr('data-para');
    console.log(table_id,link_of_table)
    loadTableInt('#'+table_id,link_of_table);
}

