var _select_static={};

_select_static.stat=[
	{'id':1,'name':"Hiện"},
	{'id':0,'name':"Ẩn"},
];

//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=1303635702
//bệnh án cơ bản
_select_static.book=[
	{'id':1,'name':"Sổ khám phụ khoa",'attr_url_save':'api/v1/patient/gynecolog_book','url_get':'outpatient_book_detail'},
	{'id':2,'name':"Sổ khám thai",'attr_url_save':'api/v1/patient/prenatal_book','url_get':'pregnancy_checklist'},
	{'id':3,'name':"Sổ khám nhủ",'attr_url_save':'api/v1/patient/breast_examination_book','url_get':'book_of_examinations'},
	{'id':4,'name':"Sổ khám mãn kinh",'attr_url_save':'api/v1/patient/menopause_book','url_get':'the_manchuria_book'},
	{'id':5,'name':"Sổ khám KHHGD",'attr_url_save':'api/v1/patient/family_planing_book','url_get':'family_planing_book'},
	// {'id':6,'name':"Sổ khám sức khỏe",'attr_url_save':'api/v1/patient/family_planing_book','url_get':'family_planing_book'},
	// {'id':101,'name':"Bệnh án sản khoa",'attr_url_save':'','url_get':'outpatient_book_detail'},
	// {'id':102,'name':"Bệnh án phụ khoa",'attr_url_save':'','url_get':'outpatient_book_detail'},
	// {'id':103,'name':"Bệnh án sơ sinh",'attr_url_save':'','url_get':'outpatient_book_detail'}
];
//loại phiếu khám
//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=2098734264

_select_static.specify_service=[
    {'id':1,'name':'Vừa tạo phiếu','name_eng':'New'},
    {'id':2,'name':'Đã thanh toán','name_eng':'Charged'},
    {'id':2,'name':'BN đang thực hiện dịch vụ','name_eng':'Assign to room'},
    {'id':3,'name':'Dịch vụ đang xử lý','name_eng':'In process'},
    {'id':5,'name':'Dịch vụ hoàn thành','name_eng':'Complete'}
]

_select_static.gender=[
	{'id':0,'name':'Nữ'},
	{'id':1,'name':'Nam'},
]
_select_static.is_married=[
	{'id':0,'name':'Chưa lập gia đình'},
	{'id':1,'name':'Đã lập gia đình'},
]
_select_static.type_user=[
	{'id':1,'name':'Bệnh nhân'},
	{'id':2,'name':'Bác sĩ'},
	{'id':3,'name':'Y tá'},
]

//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=2084171283
_select_static.emergency_contact_relationship_type=[
	{'id':1,'name':'Con'},
	{'id':2,'name':'Mẹ'},
	{'id':3,'name':'Cha'},
	{'id':4,'name':'Cháu'},
	{'id':5,'name':'Ông'},
	{'id':6,'name':'Bà'},
	{'id':7,'name':'Cô'},
	{'id':8,'name':'Chú'},
	{'id':9,'name':'Bác'},
	{'id':10,'name':'Anh'},
	{'id':11,'name':'Chị'},
	{'id':12,'name':'Bạn'},
	{'id':13,'name':'Đồng nghiệp'},
	{'id':14,'name':'Khác'}
]
//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=280438526
_select_static.menstrual_status=[
	{'id':1,'name':'Đều'},
	{'id':2,'name':'Không Đều'},
];
_select_static.menstrual_flow=[
	{'id':1,'name':'Nhiều'},
	{'id':2,'name':'Trung bình'},
	{'id':3,'name':'Ít'},
]

//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=2098734264
//sih_medical_examination_form	
_select_static.sih_medical_examination_form_stat=[
	{'id':0,'name':'Disable'},
	{'id':1,'name':'Mới khởi tạo'},
	{'id':2,'name':'Đã trả kết quả'},
]
//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=1499819029
//sih_menopause_form
_select_static.sih_menopause_form=[
	{'id':0,'name':'Không'},
	{'id':1,'name':'Đôi khi'},
	{'id':2,'name':'Nhiều'}
]

//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=369054156
//sih_subclinical_paper	 loại xét nghiệm
//sih_subclinical_template	
//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=1466479494
_select_static.type_subclinical_paper=[
	{'id':1,'name':'Nội soi'},
	{'id':2,'name':'X-quang'},
	{'id':3,'name':'X-quang nhủ'},
	{'id':4,'name':'Siêu âm trắng đen'},
	{'id':5,'name':'Siêu âm 3d'},
	{'id':6,'name':'Xét nghiệm CTC'},
	{'id':7,'name':'Xét nghiệm nội tiết tế bào qua âm đạo'},
	{'id':8,'name':'Xét nghiệm chọc hút tế bào qua kim nhỏ'}
]
//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=1148408796
//sih_analysis_paper	
_select_static.type_analysis_paper=[
	{'id':1,'name':'Máu'},
	{'id':2,'name':'Nước tiểu'},
]

//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=85544700
//sih_specify_service_paper	 phiếu chỉ định dịch vụ
_select_static.price_type=[
	{'id':1,'name':'Trong giờ'},
	{'id':2,'name':'Ngoài giờ'},
	{'id':3,'name':'Lễ'},
	{'id':4,'name':'Người nước ngoài'},
]
//0:disable 1: New 2:Charged 3 :Assign to room  4: In process 5:Complete
_select_static.specify_service_paper_stat=[
	{'id':0,'name':'Disable'},
	{'id':1,'name':'New'},
	{'id':2,'name':'Charged'},
	{'id':3,'name':'Assign to room'},
	{'id':4,'name':'In process'},
	{'id':5,'name':'Complete'},
]

//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=611744281
//sih_warehouse_product_in_package_history	
_select_static.reason_type_warehouse_product_in_package_history=[
	{'id':0,'name':'khởi tạo'},
	{'id':1,'name':'Nhập kho'},
	{'id':2,'name':'Xuất kho'},
	{'id':3,'name':'Xuất bán'},
	{'id':4,'name':'Tiêu hao'},
]

//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=1864571868
//sih_warehouse_request_form	
_select_static.stat_warehouse_request_form=[
	{'id':0,'name':'Disable'},
	{'id':1,'name':'Chưa xử lý'},
	{'id':2,'name':'Đã xử lý'},
]

//https://docs.google.com/spreadsheets/d/1ZYu4QvaFYnh2Djz6cWJszRIlGVw9uaIyIsRCUXO2420/edit#gid=989761312
//sih_invoice	 

_select_static.type_invoice=[
	{'id':1,'name':'Tiền mặt'},
	{'id':2,'name':'Thẻ thanh toán'},
]
_select_static.stat_invoice=[
	{'id':0,'name':'Disable'},
	{'id':1,'name':'Tạm xuất'},
	{'id':2,'name':'Đã tính tiền'},
	{'id':3,'name':'Đã xuất hoá đơn ĐT'},
]

//_select_static_search_to_string('gender',0);
//return "<span att-value="0">Nữ</span>"

function _select_static_search_to_string(at,idvl){
	switch(at){
		case 'book':
			return _string_type_book(at,idvl);
		default:
			return _string_type_default(at,idvl);
	}
}
function _string_type_book(at,idvl){
	idvl=parseInt(idvl)
	var ob  =_select_static[at];
	var at_ob=ob.findIndex(x => x.id==idvl)
	console.log(at_ob)
	if(at_ob !=-1){
		return '<span att-value="' + ob[at_ob]['id'] + '" att-url-link="'+ob[at_ob]['url_get']+'">'+ob[at_ob]['name']+'</span>';
	}
	return ''
}
function _string_type_default(at,idvl){
	idvl=parseInt(idvl)
	var ob  =_select_static[at];
	var at_ob=ob.findIndex(x => x.id==idvl)
	console.log(at_ob)
	if(at_ob !=-1){
		return '<span att-value="' + ob[at_ob]['id'] + '">'+ob[at_ob]['name']+'</span>';
	}
	return ''
}
function _select_static_buld_select(at){
	switch(at){
		case 'book':
			return _buil_select_option_book(at);
		default:
			return _buil_select_option(at);
	}
}
function _buil_select_option(at){
	var ob =_select_static[at];
	var str='';
	for (var property1 in ob) {
		str +='<option value="'+ob[property1]['id']+'">'+ob[property1]['name']+'</option>';
	}
	return str;
}
function _buil_select_option_book(at){
	var ob =_select_static[at];
	var str='';
	for (var property1 in ob) {
		str +='<option value="'+ob[property1]['id']+'" attr-url-save="'+ob[property1]['attr_url_save']+'" attr-url-get="'+ob[property1]['url_get']+'">'+ob[property1]['name']+'</option>';
	}
	return str;
}