//var link_api = 'api/v1/list/';
var link_api = link_base;

var at_table = 'table';
var link_of_table = '';

$(document).ready(function () {
    'use strict';
    load_list_table();
    $('.thongtin_benhnhan .add_post').click(function (e) {
        $($(this).attr('data-target')).find('.form-control[attr-name-load="id"]').eq(0).val('')
    })
    $('.thongtin_benhnhan .save-event').click(function (e) {
        console.log('.thongtin_benhnhan save-event')
        e.preventDefault();
        var link = link_api + $(this).closest('.group-popup').find('.table-responsive').eq(0).find('table').attr('data-link-api')
        var list_in = $(this).closest('.modal-content').find('.form-control')
        var data = {};
        var at_id_input_hidden=$(this).closest('.modal-content').find('.form-control[attr-name-load="id"]').eq(0).val()
        $.each(list_in, function (index, value) {
            var attr = $(this).attr('attr-save');
            if (attr) {
                var vlat=$(this).val();
                if(vlat==null){vlat=""}
                data[attr] = vlat
            }
        })
        console.log(data)
        var current = $(this)
        if(at_id_input_hidden){
            put_data_api(link+'/'+at_id_input_hidden, data, function (statusresult, result) {
                var id_tem = current.closest('.modal').attr('id');
                $('#' + id_tem).modal('toggle');//close
                var table_current = current.closest('.group-popup').find('.table-responsive').eq(0).find('table');
                console.log(table_current, id_tem)
                load_1_table(table_current)
            })
        }else{
            post_data_api(link, data, function (statusresult, result) {
                var id_tem = current.closest('.modal').attr('id');
                $('#' + id_tem).modal('toggle');//close
                var table_current = current.closest('.group-popup').find('.table-responsive').eq(0).find('table');
                console.log(table_current, id_tem)
                load_1_table(table_current)
            })
        }
    })
    //xoa event
    $('.thongtin_benhnhan .xoa-event').click(function (e) {
        e.preventDefault();
        var link = link_api + $(this).closest('.group-popup').find('.table-responsive').eq(0).find('table').attr('data-link-api')
        //var list_in = $(this).closest('.modal-content').find('.form-control')

        var list_id = []

        var at_table_input = $(this).closest('.group-popup').find('.table-responsive tbody td input');
        $(at_table_input).each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var at_id = $(this).closest('tr').attr('id_colum');
                list_id.push(at_id);
            }
        });
        var list_text = list_id.join('_')
        var data = {};
        var current = $(this)
        delete_at(data, link + '/' + list_text, function (statusresult, result) {
            var id_tem = current.closest('.modal').attr('id');
            $('#' + id_tem).modal('toggle');//close
            var table_current = current.closest('.group-popup').find('.table-responsive').eq(0).find('table');
            load_1_table(table_current)
        })

    })
    $('body ').on('click', 'table tbody input[type="checkbox"]', (function () {
        check_set_button_delete_sub(this);
    }));

    $('body ').on('click', '#medical_record_noikhoa tbody tr td', (function () {
        //alert('24132')
        var colindex = $(this).parent().children().index($(this));
        if (colindex == 0)
            return;
        var id = $(this).closest('tr').attr('id_colum');
        var url_link = $(this).closest('tr').find('td').eq(2).find('span').attr('att-url-link');
        var list_id = id.split("_");
        if (list_id.length == 2) {
            var link = link_base + url_link + '/?book_id=' + list_id[0] + '&' + 'parent_id=' + list_id[1];
            window.location.href = link
        } else {
            alert('Not have book_id or parent_id')
        }

    }));
})

//load list sub table
function load_list_table() {
    var list_tb = $('.thongtin_benhnhan').find('table')
    $.each(list_tb, function (idnex, value) {
        load_1_table(this)
    })
}
function load_1_table(table) {
    var table_id = $(table).attr('id')
    link_of_table = link_base + $(table).attr('data-link-api') + '/?' + $(table).attr('data-para');
    loadTableInt('#' + table_id, link_of_table);
}

//sort

//edit