
var at_ttbn = '.thongtin_benhnhan';
var template_select_more_link = {};

jQuery(document).ready(function () {
    $(".thongtin_benhnhan form.patients").submit(function (e) {
        e.preventDefault();
        var at = $(this).find('.form-control');
        var data={};
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-value');
            var type = $(this).prop("tagName");
            if (attr) {
                var value_inpt = $(this).val()
                data[attr]=value_inpt
                if (type == 'SELECT') {
                }
            }
        })
        console.log(data)
        var link =$(this).attr('link-api');

        link = link_base+link
        update_thongtin_bn(link,data,function(statusresult, result){
            console.log('save ok')
            glb_show_message('Lưu thành công')
        });
    })
    $('a[data-toggle="tab"]').on('click', function(e) {
        var nameid= $(e.target).attr('href');
        console.log(nameid +'__active')
        switch (nameid){
            case '#menu1':
                //load_table();
                load_list_table_menu1();
                break;
            case '#menu2':
                //load_list_table_menu2();
                break;
            case '#menu3':
                //load_list_table_menu3();
                break;
            case '#menu4':
                break;
            case '#menu5':
                load_list_table_menu5();
                break;
            case '#home':
                break;
            default:
                break;
        }
    });
    //menu5
    $('#luu_so_kham_ngoai_tru').click(function(){
        var url_get_patient= $(this).attr('att-get-patient-url');
        var url_save_book= $('#type_book option:selected').attr('attr-url-save');
        var url_link_show_book= $('#type_book option:selected').attr('attr-url-get');
        
        var link1 = link_base+url_save_book;
        var data1={};
        data1.patient_id = $('#patient_id_get').val();
//            data.code = '';
//            data.gynecolog_book_id = '';
//            data.type = '';
        
        post_data_api(link1, data1,function(status1, result1){
            $('#myAdd_menu5').modal('toggle')
            //load_list_table_menu5();
            var newid=result1.data.id;
            
            var temp =url_save_book.split("/")
            var tem1=temp[temp.length-1];//gynecolog_book
            var para_table = 'sih_'+tem1+':id='+newid;
            var link_get_parent=link_base+'api/v1/patient/medical_record/?'+para_table;
            get_data_api(link_get_parent,{},function(status2, result2){
                var id_parent=result2.data[0].id;
                var link_go = link_base+url_link_show_book+'/?book_id='+newid+'&'+'parent_id='+id_parent;
                window.location.href = link_go
            })
        })
    })
    //Thong tin xuat hoa don cty
    $('body ').on('change','#patient_information_bill tbody input[type="checkbox"]', (function () {
        console.log('#patient_information_bill tbody input[type="checkbox')
        var tem_tb = $('#patient_information_bill').find('table').eq(0);
        //show_hide_button_table(tem_tb);
        show_hide_button_edit(tem_tb);
    }));
    
    $('body ').on('click','#patient_information_bill .doc-buttons .edit_select', (function (e) {
        console.log('#patient_information_bill .doc-buttons .edit_select')
        var get_id_befor_table='#'+$(this).closest('.group-popup').attr('id');
        event_after_edit_select_click(get_id_befor_table)
    }));
    
    //End Thong tin xuat hoa dong cty   
    
    $('body ').on('change','#patient_emergency_info tbody input[type="checkbox"]', (function () {
        console.log('#patient_emergency_info tbody input[type="checkbox')
        var tem_tb = $('#patient_emergency_info').find('table').eq(0);
        show_hide_button_edit(tem_tb);
    }));
    $('body ').on('click','#patient_emergency_info .doc-buttons .edit_select', (function (e) {
        console.log('#patient_emergency_info .doc-buttons .edit_select')
        var get_id_befor_table='#'+$(this).closest('.group-popup').attr('id');
        event_after_edit_select_click(get_id_befor_table)
    }));
    
    $('body ').on('keyup focusout','#text_of_id_patient', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (e.which == 13 || e.type == 'focusout') {
            //alert('get data by id pati')
            var link =link_base+'api/v1/patient/patients/?sih_user:code=';
            var info = $('#text_of_id_patient').val();
            link=link+info;
            get_data_api(link, {}, function (statusresult, result) {
                show_data_in_patient(result.data);
            })
        }
    }));
    
    $('body ').on('change','#patient_information_bill tbody input[type="checkbox"]', (function () {
        
    }))
    
    
});//end ready


load_init_thongtin_benhnhan();

function show_data_in_patient(data){
    //empty_temp()
    //show_hind_button_add('#them_so_kham',1)
    if(data.length<1){
        alert('Not exit Patient');
        return
    }
    var data_at = data[0].user_id;
    var id_patient=data[0].id;
    var list_info = $('#myThongtin_nguoilienhe').find('.form-control');
    
    $.each(list_info,function(index,value){

        var attr = $(this).attr('attr-patient-show');
        //console.log(attr,data_at)
//        var at='';
        
        //show_hind_button_add('#them_so_kham',0)
        if(attr){
            var at = resolve_keys_object(attr,data_at)
            $(this).val(at);
        }
    })
    $('#myThongtin_nguoilienhe').find('.form-control[attr-patient-show="id"]').val('');
    $('#myThongtin_nguoilienhe').find('.form-control[attr-patient-show="id"]').val(id_patient);
}

function event_after_edit_select_click(get_id_befor_table){
    var list_checkbox = $(get_id_befor_table).find('tbody input[type="checkbox"]');
    var int_current=0;
    var tem_id='';
    $.each(list_checkbox, function () {
        var sThisVal = $(this).prop('checked');
        if (sThisVal) {
            int_current++;
            tem_id=$(this).closest('tr').attr('id_colum')
        }
    })
    var url_api = $(get_id_befor_table).find('table').attr('data-link-api');
    if(int_current==1){
        var id_set_value = $(get_id_befor_table).find('.edit_select').eq(0).attr('data-target');
        var link =link_base+url_api+'/'+tem_id;
        var data={};
        get_data_api(link, data, function (statusresult, result) {
            set_value_for_input_popup(id_set_value,result.data);
        })
    }
}

function set_value_for_input_popup(at_popid,datain){
    var list_in =$(at_popid).find('.form-control');
    $.each(list_in,function(){
        var attr_show=$(this).attr('attr-name-load');
        if(attr_show){
            var vlat =resolve_keys_object(attr_show,datain);
            var tag = $(this).prop("tagName").toLowerCase();
            if (tag == 'select') {
                $(this).attr('attt-vl',vlat)
            }
            console.log(vlat)
            $(this).val(vlat);
            $(this).change();
        }
    })
}

function load_init_thongtin_benhnhan() {
    var data = {};
    //console.log($(at_ttbn));
    var link = link_base + $(at_ttbn).find('form').eq(0).attr('link-api');
    get_data_api(link, data, function (statusresult, result) {
        //console.log(result)
        set_value_thongtinbenhnha(result.data)
    })
}

function set_value_thongtinbenhnha(data) {
    var at = $(at_ttbn).find('.patients .form-control');
    $.each(at, function (index, value) {
        var attr = $(this).attr('attr-value');
        var api_link = $(this).attr('api-link');
        var type = $(this).prop("tagName");
        if (attr) {
            //alert(attr)
            var vl = data.user_id[attr];
            //console.log(vl)
            $(this).val(vl);
            if (type == 'SELECT' && api_link) {
                //console.log(vl)
                //set value
                $(this).attr('attt-vl', vl.id);
                $(this).val(vl.id)
            }
        }
    })
}
first_load_list_select_thongtin_benhnhan();
function first_load_list_select_thongtin_benhnhan() {
    var list_tb = $('.thongtin_benhnhan').find('form.patients select')
    $.each(list_tb, function (idnex, value) {
        var info_select = get_select_info(this)
        var attr_auto_load=$(this).attr('auto-load');

        if (info_select.api_link && attr_auto_load!="false") {
            var link_of_select = link_base + info_select.api_link;
            get_data_api(link_of_select, {item_in_page: 0}, function (statusresult, result) {
                //set_value_thongtinbenhnha(result.data)      
                //alert('load +'+link_of_select)
                two_load_list_select_thongtin_benhnhan(info_select, result.data);
            })
        }
        //alert(link_of_select)
        //var table_id =  $(this).attr('id')
        //loadTableInt('#'+table_id,link_of_table);
    })
    load_select_for_popup();
}

function two_load_list_select_thongtin_benhnhan(at_select, data) {
    template_select_more_link[at_select.at_select_name] = data
    //var at ='';//name of select in template_status_vi
    var str = build_html_select_option_ttbn(data)

    var tem = $('.thongtin_benhnhan').find('form.patients select[api-link="' + at_select.api_link + '"]');
    tem.html(str)
    tem.val(tem.attr('attt-vl'))
    tem.trigger('change')
    //find all popup
}
function two_load_list_select_popup_thongtin_benhnhan(at_select, data) {
    template_select_more_link[at_select.at_select_name] = data
    var str = build_html_select_option_ttbn(data)
    
    var tem = $('.thongtin_benhnhan').find('.modal select[api-link="' + at_select.api_link + '"]');
    tem.html(str)
}

function load_select_for_popup(){
    //alert('load')
    var list_tb = $('.thongtin_benhnhan').find('.modal select')
    $.each(list_tb, function (idnex, value) {
        var info_select = get_select_info(this)
        var attr_auto_load=$(this).attr('auto-load');
        //console.log(info_select)
        if (info_select.api_link && attr_auto_load!="false") {
            //alert(info_select.api_link)
            var link_of_select = link_base + info_select.api_link;
            get_data_api(link_of_select, {item_in_page: 0}, function (statusresult, result) {
                two_load_list_select_popup_thongtin_benhnhan(info_select, result.data);
            })
        }
    })
}

function build_html_select_option_ttbn(select) {
    var tem_ct = '';
    for (var i = 0; i < select.length; i++) {
        tem_ct += '<option value="' + select[i].id + '">' + select[i].name + '</option>';
    }
    return tem_ct;
}

function get_select_info(at) {
    //at =select current
    var info = {};
    //console.log(at)
    var link_api = $(at).attr('api-link')
    if (link_api) {
        info.api_link = link_api
        ///alert(info.api_link)
        var tem = info.api_link.split("/");
        info.at_select_name = tem[tem.length - 1];
    }
    return info
}

function update_thongtin_bn(link,data,callback){
    //var link = link_run + '/' + list_string;
    //var data = {};
    //data.stat = status;
    update_at(data, link, function (statusresult, result) {
        callback(statusresult, result)
    })
}