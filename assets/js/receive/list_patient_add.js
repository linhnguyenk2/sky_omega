var link_api = link_base;
var link_of_table = '';
var template_select_more_link={};
var id_of_patient='';

var at_main = '.themmoi_benhnhan';

$(document).ready(function () {
    'use strict';
//    $('.thongtin_benhnhan .save-event').click(function(e){
//        e.preventDefault();
//    })
    emty_all_input();
    load_select_for_popup();
    
    $('#layma_bn_moi').click(function(e){
        e.preventDefault();
        get_mbn(function(result){
            var id_code = result.data.user_id.code;
            id_of_patient = result.data.id
            $('#layma_bn_moi_text').val(id_code)
            $('#patient_emergency_contact').attr('data-para','sih_patients:id='+id_of_patient);
            $('input[attr-save="patient_id"]').val(id_of_patient);
        })
    })
    
    $(at_main +' #tao-benh-nhan').click(function (e) {
        e.preventDefault();
        //alert('123123')
        var at = $('.add-thongtin-benhnhan').find('.form-control');
        var data={};
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-value');
            var type = $(this).prop("tagName");
            if (attr) {
                var value_inpt = $(this).val()
                data[attr]=value_inpt
                if (type == 'SELECT') {
                }
            }
        })
        console.log(data)
        var link ='api/v1/patient/patients';
        link = link_base+link+'/'+id_of_patient
        put_data_api(link,data,function(statusresult, result){
            alert('Tạo mới thành công')
        });
    })
    
    //event for popup
    $('.save-event').click(function(e){
        e.preventDefault();
        //console.log($(this).closest('.group-popup').eq(0).closest('div'))
        var link =link_api+$(this).closest('.group-popup').find('.table-responsive').eq(0).find('table').attr('data-link-api')
        var list_in = $(this).closest('.modal-content').find('.form-control')
        var data={};
        //alert(link);
        $.each(list_in,function(index,value){
            ///alert('havedata')
            var attr = $(this).attr('attr-save');
            if(attr){
                data[attr] = $(this).val()
            }
        })
       var current = $(this)
        save_add_data(link,data,function(statusresult, result){
            var id_tem = current.closest('.modal').attr('id');
            $('#'+id_tem).modal('toggle');//close
            var table_current = current.closest('.group-popup').find('.table-responsive').eq(0).find('table');
            console.log(table_current,id_tem)
            load_1_table(table_current)
        })
    })
    //xoa event
    $('.xoa-event').click(function(e){
        e.preventDefault();
        var link =link_api+$(this).closest('.group-popup').find('.table-responsive').eq(0).find('table').attr('data-link-api')
        //var list_in = $(this).closest('.modal-content').find('.form-control')
        
        var list_id=[]
        
        var at_table_input = $(this).closest('.group-popup').find('.table-responsive tbody td input');
        $(at_table_input).each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var at_id =$(this).closest('tr').attr('id_colum');
                list_id.push(at_id);
            }
        });
        var list_text=list_id.join('_')
        var data={};
        var current = $(this)
        delete_at(data, link+'/'+list_text, function(statusresult, result){
            var id_tem = current.closest('.modal').attr('id');
            $('#'+id_tem).modal('toggle');//close
            var table_current = current.closest('.group-popup').find('.table-responsive').eq(0).find('table');
            load_1_table(table_current)
        })
        
    })
    $('body ').on('click', 'table tbody input[type="checkbox"]', (function () {
        check_set_button_delete_sub(this);
    }));
})

function get_mbn(callback){
    var link =link_api +'api/v1/patient/patients';
    post_data_api(link, {stat:0}, function (statusresult, result) {
        callback(result)
    })
}


function load_select_for_popup(){
    //alert('load')
    var list_tb = $(at_main).find('.modal select')
    $.each(list_tb, function (idnex, value) {
        var info_select = get_select_info(this)
        //console.log(info_select)
        if (info_select.api_link) {
            //alert(info_select.api_link)
            var link_of_select = link_base + info_select.api_link;
            get_data_api(link_of_select, {item_in_page: 0}, function (statusresult, result) {
                two_load_list_select_popup_thongtin_benhnhan(info_select, result.data);
            })
        }
    })
}

function two_load_list_select_popup_thongtin_benhnhan(at_select, data) {
    template_select_more_link[at_select.at_select_name] = data
    var str = build_html_select_option_name(data)
    
    var tem = $('.themmoi_benhnhan').find('select[api-link="' + at_select.api_link + '"]');
    tem.html(str)
}


//load list sub table
function load_list_table(){
    var list_tb = $('.themmoi_benhnhan').find('table')
    $.each(list_tb,function(idnex,value){
        load_1_table(this)
    })
}
function load_1_table(table){
    var table_id =  $(table).attr('id')
    link_of_table = link_base + $(table).attr('data-link-api')+'/?'+$(table).attr('data-para');
    loadTableInt('#'+table_id,link_of_table);
}