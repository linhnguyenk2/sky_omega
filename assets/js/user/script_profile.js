$(document).ready(function () {
    $('body').on('click', '#avata_user', function (e) {
        e.preventDefault();
       // $('#uploadFile').trigger();
       $("#uploadFile").focus().trigger("click");
        /*
        var data = {
            email: $("#old_email").val(),
            password: $("#old_password").val()
        };

        if ((data['email'].length > 4) && (data['password'].length > 3)) {
            $.ajax({
                type: 'POST',
                url: './index.php/user/signIn',
                data: data,
                success: function (res) {
                    var data = JSON.parse(res);
                    if (data.ok != '0') {
                        $('#ajaxModal').modal('hide')
                        //window.location.reload();
                    } else {
                        //$('#old_password').prop('title', 'Wrong Some Thing');
                        $('#old_password').tooltip({ 'trigger': 'focus', 'title': 'Wrong Password' });
                        $('#old_password').trigger('focus');

                    }
                }
            });
        } else {
            $('#old_password').tooltip({ 'trigger': 'focus', 'title': 'Mật khẩu dài hơn 3 ký tự' });
            $('#old_password').trigger('focus');
        }
        */
    });
    $('#uploadFile').bind('change', function() {

        //because this is single file upload I use only first index
        var f = this.files[0]
        console.log(f);
        var ext = f.name.split('.').pop().toLowerCase();
        if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
            alert('invalid extension!');
            return;
        }

        //here I CHECK if the FILE SIZE is bigger than 8 MB (numbers below are in bytes)
        if (f.size > 1388608 || f.fileSize > 1388608)
        {
           //show an alert to the user
           alert("Allowed file size exceeded. (Max. 1 MB)")

           //reset file upload control
           this.value = null;
        }else{
            $('#form_image').trigger("submit");
            //alert('save image')
        }
      
      });
      $("#form_image").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var form_data = new FormData(this); //Creates new FormData object
        $.ajax({
            url : post_url,
            type: request_method,
            data : form_data,
            contentType: false,
            cache: false,
            processData:false
        }).done(function(response){ //
            //console.log(response);
            var data = JSON.parse(response);
            console.log(data)
            if(data.status==true){
                $('#avata_user').attr("src",data.link.trim());
            }else{
                alert(data.msg);
            }
            //$("#server-results").html(response);
        });
    });
})