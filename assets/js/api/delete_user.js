
var link_api = 'api/v1/list/';

var at_table = '#delete_user';
var link_of_table = '';

$(document).ready(function () {
    'use strict';
    link_of_table =link_base + $(at_table).attr('data-link-api');

    loadTableInt(at_table,link_of_table);
    load_place_holder(at_table);
    $('body ').on('click', at_table + ' thead input[type="checkbox"]', (function () {
        var sThisVal = $(this).prop('checked');
        $(at_table + ' tbody input[type="checkbox"]').each(function () {
            $(this).prop('checked', sThisVal);
        });
        show_hide_button(at_table);
    }));

    $('body ').on('change', at_table + ' tbody input[type="checkbox"]', (function () {
        show_hide_button(at_table);
    }));

    //--------------Delete

    $('body ').on('click', '#category_name #delete .modal-footer button[data-dismiss!="modal"]', (function () {
        var list_in = $('#delete .modal-content input').val();
        list_in = list_in.split(",");

        delete_more(link_of_table,list_in, function () {
            loadTableInt(at_table,link_of_table)
            $('#myDelete').modal('toggle');
            show_hide_button(0);
        })

    }));
   


    $('body ').on('click', '#category_name [data-target="#myDelete"]', (function () {
        var list_id = [];
        var list_all = [];
        var list_name = [];
        $(at_table + ' td input').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var list_id = $(this).closest('tr').attr('id_colum');
                list_all.push(list_id);
                var showat = 2;
                if ($(this).closest('table').find('thead tr').attr('show-position')) {
                    showat = $(this).closest('table').find('thead tr').attr('show-position');
                }
                list_name.push($(this).closest('tr').find('td').eq(showat).text());
            }
        });
        if (list_all[0]) {
            var more_one = '.one_show';
            if (list_all.length > 1) {
                more_one = '.more_show';
            }
            get_allinptu_delete(list_all.join(), more_one, list_name[0]);
        }

    }));


    //--------------End Delete

    //--------Search keyword

    var searchkey = $(at_table).closest('div').find('.row').eq(0).find('.dataTables_filter input');
    $('body ').on('keyup', searchkey, (function (e) {
        if (e.which == 13) {
            loadTableInt(at_table,link_of_table);
        }
    }));

    //--------End Search

    //-------Change Page
    $('body ').on('change', '.select-page-option', (function (e) {
        loadTableInt(at_table,link_of_table);
    }));

    //-------End Change Page

    $('body ').on('click', '#category_name [data-target="#myPush"]', (function () {
        var list_id = [];
        var list_all = [];
        var list_name = [];
        $(at_table + ' td input').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var list_id = $(this).closest('tr').attr('id_colum');
                list_all.push(list_id);
                var showat = 2;
                if ($(this).closest('table').find('thead tr').attr('show-position')) {
                    showat = $(this).closest('table').find('thead tr').attr('show-position');
                }
                list_name.push($(this).closest('tr').find('td').eq(showat).text());
            }
        });

        if (list_all[0]) {
            var more_one = '.one_show';
            if (list_all.length > 1) {
                more_one = '.more_show';
            }
            get_allinptu_push(list_all.join(), more_one, list_name[0]);
        }

    }));
    $('body ').on('click', '#category_name [data-target="#myUnpush"]', (function () {
        var list_id = [];
        var list_all = [];
        var list_name = [];
        $(at_table + ' td input').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var list_id = $(this).closest('tr').attr('id_colum');
                list_all.push(list_id);
                var showat = 2;
                if ($(this).closest('table').find('thead tr').attr('show-position')) {
                    showat = $(this).closest('table').find('thead tr').attr('show-position');
                }
                list_name.push($(this).closest('tr').find('td').eq(showat).text());
            }
        });

        if (list_all[0]) {
            var more_one = '.one_show';
            if (list_all.length > 1) {
                more_one = '.more_show';
            }
            get_allinptu_unpush(list_all.join(), more_one, list_name[0]);
        }

    }));
    
    $('body ').on('click', '#category_name #push .modal-footer button[data-dismiss!="modal"]', (function () {
        var list_in = $('#push .modal-content input').val();
        list_in = list_in.split(",");
        update_more_status(link_of_table,list_in, '1', function () {
            loadTableInt(at_table,link_of_table)
            $('#myPush').modal('toggle');
            show_hide_button(0);
        })
    }));
    $('body ').on('click', '#category_name #unpush .modal-footer button[data-dismiss!="modal"]', (function () {
        var list_in = $('#unpush .modal-content input').val();
        list_in = list_in.split(",");

        update_more_status(link_of_table,list_in, '0', function () {
            loadTableInt(at_table,link_of_table)
            $('#myUnpush').modal('toggle');
            show_hide_button(0);
        })

    }));



    //-----End edit

    //----Sort

    $('body ').on('click', at_table + ' thead tr th', (function () {
        if ($(this).hasClass('sorting_disabled')) {
            return;
        }
        var sortat = $(this).attr('data-sort');
        if (sortat != 'desc') {
            sortat = 'asc';
        } else {
            sortat = 'desc';
        }
        remove_all_sort(at_table);
        $(this).addClass('sortat');
        $(this).attr('data-sort', sortat);
        change_sort_direction(at_table);
        loadTableInt(at_table,link_of_table);

    }));
    
    //-----end sort
    //--------------Open modal
    $('#btn_add').click(function(){
        $("#myModalLabel").html("Thêm mới");
        $("#editMenupermission").html("Thêm menu bị khóa người dùng");
        $('#editMenupermission').attr('id', 'addMenupermission');
     });

    $('#btn_edit').click(function(){
        $("#myModalLabel").html("Chỉnh sửa");
        
        $('#addMenupermission').attr('id', 'editMenupermission');
        $("#editMenupermission").html("Lưu");

        var at = $('.popupdeleteuser').find('.cdeleteuser');
        var data={};
        var link ='api/v1/permission/menu_permission/'
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-name');
            if (attr) {
                var value_inpt = $(this).val()
                data[attr]=value_inpt
            }
        })
        var id_col
        $(at_table + ' tbody input[type="checkbox"]').each(function () {
        var sThisVal = $(this).prop('checked');
        if (sThisVal) {
            id_col =$(this).closest('tr').attr('id_colum')
        }
        });
        link = link_base+link+'/'+id_col
        
        get_data_api(link,data,function (statusresult, result){
            $(".popupdeleteuser #type_user").val(result.data.type_user);
            $(".popupdeleteuser #html_id").val(result.data.html_id);
        });
     });

    //--------------ADD
        $('body').on('click',' #addMenupermission', (function () {
        var at = $('.popupdeleteuser').find('.cdeleteuser');
        var data={};
        var link ='api/v1/permission/menu_permission/'
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-name');
            if (attr) {
                var value_inpt = $(this).val()
                data[attr]=value_inpt
            }
        })
        link = link_base+link
        post_data_api(link,data,function(statusresult, result){
           loadTableInt(at_table,link_of_table)
           $('#openModal').modal('toggle');
           glb_show_message('Thêm thành công')
        });
    }));
    //--------------END ADD
    //--------------EDIT
    $('body').on('click',' #editMenupermission', (function () {
        var at = $('.popupdeleteuser').find('.cdeleteuser');
        var data={};
        var link ='api/v1/permission/menu_permission/'
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-name');
            if (attr) {
                var value_inpt = $(this).val()
                data[attr]=value_inpt
            }
        })
        var id_col
        $(at_table + ' tbody input[type="checkbox"]').each(function () {
        var sThisVal = $(this).prop('checked');
        if (sThisVal) {
            id_col =$(this).closest('tr').attr('id_colum')
        }
        });
        data.id = id_col
        link = link_base+link+'/'+id_col
        put_data_api(link,data,function(statusresult, result){
           loadTableInt(at_table,link_of_table)
           show_hide_button(0);
           $('#openModal').modal('toggle');
           glb_show_message('Cập nhật thành công')
        });
    }));
})

function two_load_list_select_popup_delete_user(at_select, data,attr,value_col) {

    var tem_ct = '';
    value_col = value_col || '';
    var val_option 
    for (var i = 0; i < data.length; i++) {
        if(value_col!='')
        {
            if(data[i][attr] ==1)
            {
                val_option = "Bệnh nhân"
            }
            else if(data[i][attr] ==2)
            {
                val_option = "Bác sĩ"
            }
            else if(data[i][attr] ==3)
            {
                val_option = "Y tá"
            }
            else
                val_option = data[i][value_col]
            tem_ct += '<option value="' + data[i][attr] + '">' + val_option + '</option>';
        }
        else{
           tem_ct += '<option value="' + data[i].id + '">' + data[i][attr] + '</option>';

        }
    }

    var tem = $('.permission_delete_user').find('select[name="' + at_select.attr_name + '"]');
    tem.html(tem_ct)
}
load_select_for_popup();
function load_select_for_popup(){
    var list_tb = $('.permission_delete_user').find('select')
    $.each(list_tb, function (idnex, value) {
        var info_select = get_select_info(this)
        var attr = $(this).attr('attr-value')
        var name = $(this).attr('attr-name')
        if (info_select.api_link) {
        var link_of_select = link_base + info_select.api_link;
        if(attr != 'id')
        {

           get_data_api(link_of_select, {item_in_page: 0}, function (statusresult, result) {
                two_load_list_select_popup_delete_user(info_select, result.data,name,attr);
            });
        }
        else
        {
            get_data_api(link_of_select, {item_in_page: 0}, function (statusresult, result) {
                two_load_list_select_popup_delete_user(info_select, result.data,'html_id');
            });
        }
        }
        //console.log(info_select)
        
    });
}
function get_select_info(at) {
    //at =select current
    var info = {};
    //console.log(at)
    var link_api = $(at).attr('api-link')

    if (link_api) {
        info.attr_name = $(at).attr('name')
        info.api_link = link_api
        ///alert(info.api_link)
        var tem = info.api_link.split("/");
        info.at_select_name = tem[tem.length - 1];
    }
    return info
}
