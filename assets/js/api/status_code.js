/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var api_status={};

api_status.API_STATUS_OK="10000";
api_status.API_STATUS_EMPTY="11000";
api_status.API_STATUS_AUTH_FAIL="12000";
api_status.API_STATUS_UPDATE_FAIL="12004";
api_status.API_STATUS_DELETE_FAIL="12003";

var http_status = {}

http_status.HTTP_CONTINUE="100";
http_status.HTTP_SWITCHING_PROTOCOLS="101";
http_status.HTTP_OK="200";
http_status.HTTP_CREATED="201";
http_status.HTTP_ACCEPTED="202";
http_status.HTTP_NONAUTHORITATIVE_INFORMATION="203";
http_status.HTTP_NO_CONTENT="204";
http_status.HTTP_RESET_CONTENT="205";
http_status.HTTP_PARTIAL_CONTENT="206";
http_status.HTTP_MULTIPLE_CHOICES="300";
http_status.HTTP_MOVED_PERMANENTLY="301";

http_status.HTTP_FOUND="302";
http_status.HTTP_SEE_OTHER="303";
http_status.HTTP_NOT_MODIFIED="304";
http_status.HTTP_USE_PROXY="305";

http_status.HTTP_UNUSED="306";
http_status.HTTP_TEMPORARY_REDIRECT="307";
http_status.errorCodesBeginAt="400";
http_status.HTTP_BAD_REQUEST="400";
http_status.HTTP_UNAUTHORIZED="401";
http_status.HTTP_PAYMENT_REQUIRED="402";

http_status.HTTP_FORBIDDEN="403";
http_status.HTTP_NOT_FOUND="404";
http_status.HTTP_METHOD_NOT_ALLOWED="405";
http_status.HTTP_NOT_ACCEPTABLE="402";




http_status.HTTP_PROXY_AUTHENTICATION_REQUIRED="407";
http_status.HTTP_REQUEST_TIMEOUT="408";
http_status.HTTP_CONFLICT="409";
http_status.HTTP_GONE="410";
http_status.HTTP_LENGTH_REQUIRED="411";

http_status.HTTP_PRECONDITION_FAILED="412";
http_status.HTTP_REQUEST_ENTITY_TOO_LARGE="413";
http_status.HTTP_REQUEST_URI_TOO_LONG="414";
http_status.HTTP_UNSUPPORTED_MEDIA_TYPE="415";
http_status.HTTP_REQUESTED_RANGE_NOT_SATISFIABLE="416";

http_status.HTTP_EXPECTATION_FAILED="417";
http_status.HTTP_INTERNAL_SERVER_ERROR="500";
http_status.HTTP_NOT_IMPLEMENTED="501";
http_status.HTTP_BAD_GATEWAY="502";
http_status.HTTP_SERVICE_UNAVAILABLE="503";
http_status.HTTP_GATEWAY_TIMEOUT="504";
http_status.HTTP_VERSION_NOT_SUPPORTED="505";
