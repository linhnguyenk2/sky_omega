$(function () {
    var link_main = $(at_table).attr('data-link-api');
    //data-type-select="package_healthcare_service_type"
    $('tr th[data-type-select="package_healthcare_service_type"]').ready(function () {
    });

    $('body ').on('click', '#category_name [data-target="#myAdd"]', (function () {
        load_select_change_for_select()
        
    }))
    $('body ').on('click', '#category_name [data-target="#myEdit"]', (function () {
        load_select_change_for_select()
        var id_edit
        $('#category_name [data-example="example_more"] td input').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var findlisttd = $(this).closest('tr').find('td');
                var leng = $(findlisttd).length;
                var list = [];
                var at = $(this).closest('tr').attr('id_colum');
                id_edit=at
                list.push(at);//push id first
                $(findlisttd).each(function (index) {
                    list.push($(this).html());
                });
                get_allinptu_edit(list);
                return;
            }
        });
        var temid=id_edit;
        load_all_old_sub(temid);
    }))
    $('body ').on('focusout change', '.select-type-head', (function (e) {
        var vl = $(this).val();
        $(this).closest('div').find('input').val(vl);
    }))
    $('body ').on('click', '#category_name #add .modal-footer button[data-dismiss!="modal"]', (function () {
        var list_in = {};
        $('#add .modal-content input').each(function (index) {
            if ($(this).hasClass('input-have-select')) {
                $(this).val($(this).closest('div').find('select').val());
            }
            var at = $(this).val();
            if ($(this).attr('type') == 'checkbox') {
                at = $(this).eq(0).attr("checked") ? 1 : 0;
            }
            var attr_col = $(this).attr('sh-cl');
            if (attr_col) {
                list_in[attr_col] = at;
            }
        });
        
        
        var info_data = get_list_data_intable(this);
        for(var pro in info_data){
            if(!jQuery.isEmptyObject(info_data[pro]))
                list_in[pro]=JSON.stringify(info_data[pro])
        }
        save_add_data(link_main, list_in, function (statusresult, result) {
            alert('save ok')
            show_api_if_error(statusresult, result)
        })

    }))
    $('body ').on('click', '#category_name #edit .modal-footer button[data-dismiss!="modal"]', (function () {
        var list_in = {};
        $('#edit .modal-content input').each(function (index) {
            if (index == 0) {
                var at = $(this).val();
                var at = $(this).closest('tr').attr('id_colum');
            }

            if ($(this).attr('type') == 'checkbox') {

                var at = $(this).is(":checked") ? '1' : '0'
            } else {
                var at = $(this).val();
            }
            var attr_col = $(this).attr('sh-cl');
            if (attr_col) {
                list_in[attr_col] = at;
            }
        });
        
        var info_data = get_list_data_intable(this);
        for(var pro in info_data){
            if(!jQuery.isEmptyObject(info_data[pro]))
                list_in[pro]=JSON.stringify(info_data[pro])
        }
        
        update_at(list_in, link_main + '/' + list_in.id, function (statusresult, result) {
            alert('save ok')
            show_api_if_error(statusresult, result)
        })

    }));

    $('body ').on('keyup', '#add .table-responsive table tfoot tr th', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        var at_select='';
        
        var index_select = find_index_select_in_table($(this).closest('table'))
        
        if (e.which == 13) {
            var info = get_sub_element_get_info(this);
            at_select=info.at_select_name;
            var value_ft = get_value_footer(this)
            //value at select =''
            if(!value_ft[index_select]){
                return;
            }
            var str_row='';
            str_row+='<tr type_colum="add">';
            for(var pro in value_ft){
                str_row+='<td>'+value_ft[pro]+'</td>';
            }
            str_row+='</tr>';
            $(info.table).find('tbody').append(str_row)
        }
        
        var list_id_select = get_list_select_value_old($(this).closest('table'),index_select)
        update_select_show_next($(this).closest('table'),at_select,list_id_select);
        
    }));
    $('body ').on('keyup', '#edit .table-responsive table tfoot tr th', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        //enter 
        var index_select = find_index_select_in_table($(this).closest('table'))
        var at_select='';
        if (e.which == 13) {
            var info = get_sub_element_get_info(this);
            at_select=info.at_select_name;
            var value_ft = get_value_footer(this)
            if(!value_ft[index_select]){
                return;
            }
            var str_row='';
            str_row+='<tr type_colum="add">';
            for(var pro in value_ft){
                str_row+='<td>'+value_ft[pro]+'</td>';
            }
            str_row+='</tr>';
            $(info.table).find('tbody').append(str_row)
        }
        var list_id_select = get_list_select_value_old($(this).closest('table'),index_select)
        update_select_show_next($(this).closest('table'),at_select,list_id_select);
    }));    
    $('body ').on('click', '.grouplistsub tbody input[type="checkbox"]', (function () {
        check_set_button_delete_sub(this);
    }));
    $('body ').on('click', '.grouplistsub .xoa-element', (function () {
        var at_table_input = $(this).closest('.grouplistsub').find('tbody td input');
        $(at_table_input).each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var type_ele = $(this).closest('tr').attr('type_colum');
                if(type_ele=='add'){
                    $(this).closest('tr').remove();
                }
                if(type_ele=='edit'){
                    $(this).closest('tr').addClass('display_none');
                }
            }
        });
        $(this).closest('.dataTables_wrapper').find('.doc-buttons .xoa-element').addClass('disabled');
        var at_tem=$(this).closest('.grouplistsub').find('table')
        var index_select = find_index_select_in_table(at_tem)
        console.log(index_select)
        var list_id_select = get_list_select_value_old(at_tem,index_select)
        var at_select = $(at_tem).find('tfoot .data-type-select').attr('at-select')
        update_select_show_next(at_tem,at_select,list_id_select);
    }));
    var is_onchange=false;
    $('body ').on('dblclick', '.grouplistsub table tbody tr td', (function (e) {
        e.preventDefault();
        var info = get_sub_element_get_info(this);
        //at_select=info.at_select_name;
        var value_ft = get_value_footer(this)
        var at = $(this).parent().children().index($(this));
        //alert(at)
        var info_col_head = get_attr_tb_header_sub($(this).closest('table'),at);
        if(is_onchange==true){
            return;
        }
        if(info_col_head.list_vl=='quantity'){
            //alert('123');
            is_onchange=true
            var datacurrent = $(this).text();
            $(this).html('<input type="text" id="onchange" value="'+datacurrent+'">');
            $( "#onchange" ).focus();
        }
        
        
        //console.log(info,value_ft,at,info_col_head)
    }));
    $('body ').on('keyup focusout', '.grouplistsub table tbody tr #onchange', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        var datacurrent= $(this).val();
        if(e.which == 13 || e.type=='focusout') {
            $(this).closest('td').text(datacurrent);
            is_onchange=false
        }
    }));
})

function find_index_select_in_table(at){
    console.log(at)
    var position_select=$(at).find('tfoot select').eq(0);
    var td_select=$(position_select).closest('th');
    return $(td_select).parent().children().index($(td_select));
}
function get_list_select_value_old(at,index_select){
    var list_tr = $(at).find('tbody tr');
    var list_selected=[];
    $.each(list_tr,function(index){
        var list_td =$(list_tr[index]).find('td');
        var tem=$(list_td[index_select]).find('span').attr('att-value')
        list_selected.push(tem)
    })
    return list_selected;
}
function update_select_show_next(at_table,class_select,arrselectd){
    console.log(class_select,arrselectd)
    if(!class_select)return
    var select =  $(at_table).find('.' +class_select+ ' option');
    $(select).removeAttr('disabled')
    $.each(select,function(index,value){
        var vl = $(this).val();
        var text = $(this).text();
        
        if(arrselectd.indexOf(vl)>-1){
            //alert(arrselectd.indexOf(vl))
            $(select[index]).attr('disabled','disabled')
        }
        //console.log(vl,text);
    })
//    var str = '';
//    str = '<select class="form-control">';
//    for (var i = 0; i < data.length; i++) {
//        str += '<option value="' + data[i]['id'] + '">' + data[i]['name'] + '</option>'
//    }
//    str += '</select>';
//    return str
}

function get_list_data_intable(at){
    var at_result = {}
    var list = $(at).closest('.modal-content').find('.grouplistsub');
    $(list).each(function (index,value) {
        var tbat = $(this).find('tbody');
        var info = get_sub_element_get_info(tbat);
        var data =get_all_data_of_table(tbat)
        at_result[info.at_ob_save]=data
    })
    return at_result;
}


function load_all_old_sub(temid){
    if(!temid)return
    get_old_api_data(temid)
}



function get_all_data_of_table(attable){
    var heretb=$(attable).closest('table')
    var list_cl=get_attr_save_sub_table(heretb)
    var listtr = $(attable).find('tr');
    var data_sub_body=[];
    $.each(listtr,function(index_1,value_1){
        
        var at_td = $(value_1).find('td');
        data_sub_body.push({})
        var list_tr_at=$(listtr[index_1])
        if(list_tr_at.attr('type_colum')=='edit'){
            data_sub_body[index_1]['action']='update'
        }
        if(list_tr_at.hasClass('display_none')){
            data_sub_body[index_1]['action']='delete'
        }
        
        $.each(at_td,function(index_2,value_2){
            var ct_html=$(value_2).html();
            var at_text=ct_html;
            var atr_htmlselect=$(ct_html).attr('att-value')
            if(atr_htmlselect){
                at_text=atr_htmlselect
            }
            if(list_cl[index_2]){
                data_sub_body[index_1][list_cl[index_2]]=at_text;
            }
        })
    })
    return data_sub_body;
}

function get_attr_save_sub_table(attable){
    var list = $(attable).find('thead th');
    var result={};
    var tem;
    $.each(list,function(index,value){
        tem='';
        var attr = $(value).attr('att-save');
        if(attr){
            tem=attr
        }
        result[index]=tem
    })
    return result;
}

function check_set_button_delete_sub(temp_at){
    var temp_table = $(temp_at).closest('table')
    var temp_at = $(temp_table).find('tbody input[type="checkbox"]')
    var c_current = 0;
    $.each(temp_at,function (index,value) {
        var sThisVal = $(this).prop('checked');
        if (sThisVal) {
            c_current++;
        }
    });
    if (c_current >= 1) {
        $(temp_at).closest('.dataTables_wrapper').find('.doc-buttons .xoa-element').removeClass('disabled');
    }
    if (c_current == 0) {
        $(temp_at).closest('.dataTables_wrapper').find('.doc-buttons .xoa-element').addClass('disabled');
    }
}

function get_old_api_data(temid){
    $('#edit .grouplistsub').each(function () {
        var tbat = $(this).find('table');
        var tbattr=tbat.attr('data-link-api')+'?package_healthcare_service_id='+temid
        var data = {};
        get_list_sub_load(tbat,tbattr, data, function (tbat_last,statusresult, result) {
            var tem = tbattr.split("/");
            var name_sele = tem[tem.length - 1];
            if(result.data){
                var senddata=result['data'];
                
                var list_colm =$(tbat_last).find('thead th');
                var listtd={};
                var listtd_sub={};
                for(var i=0;i<list_colm.length;i++){
                    var atrs = $(list_colm[i]).attr('att-name');                    
                    var atrs_subname = $(list_colm[i]).attr('att-sub-name');                    
                    listtd[i]='';
                    listtd_sub[i]='';
                    if(atrs){
                        listtd[i]=atrs;
                    }
                    if(atrs_subname){
                        listtd_sub[i]=atrs_subname;
                    }
                }
                var str='';

                for (var property1 in senddata) {
                    
                    str+='<tr type_colum="edit" at-old-id="'+senddata[property1]['id']+'">'
                    var str_tem='';
                    for (var property2 in listtd) {
                        if(property2==0){
                            str_tem+='<td><input type="checkbox"></td>';
                        }else{
                            var info_col= get_attr_tb_header_sub(tbat_last,property2);
                            if(listtd[property2] !=''){
                                if(typeof senddata[property1][listtd[property2]] =='object'){
                                    if(listtd_sub[property2]=='name'){
                                        var txtx = senddata[property1][listtd[property2]][listtd_sub[property2]];
                                        if(txtx== undefined){
                                            str_tem += senddata[property1][listtd[property2]][listtd_sub[property2][info_col['list_vl']][info_col['list_col']]];
                                            
                                        }else{
                                            str_tem+='<td><span att-value="'+senddata[property1][listtd[property2]]['id']+'">'+senddata[property1][listtd[property2]][listtd_sub[property2]]+'</span></td>';
                                        }
                                    }else{
                                        
                                        
                                        if(listtd[property2]=='medicine_id' && listtd_sub[property2]=='unit_id_name'){
                                            //console.log(senddata[property1])
                                            //console.log(listtd[property2])
                                            //console.log(senddata[property1]['medicine_id'])
                                            //console.log(listtd[property2]['medicine_id']['unit_id']['name']);
                                            //str_tem+='<td><span att-value="'+senddata[property1]['medicine_id']['unit_id']['id']+'</span>'+senddata[property1]['medicine_id']['unit_id']['name']+'</td>';
                                            str_tem +='<td><span att-value="'+senddata[property1]['medicine_id']['unit_id']['id']+'">'+senddata[property1]['medicine_id']['unit_id']['name']+'</span></td>';
                                        }else{
                                            var lsit_thuoc = template_select_more_link['medicine'][0]+property2;
                                            str_tem+='<td>'+senddata[property1][listtd[property2]][listtd_sub[property2]]+'</td>';
                                        }
                                    }
                                }else{
                                    str_tem+='<td>'+senddata[property1][listtd[property2]]+'</td>';
                                }
                            }else{
                                str_tem+='<td>1</td>';
                            }
                            
                        }
                    }
                    str +=str_tem;
                    str +='</tr>';
                }
                $(tbat_last).find('tbody').html(str);
                
            }
            var at_tem=tbat
            var index_select = find_index_select_in_table(at_tem)
            console.log(index_select)
            var list_id_select = get_list_select_value_old(at_tem,index_select)
            var at_select = $(at_tem).find('tfoot .data-type-select').attr('at-select')
            update_select_show_next(at_tem,at_select,list_id_select);
        })
    })
}

function build_html_body_sub(data,table){
    var list_colm =$(table).find('thead th');
    var listtd={};
    for(var i=0;i<list_colm.length;i++){
        var atrs = $(list_colm[i]).attr('att-name');
        listtd[i];
        if(atrs){
            listtd[i]=atrs;
        }
    }
    var str='<tr>';
    for(var m=0;m<data.length;m++){
        var str_tem='';
        for(var n=0;n<listtd.length;n++){
            str_tem+='<td></td>';
        }
        str +=str_tem;
    }
    str +='</tr>';
    return str;
}

function get_sub_element_get_info(at) {
    var info = {};
    info.at_link = $(at).closest('table').attr('data-link-api')
    info.at_select = $(at).closest('table').attr('data-link-select')
    info.at_ob_save = $(at).closest('table').attr('data-info-save')
    var tem = info.at_select.split("/");
    info.at_select_name = tem[tem.length - 1];
    info.table=$(at).closest('table');
    return info
}
function get_value_footer(at) {
    var main = $(at).closest('tr').find('th');
    var datatem = {};
    var input='';
    for (var i = 0; i < main.length; i++) {
        var ftem='',ftem1='';
        input=''
        ftem = $(main[i]).find('input').val();
        ftem1 = $(main[i]).find('select').val();
        var is_type=false;
        if(ftem){
            input=ftem;
            is_type=true
        }
        if(ftem1){
            is_type=true
            var position = i
            
            var info_col = get_attr_tb_header_sub($(at).closest('table'),position);
            var info_col_first = get_attr_tb_header_sub($(at).closest('table'),position-1); //back set id
                        
            var fr = $(main[i]).attr('at-select')
            var list_at =template_select_more_link[fr]
            var result_one = list_at.filter(function (item, key) {
                return item['id'] == ftem1;
            })
            var txt_show = result_one['0'][info_col.list_col];
            if(info_col_first.list_vl==info_col.list_vl){
                datatem[i-1]=result_one['0']['code'];
            }
            if(fr=='medicine'){
                datatem[i+1]='<span att-value="'+result_one['0']['unit_id']['id']+'">'+result_one['0']['unit_id']['name']+'</span>';
            }
            input='<span att-value="'+ftem1+'">'+txt_show+'</span>';
        }
        if(i==0){
            is_type=true
            input='<input type="checkbox">'
        }
        if(is_type){
            datatem[i]=input
        }
    }
    return datatem;
}
function get_attr_tb_header_sub(at,postsition){
    var tem=$(at).closest('.dataTables_wrapper').find('thead th').eq(postsition);
    var pro_at=tem.attr('att-sub-name');
    var pro_vl=tem.attr('att-name');
    var list={}
    if(pro_at || pro_vl){
        list['list_col']=pro_at;
        list['list_vl']=pro_vl;
    }
    return list;
}
function load_select_change_for_select() {
    var temp = $('tfoot tr th[data-type-select="package_healthcare_service_type"] .select-at-table').clone(true);
    $('#add .select-type-head').html(temp.html());
    $('#edit .select-type-head').html(temp.html());
}
function get_allinptu_edit(list) {
    $('#edit form input').each(function (index) {
        if ($(this).attr('type') == 'checkbox') {
            if (list[index] == 'Bật' || list[index] == 1 || list[index] == 'O' || list[index] == 'o' || list[index] == true) {
                $(this)[0].checked = true;
            } else {
                $(this).attr('checked', false);
            }
        } else {
            $(this).val(list[index]);
            if ($(this).hasClass('input-have-select')) {
                var vlselect = $(list[index]).attr('att-value')
                $(this).val(vlselect);
                $(this).closest('div').find('select').val(vlselect);
            }
        }
    })
}
get_list_more_select_sub();
function get_list_more_select_sub() {
    $('#add .grouplistsub').each(function () {
        var m = $(this).find('table').attr('data-link-select');
        var data = {};
        get_set_more_one_select(m, data)
    })
}
function get_set_more_one_select(m, data){
    get_list_sub(m, data, function (statusresult, result) {
            var tem = m.split("/");
            var name_sele = tem[tem.length - 1];
            var datastr = buil_html_fromt_at(result.data)
            template_select_more_link[name_sele] = result.data
            $('.data-type-select.' + name_sele).html(datastr);

        })
}
function buil_html_fromt_at(data) {
    var str = '';
    str = '<select class="form-control">';
    for (var i = 0; i < data.length; i++) {
        str += '<option value="' + data[i]['id'] + '">' + data[i]['name'] + '</option>'
    }
    str += '</select>';
    return str
}
function get_list_sub(link_in, data, callback) {
    connectApi.getList(link_in, data, function (statusresult, result) {
        callback(statusresult, result)
    })
}
function get_list_sub_load(tbat,link_in, data, callback) {
    connectApi.getList(link_in, data, function (statusresult, result) {
        callback(tbat,statusresult, result)
    })
}

var template_select_more_link = {};