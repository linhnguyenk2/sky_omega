
$('body ').on('click', '.dataTables_paginate .first', (function (e) {
    e.preventDefault();
    e.stopPropagation();

    var name_table = $(this).closest('.dataTables_wrapper').find('table').attr('id_table')
    _get_page_table_info(name_table, 'first');
}));
$('body ').on('click', '.dataTables_paginate .previous', (function (e) {
    e.preventDefault();
    e.stopPropagation();
    var name_table = $(this).closest('.dataTables_wrapper').find('table').attr('id_table')
    _get_page_table_info(name_table, 'previous');
}));
$('body ').on('click', '.dataTables_paginate .next', (function (e) {
    e.preventDefault();
    e.stopPropagation();
    var name_table = $(this).closest('.dataTables_wrapper').find('table').attr('id_table')
    _get_page_table_info(name_table, 'next');
}));
$('body ').on('click', '.dataTables_paginate .last', (function (e) {
    e.preventDefault();
    e.stopPropagation();
    var name_table = $(this).closest('.dataTables_wrapper').find('table').attr('id_table')
    _get_page_table_info(name_table, 'last');
}));
$('body ').on('click', '.dataTables_paginate span .paginate_button', (function (e) {
    e.preventDefault();
    e.stopPropagation();
    var name_table = $(this).closest('.dataTables_wrapper').find('table').attr('id_table')
    var at=$(this).attr('data-dt-idx');
    _get_page_table_info(name_table, 'at',at);
}));

function _get_page_table_info(tem_tb, eve,at) {
    var page = parseInt($(tem_tb).attr('page'));
    var sumpage = $(tem_tb).attr('sumpage');
    var limit = $(tem_tb).attr('limit');
    var page_chage;
    switch (eve) {
        case 'first':
            page_chage = 1;
            break;
        case 'previous':
            page--;
            if (page < 1) {
                page_chage = 1;
            } else {
                page_chage = page;
            }
            break;
        case 'next':
            page++;
            if (page >= sumpage) {
                page_chage = sumpage;
            } else {
                page_chage = page;
            }
            break;
        case 'last':
            page_chage = sumpage;
            break;
        case 'at':
            page_chage = at;
            break;
        default:
            break;
    }
    $(tem_tb).attr('page', page_chage);
    var url_api = $(tem_tb).attr('data-link-api');
    var data_para = $(tem_tb).attr('data-para');
    if(data_para){
        loadTableInt(tem_tb,link_base+url_api+'/?'+data_para)
    }else{
        loadTableInt(tem_tb,link_base+url_api)
    }
    build_number_page(tem_tb)
}

function build_number_page(tem_tb) {
    var page = parseInt($(tem_tb).attr('page'));
    var sumpage = parseInt($(tem_tb).attr('sumpage'));
    var limit = parseInt($(tem_tb).attr('limit'));

    var tem = get_min_max(page,sumpage);
    var ct = content_pageding(page,tem['min'],tem['max'])
    $(tem_tb).closest('div').find('.dataTables_info').html('Showing page '+page+' of '+sumpage);
    $(tem_tb).closest('div').find('.dataTables_paginate span').html(ct);
}

function get_min_max(page,sumpage){
    var min=1;
    var max=0;
    if(page-1<=1){
        min=1
    }else{
        min=page-1;
    }
    if(sumpage-1<=page){
        max=sumpage;
    }else{
        max=page+1;
    }
    if(sumpage<page){
        page =sumpage
    }
    var at=[];
    at['min']=min;
    at['max']=max;
    return at;
}

function content_pageding(p_current, min, max) {
    var tem_content = '';
    var c_current;
    for (var i = min; i <= max; i++) {
        c_current = '';
        if (i == p_current) {
            c_current = 'current';
        }
        tem_content += '<a class="paginate_button ' + c_current + '" data-dt-idx="' + i + '" tabindex="0">' + i + '</a>';
    }
    return tem_content;
}