var link_api = '';

var at_table = '#list_register_services';
var link_of_table = '';
$(document).ready(function () {
    'use strict';

    link_of_table =link_base + $(at_table).attr('data-link-api');

    loadTableInt(at_table,link_of_table);
    load_place_holder(at_table);
    $('body ').on('click', at_table + ' thead input[type="checkbox"]', (function () {
        var sThisVal = $(this).prop('checked');
        $(at_table + ' tbody input[type="checkbox"]').each(function () {
            $(this).prop('checked', sThisVal);
        });
        show_hide_button(at_table);
    }));

    $('body ').on('change', at_table + ' tbody input[type="checkbox"]', (function () {
        show_hide_button(at_table);
    }));


    /*ADD*/
    $('body').on('click',' #addRegisterservices', (function () {
        var at = $('.modal-content').find('.form-control');
        var data={};
        var link ='api/v1/patient/specify_service_paper?staff_id_type=400'
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-name');
            if (attr) {
                var value_inpt = $(this).val()
                data[attr]=value_inpt
            }
        });
        var type = $('#datatimepicker').attr('attr-fomart-save')
        //console.log(data)
        data.created_at = _get_currenttime()
        data.staff_id = ($('.user_id').val())
        data.appointment_time = _formart_static(type,data.appointment_time)
        console.log(data)
        var link = 'api/v1/patient/specify_service_paper?staff_id_type=400'
        link = link_base+link
        post_data_api(link,data,function(statusresult, result){
            loadTableInt(at_table,link);
            $('#openModal').modal('toggle');
            glb_show_message('Thêm thành công')
        });
    }));


    $('body').on('change', '#patient_id', function() {
         show_info_patient()
    });

    /*END ADD*/
    $('body').on('click',' #btn_add', (function () {
        $('#patient_id').attr('disabled',false)
        $('#datatimepicker').attr('attr-fomart-save','HH:mm dd/mm/yyyy-yyyy/mm/dd')
        $('#datatimepicker').attr('attr-fomart-show','HH:mm dd/mm/yyyy')
        $("#myModalLabel").html("Thêm mới");
        $("#editRegisterservices").html("Thêm");
        $('#editRegisterservices').attr('id', 'addRegisterservices');
     }));


    $('body').on('click',' #btn_delete', (function () {
        $('#editRegisterservices').attr('id', 'addRegisterservices');
     }));
    /*EDIT*/

    $('body').on('click',' #btn_edit', (function () {
         
        var id_col
        var data={};

        $(at_table + ' tbody input[type="checkbox"]').each(function () {
        var sThisVal = $(this).prop('checked');
        if (sThisVal) {
            id_col =$(this).closest('tr').attr('id_colum')
        }
        });
        $('#datatimepicker').attr('attr-fomart-save','HH:mm dd/mm/yyyy-yyyy/mm/dd')
        $('#datatimepicker').attr('attr-fomart-show','HH:mm dd/mm/yyyy')
        $("#myModalLabel").html("Chỉnh sửa");
        $("#editRegisterservices").html("Lưu");
        $('#addRegisterservices').attr('id', 'editRegisterservices');
        $('#patient_id').attr('disabled',true)
        $('#datatimepicker').attr('attr-fomart-show','HH:mm dd/mm/yyyy')
        var type = $('#datatimepicker').attr('attr-fomart-show')
        var id_patient
        var tem_ct = '';
        var tem = $('.form-group').find('select[name="patient_id"]');
        var link =link_base + 'api/v1/patient/specify_service_paper/'+id_col;
        get_data_api(link, data, function (statusresult, result) {

        $(".form-group #name_patient").text(result.data.patient_id.user_id.fullname);
        $(".form-group #birth_day").text(result.data.patient_id.user_id.birthday);
        $(".form-group #marital_status").html(_select_static_search_to_string('is_married',result.data.patient_id.user_id.is_married));
        $(".form-group #gender").html(_select_static_search_to_string('gender',result.data.patient_id.user_id.gender));
        $(".form-group #nationality").text(result.data.patient_id.user_id.nationality_id.name);
        $(".form-group #identity_card").text(result.data.patient_id.user_id.folk_id.code);
        $(".form-group #address_patient").text(result.data.patient_id.user_id.address);
        $(".form-group #email_patient").text(result.data.patient_id.user_id.email);
        $(".form-group #phone_patient").text(result.data.patient_id.user_id.home_phone);
        $(".form-group #cell_phone_patient").text(result.data.patient_id.user_id.mobile);
        $(".form-group #job_patient").text(result.data.patient_id.user_id.title_id.name);
        $(".form-group #quantity").val(result.data.quantity);
        $(".form-group #reason").val(result.data.reason);
        $(".form-group #datatimepicker").val(_formart_static(type,result.data.appointment_time));
        tem_ct += '<option selected value="' + result.data.patient_id.id + '">' + result.data.patient_id.user_id.code + '</option>';
        tem.html(tem_ct)
    })
    }))

    $('body').on('click',' #editRegisterservices', (function () {
    var id_col
    $(at_table + ' tbody input[type="checkbox"]').each(function () {
    var sThisVal = $(this).prop('checked');
    if (sThisVal) {
        id_col =$(this).closest('tr').attr('id_colum')
    }
    });
    var at = $('.modal-content').find('.form-control');
    var data={};
    $.each(at, function (index, value) {
        var attr = $(this).attr('attr-name');
        if (attr) {
            var value_inpt = $(this).val()
            data[attr]=value_inpt
        }
    });
    var type = $('#datatimepicker').attr('attr-fomart-save')
    data.staff_id = ($('.user_id').val())
    data.appointment_time = _formart_static(type,data.appointment_time)
    var link = 'api/v1/patient/specify_service_paper'
    var link_show_tb = 'api/v1/patient/specify_service_paper?staff_id_type=400'
        link_show_tb = link_base+link_show_tb
    link = link_base+link+'/'+id_col
    put_data_api(link,data,function(statusresult, result){
        loadTableInt(at_table,link_show_tb);
        $('#openModal').modal('toggle');
        glb_show_message('Cập nhật thành công')
    });
    }));

    /*END EDIT*/

    //--------------Delete

       $('body ').on('click', '#category_name #delete .modal-footer button[data-dismiss!="modal"]', (function () {
        var list_in = $('#delete .modal-content input').val();
        list_in = list_in.split(",");
        var link_delete
        link_delete = link_base+'api/v1/patient/specify_service_paper'
        delete_more(link_delete,list_in, function () {
            loadTableInt(at_table,link_of_table)
            $('#myDelete').modal('toggle');
            show_hide_button(0);
        })

    }));

    $('body ').on('click', '#category_name [data-target="#myDelete"]', (function () {
        var list_id = [];
        var list_all = [];
        var list_name = [];
        $(at_table + ' td input').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var list_id = $(this).closest('tr').attr('id_colum');
                list_all.push(list_id);
                var showat = 2;
                if ($(this).closest('table').find('thead tr').attr('show-position')) {
                    showat = $(this).closest('table').find('thead tr').attr('show-position');
                }
                list_name.push($(this).closest('tr').find('td').eq(showat).text());
            }
        });
        if (list_all[0]) {
            var more_one = '.one_show';
            if (list_all.length > 1) {
                more_one = '.more_show';
            }
            get_allinptu_delete(list_all.join(), more_one, list_name[0]);
        }

    }));

    $('body ').on('dblclick', at_table+' tbody tr td', (function (index,val) {
    var colindex= $(this).parent().children().index($(this));
    if(colindex==0)
        return;
    var id = $(this).closest('tr').attr('id_colum');
    var link =link_base+'edit_services/'+id;
    window.location.href=link
    }));





    //--------------ADD
        $('body').on('click',' #addMenupermission', (function () {
        var at = $('.popupdeleteuser').find('.cdeleteuser');
        var data={};
        var link ='api/v1/permission/menu_permission/'
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-name');
            if (attr) {
                var value_inpt = $(this).val()
                data[attr]=value_inpt
            }
        })
        link = link_base+link
        post_data_api(link,data,function(statusresult, result){
           loadTableInt(at_table,link_of_table)
           $('#openModal').modal('toggle');
           glb_show_message('Thêm thành công')
        });
    }));

});


function show_info_patient()
{
  var id = $('#patient_id').val()
  var at = $('#add_services_form').find('.form-group');
        var data={};
        var link ='api/v1/patient/patients'
        $.each(at, function (index, value) {
            var attr = $(this).attr('attr-name');
            if (attr) {
                var value_inpt = $(this).val()
                data[attr]=value_inpt
            }
        })

        link = link_base+link+'/'+id
        get_data_api(link,{item_in_page: 0},function (statusresult, result){
           $(".form-group #name_patient").text(result.data.user_id.fullname);
           $(".form-group #birth_day").text(result.data.user_id.birthday);
           $(".form-group #marital_status").html(_select_static_search_to_string('is_married',result.data.user_id.is_married));
           $(".form-group #gender").html(_select_static_search_to_string('gender',result.data.user_id.gender));
           $(".form-group #nationality").text(result.data.user_id.nationality_id.name);
           $(".form-group #identity_card").text(result.data.user_id.folk_id.code);
           $(".form-group #address_patient").text(result.data.user_id.address);
           $(".form-group #email_patient").text(result.data.user_id.email);
           $(".form-group #phone_patient").text(result.data.user_id.home_phone);
           $(".form-group #cell_phone_patient").text(result.data.user_id.mobile);
           $(".form-group #job_patient").text(result.data.user_id.fullname);
        });
}
/*Get select patient*/
function load_option_for_patient(at_select, data,attr) {
    var str = build_html_select_option_code_patient(data);
    var tem = $('.form-group').find('select[name="' + at_select.attr_name + '"]');
    tem.html(str)
}
function load_option_for_type_service(at_select, data,attr) {
    var str = build_html_select_option_name(data);
    var tem = $('.form-group').find('select[name="' + at_select.attr_name + '"]');
    tem.html(str)
}
function load_option_for_place_action(at_select, data,attr) {
    var tem_ct = '';
    for (var i = 0; i < data.length; i++) {
        tem_ct += '<option value="' + data[i].id + '">' + data[i].location_id.name + '</option>';
    }
    var tem = $('.form-group').find('select[name="' + at_select.attr_name + '"]');
    tem.html(tem_ct)
}
load_select_for_add_service();
function load_select_for_add_service(){
    var list_tb = $('.form-group').find('select')

    $.each(list_tb, function (idnex, value) {

        var info_select = get_select_info(this)
        var attr = $(this).attr('attr-value')
        if (info_select.api_link) {
        var link_of_select = link_base + info_select.api_link;
            if(attr =="patient_id")
            {
                get_data_api(link_of_select, {item_in_page: 0}, function (statusresult, result) {
                load_option_for_patient(info_select, result.data,attr);
                
                ($('#patient_id').find("option:first").val()); 
                $('#patient_id').val("1")
                $(this).trigger('change')
                show_info_patient()});
            }
            if(attr =="service_location_id")
            {
                get_data_api(link_of_select, {item_in_page: 0}, function (statusresult, result) {
                load_option_for_place_action(info_select, result.data,attr);});
            }
            if(attr =="type_services")
            {
                get_data_api(link_of_select, {item_in_page: 0}, function (statusresult, result) {
                load_option_for_type_service(info_select, result.data,attr);});

            }
        }
    });
}
function get_select_info(at) {
    var info = {};
    var link_api = $(at).attr('api-link')

    if (link_api) {
        info.attr_name = $(at).attr('name')
        info.api_link = link_api
        var tem = info.api_link.split("/");
        info.at_select_name = tem[tem.length - 1];
    }
    return info
}
