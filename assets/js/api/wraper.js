

var APIConnect = function () {
    this.getList = function (link, data, callback) {
        $.ajax({
            url: link,
            type: "GET",
            data: data,
            dataType: "json",
            beforeSend: function(xhr){
                var session_id = '';
                var device_id = '';

                if (document.getElementsByClassName("session-id").length > 0) {
                    var session_id = document.getElementsByClassName("session-id")[0].value;
                }

                if (document.getElementsByClassName("device-id").length > 0) {
                    var device_id = document.getElementsByClassName("device-id")[0].value;
                }

                xhr.setRequestHeader('Session-Id', session_id);
                xhr.setRequestHeader('Device-Id', device_id);
            },
            success: function (data, status, jqXHR) {
                callback(jqXHR.status, data)
            },
            error: function (jqXHR, status, err) {
                callback(jqXHR.status, jqXHR);
            },
            complete: function (jqXHR, status) {

            }
        })
    }
    this.deleteEvent = function (link, data, callback) {
        $.ajax({
            url: link,
            type: "DELETE",
            data: data,
            dataType: "json",
            beforeSend: function(xhr){
                var session_id = '';
                var device_id = '';

                if (document.getElementsByClassName("session-id").length > 0) {
                    var session_id = document.getElementsByClassName("session-id")[0].value;
                }

                if (document.getElementsByClassName("device-id").length > 0) {
                    var device_id = document.getElementsByClassName("device-id")[0].value;
                }

                xhr.setRequestHeader('Session-Id', session_id);
                xhr.setRequestHeader('Device-Id', device_id);
            },
            success: function (data, status, jqXHR) {
                callback(jqXHR.status, data)
            },
            error: function (jqXHR, status, err) {
                callback(jqXHR.status, jqXHR);
            },
            complete: function (jqXHR, status) {
            }
        })
    }
    this.addEvent = function (link, data, callback) {
        $.ajax({
            url: link,
            type: "POST",
            data: data,
            dataType: "json",
            beforeSend: function(xhr){
                var session_id = '';
                var device_id = '';

                if (document.getElementsByClassName("session-id").length > 0) {
                    var session_id = document.getElementsByClassName("session-id")[0].value;
                }

                if (document.getElementsByClassName("device-id").length > 0) {
                    var device_id = document.getElementsByClassName("device-id")[0].value;
                }

                xhr.setRequestHeader('Session-Id', session_id);
                xhr.setRequestHeader('Device-Id', device_id);
            },
            success: function (data, status, jqXHR) {
                if (jqXHR.status == http_status.HTTP_OK) {
                    if (data.status.return_code == api_status.API_STATUS_OK) {
                        callback(jqXHR.status, data)
                    } else {
                        callback(jqXHR.status, data)
                    }
                } else {
                    callback(jqXHR.status, data)
                }
            },
            error: function (jqXHR, status, err) {
                callback(jqXHR.status, jqXHR);
            },
            complete: function (jqXHR, status) {
            }
        })
    }
    this.updateEvent = function (link, data, callback) {
        $.ajax({
            url: link,
            type: "PUT",
            data: data,
            dataType: "json",
            beforeSend: function(xhr){
                var session_id = '';
                var device_id = '';

                if (document.getElementsByClassName("session-id").length > 0) {
                    var session_id = document.getElementsByClassName("session-id")[0].value;
                }

                if (document.getElementsByClassName("device-id").length > 0) {
                    var device_id = document.getElementsByClassName("device-id")[0].value;
                }

                xhr.setRequestHeader('Session-Id', session_id);
                xhr.setRequestHeader('Device-Id', device_id);
            },
            success: function (data, status, jqXHR) {

                if (jqXHR.status == http_status.HTTP_OK) {
                    if (data.status.return_code == api_status.API_STATUS_OK) {
                        callback(jqXHR.status, data)
                    } else {
                        callback(jqXHR.status, data)
                    }
                } else {
                    callback(jqXHR.status, data)
                }
            },
            error: function (jqXHR, status, err) {
                callback(jqXHR.status, jqXHR);
            },
            complete: function (jqXHR, status) {
            }
        })
    }
}