var link_api = 'api/v1/list/';
var at_table = '#DataTables_Table_0 table';
var link_of_table = '';

$(document).ready(function () {
    'use strict';

    link_of_table = $(at_table).attr('data-link-api');

    loadTableInt(at_table,link_of_table);
    load_place_holder(at_table);

    $('body ').on('click', at_table + ' thead input[type="checkbox"]', (function () {
        var sThisVal = $(this).prop('checked');
        $(at_table + ' tbody input[type="checkbox"]').each(function () {
            $(this).prop('checked', sThisVal);
        });
        show_hide_button(at_table);
    }));

    $('body ').on('change', at_table + ' tbody input[type="checkbox"]', (function () {
        show_hide_button(at_table);
    }));

    //--------------Delete

    $('body ').on('click', '#category_name #delete .modal-footer button[data-dismiss!="modal"]', (function () {
        var list_in = $('#delete .modal-content input').val();
        list_in = list_in.split(",");

        delete_more(link_of_table,list_in, function () {
            loadTableInt(at_table,link_of_table)
            $('#myDelete').modal('toggle');
            show_hide_button(0);
        })

    }));
   


    $('body ').on('click', '#category_name [data-target="#myDelete"]', (function () {
        var list_id = [];
        var list_all = [];
        var list_name = [];
        $(at_table + ' td input').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var list_id = $(this).closest('tr').attr('id_colum');
                list_all.push(list_id);
                var showat = 2;
                if ($(this).closest('table').find('thead tr').attr('show-position')) {
                    showat = $(this).closest('table').find('thead tr').attr('show-position');
                }
                list_name.push($(this).closest('tr').find('td').eq(showat).text());
            }
        });
        if (list_all[0]) {
            var more_one = '.one_show';
            if (list_all.length > 1) {
                more_one = '.more_show';
            }
            get_allinptu_delete(list_all.join(), more_one, list_name[0]);
        }

    }));


    //--------------End Delete

    //--------Search keyword

    var searchkey = $(at_table).closest('div').find('.row').eq(0).find('.dataTables_filter input');
    $('body ').on('keyup', searchkey, (function (e) {
        if (e.which == 13) {
            loadTableInt(at_table,link_of_table);
        }
    }));

    //--------End Search

    //-------Change Page
    $('body ').on('change', '.select-page-option', (function (e) {
        loadTableInt(at_table,link_of_table);
    }));

    //-------End Change Page


    //-------Add new 
    $('body ').on('keyup', at_table + ' tfoot tr th', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (e.which == 13) {
            add_row_table(at_table);
        }
        if (e.which == 27) {

        }
    }));

    

    //---------Edit 
    var value_first = '';
    var value_first_html = '';
    var is_onchange = false;
    $('body ').on('dblclick', at_table + ' tbody tr td', (function (e) {
        e.preventDefault();
        if ($(this).html() == '<input type="checkbox">') {
            return;
        }
        var col = $(this).parent().children().index($(this));
        var gettemplate = $(at_table + ' tfoot tr th').eq(col);
        if ($(gettemplate).attr('att-edit') == 'false') {
            return;
        }
        if (is_onchange == true) {
            return;
        }
        var content_html = $(this).html();
        var atr = $(content_html).attr('att-value');
        var datacurrent;
        value_first_html = content_html;
        var gettemplate = $(at_table + ' tfoot tr th').eq(col).html();
        var have_select = $(gettemplate).hasClass('select-at-table');
        if (atr != undefined) {
            var col = $(this).parent().children().index($(this));
            $(this).html(gettemplate);
            $(this).find('select').val(atr);
            datacurrent = atr;

        } else if(have_select==true){
            
            var col = $(this).parent().children().index($(this));
            $(this).html(gettemplate);
            //$(this).find('select').val(atr);
            datacurrent = atr;
        }else {
            datacurrent = $(this).text();
            $(this).html('<input type="text" id="onchange" value="' + datacurrent + '">');
            $("#onchange").focus();
        }
        value_first = datacurrent;
        is_onchange = true;
    }));
    $('body ').on('click', at_table + ' tbody #onchange', (function (e) {
        e.preventDefault();
        e.stopPropagation();
    }));
    $('body ').on('keyup focusout', at_table + ' tbody #onchange, ' + at_table + ' tbody tr td select', (function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (is_onchange == false) {
            return
        }
        if (e.which == 13 || e.type == 'focusout') {
            var datacurrent = $(this).val();
            var id_datacurrent = $(this).closest('tr').attr('id_colum');
            var positionat = $(this).parent().index();
            var data_first = {};
            var data_save = {};

            var data_send1 = {};
            var colum_id1 = $(at_table + ' thead tr').find('th').eq(0).attr('att-name');
            var colum_id2 = $(at_table + ' thead tr').find('th').eq(positionat).attr('att-name');
            if (!colum_id1) {
                console.error('Not set attibute for id save data');
                colum_id1 = 0;
                return;
            }
            if (!colum_id2) {
                console.error('Not set attibute for id save data');
                colum_id1 = 1;
                return;
            }
            data_send1[colum_id1] = id_datacurrent;
            data_send1[colum_id2] = datacurrent;
            data_save = data_send1;

            var data_first = {};
            data_first[colum_id1] = id_datacurrent;
            data_first[colum_id2] = value_first;

            var at_positioninfo = this;
            if (value_first == datacurrent) {
                if (is_onchange == true) {
                    if ($(value_first_html).attr('att-value') != undefined) {
                        $(at_positioninfo).closest('td').html(value_first_html);
                    } else {
                        $(at_positioninfo).closest('td').text(datacurrent);
                    }
                    is_onchange = false;
                }
                return;
            }
            var link = link_of_table + '/' + id_datacurrent;
            update_at(data_save, link, function (statusresult, result) {
                if (is_onchange == true) {
                    is_onchange = false;
                }
                loadTableInt(at_table,link_of_table)
            })
        }
        if (e.keyCode == 27) {
            if ($(value_first_html).attr('att-value') != undefined) {
                $(this).closest('td').html(value_first_html);
            } else {
                $(this).closest('td').text(value_first);
            }
            if (is_onchange == true) {
                is_onchange = false;
            }
        }
    }));


    $('body ').on('click', '#category_name [data-target="#myPush"]', (function () {
        var list_id = [];
        var list_all = [];
        var list_name = [];
        $(at_table + ' td input').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var list_id = $(this).closest('tr').attr('id_colum');
                list_all.push(list_id);
                var showat = 2;
                if ($(this).closest('table').find('thead tr').attr('show-position')) {
                    showat = $(this).closest('table').find('thead tr').attr('show-position');
                }
                list_name.push($(this).closest('tr').find('td').eq(showat).text());
            }
        });

        if (list_all[0]) {
            var more_one = '.one_show';
            if (list_all.length > 1) {
                more_one = '.more_show';
            }
            get_allinptu_push(list_all.join(), more_one, list_name[0]);
        }

    }));
    $('body ').on('click', '#category_name [data-target="#myUnpush"]', (function () {
        var list_id = [];
        var list_all = [];
        var list_name = [];
        $(at_table + ' td input').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            if (sThisVal) {
                var list_id = $(this).closest('tr').attr('id_colum');
                list_all.push(list_id);
                var showat = 2;
                if ($(this).closest('table').find('thead tr').attr('show-position')) {
                    showat = $(this).closest('table').find('thead tr').attr('show-position');
                }
                list_name.push($(this).closest('tr').find('td').eq(showat).text());
            }
        });

        if (list_all[0]) {
            var more_one = '.one_show';
            if (list_all.length > 1) {
                more_one = '.more_show';
            }
            get_allinptu_unpush(list_all.join(), more_one, list_name[0]);
        }

    }));
    
    $('body ').on('click', '#category_name #push .modal-footer button[data-dismiss!="modal"]', (function () {
        var list_in = $('#push .modal-content input').val();
        list_in = list_in.split(",");
        update_more_status(link_of_table,list_in, '1', function () {
            loadTableInt(at_table,link_of_table)
            $('#myPush').modal('toggle');
            show_hide_button(0);
        })
    }));
    $('body ').on('click', '#category_name #unpush .modal-footer button[data-dismiss!="modal"]', (function () {
        var list_in = $('#unpush .modal-content input').val();
        list_in = list_in.split(",");

        update_more_status(link_of_table,list_in, '0', function () {
            loadTableInt(at_table,link_of_table)
            $('#myUnpush').modal('toggle');
            show_hide_button(0);
        })

    }));



    //-----End edit

    //----Sort

    $('body ').on('click', at_table + ' thead tr th', (function () {
        if ($(this).hasClass('sorting_disabled')) {
            return;
        }
        var sortat = $(this).attr('data-sort');
        if (sortat != 'desc') {
            sortat = 'asc';
        } else {
            sortat = 'desc';
        }
        remove_all_sort(at_table);
        $(this).addClass('sortat');
        $(this).attr('data-sort', sortat);
        change_sort_direction(at_table);
        loadTableInt(at_table,link_of_table);

    }));
    
    //-----end sort


})
