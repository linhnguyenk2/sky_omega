var s, SumElement = {
    settings: {
        atClass: 'div[id^="myShow"] td',
        deleteElement: 'div[id^="myShow"] .show-body .btn-danger',
        addElement: 'div[id^="myShow"] .show-footer .btn-primary',
        selectElement: 'div[id^="myShow"] .show-body select',
        inputElement: 'div[id^="myShow"] .show-body input',
    },
    init: function () {
        s = this.settings;
        this.bind_All();
    },
    bind_All: function () {
        this.bindClickAdd();
        this.bindClickDele();
        this.bindClickAddElement();
        this.bindClickSelectElement();
        this.bindClickInputElement();
    },
    bindClickAdd: function () {
        $('body').on("click", s.atClass, function () {
            SumElement.funGetList(123);
        });
    },
    bindClickDele: function () {
        $('body').on("click", s.deleteElement, function () {
            var infoModel = SumElement.funGetInfoModel($(this));
            SumElement.funDelete(infoModel.link_save, infoModel.div_id,function(data){
                reload_table_sub(infoModel.link_save, infoModel.id_medicine, infoModel.data_model)
            });
        });
    },
    bindClickAddElement: function () {
        $('body').on("click", s.addElement, function () {
            var infoModel = SumElement.funGetInfoModel($(this));
            var data = {};
            $.each($(this).closest('.show-footer').find('.form-control'), function (index, value) {
                var vl = $(value).val();
                var col_value = $(value).attr('data-col');
                data[col_value] = vl;
            })
            if(infoModel.name_id_medicine){
                data[infoModel.name_id_medicine] = infoModel.id_medicine;
            }else{
                data.medicine_id = infoModel.id_medicine;
            }
            if(infoModel.link_save)
            save_add_data(infoModel.link_save, data, function (statusresult, result) {
                reload_table_sub(infoModel.link_save, infoModel.id_medicine, infoModel.data_model)
            });   
        });
    },
    bindClickSelectElement: function (){
        var tem_selefocus='';
         $('body').on("focus", s.selectElement, function (e) {
            e.preventDefault();
            e.stopPropagation();
            tem_selefocus=$(this).val();
        });
        $('body').on("keyup focusout", s.selectElement, function (e) {
            e.preventDefault();
            e.stopPropagation();
            if(e.which == 13 || e.type=='focusout') {
                if(tem_selefocus==$(this).val())
                    return
                var infoModel = SumElement.funGetInfoModel($(this));
                var data = {};
                data.id=infoModel.div_id
                data[$(this).attr('data-col')]=$(this).val();
                SumElement.funUpdate(infoModel.link_save+'/'+infoModel.div_id,data,function(){
                    reload_table_sub(infoModel.link_save, infoModel.id_medicine, infoModel.data_model)
                })
            }
        });
    },
    bindClickInputElement: function (){
        var tem_textfocus='';
         $('body').on("focus", s.inputElement, function (e) {
            e.preventDefault();
            e.stopPropagation();
            tem_textfocus=$(this).val();
        });
        $('body').on("keyup focusout", s.inputElement, function (e) {
            e.preventDefault();
            e.stopPropagation();
            if(e.which == 13 || e.type=='focusout') {
                if(tem_textfocus==$(this).val())
                    return
                var infoModel = SumElement.funGetInfoModel($(this));
                var data = {};
                data.id=infoModel.div_id
                data[$(this).attr('data-col')]=$(this).val();
                SumElement.funUpdate(infoModel.link_save+'/'+infoModel.div_id,data,function(){
                    reload_table_sub(infoModel.link_save, infoModel.id_medicine, infoModel.data_model)
                })
            }
        });
    },
    funUpdate: function (link,data,callback){
        update_at(data, link, function (statusresult, result) {
            callback(1)
        })
    },
    funGetList: function (innumber) {
    },
    funDelete: function (link, id,callback) {
        var data = {};
        link = link + '/' + id;
        delete_at(data, link, function (statusresult, result) {
            callback(1)
        })
    },
    funGetInfoModel:function (sub){
        var data={};
        data.link= $(sub).closest('.form-horizontal').attr('data-link-api-main');
        data.link_save= $(sub).closest('.form-horizontal').attr('data-link-api-save');
        if(!data.link_save){
            data.link_save=data.link
        }
        data.id_medicine= $(sub).closest('.form-horizontal').attr('data-at-id');
        data.div_id= $(sub).closest('.form-group').attr('div_id');
        data.name_id_medicine= $(sub).closest('.form-horizontal').attr('data-at-name');
        data.data_model= '#'+$(sub).closest('.modal').attr('id');
        return data;
    }
}
var info_unit_id='';
$(function () {
    
    $('body').on('click', at_table + ' tbody a', function () {
        var id_at = $(this).closest('tr').attr('id_colum');
        var data_model = $(this).attr('data-target');
        $(data_model).find('.form-horizontal').attr('data-at-id', id_at);
        var linkapi_get = $(data_model).find('.form-horizontal').attr('data-link-api-main');
        var linkapi_get_select = $(data_model).find('.form-horizontal').attr('data-link-api-sub');
        var row_num = $($(this).closest('td')).parent().parent().children().index($($(this).closest('td')).parent());
        var colmn =$($(this).closest('.table')).find('thead th[att-save="unit_id"]')[0].cellIndex;
        info_unit_id =$($(this).closest('.table')).find('tbody tr').eq(row_num).find('td').eq(colmn).html();
        
        if(!linkapi_get_select){
            reload_table_sub(linkapi_get, id_at, data_model);
            build_foot_select_template(data_model);
        }else{
            var name_get = get_name_end_point(linkapi_get_select);
            load_template_select_connect_api(linkapi_get_select, '', function (r_id, r_statusresult, r_result) {
                list_select_buil[name_get]=r_result.data;
                reload_table_sub(linkapi_get, id_at, data_model);
                build_foot_select_template(data_model);
            })
        }
    })
    
    SumElement.init();

});

    function get_name_end_point(str){
        if(!str) return
        var tem = str.split("/");
        return tem[tem.length-1];
    }
    function reload_table_sub(linkapi_get, id_at, data_model){
        if(!linkapi_get || !id_at){
            return;
        }
        
        get_data_for_model(linkapi_get, id_at, function (result) {
            var data = result.data;
            var content_template = $(data_model).find('.form-horizontal .show-footer').clone(true);

            var text_all = '';
            if (data) {
                var list_ele = get_propertiobject(data[0]);
                var txt_change = '';
                for (var at in data) {
                    var jq_html = (content_template);
                    for (var property1 in list_ele) {
                        var valueat = data[at][list_ele[property1]]


                        //var at_atr_change = '.form-control.at_' + list_ele[property1];
                        var at_atr_change = '.at_' + list_ele[property1];
                        //alert(at_atr_change)
                        var getid = jq_html.find(at_atr_change);
                        //var getid = jq_html.find('form-control').filter(at_atr_change);
                        //alert(getid.length)
                        console.log(jq_html)
                        if (getid.length > 0) {
                            for(var j= 0;j<getid.length;j++){
                                var at_select=getid.eq(j)
                                var tag = at_select.prop("tagName").toLowerCase();
                                if (tag == 'input') {
                                    at_select.attr('value', valueat);
                                }
                                //alert(tag)
                                if (tag == 'select') {
                                    var attr_show = at_select.attr('type-select-tem');
                                     var attr_id_show = at_select.attr('type-attr-show');
                                    var attr_id_load = at_select.attr('type-attr-load');
                                    //alert(attr_id_load)
                                    if(attr_id_load){
                                        valueat =  resolve_keys_object(attr_id_load,data[at]);
                                        alert(valueat)
                                    }
                                    if (attr_show) {
                                        var tem = buil_select_template(attr_show,attr_id_show, 1, 100, valueat);
                                        at_select.html(tem);
                                    } else {
                                        at_select.attr('newvalue', valueat);
                                    }
                                }
                                if (tag == 'div') {
                                    at_select.attr('div_' + list_ele[property1], valueat);
                                }
                                if (tag == 'span') {
                                    if(typeof valueat=='object'){
                                        at_select.html(info_unit_id);
                                    }
                                }
                            }
                            
                        }
                    }
                    jq_html = buil_remove_button_add(jq_html);
                    text_all += jq_html.html();
                }
            }
            $(data_model).find('.form-horizontal .show-body').html(text_all);
            
        });
    }
    function build_foot_select_template(data_model){
        if(data_model=='#myShow04' && info_unit_id){
            $('#myShow04 .at_unit_id').html(info_unit_id)
            $('#myShow04 .parent_unit_medicine_id').val($(info_unit_id).attr('att-value'))
        }
        var jq_html = $(data_model).find('.form-horizontal .show-footer'); //not clone
        var all_select= jq_html.find('select');
        $.each(all_select,function(index,value){
            var attr_show = $(value).attr('type-select-tem');
            var attr_id_show = $(value).attr('type-attr-show');
            if (attr_show) {
                var tem = buil_select_template(attr_show,attr_id_show, 1, 100, '');
                all_select.eq(index).html(tem)
            }
        })
    }
    
    function buil_remove_button_add(jq_eleent) {
        var tem = jq_eleent.find('.btn-primary');
        tem.removeClass('btn-primary');
        tem.addClass('btn-danger');
        tem.html('Xóa');
        return jq_eleent;
    }
    function buil_select_template(type,attr_id_show, min, max, select) {
        if (type == 'age') {
            return get_select_option_basic(min, max, select)
        }else{
            if(list_select_buil[type]){
               if(typeof select === 'object'){
                   return build_html_select_option_object_have_selected( list_select_buil[type],attr_id_show,select.id)
               }else{
                   return build_html_select_option_object_have_selected( list_select_buil[type],attr_id_show,select)
               }
            }
        }
    }
    function get_select_option_basic(min, max, select) {
        var str = '';
        for (var i = min; i < max; i++) {
            if (i == select) {
                str += '<option selected="selected" value="' + i + '">' + i + '</option>'
            } else {
                str += '<option value="' + i + '">' + i + '</option>'
            }
        }
        return str;
    }
    function build_html_select_option_object_have_selected(select,attr_id_show,at) {
        console.log(select,attr_id_show,at)
        if(!attr_id_show)attr_id_show='id';
        var tem_ct = '';
        for (var i = 0; i < select.length; i++) {
            var text_selected= '';
            var id_current=resolve_keys_object(attr_id_show,select[i]);
            //if(String(select[i].id) ==String(at)){
            if(String(id_current) ==String(at)){
                text_selected ='selected="selected" value-lazy="'+at+'"'
            }
            //tem_ct += '<option '+text_selected+' value="' + select[i].id + '">' + select[i].name + '</option>';
            tem_ct += '<option '+text_selected+' value="' + id_current + '">' + select[i].name + '</option>';
           
        }
        return tem_ct;
    }
    function get_propertiobject(object1) {
        var element = [];

        for (var prop in object1) {
            if (object1.hasOwnProperty(prop)) {
                element.push(prop);
            }
        }
        return element;
    }

    function get_data_for_model(link, id, callback) {
        var data = {};
        data.item_in_page = 0;
        data.medicine_id = id;
        get_link_api_main(link, data, function (statusresult, result) {
            callback(result);
        })
    }

    function get_link_api_main(link_in, data, callback) {

        connectApi.getList(link_in, data, function (statusresult, result) {
            if (statusresult == http_status.HTTP_OK) {
                if (get_status_success_api(result.status.return_code)) {
                    callback(statusresult, result)
                } else {
                    glb_show_message(result.status.message + " : " + result.status.return_code)
                    callback(statusresult, result)
                }
            } else {
                alert('False Loading Data')
            }
        });
    }

var list_select_buil={};
