//list_function_use

//----------delete

function delete_more(link_tem,list_in, callback) {
    var list_string = list_in.join('_');
    var data = {};
    var link = link_tem + '/' + list_string;
    delete_at(data, link, function (statusresult, result) {
        callback(1)
    })
}

function delete_at(data, link, callback) {
    connectApi.deleteEvent(link, data, function (statusresult, result) {
        callback(statusresult, result)
    });
}

function get_allinptu_delete(list, more_one, list_name) {
    $('#delete form input').val(list);
    $('#delete form .at_change').text(list_name);

    $('#delete form .form-group .control-label ').hide();
    $('#delete form .form-group ' + more_one).show();
}

//--------------End Delete

//---add new
function add_row_table(temp_table) {
    var data = {};
    var list = get_list_th(temp_table)
    var list_vl = get_value_tfoot_value(temp_table);

    var list_requirement = get_input_requirement(temp_table);
    var list_error = [];
    for (var i = 0; i < list.length; i++) {
        if (i == 0) {

        } else {
            if (list[i] != undefined && list[i] != '') {
                var check = check_condition_type(list_requirement[i], list_vl[i])
                if (check == false) {
                    var text_column = get_attribute_text_column(temp_table, i);
                    list_error.push(text_column)
                } else {
                    data[list[i]] = list_vl[i];
                }
            }
        }
    }
    if (list_error.length > 0) {
        glb_show_message('Cần Nhập : ' + list_error.toString())
        console.log(list_error);
        return;
    }
    var link_api_table = $(temp_table).attr('data-link-api');
    save_add_data(link_api_table, data, function (statusresult, result) {
        if (parseInt(statusresult) == parseInt(http_status.HTTP_OK)) {
            if(get_status_success_api(result.status.return_code)){
                empty_after_sae(temp_table)
                loadTableInt(temp_table,link_api_table);
                show_hide_button(0);
            } else {
                //alert('False Loading Data')
            }
        } else {
            //glb_show_error_ajax(result)
        }
    });
}
function check_condition_type(type_check, value) {
    if (type_check == 'true' && (value == '' || value == undefined || value == null)) {
        return false;
    }
    return true;
}

function empty_after_sae(temp_table) {
    $(temp_table + ' tfoot tr th').find('input').val('');
}

function save_add_data(link, data, callback) {
    connectApi.addEvent(link, data, function (statusresult, result) {
        callback(statusresult, result);
    });
}

//-----End add

//----edit




function get_allinptu_push(list, more_one, list_name) {
    $('#push form input').val(list);
    $('#push form .at_change').text(list_name);

    $('#push form .form-group .control-label ').hide();
    $('#push form .form-group ' + more_one).show();

}
function get_allinptu_unpush(list, more_one, list_name) {
    $('#unpush form input').val(list);
    $('#unpush form .at_change').text(list_name);

    $('#unpush form .form-group .control-label ').hide();
    $('#unpush form .form-group ' + more_one).show();

}

function update_more_status(link_run, list_in, status, callback) {
    var list_string = list_in.join('_');
    var link = link_run + '/' + list_string;
    var data = {};
    data.stat = status;
    update_at(data, link, function (statusresult, result) {
        callback(1)
    })
}

function update_at(data, link, callback) {
    connectApi.updateEvent(link, data, function (statusresult, result) {
        if (statusresult == http_status.HTTP_OK) {
            if(get_status_success_api(result.status.return_code)){
                callback(statusresult, result)
            } else {
                glb_show_message(result.status.message + " : " + result.status.return_code)
                callback(statusresult, result)
            }
        } else {
            callback(statusresult, result)
        }
    });
}

//-----End edit

//---sort
function remove_all_sort(temp_table) {
    $(temp_table + ' thead tr th').removeClass('sortat');
    $(temp_table + ' thead tr th').removeClass('sorting_desc');
    $(temp_table + ' thead tr th').removeClass('sorting_asc');
    $(temp_table + ' thead tr th').attr('data-sort', '');
}
function change_sort_direction(temp_table) {
    var temp = $(temp_table + ' thead tr .sortat').attr('data-sort').toString().trim();
    var nameclass;
    if (temp == 'desc') {
        temp = 'asc';
        nameclass = 'sorting_asc';
    } else {
        temp = 'desc';
        nameclass = 'sorting_desc';
    }
    $(temp_table + ' thead tr .sortat').attr('data-sort', temp);
    $(temp_table + ' thead tr .sortat').removeClass('sorting_desc');
    $(temp_table + ' thead tr .sortat').removeClass('sorting_asc');
    $(temp_table + ' thead tr .sortat').addClass(nameclass);
    return temp;
}

//-----end sort

var connectApi = new APIConnect();
/*
function loadTableInt(temp_table,link_in) {
    console.log('load table from medicaine')

    var data = {};
    data.keyword = $(temp_table).closest('div').find('.row').eq(0).find('input[type="search"]').val();
    data.page_number = ($(temp_table).attr('page') ? $(temp_table).attr('page') : 0);
    data.item_in_page = $(temp_table).closest('div').find('.row').eq(0).find('select').val();
    var sortat = '';
    if ($(temp_table + ' thead .sortat') != undefined)
        if ($(temp_table + ' thead .sortat').attr('att-name') != undefined) {
            sortat = $(temp_table + ' thead .sortat').attr('att-name');
        }
    data.sort_by = sortat;
    data.sort_direction = $(temp_table + ' thead .sortat').attr('data-sort');

    connectApi.getList(link_in, data, function (statusresult, result) {
        if (statusresult == http_status.HTTP_OK) {
            if(result.status){
                if(get_status_success_api(result.status.return_code)){
                    show_data_for_table(result, temp_table);
                    build_number_page(temp_table)
                    un_check_bockall(temp_table)
                } else {
                    glb_show_message(result.status.message + " : " + result.status.return_code)
                }
            }else{
                glb_show_message(result + " : " + result)
            }
        } else {
            alert('False Loading Data')
        }
    });
    load_select_table_footer(temp_table)
    
}
*/
function loadTableInt(temp_table,link_in) {
    console.log('load table from function use patient')
    var data = {};
    data.keyword = $(temp_table).closest('div').find('.row').eq(0).find('input[type="search"]').val();
    data.page_number = ($(temp_table).attr('page') ? $(temp_table).attr('page') : 0);
    data.item_in_page = $(temp_table).closest('div').find('.row').eq(0).find('select').val();
    var sortat = '';
    if ($(temp_table + ' thead .sortat') != undefined)
        if ($(temp_table + ' thead .sortat').attr('att-name') != undefined) {
            sortat = $(temp_table + ' thead .sortat').attr('att-name');
        }
    data.sort_by = sortat;
    data.sort_direction = $(temp_table + ' thead .sortat').attr('data-sort');

    connectApi.getList(link_in, data, function (statusresult, result) {
        if (statusresult == http_status.HTTP_OK) {
            if(get_status_success_api(result.status.return_code)){
                
                show_data_for_table(result, temp_table);
                
                build_number_page(temp_table)
                un_check_bockall(temp_table)
            } else {
                glb_show_message(result.status.message + " : " + result.status.return_code)
            }
        } else {
            //alert('False Loading Data')
        }
    });
    load_select_table_footer(temp_table)
    
}


function load_select_table_footer(temp_table) {
    $(temp_table + ' tfoot tr th').each(function (index, value) {

        var get_tite = $(this).attr('data-type-select');
        if (get_tite != undefined) {
            if (get_tite == 'select-status') {
                //var text = load_template_select(get_tite);
                //$(this).html(text);
            } else {
                load_select_footer(temp_table, get_tite, index);
            }
        }
    })
}
function load_select_footer(temp_table, get_tite, index) {
    load_template_select_connect_api(get_tite, index, function (r_id, r_statusresult, r_result) {
        if (r_id != '') {
            var content = object_toselect_option(r_result,temp_table,index)
            $(temp_table + ' tfoot tr').find('th').eq(r_id).find('select').html(content);
        }
    })
}
function object_toselect_option(inobject,tem_table,at) {
    var content = '';
    if (inobject.data) {
        for (var i = 0; i < inobject.data.length; i++) {
            var show= get_attribute_tfoot_select_datashow(tem_table,at);
            if(show){
                content += '<option value="' + inobject.data[i].id + '">' + inobject.data[i][show] + '</option>';
            }else{
                content += '<option value="' + inobject.data[i].id + '">' + inobject.data[i].name + '</option>';
            }
        }
    }
    return content;
}


function show_data_for_table(datain, temp_table) {
    var data = datain.data;
    var content = '';
    var list_col = get_list_th(temp_table);
    var list_foot_modelshow = get_attribute_tfoot_modelshow(temp_table);
    for (var i = 0; i < data.length; i++) {
        content += '<tr id_colum="' + data[i].id + '">';
        for (var j = 0; j < list_col.length; j++) {
            if (j == 0) {
                content += '<td><input type="checkbox"></td>';
            } else if (list_col[j] != undefined) {
                var att_select = get_attribute_tfoot_select(temp_table, j);
                var post = list_col[j];
                var tem_content = data[i][list_col[j]];
                if (typeof (tem_content) == 'object') {
                    var show= get_attribute_tfoot_select_datashow(temp_table,j);
                    var temdata='';
                    if(show){
                        temdata = '<span att-value="' + tem_content['id'] + '">' + tem_content[show] + '</span>';
                    }else{
                        temdata = '<span att-value="' + tem_content['id'] + '">' + tem_content['name'] + '</span>';
                    }
                    tem_content = temdata;
                } else {
                    if (att_select != undefined) {
                        tem_content = get_text_select_value_at(template_status_vi[att_select], data[i][post]);
                    }
                }

                content += '<td>' + tem_content + '</td>';
            } else if(list_foot_modelshow[j] !=undefined && list_foot_modelshow[j] !=''){
                content += '<td>' +'<a href="#" class="btn btn-s-md btn-primary" data-toggle="modal" data-target="#'+list_foot_modelshow[j]+'">Sửa</a>'+ '</td>';
            }else {
                content += '<td></td>';
            }
        }
        content += '</tr>';
    }
    $(temp_table).attr('page', datain.page.current);
    $(temp_table).attr('limit', datain.page.limit);
    $(temp_table).attr('sumpage', datain.page.sumpage);
    $(temp_table).attr('id_table', temp_table);
    $(temp_table + ' tbody').html(content);
}

function show_data_for_table(datain, temp_table) {
    var data = datain.data;
    var content = '';
    var list_col = get_list_th(temp_table);
    var info_tr;
    //console.log(list_col)
    var at_id = $(temp_table).find('thead tr').attr('attr-show-id');
    var at_parent = $(temp_table).find('thead tr').attr('attr-id-parent');
    if(!at_id){at_id='id'}
    for (var i = 0; i < data.length; i++) {
        info_tr =get_info_tr_class_event(temp_table,data[i].id)
        //content += '<tr id_colum="' + data[i].id+'" ' +info_tr+ '>';
        var id_get_book=resolve_keys_object(at_id,data[i]);
        var str_for_request=id_get_book;
        if(at_parent){
            var id_get_parent=resolve_keys_object(at_parent,data[i]);
            str_for_request += '_'+id_get_parent
        }
        
        // content += '<tr id_colum="' + resolve_keys_object(at_id,data[i])  + '">';
        content += '<tr id_colum="' + str_for_request  + '">';
        for (var j = 0; j < list_col.length; j++) {
            if (j == 0) {
                content += '<td><input type="checkbox"></td>';
            } else if (list_col[j] != undefined) {
                var att_select = get_attribute_tfoot_select(temp_table, j);
                //console.log(temp_table, j)
                var post = list_col[j];
                //console.log(data[i])
                //console.log(list_col[j])
                
                var tem_content1 = data[i];
                var tem_content = resolve_keys_object(list_col[j],tem_content1);
                //list_col[j]
                //console.log(list_key)
                //var text = "['user_id']['username']";
                console.log(list_col[j])
                //return 
                if (typeof (tem_content1) == 'object') {
                    console.log('w1')
                    var show= get_attribute_tfoot_select_datashow(temp_table,j);
                    var temdata='';
                    //alert(show)
                    var at = list_col[j].lastIndexOf('.');
                    var remove_last_element_dot = list_col[j].substring(0,at);
                    var get_attr_id=remove_last_element_dot+'.id';
                
                    if(show){
                        
                        //have infor span
                        // temdata = '<span att-value="' + tem_content['id'] + '">' + tem_content[show] + '</span>';
                        temdata = '<span att-value="' + resolve_keys_object(get_attr_id,tem_content1) + '">' + resolve_keys_object(list_col[j],tem_content1) + '</span>';
                    }else{
                        //no have span
                        // if(tem_content){
                        //     if(tem_content['id']){
                        //         //temdata = '<span att-value="' + tem_content['id'] + '">' + tem_content['name'] + '</span>';
                        //         temdata = resolve_keys_object(list_col[j],tem_content1);
                        //     }
                        // }
                        temdata = resolve_keys_object(list_col[j],tem_content1);
                        
                    }
                    tem_content = temdata;
                } else {
                    console.log('w2')
                    console.log(tem_content)
                    if (att_select != undefined) {
                        tem_content = get_text_select_value_at(template_status_vi[att_select], data[i][post]);
                    }
                }
                
                console.log(tem_content)
                content += '<td>' + tem_content + '</td>';
            } else {
                content += '<td></td>';
            }
        }
        content += '</tr>';
    }
    $(temp_table).attr('page', datain.page.current);
    $(temp_table).attr('limit', datain.page.limit);
    $(temp_table).attr('sumpage', datain.page.sumpage);
    $(temp_table).attr('id_table', temp_table);
    $(temp_table + ' tbody').html(content);
}

function get_list_th(temp_table) {
    var datareturn = [];
    $(temp_table + ' thead tr th').each(function (index, value) {
        datareturn.push(get_attribute_name_column(temp_table, index));
    });
    return datareturn;
}
function get_attribute_tfoot_select(temp_table, position) {
    return $(temp_table + ' tfoot tr').find('th').eq(position).attr('data-type-select');
}
function get_attribute_tfoot_select_datashow(temp_table, position) {
    return $(temp_table + ' tfoot tr').find('th').eq(position).attr('data-show');
}
function get_attribute_tfoot_modelshow(temp_table) {
    var datareturn = [];
    $(temp_table + ' thead tr th').each(function (index, value) {
        datareturn.push(get_attribute_name_model(temp_table, index));
    });
    return datareturn;
}
function get_attribute_name_model(temp_table, position) {
    return $(temp_table + ' tfoot tr').find('th').eq(position).attr('modelshow');
}
function get_attribute_name_column(temp_table, position) {
    return $(temp_table + ' thead tr').find('th').eq(position).attr('att-name');
}
function get_attribute_text_column(temp_table, position) {
    return $(temp_table + ' thead tr').find('th').eq(position).text();
}
function load_place_holder(temp_table) {
    $(temp_table + ' tfoot tr th').each(function (index, value) {
        if ($(this).find('input').attr('type') == 'text') {
            $(this).find('input').attr('placeholder', get_attribute_text_column(temp_table, index));
        }
        var get_tite = $(this).attr('data-type-select');
        if (get_tite != undefined) {
            if (get_tite == 'select-status') {
                var text = load_template_select(get_tite);
                $(this).html(text);
            } else {
                var text = load_template_select('select-empty');
                $(this).html(text);
            }

        }
    })
}

function load_template_select(at) {
    var content_show = '';
    if (template_status_vi[at]) {
        var content_default = build_html_select_option(template_status_vi[at]);
        content_show = '<select class="select-at-table">' + content_default + '</select>';
    }
    return content_show;
}

function load_template_select_connect_api(at, id, callback) {
    if(!at)return
    var data = {};
    data.item_in_page=0;
    connectApi.getList(link_api + at, data, function (statusresult, result) {
        if (statusresult == http_status.HTTP_OK) {
            if(get_status_success_api(result.status.return_code)){
                //show_data_for_table(result, temp_table);
                //build_number_page(temp_table)
                //console..log(result)
                callback(id, statusresult, result);
            } else {
                callback('', statusresult, result);
                glb_show_message(result.status.message + " : " + result.status.return_code)
            }
        } else {
            callback('', statusresult, result);
            glb_show_message(result.status.message + " : " + result.status.return_code)
        }
    });

}

function build_html_select_option(select) {
    var tem_ct = '';
    for (var i = 0; i < select.length; i++) {
        tem_ct += '<option value="' + select[i].id + '">' + select[i].show + '</option>';
    }
    return tem_ct;
}
function get_text_select_value_at(tem_obj, at) {
    //console.log(tem_obj, at)
    if (tem_obj == undefined) {
        return '';
    }
    var result = tem_obj.filter(function (item, key) {
        return item['id'] == at;
    })
    if (result.length > 0) {
        if (undefined != result[0]['id']) {
            return '<span att-value="' + result[0]['id'] + '">' + result[0]['show'] + '</span>';
        } else {
            return '';
        }
    } else {
        return at;
    }
}

function show_hide_button(temp_table) {
    var c_current = 0;
    var c_current_leng = 0;
    $(temp_table + ' tbody input[type="checkbox"]').each(function () {
        c_current_leng++;
        var sThisVal = $(this).prop('checked');
        if (sThisVal) {
            c_current++;
        }
    });
    var s_tt = false;
    if (c_current_leng == c_current) {
        s_tt = true;
    } else {
        s_tt = false;
    }
    $(temp_table + ' thead input[type="checkbox"]').prop('checked', s_tt);
    if (c_current == 0) {
        $('#btn_edit').addClass('disabled');
        $('#btn_publish').addClass('disabled');
        $('#btn_unpublish').addClass('disabled');
        $('#btn_delete').addClass('disabled');
    }
    if (c_current == 1) {
        $('#btn_edit').removeClass('disabled');
        $('#btn_publish').removeClass('disabled');
        $('#btn_unpublish').removeClass('disabled');
        $('#btn_delete').removeClass('disabled');
    }
    if (c_current > 1) {
        $('#btn_edit').addClass('disabled');
        $('#btn_publish').removeClass('disabled');
        $('#btn_unpublish').removeClass('disabled');
        $('#btn_delete').removeClass('disabled');
    }
}

function get_value_tfoot_value(temp_table) {
    var tem = [];
    $(temp_table + ' tfoot th').each(function (index, value) {
        if ($(this).find('input').attr('type') == 'text') {
            tem.push($(this).find('input').val())
        } else if ($(this).find('input').attr('type') == 'checkbox') {
            tem.push($(this).find('input').val)
        } else if ($(this).find('select').length > 0) {
            tem.push($(this).find('select').val())
        } else {
            tem.push($(this).text())
        }
    })
    return tem;
}
function get_input_requirement(temp_table) {

    var datareturn = [];
    $(temp_table + ' thead tr th').each(function (index, value) {
        var att_requi = $(this).attr('att-requirement');
        if (att_requi != undefined && att_requi != '') {
            datareturn.push(att_requi);
        } else {
            datareturn.push('');
        }
    });
    return datareturn;

    var tem = [];
    $(temp_table + ' tfoot th').each(function (index, value) {
        if ($(this).find('input').attr('type') == 'text') {
            tem.push($(this).find('input').val())
        } else if ($(this).find('input').attr('type') == 'checkbox') {
            tem.push($(this).find('input').val)
        } else if ($(this).find('select').length > 0) {
            tem.push($(this).find('select').val())
        } else {
            tem.push($(this).text())
        }
    })
    return tem;
}

function un_check_bockall(tem_table){
    var test= $(tem_table + ' thead input[type="checkbox"]').eq(0);
    if(test.is(":checked")){
        $(test).prop( "checked", false );
    }
}

function get_status_success_api(return_code){
    if(return_code == api_status.API_STATUS_OK || return_code == api_status.API_STATUS_EMPTY){
        return true;
    }else{
        return false;
    }
}