!function ($) {

  $(function(){
 	
	// sparkline
	var sr, sparkline = function($re){
		$(".sparkline").each(function(){
			var $data = $(this).data();
			if($re && !$data.resize) return;
			($data.type == 'pie') && $data.sliceColors && ($data.sliceColors = eval($data.sliceColors));
			($data.type == 'bar') && $data.stackedBarColor && ($data.stackedBarColor = eval($data.stackedBarColor));
			$data.valueSpots = {'0:': $data.spotColor};
			$(this).sparkline('html', $data);
		});
	};
	$(window).resize(function(e) {
		clearTimeout(sr);
		sr = setTimeout(function(){sparkline(true)}, 500);
	});
	sparkline(false);


	// easypie
    $('.easypiechart').each(function(){
    	var $this = $(this), 
    	$data = $this.data(), 
    	$step = $this.find('.step'), 
    	$target_value = parseInt($($data.target).text()),
    	$value = 0;
    	$data.barColor || ( $data.barColor = function($percent) {
            $percent /= 100;
            return "rgb(" + Math.round(200 * $percent) + ", 200, " + Math.round(200 * (1 - $percent)) + ")";
        });
    	$data.onStep =  function(value){
    		$value = value;
    		$step.text(parseInt(value));
    		$data.target && $($data.target).text(parseInt(value) + $target_value);
    	}
    	$data.onStop =  function(){
    		$target_value = parseInt($($data.target).text());
    		$data.update && setTimeout(function() {
		        $this.data('easyPieChart').update(100 - $value);
		    }, $data.update);
    	}
		$(this).easyPieChart($data);
	});

  	// combodate
	$(".combodate").each(function(){ 
		$(this).combodate();
		$(this).next('.combodate').find('select').addClass('form-control');
	});

	function get_today_date(){
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!

		var yyyy = today.getFullYear();
		if(dd<10){
				dd='0'+dd;
		} 
		if(mm<10){
				mm='0'+mm;
		} 
		var today = dd+'/'+mm+'/'+yyyy;
		return today;
	}

	// datepicker
	// $(".datepicker-input").each(function(){ $(this).datepicker();});

	// datetimepicker
	// https://xdsoft.net/jqplugins/datetimepicker/
	// console.log(new Date().toJSON().slice(0,10));
	// console.log(get_today_date());
	// jQuery('.datetimepicker-date').val(new Date().toJSON().slice(0,10));
	if(jQuery('.datetimepicker-date').length > 0) 
	// if(jQuery('.datetimepicker-date'))
	jQuery('.datetimepicker-date').datetimepicker({
		// value:get_today_date(),
		// lang: 'vn',
		//mask:true,
		// minDate:0,
		//defaultDate:new Date(), //position date show
		//defaultDate:$(this).val(),
		// defaultDate:'12.12.1987',
		//format: 'Y/m/d',
		format: 'd/m/Y',
		timepicker:false,
		weeks:true,
		scrollMonth : false,
		scrollInput : false,
		scrollTime : false,
		// onGenerate:function( ct ){
		// 	jQuery(this).find('.xdsoft_date.xdsoft_weekend')
		// 		.addClass('xdsoft_disabled');
		// },
		// weekends:['01/01/2014','02/01/2014','03/01/2014','04/01/2014','05/01/2014','06/01/2014'],	
	});
	if(jQuery('.datetimepicker-datetoday').length > 0) 
	jQuery('.datetimepicker-datetoday').datetimepicker({
		// lang: 'vn',
		value:get_today_date(),
		mask:true,
		minDate:0,//today
		defaultDate:new Date(), //position date show
		// defaultDate:'12.12.1987',
		//format: 'Y-m-d',
		format: 'd/m/Y',
		timepicker:false,
		weeks:true,
		scrollMonth : false,
		scrollInput : false,
		scrollTime : false,
		onGenerate:function( ct ){
			jQuery(this).find('.xdsoft_date.xdsoft_weekend')
				.addClass('xdsoft_disabled');
		},
		weekends:['01/01/2014','02/01/2014','03/01/2014','04/01/2014','05/01/2014','06/01/2014'],	
	});
	if(jQuery('.datetimepicker').length > 0) 
	jQuery('.datetimepicker').datetimepicker(
		{
                    format: 'H:i d/m/Y',
		}
	);

	// dropfile
	$('.dropfile').each(function(){
		var $dropbox = $(this);
		if (typeof window.FileReader === 'undefined') {
		  $('small',this).html('File API & FileReader API not supported').addClass('text-danger');
		  return;
		}

		this.ondragover = function () {$dropbox.addClass('hover'); return false; };
		this.ondragend = function () {$dropbox.removeClass('hover'); return false; };
		this.ondrop = function (e) {
		  e.preventDefault();
		  $dropbox.removeClass('hover').html('');
		  var file = e.dataTransfer.files[0],
		      reader = new FileReader();
		  reader.onload = function (event) {
		  	$dropbox.append($('<img>').attr('src', event.target.result));
		  };
		  reader.readAsDataURL(file);
		  return false;
		};
	});

	// fuelux pillbox
	var addPill = function($input){
		var $text = $input.val(), $pills = $input.closest('.pillbox'), $repeat = false, $repeatPill;
		if($text == "") return;
		$("li", $pills).text(function(i,v){
	        if(v == $text){
	        	$repeatPill = $(this);
	        	$repeat = true;
	        }
	    });
	    if($repeat) {
	    	$repeatPill.fadeOut().fadeIn();
	    	return;
	    };
	    $item = $('<li class="label bg-dark">'+$text+'</li> ');
		$item.insertBefore($input);
		$input.val('');
		$pills.trigger('change', $item);
	};

	$('.pillbox input').on('blur', function() {
		addPill($(this));
	});

	$('.pillbox input').on('keypress', function(e) {
	    if(e.which == 13) {
	        e.preventDefault();
	        addPill($(this));
	    }
	});

	// slider
	$('.slider').each(function(){
		$(this).slider();
	});

	// wizard
  $(document).on('change', '.wizard', function (e, data) {
    if(data.direction !== 'next' ) return;
    var item = $(this).wizard('selectedItem');
    var $step = $(this).find('.step-pane:eq(' + (item.step-1) + ')');
    var validated = true;
    $('[data-required="true"]', $step).each(function(){
      return (validated = $(this).parsley( 'validate' ));
    });
    if(!validated) return e.preventDefault();
  });

	// sortable
	if ($.fn.sortable) {
	  $('.sortable').sortable();
	}

	// slim-scroll
	$('.no-touch .slim-scroll').each(function(){
		var $self = $(this), $data = $self.data(), $slimResize;
		$self.slimScroll($data);
		$(window).resize(function(e) {
			clearTimeout($slimResize);
			$slimResize = setTimeout(function(){$self.slimScroll($data);}, 500);
		});

    $(document).on('updateNav', function(){
      $self.slimScroll($data);
    });
	});

	// pjax
	if ($.support.pjax) {
	  $(document).on('click', 'a[data-pjax]', function(event) {
	  	event.preventDefault();
	    var container = $($(this).data('target'));
	    $.pjax.click(event, {container: container});
	  })
	};

	// portlet
	$('.portlet').each(function(){
		$(".portlet").sortable({
	        connectWith: '.portlet',
            iframeFix: false,
            items: '.portlet-item',
            opacity: 0.8,
            helper: 'original',
            revert: true,
            forceHelperSize: true,
            placeholder: 'sortable-box-placeholder round-all',
            forcePlaceholderSize: true,
            tolerance: 'pointer'
	    });
    });

	// docs
    $('#docs pre code').each(function(){
	    var $this = $(this);
	    var t = $this.html();
	    $this.html(t.replace(/</g, '&lt;').replace(/>/g, '&gt;'));
	});

	// fontawesome
	$(document).on('click', '.fontawesome-icon-list a', function(e){
		e && e.preventDefault();
	});

	// table select/deselect all
	$(document).on('change', 'table thead [type="checkbox"]', function(e){
		e && e.preventDefault();
		var $table = $(e.target).closest('table'), $checked = $(e.target).is(':checked');
		$('tbody [type="checkbox"]',$table).prop('checked', $checked);
	});

	// random progress
	$(document).on('click', '[data-toggle^="progress"]', function(e){
		e && e.preventDefault();

		$el = $(e.target);
		$target = $($el.data('target'));
		$('.progress', $target).each(
			function(){
				var $max = 50, $data, $ps = $('.progress-bar',this).last();
				($(this).hasClass('progress-xs') || $(this).hasClass('progress-sm')) && ($max = 100);
				$data = Math.floor(Math.random()*$max)+'%';
				$ps.css('width', $data).attr('data-original-title', $data);
			}
		);
	});
	
	// add notes
	function addMsg($msg){
		var $el = $('.nav-user'), $n = $('.count:first', $el), $v = parseInt($n.text());
		$('.count', $el).fadeOut().fadeIn().text($v+1);
		$($msg).hide().prependTo($el.find('.list-group')).slideDown().css('display','block');
	}
	var $msg = '<a href="#" class="media list-group-item">'+
                  '<span class="pull-left thumb-sm text-center">'+
                    '<i class="fa fa-envelope-o fa-2x text-success"></i>'+
                  '</span>'+
                  '<span class="media-body block m-b-none">'+
                    'Sophi sent you a email<br>'+
                    '<small class="text-muted">1 minutes ago</small>'+
                  '</span>'+
                '</a>';	
  setTimeout(function(){addMsg($msg);}, 1500);

	// select2 
 	if ($.fn.select2) {
      $("#select2-option").select2();
      $("#select2-tags").select2({
        tags:["red", "green", "blue"],
        tokenSeparators: [",", " "]}
      );
  	}


  });
}(window.jQuery);